<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Routes group config
    |--------------------------------------------------------------------------
    |
    | The default group settings for the elFinder routes.
    |
    */
    'route'          => [
        'prefix'     => 'translations',
        'middleware' => 'auth',
    ],

    /**
     * Enable deletion of translations
     *
     * @type boolean
     */
    'delete_enabled' => true,

    /**
     * Exclude specific groups from Laravel Translation Manager.
     * This is useful if, for example, you want to avoid editing the official Laravel language files.
     *
     * @type array
     *
     *    array(
     *        'pagination',
     *        'reminders',
     *        'validation',
     *    )
     */
    'exclude_groups' => [
        'admin/clientes',
        'admin/descargas',
        'admin/descargas_categorias',
        'admin/faqs',
        'admin/faqs_categorias',
        'admin/media',
        'admin/menus',
        'admin/ofertas',
        'admin/paginas',
        'admin/paginas_categorias',
        'admin/permissions',
        'admin/producto_puntos',
        'admin/puntos_ranking_report',
        'admin/punto_report',
        'admin/roles',
        'admin/rotulos',
        'admin/seo',
        'admin/seo_enlaces',
        'admin/slides',
        'admin/traducciones',
        'admin/users',
        'admin/vademecum',
        'admin/vademecum_agrupaciones',
        'admin/vademecum_categorias',
        'admin/vademecum_subcategorias',
        'validation',
        'passwords',
        'pagination',
        'auth'
    ],

    /**
     * Exclude specific languages from Laravel Translation Manager.
     *
     * @type array
     *
     *    array(
     *        'fr',
     *        'de',
     *    )
     */
    'exclude_langs'  => [],

    /**
     * Export translations with keys output alphabetically.
     */
    'sort_keys '     => false,

    'trans_functions' => [
        'trans',
        'trans_choice',
        'Lang::get',
        'Lang::choice',
        'Lang::trans',
        'Lang::transChoice',
        '@lang',
        '@choice',
        '__',
        '$trans.get',
    ],

];
