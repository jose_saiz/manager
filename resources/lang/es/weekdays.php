<?php

return [
    
    /* Contenido estático de la sección faq
     |--------------------------------------------------------------------------
     | Saizfact, última actualización de esta documentación realizada el 16/05/2022
     | page_title: Etiqueta del título de la página utilizada en el archivo /views/partials/breadcrumbs
     |
     | parent_section: Etiqueta del nombre de la sección padre utilizada en el archivo /views/partials/breadcrumbs
     |
     | subsection: Etiqueta del nombre de la subsección (en el caso de que esta vista tuviera una sección por encima),
     | puede dejarse en blanco si no fuera una subsección, utilizada en el archivo /views/partials/breadcrumbs
     |
     | new: Etiqueta de nuevo elemento, utilizada en el archivo /views/partials/breadcrumbs.
     |
     | edit: Etiqueta de edición de elemento, utilizada en el archivo  /views/partials/breadcrumbs. Recibe como
     | parametro el nombre del elemento (:name).
     |
     | modal: Mensaje de aviso en la ventana modal al eliminar un elemento, utilizada en el archivo
     | /views/partials/delete_modal.
     |
     |--------------------------------------------------------------------------
     */
    
    '0' => 'Domingo',
    '1' => 'Lunes',
    '2' => 'Martes',
    '3' => 'Miercoles',
    '4' => 'Jueves',
    '5' => 'Viernes',
    '6' => 'Sábado',
];