<?php

return array (
    
    'printer' => '/{locale}/impresora/{categoria}/{printer}',
    'printers-category' => '/{locale}/impresora/{categoria}',
    'printers' => '/{locale}/impresoras',
    
    'test' => '/prueba/{categoria}/{test}',
    'tests-category' => '/pruebas/{categoria}',
    'tests' => '/pruebas',
    
    'project' => '/project/{categoria}/{proyecto}',
    'projects-category' => '/projects/{categoria}',
    'projects' => '/projects',
);
