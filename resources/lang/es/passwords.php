<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contrase�as deben tener al menos ocho caracteres y coincidir con la confirmaci�n.',
    'reset' => '�Tu contrase�a ha sido restablecida!',
    'sent' => '�Hemos enviado por correo electr�nico el enlace para restablecer su contrase�a!',
    'token' => 'Este token de restablecimiento de contrase�a no es v�lido.',
    'user' => "No podemos encontrar un usuario con esa direcci�n de correo electr�nico.",

];
