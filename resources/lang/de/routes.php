<?php

return array (
    
    'printer' => '/{locale}/printer/{categoria}/{printer}',
    'printers-category' => '/{locale}/printers/{categoria}',
    'printers' => '/{locale}/printers',
    
    
    'test' => '/test/{categoria}/{test}',
    'tests-category' => '/tests/{categoria}',
    'tests' => '/tests',
    
    
    'project' => '/proyecto/{categoria}/{proyecto}',
    'projects-category' => '/proyectos/{categoria}',
    'projects' => '/proyectos',
);
