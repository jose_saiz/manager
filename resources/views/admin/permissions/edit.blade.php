@php
    $route = 'permissions';
@endphp

@extends('admin.layout.form', [
    'route' => $route, 
    'name' => $permission->name, 
    'id' => $permission->id
])

@section('form')

    @include('admin.partials.delete_modal', ['route' => $route, 'name' => $permission->name])
    
    <div class="white-box">
    
        @include('admin.partials.form_title', ['route' => $route, 'name' => $permission->name])
        @canatleast(['edit.permisos'])
            @include('admin.partials.crud_buttons', ['id' => $permission->id])
        @endcanatleast
        @include('admin.partials.errors')
    
        <div>
            <form class='admin-form' id='permissions-form' action="{{route('permissions_store')}}">
        
                {{ csrf_field() }}
        
                @isset ($permission->id)
                    <input type="hidden" name="id" value="{{$permission->id}}">
                @endisset
        
                <div class="form-group row m-t-10">
                    <label for="name" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                        Nombre *
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="name" value="{{ old('_token') ? old('name') : $permission->name }}" class="form-control {{ ($errors->has('name')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
        
                <div class="form-group row m-t-10">
                    <label for="slug" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('slug')) ? 'text-danger' : '' }}">
                        Slug *
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="slug" value="{{ old('_token') ? old('slug') : $permission->slug }}" class="form-control {{ ($errors->has('slug')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
                
                <div class="form-group row m-t-10">
                    <label for="route" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('route')) ? 'text-danger' : '' }}">
                        Ruta / URL 
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="route" value="{{ old('_token') ? old('route') : $permission->route }}" class="form-control {{ ($errors->has('route')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
                
                <div class="form-group row m-t-10">
                    <label for="description" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('description')) ? 'text-danger' : '' }}">
                        Descripción 
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="description" value="{{ old('_token') ? old('description') : $permission->description }}" class="form-control {{ ($errors->has('description')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
                
                <input type="hidden" name="activo" value="true" />
                {{--
                <div class="form-group row">
                    <label for="activo" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('activo')) ? 'text-danger' : '' }}">
                        Activo 
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="checkbox" name="activo" value="true" {{ (old('_token') ? old('activo') : $permission->activo) ? 'checked' : '' }} class="bt-switch" data-size="small">
                    </div>
                </div>
                --}}
            </form>
        </div>
    </div>

@endsection
