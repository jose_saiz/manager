{{--
    |
    | *$categorias se recoge de /app/Http/ViewComposers/Admin/TestsCategorias 08/11/2017_saizfact
    |    
    --}}

@php
    $route = 'admin_tests';
@endphp

@extends('admin.layout.form', [
    'route' => 'tests', 
    'name' => $test->nombre, 
    'id' => $test->id
])

@section('form')

    @include('admin.partials.delete_modal', ['route' => $route, 'name' => $test->nombre])
    
    <div class="white-box">
    
        @include('admin.partials.form_title', ['route' => $route, 'name' => $test->nombre])
        @canatleast(['edit.tests'])
            @include('admin.partials.crud_buttons', ['id' => $test->id])
        @endcanatleast
        @include('admin.partials.errors')
    
        <div>
            <form class='admin-form' id='tests-form' action="{{route($route . '_store')}}">
        
                {{ csrf_field() }}
                
                <ul class="nav nav-tabs" role="tablist">
        
                    <li role="presentation" class="nav-item">
                        <a href="#contenido" class="nav-link active" aria-controls="contenido" role="tab" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <span class="hidden-xs">Contenido</span>
                        </a>
                    </li>
        
                    <li role="presentation" class="nav-item">
                        <a href="#seo" class="nav-link" aria-controls="seo" role="tab" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">
                                <i class="fa fa-tag"></i>
                            </span>
                            <span class="hidden-xs">SEO</span>
                        </a>
                    </li>
        
                </ul>
                
                @isset ($test->id)
                    <input type="hidden" name="id" value="{{$test->id}}">
                @endisset
 
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="contenido">       
                        <div class="form-group row m-t-10">
                            <label for="nombre" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">
                                Nombre *
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <input type="text" name="nombre" value="{{ old('_token') ? old('nombre') : $test->nombre }}" class="form-control {{ ($errors->has('nombre')) ? 'is-invalid' : '' }}">
                            </div>
                        </div>
                
                        <div class="form-group row m-t-10">
                            <label for="categoria_id" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('categoria_id')) ? 'text-danger' : '' }}">
                                Categoría *
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <select name="categoria_id" data-placeholder="Seleccione una categoría" class="form-control select2 {{ ($errors->has('categoria_id')) ? 'is-invalid' : '' }}">
                                    <option></option>
                                    @foreach($categorias as $categoria)
                                        <option value="{{$categoria->id}}" {{$test->categoria_id == $categoria->id ? 'selected':''}} class="{{$categoria->activo ? '':'desactivado'}}">{{ $categoria->nombre }}</option>
                                    @endforeach
                                </select>                   
                            </div>
                        </div>
                
                        <div class="form-group row">
                            <label for="activo" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('activo')) ? 'text-danger' : '' }}">
                                Activo *
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <input type="checkbox" name="activo" value="true" {{ (old('_token') ? old('activo') : $test->activo) ? 'checked' : '' }} class="bt-switch" data-size="small">
                            </div>
                        </div>
                
                        <div class="form-group row">
                            <label for="orden" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('orden')) ? 'text-danger' : '' }}">
                                Orden *
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <input type="text" name="orden" value="{{ old('_token') ? old('orden') : $test->orden }}" class="form-control {{ ($errors->has('orden')) ? 'is-invalid' : '' }}">
                            </div>
                        </div>
                
                        @component('admin.partials.locale', ['tab' => 'contenido'])
                
                        @foreach ($idiomas as $idioma)
                
                        <div role="tabpanel" class="tab-pane {{ $loop->first ? ' active' : '' }}" id="contenido-{{$idioma->alias}}">
                            <div class="form-group row m-t-10">
                                <label for="titulo" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">
                                    Título *
                                </label>
                                <div class="col-lg-9 col-md-8">
                                    <input type="text" name="locale[titulo.{{$idioma->alias}}]" value='{{$locale["titulo.$idioma->alias"] }}' class="form-control">
                                </div>
                            </div>
                
                            <div class="form-group row m-t-10">
                                <label for="descripcion" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">
                                    Descripción *
                                </label>
                                <div class="col-lg-12 col-md-12">
                                    <textarea name="locale[descripcion.{{$idioma->alias}}]" class="rich" id="ckeditor.{{$idioma->alias}}" class="form-control">
                                        {{$locale["descripcion.$idioma->alias"]}}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endcomponent
                    </div>
                    <div role="tabpanel" class="tab-pane" id="seo">
                        @component('admin.partials.locale', ['tab' => 'seo'])
                        @foreach ($idiomas as $idioma)
                        <div role="tabpanel" class="tab-pane {{ $loop->first ? ' active' : '' }}" id="seo-{{$idioma->alias}}">
                            <div class="form-group row m-t-10">
                                <label for="name" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                                    Descripción *
                                </label>
                                <div class="col-lg-9 col-md-8">
                                    <input type="text" name="seo[description.{{$idioma->alias}}]" value='{{$seo["description.$idioma->alias"] }}' class="form-control">
                                </div>
                            </div>
        
                            <div class="form-group row m-t-10">
                                <label for="slug" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('slug')) ? 'text-danger' : '' }}">
                                    Keywords *
                                </label>
                                <div class="col-lg-9 col-md-8">
                                    <input type="text" name="seo[keywords.{{$idioma->alias}}]" value='{{$seo["keywords.$idioma->alias"] }}' class="form-control">
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endcomponent
                    </div>
                </div>
            </form>
       </div>
    </div>

@endsection
