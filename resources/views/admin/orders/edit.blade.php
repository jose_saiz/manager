{{--
    |
    | *$categorias se recoge de /app/Http/ViewComposers/Admin/OrdersCategorias 25/04/2022_saizfact
    |    
    --}}

@php
    $route = 'admin_orders';
@endphp

@extends('admin.layout.form_table', [
    'route' => $route, 
    'parent_section' => 'admin_orders', 
    'name' => $order->id, 
    'id' => $order->id
])

@section('form')

    @include('admin.partials.delete_modal', ['route' => $route, 'name' => $order->customer_id])
    
    <div class="white-box">
    
        {{--@include('admin.partials.form_title', ['route' => $route, 'name' => $order->code])--}}
        @canatleast(['edit.orders'])
            @include('admin.partials.crud_buttons', ['id' => $order->id])
        @endcanatleast
        @include('admin.partials.errors')
    
        <div>
            <form class='admin-form' id='orders-form' action="{{route($route . '_store')}}">
        
                {{ csrf_field() }}
                
                @isset ($order->id)
                    <input type="hidden" name="id" value="{{$order->id}}">
                @endisset
                    <input type="hidden" name="old_state_id" value="{{$order->state_id}}">
                @if ($order->state_id > 20)
                    <input type="hidden" name="customer_id" id="customer_id" value="{{$order->customer_id}}">
                     @foreach($products as $product)
                     <input type="hidden" name="quantity-{{$product->id}}" id="quantity-{{$product->id}}" value="{{$quantity[$product->id]}}">
                     @endforeach
                @endif
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="contenido">       
                        <div class="panel-body">
                            <div class="form-group row m-t-10">
                                <label for="customer_id" class="col-lg-5 col-md-5 col-form-label {{ ($errors->has('customer_id')) ? 'text-danger' : '' }}">
                                    Customer *
                                </label>
                                <div class="col-lg-7 col-md-7">
                                    <select name="customer_id" id="customer_id" data-placeholder="Select customer" 
                                            class="form-control select2 {{ ($errors->has('customer_id')) ? 'is-invalid' : '' }}"
                                            {{$order->state_id > 20?'disabled':'' }}>
                                        <option></option>
                                        @foreach($customers as $customer)
                                            <option value="{{$customer->id}}" {{$order->customer_id == $customer->id || (!isset($order->customer_id))? 'selected': ''}} {{$customer->active ?'':'disabled'}} class="{{$customer->active ? '':'desactivado'}}">{{ $customer->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-lg-1 col-md-1"></div>
                            </div>
                            @foreach($products as $product)
                            <div class="form-group row m-t-10">
                                <label for="quantity-{{$product->id}}" class="col-lg-5 col-md-5 col-form-label {{ ($errors->has('orden')) ? 'text-danger' : '' }}">
                                    {{$product->name}}:
                                </label>
                                <div class="col-lg-4 col-md-4">
                                    <input type="number" id="quantity-{{$product->id}}" name="quantity-{{$product->id}}" 
                                           value = "{{$quantity[$product->id]}}" min="0" max="150" class="form-control"
                                           {{$order->state_id > 20?'disabled':'' }}>
                                </div>
                            </div>
                            @endforeach
                            <div class="form-group row m-t-10">
                                <label for="priority" class="col-lg-5 col-md-5 col-form-label {{ ($errors->has('orden')) ? 'text-danger' : '' }}">
                                    Priority:
                                </label>
                                <div class="col-lg-4 col-md-4">
                                    <input type="number" id="priority" name="priority" min="0" max="100" 
                                           value="{{$order->priority}}" class="form-control"
                                           {{$order->state_id > 20?'disabled':'' }}>
                                </div>
                            </div>

                            <div class="form-group row m-t-10">
                                <label for="state_id" class="col-lg-5 col-md-5 col-form-label {{ ($errors->has('state_id')) ? 'text-danger' : '' }}">
                                    State
                                </label>
                                <div class="col-lg-7 col-md-7">
                                    <select name="state_id"  id="state_id" data-placeholder="Select state" 
                                            class="form-control select2 {{ ($errors->has('state_id')) ? 'is-invalid' : '' }}"
                                            {{!isset ($order->id)?'disabled':''}}>
                                            
                                        <option></option>
                                        @foreach($states_order as $state)
                                        	@if ((($order->state_id > 20) && ($state->id != 20) ) || ($order->state_id == 20))
                                            <option value="{{$state->id}}" {{$order->state_id == $state->id || $state->id == 20 ? 'selected': ''}} >
                                                {{ $state->name }}
                                            </option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div><!-- END <div class="panel-body"> -->
                    </div> <!-- END <div role="tabpanel" class="tab-pane active" id="contenido"> -->
                    
                </div><!--END <div class="tab-content"> -->
            </form>
       </div>
       
    </div>
@endsection
@section('table')
    <div class="white-box">
          <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="nav-item">
                <a href="#order-lines" class="nav-link active" aria-controls="order-lines" role="tab" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs">
                        <i class="fa fa-pencil"></i>
                    </span>
                    <span class="hidden-xs">Order Lines</span>
                </a>
            </li>
            <li role="presentation" class="nav-item">
                <a href="#products" class="nav-link" aria-controls="products" role="tab" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs">
                        <i class="fa fa-pencil"></i>
                    </span>
                    <span class="hidden-xs">Final Products</span>
                </a>
            </li>
            <li role="presentation" class="nav-item">
                <a href="#parts" class="nav-link" aria-controls="parts" role="tab" data-toggle="tab" aria-expanded="true">
                    <span class="visible-xs">
                        <i class="fa fa-pencil"></i>
                    </span>
                    <span class="hidden-xs">Parts</span>
                </a>
            </li>

        </ul>
        <div class="row" id="three-panels">
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="order-lines">
                    <b>Total Price:</b> {{$total_price}} €
                    <table id="main-table" class="table table-striped"  route="{{route('admin_order_lines_json',[$order_id])}}">
                    <thead>
                        <tr>
                            <th data-width="1px" data-data="product_name"    data-name="product_name">Product</th>
                            <th data-width="1px" data-data="quantity"   data-name="quantity">Quantity</th>
                            <th data-width="1px" data-data="unit_price"   data-name="unit_price">Price</th>
                            <th data-width="1px" data-data="total" data-name="total" >total</th>
                         </tr>
                    </thead>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="products">
                    <table id="main-table-2" class="table table-striped"  route="{{route('admin_final_products_json',[$order_id])}}">
                    <thead>
                        <tr>
                            <th data-width="1px" data-data="id" data-name="id" >Final Product</th>
                            <th data-width="1px" data-data="product_name"    data-name="product_name">Product</th>
                            <th data-orderable="false" data-data="percent"    data-name="percent">  Printed </th>
                         </tr>
                    </thead>
                    </table>
                </div>
                <div role="tabpanel" class="tab-pane" id="parts">
                    <table id="main-table-3" class="table table-striped"  route="{{route('admin_parts_order_json',[$order_id])}}">
                    <thead>
                        <tr>
                            <th data-width="1px" data-data="id_final_product" data-name="id_final_product" >Final Product</th>
                            <th data-width="1px" data-data="product_name" data-name="product_name">Product</th>
                            <th data-width="1px" data-data="part_name" data-name="part_name">Part</th>
                            <th data-width="1px" data-data="weight"  data-name="weight">Weight</th>
                            <th data-width="1px" data-data="printer_name" data-name="printer_name">Printer</th>
                            <th data-width="1px" data-data="initiated" data-name="initiated">Initiated</th>
                            <th data-width="1px" data-data="start_datetime" data-name="start_datetime">Start</th>
                            <th data-width="1px" data-data="end_datetime" data-name="end_datetime">End</th>
                         </tr>
                    </thead>
                    </table>

                </div>
            </div>
        </div>
    </div>
@endsection

