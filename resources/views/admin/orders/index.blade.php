@php
    $route = 'admin_orders';
    $filters_order_route = '';
@endphp

@canatleast(['edit.orders'])
    @php
        $filters_order_route = 'orders_reorder';
    @endphp
@endcanatleast

@extends('admin.layout.table', [
    'route' => $route,
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">Parts Order List</h3>
        
        @canatleast(['edit.orders'])
            @include('admin.partials.create_button', ['route' => $route .'_create','language'=>'en'])
            @include('admin.partials.modal_change_roll', ['route' => $route .'_modal_change_roll','language'=>'en'])
            @include('admin.partials.modal_change_available', ['route' => $route .'_modal_change_avialable','language'=>'en'])
            <!-- @include('admin.partials.modal_api_create', ['route' => $route .'_modal_api_create'])-->
        @endcanatleast
        
        <div class="table-responsive m-t-10">
            
            @include('admin.partials.filters', [
                'order_route' => $filters_order_route,
                'order_visible' => ($filters_order_route ? 'visible' : ''),
                'filter_placeholder' => 'Filtrar por Categoría',
            ])
            
            <table id="main-table" class="table table-striped"  route="{{route($route.'_json')}}">
                <thead>
                    <tr>
                        <th data-width="1px" data-orderable="false" data-orderable="false" data-defaultContent="
                            <a class='edit-button' route='{{route($route)}}'> 
                                <i class='fa fa-pencil text-inverse m-r-10'></i> 
                            </a>"
                        >edit &nbsp;&nbsp;&nbsp;</th>
                        <th data-width="1px" data-data="id"    data-name="id">id</th>
                        <th data-width="1px" data-data="state.name"   data-name="state.name">state</th>
                        <th data-width="1px" data-data="final_products" data-name="final_products">
                            Products
                        </th>
                        <th data-width="1px"  data-data="parts" data-name="parts">
                            parts
                        </th>
                        <th data-width="1px" data-data="customer.name"   data-name="customer.name">customer</th>
                        <th data-width="1px" data-data="priority"   data-name="priority">priority</th>
                        
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection
