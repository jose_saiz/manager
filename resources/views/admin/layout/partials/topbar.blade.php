<!-- Top Navigation -->
<nav class="navbar navbar-default navbar-static-top m-b-0">
    <div class="navbar-header">
        <!-- .Logo -->
        <div class="top-left-part"></div>
        <ul class="nav navbar-top-links navbar-left pull-left">
            <li>
                <a href="javascript:void(0)" class="logotext">
                    <!--This is logo text-->
                    <img style="height: 88px;" src="/images/logo-text.png" alt="home" class="light-logo" alt="home" /></a>
            </li>
        </ul>
        <!-- /.Logo -->
        
        <!-- top right panel -->
        <ul class="nav navbar-top-links navbar-right pull-right">
            <!-- .dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <b class="hidden-xs">@if(Auth::guard('web')->user()){{Auth::guard('web')->user()->name}} @endif</b> </a>
                <ul class="dropdown-menu dropdown-user animated flipInY">
                    <li><a href="{{route('admin.logout')}}"><i class="fa fa-power-off"></i> Logout</a>--{$parts_summary['unassigned']}}--</li>
                    <li >
                        <a><i class="fa fa-language"></i> {{__('language')}}: </a>
                        <div style="float:left;margin-top: -8px;margin-left: 41px;">
                            <a href="{{route('admin.language','en')}}">EN</a>-<a href="{{route('admin.language','de')}}">DE</a>-<a href="{{route('admin.language','es')}}">ES</a>
                    	</div>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- top right panel -->
    </div>
</nav>
<!-- End Top Navigation -->