
<nav class="navbar navbar-default " role="navigation">
    			<div class="navbar-header">
					<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button> 
					
					<div class="business-logo-align col-md-10" >
						<a href="#">
							<img style="height: 88px;" src="/images/logo-text.png" alt="home" class="light-logo" alt="home" /></a> 
						</a>
					</div> 
					
				
				
				<div class="navbar-collapse collapse">
					<ul class="nav navbar-top-link navbar-right">
						<!-- .dropdown -->
                        <li class="dropdown">
                            <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <b class="hidden-xs">@if(Auth::guard('web')->user()){{Auth::guard('web')->user()->name}} @endif</b> </a>
                            <ul class="dropdown-menu dropdown-user animated flipInY">
                                <li><a href="{{route('admin.logout')}}"><i class="fa fa-power-off"></i> Logout</a></li>
                                <li >
                                    <a><i class="fa fa-language"></i> {{__('language')}}: </a>
                                    <div style="float:left;margin-top: -8px;margin-left: 41px;">
                                        <a href="{{route('admin.language','en')}}">EN</a>-<a href="{{route('admin.language','de')}}">DE</a>-<a href="{{route('admin.language','es')}}">ES</a>
                                	</div>
                                </li>
                            </ul>
                            <!-- /.dropdown-user -->
                        </li>
			            <!-- /.dropdown -->
					</ul>
					
					<div class=" col-xs-9 col-sm-12 col-md-8 pd-t-10" >
						<ul class="nav navbar-nav  navbar-centerr">
							<li><a href="#" class="busi-btn-active">Click One</a></li>
							<li><a href="#" class="busi-btn">Click Two</a></li>
							 <li><a href="#" class="busi-btn">Click Three</a></li>
							<li><a href="#" class="busi-btn">Click Four</a></li>
						</ul>
					</div> 
				</div>
				</div>
			</nav>
            
            Bootstrap 3 column navigation 