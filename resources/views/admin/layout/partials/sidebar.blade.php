<div class="side-mini-panel">
    <ul class="mini-nav">
        <div class="togglediv"><a href="javascript:void(0)" id="togglebtn"><i class="ti-menu"></i></a></div>
        @canatleast(['view.parts'])
        <li>
            <a href="javascript:void(0)">
                <i class="ti-layout-grid2"></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">greeny+ admin</h3>
                <ul class="sidebar-menu">
                    @canatleast(['view.parts','edit.parts'])
                       <li> <a  id="graph-link" href="/admin/parts/state">Current State</a></li>
                    @endcanatleast
                </ul>
            </div>
        </li>
        @endcanatleast
        @canatleast(['view.orders', 'edit.orders'])
        <li>
            <a href="javascript:void(0)" style="font-size:15px;">
                Order
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Greeny Orders</h3>
                <ul class="sidebar-menu">
                    <li> <a href="/admin/orders">List</a></li>
                </ul>
            </div>
        </li>
        @endcanatleast
        @canatleast(['view.parts', 'edit.parts'])
        <li>
            <a href="javascript:void(0)" style="font-size:15px;">
                Parts
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Greeny Parts</h3>
                <ul class="sidebar-menu">
                    <li> <a  id="graph-link" href="/admin/parts/state">Current State</a></li>
                    <li> <a  id="graph-link" href="/admin/parts/graph">Planning Chart</a></li>
                    <li> <a  id="save-assign" href="/admin/parts/save_assign">Save Assign</a></li>
                    <li> <a  id="clear-assign" href="/admin/parts/clear_assign">Clear Assign</a></li>
                </ul>
            </div>
        </li>
        @endcanatleast
        <li>
            <a href="javascript:void(0)" style="font-size:15px;">
                <i class="icon-printer"></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Greeny Printers</h3>
                @canatleast(['view.printers', 'edit.printers', 'view.categorias.printers', 'edit.categorias.printers'])
                <ul class="sidebar-menu">
                    @canatleast(['view.printers', 'edit.printers'])
                        <li> <a href="/admin/printers">List</a></li>
                    @endcanatleast
                    <li> <a href="/admin/parts">List with parts</a></li>
                    @canatleast(['view.categorias.printers', 'edit.categorias.printers'])
                        <li> <a href="/admin/printers/categorias">Categories</a></li>
                    @endcanatleast
                </ul>
                @endcanatleast
            </div>
        </li>
        @canatleast(['view.users', 'edit.users'])
        <li>
            <a href="javascript:void(0)">
                <i class="icon-people"></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Users admin</h3>
                <ul class="sidebar-menu">
                        <li class="menu"><a href="javascript:void(0)">Users Options<i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="sub-menu">
                                    <li> <a href="/admin/users">List</a></li>
                                    <li> <a href="/admin/users/roles">Roles</a></li>
                                    <li> <a href="/admin/users/permissions" style="color:orange;">Permissions</a></li>
                            </ul>
                        </li>
                </ul>
            </div>
        </li>
        @endcanatleast
        @canatleast(['view.configs', 'edit.configs'])
        <li>
            <a href="javascript:void(0)">
                <i data-icon="P" class="linea-icon linea-basic fa-fw"></i>
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Configurations Options</h3>
                <ul class="sidebar-menu">
                    @canatleast(['view.configs', 'edit.configs'])
                        <li class="menu"><a href="javascript:void(0)">Configuration <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="sub-menu">
                                <li> <a href="/admin/workdays">Working days</a></li>
                                <li> <a href="/admin/configs">General</a></li>
                            </ul>
                        </li>
                    @endcanatleast
                </ul>
            </div>
        </li>
        @endcanatleast
        
        
        <li>
            <a href="javascript:void(0)" style="font-size:15px;"><img src="/assets/admin/images/greeny_icon_greenyGarden.svg">
            </a>
            <div class="sidebarmenu">
                <h3 class="menu-title">Products </h3>
                <ul class="sidebar-menu">
                    @canatleast(['view.tests', 'edit.tests', 'view.categorias.tests', 'edit.categorias.tests'])
                        <li class="menu"><a href="javascript:void(0)">Files <i class="fa fa-angle-left pull-right"></i></a>
                            <ul class="sub-menu">
                                @canatleast(['view.tests', 'edit.tests'])
                                    <li> <a href="/admin/tests">List</a></li>
                                @endcanatleast
                                @canatleast(['view.categorias.tests', 'edit.categorias.tests'])
                                    <li> <a href="/admin/tests/categorias">Categories</a></li>
                                @endcanatleast
                            </ul>
                        </li>
                    @endcanatleast

                </ul>
            </div>
        </li>
        
        

        
    </ul>
</div>
