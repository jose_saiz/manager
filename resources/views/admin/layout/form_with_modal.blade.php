{{--
    | Plantilla para generar toda la vista de formulario
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 16/11/2017
    |
    | Para poder extender esta plantilla debemos pasarle obligatoriamente las variables: 
    | 
    | route: que se corresponderá con el prefijo de la ruta que hayamos definido para la sección 
    | en /web/routes
    |
    | name: que se corresponderá con el nombre del objeto devuelto por el controlador. (Por ejemplo, 
    | $faq->nombre)
    |
    | id: que se corresponderá con la id del objeto devuelto por el controlador. (Por ejemplo $faq->id)
    |
    | El div con id 'one-column' sirve para que las llamadas AJAX puedan identificar que es una vista con
    | una columna.
    |
    | El div con id 'one-column-form' sirve para poder renderizar en él las secciones con AJAX
    |----------------------------------------------------------------------------------------------------
    |    
    --}}

@extends('admin.layout.master')

@section('content')

    <div class="{{$modal_route}}-modal">
        @include("admin.".$route.".modal")
    </div>
    <div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalTitle">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="confirmDeleteModalTitle">Confirmación de borrado</h4>
                </div>

                <div class="modal-body">
                    <p>@lang('admin/'.$route.'.modal')</p>
                </div>

                <div class="modal-footer">
                    <div>
                        <form class="admin-delete-form" data-method="delete">
                            <a href=""  token="{{ csrf_token() }}" id="delete-button" class="btn btn-block btn-danger waves-effect waves-light"> 
                                <i class="fa fa-check m-r-5"></i> Sí
                            </a>
                        </form>
                    </div>
                    <div>
                        <a href="/admin/{{$route}}" class="btn btn-block btn-default waves-effect waves-light" data-dismiss="modal"> 
                            <i class="fa fa-times m-r-5">
                        </i> No</a>
                    </div>
                </div>

            </div>
        </div>
    </div>
    @include('admin.partials.breadcrumbs', ['route' => 'admin_'.$route, 'parent_section' => 'admin_'.$route, 'active' => 'element_name', 'name' => $name])
        
    <div class="row" id="one-column">
        <div class="col-md-12" id="one-column-form">
            @yield('form_with_modal')
        </div>
    </div>

@endsection