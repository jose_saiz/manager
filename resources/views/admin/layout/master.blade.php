<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="{{ URL::asset('images/favicon.png') }}">

        <title>greeny+ admin</title>

        {{-- Bootstrap Core CSS --}}
        <link href="/assets/admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/admin/css/bootstrap-extension.min.css" rel="stylesheet">
        <link href="/assets/admin/css/bootstrap-switch.min.css" rel="stylesheet">
        {{-- Animation CSS --}}
        <link href="/assets/admin/css/animate.min.css" rel="stylesheet">
        {{-- Toast CSS --}}
        <link href="/assets/admin/css/jquery.toast.min.css" rel="stylesheet">
        {{-- Custom Select: Select2 --}}
        <link href="/assets/admin/css/custom-select.css" rel="stylesheet">
        {{-- DataTables CSS --}}
        <link href="/assets/admin/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="/assets/admin/css/rowReorder.dataTables.min.css" rel="stylesheet">
        <link href="/assets/admin/css/scroller.dataTables.min.css" rel="stylesheet">
        {{-- Custom CSS --}}
        <link href="/assets/admin/css/style.min.css" rel="stylesheet">
        <link href="/assets/admin/css/custom.css" rel="stylesheet">
        {{-- Color CSS --}}
        <link href="/assets/admin/css/colors/default.min.css" id="theme" rel="stylesheet">
        {{-- Dropify CSS --}}
        <link href="/assets/admin/css/dropify.min.css" id="theme" rel="stylesheet">
        {{--  Date picker plugins css --}}
        <link href="/assets/admin/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" >
        {{-- Daterange picker plugins css --}}
        <link href="/assets/admin/css/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="/assets/admin/css/daterangepicker.css" rel="stylesheet">
        {{-- DaterTime picker plugins css --}}
        <link href="/assets/admin/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
         {{-- Time picker plugins css --}}
        <link href="/assets/admin/css/bootstrap-timepicker.min.css" rel="stylesheet">
        {{-- jQuery --}}
        <script src="/assets/admin/js/jquery-3.2.1.min.js"></script>
        {{-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --}}
        {{-- WARNING: Respond.js doesn't work if you view the page via file:// --}}
        {{--<[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <[endif]>--}}
        
        <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css">
        
        <link href="/assets/admin/css/jquery.floatingscroll.css" rel="stylesheet">
        <script src="/assets/admin/js/jquery.floatingscroll.js"></script>
        
        <link rel="stylesheet" href="/assets/admin/css/jquery-confirm.min.css">
		<script src="/assets/admin/js/jquery-confirm.min.js"></script>
        
		<link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">
    </head>
    <body class="fix-sidebar rmv-right-panel">
        {{-- Preloader 
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        --}}
        <div class="preloader">
            <div style="background-image:url(/assets/admin/images/greeny_logo_just_the_sheet.png);
                background-repeat: no-repeat;
                height: 312px;
                margin-top:14%;
                margin-left: 32%;">
            </div>
            <div style="margin-top:-288px; margin-left: calc(32% + 95px);" class="nb-spinner"></div>
        </div>
        
        
        <div id="wrapper ">
            @include('admin.layout.partials.topbar_test')
            @include('admin.layout.partials.sidebar')
            {{-- Page Content --}}
            <div id="page-wrapper" >
                <div class="container-fluid master-layout">
                    @yield('content')
                </div>
                {{-- /.container-fluid --}}
                @include('admin.layout.partials.footer')
            </div>
            {{-- /#page-wrapper --}}
            @include('admin.layout.partials.right')
        </div>
        {{--  /#wrapper --}}
        
        {{--  Popper JavaScript --}}
        <script src="/assets/admin/js/popper-1.12.6.min.js"></script>
        {{--  Bootstrap Core JavaScript --}}
        <script src="/assets/admin/js/bootstrap.min.js"></script>
        <script src="/assets/admin/js/bootstrap-extension.min.js"></script>
        <script src="/assets/admin/js/bootstrap-switch.min.js"></script>
        {{-- Slimscroll JavaScript --}}
        <script src="/assets/admin/js/jquery.slimscroll.min.js"></script>
        {{--  Wave Effects --}}
        <script src="/assets/admin/js/waves.min.js"></script>
        {{--  jQuery Peity --}}
        <script src="/assets/admin/js/jquery.peity.min.js"></script>
        <script src="/assets/admin/js/jquery.peity.init.js"></script>
        {{--  Toast JavaScript --}}
        <script src="/assets/admin/js/jquery.toast.min.js"></script>
        {{-- CKEditor & CKFinder --}}
        <script src="/assets/admin/js/ckeditor/ckeditor.js"></script>
        <script src="/assets/admin/js/ckeditor/adapters/jquery.js"></script>
        {{-- Custom Select: Select2 --}}
        <script src="/assets/admin/js/custom-select.min.js"></script>
        {{-- DataTables JavaScript --}}
        <script src="/assets/admin/js/jquery.dataTables.min.js"></script>
        <script src="/assets/admin/js/jquery.dataTables.rowReorder.min.js"></script>
        <script src="/assets/admin/js/jquery.dataTables.scroller.min.js"></script>
        {{-- TableExport JavaScript --}}
        <script src="/assets/admin/js/tableExport.min.js"></script>
        <script src="/assets/admin/js/jquery.table2excel.min.js"></script>
        {{-- Dropify JavaScript--}}
        <script src="/assets/admin/js/dropify.min.js"></script>
        {{-- jQuery Validate --}}
        <script src="/assets/admin/js/jquery.validate.js"></script>
        <script src="/assets/admin/js/validate-additional-methods.js"></script>
        {{--jQuery Autocomplete --}}
        <script src="/assets/admin/js/jquery.easy-autocomplete.min.js"></script>
        {{-- jQuery Nestable --}}
        <script src="/assets/admin/js/jquery.nestable.js"></script>

        {{-- Moment Plugin JavaScript --}}
        <script src="/assets/admin/js/moment.js">         </script>
        {{-- Date Picker Plugin JavaScript --}}
        <script src="/assets/admin/js/bootstrap-datepicker.min.js"></script>
        {{-- Date range Plugin JavaScript --}}
        <script src="/assets/admin/js/bootstrap-timepicker.min.js"></script>
        <script src="/assets/admin/js/daterangepicker.js"></script>
        {{-- Date Time Plugin JavaScript --}}
        <script src="/assets/admin/js/bootstrap-datetimepicker.min.js"></script>
        {{--  Custom JavaScript --}}
        <script>var languages = {!! json_encode($languages) !!}</script>
        <script src="/assets/admin/js/custom.js"></script>
        <script src="/assets/admin/js/saizfact.validations.js"></script>
        <script src="/assets/admin/js/saizfact-datatables.js"></script>
        {{-- <script src="/assets/admin/js/saizfact.min.js"></script>--}}
        <script src="/assets/admin/js/ed.jquery.extensions.js"></script>
        <script src="/assets/admin/js/saizfact.js"></script>
        @if(strstr(Request::url(), '/productos_extendido'))
            <script src="/assets/admin/js/pages/productos_extendido_opinion.js"></script>
            <script src="/assets/admin/js/pages/productos_extendido_faq.js"></script>
        @endif
        @if(strstr(Request::url(), '/vademecum/productos'))
            <script src="/assets/admin/js/pages/vademecum_producto_opinion.js"></script>
            <script src="/assets/admin/js/pages/vademecum_producto_faq.js"></script>
        @endif
        @if(strstr(Request::url(), '/cursos'))
            <script src="/assets/admin/js/pages/cursos_temario.js"></script>
        @endif
        <script type="text/javascript">
        </script>
        <script src="/assets/admin/js/axios.min.js"></script>
        {{-- <script src="/assets/admin/js/bootstrap.bundle.min.js"></script> --}}
        
    </body>
</html>
