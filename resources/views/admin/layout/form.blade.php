{{--
    | Plantilla para generar toda la vista de formulario
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 16/11/2017
    |
    | Para poder extender esta plantilla debemos pasarle obligatoriamente las variables: 
    | 
    | route: que se corresponderá con el prefijo de la ruta que hayamos definido para la sección 
    | en /web/routes
    |
    | name: que se corresponderá con el nombre del objeto devuelto por el controlador. (Por ejemplo, 
    | $faq->nombre)
    |
    | id: que se corresponderá con la id del objeto devuelto por el controlador. (Por ejemplo $faq->id)
    |
    | El div con id 'one-column' sirve para que las llamadas AJAX puedan identificar que es una vista con
    | una columna.
    |
    | El div con id 'one-column-form' sirve para poder renderizar en él las secciones con AJAX
    |----------------------------------------------------------------------------------------------------
    |    
    --}}

@extends('admin.layout.master')

@section('content')

    @include('admin.partials.breadcrumbs', ['route' => $route, 'parent_section' => $route, 'active' => 'element_name', 'name' => $name])
    <div class="row" id="one-column">
        <div class="col-md-12" id="one-column-form">
            @yield('form')
        </div>
    </div>

@endsection