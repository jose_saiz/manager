<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>greeny+ admin</title>

        {{-- Bootstrap Core CSS --}}
        <link href="/assets/admin/css/bootstrap.min.css" rel="stylesheet">
        <link href="/assets/admin/css/bootstrap-extension.min.css" rel="stylesheet">
        <link href="/assets/admin/css/bootstrap-switch.min.css" rel="stylesheet">
        {{-- Animation CSS --}}
        <link href="/assets/admin/css/animate.min.css" rel="stylesheet">
        {{-- Toast CSS --}}
        <link href="/assets/admin/css/jquery.toast.min.css" rel="stylesheet">
        {{-- Custom Select: Select2 --}}
        <link href="/assets/admin/css/custom-select.css')}}" rel="stylesheet">
        {{-- DataTables CSS --}}
        <link href="/assets/admin/css/jquery.dataTables.min.css" rel="stylesheet">
        <link href="/assets/admin/css/rowReorder.dataTables.min.css" rel="stylesheet">
        <link href="/assets/admin/css/scroller.dataTables.min.css" rel="stylesheet">
        {{-- Custom CSS --}}
        <link href="/assets/admin/css/style.min.css" rel="stylesheet">
        {{-- Color CSS --}}
        <link href="/assets/admin/css/colors/default.min.css" id="theme" rel="stylesheet">
        {{-- Dropify CSS --}}
        <link href="/assets/admin/css/dropify.min.css" id="theme" rel="stylesheet">
        {{--  Date picker plugins css --}}
        <link href="/assets/admin/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" >
        {{-- Daterange picker plugins css --}}
        <link href="/assets/admin/css/bootstrap-timepicker.min.css" rel="stylesheet">
        <link href="/assets/admin/css/daterangepicker.css" rel="stylesheet">
        {{-- jQuery --}}
        <script src="/assets/admin/js/jquery-3.2.1.min.js"></script>
        {{-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --}}
        {{-- WARNING: Respond.js doesn't work if you view the page via file:// --}}
        {{--<[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <[endif]>--}}
    </head>
    <body class="fix-sidebar rmv-right-panel">
        {{-- Preloader --}}
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
            @include('admin.layout.partials.topbar')
            {{-- Page Content --}}
        	<div style = "margin-left: 0px!important;height: 100vh;" id="page-wrapper">
                <div class="container-fluid master-layout">
                    @yield('content')
                </div>
                {{-- /.container-fluid --}}
                @include('admin.layout.partials.footer')
            </div>
            {{-- /#page-wrapper --}}
            @include('admin.layout.partials.right')
        </div>
        {{--  /#wrapper --}}
        
        {{--  Bootstrap Core JavaScript --}}
        <script src="/assets/admin/js/popper-1.12.6.min.js"></script>
        <script src="/assets/admin/js/bootstrap.min.js"></script>
        <script src="/assets/admin/js/bootstrap-extension.min.js"></script>
        <script src="/assets/admin/js/bootstrap-switch.min.js"></script>
        {{-- Slimscroll JavaScript --}}
        <script src="/assets/admin/js/jquery.slimscroll.min.js"></script>
        {{--  Wave Effects --}}
        <script src="/assets/admin/js/waves.min.js"></script>
        {{--  jQuery Peity --}}
        <script src="/assets/admin/js/jquery.peity.min.js"></script>
        <script src="/assets/admin/js/jquery.peity.init.js"></script>
        {{--  Toast JavaScript --}}
        <script src="/assets/admin/js/jquery.toast.min.js"></script>
        {{-- CKEditor & CKFinder --}}
        <script src="/assets/admin/js/ckeditor/ckeditor.js"></script>
        <script src="/assets/admin/js/ckeditor/adapters/jquery.js"></script>
        {{-- Custom Select: Select2 --}}
        <script src="/assets/admin/js/custom-select.min.js"></script>
        {{-- DataTables JavaScript --}}
        <script src="/assets/admin/js/jquery.dataTables.min.js"></script>
        <script src="/assets/admin/js/jquery.dataTables.rowReorder.min.js"></script>
        <script src="/assets/admin/js/jquery.dataTables.scroller.min.js"></script>
        {{-- Dropify JavaScript--}}
        <script src="/assets/admin/js/dropify.min.js"></script>
        {{-- jQuery Validate --}}
        <script src="/assets/admin/js/jquery.validate.js"></script>
        {{-- jQuery Nestable --}}
        <script src="/assets/admin/js/jquery.nestable.js"></script>

        {{-- Moment Plugin JavaScript --}}
        <script src="/assets/admin/js/moment.js">         </script>
        {{-- Date Picker Plugin JavaScript --}}
        <script src="/assets/admin/js/bootstrap-datepicker.min.js"></script>
        {{-- Date range Plugin JavaScript --}}
        <script src="/assets/admin/js/bootstrap-timepicker.min.js"></script>
        <script src="/assets/admin/js/daterangepicker.js"></script>


        {{--  Custom JavaScript --}}
        <script>var languages = {!! json_encode($languages) !!}</script>
        <script src="/assets/admin/js/custom.js"></script>
        <script src="/assets/admin/js/saizfact.validations.js"></script>
        <script src="/assets/admin/js/saizfact-datatables.js"></script>
        {{-- <script src="/assets/admin/js/saizfact.min.js"></script>--}}
        <script src="/assets/admin/js/ed.jquery.extensions.js"></script>
        <script src="/assets/admin/js/saizfact.js"></script>
       
        <script type="text/javascript">
        
        $(document).ready(function(){
            //Calculos para inicializar a un mes de la fecha actual del daterange-datepicker
            var f=new Date();
            f.setMonth( f.getMonth() - 1 );
            var anyo = f.getFullYear();
            var mes = f.getMonth();
            var dia = f.getDate();
            var ff=new Date();
            var anyof = ff.getFullYear();
            var mesf = ff.getMonth();
            var diaf = ff.getDate();
            //Innicialización daterange picker
            $('.input-daterange-datepicker').daterangepicker({
                locale: {
                   
                   "format": "DD/MM/YYYY",
                   "separator": " - ",
                   "applyLabel": "Aceptar",
                   "cancelLabel": "Cancelar",
                   "fromLabel": "Desde",
                   "toLabel": "Hasta",
                   "customRangeLabel": "Custom",
                   "daysOfWeek": ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],                   
                   "monthNames": [
                       'Enero','Febrero','Marzo','Abril','Mayo','Junio',
                       'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'
                   ],
                   "firstDay": 1
                },
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-danger',
                cancelClass: 'btn-inverse',
                startDate: f,
                endDate: ff,
            });
        });
        </script>
    </body>
</html>
