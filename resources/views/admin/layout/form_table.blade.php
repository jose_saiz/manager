{{--
    | Plantilla para generar toda la vista de formulario
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 06/05/2021
    |
    | Para poder extender esta plantilla debemos pasarle obligatoriamente las variables: 
    | 
    | route: que se corresponderá con el prefijo de la ruta que hayamos definido para la sección 
    | en /web/routes
    |
    | parent_section: que se corresponderá con el prefijo de la ruta que hayamos definido para la 
    | sección padre en /web/routes (por ejemplo, faqs sería la sección padre de faqs_categorias)
    |
    | name: que se corresponderá con el nombre del objeto devuelto por el controlador. (Por ejemplo, 
    | $faq->nombre)
    |
    | id: que se corresponderá con la id del objeto devuelto por el controlador. (Por ejemplo $faq->id)
    |
    | Los div con id 'two-columns' sirve para que las llamadas AJAX puedan identificar que es una vista con
    | una columna.
    |
    | Los div con id 'two-columns-form' y 'two-columns-table' sirven para poder renderizar en ellos las 
    | secciones con AJAX
    |----------------------------------------------------------------------------------------------------
    |    
    --}}

@extends('admin.layout.master')

@section('content')

    @include('admin.partials.breadcrumbs', ['route' => $route, 'parent_section' => $parent_section, 'active' => 'element_name', 'name' => $name])

    <div class="row" id="two-columns">
        <div class="col-md-4" id="two-columns-form">
            @yield('form')
        </div>
        <div class="col-md-8" id="two-columns-table">
            @yield('table')
        </div>
        
    </div>

@endsection
