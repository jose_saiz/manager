@php
    $route = 'users';
@endphp

@extends('admin.layout.table', [
    'route' => $route,
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">Users List</h3>
        
        @canatleast(['edit.usuarios'])
            @include('admin.partials.create_button', ['route' => $route .'_create'])
        @endcanatleast
        
        <div class="table-responsive m-t-10">
            
            @include('admin.partials.filters')
            
            <table id="main-table" class="table table-striped"  route="{{route('users_json')}}">
                <thead>
                    <tr>
                        <th data-data="id" data-name="id">#</th>
                        <th data-data="name" data-name="name">Nombre</th>
                        <th data-data="email" data-name="email">Email</th>
                        <th data-data="factory.name" data-name="factory.name">Factory</th>
                        <th data-data="roles[, ].name" data-name="roles.name">Roles</th>
                        <th data-data="updated_at" data-name="updated_at">Modificado</th>
                        <th data-orderable="false" data-defaultContent="
                            <a class='edit-button' route='{{route($route)}}'> 
                                <i class='fa fa-pencil text-inverse m-r-10'></i> 
                            </a>">
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection
