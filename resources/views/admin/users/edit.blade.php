{{--
    |
    | *$roles se recoge de /app/Http/ViewComposers/Admin/UsersRoles 20/11/2017_saizfact
    |    
    --}}

@php
    $route = 'users';
@endphp

@extends('admin.layout.form', [
    'route' => 'users', 
    'name' => $user->name, 
    'id' => $user->id
])

@section('form')

    @include('admin.partials.delete_modal', ['route' => $route, 'name' => $user->name])
    
    <div class="white-box">
    
        @include('admin.partials.form_title', ['route' => $route, 'name' => $user->name])
        @canatleast(['edit.usuarios'])
            @include('admin.partials.crud_buttons', ['id' => $user->id])
        @endcanatleast
        @include('admin.partials.errors')
    
        <div>
            <form class='admin-form' id='users-form' action="{{route('users_store')}}">
        
                {{ csrf_field() }}
        
                @isset ($user->id)
                    <input type="hidden" name="id" value="{{$user->id}}">
                @endisset
        
                <div class="form-group row m-t-10">
                    <label for="name" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                        Name *
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="name" value="{{ old('_token') ? old('name') : $user->name }}" class="form-control {{ ($errors->has('name')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
                <div class="form-group row m-t-10">
                    <label for="name" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                        Factory *
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <select id="factory_id" data-placeholder="Select factory" name="factory_id" class="form-control select2 multiple-option">
                            
                            @foreach ($factories as $factory)
                                <option value="{{$factory->id}}" class="{{$factory->active ? '':'desactivado'}}" {{$factory->id == $user->factory_id?'selected':''}}>{{$factory->name}}</option> 
                            @endforeach
                        </select>
                    </div>
                </div>
        
                <div class="form-group row m-t-10">
                    <label for="email" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('email')) ? 'text-danger' : '' }}">
                        Email *
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="email" value="{{ old('_token') ? old('email') : $user->email }}" class="form-control {{ ($errors->has('email')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
        
                <div class="form-group row m-t-10">
                    <label for="password" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('password')) ? 'text-danger' : '' }}">
                        Password *
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="password" name="password" value="" class="form-control {{ ($errors->has('password')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
                
                <div class="form-group row m-t-10">
                    <label for="password_confirmation" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('password_confirmation')) ? 'text-danger' : '' }}">
                        Confirm Password *
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="password" name="password_confirmation" value="" class="form-control {{ ($errors->has('password_confirmation')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
        
                <div class="form-group row">
                    <label for="activo" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('activo')) ? 'text-danger' : '' }}">
                        Active *
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="checkbox" name="activo" value="true" {{ (old('_token') ? old('activo') : $user->activo) ? 'checked' : '' }} class="bt-switch" data-size="small">
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="role" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('activo')) ? 'text-danger' : '' }}">Role</label>
                    <div class="col-lg-9 col-md-8">
                        <select id="role_select" data-placeholder="Select roles" name="role_select" class="form-control select2 multiple-option">
                            <option id="default-option"></option>
                            @foreach ($roles as $role)
                                <option value="{{$role->id}}" class="{{$role->activo ? '':'desactivado'}}">{{$role->name}}</option> 
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="multiple-checkbox-container row">
                    @foreach ($roles as $role)
                        
                        @php $role_assigned = in_array($role->id, $roles_assigned) @endphp
                        
                        <div class="col-lg-6 col-md-6 multiple-checkbox-element {{ $role_assigned ? 'active' : ''}}">
                            <label for="{{$role->id}}">{{$role->name}} <i class="fa fa-trash-o text-inverse m-r-10"></i></label>
                            <input id="{{$role->id}}" class='multiple-checkbox' type="checkbox" name="role_id[]" value="{{$role->id}}"  {{$role_assigned ? 'checked' : ''}}>
                        </div>
                        
                    @endforeach
                </div>
                
            </form>
        </div>
    </div>

@endsection
