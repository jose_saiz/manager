@php
    $route = 'admin_printers';
    $filters_order_route = '';
@endphp

@canatleast(['edit.printers'])
    @php
        $filters_order_route = 'printers_reorder';
    @endphp
@endcanatleast

@extends('admin.layout.table', [
    'route' => $route,
])


@section('table')
    <div class="white-box">
        <h3 class="box-title">Parts List
    	<div class="btn-group pull-right" style="padding-bottom:14px;">
        	<button type="button" class="btn btn-primary btn-lg dropdown-toggle" data-toggle="dropdown" >Export <span class="caret"></span></button>
        	<ul class="dropdown-menu" role="menu">
        		<li><a class="dataExport" data-type="csv">CSV</a></li>
        		<li><a class="dataExport" data-type="excel">XLS</a></li>          
        		<li><a class="dataExport" data-type="txt">TXT</a></li>			 			  
        	</ul>
        </div>
        </h3>
        <div class="table-responsive m-t-10 overflow-panel">
            <table style="table-layout: fixed;" id="data-table-planning" class="table table-striped" style="border: 1px;" route="{{route($route.'_json')}}">
               <thead>
                   <tr>
                   
                           <th style="word-wrap: break-word;width: 66px;border-top: 1px solid #e4e7ea;border-bottom: 1px solid #e4e7ea; border-left: 1px solid #e4e7ea;border-right: 1px solid #e4e7ea;">
                           Printers
                           </th>
                           <th colspan="{{$cont_max}}"  style="word-wrap: break-word;width: {{$cont_max*184}}px;border-top: 1px solid #e4e7ea;border-bottom: 1px solid #e4e7ea; border-left: 1px solid #e4e7ea;border-right: 1px solid #e4e7ea;">
                           Parts
                           </th>
                   </tr>
               </thead>
               <tbody>
                   @foreach ($printerMatrix as $printer)
                   <tr>
                   		@php
            			$cont =0;
        				@endphp
                		@foreach ($printer as $key => $value)
                			@php
            				$cont ++;
            				@endphp
                			@if ($key == 0)
                				<td style="border-top: 1px solid #e4e7ea;border-bottom: 1px solid #e4e7ea; border-left: 1px solid #e4e7ea;border-right: 1px solid #e4e7ea;">
                				{{$value}}
                				</td>
                			@else
                				<td style="border-top: 1px solid #e4e7ea;border-bottom: 1px solid #e4e7ea; border-left: 1px solid #e4e7ea;border-right: 1px solid #e4e7ea;">
                				File: {{$value->file->name}}<br>
                				{{$value->start_datetime}} <br> 
                				{{$value->end_datetime}}
                				</td>
                			@endif
    
                		@endforeach
        				@while ($cont <= $cont_max)
            				<td style="width: 243px;border-top: 1px solid #e4e7ea;border-bottom: 1px solid #e4e7ea; border-left: 1px solid #e4e7ea;border-right: 1px solid #e4e7ea;">
            				</td>
            				@php
            				$cont ++;
            				@endphp
                        @endwhile
                	</tr>
        			@endforeach
        		</tbody>
            </table>
        </div>
    </div>
@endsection
