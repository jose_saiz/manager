@php
    $route = 'admin_printers';
    $filters_order_route = '';
@endphp

@canatleast(['edit.printers'])
    @php
        $filters_order_route = 'printers_reorder';
    @endphp
@endcanatleast

@extends('admin.layout.table', [
    'route' => $route,
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">Printers List</h3>
        
        @canatleast(['edit.printers'])
            @include('admin.partials.create_button', ['route' => $route .'_create','language'=>'en'])
            @include('admin.partials.modal_change_roll', ['route' => $route .'_modal_change_roll','language'=>'en'])
            @include('admin.partials.modal_change_available', ['route' => $route .'_modal_change_avialable','language'=>'en'])
            <!-- @include('admin.partials.modal_api_create', ['route' => $route .'_modal_api_create'])-->
        @endcanatleast
        
        <div class="table-responsive m-t-10">
            
            @include('admin.partials.filters', [
                'order_route' => $filters_order_route,
                'order_visible' => ($filters_order_route ? 'visible' : ''),
                'filter_placeholder' => 'Filtrar por Categoría',
            ])
            
            <table id="main-table" class="table table-striped"  route="{{route($route.'_json')}}">
                <thead>
                    <tr>
                        
                        <th data-width="40px" data-name="available" data-data="available" >
                            available
                        </th>
                        <th data-width="1px" data-orderable="false" data-orderable="false" data-defaultContent="
                            <a class='edit-button' route='{{route($route)}}'> 
                                <i class='fa fa-pencil text-inverse m-r-10'></i> 
                            </a>"
                        >edit &nbsp;&nbsp;&nbsp;</th>
                        <th data-width="1px"  data-data="roll_weight" data-name="roll_weight">
                            change roll
                        </th>
                        <th data-width="1px" data-data="code" data-name="code">printer code</th>
                        <th data-data="weight" data-name="weight" data-width="1px"  data-defaultContent="" data-orderable="false"  data-searchable="false">weight Kg</th>
                        <th data-data="roll_replacement_datetime" data-name="roll_replacement_datetime">roll_replacement</th>
                        <th data-data="roll_end_datetime" data-name="roll_end_datetime">roll_end</th>
                        <th data-data="category.name" data-name="categoria.nombre" id="filter-column" data-searchable="false">type</th>
                       	<th data-data="id" data-name="id" data-visible="true">id</th>
                        <th data-data="updated_at" data-name="updated_at">updated</th>
                        <th data-data="created_at" data-name="created_at">created</th>
                        
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection
