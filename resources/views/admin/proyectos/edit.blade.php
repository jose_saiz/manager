{{--
    |
    | *$categorias se recoge de /app/Http/ViewComposers/Admin/ProyectosCategorias 08/11/2017_saizfact
    |    
    --}}

@php
    $route = 'admin_proyectos';
@endphp

@extends('admin.layout.form_with_two_modals', [
    'route' => 'proyectos', 
    'name' => $proyecto->nombre, 
    'id' => $proyecto->id,
    'modal_route' => 'media',
    
])

@section('form_with_two_modals')
    <div class="modal fade" id="delete-media" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalTitle">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
    
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="confirmDeleteModalTitle">Confirmación de borrado</h4>
                </div>
    
                <div class="modal-body">
                    <p>¿Eliminar archivo media?</p>
                </div>
    
                <div class="modal-footer">
                    <div>
                        <form class="admin-delete-form" data-method="delete">
                            <a href=""  token="{{ csrf_token() }}" class="delete-media btn btn-block btn-danger waves-effect waves-light"> 
                                <i class="fa fa-check m-r-5"></i> Sí
                            </a>
                        </form>
                    </div>
                    <div>
                        <a href="/admin/" class="btn btn-block btn-default waves-effect waves-light" data-dismiss="modal"> 
                            <i class="fa fa-times m-r-5">
                        </i>No</a>
                    </div>
                </div>
    
            </div>
        </div>
    </div>

    @include('admin.partials.delete_modal', ['route' => $route, 'name' => $proyecto->nombre])
    
    <div class="white-box">
    
        @include('admin.partials.form_title', ['route' => $route, 'name' => $proyecto->nombre])
        @canatleast(['edit.proyectos'])
            @include('admin.partials.crud_buttons', ['id' => $proyecto->id])
        @endcanatleast
        @include('admin.partials.errors')
    
        <div>
            <form class='admin-form' id='proyectos-form' action="{{route($route . '_store')}}">
        
                {{ csrf_field() }}
                 @isset ($proyecto->id)
                    <input type="hidden" id ="id" name="id" value="{{$proyecto->id}}">
                 @endisset
                
                <ul class="nav nav-tabs" role="tablist">
        
                    <li role="presentation" class="nav-item">
                        <a href="#contenido" class="nav-link active" aria-controls="contenido" role="tab" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <span class="hidden-xs">Contenido</span>
                        </a>
                    </li>
        
                    <li role="presentation" class="nav-item">
                        <a href="#seo" class="nav-link" aria-controls="seo" role="tab" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">
                                <i class="fa fa-tag"></i>
                            </span>
                            <span class="hidden-xs">SEO</span>
                        </a>
                    </li>
        
                </ul>
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="contenido">       
                        <div class="form-group row m-t-10">
                            <label for="nombre" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">
                                Nombre *
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <input type="text" name="nombre" value="{{ old('_token') ? old('nombre') : $proyecto->nombre }}" class="form-control {{ ($errors->has('nombre')) ? 'is-invalid' : '' }}">
                            </div>
                        </div>
                
                        <div class="form-group row m-t-10">
                            <label for="categoria_id" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('categoria_id')) ? 'text-danger' : '' }}">
                                Categoría *
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <select name="categoria_id" data-placeholder="Seleccione una categoría" class="form-control select2 {{ ($errors->has('categoria_id')) ? 'is-invalid' : '' }}">
                                    <option></option>
                                    @foreach($categorias as $categoria)
                                        <option value="{{$categoria->id}}" {{$proyecto->categoria_id == $categoria->id ? 'selected':''}} class="{{$categoria->activo ? '':'desactivado'}}">{{ $categoria->nombre }}</option>
                                    @endforeach
                                </select>                   
                            </div>
                        </div>
                
                        <div class="form-group row">
                            <label for="activo" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('activo')) ? 'text-danger' : '' }}">
                                Activo *
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <input type="checkbox" name="activo" value="true" {{ (old('_token') ? old('activo') : $proyecto->activo) ? 'checked' : '' }} class="bt-switch" data-size="small">
                            </div>
                        </div>
                
                        <div class="form-group row">
                            <label for="orden" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('orden')) ? 'text-danger' : '' }}">
                                Orden *
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <input type="text" name="orden" value="{{ old('_token') ? old('orden') : $proyecto->orden }}" class="form-control {{ ($errors->has('orden')) ? 'is-invalid' : '' }}">
                            </div>
                        </div>
                
                        <div class="form-group row m-t-10">
                            <label for="referencia" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('referencia')) ? 'text-danger' : '' }}">
                                Referencia 
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <input type="text" name="referencia" value="{{ old('_token') ? old('referencia') : $proyecto->referencia }}" class="form-control {{ ($errors->has('referencia')) ? 'is-invalid' : '' }}">
                            </div>
                        </div>
                
                        <div class="form-group row m-t-10">
                            <label for="sector" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('sector')) ? 'text-danger' : '' }}">
                                Sector 
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <input type="text" name="sector" value="{{ old('_token') ? old('sector') : $proyecto->sector }}" class="form-control {{ ($errors->has('sector')) ? 'is-invalid' : '' }}">
                            </div>
                        </div>
                
                        <div class="form-group row m-t-10">
                            <label for="subsector" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('subsector')) ? 'text-danger' : '' }}">
                                Subsubsector 
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <input type="text" name="subsector" value="{{ old('_token') ? old('subsector') : $proyecto->subsector }}" class="form-control {{ ($errors->has('subsector')) ? 'is-invalid' : '' }}">
                            </div>
                        </div>
                
                
                
                
                        @component('admin.partials.locale', ['tab' => 'contenido'])
                
                        @foreach ($idiomas as $idioma)
                
                        <div role="tabpanel" class="tab-pane {{ $loop->first ? ' active' : '' }}" id="contenido-{{$idioma->alias}}">
                            <div class="form-group row m-t-10">
                                <label for="titulo" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">
                                    Título *
                                </label>
                                <div class="col-lg-9 col-md-8">
                                    <input type="text" name="locale[titulo.{{$idioma->alias}}]" value='{{isset($locale["titulo.$idioma->alias"])?$locale["titulo.$idioma->alias"]:"" }}' class="form-control">
                                </div>
                            </div>
                
                            <div class="form-group row m-t-10">
                                <label for="descripcion-corta" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">
                                    Descripción Corta*
                                </label>
                                <div class="col-lg-12 col-md-12">
                                    <textarea name="locale[descripcion_corta.{{$idioma->alias}}]" class="rich" id="ckeditor.{{$idioma->alias}}" class="form-control">
                                        @if (isset($locale["descripcion.corta.$idioma->alias"]))
                                        {{$locale["descripcion.corta.$idioma->alias"]}}
                                        @endif
                                    </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="descripcion-larga" class="col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">
                                        Descripción larga 
                                    </label>
                                    <textarea name="locale[descripcion.larga.{{$idioma->alias}}]" class="rich" id="descripcion.larga.{{$idioma->alias}}" class="form-control">
                                        @if (isset($locale["descripcion.corta.$idioma->alias"]))
                                        {{$locale["descripcion.larga.$idioma->alias"]}}
                                        @endif
                                    </textarea>
                                </div>
                            </div>
                            
                        @isset ($proyecto->id)
                            <div class="form-group">
                                <label class="col-form-label {{ ($errors->has('imagenes')) ? 'text-danger' : '' }}">
                                    Documentación
                                </label>
                            
                                <div class="col-lg-4 col-sm-4 col-xs-12 p-5">
                                    <a id="new-doc" data-idioma="{{$idioma->alias}}" data-proyecto-id="{{$proyecto->id}}" class="btn btn-block btn-default waves-effect waves-light"> 
                                        <i class="fa fa-plus-circle m-r-5"></i> 
                                        Nuevo
                                    </a>
                                </div>
                                 
                                <div class="row">
                                    @if(!empty($docs))
                                        @foreach ($docs as $item)
                                            @if($item->anchor == $idioma->alias)
                                                <div class="col-md-3 col-lg-3 single-media">
                                                
                                                    <div class="media-input" id="{{$item->entity_id}}" route="{{route('media')}}" style="display: flex;justify-content: center;align-content: center;flex-direction: column;text-align: center;">
                                                        <i class="fa fa-file-pdf-o" style="font-size:60px">
                                                        
                                                        </i>
                                                        {{--
                                                        <a href="" class="media-item">
                                                            <img src="{{isset($item->filename)?asset('storage/media/fdb/'. $item->filename):'/imagenes_productos/sin_foto.jpg'}}" alt="" class="media-image" />
                                                            
                                                        </a>
                                                        --}}
                                                    </div>
                                                    <div class="media-title" id="{{$item->entity_id}}" route="{{route('media')}}">   
                                                        <a href="" class="doc-item" >
                                                        <p>
                                                            @if (isset($item->filename))
                                                                {{$item->filename}}<br>
                                                           
                                                            @endif
                                                        </p>
                                                        </a>
                                                        
                                                    </div>
                                                    <a href="#" class="media-close-button" id="close-{{$item->entity_id}}" data-id="{{$item->entity_id}}"  data-toggle="modal" data-target="#delete-media"></a>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>

                            <div class="clearfix"></div>
                            <br/>
                            
                            <div class="form-group">
                                <label class="col-form-label {{ ($errors->has('imagenes')) ? 'text-danger' : '' }}">
                                    Galería de imagenes
                                </label>
                            
                                <div class="col-lg-4 col-sm-4 col-xs-12 p-5">
                                    <a id="new-media" data-idioma="{{$idioma->alias}}" data-proyecto-id="{{$proyecto->id}}" class="btn btn-block btn-default waves-effect waves-light"> 
                                        <i class="fa fa-plus-circle m-r-5"></i> 
                                        Nuevo
                                    </a>
                                </div>
                                 
                                <div class="row">
                                    @if(!empty($images))
                                        @foreach ($images as $item)
                                            @if ($item->rel_profile == $idioma->alias)
                                                <div class="col-md-3 col-lg-3 single-media">
                                                    <div class="media-input" id="{{$item->entity_id}}" route="{{route('media')}}">
                                                        <a href="" class="media-item">
                                                            <img src="{{isset($item->path)?asset('storage/media/idb/'. $item->path):'/imagenes_productos/sin_foto.jpg'}}" alt="" class="media-image" />
                                                        </a>
                                                    </div>
                                                    <div class="media-title" id="{{$item->entity_id}}" route="{{route('media')}}">   
                                                        <a href="" class="media-item" >
                                                        <p>
                                                            @if (isset($item->filename))
                                                                {{$item->filename}}<br>
                                                           
                                                            @endif
                                                        </p>
                                                        </a>
                                                        
                                                    </div>
                                                    <a href="#" class="media-close-button" id="close-{{$item->entity_id}}" data-id="{{$item->entity_id}}"  data-toggle="modal" data-target="#delete-media"></a>
                                                </div>
                                            @endif
                                        @endforeach
                                    @endif
                                </div>
                            </div>
                        @endif
                            
                        </div>
                        @endforeach
                        @endcomponent
                    </div>
                    <div role="tabpanel" class="tab-pane" id="seo">
                        @component('admin.partials.locale', ['tab' => 'seo'])
                        @foreach ($idiomas as $idioma)
                        <div role="tabpanel" class="tab-pane {{ $loop->first ? ' active' : '' }}" id="seo-{{$idioma->alias}}">
                            <div class="form-group row m-t-10">
                                <label for="name" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                                    Descripción *
                                </label>
                                <div class="col-lg-9 col-md-8">
                                    <input type="text" name="seo[description.{{$idioma->alias}}]" value='{{$seo["description.$idioma->alias"] }}' class="form-control">
                                </div>
                            </div>
        
                            <div class="form-group row m-t-10">
                                <label for="slug" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('slug')) ? 'text-danger' : '' }}">
                                    Keywords *
                                </label>
                                <div class="col-lg-9 col-md-8">
                                    <input type="text" name="seo[keywords.{{$idioma->alias}}]" value='{{$seo["keywords.$idioma->alias"] }}' class="form-control">
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endcomponent
                    </div>
                </div>
            </form>
       </div>
    </div>

@endsection
