@php
    $route = 'admin_proyectos';
    $filters_order_route = '';
@endphp

@canatleast(['edit.proyectos'])
    @php
        $filters_order_route = 'proyectos_reorder';
    @endphp
@endcanatleast

@extends('admin.layout.table', [
    'route' => $route,
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">Listado</h3>
        
        @canatleast(['edit.proyectos'])
            @include('admin.partials.create_button', ['route' => $route .'_create'])
        @endcanatleast
        
        <div class="table-responsive m-t-10">
            
            @include('admin.partials.filters', [
                'order_route' => $filters_order_route,
                'order_visible' => ($filters_order_route ? 'visible' : ''),
                'filter' => $categorias,
                'filter_placeholder' => 'Filtrar por Categoría',
            ])

            <table id="main-table" class="table table-striped"  route="{{route($route.'_json')}}">
                <thead>
                    <tr>
                        <th data-data="id" data-name="id">#</th>
                        <th data-data="orden" data-name="orden">Orden</th>
                        <th data-data="nombre" data-name="nombre">Nombre</th>
                        <th data-data="categoria.nombre" data-name="categoria.nombre" id="filter-column">Categoría</th>
                        <th data-data="updated_at" data-name="updated_at">Modificado</th>
                        <th data-orderable="false" data-defaultContent="
                            <a class='edit-button' route='{{route($route)}}'> 
                                <i class='fa fa-pencil text-inverse m-r-10'></i> 
                            </a>">
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection
