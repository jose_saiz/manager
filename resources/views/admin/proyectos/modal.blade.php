<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" id="new-media-modal">
    <div class="modal-dialog media-modal">
        <div class="modal-content">

            <div class="modal-header">
                <a type="button" id="close-modal-button" class="close media-modal" data-type="media" data-dismiss="modal" aria-hidden="true">×</a>

                @if(isset($media->nombre))
                    <h2 id="modal-title" class="media">Editando Imagen {{$media->nombre}}<h2>
                @else
                    <h2 id="modal-title" class="media">Nueva Imagen<h2>
                @endif

                <div class="error-container alert alert-danger alert-dismissable {{ $errors->any() ? 'active' : '' }}">
                    <ul class="errors">
                        @foreach ($errors->all() as  $error)
                            <li class="error">{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="modal-body">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            
                            <form class='modal-form' id='media-item-form' action="{{route('media_store')}}"  enctype="multipart/form-data">
                                
                                <div class="col-lg-6 col-md-6">
                                    {{ csrf_field() }}

                                    <input type="hidden" name="rel_profile" id="media-rel-profile" value="{{isset($media->rel_profile) ? $media->rel_profile : '' }}">
                                    <input type="hidden" name="proyecto_id" id="proyecto-id" value="{{isset($media->proyecto_id) ? $media->proyecto_id : '' }}">
                                    @isset($media->id)
                                        <input type="hidden" id="media-id" name="id" value="{{$media->id}}">
                                    @endisset
                                    
                                    <div class="col-lg-12 col-md-12">
                                        <div class="form-group row">
                                            <label for="input_file" class="row col-lg-12 col-md-12 col-form-label {{ ($errors->has('input_file')) ? 'text-danger' : '' }}">
                                                Archivo *
                                            </label>
                                            <input type="file" 
                                                name="input_file"   
                                                id="input_file" 
                                                class="dropify media" 
                                                data-height="335"
                                                data-show-remove="false" 
                                                data-max-file-size="8M"
                                                data-default-file="{{isset($path) ? asset('storage/media/idb/'. $path) : ''}}"
                                                value="{{isset($path) ? asset('storage/media/idb/'. $path) : ''}}"
                                                {{-- esto funciona pero conservaremos el validate jquery: data-allowed-file-extensions="png jpg jpeg"--}}
                                                {{--accept="image/*"--}}
                                            />
                                        </div>
                                    </div>
                                </div>

                                <div class="col-lg-6 col-md-6">
                                    <div class="form-group row">
                                        <label for="nombre" class="col-lg-12 col-md-12 col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">
                                            Nombre *
                                        </label>
                                        <div class="col-lg-12 col-md-12">
                                            <input id="media-name" type="text" name="nombre" value="{{isset($media->nombre)?$media->nombre:''}}" class="form-control {{ ($errors->has('nombre')) ? 'is-invalid' : '' }}">
                                        </div>
                                        <div class="col-lg-9 col-md-8">
                                           @php
                                           $tipo_id_selected = 0 ;
                                           if (isset($media)){
                                                $tipo_id_selected = $media->tipo_id;
                                           }
                                           @endphp
                                           <label for="tipo_id" class="col-lg-12 col-md-12 col-form-label {{ ($errors->has('tipo_id')) ? 'text-danger' : '' }}">
                                                Seleccione en que página se va mostrar la imagen *
                                           </label>
                                           <select id = "tipo_id" name="tipo_id" data-placeholder="Seleccione el tipo de imagen" class="form-control primary-select-related {{ ($errors->has('tipo_id')) ? 'is-invalid' : '' }}">
                                                <option {{$tipo_id_selected== 1 ? 'selected':''}} value="1" >Slide</option>
                                                <option {{$tipo_id_selected== 2 ? 'selected':''}} value="2" >Parrilla</option>
                                                <option {{$tipo_id_selected== 3 ? 'selected':''}} value="3" >Galería</option>
                                            </select>                   
                                        </div>
                                        <label for="orden" class="col-lg-12 col-md-12 col-form-label {{ ($errors->has('orden')) ? 'text-danger' : '' }}">
                                            Orden 
                                        </label>
                                        <div class="col-lg-12 col-md-12">
                                            <input id="orden" type="text" name="orden" value="{{isset($media->orden)?$media->orden:''}}" class="form-control {{ ($errors->has('orden')) ? 'is-invalid' : '' }}">
                                        </div>
                                        <div class="col-lg-12 col-sm-12 col-xs-12 p-5">
                                            <button  data-type="media" class="modal-store btn btn-block btn-default waves-effect waves-light"> 
                                                Guardar
                                            </button>
                                        </div>
                                        
                                    </div>
                                    
                                    <div class="col-lg-12 col-md-12">
                                        @if(isset($content_type))
                                            @if(strpos($content_type, 'image') !== false && isset($path))
                                                <a href="{{asset('storage/media/image/'. $path)}}" target="_blank" id="media-link">{{asset('storage/media/image/'. $path)}}</a>
                                            @elseif(strpos($content_type, 'document') !== false && isset($path))
                                                <a href="{{asset('storage/media/documents/'. $path)}}" target="_blank" id="media-link">{{asset('storage/media/documents/'. $path)}}</a>
                                            @endif
                                        @endif
                                    </div>

                                </div>
                    
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>