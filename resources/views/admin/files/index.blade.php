@php
    $route = 'admin_files';
    $filters_order_route = '';
@endphp

@canatleast(['edit.files'])
    @php
        $filters_order_route = 'files_reorder';
    @endphp
@endcanatleast

@extends('admin.layout.table', [
    'route' => $route,
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">File List</h3>
        
        @canatleast(['edit.files'])
            @include('admin.partials.create_button', ['route' => $route .'_create','language'=>'en'])
            @include('admin.partials.modal_change_roll', ['route' => $route .'_modal_change_roll','language'=>'en'])
            @include('admin.partials.modal_change_available', ['route' => $route .'_modal_change_avialable','language'=>'en'])
            <!-- @include('admin.partials.modal_api_create', ['route' => $route .'_modal_api_create'])-->
        @endcanatleast
        
        <div class="table-responsive m-t-10">
            
            @include('admin.partials.filters', [
                'order_route' => $filters_order_route,
                'order_visible' => ($filters_order_route ? 'visible' : ''),
                'filter_placeholder' => 'Filtrar por Categoría',
            ])
            
            <table id="main-table" class="table table-striped"  ">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>fileHash</th>
                        <th>partNo</th>
                        <th>modelVersion</th>
                        <th>slicedVersion</th>
                        <th>filament_used</th>
                        <th>estimated_printing_time</th>
                        <th>fileTime</th>
                        <th>fileSize</th>
                        <th>files</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($result_table as $file)
                    <tr>
                        <td>{{$file['id']}}</td>
                        <td>{{$file['fileHash']}}</td>
                        <td>{{$file['partNo']}}</td>
                        <td>{{$file['modelVersion']}}</td>
                        <td>{{$file['slicedVersion']}}</td>
                        <td>{{$file['filament_used']}}</td>
                        <td>{{$file['estimated_printing_time']}}</td>
                        <td>{{$file['fileTime']}}</td>
                        <td >{{$file['fileSize']}}</td>
                        <td>files</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection
