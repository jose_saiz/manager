{{--
    | Plantilla para el componente filters
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 21/12/2017
    |
    | Este archivo recibe las variables a través de las plantillas index, y aporta funciones de
    | filtraje a las tablas que usan Datatables y están definidas en /js/saizfact.datatables.js. 
    | Por defecto mostrará siempre el buscador general. Parámetros opcionales que acepta:
    |
    | $new [string]: si es 'true' aparecerá el filtro de novedades. Debe existir un campo 'novedad' en la
    | tabla para funcionar. 
    |
    | $order_route [string]: la ruta a la acción reorderTable en el controlador (por ejemplo, vademecum_reorder).
    | Hace disponible el botón de ordenación, debe existir el campo 'orden' en la tabla para usarlo y ser
    | agregada la columna (th) orden en la tabla. También se puede usar exclusivamente para las novedades, 
    | para ello deberá existir el campo 'orden_novedad' en la tabla y ser agregada como columna en la tabla. 
    | Ver ejemplo en /admin/vademecum/index.
    |
    | $order_visible [string]: si es 'true' hace que el botón de ordenación esté visible en todo momento.
    |
    | $subfilter [collection]: Filtro secundario que depende de $filter haciendo uso de select_related 
    | (/js/saizfact.js), al elegir un elemento del subfiltro se actualizan las opciones de $filter.Si 
    | usamos esta opción debemos etiquetar la columna que será filtrada con la id subfilter-column.
    |
    | $subfilter_placeholder [string]: Mensaje para el placeholder de $subfilter.
    |
    | $filter [collection]: Filtro primario, se puede utilizar sin necesidad de utilizar $subfilter. Si 
    | usamos esta opción debemos etiquetar la columna que será filtrada con la id filter-column.
    |
    | $filter_placeholder [string]: Mensaje para el placeholder de $subfilter.
    | ----------------------------------------------------------------------------------------------------
    |    
    --}}

<div class="filters-container col-lg-12 col-md-12">

    <div class="col-lg-3 col-md-3 filter-buttons">
        @isset($new)
            <div class="col-lg-2 col-md-2">
                <i class="fa fa-star fa-2x new-button" aria-hidden="true"></i>
            </div>
        @endisset

        @if (isset($order_route) && $order_route)
            <div class="col-lg-1 col-md-1">
                <i class="fa fa-random fa-2x order-button {{$order_visible or ''}}" route="{{route($order_route)}}" aria-hidden="true"></i>
            </div>       
        @endif
    </div>

    <div class="subfilter col-lg-3 col-md-3">
        @isset($subfilter)
            <select 
                data-placeholder="{{ $subfilter_placeholder or '' }}"
                id="subfilter" 
                class="form-control select2 primary-select-related">
                    <option></option>
                    <option name="todas">Todas</option>
                    @foreach($subfilter as $subfilter_input)
                        <option value="{{$subfilter_input->id}}" name='{{$subfilter_input->nombre or $subfilter_input->name}}'>{{ $subfilter_input->nombre or $subfilter_input->name}}</option>
                    @endforeach
            </select>
        @endisset                   
    </div>

    <div class="filter col-lg-3 col-md-3">
        @isset($filter)
            <select 
                data-placeholder="{{ $filter_placeholder or '' }}"
                id="filter"
                class="form-control select2 secondary-select-related">
                    <option></option>
                    @foreach($filter as $filter_input)
                        <option value="{{$filter_input->id}}" name='{{$filter_input->nombre or $subfilter_input->name}}' related-option-id="{{$filter_input->categoria_id or ''}}">{{$filter_input->nombre or $subfilter_input->name}}</option>
                    @endforeach
            </select>
        @endisset                
    </div>

    
    <div class="filter col-lg-3 col-md-3">
        <input type="text" class="buscador" placeholder="Search..." value="">
    </div>
    
    <div class="filter col-lg-3 col-md-3">
        @isset($date_filter)
            <h5 class="m-t-30">Date From - Date To</h5>
            <input class="form-control input-daterange-datepicker" type="text" id="daterange" name="daterange" value="01/01/2015 - 01/31/2015">
            <i style="position: absolute;bottom: 10px;right: 10px;top: auto;cursor: pointer;" class="glyphicon glyphicon-calendar fa fa-calendar"></i>    
        @endisset
    </div>
    
</div>