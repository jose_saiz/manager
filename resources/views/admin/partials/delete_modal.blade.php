{{--
    | Plantilla para el componente ventana modal para eliminar un elemento de la base de datos
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 16/11/2017
    |
    | Este archivo recibe las variables a través de las plantillas form y table_form ubicadas 
    | en /resources/views/admin/layout
    |
    | La etiqueta @lang autocompleta de manera dinámica el texto, los archivos de configuración se encuentran en 
    | /resources/lang/es/admin donde se definen para cada ruta. Es importante que cada vez que se abra una nueva 
    | sección se definan todas las etiquetas en su su archivo correspondiente. 
    |
    | La id #delete-button dispara la llamada ajax ubicada en el archivo saizfact.js dentro de la 
    | sección //crud_buttons y elimina el elemento de la base de datos. Se incluye la etiqueta token que
    | es recogida por el disparador por tal de poder permitir la eliminación del elemento en el servidor. 
    | 
    |----------------------------------------------------------------------------------------------------
    |    
    --}}

<div class="modal fade" id="delete-modal" tabindex="-1" role="dialog" aria-labelledby="confirmDeleteModalTitle">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="confirmDeleteModalTitle">Delete Confirmation</h4>
            </div>

            <div class="modal-body">
                <p>@lang(''.$route.'.modal', ['name' => $name])</p>
            </div>

            <div class="modal-footer">
                <div>
                    <form class="admin-delete-form" data-method="delete">
                        <a href=""  token="{{ csrf_token() }}" id="delete-button" class="btn btn-block btn-danger waves-effect waves-light"> 
                            <i class="fa fa-check m-r-5"></i> Yes
                        </a>
                    </form>
                </div>
                <div>
                    <a href="/admin/faqs" class="btn btn-block btn-default waves-effect waves-light" data-dismiss="modal"> 
                        <i class="fa fa-times m-r-5">
                    </i> No</a>
                </div>
            </div>

        </div>
    </div>
</div>