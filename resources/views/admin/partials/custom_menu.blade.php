<ol class="menu-nestable-list">
    
    @foreach ($items->sortBy('order') as $item)
    
        <li class="menu-nestable-item" data-id="{{ $item->id }}">
            @canatleast(['edit.menus'])
                <div class="pull-right item-actions">
                    <div class="btn-sm btn-danger pull-right rounded-0 menu-item-button delete-menu-item" data-id="{{ $item->id }}" data-route="{{route('menus_item')}}">
                        <i class="fa fa-trash-o"></i> Delete
                    </div>
                    <div class="btn-sm btn-default pull-right rounded-0 menu-item-button edit-menu-item"  data-id="{{ $item->id }}" data-route="{{route('menus_item')}}">
                        <i class="fa fa-pencil"></i> Edit
                    </div>
                </div>
            @endcanatleast
            
            <div class="menu-nestable-handle">
                @if($item->url != null && route($item->url, $item->parameters) != null)
                    <span>{{$item->name}}</span> <small class="url">{{route($item->url, $item->parameters)}}</small>
                @endif

                <span>{{$item->name}}</span> <small class="url">{{ $item->custom_url }}</small>
            </div>
            
            @if(!$item->children->isEmpty())
                @include('admin.partials.custom_menu', ['items' => $item->children])
            @endif
        </li>
    
    @endforeach
    
</ol>