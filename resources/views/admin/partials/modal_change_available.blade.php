
<div class="modal fade" id="create-modal-available" tabindex="-1" role="dialog" aria-labelledby="confirmCreateModalTitle" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="confirmCreateModalAvailableTitle">Change printer to available</h4>
            </div>
            <form id="modal_change_available_form" class="admin-form-modal" data-method="create" >
                {{ csrf_field() }}
                <input type="hidden" id="id"        name="id"        value="id">
                <input type="hidden" id="action"    name="action"    value="available">
                <input type="hidden" id="available" name="available" value="true" >
                
                <input type="hidden" id="start_date_time"       name="start_date_time"        value="start_date_time">
                <input type="hidden" id="start_date_time_print" name="start_date_time_print"  value="start_date_time_print">
                
                
                <div class="modal-body">
                    @include('admin.partials.errors')
                    <div class="form-group row m-t-10">
                        <label for="code" class="col-lg-3 col-md-3 col-form-label {{ ($errors->has('code')) ? 'text-danger' : '' }}">
                            code Printer 
                        </label>
                        <div class="col-lg-9 col-md-9">
                            <input disabled type="text" name="code" value="{{ old('_token') ? old('code') : '' }}" class="form-control {{ ($errors->has('code')) ? 'is-invalid' : '' }} input-error_form">
                        </div>
                    </div>
                    <h5 class="modal-title" id="confirmCreateModalAvailableText">How do you want the availability of the printer to be changed?</h5>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="available-radio" id="available-unassing" value="unassing" checked>
                      <label class="form-check-label" for="available-unassing">
                          &nbsp;&nbsp;unassign current part
                      </label>
                    </div>
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="available-radio" id="available-stop" value="stop" >
                      <label class="form-check-label" for="available-stop">
                          &nbsp;&nbsp;stop current part
                      </label>
                    </div>

                </div>
                <div class="modal-footer">
                    <div>
                        <a href=""  token="{{ csrf_token() }}" id="api-available" class="btn btn-block btn-danger waves-effect waves-light"> 
                            <i class="fa fa-check m-r-5"></i> yes
                        </a>
                    </div>
                    <div>
                        <a href="/backend/faqs" class="btn btn-block btn-default waves-effect waves-light" data-dismiss="modal"> 
                            <i class="fa fa-times m-r-5">
                        </i> no</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>