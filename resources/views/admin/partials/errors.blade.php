{{--
    | Plantilla para el componente ventana de errores
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 16/11/2017
    |
    | Este archivo recoge los errores de validación de formularios cuyas reglas han sido definidas tanto en 
    | /app/Http/Requests/Admin en la parte de servidor y en saizfact.validations.js en la parte de front.
    |
    | Si un formulario devuelve un error de validación se añade la clase 'active' al div .error-container
    | Esta clase recoge su estilo en el archivo /resources/assets/less/saizfact.less. Si no tiene la clase 
    | 'active' su opacidad está en 0, pero si la tiene su opacidad será 1 y por tanto se hará visible en 
    | pantalla. 
    | 
    |----------------------------------------------------------------------------------------------------
    |    
    --}}

<div class="error-container alert alert-danger alert-dismissable {{ $errors->any() ? 'active' : '' }}">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
    <ul class="errors">
        @foreach ($errors->all() as  $error)
            <li class="error">{{ $error }}</li>
        @endforeach
    </ul>
</div>
