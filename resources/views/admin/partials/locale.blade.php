{{--
    | Plantilla para el componente de traducciones mediante etiquetas
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 25/04/2022
    |
    |   $languages se recoge de /app/Http/ViewComposers/Admin/Translations
    |
    |   $tab es un parametro optativo, se utiliza cuando el formulario donde queremos añadir 
    |   las tabs de traducción tiene a su vez tabs (ver ejemplo en la sección Páginas en 
    |   /views/admin/paginas/edit). Esto permite al sistema de tabs distinguir qué contenido 
    |   tiene que enseñar.
    |
    |   {{ $loop->first ? ' active' : '' }} Si es la primera iteracion de bucle añade la clase active.
    |   para que la pestaña salga activada. El estilo se define en /resources/assets/less/saizfact.less 
    | ----------------------------------------------------------------------------------------------------
    |    
    --}}

@if(isset($tab))

<ul class="nav customtab nav-tabs" role="tablist">
    @foreach ($languages as $language)
        <li role="presentation" class="nav-item">
            <a href='#{{$tab}}-{{$language->alias}}' class="nav-link {{ $loop->first ? ' active' : '' }}" aria-controls="{{$language->alias}}" role="tab" data-toggle="tab" aria-expanded="true">
                <span class="hidden-xs"> {{$language->name}} </span>
				<span class="hidden-sm hidden-md hidden-lg hidden-xl">{{ strtoupper($language->alias) }}</span>
            </a>
        </li>
     @endforeach
</ul>

<div class="tab-content">
    {{ $slot }}
</div>

@else 

<ul class="nav customtab nav-tabs" role="tablist">
    @foreach ($languages as $language)
        <li role="presentation" class="nav-item">
            <a href='#{{$language->alias}}' class="nav-link {{ $loop->first ? ' active' : '' }}" aria-controls="{{$language->alias}}" role="tab" data-toggle="tab" aria-expanded="true">
                <span class="hidden-xs"> {{$language->name}} </span>
				<span class="hidden-sm hidden-md hidden-lg hidden-xl">{{ strtoupper($language->alias) }}</span>
            </a>
        </li>
     @endforeach
</ul>

<div class="tab-content">
    {{ $slot }}
</div>

@endif