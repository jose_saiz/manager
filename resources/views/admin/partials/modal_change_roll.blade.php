
<div class="modal fade" id="create-modal-change-roll" tabindex="-1" role="dialog" aria-labelledby="confirmCreateModalTitle" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
            
            
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="confirmCreateModalTitle">filament Change</h4>
                <h5 class="modal-title" id="confirmCreateModalTitle">filament to Finish current job at <span id = "tit_roll_end"></span></h5>
            </div>
            <form id="modal_change_roll_form" class="admin-form-modal" data-method="create" >
                {{ csrf_field() }}
                <input type="hidden" name="id"       value="id"          class="form-control">
                <input type="hidden" name="roll_end" value="roll_end"    class="form-control">
                <input type="hidden" name="action"   value="change_roll" class="form-control">
                <div class="modal-body">
                    @include('admin.partials.errors')
                    <div class="form-group row m-t-10">
                        <label for="code" class="col-lg-3 col-md-3 col-form-label {{ ($errors->has('code')) ? 'text-danger' : '' }}">
                            code Printer 
                        </label>
                        <div class="col-lg-9 col-md-9">
                            <input disabled type="text" name="code" value="{{ old('_token') ? old('code') : '' }}" class="form-control {{ ($errors->has('code')) ? 'is-invalid' : '' }} input-error_form">
                        </div>
                    </div>
                    <div class="form-group row m-t-10">
                        <label for="weight" class="col-lg-3 col-md-3 col-form-label {{ ($errors->has('weight')) ? 'text-danger' : '' }}">
                            filament Weight (g.) 
                        </label>
                        <div class="col-lg-9 col-md-9">
                            <input type="text" name="weight" value="{{ old('_token') ? old('weight') : '' }}" class="form-control {{ ($errors->has('weight')) ? 'is-invalid' : '' }}">
                        </div>
                    </div>
                    <div class="form-group row m-t-10">
                        <label for="datetime_replace" class="col-lg-3 col-md-3 col-form-label {{ ($errors->has('datetime_replace')) ? 'text-danger' : '' }}">
                            date Time Roll Replacement
                        </label>
                        <div class="col-lg-7 col-md-6">
                            <input type="text" name="datetime_replace" value="{{ old('_token') ? old('datetime_replace') : '' }}" class="datetime-picker form-control {{ ($errors->has('datetime_replace')) ? 'is-invalid' : '' }}"data-date-format="YYYY-MM-DD hh:mm:ss">
                            
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div>
                        <a href=""  token="{{ csrf_token() }}" id="api-change-roll" class="btn btn-block btn-danger waves-effect waves-light"> 
                            <i class="fa fa-check m-r-5"></i> yes
                        </a>
                    </div>
                    <div>
                        <a href="/backend/faqs" class="btn btn-block btn-default waves-effect waves-light" data-dismiss="modal"> 
                            <i class="fa fa-times m-r-5">
                        </i> no</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>