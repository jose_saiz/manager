
<div class="modal fade" id="create-modal" tabindex="-1" role="dialog" aria-labelledby="confirmCreateModalTitle" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
            
            
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="confirmCreateModalTitle">Crear solicitud de presupuesto API</h4>
            </div>
            <form id="api-create-form" class="{admin-form-modal}}" data-method="create" >
                {{ csrf_field() }}
                <input type="hidden" name="state_id" value="1" class="form-control">
                <div class="modal-body">
                    @include('admin.partials.errors')
                    <div class="form-group row m-t-10">
                        <label for="name" class="col-lg-3 col-md-3 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                            Título 
                        </label>
                        <div class="col-lg-9 col-md-9">
                            <input type="text" name="name" value="{{ old('_token') ? old('name') : '' }}" class="form-control {{ ($errors->has('name')) ? 'is-invalid' : '' }} input-error_form">
                        </div>
                    </div>
                    <div class="form-group row m-t-10">
                        <label for="description" class="col-lg-3 col-md-3 col-form-label {{ ($errors->has('description')) ? 'text-danger' : '' }}">
                            Description *
                        </label>
                        <div class="col-lg-9 col-md-9">
                            {{--<input type="text" name="description" value="{{ old('_token') ? old('description') : '' }}" class="form-control {{ ($errors->has('description')) ? 'is-invalid' : '' }}">--}}
                            <textarea style=" overflow-y: scroll;" id="description" name="description" rows="3" class="form-control {{ ($errors->has('description')) ? 'is-invalid' : '' }}">{{ old('_token') ? old('description') : '' }}</textarea>
                            
                        </div>
                    </div>

                    <div class="form-group row m-t-10">
                        <label for="name_customer" class="col-lg-3 col-md-3 col-form-label {{ ($errors->has('name_customer')) ? 'text-danger' : '' }}">
                            Nombre 
                        </label>
                        <div class="col-lg-9 col-md-8">
                            <input type="text" name="name_customer" value="{{( old('_token') ? old('name_customer') : isset($budget_demand->customer->name))? $budget_demand->customer->name :'' }}" class="form-control {{ ($errors->has('name_customer')) ? 'is-invalid' : '' }}">
                        </div>
                    </div>
                    <div class="form-group row m-t-10">
                        <label for="email" class="col-lg-3 col-md-3 col-form-label {{ ($errors->has('description')) ? 'text-danger' : '' }}">
                            Email *
                        </label>
                        <div class="col-lg-9 col-md-8">
                            <input type="text" id="email" name="email" value="{{ (old('_token') ? old('description') : isset($budget_demand->customer->email)) ? $budget_demand->customer->email :''}}" class="create-modal-email form-control {{ ($errors->has('description')) ? 'is-invalid' : '' }}">
                        </div>
                    </div>
                    <div class="form-group row m-t-10">
                        <label for="phone" class="col-lg-3 col-md-3 col-form-label {{ ($errors->has('phone')) ? 'text-danger' : '' }}">
                            Teléfono *
                        </label>
                        <div class="col-lg-9 col-md-8">
                            <input type="text" name="phone" id="phone" value="{{ (old('_token') ? old('phone') : isset($budget_demand->customer->phone)) ? $budget_demand->customer->phone :''}}" class="form-control {{ ($errors->has('phone')) ? 'is-invalid' : '' }}">
                        </div>
                    </div>
                    <div class="form-group row m-t-10">
                        <label for="address" class="col-lg-3 col-md-3 col-form-label {{ ($errors->has('address')) ? 'text-danger' : '' }}">
                            Dirección *
                        </label>
                        <div class="col-lg-9 col-md-8">
                            <input type="text" name="address" id="address" value="{{ (old('_token') ? old('address') : isset($budget_demand->customer->address)) ? $budget_demand->customer->address :''}}" class="form-control {{ ($errors->has('address')) ? 'is-invalid' : '' }}">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div>
                        <a href=""  token="{{ csrf_token() }}" id="create-api-button" class="btn btn-block btn-danger waves-effect waves-light"> 
                            <i class="fa fa-check m-r-5"></i> Sí
                        </a>
                    </div>
                    <div>
                        <a href="/backend/faqs" class="btn btn-block btn-default waves-effect waves-light" data-dismiss="modal"> 
                            <i class="fa fa-times m-r-5">
                        </i> No</a>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>