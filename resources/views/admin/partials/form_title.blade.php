{{--
    | Plantilla para el componente título del formulario
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 16/11/2017
    |
    | Este archivo recibe las variables a través de las plantillas form y table_form ubicadas 
    | en /resources/views/admin/layout
    |
    | La etiqueta @lang autocompleta de manera dinámica el texto, los archivos de configuración se encuentran en 
    | /resources/lang/es/admin donde se definen para cada ruta. Es importante que cada vez que se abra una nueva 
    | sección se definan todas las etiquetas en su su archivo correspondiente. 
    |
    | Si un formulario está editando un elemento que ya existe en la base de datos se cumplirá la condición 
    | $name y aparecerá el mensaje de editando el elemento, si no aparecerá el mensaje de creando un nuevo elemento. 
    | 
    |----------------------------------------------------------------------------------------------------
    |    
    --}}

<h3 class="box-title">
    @if ($name)
        @lang($route.'.edit', ['name' => $name]) 
    @else
        @lang($route.'.new')
    @endif
</h3>
