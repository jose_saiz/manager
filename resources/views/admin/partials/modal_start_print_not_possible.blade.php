
<div style ="padding-top: 100px;" class="modal fade" id="create-modal-available" tabindex="-1" role="dialog" aria-labelledby="confirmCreateModalTitle" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="confirmCreateModalAvailableTitle">There is a problem to start the printing of this piece</h4>
            </div>
            <form id="modal_start_print_not_possible_form" class="admin-form-modal" data-method="create" >
                {{ csrf_field() }}
                <input type="hidden" id="start_date_time" name="start_date_time" value="start_date_time">
                <input type="hidden" id="printer_id"      name="printer_id"      value="printer_id">
                <input type="hidden" id="part_id"         name="part_id"         value="part_id">
                <input type="hidden" id="state_id"        name="state_id"        value="state_id">
                <input type="hidden" id="action"    name="action"    value="start">
                <input type="hidden" id="available" name="available" value="true" >
                <div class="modal-body">
                    <h5 class="modal-title" id="confirmCreateModalAvailableText">
                        The previous part has not finished printing. If there was a problem with the part you were printing, cancel it before.<br>
                        
                        If the part has really finished and the part has started printing, press "Start".<br>
                        If the part has not finished and therefore the printing of the new part has not started, press "Wait".<br>
                    </h5>
                </div>
                <div class="modal-footer">
                    <div>
                        <a  href=""  
                            id="modal_start_part" 
                            class="btn btn-block btn-danger waves-effect waves-light"
                            data-token="{{ csrf_token() }}"
                            data-start ="'.$this->start_date_time_print->format('Y-m-d H:i:s').'"
                            data-printer = "'.$printerTimeLine->printer_id.'"
                            data-part = "'.$printerTimeLine->last_slot_out_range->part.'">
                            <i class="fa fa-check m-r-5"></i> Start
                        </a>
                    </div>
                    <div>
                        <a href="" class="btn btn-block btn-default waves-effect waves-light" data-dismiss="modal"> 
                            <i class="fa fa-times m-r-5">
                        </i> Wait</a>
                        
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>