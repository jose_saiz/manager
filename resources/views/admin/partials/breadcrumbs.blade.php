{{--
    | Plantilla para el componente breadcrumbs
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 26/04/2022
    |
    | Este archivo recibe las variables a través de las plantillas form, table y table_form ubicadas 
    | en /resources/views/admin/layout
    |
    | Las etiquetas @lang autocompletan de manera dinámica el texto, los archivos de configuración se encuentran en 
    | /resources/lang/es/admin donde se definen para cada ruta. Es importante que cada vez que se abra una nueva 
    | sección se definan todas las etiquetas en su su archivo correspondiente. 
    |
    | $parent_section enlaza a la ruta index definida en /routes/web. Por ejemplo, para la sección faqs su parent_section 
    | sería la propia la propia ruta index de faqs, en cambio para faqs_categorias su parent_section sería faqs. 
    |
    | El switch $active utiliza subsection por defecto con la plantilla form_table, y element_name por defecto
    | con la plantilla form, en blanco con la plantilla table. Subsection define a la breadcumbs que nos encontramos
    | en una subsección (por ejemplo, faqs_categorias es una subsección de faqs), en cambio element_name define a 
    | breadcumbs que estamos editando o creando un elemento en un formulario (por ejemplo 'Editando faq44' o 'Creando 
    | nueva faq'.   
    | ----------------------------------------------------------------------------------------------------
    |    
    --}}

<div class="row bg-title">
    <div class="col-lg-12">
        <h4 class="page-title">@lang($route.'.page_title')</h4>
    </div>
    <div class="col-sm-12 col-md-12 col-xs-12">
        <ol class="breadcrumb pull-left">
            <li><a href="/admin/">Dashboard</a></li>
            <li><a href="{{route($parent_section)}}" > @lang($route.".parent_section")</a></li>
            <li class="active">
                @switch($active)
                    @case('subsection')
                        @lang($route.'.subsection')
                        @break
                    @case('element_name')
                        @if($name)
                            @lang($route.'.edit', ['name' => $name]) 
                        @else
                            @lang($route.'.new')
                        @endif
                    @default
                @endswitch
            </li>
        </ol>
    </div>
</div>
