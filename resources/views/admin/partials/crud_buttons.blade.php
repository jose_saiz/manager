{{--
    | Plantilla para el componente botones "Guardar", "Resetear", "Eliminar"
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 16/11/2017
    |
    | Este archivo recibe las variables a través de las plantillas form y table_form ubicadas 
    | en /resources/views/admin/layout
    |
    | @isset comprueba si en la página hay un elemento ya introducido en la base de datos, 
    | si es así mostrará los botones de resetear valores y eliminar. 
    |
    | La id #store-button dispara la llamada ajax ubicada en el archivo saizfact.js dentro de la 
    | sección //crud_buttons y guarda o actualiza el formulario. 
    |
    | La id #reset-button dispara la llamada ajax ubicada en el archivo saizfact.js dentro de la 
    | sección //crud_buttons y resetea los valores del formulario a los que había previamente. 
    |
    | La id #delete-modal hace aparecer la ventana modal ubicada en /views/admin/partials/delete_modal 
    | 
    |----------------------------------------------------------------------------------------------------
    |    
    --}}

<div class="dropdown-divider"></div>
    <div class="row m-b-5">

        <div class="col-lg-4 col-sm-4 col-xs-12 p-5">
            <a id="store-button" class="btn btn-block btn-default waves-effect waves-light"> 
                <i class="fa fa-save m-r-5"></i> Save
            </a>
        </div>

        @isset ($id)

        <div class="col-lg-4 col-sm-4 col-xs-12 p-5">
            <a id="reset-button" class="btn btn-block btn-default waves-effect waves-light"> 
                <i class="fa fa-times m-r-5"></i> Reset
            </a>
        </div>

        <div class="col-lg-4 col-sm-4 col-xs-12 p-5">
            <a class="btn btn-block btn-danger waves-effect waves-light" data-toggle="modal" data-target="#delete-modal"> 
                <i class="fa fa-trash-o m-r-5"></i> Delete
            </a>
        </div>
        
        @endisset

    </div>
<div class="dropdown-divider"></div>