<div class="locale-seo-container col-lg-9 col-md-8">
    <select name="seo[parent_page.{{$idioma->alias}}]" class="locale-seo-selector form-control" disabled>
        <option class="parent-placeholder" disabled selected>Opcionalmente puede seleccionar una página padre, elija previamente un dominio</option>
        @isset($parents)
            @foreach($parents as $parent)
                @if($parent->rel_profile == $idioma->alias)
                    <option class="parent-option" value="{{$parent->url}}" data-domain="{{$parent->subdomain}}">{{$parent->title}}</option>
                @endif
            @endforeach
        @endisset
    </select>                   
</div>
