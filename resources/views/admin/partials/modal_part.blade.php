
<div style ="font-family: 'Montserrat'" class="modal fade printer-state-modal-part" id="create-modal-part" tabindex="-1" role="dialog" aria-labelledby="confirmCreateModalTitle" >
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="border-radius: 0px 0px 13px 13px;">
        <form style="padding-top: 10px;" id="modal_part_form" class="admin-form-modal" data-method="create"  method="POST" action="{{route('admin_parts_play_state_post')}}">
            {{ csrf_field() }}
            
            <input type="hidden" id="id_to_disable_printer"             name="id"              value="printer_id_to_disable">
            <input type="hidden" id="available_to_disable_printer"      name="available"       value="false">
            <input type="hidden" id="action_to_disable_printer"         name="action"          value="available">
            <input type="hidden" id="unassing-radio_to_disable_printer" name="available-radio" value="unassing">
            
            <input type="hidden" id="start_date_time"       name="start_date_time"        value="start_date_time">
            <input type="hidden" id="printer_id_post"       name="printer_id_post"        value="printer_id_post">
            <input type="hidden" id="file_id"               name="file_id"                value="file_id">
            <input type="hidden" id="order_id"              name="order_id"               value="order_id">
            
            <div class="modal-header" style="background-color: #72a166;margin-top: -4em;border-radius: 13px 13px 0px 0px;">

                
                <div style="padding-top: 0px;display:flex" class="">
                    <h3 style="color: white;" class="modal-title" id="ModalPartTitle"><div style="display:inline;" id="ModalPartTitleAux"></div></h3>
                    <label  style="color: white;padding-top: 6px;margin-left: 26px;padding-right: 5px;" 
                            for="days_current" 
                            class="col-form-label {{ ($errors->has('code')) ? 'text-danger' : '' }}">
                        <strong>Operator: </strong>
                    </label>
                    <select style="width:176px;border-radius: 8px;" 
                    		name="employe_id" id="employe_id" 
                    		data-placeholder="Select Operator" 
                    		class="form-control select2 {{ ($errors->has('categoria_id')) ? 'is-invalid' : '' }}" required>
                        	<option></option>
                        @foreach($factory_employees as $employee)
                            <option value="{{$employee->id}}" {{$employee->name ==  Auth::guard('web')->user()->name ? 'selected' : ''}} > {{ $employee->name }}</option>
                        @endforeach
                    </select>
                    <button id="disable-printer-button"  
                            style= "border-radius: 8px; background: rgb(183 26 26); margin-left: 1em; display: block; width: 132px; height: 36px;"
                            name="submit" 
                            type="submit" 
                            class="btn btn-primary">
                        Disable
                    </button>
                </div>
                <div style="padding-top: 0px;display:inline" >
                    
                    <!--  </a>-->
                </div>
            </div>
            
            <div class="modal-body">
                <div id="current_div">
                    <div style = "width: 121px;float: left;" class="col-lg-4 col-md-4 text-center"> 
                        <div style="padding-bottom: 8px;"  class="text-center" id="file_current"></div>
                        <img id="img-product-current" style= "height:150px" src="" alt="" class="img-thumbnail ">
                        <div class="text-center" id="part_current"></div>
                    </div>
                    <table  style="vertical-align: top;" class="pull-left col-lg-8 col-md-7 col-sm-6 col-6">
                         <tbody>
                             <tr>
                                 <td  id="button_current"colspan="2" style="text-align: right;">
                                 
                                 </td>
                             </tr>
                              <tr>
                                 <td style="vertical-align: top;"><strong>Order:</strong></td>
                                 
                                 <td  id="order_current" ></td>
                             </tr>
                             <tr>
                                 <td style="vertical-align: top;" ><strong>Product:</strong></td>
                                 
                                 <td id="product_current" ></td>
                             </tr>
                             <tr>
                                 <td  style="vertical-align: top;"><strong>Total:</strong></td>
                                 
                                 <td id="time_current" ></td>
                             </tr>
                              <tr>
                                 <td style="vertical-align: top;" ><strong>Printing:</strong></td>
                                 
                                 <td id="time_printing_current" ></td>
                             </tr>
                             <tr>
                                 <td style="vertical-align: top;" ><strong>Printing Percent:</strong></td>
                                 
                                 <td style="vertical-align: top;" id="percent_printing_current" ></td>
                             </tr>
                              <tr>
                                 <td style="vertical-align: top;" ><strong>Left:</strong></td>
                                 
                                 <td id="time_left_current" ></td>
                             </tr>
                              <tr>
                                 <td style="vertical-align: top;" ><strong>Start:</strong></td>
                                 
                                 <td id="start_current" ></td>
                             </tr>
                                <tr>
                                 <td style="vertical-align: top;" ><strong>End:</strong></td>
                                 
                                 <td id="end_current" ></td>
                             </tr>
                             <tr style="">
                                 <td style=" width: 183px;" class="btn-mais-info-current text-primary">
                                     <i class="open_info_current fa fa-plus-square-o"></i>
                                     <i class="open_info_current hide fa fa-minus-square-o"></i> Additional Information
                                 </td>
                                 <td> </td>
                                 <td ></td>
                             </tr> 
                         </tbody>
                    </table>
                    
                    <div style=" width: 183px;" class="clearfix"></div>
                    <p class="open_info_current hide">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    
                    
                    <input type="hidden" id="initiated_current" name="initiated_current" value="initiated_current">
                    <input type="hidden" id="current_state_id"  name="current_state_id"  value="state_current_id">
                    <input type="hidden" id="current_part_id"   name="current_part_id"   value="part_id">
                    <input type="hidden" id="current_action"    name="current_action"    value="current_action">
                    
                    
                    <div class="form-group row m-t-5">                    
                        <div style="display:none;padding-top: 0px;" class="col-lg-6 col-md-6" id = "submit-current-start">
                            <button id="current_play-post-button-state" style= "text-align: -webkit-right;border-radius: 8px;"  name="submit" type="submit" class="btn btn-primary play-post-button-state">
                                Start Printing
                            </button>
                        </div>
                        <label id= "label_error"style="padding-top: 0px;border-radius: 8px;" for="days_current" class="col-lg-4 col-md-4 col-form-label {{ ($errors->has('code')) ? 'text-danger' : '' }} cancel-part-group-current">
                            <strong>Error: </strong>
                        </label>
                        <div id ="select_error" style="padding-top: 0px;border-radius: 8px;" class="col-lg-8 col-md-8 cancel-part-group-current">
                            <select style="border-radius: 8px;" id="error_id" name="error_id" data-placeholder="Select Error type" class="form-control select2 {{ ($errors->has('categoria_id')) ? 'is-invalid' : '' }} p">
                                <option value=""></option>
                                @foreach($printers_errors as $error)
                                    <option value="{{$error->id}}" >{{ $error->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        
                    </div>
                    
                    
                    
                    
                    <div id="incident-description" class="form-group row m-t-5 cancel-part-group-current">
                        <label style="padding-top: 0px;" for="days_current" class="col-lg-4 col-md-4 col-form-label {{ ($errors->has('code')) ? 'text-danger' : '' }}">
                            <strong>Incident description: </strong>
                        </label>
                        <div style="padding-top: 7px;" class="col-lg-8 col-md-8">
                            <textarea style="border-radius: 8px;" id="incident_description" name="incident_description" rows="2" columns="" class="form-control {{ ($errors->has('code')) ? 'is-invalid' : '' }} input-error_form">
                            </textarea>
                        </div>
                        <div>
                            <!--   <a class="btn btn-primary" id = "current-cancel"  href="#">-->
                            <button id="cancel-post-button-state"  style= "text-align: -webkit-right;border-radius: 8px;"  name="submit" type="submit" class="btn btn-primary cancel-post-button-state">
                                Cancel Printing
                            </button>
                            <!--  </a>-->
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
                <div style="margin-bottom: 50px;" id="printing-real-time" class="form-group row m-t-5 cancel-part-group-current">
                         <label for="final_time" class="col-lg-4 col-md-4 col-form-label {{ ($errors->has('code')) ? 'text-danger' : '' }} cancel-group">
                            <strong>Real Time Printing: </strong>
                        </label>
                        <label for="days_current" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('code')) ? 'text-danger' : '' }}">
                           Hours 
                        </label>
                        <div class="col-lg-2 col-md-2">
                            <input style="border-radius: 8px;" type="number" min="0" max="100" id="final_hours" name="final_hours" value="{{ old('_token') ? old('code') : '' }}" class="form-control {{ ($errors->has('code')) ? 'is-invalid' : '' }} input-error_form" required>
                        </div>
                        <label for="days_current" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('code')) ? 'text-danger' : '' }}">
                            Minutes 
                        </label>
                        <div class="col-lg-2 col-md-2">
                            <input style="border-radius: 8px;" type="number" min="0" max="59" id="final_minutes" name="final_minutes" value="{{ old('_token') ? old('code') : '' }}" class="form-control {{ ($errors->has('code')) ? 'is-invalid' : '' }} input-error_form" required>
                        </div>
                        <div style="display:none;" class="" id = "submit-finish">
                            <button id="finish-post-button-state"  style= "text-align: -webkit-right;border-radius: 8px;"  name="submit-finish" type="submit" class="btn btn-primary finish-button-state">
                                Finished Printing
                            </button>
                        </div>
                </div>
                <div class="modal-footer">
                </div>
                <div id="next_div">
                    <div style = "width: 121px;float: left;" class="col-lg-4 col-md-4 text-center"> 
                        <div style="padding-bottom: 8px;"  class="text-center" id="file_next"></div>
                        <img id="img-product-next" style= "height:110px" src="" alt="" class="img-thumbnail ">
                        <div class="text-center" id="part_next"></div>
                    </div>
                    <table  style="vertical-align: top;" class="pull-left col-lg-8 col-md-7 col-sm-6 col-6">
                         <tbody>
                             <tr>
                                 <td  id="button_next"colspan="2" style="text-align: right;">
                                 
                                 </td>
                             </tr>
                              <tr>
                                 <td style="vertical-align: top;"><strong>Order:</strong></td>
                                 
                                 <td  id="order_next" ></td>
                             </tr>
                             <tr>
                                 <td style="vertical-align: top;" ><strong>Product:</strong></td>
                                 
                                 <td id="product_next" ></td>
                             </tr>
                             <tr>
                                 <td  style="vertical-align: top;"><strong>Total:</strong></td>
                                 
                                 <td id="time_next" ></td>
                             </tr>
                              <!--
                              <tr>
                                 <td style="vertical-align: top;" ><strong>Printing:</strong></td>
                                 
                                 <td id="time_printing_next" ></td>
                             </tr>
                             <tr>
                                 <td style="vertical-align: top;" ><strong>Printing Percent:</strong></td>
                                 
                                 <td style="vertical-align: top;" id="percent_printing_next" ></td>
                             </tr>
                              <tr>
                                 <td style="vertical-align: top;" ><strong>Left:</strong></td>
                                 
                                 <td id="time_left_next" ></td>
                             </tr>
                              -->
                              <tr>
                                 <td style="vertical-align: top;" ><strong>Start:</strong></td>
                                 
                                 <td id="start_next" ></td>
                             </tr>
                                <tr>
                                 <td style="vertical-align: top;" ><strong>End:</strong></td>
                                 
                                 <td id="end_next" ></td>
                             </tr>
                             <tr style="">
                                 <td style="width: 183px;" class="btn-mais-info-next text-primary">
                                     <i class="open_info_next fa fa-plus-square-o"></i>
                                     <i class="open_info_next hide fa fa-minus-square-o"></i> Additional Information
                                 </td>
                                 <td> </td>
                                 <td ></td>
                             </tr> 
                         </tbody>
                    </table>
                    
                    <div  class="clearfix"></div>
                    <p class="open_info_next hide">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
                    
                    
                    <input type="hidden" id="initiated_next" name="initiated_next" value="initiated_next">
                    <input type="hidden" id="next_state_id"     name="next_state_id"     value="state_next_id">
                    
                    <input type="hidden" id="next_part_id"     name="next_part_id"     value="next_part_id">
                    <input type="hidden" id="next_action"     name="next_action"     value="next_action">
                    
                    
                    <div class="form-group row m-t-5">                    
                        <div style="display:none;padding-top: 0px;" class="col-lg-6 col-md-6" id = "submit-next-start">
                            <button id="next_play-post-button-state"  style= "text-align: -webkit-right;border-radius: 8px;"  name="submit" type="submit" class="btn btn-primary play-post-button-state">
                                Start Printing
                            </button>
                        </div>
                   </div>
                </div>
            </div>
        </form>
        </div>
    </div>
</div>