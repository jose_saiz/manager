{{--
    | Plantilla para el componente breadcrumbs
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 12/05/2022
    |
    | Este archivo recibe las variables a través de las plantillas form, table y table_form ubicadas 
    | en /resources/views/admin/layout
    |
    | Las etiquetas @lang autocompletan de manera dinámica el texto, los archivos de configuración se encuentran en 
    | /resources/lang/es/admin donde se definen para cada ruta. Es importante que cada vez que se abra una nueva 
    | sección se definan todas las etiquetas en su su archivo correspondiente. 
    |
    | $parent_section enlaza a la ruta index definida en /routes/web. Por ejemplo, para la sección faqs su parent_section 
    | sería la propia la propia ruta index de faqs, en cambio para faqs_categorias su parent_section sería faqs. 
    |
    | El switch $active utiliza subsection por defecto con la plantilla form_table, y element_name por defecto
    | con la plantilla form, en blanco con la plantilla table. Subsection define a la breadcumbs que nos encontramos
    | en una subsección (por ejemplo, faqs_categorias es una subsección de faqs), en cambio element_name define a 
    | breadcumbs que estamos editando o creando un elemento en un formulario (por ejemplo 'Editando faq44' o 'Creando 
    | nueva faq'.   
    | ----------------------------------------------------------------------------------------------------
    |    
    --}}

<div class="row bg-title">
 

    <div class="col-sm-11 col-md-10 col-xs-11">
    	<h4 class="page-title">@lang($route.'.page_title')</h4>
        <ol class="breadcrumb pull-left">
            <li><a href="/admin/">Dashboard</a></li>
            <li><a href="{{route($parent_section)}}" > @lang($route.".parent_section")</a></li>
            <li class="active">
                @switch($active)
                    @case('subsection')
                        @lang($route.'.subsection')
                        @break
                    @case('element_name')
                        @if($name)
                            @lang($route.'.edit', ['name' => $name]) 
                        @else
                            @lang($route.'.new')
                        @endif
                    @default
                @endswitch
            </li>
        </ol>
     </div>
     
     <div class="col-lg-1" style="padding-top: 10px;">
     <form id='set-time-form' action="{{route('admin_set_start_datetime')}}" method="post">
     	{{ csrf_field() }}	
        <input type="datetime-local" id="start-date-time" name="start-date-time"
            value="{{$start_date_time->format('Y-m-d') }}T{{ $start_date_time->format('H:i') }}"
            min="{{$min->format('Y-m-d') }}T{{$min->format('H:i') }}" 
            max="{{$max->format('Y-m-d') }}T{{$max->format('H:i') }}"
        />
        {{--<button style="width: 154px;" type="button" class="btn btn-primary btn-sm btn-block">Set Current Time</button>--}}
        <input style="width: 154px;" id="button-start-date-time" class="btn btn-primary btn-sm btn-block" type="submit" value="Set Current Time">
        </form>
        <label style="color:white" for="zoom-range">Zoom</label>
        <input style="width: 154px;" id="zoom-range" type="range" step="5" min="5" max="100" onChange="changeZoom(this.value)" />
    
    </div>
    
</div>

