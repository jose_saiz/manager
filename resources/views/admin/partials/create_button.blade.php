{{--
    | Plantilla para el componente boton "Nuevo"
    |------------------------------------------------------------------------------------------------------
    | Saizfact, última actualización de esta documentación realizada el 26/04/2022
    |
    | Este archivo recibe las variables a través de las plantillas table y table_form ubicadas 
    | en /resources/views/admin/layout
    |
    | La id #create-button dispara la llamada ajax ubicada en el archivo saizfact.js dentro de la 
    | sección //navigation_buttons
    |
    | La etiqueta url del boton apunta a la ruta "create" definida en /routes/web y es recogida 
    | por el disparador ajax   
    |----------------------------------------------------------------------------------------------------
    |    
    --}}

<div class="dropdown-divider"></div>
<div class="row m-b-5">
    <div class="col-lg-4 col-sm-4 col-xs-12 p-5">
        <a id="create-button" url="{{route($route)}}" class="btn btn-block btn-default waves-effect waves-light"> 
            <i class="fa fa-plus-circle m-r-5"></i> 
            New
        </a>
    </div>
</div>

<div class="dropdown-divider"></div>
