@php
    $route = 'roles';
@endphp

@extends('admin.layout.table', [
    'route' => $route,
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">Listado</h3>
        
        @canatleast(['edit.roles'])
            @include('admin.partials.create_button', ['route' => $route .'_create'])
        @endcanatleast
        
        <div class="table-responsive m-t-10">
            
            @include('admin.partials.filters')
            
            <table id="main-table" class="table table-striped" route="{{route('roles_json')}}">
                <thead>
                    <tr>
                        <th data-data="id" data-name="id">#</th>
                        <th data-data="name" data-name="name">Nombre</th>
                        <th data-data="slug" data-name="slug">Slug</th>
                        {{--<th data-data="description" data-name="description">Descripción</th>--}}
                        <th data-data="permissions[, ].name" data-name="permissions.name">Permisos</th>
                        <th data-data="updated_at" data-name="updated_at">Modificado</th>
                        <th data-orderable="false" data-defaultContent="
                            <a class='edit-button' route='{{route($route)}}'> 
                                <i class='fa fa-pencil text-inverse m-r-10'></i> 
                            </a>">
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection