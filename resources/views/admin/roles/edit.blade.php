{{--
    |
    | *$permissions se recoge de /app/Http/ViewComposers/Admin/UsersPermissions 20/11/2017_saizfact
    |    
    --}}

@php
    $route = 'roles';
@endphp

@extends('admin.layout.form', [
    'route' => $route, 
    'name' => $role->name, 
    'id' => $role->id
])

@section('form')

    @include('admin.partials.delete_modal', ['route' => $route, 'name' => $role->name])
    
    <div class="white-box">
    
        @include('admin.partials.form_title', ['route' => $route, 'name' => $role->name])
        @canatleast(['edit.roles'])
            @include('admin.partials.crud_buttons', ['id' => $role->id])
        @endcanatleast
        @include('admin.partials.errors')
    
        <div>
            <form class='admin-form' id='roles-form' action="{{route('roles_store')}}">
        
                {{ csrf_field() }}
        
                @isset ($role->id)
                    <input type="hidden" name="id" value="{{$role->id}}">
                @endisset
                
                <input type="hidden" id="permission_list" name="permission_list" value="secret" class="form-control" >
        
                <div class="form-group row m-t-10">
                    <label for="name" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                        Nombre *
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="name" value="{{ old('_token') ? old('name') : $role->name }}" class="form-control {{ ($errors->has('name')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
        
                <div class="form-group row m-t-10">
                    <label for="slug" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('slug')) ? 'text-danger' : '' }}">
                        Slug *
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="slug" value="{{ old('_token') ? old('slug') : $role->slug }}" class="form-control {{ ($errors->has('slug')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
        
                <div class="form-group row m-t-10">
                    <label for="description" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('description')) ? 'text-danger' : '' }}">
                        Descripción 
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="description" value="{{ old('_token') ? old('description') : $role->description }}" class="form-control {{ ($errors->has('description')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
        
                <div class="form-group row">
                    <label for="activo" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('activo')) ? 'text-danger' : '' }}">
                        Activo 
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <input type="checkbox" name="activo" value="true" {{ (old('_token') ? old('activo') : $role->activo) ? 'checked' : '' }} class="bt-switch" data-size="small">
                    </div>
                </div>
        
                <div class="form-group row">
                    <label for="special" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('special')) ? 'text-danger' : '' }}">
                        Especial 
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <select name="special" data-placeholder="..." data-allow-clear="true" class="form-control select2 {{ ($errors->has('special')) ? 'is-invalid' : '' }}">
                            <option></option>
                            <option value="all-access" {{$role->special == 'all-access' ? 'selected':''}}>All Access</option>
                            <option value="no-access" {{$role->special == 'no-access' ? 'selected':''}}>No Access</option>
                        </select>
                    </div>
                </div>
                
                <div class="form-group row">
                    <label for="role" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('activo')) ? 'text-danger' : '' }}">
                        Permisos 
                    </label>
                    <div class="col-lg-9 col-md-8">
                        <select data-placeholder="Seleccione los permisos de este rol" name="role_select" class="form-control select2 multiple-option">
                            <option></option>
                            @foreach ($permissions as $permission)
                                <option value="{{$permission->id}}" class="{{$permission->activo ? '':'desactivado'}}">{{$permission->name}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                
                <div class="multiple-checkbox-container row">
                    @foreach ($permissions as $permission)
                        
                        @php $permission_assigned = in_array($permission->id, $permissions_assigned) @endphp
                        
                        <div class="col-lg-6 col-md-6 multiple-checkbox-element {{ $permission_assigned ? 'active' : ''}}">
                            <label for="{{$permission->id}}">{{$permission->name}} <i class="fa fa-trash-o text-inverse m-r-10"></i></label>
                            <input id="{{$permission->id}}" class='multiple-checkbox' type="checkbox" name="permission_id[]" value="{{$permission->id}}"  {{ $permission_assigned ? 'checked' : ''}}>
                        </div>
                        
                    @endforeach
                </div>
                
            </form>
        </div>
    </div>

@endsection
