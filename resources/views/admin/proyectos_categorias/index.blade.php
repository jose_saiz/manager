@php
    $route = 'admin_proyectos_categorias';
@endphp

@extends('admin.layout.table_form', [
    
    'route' => $route, 
    'parent_section' => 'admin_proyectos', 
    'name' => $categoria->nombre, 
    'id' => $categoria->id
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">Listado</h3>

        @canatleast(['edit.categorias.proyectos'])
            @include('admin.partials.create_button', ['route' => $route .'_create'])
        @endcanatleast

        <div class="table-responsive m-t-10">
        
            @include('admin.partials.filters')

            <table id="main-table" class="table table-striped"  route="{{route($route.'_json')}}">
                <thead>
                    <tr>
                        <th data-data="id" data-name="id">#</th>
                        <th data-data="nombre" data-name="nombre">Nombre</th>
                        <th data-data="updated_at" data-name="updated_at">Modificado</th>
                        <th data-orderable="false" data-defaultContent="
                            <a class='edit-button' route='{{route($route)}}'> 
                                <i class='fa fa-pencil text-inverse m-r-10'></i> 
                            </a>">
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('form')

    @include('admin.partials.delete_modal', ['route' => $route, 'name' => $categoria->nombre])

    <div class="white-box">
    
        @include('admin.partials.form_title', ['route' => $route, 'name' => $categoria->nombre])
        @canatleast(['edit.categorias.proyectos'])
            @include('admin.partials.crud_buttons', ['id' => $categoria->id])
        @endcanatleast
        @include('admin.partials.errors')
    
        <div>
            <form class='admin-form' id="proyectos-categorias-form"  action="{{route($route.'_store')}}">
                {{ csrf_field() }}
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item">
                        <a href="#contenido" class="nav-link active" aria-controls="contenido" role="tab" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <span class="hidden-xs">Contenido</span>
                        </a>
                    </li>
                    <li role="presentation" class="nav-item">
                        <a href="#seo" class="nav-link" aria-controls="seo" role="tab" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">
                                <i class="fa fa-tag"></i>
                            </span>
                            <span class="hidden-xs">SEO</span>
                        </a>
                    </li>
                </ul>
                @isset ($categoria->id)
                    <input type="hidden" name="id" value="{{$categoria->id}}">
                @endisset
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="contenido">
                        <div class="form-group row m-t-10">
                            <label for="nombre" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">Nombre *</label>
                            <div class="col-lg-9 col-md-8">
                                <input type="text" name="nombre" value="{{ old('_token') ? old('nombre') : $categoria->nombre }}" class="form-control {{ ($errors->has('nombre')) ? 'is-invalid' : '' }}">
                            </div>
                        </div>
                        {{--
                        <div class="form-group row">
                            <label for="privada" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('privada')) ? 'text-danger' : '' }}">Privada</label>
                            <div class="col-lg-9 col-md-8">
                                <input type="checkbox" name="privada" value="false" {{ old('_token') ? old('activo') : $categoria->privada ? 'checked' : '' }} class="bt-switch" data-size="small">
                            </div>
                        </div>
                        --}}
                        <div class="form-group row">
                            <label for="activo" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('activo')) ? 'text-danger' : '' }}">Activo</label>
                            <div class="col-lg-9 col-md-8">
                                <input type="checkbox" name="activo" value="true" {{ old('_token') ? old('activo') : $categoria->activo ? 'checked' : '' }} class="bt-switch" data-size="small">
                            </div>
                        </div>
                        @component('admin.partials.locale', ['tab' => 'contenido'])
                        @foreach ($idiomas as $idioma)
                        <div role="tabpanel" class="tab-pane {{ $loop->first ? ' active' : '' }}" id="contenido-{{$idioma->alias}}">
                            <div class="form-group row m-t-10">
                                <label for="titulo" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">
                                    Título *
                                </label>
                                <div class="col-lg-9 col-md-8">
                                    <input type="text" name="locale[titulo.{{$idioma->alias}}]" value='{{isset($locale["titulo.$idioma->alias"])?$locale["titulo.$idioma->alias"]:"" }}' class="form-control">
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endcomponent
                    </div>
                    <div role="tabpanel" class="tab-pane" id="seo">
                        @component('admin.partials.locale', ['tab' => 'seo'])
                        @foreach ($idiomas as $idioma)
                        
                        <div role="tabpanel" class="tab-pane {{ $loop->first ? ' active' : '' }}" id="seo-{{$idioma->alias}}">
                            <div class="form-group row m-t-10">
                                <label for="name" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                                    Descripción *
                                </label>
                                <div class="col-lg-9 col-md-8">
                                    <input type="text" name="seo[description.{{$idioma->alias}}]" value='{{$seo["description.$idioma->alias"] }}' class="form-control">
                                </div>
                            </div>
        
                            <div class="form-group row m-t-10">
                                <label for="slug" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('slug')) ? 'text-danger' : '' }}">
                                    Keywords *
                                </label>
                                <div class="col-lg-9 col-md-8">
                                    <input type="text" name="seo[keywords.{{$idioma->alias}}]" value='{{$seo["keywords.$idioma->alias"] }}' class="form-control">
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endcomponent
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
