@php
    $route = 'config';
@endphp

@extends('admin.layout.form', [
    'route' => $route, 
    'name' => $config->nombre, 
    'id' => $config->id
])

@section('form')

    @include('admin.partials.delete_modal', ['route' => $route, 'name' => $config->nombre])

    <div class="white-box">
    
        @include('admin.partials.form_title', ['route' => $route, 'name' => $config->nombre])
        @canatleast(['edit.configs'])
            @include('admin.partials.crud_buttons', ['id' => $config->id])
        @endcanatleast
        @include('admin.partials.errors')
    
        <div>
            <form class='admin-form' id="configs-form"  action="{{route('config_store')}}">
        
                {{ csrf_field() }}
        
                @isset ($config->id)
                    <input type="hidden" name="id" value="{{$config->id}}">
                @endisset
                
                <div class="form-group row m-t-10">
                    <label for="nombre" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">Name *</label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="nombre" value="{{ old('_token') ? old('nombre') : $config->nombre }}" class="form-control {{ ($errors->has('nombre')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
                
                <div class="form-group row m-t-10">
                    <label for="clave" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('clave')) ? 'text-danger' : '' }}">Key *</label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="clave" value="{{ old('_token') ? old('clave') : $config->clave }}" class="form-control {{ ($errors->has('clave')) ? 'is-invalid' : '' }}"
                            {{(Auth::guard('web')->user()->isSaizfact() ? '' : 'readonly')}}
                            style="{{(Auth::guard('web')->user()->isSaizfact() ? 'border:1px solid orange;' : '')}}">
                    </div>
                </div>
                
                <div class="form-group row m-t-10">
                    <label for="valor" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('valor')) ? 'text-danger' : '' }}">Value *</label>
                    <div class="col-lg-9 col-md-8">
                        <input type="text" name="valor" value="{{ old('_token') ? old('valor') : $config->valor }}" class="form-control {{ ($errors->has('valor')) ? 'is-invalid' : '' }}">
                    </div>
                </div>
                
                <div class="form-group row m-t-10">
                            <label for="dominio" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('dominio')) ? 'text-danger' : '' }}">
                                Domain (void for all)
                            </label>
                            <div class="col-lg-9 col-md-8">
                                <select name="dominio" data-placeholder="Seleccione un dominio para la página"  id="select-dominio" class="form-control select2 {{ ($errors->has('dominio')) ? 'is-invalid' : '' }}">
                                    <option></option>
                                    <option value="academy" {{$config->dominio == 'academy' ? 'selected':''}}>Academy</option>
                                    <option value="acofar" {{$config->dominio == 'manager' ? 'selected':''}}>Manager</option>
                                    <option value="corporate" {{$config->dominio == 'factory' ? 'selected':''}}>Factory</option>
                                    <option value="formulacion" {{$config->dominio == 'head_quarters' ? 'selected':''}}>Head Quarters</option>
                                </select>
                            </div>
                        </div>

                
            </form>
        </div>
    </div>

@endsection
