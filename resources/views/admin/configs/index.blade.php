@php
    $route = 'config';
@endphp

@extends('admin.layout.table', [
    'route' => $route
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">Config List</h3>

        @canatleast(['edit.configs'])
            @include('admin.partials.create_button', ['route' => $route .'_create'])
        @endcanatleast

        <div class="table-responsive m-t-10">
        
            @include('admin.partials.filters')

            <table id="main-table" class="table table-striped"  route="{{route('config_json')}}">
                <thead>
                    <tr>
                        <th data-data="id" data-name="id">#</th>
                        <th data-data="nombre" data-name="nombre">Name</th>
                        <th data-data="clave" data-name="clave">Key</th>
                        <th data-data="valor" data-name="valor">Value</th>
                        <th data-data="dominio" data-name="dominio">Domain</th>
                        <th data-orderable="false" data-defaultContent="
                            <a class='edit-button' route='{{route($route)}}'>
                                <i class='fa fa-pencil text-inverse m-r-10'></i>
                            </a>">
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection