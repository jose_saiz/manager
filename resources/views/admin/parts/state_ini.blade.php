@php
    //header("Refresh:10");
    $route = 'admin_parts_state';
    $parent_section = 'admin_parts'; 
    $name = 'State Printers'; 
    
    $color="yyyy";
    $blink_class =  "YYYY";
@endphp

@extends('admin.layout.master')
@section('content')
@include('admin.partials.breadcrumbs_state', ['route' => $route, 'parent_section' => $parent_section, 'active' => 'element_name', 'name' => $name,])
@canatleast(['edit.parts'])
	@include('admin.partials.modal_part', ['route' => $route .'_modal_part','language'=>'en'])
    @include('admin.partials.modal_start_print_not_possible_state', ['route' => $route .'_modal_start_print_not_possible_state','language'=>'en'])
    
@endcanatleast
<div class="inline" style="margin-bottom: 10px;width: 100%;">
    <b>{{$printersMatrix->count()}} Printers: </b> 
    @foreach($printersMatrix as $printer)
        &nbsp;&nbsp;<a class="{{$printer->blink_class}}" style="color:{{$printer->color}};display:inline-block" href="#printer{{$printer->printer_id}}">{{$printer->code_printer}}</a>
    @endforeach
    <br>
    @if ((isset ($next_part)) && (count($next_part)>0))
        <b>Next Part: </b>
        <a  style="display:inline-block" href="#printer{{strval($next_part['printer']->printer_id-1)}}"/>{{$next_part['slot']->part}}</a>
        <b> - Printer: </b>{{$next_part['printer']->code_printer}} <b> - Estimated start time: </b>{{$next_part['slot']->start}}
    @endif
</div>

<div class="inline"  style="margin-bottom: 17px;width: 100%;">
    <b> Greenies: {{  $summary['greenies']['total']}} </b>
            (Assigned: {{count($summary['greenies']['assigned'])}}, 
            Unassigned: {{count($summary['greenies']['unassigned'])}},
            Printed: {{count($summary['greenies']['printed'])}},
            Partially assigned: {{count($summary['greenies']['partially_assigned'])}}, 
            Other {{count($summary['greenies']['other'])}}) 
    <br>
    <b> Parts: {{  count($summary['parts']['assigned']) + count($summary['parts']['unassigned'])}}</b>
            (Assigned: {{count($summary['parts']['assigned'])}}, 
            Unassigned: {{count($summary['parts']['unassigned'])}},
            Printed: {{count($summary['parts']['printed'])}},
            Missing: {{count($summary['parts']['missing']) }},
            Pending: {{$printersMatrix->pending_parts_count - $summary['parts']['total']}} ,  
            Other {{count($summary['parts']['other'])}}) 
    <br>
    <b> Efficiency: {{  $printersMatrix->percent()}}% </b>
    <br>
    {{$printersMatrix->printNightOverlaps();}}
  
</div>

<div class="white-box">
    <h3 class="box-title">Listado
    <div class="btn-group pull-right" style="padding-bottom:14px;">
        <button type="button" class="btn btn-primary btn-lg dropdown-toggle" data-toggle="dropdown" >Export <span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu">
            <li><a class="dataExport" data-type="csv">CSV</a></li>
            <li><a class="dataExport" data-type="excel">XLS</a></li>          
            <li><a class="dataExport" data-type="txt">TXT</a></li>                           
        </ul>
    </div>
    </h3>
    {{$printersMatrix->count()}}
    <div class="table-responsive m-t-6 overflow-panel" style="overflow-x: auto;">
        <table style="table-layout: fixed;" id="data-table-planning" class="table table-striped" style="border: 1px;" ">

           <tbody>
                @for ($i=0;$i<=$rows;$i++)
                <tr>
                    @for ($j=0;$j<=$columns;$j++ )
                    <td style ="border-right: 1px solid #e4e7ea;border-left: 1px solid #e4e7ea;">
                    @if (array_key_exists($j,$statePrinterMatrix[$i]))
                        <table class="table-responsive m-t-6">
                            <tr>
                            	<td style="color:white;background-color:{{$statePrinterMatrix[$i][$j]->lamp}}">
                            		<a title="aaaaaaaaaaaaaaaala" id="button-show-modal-part-{{$statePrinterMatrix[$i][$j]->current_slot->part}}" class="show-button-modal-part"
                                                        data-start ="{{$start_date_time->format('Y-m-d H:i:s')}}"
                                                        data-printer = "{{$statePrinterMatrix[$i][$j]->code_printer}}"
                                                        data-file_part_name_current = "{{$statePrinterMatrix[$i][$j]->current_slot ->file_part_name}}"
                                                        data-file_name_current = "{{$statePrinterMatrix[$i][$j]->current_slot ->file_name}}"
                                                        data-part_c = "{{$statePrinterMatrix[$i][$j]->current_slot->part}}"
                                                        data-state = "{{$statePrinterMatrix[$i][$j]->current_slot->state}}"
                                                        href= "">
                                    				
                                    			
                            		{{$statePrinterMatrix[$i][$j]->code_printer}}
                            		</a>
                        		</td>
                    		</tr>
                            @if ($statePrinterMatrix[$i][$j]->current_slot!=null)
                            <tr>
                                <td     class="{{(((isset ($next_part)) && (count($next_part)>0)) && ($next_part['slot']->part== $statePrinterMatrix[$i][$j]->current_slot->part ))?'blink_next_part':''}}"
                                        style="font-size: 11px;
                                           height: 136px;
                                           border: 3px solid black;
                                           background-color: rgb({{conf_parts_colors()[$statePrinterMatrix[$i][$j]->current_slot->file_part_name][0][0]}}, 
                                                                 {{conf_parts_colors()[$statePrinterMatrix[$i][$j]->current_slot->file_part_name][0][1]}}, 
                                                                 {{conf_parts_colors()[$statePrinterMatrix[$i][$j]->current_slot->file_part_name][0][2]}});
                                           color:            rgb({{conf_parts_colors()[$statePrinterMatrix[$i][$j]->current_slot->file_part_name][1][0]}}, 
                                                                 {{conf_parts_colors()[$statePrinterMatrix[$i][$j]->current_slot->file_part_name][1][1]}}, 
                                                                 {{conf_parts_colors()[$statePrinterMatrix[$i][$j]->current_slot->file_part_name][1][2]}});">
                                    <table>
                                    	<tr>
                                    		<td>
                                    		{{$statePrinterMatrix[$i][$j]->current_slot ->file_part_name}}
                                            <br/>
                                            {{$statePrinterMatrix[$i][$j]->current_slot ->part}}
                                            <br/>
                                            @if ($statePrinterMatrix[$i][$j]->current_slot->state == 9)
                                                 @if ($statePrinterMatrix[$i][$j]->current_slot->initiated == 0)
                                                 <a title="aaaaaaaaaaaaaaaala" id="button-time-line-play-{{$statePrinterMatrix[$i][$j]->current_slot->part}}" class="button-time-line play-button-state"
                                                    data-start ="{{$start_date_time->format('Y-m-d H:i:s')}}"
                                                    data-printer = "{{$statePrinterMatrix[$i][$j]->printer_id}}"
                                                    data-part = "{{$statePrinterMatrix[$i][$j]->current_slot->part}}"
                                                    data-state = "9"
                                                    href= "/admin/parts/play_state/{{$start_date_time_print->format('Y-m-d H:i:s')}}/{{$statePrinterMatrix[$i][$j]->printer_id}}/{{$statePrinterMatrix[$i][$j]->current_slot->part}}/9">
                                                    <img  style="width:25px" src="/assets/admin/images/play.png">
                                                 </a>
                                                 @else
                                                 <a title="aaaaaaaaaaaaaaaala" id="button-time-line-cancel-{{$statePrinterMatrix[$i][$j]->current_slot->part}}" class="button-time-line"
                                                    href= "/admin/parts/cancel_state/{{$start_date_time_print->format('Y-m-d H:i:s')}}/{{$statePrinterMatrix[$i][$j]->current_slot->part}}/{{$statePrinterMatrix[$i][$j]->printer_id}}/">
                                                    <img  style="width:25px" src="/assets/admin/images/cancel.png">
                                                 </a>
                                                 @endif
                                            @else
                                                 @if ($statePrinterMatrix[$i][$j]->current_slot->state == 10)
                                                 <img  style="width:25px" src="/assets/admin/images/printed.png">
                                                 @endif
                                            @endif
                                    		</td>
                                    		<td>
                                    			<img  style="width:48px" src="/images/products/{{$statePrinterMatrix[$i][$j]->current_slot->file_name}}.png">	
                                    		</td>
                                		</tr>
                                		<tr>
                                    		<td>
                                    		
                                    			{{--$statePrinterMatrix[$i][$j]->current_time_printing['hours']}}:{{$statePrinterMatrix[$i][$j]->current_time_printing['minutes']--}}
                                    		
                                    		</td>
                                    		<td>
                                    			{{--$statePrinterMatrix[$i][$j]->current_time_left['hours']}}:{{$statePrinterMatrix[$i][$j]->current_time_left['minutes']--}} 
                                    		</td>
                                		</tr>
                            		</table>
                                </td>
                            </tr>
                            @endif
                            @if ($statePrinterMatrix[$i][$j]->next_slot!=null)
                            <tr>
                                <td class="{{(((isset ($next_part)) && (count($next_part)>0)) && ($next_part['slot']->part== $statePrinterMatrix[$i][$j]->next_slot->part ))?'blink_next_part':''}}"
                                    style="font-size: 11px;
                                           height: 136px;
                                           background-color: rgb({{conf_parts_colors()[$statePrinterMatrix[$i][$j]->next_slot->file_part_name][0][0]}}, 
                                                                 {{conf_parts_colors()[$statePrinterMatrix[$i][$j]->next_slot->file_part_name][0][1]}}, 
                                                                 {{conf_parts_colors()[$statePrinterMatrix[$i][$j]->next_slot->file_part_name][0][2]}});
                                           color:            rgb({{conf_parts_colors()[$statePrinterMatrix[$i][$j]->next_slot->file_part_name][1][0]}}, 
                                                                 {{conf_parts_colors()[$statePrinterMatrix[$i][$j]->next_slot->file_part_name][1][1]}}, 
                                                                 {{conf_parts_colors()[$statePrinterMatrix[$i][$j]->next_slot->file_part_name][1][2]}});">
                                    <table>
                                        <tr>
                                            <td>
                                             	{{$statePrinterMatrix[$i][$j]->next_slot->file_part_name}}
                                                <br/>
                                                {{$statePrinterMatrix[$i][$j]->next_slot->part}}
                                                <br/>
                                                @if (($statePrinterMatrix[$i][$j]->next_slot->state == 9) && ($statePrinterMatrix[$i][$j]->current_slot->initiated != 0))
                                                <!-- >&& ($statePrinterMatrix[$i][$j]->next_slot->initiated == 1)) -->
                                                     @if ($statePrinterMatrix[$i][$j]->next_slot->initiated == 0)
                                                     <a title="aaaaaaaaaaaaaaaala" id="button-time-line-play-{{$statePrinterMatrix[$i][$j]->next_slot->part}}" class="button-time-line play-button-state"
                                                        data-start ="{{$start_date_time->format('Y-m-d H:i:s')}}"
                                                        data-printer = "{{$statePrinterMatrix[$i][$j]->printer_id}}"
                                                        data-part = "{{$statePrinterMatrix[$i][$j]->next_slot->part}}"
                                                        data-state = "9"
                                                        href= "/admin/parts/play_state/{{$start_date_time_print->format('Y-m-d H:i:s')}}/{{$statePrinterMatrix[$i][$j]->printer_id}}/{{$statePrinterMatrix[$i][$j]->next_slot->part}}/9">
                                                        <img  style="width:25px" src="/assets/admin/images/play.png">
                                                     </a>
                                                     @else
                                                         <a title="aaaaaaaaaaaaaaaala" id="button-time-line-cancel-{{$statePrinterMatrix[$i][$j]->next_slot->part}}" class="button-time-line"
                                                        href= "/admin/parts/cancel_state/{{$start_date_time_print->format('Y-m-d H:i:s')}}/{{$statePrinterMatrix[$i][$j]->next_slot->part}}/{{$statePrinterMatrix[$i][$j]->printer_id}}/">
                                                        <img  style="width:25px" src="/assets/admin/images/cancel.png">
                                                         </a>
                                                     @endif
                                                @else
                                                     @if ($statePrinterMatrix[$i][$j]->next_slot->state == 10)
                                                     <img  style="width:25px" src="/assets/admin/images/printed.png">
                                                     @endif
                                                @endif
                                            </td>
                                            <td>
                                            	 <img  style="width:48px" src="/images/products/{{$statePrinterMatrix[$i][$j]->next_slot->file_name}}.png">
                                            </td>
                                		</tr>
                                    </table>
                                </td>
                            </tr>
                            @endif
                        </table>
                    @endif
                    </td>
                    @endfor
                </tr>
                @endfor

            </tbody>
        </table>
    </div>
</div>
@endsection
