@php
    //header("Refresh:10");
    $route = 'admin_parts_state';
    $parent_section = 'admin_parts'; 
    $name = 'State Printers'; 
    
    $color="yyyy";
    $blink_class =  "YYYY";
@endphp

@extends('admin.layout.master')
@section('content')
@include('admin.partials.breadcrumbs_state', ['route' => $route, 'parent_section' => $parent_section, 'active' => 'element_name', 'name' => $name,])
@canatleast(['edit.parts'])
    @include('admin.partials.modal_part', ['route' => $route .'_modal_part','language'=>'en'])
    @include('admin.partials.modal_start_print_not_possible_state_post', ['route' => $route .'_modal_start_print_not_possible_state','language'=>'en'])
    
@endcanatleast


<div style= "float:right;margin-left: -231px;padding-top:27px">
	<b>
        Left to right
    </b>
</div>
<div style= "float:right;" class="checkbox-JASoft ">

    <input type="checkbox" id="left-to-right-direction" value="initial" {{(session('left_to_right_direction')==1)?'checked':''}}/>
    <label for="left-to-right-direction">TEXTO QUE NO DEBERÍA VERSE</label>
</div>

<div class="inline" style="margin-bottom: 10px;width: 100%;">
    <b>{{$printersMatrix->count()}} Printers: </b> 
    @foreach($printersMatrix as $printer)
    	@if ($printer->available == 1)
        &nbsp;&nbsp;<a class="{{$printer->blink_class}}" style="color:{{$printer->color}};display:inline-block" href="#printer{{$printer->printer_id}}">{{$printer->code_printer}}</a>
        @else
        &nbsp;&nbsp;<div style="color:grey;display:inline-block;">{{$printer->code_printer}}</div>
        @endif 
    @endforeach
    <br>
    @if ((isset ($next_part)) && (count($next_part)>0))
        <b>Next Part: </b>
        <a  style="display:inline-block" href="#printer{{strval($next_part['printer']->printer_id-1)}}"/>{{$next_part['slot']->part}}</a>
        <b> - Printer: </b>{{$next_part['printer']->code_printer}} <b> - Estimated start time: </b>{{$next_part['slot']->start}}
    @endif
</div>


<div class="inline"  style="margin-bottom: 17px;width: 100%;font-family: 'Montserrat';">
    <b> Greenies: {{  $summary['greenies']['total']}} </b>
            (Assigned: {{count($summary['greenies']['assigned'])}}, 
            Unassigned: {{count($summary['greenies']['unassigned'])}},
            Printed: {{count($summary['greenies']['printed'])}},
            Partially assigned: {{count($summary['greenies']['partially_assigned'])}}, 
            Other {{count($summary['greenies']['other'])}}) 
    <br>
    <b> Parts: {{  count($summary['parts']['assigned']) + count($summary['parts']['unassigned'])}}</b>
            (Assigned: {{count($summary['parts']['assigned'])}}, 
            Unassigned: {{count($summary['parts']['unassigned'])}},
            Printed: {{count($summary['parts']['printed'])}},
            Missing: {{count($summary['parts']['missing']) }},
            Pending: {{$printersMatrix->pending_parts_count - $summary['parts']['total']}} ,  
            Other {{count($summary['parts']['other'])}}) 
    <br>
    <b> Efficiency: {{  $printersMatrix->percent()}}% </b>
    <br>
    <i style="color:green" 
        class="fa fa-circle m-r-5 ">
    </i>Printing
    <i style="color:red" 
        class="fa fa-circle m-r-5 blink_lamp">
    </i>Finished print and we have to launch another
        <i style="color:blue" 
        class="fa fa-circle m-r-5 blink_lamp">
    </i>Printer about to finish and we have to launch another
    <i style="color:magenta" 
        class="fa fa-circle m-r-5 blink_lamp">
    </i>Last part printed and confirm and confirm the result
    <i style="color:brown" 
        class="fa fa-circle m-r-5 ">
    </i>Last part Printed correctly
    <i style="color:grey" 
        class="fa fa-circle m-r-5">
    </i>Printer disable.
    <i style="color:white" 
        class="fa fa-circle m-r-5">
    </i>Printer available but no assigned parts
    <i style="color:Yellow" 
        class="fa fa-circle m-r-5">
    </i>The filament will end in 2 hours
    
    <i style="color:Yellow" 
        class="fa fa-circle m-r-5 blink_lamp">
    </i>the filament is finished
    
    {{$printersMatrix->printNightOverlaps();}}
  
</div>
<div style= "overflow:auto" class="overflow-panel"  data-fl-scrolls>
@php

$width = "";
if ($printersMatrix->count()> 27){
	$width = "width:".($printersMatrix->count()/27*1000)."px;";
}

@endphp
<div style="{{$width}}" class="white-box" >
    <h3 class="box-title">Printers Rack
    <div class="btn-group pull-right" style="padding-bottom:14px;">
        <button type="button" class="btn btn-primary btn-lg dropdown-toggle" data-toggle="dropdown" >Export <span class="caret"></span></button>
        <ul class="dropdown-menu" role="menu">
            <li><a class="dataExport" data-type="csv">CSV</a></li>
            <li><a class="dataExport" data-type="excel">XLS</a></li>          
            <li><a class="dataExport" data-type="txt">TXT</a></li>                           
        </ul>
    </div>
    </h3>
    {{$printersMatrix->count()}}:{{$rows+1}}x{{$columns+1}}
    <div id="statePanel" class="table-responsive m-t-6 overflow-panel" style="overflow-x: hidden">
    
        <table style="table-layout: fixed;margin-left:-25px;" id="data-table-planning" class="table" >

           <tbody align="center">
                @for ($i=0;$i<=$rows;$i++)
                <tr>
                    @for ($j=0;$j<=$columns;$j++ )
                    <!-- <td style ="border-right: 1px solid #e4e7ea;border-left: 1px solid #e4e7ea;"> -->
                   <td style ="border:0px;" align="center">

                   
                   {{--$i."x".$j --}}
                    @if (array_key_exists($j,$statePrinterMatrix[$i]))
                        <table style="overflow: unset;"class="table-responsive m-t-6">
                            <tr>
                                <!--<td style="color:white;background-color:black;border-radius:9px" class="shape11">-->
                                <td style="color:black;border-top: 0px;" class=""> 
                                <!-- <td style="color:black;" class="shape33">  --> 
                                <!--<td class="rack-body  rack-button">-->
                                <!--  <td class="shape11"> --> 
                                 @if ($statePrinterMatrix[$i][$j]->current_slot!=null)
                                 {{--var_dump($statePrinterMatrix[$i][$j]->last_slot_out_range)--}}

                                    <a  data-toggle="tooltip" 
                                    	title="
            								 <div class='row m-t-5 ' style='width:600px;'>
                								 <div class='col-lg-4 col-md-4' >
                                                 	Printing: {{$statePrinterMatrix[$i][$j]->current_time_printing['hours']}}:{{$statePrinterMatrix[$i][$j]->current_time_printing['minutes']}}<br>
                                                 	Percent: {{$statePrinterMatrix[$i][$j]->current_percent_printing}}%<br>
                                                 	Left: {{$statePrinterMatrix[$i][$j]->current_time_left['hours']}}:{{$statePrinterMatrix[$i][$j]->current_time_left['minutes']}}
                                                 	<img style='width:180px;height:180px;' src='/images/products/{{$statePrinterMatrix[$i][$j]->current_slot ->file_name}}.png' />
                                                 </div>
                                         	 </div>
                                    		"
                                    	id="button-show-modal-part-{{$statePrinterMatrix[$i][$j]->printer_id}}" 
                                        class="show-button-modal-part "
                                        style="color: black;"
                                            data-start_date_time ="{{$start_date_time->format('Y-m-d H:i:s')}}"
                                            data-printer = "{{$statePrinterMatrix[$i][$j]->printer_id}}"
                                            data-code_printer = "{{$statePrinterMatrix[$i][$j]->code_printer}}"
                                            data-available_printer = "{{$statePrinterMatrix[$i][$j]->available}}"
                                            
                                            data-order_id = "{{$statePrinterMatrix[$i][$j]->current_slot->order}}"
                                            
                                            data-product_current = "{{$statePrinterMatrix[$i][$j]->current_slot->product_name}}"
                                            data-final_product_current = "{{$statePrinterMatrix[$i][$j]->current_slot->final_product}}"
                                            data-file_part_name_current = "{{$statePrinterMatrix[$i][$j]->current_slot ->file_part_name}}"
                                            data-file_name_current = "{{$statePrinterMatrix[$i][$j]->current_slot ->file_name}}"
                                            data-part_current =  "{{$statePrinterMatrix[$i][$j]->current_slot->part}}"
                                            data-days_current =  "{{$statePrinterMatrix[$i][$j]->current_days}}"
                                            data-time_current =  "{{$statePrinterMatrix[$i][$j]->current_time}}"
                                            data-start_current = "{{$statePrinterMatrix[$i][$j]->current_slot->start}}"
                                            data-end_current =   "{{$statePrinterMatrix[$i][$j]->current_slot->end}}"
                                            data-start_compact_current = "{{$statePrinterMatrix[$i][$j]->current_start_compact}}"
                                            data-end_compact_current =   "{{$statePrinterMatrix[$i][$j]->current_end_compact}}"
                                            data-state_current = "{{$statePrinterMatrix[$i][$j]->current_slot->state}}"
                                            data-time_left_current =  "{{$statePrinterMatrix[$i][$j]->current_time_left['hours']}}:{{$statePrinterMatrix[$i][$j]->current_time_left['minutes']}} "
                                            data-time_printing_current =  "{{$statePrinterMatrix[$i][$j]->current_time_printing['hours']}}:{{$statePrinterMatrix[$i][$j]->current_time_printing['minutes']}}"
                                            data-percent_printing_current = "{{$statePrinterMatrix[$i][$j]->current_percent_printing}}"
                                            data-initiated_current = "{{$statePrinterMatrix[$i][$j]->current_slot->initiated}}"
                                            data-state_current = "{{$statePrinterMatrix[$i][$j]->current_slot->state}}"
                                            data-button_current = "{{$statePrinterMatrix[$i][$j]->current_button}}"
                                            data-action_current = "{{$statePrinterMatrix[$i][$j]->current_action}}"
                                            
                                        @if (isset($statePrinterMatrix[$i][$j]->next_slot))
                                            data-order_next = "{{$statePrinterMatrix[$i][$j]->next_slot->order}}"
                                            data-product_next = "{{$statePrinterMatrix[$i][$j]->next_slot->product_name}}"
                                            data-final_product_next = "{{$statePrinterMatrix[$i][$j]->next_slot->final_product}}"
                                            data-file_part_name_next = "{{$statePrinterMatrix[$i][$j]->next_slot ->file_part_name}}"
                                            data-file_name_next = "{{$statePrinterMatrix[$i][$j]->next_slot ->file_name}}"
                                            data-part_next =  "{{$statePrinterMatrix[$i][$j]->next_slot->part}}"
                                            data-days_next =  "{{$statePrinterMatrix[$i][$j]->next_days}}"
                                            data-time_next =  "{{$statePrinterMatrix[$i][$j]->next_time}}"
                                            data-start_next = "{{$statePrinterMatrix[$i][$j]->next_slot->start}}"
                                            data-end_next =   "{{$statePrinterMatrix[$i][$j]->next_slot->end}}"
                                            data-start_compact_next = "{{$statePrinterMatrix[$i][$j]->next_start_compact}}"
                                            data-end_compact_next =   "{{$statePrinterMatrix[$i][$j]->next_end_compact}}"
                                            data-state_next = "{{$statePrinterMatrix[$i][$j]->next_slot->state}}"
                                            data-time_left_next =  "{{$statePrinterMatrix[$i][$j]->next_time_left['hours']}}:{{$statePrinterMatrix[$i][$j]->next_time_left['minutes']}} "
                                            data-time_printing_next =  "{{$statePrinterMatrix[$i][$j]->next_time_printing['hours']}}:{{$statePrinterMatrix[$i][$j]->next_time_printing['minutes']}}"
                                            data-percent_printing_next = "{{$statePrinterMatrix[$i][$j]->next_percent_printing}}"
                                            data-initiated_next = "{{$statePrinterMatrix[$i][$j]->next_slot->initiated}}"
                                            data-state_next = "{{$statePrinterMatrix[$i][$j]->next_slot->state}}"
                                            data-button_next = "{{$statePrinterMatrix[$i][$j]->next_button}}"
                                            data-action_next = "{{$statePrinterMatrix[$i][$j]->next_action}}"
                                        @endif
                                                        
                                        href= "">
                                        <div class="shape33B">
                                            <div>
                                                {{$statePrinterMatrix[$i][$j]->code_printer}}  
                                                
                                                <!-- 
                                                <div class="circle_{{isset($statePrinterMatrix[$i][$j]->lamp)? $statePrinterMatrix[$i][$j]->lamp:'black'}} blink_lamp">
                                                </div>
                                                 -->
                                                 
                                                <i     style="color:{{ isset($statePrinterMatrix[$i][$j]->lamp)? $statePrinterMatrix[$i][$j]->lamp:'black'}}" 
                                                    class="fa fa-circle m-r-5 {{isset($statePrinterMatrix[$i][$j]->lamp)
                                                                                &&
                                                                                (($statePrinterMatrix[$i][$j]->lamp ==  'blue') || ($statePrinterMatrix[$i][$j]->lamp ==  'red')|| ($statePrinterMatrix[$i][$j]->lamp ==  'magenta')) 
                                                                                ? 'blink_lamp'
                                                                                :''}} ">
                                                </i><br>
                                                {{$statePrinterMatrix[$i][$j]->current_slot ->file_part_name}}
                                            </div>
                                        </div>
                                    </a>
                                @else
                                    @if ($statePrinterMatrix[$i][$j]->last_slot_out_range!=null)
                                        <a  id="button-show-modal-part-{{$statePrinterMatrix[$i][$j]->printer_id}}"
                                            class="show-button-modal-part "
                                            style="color: black;"
                                                data-start_date_time ="{{$start_date_time->format('Y-m-d H:i:s')}}"
                                                data-printer = "{{$statePrinterMatrix[$i][$j]->printer_id}}"
                                                data-code_printer = "{{$statePrinterMatrix[$i][$j]->code_printer}}"
                                                data-available_printer = "{{$statePrinterMatrix[$i][$j]->available}}"
                                                
                                                data-order = "{{$statePrinterMatrix[$i][$j]->last_slot_out_range->order}}"
                                                
                                                data-product_current = "{{$statePrinterMatrix[$i][$j]->last_slot_out_range->product_name}}"
                                                data-final_product_current = "{{$statePrinterMatrix[$i][$j]->last_slot_out_range->final_product}}"
                                                data-file_part_name_current = "{{$statePrinterMatrix[$i][$j]->last_slot_out_range ->file_part_name}}"
                                                data-file_name_current = "{{$statePrinterMatrix[$i][$j]->last_slot_out_range ->file_name}}"
                                                data-part_current =  "{{$statePrinterMatrix[$i][$j]->last_slot_out_range->part}}"
                                                data-days_current =  "{{$statePrinterMatrix[$i][$j]->last_slot_out_range_days}}"
                                                data-time_current =  "{{$statePrinterMatrix[$i][$j]->last_slot_out_range_time}}"
                                                data-start_current = "{{$statePrinterMatrix[$i][$j]->last_slot_out_range->start}}"
                                                data-end_current =   "{{$statePrinterMatrix[$i][$j]->last_slot_out_range->end}}"
                                                data-start_compact_current = "{{$statePrinterMatrix[$i][$j]->last_slot_out_range_start_compact}}"
                                                data-end_compact_current =   "{{$statePrinterMatrix[$i][$j]->last_slot_out_range_end_compact}}"
                                                data-state_current = "{{$statePrinterMatrix[$i][$j]->last_slot_out_range->state}}"
                                                data-time_left_current =  "{{$statePrinterMatrix[$i][$j]->last_slot_out_range_time_left['hours']}}:{{$statePrinterMatrix[$i][$j]->last_slot_out_range_time_left['minutes']}} "
                                                data-time_printing_current =  "{{$statePrinterMatrix[$i][$j]->last_slot_out_range_time_printing['hours']}}:{{$statePrinterMatrix[$i][$j]->last_slot_out_range_time_printing['minutes']}}"
                                                data-percent_printing_current = "{{$statePrinterMatrix[$i][$j]->last_slot_out_range_percent_printing}}"
                                                data-initiated_current = "{{$statePrinterMatrix[$i][$j]->last_slot_out_range->initiated}}"
                                                data-state_current = "{{$statePrinterMatrix[$i][$j]->last_slot_out_range->state}}"
                                                data-button_current = "last_slot_out_range"
                                                data-action_current = "last_slot_out_range"
                                            
                                        href= "">
                                            <div class="shape33B">
                                                <div>
                                                    {{$statePrinterMatrix[$i][$j]->code_printer}}  
                                                    
                                                    <!-- 
                                                    <div class="circle_{{isset($statePrinterMatrix[$i][$j]->lamp)? $statePrinterMatrix[$i][$j]->lamp:'black'}} blink_lamp">
                                                    </div>
                                                     -->
                                                    
                                                    <i     style="color:{{ isset($statePrinterMatrix[$i][$j]->lamp)?  $statePrinterMatrix[$i][$j]->lamp:'black'}}" 
                                                        class="fa fa-circle m-r-5 {{isset($statePrinterMatrix[$i][$j]->lamp)
                                                                                    &&
                                                                                    (($statePrinterMatrix[$i][$j]->lamp ==  'blue') || ($statePrinterMatrix[$i][$j]->lamp ==  'red')|| ($statePrinterMatrix[$i][$j]->lamp ==  'magenta')) 
                                                                                    ? 'blink_lamp'
                                                                                    :''}} ">
                                                    </i><br>
                                                    {{$statePrinterMatrix[$i][$j]->last_slot_out_range ->file_part_name}}
                                                </div>
                                            </div>
                                        </a>
                                     @else
                                        <a  id="button-activate-printer-{{$statePrinterMatrix[$i][$j]->printer_id}}" 
                                        	data-printer= "{{$statePrinterMatrix[$i][$j]->printer_id}}" 
                                        	style="color: black;" 
                                        	class="{{$statePrinterMatrix[$i][$j]->available == 1?'disable-printer-button':'enable-printer-button'}}" 
                                        	href= "">
                                            <div class="shape33B">
                                                <div>
                                                    {{$statePrinterMatrix[$i][$j]->code_printer}}  
                                                    <i     style="color:{{$statePrinterMatrix[$i][$j]->lamp}}" 
                                                        class="fa fa-circle m-r-5 ">
                                                    </i><br>
                                                    @if ($statePrinterMatrix[$i][$j]->last_slot_out_range != null) 
                                                        {{$statePrinterMatrix[$i][$j]->last_slot_out_range ->file_part_name}}
                                                    @endif
                                                </div>
                                            </div>
                                        </a>
                                            
                                    @endif

                                @endif    
                                </td>
                            </tr>
                        </table>
                    @endif
                    </td>
                    @endfor
                </tr>
                @endfor

            </tbody>
        </table>
    </div>
</div>
</div>
@endsection

