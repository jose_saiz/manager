{{--
    |
    | *$categorias se recoge de /app/Http/ViewComposers/Admin/PrintersCategorias 25/04/2022_saizfact
    |    
    --}}

@php
    $route = 'admin_parts';
@endphp

@extends('admin.layout.form', [
    'route' => $route, 
    'name' => $printer->code, 
    'id' => $printer->id
])

@section('form')

    @include('admin.partials.delete_modal', ['route' => $route, 'name' => $printer->code])
    
    <div class="white-box">
    
        {{--@include('admin.partials.form_title', ['route' => $route, 'name' => $printer->code])--}}
        @canatleast(['edit.printers'])
            @include('admin.partials.crud_buttons', ['id' => $printer->id,])
        @endcanatleast
        <div>
            <form class='admin-form' id='printers-form' action="{{route($route . '_store')}}">
        
                {{ csrf_field() }}
                
                <ul class="nav nav-tabs" role="tablist">
        
                    <li role="presentation" class="nav-item">
                        <a href="#contenido" class="nav-link active" aria-controls="contenido" role="tab" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <span class="hidden-xs">Contenido</span>
                        </a>
                    </li>
        
                    <li role="presentation" class="nav-item">
                        <a href="#state" class="nav-link" aria-controls="state" role="tab" data-toggle="tab" aria-expanded="false">
                            <span class="visible-xs">
                                <i class="fa fa-tag"></i>
                            </span>
                            <span class="hidden-xs">State</span>
                        </a>
                    </li>
        
                </ul>
                
                @isset ($printer->id)
                    <input type="hidden" name="id" value="{{$printer->id}}">
                @endisset
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="contenido">       
                        <div class="panel-body">
                            <div class="form-group row m-t-10">
                                <label for="code" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('code')) ? 'text-danger' : '' }}">
                                    Code *
                                </label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="code" value="{{ old('_token') ? old('code') : $printer->code }}" class="form-control {{ ($errors->has('code')) ? 'is-invalid' : '' }}">
                                </div>
                                <div class="col-lg-1 col-md-1"></div>
                                <label for="orden" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('orden')) ? 'text-danger' : '' }}">
                                    Order *
                                </label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="orden" value="{{ old('_token') ? old('orden') : $printer->orden }}" class="form-control {{ ($errors->has('orden')) ? 'is-invalid' : '' }}">
                                </div>
                            </div>
                    
                            <div class="form-group row m-t-10">
                                <label for="categoria_id" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('categoria_id')) ? 'text-danger' : '' }}">
                                    Categorie *
                                </label>
                                <div class="col-lg-3 col-md-3">
                                    <select name="categoria_id" data-placeholder="Selecet categoriy" class="form-control select2 {{ ($errors->has('categoria_id')) ? 'is-invalid' : '' }}">
                                        <option></option>
                                        @foreach($categorias as $categoria)
                                            <option value="{{$categoria->id}}" {{$printer->category_id == $categoria->id ? 'selected':''}} class="{{$categoria->active ? '':'desactivado'}}">{{ $categoria->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            	<div class="col-lg-1 col-md-1"></div>
                                <label for="available" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('available')) ? 'text-danger' : '' }}">
                                    available *
                                </label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="checkbox" name="available" value="true" {{ (old('_token') ? old('available') : $printer->available) ? 'checked' : '' }} class="bt-switch" data-size="small">
                                </div>
                            </div>
                    
                            <div class="form-group row">
                                <label for="orden" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('roll_end_datetime')) ? 'text-danger' : '' }}">
                                    roll_end
                                </label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" id="roll_end_datetime" name="roll_end_datetime" value="{{ old('_token') ? old('roll_end_datetime') : $printer->roll_end_datetime }}" 
                                           class="datetime-picker form-control {{ ($errors->has('roll_end_datetime')) ? 'is-invalid' : '' }}" data-date-format="YYYY-MM-DD hh:mm:ss">
                                </div>
                                <div class="col-lg-1 col-md-1"></div>
                                <label for="roll_replacement_datetime" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('roll_replacement_datetime')) ? 'text-danger' : '' }}">
                                    roll_replacement
                                </label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" id="roll_replacement_datetime" name="roll_replacement_datetime" value="{{ old('_token') ? old('roll_replacement_datetime') : $printer->roll_replacement_datetime }}" 
                                           class="datetime-picker form-control {{ ($errors->has('roll_replacement_datetime')) ? 'is-invalid' : '' }}" 
                                           data-date-format="YYYY-MM-DD hh:mm:ss" data-show-meridian="false">
                                    
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="orden" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('roll_weight')) ? 'text-danger' : '' }}">
                                    roll_weight
                                </label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" name="roll_weight" value="{{ old('_token') ? old('roll_weight') : $printer->roll_weight }}" class="form-control {{ ($errors->has('roll_weight')) ? 'is-invalid' : '' }}">
                                </div>
                            </div>
                        </div><!-- END <div class="panel-body"> -->



                
                        @component('admin.partials.locale', ['tab' => 'contenido'])
                
                        @foreach ($languages as $idioma)
                
                        <div role="tabpanel" class="tab-pane {{ $loop->first ? ' active' : '' }}" id="contenido-{{$idioma->alias}}">
                            <div class="form-group row m-t-10">
                                <label for="description" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('description')) ? 'text-danger' : '' }}">
                                    Description *
                                </label>
                                <div class="col-lg-12 col-md-12">
                                    <textarea name="locale[description.{{$idioma->alias}}]" class="rich" id="ckeditor.{{$idioma->alias}}" class="form-control">
                                        {{$locale["description.$idioma->alias"]}}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endcomponent
                    </div> <!-- END <div role="tabpanel" class="tab-pane active" id="contenido"> -->
                    
                    
                    
                    <div role="tabpanel" class="tab-pane" id="state">
                        <div role="tabpanel" class="tab-pane " id="">
                            <div class="form-group row m-t-10">

                            </div>
                        </div>
                    </div>
                </div><!--END <div class="tab-content"> -->
            </form>
       </div>
    </div>

@endsection
