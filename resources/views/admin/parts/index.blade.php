@php
    $route = 'admin_parts';
    $filters_order_route = '';
@endphp

@canatleast(['edit.parts'])
    @php
        $filters_order_route = 'parts_reorder';
    @endphp
@endcanatleast

@extends('admin.layout.table', [
    'route' => $route
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">List</h3>

        @canatleast(['edit.parts'])
            @include('admin.partials.create_button', ['route' => $route .'_create','language'=>'en'])
            @include('admin.partials.modal_change_roll', ['route' => $route .'_modal_change_roll','language'=>'en'])
            @include('admin.partials.modal_change_available', ['route' => $route .'_modal_change_avialable','language'=>'en'])
            <!-- @include('admin.partials.modal_api_create', ['route' => $route .'_modal_api_create'])-->
        @endcanatleast
        @if (isset($message))
        <div class="error-container alert alert-danger alert-dismissable active">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <ul class="errors">
                    <li class="error">{{ $message }}</li>
            </ul>
	    </div>
	    @endif
        <div class="table-responsive m-t-10">
            
            @include('admin.partials.filters', [
                'order_route' => $filters_order_route,
                'order_visible' => ($filters_order_route ? 'visible' : ''),
                'filter_placeholder' => 'Filtrar por Categoría',
            ])
            <div class="circle_red">
            </div>
            <i style="color:red" class="fa fa-circle m-r-5 blink_lamp"></i>




            <table id="main-table" class="table table-striped"  route="{{route($route.'_json')}}">
                <thead>
                    <tr>

                        <th data-width="1px" data-data="id"    data-name="id">id</th>
                        <th data-width="1px" data-data="file_id"    data-name="file_id">file_id</th>
                        <th data-width="1px" data-data="file.name"    data-name="file.name">file</th>
                        <th data-width="1px" data-data="file_part_name" data-name="file_part_name">file</th>
                        <th data-width="1px" data-data="version_part_name" data-name="version_part_name">version</th>
                        <th data-width="1px" data-data="order_id"   data-name="order_id">order</th>
                        <!-- <th data-width="1px" data-data="order.neighborhood_id"   data-name="order.neighborhood_id">neighborhood_id</th> -->
                        <th data-width="1px" data-data="printer.code"   data-name="printer.code">printer</th>
                        <th data-width="1px" data-data="state.name"   data-name="state.name">state</th>
                        <th data-width="1px" data-data="state_id"   data-name="state_id">precent</th>
                        <th data-width="1px" data-data="start_datetime"   data-name="start_datetime">start_datetime</th>
                        <th data-width="1px" data-data="end_datetime"   data-name="end_datetime">end_datetime</th>
                        <th data-width="1px" data-data="weight"   data-name="weight">weight</th>
                        <th data-width="1px" data-data="initiated"   data-name="initiated">initiated</th>

                        
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection


