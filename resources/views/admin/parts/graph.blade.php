@php
    $route = 'admin_parts_graph';
    $parent_section = 'admin_parts'; 
    $name = 'State Printers'; 
    
    $color="yyyy";
    $blink_class =  "YYYY";
@endphp

@extends('admin.layout.master')
@section('content')
@include('admin.partials.breadcrumbs_graph', ['route' => $route, 'parent_section' => $parent_section, 'active' => 'element_name', 'name' => $name,])
@canatleast(['edit.parts'])
    @include('admin.partials.modal_start_print_not_possible', ['route' => $route .'_modal_start_print_not_possible','language'=>'en'])
@endcanatleast
<div class="inline" style="margin-bottom: 10px;width: 100%;">
    <b>{{$printersMatrix->count()}} Printers: </b> 
    @foreach($printersMatrix as $printer)
        @if ($printer->available == 1)
        &nbsp;&nbsp;<a class="{{$printer->blink_class}}" style="color:{{$printer->color}};display:inline-block" href="#printer{{$printer->printer_id}}">{{$printer->code_printer}}</a>
        @else
        &nbsp;&nbsp;<a class="{{$printer->blink_class}}" style="color:grey;display:inline-block" href="#printer{{$printer->printer_id}}">{{$printer->code_printer}}</a>
        @endif 
    @endforeach
    <br>
    @if ((isset ($next_part)) && (count($next_part)>0))
        <b>Next Part: </b>
        <a  style="display:inline-block" href="#printer{{strval($next_part['printer']->printer_id-1)}}"/>{{$next_part['slot']->part}}</a>
        <b> - Printer: </b>{{$next_part['printer']->code_printer}} <b> - Estimated start time: </b>{{$next_part['slot']->start}}
    @endif
</div>

<div class="inline"  style="margin-bottom: 17px;width: 100%;">
    <b> Greenies: {{  $summary['greenies']['total']}} </b>
            (Assigned: {{count($summary['greenies']['assigned'])}}, 
            Unassigned: {{count($summary['greenies']['unassigned'])}},
            Printed: {{count($summary['greenies']['printed'])}},
            Partially assigned: {{count($summary['greenies']['partially_assigned'])}}, 
            Other {{count($summary['greenies']['other'])}}) 
    <br>
    <b> Parts: {{  count($summary['parts']['assigned']) + count($summary['parts']['unassigned'])}}</b>
            (Assigned: {{count($summary['parts']['assigned'])}}, 
            Unassigned: {{count($summary['parts']['unassigned'])}},
            Printed: {{count($summary['parts']['printed'])}},
            Missing: {{count($summary['parts']['missing']) }},
            Pending: {{$printersMatrix->pending_parts_count - $summary['parts']['total']}} ,  
            Other {{count($summary['parts']['other'])}}) 
    <br>
    <b> Efficiency: {{  $printersMatrix->percent()}}% </b>
    <br>
    {{$printersMatrix->printNightOverlaps();}}
  
</div>

<div class="white-box "  >

{{$printersMatrix->printHtmlAll();}}
</div>
@endsection
