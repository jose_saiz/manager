{{--      
    Las filas marcadas en rojo son las que vienen con descripcion o keywords vacías, dicha función se define en el archivo saizfact-datatable.js

    if (data['description.description'] == null || data['keywords.keywords'] == null) {
        $(row).addClass('highlight');
    }

--}}

@php
    $route = 'seo';
@endphp

@extends('admin.layout.table_form', [
    'route' => $route, 
    'parent_section' => 'tags', 
    'name' => $seos->key, 
    'id' => $seos->id
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">Listado</h3>
        
        <div class="table-responsive m-t-10">
            
            @include('admin.partials.filters')
            
            <table id="main-table" class="table table-striped"  route="{{route($route . '_json')}}">
                <thead>
                    <tr>
                        <th data-data="id" data-name="id" data-visible="false"></th>
                        <th data-data="description" data-name="description" data-visible="false"></th>
                        <th data-data="keywords" data-name="keywords" data-visible="false"></th>
                        <th data-data="key" data-name="key">Titulo</th>
                        <th data-orderable="false" data-defaultContent="
                            <a class='edit-button' route='{{route($route)}}'> 
                                <i class='fa fa-pencil text-inverse m-r-10'></i> 
                            </a>">
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('form')
    
    <div class="white-box">
    
        @include('admin.partials.form_title', ['route' => $route, 'name' => $seos->key])

        @canatleast(['edit.tags'])
            @include('admin.partials.crud_buttons_without_remove', ['id' => $seos->key])
        @endcanatleast

        @include('admin.partials.errors')
    
        <div>
            <form class='admin-form' id='seo-form' action="{{route($route .'_store')}}">
        
                {{ csrf_field() }}
        
                @isset ($seos->id)
                    <input type="hidden" name="group" value="{{$seos->group}}">
                    <input type="hidden" name="key" value="{{$seos->key}}">
                @endisset

                @component('admin.partials.locale')
        
                @foreach ($idiomas as $idioma)
        
                <div role="tabpanel" class="tab-pane {{ $loop->first ? ' active' : '' }}" id="{{$idioma->alias}}">
                    <div class="form-group row m-t-10">
                        <label for="locale" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                            Url *
                        </label>
                        <div class="col-lg-9 col-md-8">
                            <input type="text" name="locale[url.{{$idioma->alias}}]" value='{{$locale["url.$idioma->alias"] or ''}}' class="form-control {{ ($errors->has('url')) ? 'is-invalid' : '' }} block-parameters">
                        </div>
                    </div>

                    <div class="form-group row m-t-10">
                        <label for="locale" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                            Description *
                        </label>
                        <div class="col-lg-9 col-md-8">
                            <input type="text" name="locale[description.{{$idioma->alias}}]" value='{{$locale["description.$idioma->alias"] or ''}}' class="form-control {{ ($errors->has('value')) ? 'is-invalid' : '' }}">
                        </div>
                    </div>

                    <div class="form-group row m-t-10">
                        <label for="locale" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                            Keywords *
                        </label>
                        <div class="col-lg-9 col-md-8">
                            <input type="text" name="locale[keywords.{{$idioma->alias}}]" value='{{$locale["keywords.$idioma->alias"] or ''}}' class="form-control {{ ($errors->has('value')) ? 'is-invalid' : '' }}">
                        </div>
                    </div>

                    <input type="hidden" name="locale[old_url.{{$idioma->alias}}]" value="{{$locale["url.$idioma->alias"] or ''}}">
                </div>
        
                @endforeach
        
                @endcomponent
        
            </form>
       </div>
    </div>

@endsection