@php
    $route = 'admin_printers_categorias';
@endphp

@extends('admin.layout.table_form', [
    
    'route' => $route, 
    'parent_section' => 'admin_printers', 
    'name' => $categoria->nombre, 
    'id' => $categoria->id
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">Printers Category Listo</h3>

        @canatleast(['edit.categorias.printers'])
            @include('admin.partials.create_button', ['route' => $route .'_create'])
        @endcanatleast

        <div class="table-responsive m-t-10">
        
            @include('admin.partials.filters')

            <table id="main-table" class="table table-striped"  route="{{route($route.'_json')}}">
                <thead>
                    <tr>
                        <th data-data="id" data-name="id">#</th>
                        <th data-data="name" data-name="name">Name</th>
                        <th data-data="updated_at" data-name="updated_at">Updated at</th>
                        <th data-orderable="false" data-defaultContent="
                            <a class='edit-button' route='{{route($route)}}'> 
                                <i class='fa fa-pencil text-inverse m-r-10'></i> 
                            </a>">
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('form')

    @include('admin.partials.delete_modal', ['route' => $route, 'name' => $categoria->nombre])

    <div class="white-box">
    
        @include('admin.partials.form_title', ['route' => $route, 'name' => $categoria->nombre])
        @canatleast(['edit.categorias.printers'])
            @include('admin.partials.crud_buttons', ['id' => $categoria->id])
        @endcanatleast
        @include('admin.partials.errors')
    
        <div>
            <form class='admin-form' id="printers-categorias-form"  action="{{route($route.'_store')}}">
                {{ csrf_field() }}
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="nav-item">
                        <a href="#contenido" class="nav-link active" aria-controls="contenido" role="tab" data-toggle="tab" aria-expanded="true">
                            <span class="visible-xs">
                                <i class="fa fa-pencil"></i>
                            </span>
                            <span class="hidden-xs">Content</span>
                        </a>
                    </li>
                </ul>
                @isset ($categoria->id)
                    <input type="hidden" name="id" value="{{$categoria->id}}">
                @endisset
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="contenido">
                        <div class="form-group row m-t-10">
                            <label for="name" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('nombre')) ? 'text-danger' : '' }}">Name *</label>
                            <div class="col-lg-9 col-md-8">
                                <input type="text" name="name" value="{{ old('_token') ? old('name') : $categoria->name }}" class="form-control {{ ($errors->has('name')) ? 'is-invalid' : '' }}">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="active" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('active')) ? 'text-danger' : '' }}">Active</label>
                            <div class="col-lg-9 col-md-8">
                                <input type="checkbox" name="active" value="true" {{ old('_token') ? old('active') : ($categoria->active ? 'checked' : '') }} class="bt-switch" data-size="small">
                            </div>
                        </div>
                        @component('admin.partials.locale', ['tab' => 'contenido'])
                        @foreach ($languages as $language)
                            <?php // var_dump($language);?>
                        <div role="tabpanel" class="tab-pane {{ $loop->first ? ' active' : '' }}" id="contenido-{{$language->alias}}">
                            <div class="form-group row m-t-10">
                                <label for="description" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                                    Description *
                                </label>
                                <div class="col-lg-12 col-md-12">
                                    <textarea name="locale[description.{{$language->alias}}]" class="rich" id="ckeditor.{{$language->alias}}" class="form-control">
                                        {{$locale["description.$language->alias"]}}
                                    </textarea>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endcomponent
                    </div>
                </div>
            </form>
        </div>
    </div>

@endsection
