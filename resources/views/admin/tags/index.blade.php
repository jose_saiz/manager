@php
    $route = 'tags';
    $filters_order_route = '';
@endphp

@extends('admin.layout.table_form', [
    'route' => $route, 
    'parent_section' => 'tags', 
    'name' => $tag->key, 
    'id' => $tag->id
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">Listado</h3>
        
        <div class="table-responsive m-t-10">
            
            @include('admin.partials.filters')
            
            <table id="main-table" class="table table-striped"  route="{{route($route . '_json')}}">
                <thead>
                    <tr>
                        <th data-data="id" data-name="id" data-visible="false">#</th>
                        <th data-data="group" data-name="group">Grupo</th>
                        <th data-data="key" data-name="key">Clave</th>
                        <th data-data="value" data-name="value">Valor</th>
                        <th data-orderable="false" data-defaultContent="
                            <a class='edit-button' route='{{route($route)}}'> 
                                <i class='fa fa-pencil text-inverse m-r-10'></i> 
                            </a>">
                        </th>
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection

@section('form')
    
    @isset ($tag->key)
        <div class="white-box">
            @include('admin.partials.form_title', ['route' => $route, 'name' => $tag->key])
    
            @canatleast(['edit.traducciones'])
                @include('admin.partials.crud_buttons_without_remove', ['id' => $tag->key])
            @endcanatleast
    
            @include('admin.partials.errors')
        
            <div>
                <form class='admin-form' id='tags-form' action="{{route($route .'_store')}}">
            
                    {{ csrf_field() }}
            
                    @isset ($tag->id)
                        <input type="hidden" name="group" value="{{$tag->group}}">
                        <input type="hidden" name="key" value="{{$tag->key}}">
                    @endisset
    
                    @component('admin.partials.locale')
            
                    @foreach ($idiomas as $idioma)
            
                    <div role="tabpanel" class="tab-pane {{ $loop->first ? ' active' : '' }}" id="{{$idioma->alias}}">
                        <div class="form-group row m-t-10">
                            <label for="locale" class="col-lg-3 col-md-4 col-form-label {{ ($errors->has('name')) ? 'text-danger' : '' }}">
                                Valor *
                            </label>
                            <div class="col-lg-9 col-md-8">
                            @if (isset($locale) && strlen($locale["value.$idioma->alias"]) > 300 )
                            	<textarea name="locale[value.{{$idioma->alias}}]" class="rich basic" id="ckeditor.{{$idioma->alias}}">{{$locale["value.$idioma->alias"] or ''}}</textarea>
                            @else
                                <input type="text" name="locale[value.{{$idioma->alias}}]" value='{{$locale["value.$idioma->alias"] or ''}}' class="form-control {{ ($errors->has('value')) ? 'is-invalid' : '' }}">
                            @endif
                            </div>
                        </div>
                    </div>
            
                    @endforeach
            
                    @endcomponent
            
                </form>
           </div>
        </div>
    @endisset
@endsection