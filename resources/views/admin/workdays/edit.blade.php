{{--
    |
    | *$categorias se recoge de /app/Http/ViewComposers/Admin/WorkdaysCategorias 13/05/2022_saizfact
    |    
    --}}

@php
    $route = 'admin_workdays';
@endphp

@extends('admin.layout.form', [
    'route' => $route, 
    'name' => $workday->code, 
    'id' => $workday->id
])

@section('form')

    @include('admin.partials.delete_modal', ['route' => $route, 'name' => $workday->code])
    
    <div class="white-box">
    
        {{--@include('admin.partials.form_title', ['route' => $route, 'name' => $workday->code])--}}
        @canatleast(['edit.workdays'])
            @include('admin.partials.crud_buttons', ['id' => $workday->id,])
        @endcanatleast
        @include('admin.partials.errors')
    
        <div>
            <form class='admin-form' id='workdays-form' action="{{route($route . '_store')}}">
        
                {{ csrf_field() }}
                
               
                
                @isset ($workday->id)
                    <input type="hidden" name="id" value="{{$workday->id}}">
                @endisset
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="contenido">       
                        <div class="panel-body">
                            <div class="form-group row m-t-10">
                                <label for="day" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('day')) ? 'text-danger' : '' }}">
                                    Weekday * 
                                </label>
                                <div class="col-lg-3 col-md-3">
                                    <select name="day" data-placeholder="Selecet weekday" class="form-control select2 {{ ($errors->has('day')) ? 'is-invalid' : '' }}">
                                        <option></option>
                                        @for($i=0;$i<7;$i++)
                                            <option value="{{$i}}" {{$workday->day == $i ? 'selected':''}} >{{ __('weekdays.'.$i) }}</option>
                                        @endfor
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="orden" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('roll_end_datetime')) ? 'text-danger' : '' }}">
                                    Begin time
                                </label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" id="begin_time" name="begin_time" value="{{ old('_token') ? old('begin_time') : ($workday->end_time==''?'00:00': $workday->begin_time) }}" 
                                           class="datetime-picker form-control {{ ($errors->has('begin_time')) ? 'is-invalid' : '' }}" data-date-format="HH:mm" data-show-meridian="false">
                                </div>
                                <div class="col-lg-1 col-md-1"></div>
                                <label for="end_time" class="col-lg-2 col-md-2 col-form-label {{ ($errors->has('end_time')) ? 'text-danger' : '' }}">
                                    End time
                                </label>
                                <div class="col-lg-3 col-md-3">
                                    <input type="text" id="end_time" name="end_time" value="{{ old('_token') ? old('end_time') :  ($workday->end_time==''?'00:00': $workday->end_time)}}" 
                                           class="datetime-picker form-control {{ ($errors->has('end_time')) ? 'is-invalid' : '' }}" data-date-format="HH:mm" data-show-meridian="false">
                                </div>
                            </div>
                        </div><!-- END <div class="panel-body"> -->
                    </div> <!-- END <div role="tabpanel" class="tab-pane active" id="contenido"> -->
                </div><!--END <div class="tab-content"> -->
            </form>
       </div>
    </div>

@endsection
