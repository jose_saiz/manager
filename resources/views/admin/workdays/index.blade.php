@php
    $route = 'admin_workdays';
    $filters_order_route = '';
@endphp

@canatleast(['edit.workdays'])
    @php
        $filters_order_route = 'workdays_reorder';
    @endphp
@endcanatleast

@extends('admin.layout.table', [
    'route' => $route,
])

@section('table')

    <div class="white-box">
        <h3 class="box-title">List</h3>
        
        @canatleast(['edit.workdays'])
            @include('admin.partials.create_button', ['route' => $route .'_create','language'=>'en'])
            @include('admin.partials.modal_change_roll', ['route' => $route .'_modal_change_roll','language'=>'en'])
            @include('admin.partials.modal_change_available', ['route' => $route .'_modal_change_avialable','language'=>'en'])
            <!-- @include('admin.partials.modal_api_create', ['route' => $route .'_modal_api_create'])-->
        @endcanatleast
        
        <div class="table-responsive m-t-10">
            
            @include('admin.partials.filters', [
                'order_route' => $filters_order_route,
                'order_visible' => ($filters_order_route ? 'visible' : ''),
                'filter_placeholder' => 'Filtrar por Categoría',
            ])
            
            <table id="main-table" class="table table-striped"  route="{{route($route.'_json')}}">
                <thead>
                    <tr>
                        <th data-width="1px" data-orderable="false" data-orderable="false" data-defaultContent="
                            <a class='edit-button' route='{{route($route)}}'> 
                                <i class='fa fa-pencil text-inverse m-r-10'></i> 
                            </a>"
                        >edit &nbsp;&nbsp;&nbsp;</th>
                        <th data-data="day" data-name="day" data-width="1px"  data-defaultContent="" data-orderable="false"  data-searchable="false">Weekday</th>
                        <th data-data="begin_time" data-name="begin_time">Begin Workday</th>
                        <th data-data="end_time" data-name="end_time">End Workday</th>
                        <th data-data="category.name" data-name="category.name" id="filter-column" data-searchable="false">Type</th>
                        <th data-data="updated_at" data-name="updated_at">updated</th>
                        <th data-data="created_at" data-name="created_at">created</th>
                        
                    </tr>
                </thead>
            </table>
        </div>
    </div>

@endsection
