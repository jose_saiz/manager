@lang('test.ficha',['es'=>'Ficha de Prueba','en'=>'Test Card'])
<br>


    Id: {{$test->id}} <br>
    Nombre: {{$test->nombre}} <br>
    Orden: {{$test->orden}} <br>
    Activo: {{$test->activo}} <br>
    Título: {{$test_contents['titulo']}} <br><br>
    Descripción: {!!$test_contents['descripcion']!!} <br>
    
    SEO Descripcion: {{$seo_test_contents['description']}}<br>
    SEO Keywords: {{$seo_test_contents['keywords']}}<br><br>
    
    Categoría:<br>
    Título: {{$test_categoria_contents['titulo']}} <br>
    SEO Descripcion: {{$seo_test_categoria_contents['description']}}<br>
    SEO Keywords: {{$seo_test_categoria_contents['keywords']}}<br>

