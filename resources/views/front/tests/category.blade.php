@php

@endphp

@lang('test.lista-por-categorias',['es'=>'Lista de Prueba por Categorías','en'=>'Category Test List'])
<br><br>

    Título: {{$test_categoria_contents['titulo']}} <br>
    SEO Descripcion: {{$tests_locale_slug_seo_categoria_contents['description']}}<br>
    SEO Keywords: {{$tests_locale_slug_seo_categoria_contents['keywords']}}<br>

<br><br>

@foreach ($tests as $test)
    <a href="{{route('test',[Str::slug($test_categoria_contents['titulo']),Str::slug($tests_contents[$test->id]['titulo'])])}}">
    {{$tests_contents[$test->id]['titulo']}}
    </a>
    <br>
@endforeach
