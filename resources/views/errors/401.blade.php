@extends('errors.layout')

@section('title', 'Unauthorized access')

@section('message', 'You do not have access to this page.')
