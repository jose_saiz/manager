@extends('errors.layout')

@section('title', 'Error')

@section('message', 'A problem is keeping the server busy and we can't show you the page.')
