@extends('errors.layout')

@section('title', 'Service not available')

@section('message', 'We will be back soon.')
