@extends('errors.layout')

@section('title', 'Error')

@section('message', 'Too many connections to the server, please wait a few minutes.')
