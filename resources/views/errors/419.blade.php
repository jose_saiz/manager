@extends('errors.layout')

@section('title', 'Expired page')

@section('message')
    The page you want to view has expired due to inactivity.
    <br/><br/>
    Please upload it again.
@stop
