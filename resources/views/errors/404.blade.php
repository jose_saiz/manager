@extends('front.layout.master', ['code' => 404])

@section('content')
    <div class="error-404 container">
        <div class="error-404-header">
            <h3>404</h3>
            <h2>@lang('paginas-error.404-titulo', ['es' => '¡Uuuuuuups!<br/>Something has not gone well.'])</h2>
        </div>
        <div class="error-404-content">
            <p>
                @lang('paginas-error.404-texto-1', ['es' => 'No te preocupes, ¡todo tiene solución!<br>Para regresar a la página principal, haz clic'])
                <a href="/">@lang('paginas-error.404-texto-2', ['es' => 'aquí'])</a>
                @lang('paginas-error.404-texto-3', ['es' => 'o encuentra lo que estabas buscando con nuestro'])
                <a href="#" class="open-modal" data-modal-open-class="modal-menu-search">@lang('paginas-error.404-texto-4', ['es' => 'buscador'])</a>.
            </p>
            {{--
            <p>@lang('paginas-error.404-texto-5', ['es' => 'Si lo prefieres también puedes buscar entre nuestras categorías principales.'])</p>
            --}}
        </div>
    </div>


    @include('front.partials.formulacion.featured_products')
 
@endsection
