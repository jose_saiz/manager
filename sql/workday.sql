/*
Navicat MySQL Data Transfer

Source Server         : localhostMariaDB
Source Server Version : 50505
Source Host           : localhost:3307
Source Database       : greeny-manager

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-06-02 14:08:12
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for workday
-- ----------------------------
DROP TABLE IF EXISTS `workday`;
CREATE TABLE `workday` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `neighborhood_factory_id` int(11) DEFAULT NULL,
  `day` int(11) DEFAULT NULL,
  `begin_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `active` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of workday
-- ----------------------------
INSERT INTO `workday` VALUES ('1', '1', '0', '08:00:00', '17:00:00', '3', '1', '2022-05-16 15:01:00', '2022-05-18 14:48:11');
INSERT INTO `workday` VALUES ('2', '1', '1', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('3', '1', '2', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('4', '1', '3', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('5', '1', '4', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('6', '1', '5', '09:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('7', '1', '6', '08:00:00', '20:00:00', '3', '1', '2022-05-16 15:01:00', '2022-05-17 19:47:13');
INSERT INTO `workday` VALUES ('8', '3', '0', '08:00:00', '17:00:00', '3', '1', '2022-05-16 15:01:00', '2022-05-17 13:00:48');
INSERT INTO `workday` VALUES ('9', '3', '1', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('10', '3', '2', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('11', '3', '3', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('12', '3', '4', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('13', '3', '5', '09:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('14', '3', '6', '08:00:00', '20:00:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('16', '6', '0', '08:00:00', '17:00:00', '3', '1', '2022-05-16 15:01:00', '2022-05-18 14:48:11');
INSERT INTO `workday` VALUES ('17', '6', '1', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('18', '6', '2', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('19', '6', '3', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('20', '6', '4', '08:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('21', '6', '5', '09:00:00', '21:30:00', '3', '1', '2022-05-16 15:01:00', '2022-05-16 15:02:33');
INSERT INTO `workday` VALUES ('22', '6', '6', '08:00:00', '20:00:00', '3', '1', '2022-05-16 15:01:00', '2022-05-17 19:47:13');
