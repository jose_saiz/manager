/*
Navicat MySQL Data Transfer

Source Server         : localhostMariaDB
Source Server Version : 50505
Source Host           : localhost:3307
Source Database       : greeny-manager

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-01-25 12:36:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES ('1', '2014_10_12_000000_create_users_table', '1');
INSERT INTO `migrations` VALUES ('2', '2014_10_12_100000_create_password_resets_table', '1');
INSERT INTO `migrations` VALUES ('3', '2015_01_20_084450_create_roles_table', '1');
INSERT INTO `migrations` VALUES ('4', '2015_01_20_084525_create_role_user_table', '1');
INSERT INTO `migrations` VALUES ('5', '2015_01_24_080208_create_permissions_table', '1');
INSERT INTO `migrations` VALUES ('6', '2015_01_24_080433_create_permission_role_table', '1');
INSERT INTO `migrations` VALUES ('7', '2015_12_04_003040_add_special_role_column', '1');
INSERT INTO `migrations` VALUES ('8', '2017_10_17_170735_create_permission_user_table', '1');

-- ----------------------------
-- Table structure for t_config
-- ----------------------------
DROP TABLE IF EXISTS `t_config`;
CREATE TABLE `t_config` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `dominio` varchar(64) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nombre` varchar(150) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `clave` varchar(64) COLLATE utf8mb3_unicode_ci NOT NULL,
  `valor` varchar(1024) COLLATE utf8mb3_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of t_config
-- ----------------------------
INSERT INTO `t_config` VALUES ('6', '', 'Revisión de estáticos', 'revision', '1.0.4', '2019-01-09 13:07:16', '2019-03-26 12:50:06');
INSERT INTO `t_config` VALUES ('7', null, 'Precio Máximo', 'precio-maximo', '5000000', '2019-09-10 11:37:12', '2019-09-10 11:37:12');
INSERT INTO `t_config` VALUES ('8', null, 'Precio Mínimo', 'precio-minimo', '5000', '2019-09-10 11:37:56', '2019-09-10 11:37:56');
INSERT INTO `t_config` VALUES ('9', null, 'Contacto To', 'mail.contacto.to', 'jose@easydevel.com', '2019-09-11 08:33:31', '2019-09-11 10:13:27');
INSERT INTO `t_config` VALUES ('10', null, 'Contacto From', 'mail.contacto.from', 'info@ulyses.com', '2019-09-11 10:12:52', '2019-09-11 10:12:52');

-- ----------------------------
-- Table structure for t_contacto_form
-- ----------------------------
DROP TABLE IF EXISTS `t_contacto_form`;
CREATE TABLE `t_contacto_form` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `dominio` varchar(64) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `nombre` varchar(128) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `apellidos` varchar(128) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `telefono` varchar(128) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `email` varchar(128) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `mensaje` text COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=194 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of t_contacto_form
-- ----------------------------
INSERT INTO `t_contacto_form` VALUES ('109', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 09:59:53', '2019-09-11 09:59:53');
INSERT INTO `t_contacto_form` VALUES ('110', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 10:22:08', '2019-09-11 10:22:08');
INSERT INTO `t_contacto_form` VALUES ('111', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 10:24:03', '2019-09-11 10:24:03');
INSERT INTO `t_contacto_form` VALUES ('112', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 10:26:24', '2019-09-11 10:26:24');
INSERT INTO `t_contacto_form` VALUES ('113', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 10:27:41', '2019-09-11 10:27:41');
INSERT INTO `t_contacto_form` VALUES ('114', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 10:28:04', '2019-09-11 10:28:04');
INSERT INTO `t_contacto_form` VALUES ('115', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 10:55:53', '2019-09-11 10:55:53');
INSERT INTO `t_contacto_form` VALUES ('116', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 10:58:57', '2019-09-11 10:58:57');
INSERT INTO `t_contacto_form` VALUES ('117', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 11:12:01', '2019-09-11 11:12:01');
INSERT INTO `t_contacto_form` VALUES ('118', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 11:15:35', '2019-09-11 11:15:35');
INSERT INTO `t_contacto_form` VALUES ('119', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 11:17:53', '2019-09-11 11:17:53');
INSERT INTO `t_contacto_form` VALUES ('120', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 11:34:06', '2019-09-11 11:34:06');
INSERT INTO `t_contacto_form` VALUES ('121', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 11:47:45', '2019-09-11 11:47:45');
INSERT INTO `t_contacto_form` VALUES ('122', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 11:48:15', '2019-09-11 11:48:15');
INSERT INTO `t_contacto_form` VALUES ('123', null, null, null, null, 'jose@easydevel.com', null, '2019-09-11 11:52:36', '2019-09-11 11:52:36');
INSERT INTO `t_contacto_form` VALUES ('124', null, 'Jose', 'Saiz', null, 'jose@easydevel.com', 'cccccccccccccccc cccccccccccccccccccccc', '2019-09-11 13:17:39', '2019-09-11 13:17:39');
INSERT INTO `t_contacto_form` VALUES ('125', null, 'Jose', 'Saiz', null, 'jose@easydevel.com', 'cccccccccccccccc cccccccccccccccccccccc', '2019-09-11 14:44:41', '2019-09-11 14:44:41');
INSERT INTO `t_contacto_form` VALUES ('126', null, 'Jose', 'Saiz', null, 'jose@easydevel.com', 'dddddddddddddddddddd ddddddddddddddddddddddddd ddddddddddddddddd', '2019-09-11 14:45:29', '2019-09-11 14:45:29');
INSERT INTO `t_contacto_form` VALUES ('127', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'gsdfgsdfgsdg', '2019-09-11 14:58:42', '2019-09-11 14:58:42');
INSERT INTO `t_contacto_form` VALUES ('128', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'xxxxxxxxxxxxxxxxxxxxx', '2019-09-11 15:02:49', '2019-09-11 15:02:49');
INSERT INTO `t_contacto_form` VALUES ('129', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaa', '2019-09-11 15:15:31', '2019-09-11 15:15:31');
INSERT INTO `t_contacto_form` VALUES ('130', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaaaaa', '2019-09-11 15:16:30', '2019-09-11 15:16:30');
INSERT INTO `t_contacto_form` VALUES ('131', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'mmmmmmmmmmmmmmmmmmm', '2019-09-11 15:21:13', '2019-09-11 15:21:13');
INSERT INTO `t_contacto_form` VALUES ('132', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'mmmmmmmmmmmmmmmmmmm', '2019-09-11 15:21:56', '2019-09-11 15:21:56');
INSERT INTO `t_contacto_form` VALUES ('133', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'mmmmmmmmmmmmmmmmmmm', '2019-09-11 15:23:30', '2019-09-11 15:23:30');
INSERT INTO `t_contacto_form` VALUES ('134', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'mmmmmmmmmmmmmmmmmmm', '2019-09-11 15:31:29', '2019-09-11 15:31:29');
INSERT INTO `t_contacto_form` VALUES ('135', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'nnnnnnnnnnnnnnnnnnnnnnnnnn', '2019-09-11 15:33:30', '2019-09-11 15:33:30');
INSERT INTO `t_contacto_form` VALUES ('136', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaaaaaaaaaaa', '2019-09-11 15:38:02', '2019-09-11 15:38:02');
INSERT INTO `t_contacto_form` VALUES ('137', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'vvvvvvvvvvvvvvvvvvvvvv', '2019-09-11 15:50:09', '2019-09-11 15:50:09');
INSERT INTO `t_contacto_form` VALUES ('138', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'vvvvvvvvvvvvvvvvvvvvvv', '2019-09-11 15:51:44', '2019-09-11 15:51:44');
INSERT INTO `t_contacto_form` VALUES ('139', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'vvvvvvvvvvvvvvvvvvvvvv', '2019-09-11 15:54:57', '2019-09-11 15:54:57');
INSERT INTO `t_contacto_form` VALUES ('140', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'vvvvvvvvvvvvvvvvvvvvvv', '2019-09-11 15:55:09', '2019-09-11 15:55:09');
INSERT INTO `t_contacto_form` VALUES ('141', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:02:50', '2019-09-11 16:02:50');
INSERT INTO `t_contacto_form` VALUES ('142', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:08:06', '2019-09-11 16:08:06');
INSERT INTO `t_contacto_form` VALUES ('143', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:11:57', '2019-09-11 16:11:57');
INSERT INTO `t_contacto_form` VALUES ('144', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:14:50', '2019-09-11 16:14:50');
INSERT INTO `t_contacto_form` VALUES ('145', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:15:45', '2019-09-11 16:15:45');
INSERT INTO `t_contacto_form` VALUES ('146', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:20:42', '2019-09-11 16:20:42');
INSERT INTO `t_contacto_form` VALUES ('147', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', null, '2019-09-11 16:23:54', '2019-09-11 16:23:54');
INSERT INTO `t_contacto_form` VALUES ('148', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:26:11', '2019-09-11 16:26:11');
INSERT INTO `t_contacto_form` VALUES ('149', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:31:59', '2019-09-11 16:31:59');
INSERT INTO `t_contacto_form` VALUES ('150', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:35:06', '2019-09-11 16:35:06');
INSERT INTO `t_contacto_form` VALUES ('151', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:35:10', '2019-09-11 16:35:10');
INSERT INTO `t_contacto_form` VALUES ('152', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:35:19', '2019-09-11 16:35:19');
INSERT INTO `t_contacto_form` VALUES ('153', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:35:21', '2019-09-11 16:35:21');
INSERT INTO `t_contacto_form` VALUES ('154', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:35:21', '2019-09-11 16:35:21');
INSERT INTO `t_contacto_form` VALUES ('155', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:35:22', '2019-09-11 16:35:22');
INSERT INTO `t_contacto_form` VALUES ('156', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:35:22', '2019-09-11 16:35:22');
INSERT INTO `t_contacto_form` VALUES ('157', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:35:26', '2019-09-11 16:35:26');
INSERT INTO `t_contacto_form` VALUES ('158', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:36:03', '2019-09-11 16:36:03');
INSERT INTO `t_contacto_form` VALUES ('159', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:37:55', '2019-09-11 16:37:55');
INSERT INTO `t_contacto_form` VALUES ('160', null, 'Jose', 'Saiz', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaa', '2019-09-11 16:38:32', '2019-09-11 16:38:32');
INSERT INTO `t_contacto_form` VALUES ('161', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'aaaaaaaaaaaaaaaaaaaaaaaaaa', '2019-09-11 16:38:45', '2019-09-11 16:38:45');
INSERT INTO `t_contacto_form` VALUES ('162', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'cccccccccccccccccccccccccc', '2019-09-11 16:42:27', '2019-09-11 16:42:27');
INSERT INTO `t_contacto_form` VALUES ('163', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', null, '2019-09-11 16:43:41', '2019-09-11 16:43:41');
INSERT INTO `t_contacto_form` VALUES ('164', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'xcz<cc<zc<zc', '2019-09-11 16:44:06', '2019-09-11 16:44:06');
INSERT INTO `t_contacto_form` VALUES ('165', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', '<cxc<zcx<z<zc<zc', '2019-09-11 16:46:00', '2019-09-11 16:46:00');
INSERT INTO `t_contacto_form` VALUES ('166', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', '<cxc<zcx<z<zc<zc', '2019-09-11 16:46:21', '2019-09-11 16:46:21');
INSERT INTO `t_contacto_form` VALUES ('167', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'sssssssss    s                        \r\n                   ddddddddddddddddddddddddddddddd', '2019-09-12 06:33:31', '2019-09-12 06:33:31');
INSERT INTO `t_contacto_form` VALUES ('168', null, null, null, null, null, null, '2019-09-12 10:34:23', '2019-09-12 10:34:23');
INSERT INTO `t_contacto_form` VALUES ('169', null, null, null, null, null, null, '2019-09-12 10:35:17', '2019-09-12 10:35:17');
INSERT INTO `t_contacto_form` VALUES ('170', null, null, null, null, null, null, '2019-09-12 10:40:44', '2019-09-12 10:40:44');
INSERT INTO `t_contacto_form` VALUES ('171', null, null, null, null, null, null, '2019-09-12 10:42:51', '2019-09-12 10:42:51');
INSERT INTO `t_contacto_form` VALUES ('172', null, null, null, null, null, null, '2019-09-12 10:49:28', '2019-09-12 10:49:28');
INSERT INTO `t_contacto_form` VALUES ('173', null, null, null, null, null, null, '2019-09-12 10:51:11', '2019-09-12 10:51:11');
INSERT INTO `t_contacto_form` VALUES ('174', null, 'Jose', 'Curso', null, null, null, '2019-09-12 10:51:30', '2019-09-12 10:51:30');
INSERT INTO `t_contacto_form` VALUES ('175', null, 'Jose', 'Curso', null, null, null, '2019-09-12 10:51:54', '2019-09-12 10:51:54');
INSERT INTO `t_contacto_form` VALUES ('176', null, 'Jose', 'Curso', null, null, null, '2019-09-12 10:56:58', '2019-09-12 10:56:58');
INSERT INTO `t_contacto_form` VALUES ('177', null, null, null, null, null, null, '2019-09-12 10:57:27', '2019-09-12 10:57:27');
INSERT INTO `t_contacto_form` VALUES ('178', null, null, null, null, null, null, '2019-09-12 11:01:23', '2019-09-12 11:01:23');
INSERT INTO `t_contacto_form` VALUES ('179', null, null, null, null, null, null, '2019-09-12 11:02:11', '2019-09-12 11:02:11');
INSERT INTO `t_contacto_form` VALUES ('180', null, null, null, null, null, null, '2019-09-12 11:21:50', '2019-09-12 11:21:50');
INSERT INTO `t_contacto_form` VALUES ('181', null, 'jose', null, null, null, null, '2019-09-12 11:26:12', '2019-09-12 11:26:12');
INSERT INTO `t_contacto_form` VALUES ('182', null, null, null, null, null, null, '2019-09-12 11:26:51', '2019-09-12 11:26:51');
INSERT INTO `t_contacto_form` VALUES ('183', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'asdfasf', '2019-09-12 11:28:50', '2019-09-12 11:28:50');
INSERT INTO `t_contacto_form` VALUES ('184', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'asdfasf', '2019-09-12 11:29:16', '2019-09-12 11:29:16');
INSERT INTO `t_contacto_form` VALUES ('185', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'asdfasf', '2019-09-12 11:29:25', '2019-09-12 11:29:25');
INSERT INTO `t_contacto_form` VALUES ('186', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'fadfafsa dsfafd', '2019-09-12 11:30:55', '2019-09-12 11:30:55');
INSERT INTO `t_contacto_form` VALUES ('187', null, null, null, null, null, null, '2019-09-12 11:34:41', '2019-09-12 11:34:41');
INSERT INTO `t_contacto_form` VALUES ('188', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'fadfafsa dsfafd', '2019-09-12 11:34:54', '2019-09-12 11:34:54');
INSERT INTO `t_contacto_form` VALUES ('189', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'fadfafsa dsfafd', '2019-09-12 11:34:54', '2019-09-12 11:34:54');
INSERT INTO `t_contacto_form` VALUES ('190', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'fadfafsa dsfafd', '2019-09-12 11:35:08', '2019-09-12 11:35:08');
INSERT INTO `t_contacto_form` VALUES ('191', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'fadfafsa dsfafd', '2019-09-12 11:37:17', '2019-09-12 11:37:17');
INSERT INTO `t_contacto_form` VALUES ('192', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'fadfafsa dsfafd', '2019-09-12 11:37:50', '2019-09-12 11:37:50');
INSERT INTO `t_contacto_form` VALUES ('193', null, 'Jose', 'Curso', null, 'jose+formacioncurso3333@easydevel.com', 'Hola\r\n\r\nEstoy interesado recibir más información sobre el proyecto: \r\n\r\nLocal Activo en Rentabilidad Traspaso\r\nReferencia: 666666666\r\nPrecio: 2.100.000 €\r\nFecha vigencia: 02/09/2019 - 26/09/2019 \r\n\r\nCategoria: Activos en rentabilidad\r\nTipo: Local\r\nTipo de operación: Traspaso', '2019-09-12 11:44:51', '2019-09-12 11:44:51');

-- ----------------------------
-- Table structure for t_folder
-- ----------------------------
DROP TABLE IF EXISTS `t_folder`;
CREATE TABLE `t_folder` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `anchor` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(11) NOT NULL,
  `width` smallint(6) DEFAULT NULL,
  `height` smallint(6) DEFAULT NULL,
  `document_id` varchar(32) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `t_folder_entity_id_index` (`entity_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=5689 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_folder
-- ----------------------------
INSERT INTO `t_folder` VALUES ('5656', '/0_101995_1.pdf', 'media-doc', '71', 'es', '0_101995_1.pdf', 'document/pdf', '1337411', null, null, null, '2019-08-20 10:53:20', '2019-08-20 10:53:20');
INSERT INTO `t_folder` VALUES ('5657', '/Cosmética personalizada-Crema facial pieles sensibles (1).pdf', 'media-doc', '72', 'es', 'Cosmética personalizada-Crema facial pieles sensibles (1).pdf', 'document/pdf', '83064', null, null, null, '2019-08-20 12:15:52', '2019-08-20 12:15:52');
INSERT INTO `t_folder` VALUES ('5658', '/uyeyerytre-2.txt', 'media-doc', '73', 'es', 'uyeyerytre.txt', 'document/txt', '1129395', null, null, null, '2019-08-21 10:13:19', '2019-08-21 10:13:19');
INSERT INTO `t_folder` VALUES ('5659', '/uyeyerytre-3.txt', 'media-doc', '74', 'es', 'uyeyerytre.txt', 'document/txt', '1129395', null, null, null, '2019-08-21 10:23:00', '2019-08-21 10:23:00');
INSERT INTO `t_folder` VALUES ('5660', '/uyeyerytre-4.txt', 'media-doc', '75', 'es', 'uyeyerytre.txt', 'document/txt', '1129395', null, null, null, '2019-08-21 10:32:02', '2019-08-21 10:32:02');
INSERT INTO `t_folder` VALUES ('5661', '/uyeyerytre-5.txt', 'media-doc', '78', 'es', 'uyeyerytre.txt', 'document/txt', '1129395', null, null, null, '2019-08-21 10:36:58', '2019-08-21 10:36:58');
INSERT INTO `t_folder` VALUES ('5662', '/uyeyerytre-6.txt', 'media-doc', '79', 'es', 'uyeyerytre.txt', 'document/txt', '1129395', null, null, null, '2019-08-21 10:59:59', '2019-08-21 10:59:59');
INSERT INTO `t_folder` VALUES ('5663', '/uyeyerytre-7.txt', 'media-doc', '80', 'es', 'uyeyerytre.txt', 'document/txt', '1129395', null, null, null, '2019-08-21 11:03:23', '2019-08-21 11:03:23');
INSERT INTO `t_folder` VALUES ('5664', '/uyeyerytre-2-2.txt', 'media-doc', '81', 'es', 'uyeyerytre-2.txt', 'document/txt', '1129395', null, null, null, '2019-08-21 11:10:15', '2019-08-21 11:10:15');
INSERT INTO `t_folder` VALUES ('5665', '/uyeyerytre-3-2.txt', 'media-doc', '82', 'es', 'uyeyerytre-3.txt', 'document/txt', '1129395', null, null, null, '2019-08-21 11:17:55', '2019-08-21 11:17:55');
INSERT INTO `t_folder` VALUES ('5666', '/uyeyerytre-3-2-2.txt', 'media-doc', '83', 'es', 'uyeyerytre-3-2.txt', 'document/txt', '1129395', null, null, null, '2019-08-21 11:22:35', '2019-08-21 11:22:35');
INSERT INTO `t_folder` VALUES ('5667', '/Emulsion fluida de coaltar acido salicilic.pdf', 'media-doc', '90', 'es', 'Emulsion fluida de coaltar acido salicilic.pdf', 'document/pdf', '383782', null, null, null, '2019-08-22 07:40:43', '2019-08-22 07:40:43');
INSERT INTO `t_folder` VALUES ('5668', '/Emulsion fluida de coaltar acido salicilico y clobetasol propionato.pdf', 'media-doc', '91', 'es', 'Emulsion fluida de coaltar acido salicilico y clobetasol propionato.pdf', 'document/pdf', '383782', null, null, null, '2019-08-22 07:41:12', '2019-08-22 07:41:12');
INSERT INTO `t_folder` VALUES ('5671', '/Cosmetica personalizada-Crema facial pieles sensibles.pdf', 'media-doc', '95', 'es', 'Cosmetica personalizada-Crema facial pieles sensibles.pdf', 'document/pdf', '83064', null, null, null, '2019-08-22 11:04:06', '2019-08-22 11:04:06');
INSERT INTO `t_folder` VALUES ('5672', '/Crema facial despigmentante.pdf', 'media-doc', '98', 'es', 'Crema facial despigmentante.pdf', 'document/pdf', '113293', null, null, null, '2019-08-22 11:23:13', '2019-08-22 11:23:13');
INSERT INTO `t_folder` VALUES ('5673', '/Crema facial despigmentante-2.pdf', 'media-doc', '99', 'es', 'Crema facial despigmentante.pdf', 'document/pdf', '113293', null, null, null, '2019-08-22 11:24:06', '2019-08-22 11:24:06');
INSERT INTO `t_folder` VALUES ('5674', '/Crema facial despigmentante-3.pdf', 'media-doc', '100', 'es', 'Crema facial despigmentante.pdf', 'document/pdf', '113293', null, null, null, '2019-08-22 11:24:43', '2019-08-22 11:24:43');
INSERT INTO `t_folder` VALUES ('5675', '/Crema facial despigmentante-4.pdf', 'media-doc', '101', 'es', 'Crema facial despigmentante.pdf', 'document/pdf', '113293', null, null, null, '2019-08-22 11:26:32', '2019-08-22 11:26:32');
INSERT INTO `t_folder` VALUES ('5676', '/Crema facial despigmentante-5.pdf', 'media-doc', '102', 'es', 'Crema facial despigmentante.pdf', 'document/pdf', '113293', null, null, null, '2019-08-22 11:27:40', '2019-08-22 11:27:40');
INSERT INTO `t_folder` VALUES ('5677', '/emulsiones_fluida_de_coaltar_acido_salicilico_y_clobetasol_propionato.pdf', 'media-doc', '103', 'es', 'emulsiones_fluida_de_coaltar_acido_salicilico_y_clobetasol_propionato.pdf', 'document/pdf', '383782', null, null, null, '2019-08-22 12:15:35', '2019-08-22 12:15:35');
INSERT INTO `t_folder` VALUES ('5678', '/emulsiones_fluida_de_coaltar_acido_salicilic-2.pdf', 'media-doc', '104', 'es', 'emulsiones_fluida_de_coaltar_acido_salicilic.pdf', 'document/pdf', '383782', null, null, null, '2019-08-22 12:18:36', '2019-08-22 12:18:36');
INSERT INTO `t_folder` VALUES ('5681', '/Odontologia-Gel de peroxido de carbamida.pdf', 'media-doc', '110', 'es', 'Odontologia-Gel de peroxido de carbamida.pdf', 'document/pdf', '113672', null, null, null, '2019-09-03 10:14:05', '2019-09-03 10:14:05');
INSERT INTO `t_folder` VALUES ('5682', '/Introduccion_odontologíia-3.pdf', 'media-doc', '111', 'es', 'Introduccion_odontologíia.pdf', 'document/pdf', '87543', null, null, null, '2019-09-03 10:17:12', '2019-09-03 10:17:12');
INSERT INTO `t_folder` VALUES ('5683', '/Emulsion fluida de coaltar acido salicilico.pdf', 'media-doc', '117', 'es', 'Emulsion fluida de coaltar acido salicilico.pdf', 'document/pdf', '383782', null, null, null, '2019-09-09 08:48:12', '2019-09-09 08:48:12');
INSERT INTO `t_folder` VALUES ('5684', '/Cosmetica personalizada-Crema facial pieles sensibles-2.pdf', 'media-doc', '118', 'es', 'Cosmetica personalizada-Crema facial pieles sensibles.pdf', 'document/pdf', '83064', null, null, null, '2019-09-09 08:48:40', '2019-09-09 08:48:40');
INSERT INTO `t_folder` VALUES ('5685', '/EXT. SECO GUARANA TIT.CAFEINA 10 Origen Farmalabor S1903052 .pdf', 'media-doc', '119', 'es', 'EXT. SECO GUARANA TIT.CAFEINA 10 Origen Farmalabor S1903052 .pdf', 'document/pdf', '960976', null, null, null, '2019-09-09 08:49:05', '2019-09-09 08:49:05');
INSERT INTO `t_folder` VALUES ('5686', '/Cosmética personalizada-Crema facial pieles sensibles (1)-2.pdf', 'media-doc', '120', 'es', 'Cosmética personalizada-Crema facial pieles sensibles (1).pdf', 'document/pdf', '83064', null, null, null, '2019-09-09 09:44:34', '2019-09-09 09:44:34');
INSERT INTO `t_folder` VALUES ('5687', '/ACOFARMA-Funcionamiento programa de puntos.pdf', 'media-doc', '121', 'es', 'ACOFARMA-Funcionamiento programa de puntos.pdf', 'document/pdf', '301865', null, null, null, '2019-09-09 09:46:33', '2019-09-09 09:46:33');
INSERT INTO `t_folder` VALUES ('5688', '/LECCION2_Recursividad.pdf', 'media-doc', '127', 'es', 'LECCION2_Recursividad.pdf', 'document/pdf', '119677', null, null, null, '2022-01-14 11:24:48', '2022-01-14 11:24:48');

-- ----------------------------
-- Table structure for t_imagen
-- ----------------------------
DROP TABLE IF EXISTS `t_imagen`;
CREATE TABLE `t_imagen` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `entity_id` int(10) unsigned NOT NULL,
  `rel_profile` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `grid` varchar(11) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `size` int(11) NOT NULL,
  `width` smallint(6) DEFAULT NULL,
  `height` smallint(6) DEFAULT NULL,
  `quality` int(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `t_folder_entity_id_index` (`entity_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_imagen
-- ----------------------------
INSERT INTO `t_imagen` VALUES ('133', 'thumbnail/escrudo-ferrari.png', 'media', '66', 'es', 'escrudo ferrari.png', 'image/png', 'thumbnail', '45022', '200', '200', '100', '2019-08-20 09:00:19', '2019-08-20 09:00:19');
INSERT INTO `t_imagen` VALUES ('134', 'preview/escrudo-ferrari.png', 'media', '66', 'es', 'escrudo ferrari.png', 'image/png', 'preview', '102727', '355', '315', '100', '2019-08-20 09:00:19', '2019-08-20 09:00:19');
INSERT INTO `t_imagen` VALUES ('135', 'imagenes/escrudo-ferrari.png', 'media', '66', 'es', 'escrudo ferrari.png', 'image/png', 'original', '21017', null, null, '100', '2019-08-20 09:00:19', '2019-08-20 09:00:19');
INSERT INTO `t_imagen` VALUES ('136', 'imagenes/escrudo ferrari-2.png', 'media', '66', 'es', 'escrudo ferrari.png', 'image/png', 'parrilla', '45022', '240', '200', '100', '2019-08-20 09:00:19', '2019-08-20 09:00:19');
INSERT INTO `t_imagen` VALUES ('137', 'thumbnail/Ferrari-text-logo-1920x1080.png', 'media', '67', 'es', 'Ferrari-text-logo-1920x1080.png', 'image/png', 'thumbnail', '6456', '200', '200', '100', '2019-08-20 09:23:03', '2019-08-20 09:23:03');
INSERT INTO `t_imagen` VALUES ('138', 'preview/Ferrari-text-logo-1920x1080.png', 'media', '67', 'es', 'Ferrari-text-logo-1920x1080.png', 'image/png', 'preview', '9531', '355', '315', '100', '2019-08-20 09:23:04', '2019-08-20 09:23:04');
INSERT INTO `t_imagen` VALUES ('139', 'imagenes/Ferrari-text-logo-1920x1080.png', 'media', '67', 'es', 'Ferrari-text-logo-1920x1080.png', 'image/png', 'original', '34377', null, null, '100', '2019-08-20 09:23:04', '2019-08-20 09:23:04');
INSERT INTO `t_imagen` VALUES ('140', 'imagenes/Ferrari-text-logo-1920x1080-2.png', 'media', '67', 'es', 'Ferrari-text-logo-1920x1080.png', 'image/png', 'parrilla', '6456', '240', '200', '100', '2019-08-20 09:23:05', '2019-08-20 09:23:05');
INSERT INTO `t_imagen` VALUES ('141', 'thumbnail/Arco-trobada.jpeg', 'media', '68', 'es', 'Arco trobada.jpeg', 'image/jpeg', 'thumbnail', '34212', '200', '200', '100', '2019-08-20 10:01:34', '2019-08-20 10:01:34');
INSERT INTO `t_imagen` VALUES ('142', 'preview/Arco-trobada.jpeg', 'media', '68', 'es', 'Arco trobada.jpeg', 'image/jpeg', 'preview', '75586', '355', '315', '100', '2019-08-20 10:01:35', '2019-08-20 10:01:35');
INSERT INTO `t_imagen` VALUES ('143', 'imagenes/Arco-trobada.jpeg', 'media', '68', 'es', 'Arco trobada.jpeg', 'image/jpeg', 'original', '369534', null, null, '100', '2019-08-20 10:01:35', '2019-08-20 10:01:35');
INSERT INTO `t_imagen` VALUES ('144', 'imagenes/Arco trobada-2.jpeg', 'media', '68', 'es', 'Arco trobada.jpeg', 'image/jpeg', 'parrilla', '34212', '240', '200', '100', '2019-08-20 10:01:35', '2019-08-20 10:01:35');
INSERT INTO `t_imagen` VALUES ('169', 'thumbnail/oficina4.jpeg', 'media', '105', 'es', 'oficina4.jpeg', 'image/jpeg', 'thumbnail', '183236', '698', '500', '100', '2019-09-03 08:59:06', '2019-09-03 08:59:06');
INSERT INTO `t_imagen` VALUES ('170', 'preview/oficina4.jpeg', 'media', '105', 'es', 'oficina4.jpeg', 'image/jpeg', 'preview', '904118', '1920', '1280', '100', '2019-09-03 08:59:08', '2019-09-03 08:59:08');
INSERT INTO `t_imagen` VALUES ('171', 'imagenes/oficina4.jpeg', 'media', '105', 'es', 'oficina4.jpeg', 'image/jpeg', 'original', '358424', null, null, '100', '2019-09-03 08:59:08', '2019-09-03 08:59:08');
INSERT INTO `t_imagen` VALUES ('172', 'imagenes/oficina4-2.jpeg', 'media', '105', 'es', 'oficina4.jpeg', 'image/jpeg', 'parrilla', '358424', '1000', '750', '100', '2019-09-03 08:59:08', '2019-09-03 08:59:08');
INSERT INTO `t_imagen` VALUES ('173', 'thumbnail/oficina3.jpeg', 'media', '106', 'es', 'oficina3.jpeg', 'image/jpeg', 'thumbnail', '252584', '698', '500', '100', '2019-09-03 09:22:57', '2019-09-03 09:22:57');
INSERT INTO `t_imagen` VALUES ('174', 'preview/oficina3.jpeg', 'media', '106', 'es', 'oficina3.jpeg', 'image/jpeg', 'preview', '1127043', '1920', '1280', '100', '2019-09-03 09:22:58', '2019-09-03 09:22:58');
INSERT INTO `t_imagen` VALUES ('175', 'imagenes/oficina3.jpeg', 'media', '106', 'es', 'oficina3.jpeg', 'image/jpeg', 'original', '471376', null, null, '100', '2019-09-03 09:22:58', '2019-09-03 09:22:58');
INSERT INTO `t_imagen` VALUES ('176', 'imagenes/oficina3-2.jpeg', 'media', '106', 'es', 'oficina3.jpeg', 'image/jpeg', 'parrilla', '471376', '1000', '750', '100', '2019-09-03 09:22:59', '2019-09-03 09:22:59');
INSERT INTO `t_imagen` VALUES ('177', 'thumbnail/nave.jpeg', 'media', '96', 'es', 'nave.jpeg', 'image/jpeg', 'thumbnail', '204872', '698', '500', '100', '2019-09-03 10:52:32', '2019-09-03 10:52:32');
INSERT INTO `t_imagen` VALUES ('178', 'preview/nave.jpeg', 'media', '96', 'es', 'nave.jpeg', 'image/jpeg', 'preview', '955554', '1920', '1280', '100', '2019-09-03 10:52:34', '2019-09-03 10:52:34');
INSERT INTO `t_imagen` VALUES ('179', 'imagenes/nave.jpeg', 'media', '96', 'es', 'nave.jpeg', 'image/jpeg', 'original', '382402', null, null, '100', '2019-09-03 10:52:34', '2019-09-03 10:52:34');
INSERT INTO `t_imagen` VALUES ('180', 'imagenes/nave-2.jpeg', 'media', '96', 'es', 'nave.jpeg', 'image/jpeg', 'parrilla', '382402', '1000', '750', '100', '2019-09-03 10:52:34', '2019-09-03 10:52:34');
INSERT INTO `t_imagen` VALUES ('181', 'thumbnail/nave2.jpeg', 'media', '97', 'es', 'nave2.jpeg', 'image/jpeg', 'thumbnail', '279313', '698', '500', '100', '2019-09-03 10:52:47', '2019-09-03 10:52:47');
INSERT INTO `t_imagen` VALUES ('182', 'preview/nave2.jpeg', 'media', '97', 'es', 'nave2.jpeg', 'image/jpeg', 'preview', '1251766', '1920', '1280', '100', '2019-09-03 10:52:48', '2019-09-03 10:52:48');
INSERT INTO `t_imagen` VALUES ('183', 'imagenes/nave2.jpeg', 'media', '97', 'es', 'nave2.jpeg', 'image/jpeg', 'original', '544540', null, null, '100', '2019-09-03 10:52:49', '2019-09-03 10:52:49');
INSERT INTO `t_imagen` VALUES ('184', 'imagenes/nave2-2.jpeg', 'media', '97', 'es', 'nave2.jpeg', 'image/jpeg', 'parrilla', '544540', '1000', '750', '100', '2019-09-03 10:52:49', '2019-09-03 10:52:49');
INSERT INTO `t_imagen` VALUES ('185', 'thumbnail/local.jpeg', 'media', '112', 'es', 'local.jpeg', 'image/jpeg', 'thumbnail', '286641', '698', '500', '100', '2019-09-03 10:57:11', '2019-09-03 10:57:11');
INSERT INTO `t_imagen` VALUES ('186', 'preview/local.jpeg', 'media', '112', 'es', 'local.jpeg', 'image/jpeg', 'preview', '1355094', '1920', '1280', '100', '2019-09-03 10:57:12', '2019-09-03 10:57:12');
INSERT INTO `t_imagen` VALUES ('187', 'imagenes/local.jpeg', 'media', '112', 'es', 'local.jpeg', 'image/jpeg', 'original', '580527', null, null, '100', '2019-09-03 10:57:13', '2019-09-03 10:57:13');
INSERT INTO `t_imagen` VALUES ('188', 'imagenes/local-2.jpeg', 'media', '112', 'es', 'local.jpeg', 'image/jpeg', 'parrilla', '580527', '1000', '750', '100', '2019-09-03 10:57:13', '2019-09-03 10:57:13');
INSERT INTO `t_imagen` VALUES ('189', 'thumbnail/local1.jpeg', 'media', '92', 'es', 'local1.jpeg', 'image/jpeg', 'thumbnail', '237691', '698', '500', '100', '2019-09-03 11:11:27', '2019-09-03 11:11:27');
INSERT INTO `t_imagen` VALUES ('190', 'preview/local1.jpeg', 'media', '92', 'es', 'local1.jpeg', 'image/jpeg', 'preview', '1078276', '1920', '1280', '100', '2019-09-03 11:11:28', '2019-09-03 11:11:28');
INSERT INTO `t_imagen` VALUES ('191', 'imagenes/local1.jpeg', 'media', '92', 'es', 'local1.jpeg', 'image/jpeg', 'original', '422060', null, null, '100', '2019-09-03 11:11:28', '2019-09-03 11:11:28');
INSERT INTO `t_imagen` VALUES ('192', 'imagenes/local1-2.jpeg', 'media', '92', 'es', 'local1.jpeg', 'image/jpeg', 'parrilla', '422060', '1000', '750', '100', '2019-09-03 11:11:29', '2019-09-03 11:11:29');
INSERT INTO `t_imagen` VALUES ('197', 'thumbnail/oficina3-2.jpeg', 'media', '114', 'es', 'oficina3.jpeg', 'image/jpeg', 'thumbnail', '252584', '698', '500', '100', '2019-09-05 19:00:13', '2019-09-05 19:00:13');
INSERT INTO `t_imagen` VALUES ('198', 'preview/oficina3-2.jpeg', 'media', '114', 'es', 'oficina3.jpeg', 'image/jpeg', 'preview', '1127043', '1920', '1280', '100', '2019-09-05 19:00:15', '2019-09-05 19:00:15');
INSERT INTO `t_imagen` VALUES ('199', 'imagenes/oficina3-3.jpeg', 'media', '114', 'es', 'oficina3.jpeg', 'image/jpeg', 'original', '471376', null, null, '100', '2019-09-05 19:00:15', '2019-09-05 19:00:15');
INSERT INTO `t_imagen` VALUES ('200', 'imagenes/oficina3-4.jpeg', 'media', '114', 'es', 'oficina3.jpeg', 'image/jpeg', 'parrilla', '471376', '1000', '750', '100', '2019-09-05 19:00:15', '2019-09-05 19:00:15');
INSERT INTO `t_imagen` VALUES ('201', 'thumbnail/oficina.jpeg', 'media', '115', 'es', 'oficina.jpeg', 'image/jpeg', 'thumbnail', '164323', '698', '500', '100', '2019-09-05 19:01:30', '2019-09-05 19:01:30');
INSERT INTO `t_imagen` VALUES ('202', 'preview/oficina.jpeg', 'media', '115', 'es', 'oficina.jpeg', 'image/jpeg', 'preview', '840865', '1920', '1280', '100', '2019-09-05 19:01:32', '2019-09-05 19:01:32');
INSERT INTO `t_imagen` VALUES ('203', 'imagenes/oficina.jpeg', 'media', '115', 'es', 'oficina.jpeg', 'image/jpeg', 'original', '247089', null, null, '100', '2019-09-05 19:01:32', '2019-09-05 19:01:32');
INSERT INTO `t_imagen` VALUES ('204', 'imagenes/oficina-2.jpeg', 'media', '115', 'es', 'oficina.jpeg', 'image/jpeg', 'parrilla', '247089', '1000', '750', '100', '2019-09-05 19:01:32', '2019-09-05 19:01:32');
INSERT INTO `t_imagen` VALUES ('205', 'thumbnail/oficina2.jpeg', 'media', '116', 'es', 'oficina2.jpeg', 'image/jpeg', 'thumbnail', '158349', '698', '500', '100', '2019-09-05 19:01:55', '2019-09-05 19:01:55');
INSERT INTO `t_imagen` VALUES ('206', 'preview/oficina2.jpeg', 'media', '116', 'es', 'oficina2.jpeg', 'image/jpeg', 'preview', '771260', '1920', '1280', '100', '2019-09-05 19:01:57', '2019-09-05 19:01:57');
INSERT INTO `t_imagen` VALUES ('207', 'imagenes/oficina2.jpeg', 'media', '116', 'es', 'oficina2.jpeg', 'image/jpeg', 'original', '266627', null, null, '100', '2019-09-05 19:01:57', '2019-09-05 19:01:57');
INSERT INTO `t_imagen` VALUES ('208', 'imagenes/oficina2-2.jpeg', 'media', '116', 'es', 'oficina2.jpeg', 'image/jpeg', 'parrilla', '266627', '1000', '750', '100', '2019-09-05 19:01:57', '2019-09-05 19:01:57');
INSERT INTO `t_imagen` VALUES ('211', 'thumbnail/5-Akşam-2.jpeg', 'media', '123', 'es', '5-Akşam-2.jpeg', 'image/jpeg', 'thumbnail', '334548', '698', '500', '100', '2019-09-10 11:00:36', '2019-09-10 11:00:36');
INSERT INTO `t_imagen` VALUES ('214', 'preview/5-Akşam-2.jpeg', 'media', '123', 'es', '5-Akşam-2.jpeg', 'image/jpeg', 'preview', '1458443', '1920', '1280', '100', '2019-09-10 11:00:37', '2019-09-10 11:00:37');
INSERT INTO `t_imagen` VALUES ('216', 'imagenes/5-Akşam-2.jpeg', 'media', '123', 'es', '5-Akşam-2.jpeg', 'image/jpeg', 'original', '944737', null, null, '100', '2019-09-10 11:00:38', '2019-09-10 11:00:38');
INSERT INTO `t_imagen` VALUES ('219', 'imagenes/5-Akşam-2-2.jpeg', 'media', '123', 'es', '5-Akşam-2.jpeg', 'image/jpeg', 'parrilla', '658316', '1000', '750', '100', '2019-09-10 11:00:39', '2019-09-10 11:00:39');
INSERT INTO `t_imagen` VALUES ('225', 'thumbnail/144960_68471175441651.jpeg', 'media', '126', 'es', '144960_68471175441651.jpeg', 'image/jpeg', 'thumbnail', '285082', '698', '500', '100', '2019-09-10 11:01:40', '2019-09-10 11:01:40');
INSERT INTO `t_imagen` VALUES ('226', 'preview/144960_68471175441651.jpeg', 'media', '126', 'es', '144960_68471175441651.jpeg', 'image/jpeg', 'preview', '1403886', '1920', '1280', '100', '2019-09-10 11:01:41', '2019-09-10 11:01:41');
INSERT INTO `t_imagen` VALUES ('227', 'imagenes/144960_68471175441651.jpeg', 'media', '126', 'es', '144960_68471175441651.jpeg', 'image/jpeg', 'original', '385802', null, null, '100', '2019-09-10 11:01:41', '2019-09-10 11:01:41');
INSERT INTO `t_imagen` VALUES ('228', 'imagenes/144960_68471175441651-2.jpeg', 'media', '126', 'es', '144960_68471175441651.jpeg', 'image/jpeg', 'parrilla', '564483', '1000', '750', '100', '2019-09-10 11:01:41', '2019-09-10 11:01:41');
INSERT INTO `t_imagen` VALUES ('229', 'thumbnail/greeny_logo.jpeg', 'media', '113', 'es', 'greeny_logo.jpeg', 'image/jpeg', 'thumbnail', '162637', '698', '500', '100', '2022-01-14 11:24:00', '2022-01-14 11:24:00');
INSERT INTO `t_imagen` VALUES ('230', 'preview/greeny_logo.jpeg', 'media', '113', 'es', 'greeny_logo.jpeg', 'image/jpeg', 'preview', '407466', '1920', '1280', '100', '2022-01-14 11:24:00', '2022-01-14 11:24:00');
INSERT INTO `t_imagen` VALUES ('231', 'imagenes/greeny_logo.jpeg', 'media', '113', 'es', 'greeny_logo.jpeg', 'image/jpeg', 'original', '12225', null, null, '100', '2022-01-14 11:24:00', '2022-01-14 11:24:00');
INSERT INTO `t_imagen` VALUES ('232', 'imagenes/greeny_logo-2.jpeg', 'media', '113', 'es', 'greeny_logo.jpeg', 'image/jpeg', 'parrilla', '279683', '1000', '750', '100', '2022-01-14 11:24:00', '2022-01-14 11:24:00');

-- ----------------------------
-- Table structure for t_imagen_configuracion
-- ----------------------------
DROP TABLE IF EXISTS `t_imagen_configuracion`;
CREATE TABLE `t_imagen_configuracion` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `entity_type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `directory` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `grid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content_type` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `width` smallint(6) DEFAULT NULL,
  `height` smallint(6) DEFAULT NULL,
  `quality` smallint(6) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=159 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_imagen_configuracion
-- ----------------------------
INSERT INTO `t_imagen_configuracion` VALUES ('6', 'media', 'thumbnail/', 'thumbnail', 'jpeg', '698', '500', '100', '2018-01-22 09:31:54', '2018-01-22 09:31:57');
INSERT INTO `t_imagen_configuracion` VALUES ('8', 'media', 'preview/', 'preview', 'jpeg', '1920', '1280', '100', '2018-01-22 09:42:22', '2018-01-22 09:42:25');
INSERT INTO `t_imagen_configuracion` VALUES ('9', 'producto.puntos', 'idb/producto-puntos/lg/', 'lg', 'jpg', '100', '100', '80', '2017-12-04 21:35:54', '2017-12-04 21:35:57');
INSERT INTO `t_imagen_configuracion` VALUES ('10', 'producto.puntos', 'idb/producto-puntos/md/', 'md', 'jpg', '80', '80', '100', '2017-12-04 22:05:33', '2017-12-04 22:05:36');
INSERT INTO `t_imagen_configuracion` VALUES ('11', 'producto.puntos', 'idb/producto-puntos/xs/', 'xs', 'jpg', '50', '50', '100', '2017-12-04 22:06:03', '2017-12-04 22:06:07');
INSERT INTO `t_imagen_configuracion` VALUES ('12', 'producto.puntos', 'idb/producto-puntos/sm/', 'sm', 'jpg', '100', '100', '100', '2017-12-05 10:26:47', '2017-12-05 10:26:51');
INSERT INTO `t_imagen_configuracion` VALUES ('13', 'producto.puntos', 'idb/producto-puntos/preview/', 'preview', 'jpg', '300', '300', '100', '2018-01-22 10:41:44', '2018-01-22 10:41:47');
INSERT INTO `t_imagen_configuracion` VALUES ('14', 'slide.header', 'idb/slide/', 'preview', 'jpg', '900', '300', '100', '2018-06-05 16:18:28', '2018-06-05 16:18:32');
INSERT INTO `t_imagen_configuracion` VALUES ('15', 'slide.header', 'idb/slide/', 'thumbnail', 'jpg', '900', '300', '100', '2018-06-29 16:29:18', '2018-06-29 16:29:22');
INSERT INTO `t_imagen_configuracion` VALUES ('16', 'rotulo.sidebar.home', 'idb/rotulo/preview/', 'preview', 'jpg', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('17', 'rotulo.sidebar.home', 'idb/rotulo/thumbnail/', 'thumbnail', 'jpg/png/gif', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('18', 'media', 'imagenes/', 'original', 'jpeg', null, null, '100', '2018-01-22 09:42:22', '2018-01-22 09:42:25');
INSERT INTO `t_imagen_configuracion` VALUES ('19', 'rotulo.sidebar.formulacion', 'idb/rotulo/preview/', 'preview', 'jpg', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('20', 'rotulo.sidebar.formulacion', 'idb/rotulo/thumbnail/', 'thumbnail', 'jpg/png/gif', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('21', 'rotulo.sidebar.parafarmacia', 'idb/rotulo/preview/', 'preview', 'jpg', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('22', 'rotulo.sidebar.parafarmacia', 'idb/rotulo/thumbnail/', 'thumbnail', 'jpg/png/gif', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('23', 'rotulo.sidebar.servicios', 'idb/rotulo/preview/', 'preview', 'jpg', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('24', 'rotulo.sidebar.servicios', 'idb/rotulo/thumbnail/', 'thumbnail', 'jpg/png/gif', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('25', 'rotulo.sidebar.prensa', 'idb/rotulo/preview/', 'preview', 'jpg', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('26', 'rotulo.sidebar.prensa', 'idb/rotulo/thumbnail/', 'thumbnail', 'jpg/png/gif', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('27', 'rotulo.sidebar.empleados', 'idb/rotulo/preview/', 'preview', 'jpg', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('28', 'rotulo.sidebar.empleados', 'idb/rotulo/thumbnail/', 'thumbnail', 'jpg/png/gif', '300', '300', '100', '2018-06-12 16:18:32', '2018-06-12 12:37:15');
INSERT INTO `t_imagen_configuracion` VALUES ('33', 'vademecum.marca', 'preview/', 'preview', 'jpeg', '500', '500', '100', '2018-12-10 10:58:14', null);
INSERT INTO `t_imagen_configuracion` VALUES ('38', 'vademecum.categoria.marca', 'preview/', 'preview', 'jpeg', '500', '500', '100', '2018-12-10 10:58:15', null);
INSERT INTO `t_imagen_configuracion` VALUES ('43', 'vademecum.familia', 'preview/', 'preview', 'jpeg', '500', '500', '100', '2018-12-10 10:58:15', null);
INSERT INTO `t_imagen_configuracion` VALUES ('48', 'vademecum.categoria.conceptual', 'preview/', 'preview', 'jpeg', '500', '500', '100', '2018-12-10 10:58:16', null);
INSERT INTO `t_imagen_configuracion` VALUES ('53', 'vademecum.subcategoria.conceptual', 'preview/', 'preview', 'jpeg', '500', '500', '100', '2018-12-10 10:58:17', null);
INSERT INTO `t_imagen_configuracion` VALUES ('58', 'vademecum.producto', 'preview/', 'preview', 'jpeg', '500', '500', '100', '2018-12-10 10:58:17', null);
INSERT INTO `t_imagen_configuracion` VALUES ('59', 'letrero', 'preview/', 'preview', 'jpeg', '500', '500', '100', '2018-12-19 23:18:51', null);
INSERT INTO `t_imagen_configuracion` VALUES ('60', 'producto.familia', 'lg/', 'lg', 'jpeg', '400', '500', '80', '2017-12-04 20:35:54', '2017-12-04 20:35:57');
INSERT INTO `t_imagen_configuracion` VALUES ('61', 'producto.familia', 'sm/', 'sm', 'jpeg', '200', '250', '80', '2017-12-04 20:35:54', '2017-12-04 20:35:57');
INSERT INTO `t_imagen_configuracion` VALUES ('62', 'producto.familia', 'preview/', 'preview', 'jpeg', '400', '500', '80', '2017-12-04 20:35:54', '2017-12-04 20:35:57');
INSERT INTO `t_imagen_configuracion` VALUES ('63', 'producto.familia', 'png/', 'png', 'png', '400', '250', '80', '2017-12-04 20:35:54', '2017-12-04 20:35:57');
INSERT INTO `t_imagen_configuracion` VALUES ('64', 'vademecum.marca.logo', 'preview/', 'preview', 'jpeg', '300', '300', '100', '2018-12-23 15:12:15', null);
INSERT INTO `t_imagen_configuracion` VALUES ('65', 'vademecum.marca.banner', 'preview/', 'preview', 'jpeg', '500', '500', '100', '2018-12-23 15:12:15', null);
INSERT INTO `t_imagen_configuracion` VALUES ('66', 'vademecum.categoria.conceptual.banner', 'preview/', 'preview', 'jpeg', '500', '500', '100', '2018-12-23 16:09:16', null);
INSERT INTO `t_imagen_configuracion` VALUES ('67', 'vademecum.categoria.conceptual.banner', 'preview/', 'preview', 'jpeg', '500', '500', '100', '2018-12-24 08:53:25', null);
INSERT INTO `t_imagen_configuracion` VALUES ('68', 'vademecum.subcategoria.conceptual.banner', 'preview/', 'preview', 'jpeg', '500', '500', '100', '2018-12-24 08:53:25', null);
INSERT INTO `t_imagen_configuracion` VALUES ('69', 'letrero', 'preview/', 'png', 'png', '500', '500', '100', '2018-12-19 23:18:51', null);
INSERT INTO `t_imagen_configuracion` VALUES ('70', 'letrero', 'sm/', 'sm', 'jpeg', '300', '300', '100', '2018-12-19 23:18:51', null);
INSERT INTO `t_imagen_configuracion` VALUES ('150', 'noticia', 'preview/', 'preview', 'jpeg', '475', '300', '100', '2018-12-19 23:18:43', null);
INSERT INTO `t_imagen_configuracion` VALUES ('151', 'descarga.categoria', 'preview/', 'preview', 'jpeg', '620', '280', '100', '2018-12-19 23:18:43', null);
INSERT INTO `t_imagen_configuracion` VALUES ('152', 'curso.categoria', 'preview/', 'preview', 'jpeg', '620', '280', '100', '2018-12-19 23:18:43', null);
INSERT INTO `t_imagen_configuracion` VALUES ('153', 'media', 'imagenes/', 'parrilla', 'jpeg', '1000', '750', '100', '2018-01-22 09:42:22', '2018-01-22 09:42:25');
INSERT INTO `t_imagen_configuracion` VALUES ('154', 'noticia', 'lg/', 'lg', 'jpeg', '1440', '500', '90', '2019-01-08 11:02:26', null);
INSERT INTO `t_imagen_configuracion` VALUES ('155', 'noticia', 'md/', 'md', 'jpeg', '470', '200', '100', '2019-01-08 11:02:26', null);
INSERT INTO `t_imagen_configuracion` VALUES ('156', 'noticia', 'sm/', 'sm', 'jpeg', '955', '537', '100', '2019-01-08 11:02:26', null);
INSERT INTO `t_imagen_configuracion` VALUES ('157', 'noticia', 'xs/', 'xs', 'jpeg', '355', '190', '100', '2019-01-08 11:02:26', null);
INSERT INTO `t_imagen_configuracion` VALUES ('158', 'noticia.categoria', 'preview/', 'preview', 'jpg/png/gif', '470', '200', '100', '2019-01-08 11:02:26', null);

-- ----------------------------
-- Table structure for t_language
-- ----------------------------
DROP TABLE IF EXISTS `t_language`;
CREATE TABLE `t_language` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alias` varchar(8) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_language
-- ----------------------------
INSERT INTO `t_language` VALUES ('1', 'Español', 'es', '1', null, null);
INSERT INTO `t_language` VALUES ('2', 'Ingles', 'en', '1', null, null);

-- ----------------------------
-- Table structure for t_locale
-- ----------------------------
DROP TABLE IF EXISTS `t_locale`;
CREATE TABLE `t_locale` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rel_profile` varchar(10) COLLATE utf8mb3_bin NOT NULL,
  `rel_parent` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `rel_anchor` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `tag` varchar(255) COLLATE utf8mb3_bin NOT NULL,
  `key` int(10) unsigned DEFAULT NULL,
  `value` text COLLATE utf8mb3_bin DEFAULT NULL,
  `default` tinyint(1) NOT NULL DEFAULT 1,
  `sitemap_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `t_locale_key_index` (`key`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=57633 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_bin;

-- ----------------------------
-- Records of t_locale
-- ----------------------------
INSERT INTO `t_locale` VALUES ('57399', 'es', 'tests.categorias.form', 'titulo.es', 'titulo', '1', 0x54C3AD74756C6F2043617465676F726961205072756562612031, '1', null, '2019-08-08 10:44:31', '2019-08-08 12:46:13');
INSERT INTO `t_locale` VALUES ('57400', 'en', 'tests.categorias.form', 'titulo.en', 'titulo', '1', 0x5469746C652043617465676F727920546573742031, '1', null, '2019-08-08 10:44:31', '2019-08-08 10:52:57');
INSERT INTO `t_locale` VALUES ('57405', 'es', 'tests.form', 'titulo.es', 'titulo', '6', 0x54C3AD74756C6F205072756562612031, '1', null, '2019-08-08 11:34:48', '2019-08-13 07:07:09');
INSERT INTO `t_locale` VALUES ('57406', 'es', 'tests.form', 'descripcion.es', 'descripcion', '6', 0x3C703E4465736372696369266F61637574653B6E2070727565626120313C2F703E, '1', null, '2019-08-08 11:34:48', '2019-08-08 11:34:48');
INSERT INTO `t_locale` VALUES ('57407', 'en', 'tests.form', 'titulo.en', 'titulo', '6', 0x5469746C6520546573742031, '1', null, '2019-08-08 11:34:48', '2019-08-13 07:08:10');
INSERT INTO `t_locale` VALUES ('57408', 'en', 'tests.form', 'descripcion.en', 'descripcion', '6', 0x3C703E4465736372697074696F6E205465737420313C2F703E, '1', null, '2019-08-08 11:34:48', '2019-08-13 07:08:10');
INSERT INTO `t_locale` VALUES ('57409', 'es', 'tests.categorias.form', 'titulo.es', 'titulo', '2', 0x54C3AD74756C6F2043617465676F726961205072756562612032, '1', null, '2019-08-08 12:46:43', '2019-08-09 12:29:25');
INSERT INTO `t_locale` VALUES ('57410', 'en', 'tests.categorias.form', 'titulo.en', 'titulo', '2', 0x5469746C65204361746567726F7920546573742032, '1', null, '2019-08-08 12:46:43', '2019-08-08 12:46:43');
INSERT INTO `t_locale` VALUES ('57411', 'es', 'tests.form', 'titulo.es', 'titulo', '7', 0x54C3AD74756C6F2050727565626120312E31, '1', null, '2019-08-08 12:48:43', '2019-08-13 09:46:35');
INSERT INTO `t_locale` VALUES ('57412', 'es', 'tests.form', 'descripcion.es', 'descripcion', '7', 0x3C703E446573637269706369266F61637574653B6E2050727565626120312E313C2F703E, '1', null, '2019-08-08 12:48:43', '2019-08-08 12:48:43');
INSERT INTO `t_locale` VALUES ('57413', 'en', 'tests.form', 'titulo.en', 'titulo', '7', 0x5469746C652054657374C2A0312E31, '1', null, '2019-08-08 12:48:43', '2019-08-13 09:46:36');
INSERT INTO `t_locale` VALUES ('57414', 'en', 'tests.form', 'descripcion.en', 'descripcion', '7', 0x3C703E4465736372697074696F6E266E6273703B54657374266E6273703B312E313C2F703E, '1', null, '2019-08-08 12:48:43', '2019-08-08 12:49:17');
INSERT INTO `t_locale` VALUES ('57415', 'es', 'tests.form', 'titulo.es', 'titulo', '8', 0x54C3AD74756C6F205072756562612032, '1', null, '2019-08-08 12:50:30', '2019-08-12 12:15:00');
INSERT INTO `t_locale` VALUES ('57416', 'es', 'tests.form', 'descripcion.es', 'descripcion', '8', 0x3C703E446573637269706369266F61637574653B6E2050727565626120323C2F703E, '1', null, '2019-08-08 12:50:30', '2019-08-08 12:50:30');
INSERT INTO `t_locale` VALUES ('57417', 'en', 'tests.form', 'titulo.en', 'titulo', '8', 0x5469746C6520546573742032, '1', null, '2019-08-08 12:50:30', '2019-08-13 07:05:55');
INSERT INTO `t_locale` VALUES ('57418', 'en', 'tests.form', 'descripcion.en', 'descripcion', '8', 0x3C703E4465736372697074696F6E2054657374266E6273703B323C2F703E, '1', null, '2019-08-08 12:50:30', '2019-08-08 12:50:30');
INSERT INTO `t_locale` VALUES ('57419', 'es', 'tests.form', 'titulo.es', 'titulo', '9', 0x54C3AD74756C6F2050727565626120322E32, '1', null, '2019-08-08 12:50:31', '2019-08-12 12:09:59');
INSERT INTO `t_locale` VALUES ('57420', 'es', 'tests.form', 'descripcion.es', 'descripcion', '9', 0x3C703E446573637269706369266F61637574653B6E2050727565626120322E323C2F703E, '1', null, '2019-08-08 12:50:31', '2019-08-12 12:09:59');
INSERT INTO `t_locale` VALUES ('57421', 'en', 'tests.form', 'titulo.en', 'titulo', '9', 0x5469746C65205465737420322E32, '1', null, '2019-08-08 12:50:31', '2019-08-12 12:08:51');
INSERT INTO `t_locale` VALUES ('57422', 'en', 'tests.form', 'descripcion.en', 'descripcion', '9', 0x3C703E4465736372697074696F6E2054657374266E6273703B322E323C2F703E, '1', null, '2019-08-08 12:50:31', '2019-08-12 12:08:51');
INSERT INTO `t_locale` VALUES ('57423', 'es', 'tests.categorias.form', 'titulo.es', 'titulo', '3', 0x54C3AD74756C6F2043617465676F72C3AD61205072756562612033, '1', null, '2019-08-09 11:36:45', '2019-08-09 11:36:45');
INSERT INTO `t_locale` VALUES ('57424', 'en', 'tests.categorias.form', 'titulo.en', 'titulo', '3', 0x5469746C65202043617465676F727920546573742033, '1', null, '2019-08-09 11:36:45', '2019-08-13 07:17:27');
INSERT INTO `t_locale` VALUES ('57433', 'es', 'tests.form', 'titulo.es', 'titulo', '12', 0x54C3AD74756C6F205072756562612033, '1', null, '2019-08-13 10:01:01', '2019-08-13 10:01:01');
INSERT INTO `t_locale` VALUES ('57434', 'es', 'tests.form', 'descripcion.es', 'descripcion', '12', 0x3C703E446573637269706369266F61637574653B6E2050727565626120333C2F703E, '1', null, '2019-08-13 10:01:01', '2019-08-13 10:02:34');
INSERT INTO `t_locale` VALUES ('57435', 'en', 'tests.form', 'titulo.en', 'titulo', '12', 0x5469746C6520546573742033, '1', null, '2019-08-13 10:01:02', '2019-08-13 10:01:02');
INSERT INTO `t_locale` VALUES ('57436', 'en', 'tests.form', 'descripcion.en', 'descripcion', '12', 0x3C703E4465736372697074696F6E266E6273703B54657374266E6273703B333C2F703E, '1', null, '2019-08-13 10:01:02', '2019-08-13 10:02:35');
INSERT INTO `t_locale` VALUES ('57437', 'es', 'tests.form', 'titulo.es', 'titulo', '13', 0x54C3AD74756C6F2050727565626120332E33, '1', null, '2019-08-13 10:04:37', '2019-08-13 10:05:03');
INSERT INTO `t_locale` VALUES ('57438', 'es', 'tests.form', 'descripcion.es', 'descripcion', '13', 0x3C703E446573637269706369266F61637574653B6E2050727565626120332E333C2F703E, '1', null, '2019-08-13 10:04:37', '2019-08-13 10:04:37');
INSERT INTO `t_locale` VALUES ('57439', 'en', 'tests.form', 'titulo.en', 'titulo', '13', 0x5469746C652054657374C2A0332E33, '1', null, '2019-08-13 10:04:37', '2019-08-13 10:05:03');
INSERT INTO `t_locale` VALUES ('57440', 'en', 'tests.form', 'descripcion.en', 'descripcion', '13', 0x3C703E4465736372697074696F6E266E6273703B5465737420332E333C2F703E, '1', null, '2019-08-13 10:04:38', '2019-08-13 10:04:38');
INSERT INTO `t_locale` VALUES ('57441', 'es', 'proyectos.form', 'titulo.es', 'titulo', '13', 0x70726F796563746F20332E33, '1', null, null, '2019-09-02 09:49:33');
INSERT INTO `t_locale` VALUES ('57442', 'es', 'proyectos.categorias.form', 'titulo.es', 'titulo', '4', 0x54C3AD74756C6F2063617465676F72C3AD612070726F796563746F2034, '1', null, '2019-08-16 06:47:21', '2019-08-16 06:47:21');
INSERT INTO `t_locale` VALUES ('57443', 'en', 'proyectos.categorias.form', 'titulo.en', 'titulo', '4', 0x5469746C652063617465676F72792070726F6A6563742034, '1', null, '2019-08-16 06:47:21', '2019-08-16 06:47:21');
INSERT INTO `t_locale` VALUES ('57501', 'es', 'proyectos.corta.form', 'descripcion.es', 'descripcion', '28', 0x3C703E446573637269706369266F61637574653B6E2050726F796563746F20343C2F703E, '1', null, '2019-08-16 07:24:21', '2019-08-16 07:24:21');
INSERT INTO `t_locale` VALUES ('57522', 'es', 'proyectos.form', 'titulo.es', 'titulo', '32', 0x56697669656E646120616C7175696C65722067657374696F6E2064652076697669656E6461, '1', null, '2019-08-19 14:41:32', '2019-09-09 11:35:14');
INSERT INTO `t_locale` VALUES ('57523', 'es', 'proyectos.form', 'descripcion.corta.es', 'descripcion.corta', '32', 0x3C703E61616161616161616161616161616161616161616161613C2F703E, '1', null, '2019-08-19 14:41:32', '2019-08-19 14:41:32');
INSERT INTO `t_locale` VALUES ('57524', 'es', 'proyectos.form', 'descripcion.larga.es', 'descripcion.larga', '32', null, '1', null, '2019-08-19 14:41:32', '2019-08-19 14:41:32');
INSERT INTO `t_locale` VALUES ('57525', 'en', 'proyectos.form', 'titulo.en', 'titulo', '32', 0x486F7573696E672072656E74616C20686F7573696E67206D616E6167656D656E74, '1', null, '2019-08-19 14:41:32', '2019-09-09 11:35:14');
INSERT INTO `t_locale` VALUES ('57526', 'en', 'proyectos.form', 'descripcion.corta.en', 'descripcion.corta', '32', null, '1', null, '2019-08-19 14:41:32', '2019-08-19 14:41:32');
INSERT INTO `t_locale` VALUES ('57527', 'en', 'proyectos.form', 'descripcion.larga.en', 'descripcion.larga', '32', null, '1', null, '2019-08-19 14:41:32', '2019-08-19 14:41:32');
INSERT INTO `t_locale` VALUES ('57528', 'es', 'proyectos.categorias.form', 'titulo.es', 'titulo', '3', 0x4765737469C3B36E206465206E65676F63696F732F76697669656E6461, '1', null, '2019-08-16 06:47:21', '2019-08-29 10:44:54');
INSERT INTO `t_locale` VALUES ('57529', 'es', 'proyectos.categorias.form', 'titulo.es', 'titulo', '1', 0x41637469766F7320656E2072656E746162696C69646164, '1', null, '2019-08-21 09:58:10', '2019-08-21 09:58:10');
INSERT INTO `t_locale` VALUES ('57530', 'en', 'proyectos.categorias.form', 'titulo.en', 'titulo', '1', 0x41637469766F7320656E2072656E746162696C69646164, '1', null, '2019-08-21 09:58:10', '2019-09-05 22:29:22');
INSERT INTO `t_locale` VALUES ('57531', 'es', 'proyectos.categorias.form', 'titulo.es', 'titulo', '2', 0x496E6D6F62696C6961726961, '1', null, '2019-08-21 09:59:43', '2019-08-21 09:59:43');
INSERT INTO `t_locale` VALUES ('57532', 'en', 'proyectos.categorias.form', 'titulo.en', 'titulo', '2', 0x496E6D6F62696C6961726961, '1', null, '2019-08-21 09:59:43', '2019-09-05 22:18:37');
INSERT INTO `t_locale` VALUES ('57533', 'en', 'proyectos.categorias.form', 'titulo.en', 'titulo', '3', 0x4765737469C3B36E206465206E65676F63696F732F76697669656E6461, '1', null, '2019-08-21 10:00:59', '2019-09-05 22:28:58');
INSERT INTO `t_locale` VALUES ('57534', 'es', 'proyectos.form', 'titulo.es', 'titulo', '6', 0x4C6F63616C2041637469766F20656E2052656E746162696C6964616420547261737061736F, '1', null, '2019-08-21 10:06:41', '2019-09-03 11:03:32');
INSERT INTO `t_locale` VALUES ('57535', 'es', 'proyectos.form', 'descripcion.corta.es', 'descripcion.corta', '6', 0x3C703E446573637269706369266F61637574653B6E20436F727461266E6273703B4C6F63616C2041637469766F20656E2052656E746162696C6964616420547261737061736F3C2F703E, '1', null, '2019-08-21 10:06:41', '2019-09-03 11:03:32');
INSERT INTO `t_locale` VALUES ('57536', 'es', 'proyectos.form', 'descripcion.larga.es', 'descripcion.larga', '6', null, '1', null, '2019-08-21 10:06:41', '2019-08-21 10:06:41');
INSERT INTO `t_locale` VALUES ('57537', 'en', 'proyectos.form', 'titulo.en', 'titulo', '6', 0x4C6F63616C2041637469766520696E2050726F6669746162696C697479205472616E73666572, '1', null, '2019-08-21 10:06:41', '2019-09-09 11:36:26');
INSERT INTO `t_locale` VALUES ('57538', 'en', 'proyectos.form', 'descripcion.corta.en', 'descripcion.corta', '6', null, '1', null, '2019-08-21 10:06:42', '2019-08-21 10:06:42');
INSERT INTO `t_locale` VALUES ('57539', 'en', 'proyectos.form', 'descripcion.larga.en', 'descripcion.larga', '6', null, '1', null, '2019-08-21 10:06:42', '2019-08-21 10:06:42');
INSERT INTO `t_locale` VALUES ('57540', 'es', 'proyectos.form', 'titulo.es', 'titulo', '7', 0x54C3AD74756C6F206E61766520696E647573747269616C202061637469766F20656E2072656E746162696C69646164, '1', null, '2019-08-22 11:06:45', '2019-08-22 11:06:45');
INSERT INTO `t_locale` VALUES ('57541', 'es', 'proyectos.form', 'descripcion.corta.es', 'descripcion.corta', '7', 0x3C703E446573637269706369266F61637574653B6E203C623E636F7274613C2F623E206E61766520696E647573747269616C20266E6273703B61637469766F20656E2072656E746162696C696461643C2F703E, '1', null, '2019-08-22 11:06:45', '2019-08-22 11:22:09');
INSERT INTO `t_locale` VALUES ('57542', 'es', 'proyectos.form', 'descripcion.larga.es', 'descripcion.larga', '7', 0x3C703E446573637269706369266F61637574653B6E203C623E6C617267613C2F623E206E61766520696E647573747269616C20266E6273703B61637469766F20656E2072656E746162696C696461643C2F703E, '1', null, '2019-08-22 11:06:45', '2019-08-22 11:22:09');
INSERT INTO `t_locale` VALUES ('57543', 'en', 'proyectos.form', 'titulo.en', 'titulo', '7', 0x5469746C65206F6620696E647573747269616C206275696C64696E672061637469766520696E2070726F6669746162696C697479, '1', null, '2019-08-22 11:06:45', '2019-09-09 11:37:21');
INSERT INTO `t_locale` VALUES ('57544', 'en', 'proyectos.form', 'descripcion.corta.en', 'descripcion.corta', '7', null, '1', null, '2019-08-22 11:06:45', '2019-08-22 11:06:45');
INSERT INTO `t_locale` VALUES ('57545', 'en', 'proyectos.form', 'descripcion.larga.en', 'descripcion.larga', '7', null, '1', null, '2019-08-22 11:06:45', '2019-08-22 11:06:45');
INSERT INTO `t_locale` VALUES ('57546', 'es', 'proyectos.form', 'titulo.es', 'titulo', '33', 0x4C6F63616C2041637469766F20656E2052656E746162696C696461642056656E7461, '1', null, '2019-08-22 11:20:23', '2019-09-09 18:54:38');
INSERT INTO `t_locale` VALUES ('57547', 'es', 'proyectos.form', 'descripcion.corta.es', 'descripcion.corta', '33', 0x3C703E446573637269706369266F61637574653B6E20436F727461266E6273703B4C6F63616C2041637469766F20656E2052656E746162696C696461643C2F703E, '1', null, '2019-08-22 11:20:23', '2019-08-22 11:20:23');
INSERT INTO `t_locale` VALUES ('57548', 'es', 'proyectos.form', 'descripcion.larga.es', 'descripcion.larga', '33', 0x3C703E446573637269706369266F61637574653B6E20436F727461204C61726761204C6F63616C266E6273703B41637469766F20656E2052656E746162696C696461643C2F703E, '1', null, '2019-08-22 11:20:23', '2019-08-22 11:20:23');
INSERT INTO `t_locale` VALUES ('57549', 'en', 'proyectos.form', 'titulo.en', 'titulo', '33', 0x53616C6520416374697665204C6F63616C20696E2050726F6669746162696C697479, '1', null, '2019-08-22 11:20:23', '2019-09-09 18:54:38');
INSERT INTO `t_locale` VALUES ('57550', 'en', 'proyectos.form', 'descripcion.corta.en', 'descripcion.corta', '33', null, '1', null, '2019-08-22 11:20:23', '2019-08-22 11:20:23');
INSERT INTO `t_locale` VALUES ('57551', 'en', 'proyectos.form', 'descripcion.larga.en', 'descripcion.larga', '33', null, '1', null, '2019-08-22 11:20:23', '2019-08-22 11:20:23');
INSERT INTO `t_locale` VALUES ('57552', 'es', 'proyectos.form', 'titulo.es', 'titulo', '34', 0x626262626262626262626262626262, '1', null, '2019-08-22 11:23:53', '2019-08-22 11:23:53');
INSERT INTO `t_locale` VALUES ('57553', 'es', 'proyectos.form', 'descripcion.corta.es', 'descripcion.corta', '34', 0x3C703E626262626262626262626262626262626262623C2F703E, '1', null, '2019-08-22 11:23:53', '2019-08-22 11:23:53');
INSERT INTO `t_locale` VALUES ('57554', 'es', 'proyectos.form', 'descripcion.larga.es', 'descripcion.larga', '34', null, '1', null, '2019-08-22 11:23:53', '2019-08-22 11:23:53');
INSERT INTO `t_locale` VALUES ('57555', 'en', 'proyectos.form', 'titulo.en', 'titulo', '34', null, '1', null, '2019-08-22 11:23:53', '2019-08-22 11:23:53');
INSERT INTO `t_locale` VALUES ('57556', 'en', 'proyectos.form', 'descripcion.corta.en', 'descripcion.corta', '34', null, '1', null, '2019-08-22 11:23:53', '2019-08-22 11:23:53');
INSERT INTO `t_locale` VALUES ('57557', 'en', 'proyectos.form', 'descripcion.larga.en', 'descripcion.larga', '34', null, '1', null, '2019-08-22 11:23:53', '2019-08-22 11:23:53');
INSERT INTO `t_locale` VALUES ('57576', 'es', 'proyectos.form', 'titulo.es', 'titulo', '38', 0x4765737469C3B36E206465206E65676F63696F732076697669656E64617320616C7175696C6572206F666963696E61, '1', null, '2019-08-22 12:01:28', '2019-09-03 06:36:01');
INSERT INTO `t_locale` VALUES ('57577', 'es', 'proyectos.form', 'descripcion.corta.es', 'descripcion.corta', '38', 0x3C703E446573637269706369266F61637574653B6E20436F727461204765737469266F61637574653B6E206465206E65676F63696F732076697669656E64617320616C7175696C6572206F666963696E613C2F703E, '1', null, '2019-08-22 12:01:28', '2019-09-03 06:36:01');
INSERT INTO `t_locale` VALUES ('57578', 'es', 'proyectos.form', 'descripcion.larga.es', 'descripcion.larga', '38', 0x3C703E646464646464646464646464646464646464646464646464643C2F703E, '1', null, '2019-08-22 12:01:28', '2019-08-22 13:33:12');
INSERT INTO `t_locale` VALUES ('57579', 'en', 'proyectos.form', 'titulo.en', 'titulo', '38', 0x627573696E657373206D616E6167656D656E7420686F7573696E672072656E74616C206F6666696365, '1', null, '2019-08-22 12:01:28', '2019-09-09 11:35:50');
INSERT INTO `t_locale` VALUES ('57580', 'en', 'proyectos.form', 'descripcion.corta.en', 'descripcion.corta', '38', null, '1', null, '2019-08-22 12:01:28', '2019-08-22 12:01:28');
INSERT INTO `t_locale` VALUES ('57581', 'en', 'proyectos.form', 'descripcion.larga.en', 'descripcion.larga', '38', null, '1', null, '2019-08-22 12:01:28', '2019-08-22 12:01:28');
INSERT INTO `t_locale` VALUES ('57582', 'es', 'proyectos.form', 'tags.es', 'tags', '38', 0x4C6172676F20706C617A6F2C2055626963616369C3B36E20657863656C656E74652C204E65676F63696F73, '1', null, '2019-08-22 13:33:12', '2019-09-09 11:32:28');
INSERT INTO `t_locale` VALUES ('57583', 'en', 'proyectos.form', 'tags.en', 'tags', '38', 0x4C6F6E67207465726D2C20457863656C6C656E74206C6F636174696F6E2C20427573696E657373, '1', null, '2019-08-22 13:33:12', '2019-09-09 11:35:50');
INSERT INTO `t_locale` VALUES ('57584', 'es', 'proyectos.form', 'tags.es', 'tags', '34', null, '1', null, '2019-08-29 09:09:03', '2019-08-29 09:09:03');
INSERT INTO `t_locale` VALUES ('57585', 'en', 'proyectos.form', 'tags.en', 'tags', '34', null, '1', null, '2019-08-29 09:09:03', '2019-08-29 09:09:03');
INSERT INTO `t_locale` VALUES ('57586', 'es', 'proyectos.form', 'tags.es', 'tags', '33', 0x4C6172676F20706C617A6F2C2055626963616369C3B36E20657863656C656E74652C204E65676F63696F20636F6E736F6C696461646F2C2043C3A96E747269636F, '1', null, '2019-08-29 09:42:10', '2019-09-09 18:53:49');
INSERT INTO `t_locale` VALUES ('57587', 'en', 'proyectos.form', 'tags.en', 'tags', '33', 0x4C6F6E67207465726D2C20457863656C6C656E74206C6F636174696F6E2C20436F6E736F6C69646174656420627573696E6573732C2043656E746572, '1', null, '2019-08-29 09:42:11', '2019-09-09 18:53:49');
INSERT INTO `t_locale` VALUES ('57588', 'es', 'proyectos.form', 'titulo.es', 'titulo', '39', 0x67676767676767676767676767676767676767, '1', null, '2019-08-29 10:41:15', '2019-08-29 10:41:15');
INSERT INTO `t_locale` VALUES ('57589', 'es', 'proyectos.form', 'tags.es', 'tags', '39', null, '1', null, '2019-08-29 10:41:15', '2019-08-29 10:41:15');
INSERT INTO `t_locale` VALUES ('57590', 'es', 'proyectos.form', 'descripcion.corta.es', 'descripcion.corta', '39', 0x3C703E67676767676767676767676767676767676767676767673C2F703E, '1', null, '2019-08-29 10:41:15', '2019-08-29 10:41:15');
INSERT INTO `t_locale` VALUES ('57591', 'es', 'proyectos.form', 'descripcion.larga.es', 'descripcion.larga', '39', null, '1', null, '2019-08-29 10:41:15', '2019-08-29 10:41:15');
INSERT INTO `t_locale` VALUES ('57592', 'en', 'proyectos.form', 'titulo.en', 'titulo', '39', 0x67676767676767676767676767676767676767, '1', null, '2019-08-29 10:41:15', '2019-09-05 22:28:03');
INSERT INTO `t_locale` VALUES ('57593', 'en', 'proyectos.form', 'tags.en', 'tags', '39', null, '1', null, '2019-08-29 10:41:15', '2019-08-29 10:41:15');
INSERT INTO `t_locale` VALUES ('57594', 'en', 'proyectos.form', 'descripcion.corta.en', 'descripcion.corta', '39', null, '1', null, '2019-08-29 10:41:15', '2019-08-29 10:41:15');
INSERT INTO `t_locale` VALUES ('57595', 'en', 'proyectos.form', 'descripcion.larga.en', 'descripcion.larga', '39', null, '1', null, '2019-08-29 10:41:15', '2019-08-29 10:41:15');
INSERT INTO `t_locale` VALUES ('57596', 'es', 'proyectos.form', 'titulo.es', 'titulo', '8', 0x70726F796563746F2032, '1', null, '2019-09-02 09:40:42', '2019-09-02 09:40:42');
INSERT INTO `t_locale` VALUES ('57597', 'es', 'proyectos.form', 'tags.es', 'tags', '8', null, '1', null, '2019-09-02 09:40:42', '2019-09-02 09:40:42');
INSERT INTO `t_locale` VALUES ('57598', 'es', 'proyectos.form', 'descripcion.corta.es', 'descripcion.corta', '8', 0x3C703E446573637269706369266F61637574653B6E20436F727461266E6273703B2070726F796563746F20323C2F703E, '1', null, '2019-09-02 09:40:42', '2019-09-02 09:40:42');
INSERT INTO `t_locale` VALUES ('57599', 'es', 'proyectos.form', 'descripcion.larga.es', 'descripcion.larga', '8', null, '1', null, '2019-09-02 09:40:42', '2019-09-02 09:40:42');
INSERT INTO `t_locale` VALUES ('57600', 'en', 'proyectos.form', 'titulo.en', 'titulo', '8', 0x70726F796563746F2032, '1', null, '2019-09-02 09:40:42', '2019-09-05 22:26:18');
INSERT INTO `t_locale` VALUES ('57601', 'en', 'proyectos.form', 'tags.en', 'tags', '8', null, '1', null, '2019-09-02 09:40:42', '2019-09-02 09:40:42');
INSERT INTO `t_locale` VALUES ('57602', 'en', 'proyectos.form', 'descripcion.corta.en', 'descripcion.corta', '8', null, '1', null, '2019-09-02 09:40:42', '2019-09-02 09:40:42');
INSERT INTO `t_locale` VALUES ('57603', 'en', 'proyectos.form', 'descripcion.larga.en', 'descripcion.larga', '8', null, '1', null, '2019-09-02 09:40:42', '2019-09-02 09:40:42');
INSERT INTO `t_locale` VALUES ('57604', 'es', 'proyectos.form', 'titulo.es', 'titulo', '9', 0x70726F796563746F20322E32, '1', null, '2019-09-02 09:42:04', '2019-09-02 09:42:04');
INSERT INTO `t_locale` VALUES ('57605', 'es', 'proyectos.form', 'tags.es', 'tags', '9', null, '1', null, '2019-09-02 09:42:04', '2019-09-02 09:42:04');
INSERT INTO `t_locale` VALUES ('57606', 'es', 'proyectos.form', 'descripcion.corta.es', 'descripcion.corta', '9', 0x3C703E446573637269706369266F61637574653B6E20436F7274612070726F796563746F20322E323C2F703E, '1', null, '2019-09-02 09:42:04', '2019-09-02 09:42:04');
INSERT INTO `t_locale` VALUES ('57607', 'es', 'proyectos.form', 'descripcion.larga.es', 'descripcion.larga', '9', null, '1', null, '2019-09-02 09:42:04', '2019-09-02 09:42:04');
INSERT INTO `t_locale` VALUES ('57608', 'en', 'proyectos.form', 'titulo.en', 'titulo', '9', 0x70726F796563746F20322E32, '1', null, '2019-09-02 09:42:04', '2019-09-05 22:25:47');
INSERT INTO `t_locale` VALUES ('57609', 'en', 'proyectos.form', 'tags.en', 'tags', '9', null, '1', null, '2019-09-02 09:42:04', '2019-09-02 09:42:04');
INSERT INTO `t_locale` VALUES ('57610', 'en', 'proyectos.form', 'descripcion.corta.en', 'descripcion.corta', '9', null, '1', null, '2019-09-02 09:42:04', '2019-09-02 09:42:04');
INSERT INTO `t_locale` VALUES ('57611', 'en', 'proyectos.form', 'descripcion.larga.en', 'descripcion.larga', '9', null, '1', null, '2019-09-02 09:42:04', '2019-09-02 09:42:04');
INSERT INTO `t_locale` VALUES ('57612', 'es', 'proyectos.form', 'titulo.es', 'titulo', '12', 0x496E6D6F62696C69617269612056656E7461204F666963696E61, '1', null, '2019-09-02 09:48:47', '2019-09-03 09:24:21');
INSERT INTO `t_locale` VALUES ('57613', 'es', 'proyectos.form', 'tags.es', 'tags', '12', 0x43C3A96E747269636F2C204E7565766F2C20446573706163686F73, '1', null, '2019-09-02 09:48:47', '2019-09-09 11:26:29');
INSERT INTO `t_locale` VALUES ('57614', 'es', 'proyectos.form', 'descripcion.corta.es', 'descripcion.corta', '12', 0x3C703E446573637269706369266F61637574653B6E20436F727461266E6273703B496E6D6F62696C69617269612056656E7461204F666963696E613C2F703E, '1', null, '2019-09-02 09:48:47', '2019-09-03 09:24:21');
INSERT INTO `t_locale` VALUES ('57615', 'es', 'proyectos.form', 'descripcion.larga.es', 'descripcion.larga', '12', 0x3C703E4C6F72656D20697073756D20646F6C6F722073697420616D65742C20636F6E7365637465747572206164697069736963696E6720656C69742E2043756D20726572756D2062656174616520636F6E73657175617475722C20746F74616D2066756769742C20616C696173206675676120616C697175616D2071756F642074656D706F72612061206E6973692065737365206D61676E616D206E756C6C61207175617321204572726F72207072616573656E7469756D2C207665726F20646F6C6F72756D206C61626F72756D2E204C6F72656D20697073756D20646F6C6F722073697420616D65742C20636F6E7365637465747572206164697069736963696E6720656C69742E2043756D20726572756D2062656174616520636F6E73657175617475722C20746F74616D2066756769742E3C2F703E0A0A3C703E4C6F72656D20697073756D20646F6C6F722073697420616D65742C20636F6E7365637465747572206164697069736963696E6720656C69742E2043756D20726572756D2062656174616520636F6E73657175617475722C20746F74616D2066756769742C20616C696173206675676120616C697175616D2071756F642074656D706F72612061206E6973692065737365206D61676E616D206E756C6C61207175617321204572726F72207072616573656E7469756D2C207665726F20646F6C6F72756D206C61626F72756D2E204C6F72656D20697073756D20646F6C6F722073697420616D65742C20636F6E7365637465747572206164697069736963696E6720656C69742E2043756D20726572756D2062656174616520636F6E73657175617475722C20746F74616D2066756769742E3C2F703E, '1', null, '2019-09-02 09:48:47', '2019-09-05 20:54:11');
INSERT INTO `t_locale` VALUES ('57616', 'en', 'proyectos.form', 'titulo.en', 'titulo', '12', 0x5265616C20457374617465204F66666963652053616C65, '1', null, '2019-09-02 09:48:47', '2019-09-09 11:37:57');
INSERT INTO `t_locale` VALUES ('57617', 'en', 'proyectos.form', 'tags.en', 'tags', '12', 0x43656E7472616C2C204E65772C204F666669636573, '1', null, '2019-09-02 09:48:47', '2019-09-09 11:26:29');
INSERT INTO `t_locale` VALUES ('57618', 'en', 'proyectos.form', 'descripcion.corta.en', 'descripcion.corta', '12', 0x3C703E446573637269706369266F61637574653B6E20436F727461266E6273703B496E6D6F62696C69617269612056656E7461204F666963696E613C2F703E, '1', null, '2019-09-02 09:48:47', '2019-09-05 22:17:40');
INSERT INTO `t_locale` VALUES ('57619', 'en', 'proyectos.form', 'descripcion.larga.en', 'descripcion.larga', '12', 0x3C703E4C6F72656D20697073756D20646F6C6F722073697420616D65742C20636F6E7365637465747572206164697069736963696E6720656C69742E2043756D20726572756D2062656174616520636F6E73657175617475722C20746F74616D2066756769742C20616C696173206675676120616C697175616D2071756F642074656D706F72612061206E6973692065737365206D61676E616D206E756C6C61207175617321204572726F72207072616573656E7469756D2C207665726F20646F6C6F72756D206C61626F72756D2E204C6F72656D20697073756D20646F6C6F722073697420616D65742C20636F6E7365637465747572206164697069736963696E6720656C69742E2043756D20726572756D2062656174616520636F6E73657175617475722C20746F74616D2066756769742E3C2F703E0A0A3C703E4C6F72656D20697073756D20646F6C6F722073697420616D65742C20636F6E7365637465747572206164697069736963696E6720656C69742E2043756D20726572756D2062656174616520636F6E73657175617475722C20746F74616D2066756769742C20616C696173206675676120616C697175616D2071756F642074656D706F72612061206E6973692065737365206D61676E616D206E756C6C61207175617321204572726F72207072616573656E7469756D2C207665726F20646F6C6F72756D206C61626F72756D2E204C6F72656D20697073756D20646F6C6F722073697420616D65742C20636F6E7365637465747572206164697069736963696E6720656C69742E2043756D20726572756D2062656174616520636F6E73657175617475722C20746F74616D2066756769742E3C2F703E, '1', null, '2019-09-02 09:48:48', '2019-09-05 22:17:40');
INSERT INTO `t_locale` VALUES ('57620', 'es', 'proyectos.form', 'tags.es', 'tags', '13', 0x4E7565766F2C20416D7565626C61646F, '1', null, '2019-09-02 09:49:33', '2019-09-09 15:37:20');
INSERT INTO `t_locale` VALUES ('57621', 'es', 'proyectos.form', 'descripcion.corta.es', 'descripcion.corta', '13', 0x3C703E446573637269706369266F61637574653B6E20436F7274612070726F796563746F20332E333C2F703E, '1', null, '2019-09-02 09:49:33', '2019-09-02 09:49:33');
INSERT INTO `t_locale` VALUES ('57622', 'es', 'proyectos.form', 'descripcion.larga.es', 'descripcion.larga', '13', null, '1', null, '2019-09-02 09:49:33', '2019-09-02 09:49:33');
INSERT INTO `t_locale` VALUES ('57623', 'en', 'proyectos.form', 'titulo.en', 'titulo', '13', 0x70726F796563746F20332E33, '1', null, '2019-09-02 09:49:33', '2019-09-05 22:24:58');
INSERT INTO `t_locale` VALUES ('57624', 'en', 'proyectos.form', 'tags.en', 'tags', '13', null, '1', null, '2019-09-02 09:49:33', '2019-09-02 09:49:33');
INSERT INTO `t_locale` VALUES ('57625', 'en', 'proyectos.form', 'descripcion.corta.en', 'descripcion.corta', '13', null, '1', null, '2019-09-02 09:49:33', '2019-09-02 09:49:33');
INSERT INTO `t_locale` VALUES ('57626', 'en', 'proyectos.form', 'descripcion.larga.en', 'descripcion.larga', '13', null, '1', null, '2019-09-02 09:49:33', '2019-09-02 09:49:33');
INSERT INTO `t_locale` VALUES ('57627', 'es', 'proyectos.form', 'tags.es', 'tags', '32', 0x56697669656E6461732C20556E6966616D696C6961722C43C3A96E747269636F, '1', null, '2019-09-02 18:26:32', '2019-09-09 11:35:14');
INSERT INTO `t_locale` VALUES ('57628', 'en', 'proyectos.form', 'tags.en', 'tags', '32', 0x486F7573696E672C2053696E676C652046616D696C792C2043656E7472616C, '1', null, '2019-09-02 18:26:32', '2019-09-09 11:35:14');
INSERT INTO `t_locale` VALUES ('57629', 'es', 'proyectos.form', 'tags.es', 'tags', '7', 0x4F706F7274756E696461642C2050494D45532C204E7565766F, '1', null, '2019-09-03 09:31:30', '2019-09-09 11:27:29');
INSERT INTO `t_locale` VALUES ('57630', 'en', 'proyectos.form', 'tags.en', 'tags', '7', 0x4F70706F7274756E6974792C2050494D45532C204E6577, '1', null, '2019-09-03 09:31:31', '2019-09-09 11:27:52');
INSERT INTO `t_locale` VALUES ('57631', 'es', 'proyectos.form', 'tags.es', 'tags', '6', 0x4D756C74696E6163696F6E616C2C204F706F7274756E696461642C204E65676F63696F732C2043C3A96E747269636F, '1', null, '2019-09-03 11:01:16', '2019-09-09 18:50:18');
INSERT INTO `t_locale` VALUES ('57632', 'en', 'proyectos.form', 'tags.en', 'tags', '6', 0x4D756C74696E6174696F6E616C2C204F70706F7274756E6974792C20427573696E6573732C43656E746572, '1', null, '2019-09-03 11:01:17', '2019-09-09 18:49:04');

-- ----------------------------
-- Table structure for t_locale_seo
-- ----------------------------
DROP TABLE IF EXISTS `t_locale_seo`;
CREATE TABLE `t_locale_seo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rel_profile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subdomain` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirection` tinyint(1) DEFAULT NULL,
  `menu` int(1) DEFAULT NULL,
  `sitemap` tinyint(1) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `page_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_locale_seo
-- ----------------------------
INSERT INTO `t_locale_seo` VALUES ('1', 'Test', 'en', 'routes', 'test', '', '/{language}/test', null, null, '0', null, '1', '1', null, '2019-08-07 13:15:15', '2019-08-07 13:15:15');
INSERT INTO `t_locale_seo` VALUES ('2', 'Prueba', 'es', 'routes', 'test', null, '/{language}/prueba', null, null, '0', null, '1', '1', null, '2019-08-07 13:15:15', '2019-08-07 13:15:15');
INSERT INTO `t_locale_seo` VALUES ('3', 'Tests', 'en', 'routes', 'tests', '', '/{language}/tests', null, null, '0', null, '1', '1', null, '2019-08-07 13:15:15', '2019-08-07 13:15:15');
INSERT INTO `t_locale_seo` VALUES ('4', 'Pruebas', 'es', 'routes', 'tests', null, '/{language}/pruebas', null, null, '0', null, '1', '1', null, '2019-08-07 13:15:15', '2019-08-07 13:15:15');
INSERT INTO `t_locale_seo` VALUES ('5', 'Tests by category', 'en', 'routes', 'tests-category', null, '/{language}/tests', null, null, '0', null, '1', '1', null, '2019-08-13 14:56:35', '2019-08-13 14:56:40');
INSERT INTO `t_locale_seo` VALUES ('6', 'Pruebas por categoría', 'es', 'routes', 'pruebas-categoria', null, '/{language}/pruebas', null, null, '0', null, '1', '1', null, '2019-08-13 14:59:55', '2019-08-13 14:59:59');
INSERT INTO `t_locale_seo` VALUES ('7', 'Project', 'en', 'routes', 'project', '', '/{language}/project', null, null, '0', null, '1', '1', null, '2019-08-07 13:15:15', '2019-08-07 13:15:15');
INSERT INTO `t_locale_seo` VALUES ('8', 'Proyecto', 'es', 'routes', 'project', null, '/{language}/proyecto', null, null, '0', null, '1', '1', null, '2019-08-07 13:15:15', '2019-08-07 13:15:15');
INSERT INTO `t_locale_seo` VALUES ('9', 'Projects', 'en', 'routes', 'projects', '', '/{language}/projects', null, null, '0', null, '1', '1', null, '2019-08-07 13:15:15', '2019-08-07 13:15:15');
INSERT INTO `t_locale_seo` VALUES ('10', 'Proyectos', 'es', 'routes', 'projects', null, '/{language}/proyectos', null, null, '0', null, '1', '1', null, '2019-08-07 13:15:15', '2019-08-07 13:15:15');
INSERT INTO `t_locale_seo` VALUES ('11', 'Projects by category', 'en', 'routes', 'projects-category', null, '/{language}/projects', null, null, '0', null, '1', '1', null, '2019-08-13 14:56:35', '2019-08-13 14:56:40');
INSERT INTO `t_locale_seo` VALUES ('12', 'Proyectos por categoría', 'es', 'routes', 'proyectos-categoria', null, '/{language}/proyectos', null, null, '0', null, '1', '1', null, '2019-08-13 14:59:55', '2019-08-13 14:59:59');

-- ----------------------------
-- Table structure for t_locale_slug_seo
-- ----------------------------
DROP TABLE IF EXISTS `t_locale_slug_seo`;
CREATE TABLE `t_locale_slug_seo` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rel_profile` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `rel_parent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` int(11) DEFAULT NULL,
  `parent_slug` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keywords` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_locale_slug_seo
-- ----------------------------
INSERT INTO `t_locale_slug_seo` VALUES ('3', 'es', 'tests.categorias.form', 'titulo-categoria-prueba-2', '2', null, 'Título Categoria Prueba 2', 'SEO Keywords Categoría 2', 'SEO Descripción Categoría 2', '2019-08-09 12:01:54', '2019-08-12 12:37:26');
INSERT INTO `t_locale_slug_seo` VALUES ('4', 'en', 'tests.categorias.form', 'title-categroy-test-2', '2', null, 'Title Categroy Test 2', '', '', '2019-08-09 12:01:54', '2019-08-12 12:37:26');
INSERT INTO `t_locale_slug_seo` VALUES ('5', 'es', 'tests.categorias.form', 'titulo-categoria-prueba-3', '3', null, 'Título Categoría Prueba 3', 'SEO Keywords Categoría 3', 'SEO Descripción Categoría 3', '2019-08-12 10:39:26', '2019-08-13 09:58:39');
INSERT INTO `t_locale_slug_seo` VALUES ('6', 'en', 'tests.categorias.form', 'title-category-test-3', '3', null, 'Title  Category Test 3', 'SEO Keywords Category 3', 'SEO Description Category 3', '2019-08-12 10:39:27', '2019-08-13 09:58:39');
INSERT INTO `t_locale_slug_seo` VALUES ('9', 'es', 'tests.categorias.form', 'titulo-categoria-prueba-1', '1', null, 'Título Categoria Prueba 1', 'SEO Keywords Categoría 1', 'SEO Descripción Categoría 1', '2019-08-12 12:19:57', '2019-08-12 12:19:57');
INSERT INTO `t_locale_slug_seo` VALUES ('10', 'en', 'tests.categorias.form', 'title-category-test-1', '1', null, 'Title Category Test 1', 'SEO Keywords Category 1', 'SEO Description Category 1', '2019-08-12 12:19:57', '2019-08-12 12:19:57');
INSERT INTO `t_locale_slug_seo` VALUES ('11', 'es', 'tests.form', 'titulo-prueba-22', '9', '3', 'Título Prueba 2.2', 'SEO Keywords Categoría 2.2', 'SEO Descripción Categoría 2.2', '2019-08-13 06:48:05', '2019-08-13 09:53:00');
INSERT INTO `t_locale_slug_seo` VALUES ('12', 'en', 'tests.form', 'title-test-22', '9', '4', 'Title Test 2.2', 'SEO Keywords Category 2.2', 'SEO Description Category 2.2', '2019-08-13 06:48:05', '2019-08-13 09:53:00');
INSERT INTO `t_locale_slug_seo` VALUES ('13', 'es', 'tests.form', 'titulo-prueba-2', '8', '3', 'Título Prueba 2', 'SEO Keywords Categoría 2', 'SEO Descripción Categoría 2', '2019-08-13 06:52:35', '2019-08-13 09:54:16');
INSERT INTO `t_locale_slug_seo` VALUES ('16', 'en', 'tests.form', 'title-test-2', '8', '4', 'Title Test 2', 'SEO Keywords Category 2', 'SEO Description Category 2', '2019-08-13 06:52:35', '2019-08-13 09:54:17');
INSERT INTO `t_locale_slug_seo` VALUES ('19', 'es', 'tests.form', 'titulo-prueba-1', '6', '9', 'Título Prueba 1', 'SEO Keywords Categoría 1', 'SEO Descripción Categoría 1', '2019-08-13 07:07:09', '2019-08-13 07:11:37');
INSERT INTO `t_locale_slug_seo` VALUES ('20', 'en', 'tests.form', 'title-test-1', '6', '10', 'Title Test 1', 'SEO Keywords Category 1', 'SEO Description Category 1', '2019-08-13 07:07:09', '2019-08-13 07:11:37');
INSERT INTO `t_locale_slug_seo` VALUES ('56', 'es', 'tests.form', 'titulo-prueba-11', '7', '9', 'Título Prueba 1.1', 'SEO Keywords Categoría 1.1', 'SEO Descripción Categoría 1.1', '2019-08-13 09:20:06', '2019-08-13 09:46:35');
INSERT INTO `t_locale_slug_seo` VALUES ('57', 'en', 'tests.form', 'title-test-11', '7', '10', 'Title Test 1.1', 'SEO Keywords Category 1.1', 'SEO Description Category 1.1', '2019-08-13 09:20:06', '2019-08-13 09:46:35');
INSERT INTO `t_locale_slug_seo` VALUES ('58', 'es', 'tests.form', 'titulo-prueba-3', '12', '5', 'Título Prueba 3', 'SEO Keywords Categoría 3', 'SEO Descripción Categoría 3', '2019-08-13 10:01:01', '2019-08-13 10:02:03');
INSERT INTO `t_locale_slug_seo` VALUES ('59', 'en', 'tests.form', 'title-test-3', '12', '6', 'Title Test 3', 'SEO Keywords Category 3', 'SEO Description Category 3', '2019-08-13 10:01:01', '2019-08-13 10:02:04');
INSERT INTO `t_locale_slug_seo` VALUES ('60', 'es', 'tests.form', 'titulo-prueba-33', '13', '5', 'Título Prueba 3.3', '', '', '2019-08-13 10:04:37', '2022-01-11 20:50:45');
INSERT INTO `t_locale_slug_seo` VALUES ('61', 'en', 'tests.form', 'title-test-33', '13', '6', 'Title Test 3.3', '', '', '2019-08-13 10:04:37', '2022-01-11 20:50:45');
INSERT INTO `t_locale_slug_seo` VALUES ('62', 'es', 'proyectos.categorias.form', 'titulo-categoria-proyecto-4', '4', null, 'Título categoría proyecto 4', '', '', '2019-08-16 06:47:21', '2019-08-16 06:47:21');
INSERT INTO `t_locale_slug_seo` VALUES ('63', 'en', 'proyectos.categorias.form', 'title-category-project-4', '4', null, 'Title category project 4', '', '', '2019-08-16 06:47:21', '2019-08-16 06:47:21');
INSERT INTO `t_locale_slug_seo` VALUES ('92', 'es', 'proyectos.form', 'titulo-proyecto-4', '28', '62', 'Título Proyecto 4', '', '', '2019-08-16 07:24:21', '2019-08-16 07:24:21');
INSERT INTO `t_locale_slug_seo` VALUES ('93', 'en', 'proyectos.form', '', '28', '63', null, '', '', '2019-08-16 07:24:21', '2019-08-16 07:24:21');
INSERT INTO `t_locale_slug_seo` VALUES ('94', 'es', 'proyectos.form', 'aaaaaaaaaaaaaaaaaaaaaa', '29', '62', 'aaaaaaaaaaaaaaaaaaaaaa', '', '', '2019-08-19 10:09:26', '2019-08-19 10:09:26');
INSERT INTO `t_locale_slug_seo` VALUES ('95', 'en', 'proyectos.form', '', '29', '63', null, '', '', '2019-08-19 10:09:26', '2019-08-19 10:09:26');
INSERT INTO `t_locale_slug_seo` VALUES ('96', 'es', 'proyectos.form', 'aaaaaaaaaaaaaaaaaaaaaa', '30', '62', 'aaaaaaaaaaaaaaaaaaaaaa', '', '', '2019-08-19 13:18:53', '2019-08-19 13:18:53');
INSERT INTO `t_locale_slug_seo` VALUES ('97', 'en', 'proyectos.form', '', '30', '63', null, '', '', '2019-08-19 13:18:54', '2019-08-19 13:18:54');
INSERT INTO `t_locale_slug_seo` VALUES ('98', 'es', 'proyectos.form', 'aaaaaaaaaaaaaaaaaaaaaa', '31', '62', 'aaaaaaaaaaaaaaaaaaaaaa', '', '', '2019-08-19 13:19:35', '2019-08-19 13:19:35');
INSERT INTO `t_locale_slug_seo` VALUES ('99', 'en', 'proyectos.form', '', '31', '63', null, '', '', '2019-08-19 13:19:35', '2019-08-19 13:19:35');
INSERT INTO `t_locale_slug_seo` VALUES ('100', 'es', 'proyectos.form', 'vivienda-alquiler-gestion-de-vivienda', '32', '106', 'Vivienda alquiler gestion de vivienda', '', '', '2019-08-19 14:41:32', '2019-09-09 11:35:13');
INSERT INTO `t_locale_slug_seo` VALUES ('101', 'en', 'proyectos.form', 'housing-rental-housing-management', '32', '107', 'Housing rental housing management', '', '', '2019-08-19 14:41:32', '2019-09-09 11:35:14');
INSERT INTO `t_locale_slug_seo` VALUES ('102', 'es', 'proyectos.categorias.form', 'activos-en-rentabilidad', '1', null, 'Activos en rentabilidad', 'SEO Key Words activos en rentabilidad', 'SEO Descripcion activos en rentabilidad', '2019-08-21 09:58:10', '2019-08-21 09:58:41');
INSERT INTO `t_locale_slug_seo` VALUES ('103', 'en', 'proyectos.categorias.form', 'activos-en-rentabilidad', '1', null, 'Activos en rentabilidad', '', '', '2019-08-21 09:58:10', '2019-09-05 22:29:22');
INSERT INTO `t_locale_slug_seo` VALUES ('104', 'es', 'proyectos.categorias.form', 'inmobiliaria', '2', null, 'Inmobiliaria', 'SEO Keywords Inmobiliaria', 'SEO Descripcion Inmobiliaria', '2019-08-21 09:59:43', '2019-08-21 09:59:43');
INSERT INTO `t_locale_slug_seo` VALUES ('105', 'en', 'proyectos.categorias.form', 'inmobiliaria', '2', null, 'Inmobiliaria', '', '', '2019-08-21 09:59:43', '2019-09-05 22:18:36');
INSERT INTO `t_locale_slug_seo` VALUES ('106', 'es', 'proyectos.categorias.form', 'gestion-de-negociosvivienda', '3', null, 'Gestión de negocios/vivienda', 'SEO Keywords Gestión de negocios', 'SEO Descripción Gestión de negocios', '2019-08-21 10:00:59', '2019-08-29 10:44:53');
INSERT INTO `t_locale_slug_seo` VALUES ('107', 'en', 'proyectos.categorias.form', 'gestion-de-negociosvivienda', '3', null, 'Gestión de negocios/vivienda', '', '', '2019-08-21 10:00:59', '2019-09-05 22:28:57');
INSERT INTO `t_locale_slug_seo` VALUES ('108', 'es', 'proyectos.form', 'local-activo-en-rentabilidad-traspaso', '6', '102', 'Local Activo en Rentabilidad Traspaso', '', '', '2019-08-21 10:06:41', '2019-09-03 11:03:31');
INSERT INTO `t_locale_slug_seo` VALUES ('109', 'en', 'proyectos.form', 'local-active-in-profitability-transfer', '6', '103', 'Local Active in Profitability Transfer', '', '', '2019-08-21 10:06:41', '2019-09-09 11:36:26');
INSERT INTO `t_locale_slug_seo` VALUES ('110', 'es', 'proyectos.form', 'titulo-nave-industrial-activo-en-rentabilidad', '7', '102', 'Título nave industrial  activo en rentabilidad', '', '', '2019-08-22 11:06:45', '2019-08-22 11:06:45');
INSERT INTO `t_locale_slug_seo` VALUES ('111', 'en', 'proyectos.form', 'title-of-industrial-building-active-in-profitability', '7', '103', 'Title of industrial building active in profitability', '', '', '2019-08-22 11:06:45', '2019-09-09 11:37:21');
INSERT INTO `t_locale_slug_seo` VALUES ('112', 'es', 'proyectos.form', 'local-activo-en-rentabilidad-venta', '33', '102', 'Local Activo en Rentabilidad Venta', '', '', '2019-08-22 11:20:23', '2019-09-09 18:54:37');
INSERT INTO `t_locale_slug_seo` VALUES ('113', 'en', 'proyectos.form', 'sale-active-local-in-profitability', '33', '103', 'Sale Active Local in Profitability', '', '', '2019-08-22 11:20:23', '2019-09-09 18:54:38');
INSERT INTO `t_locale_slug_seo` VALUES ('114', 'es', 'proyectos.form', 'bbbbbbbbbbbbbbb', '34', '104', 'bbbbbbbbbbbbbbb', '', '', '2019-08-22 11:23:53', '2019-09-02 18:26:08');
INSERT INTO `t_locale_slug_seo` VALUES ('115', 'en', 'proyectos.form', '', '34', '105', null, '', '', '2019-08-22 11:23:53', '2019-09-02 18:26:08');
INSERT INTO `t_locale_slug_seo` VALUES ('116', 'es', 'proyectos.form', 'dddddddddddddddddddddddddd', '35', '62', 'dddddddddddddddddddddddddd', '', '', '2019-08-22 11:42:57', '2019-08-22 11:42:57');
INSERT INTO `t_locale_slug_seo` VALUES ('117', 'en', 'proyectos.form', '', '35', '63', null, '', '', '2019-08-22 11:42:57', '2019-08-22 11:42:57');
INSERT INTO `t_locale_slug_seo` VALUES ('118', 'es', 'proyectos.form', 'ddddddddddddddddd', '36', '62', 'ddddddddddddddddd', '', '', '2019-08-22 11:55:27', '2019-08-22 11:55:27');
INSERT INTO `t_locale_slug_seo` VALUES ('119', 'en', 'proyectos.form', '', '36', '63', null, '', '', '2019-08-22 11:55:27', '2019-08-22 11:55:27');
INSERT INTO `t_locale_slug_seo` VALUES ('120', 'es', 'proyectos.form', 'ddddddddddddddddd', '37', '62', 'ddddddddddddddddd', '', '', '2019-08-22 11:57:25', '2019-08-22 11:57:25');
INSERT INTO `t_locale_slug_seo` VALUES ('121', 'en', 'proyectos.form', '', '37', '63', null, '', '', '2019-08-22 11:57:25', '2019-08-22 11:57:25');
INSERT INTO `t_locale_slug_seo` VALUES ('122', 'es', 'proyectos.form', 'gestion-de-negocios-viviendas-alquiler-oficina', '38', '106', 'Gestión de negocios viviendas alquiler oficina', '', '', '2019-08-22 12:01:27', '2019-09-03 06:36:00');
INSERT INTO `t_locale_slug_seo` VALUES ('123', 'en', 'proyectos.form', 'business-management-housing-rental-office', '38', '107', 'business management housing rental office', '', '', '2019-08-22 12:01:27', '2019-09-09 11:35:50');
INSERT INTO `t_locale_slug_seo` VALUES ('124', 'es', 'proyectos.form', 'ggggggggggggggggggg', '39', '102', 'ggggggggggggggggggg', '', '', '2019-08-29 10:41:14', '2019-09-02 18:25:14');
INSERT INTO `t_locale_slug_seo` VALUES ('125', 'en', 'proyectos.form', 'ggggggggggggggggggg', '39', '103', 'ggggggggggggggggggg', '', '', '2019-08-29 10:41:14', '2019-09-05 22:28:02');
INSERT INTO `t_locale_slug_seo` VALUES ('126', 'es', 'proyectos.form', 'proyecto-2', '8', '104', 'proyecto 2', '', '', '2019-09-02 09:40:41', '2019-09-02 09:40:41');
INSERT INTO `t_locale_slug_seo` VALUES ('127', 'en', 'proyectos.form', 'proyecto-2', '8', '105', 'proyecto 2', '', '', '2019-09-02 09:40:42', '2019-09-05 22:26:17');
INSERT INTO `t_locale_slug_seo` VALUES ('128', 'es', 'proyectos.form', 'proyecto-22', '9', '104', 'proyecto 2.2', '', '', '2019-09-02 09:42:03', '2019-09-02 09:42:03');
INSERT INTO `t_locale_slug_seo` VALUES ('129', 'en', 'proyectos.form', 'proyecto-22', '9', '105', 'proyecto 2.2', '', '', '2019-09-02 09:42:03', '2019-09-05 22:25:47');
INSERT INTO `t_locale_slug_seo` VALUES ('130', 'es', 'proyectos.form', 'inmobiliaria-venta-oficina', '12', '104', 'Inmobiliaria Venta Oficina', '', '', '2019-09-02 09:48:46', '2019-09-03 09:24:20');
INSERT INTO `t_locale_slug_seo` VALUES ('131', 'en', 'proyectos.form', 'real-estate-office-sale', '12', '105', 'Real Estate Office Sale', '', '', '2019-09-02 09:48:47', '2019-09-09 11:37:57');
INSERT INTO `t_locale_slug_seo` VALUES ('132', 'es', 'proyectos.form', 'proyecto-33', '13', '106', 'proyecto 3.3', '', '', '2019-09-02 09:49:32', '2019-09-02 09:49:32');
INSERT INTO `t_locale_slug_seo` VALUES ('133', 'en', 'proyectos.form', 'proyecto-33', '13', '107', 'proyecto 3.3', '', '', '2019-09-02 09:49:32', '2019-09-05 22:24:57');

-- ----------------------------
-- Table structure for t_locale_tag
-- ----------------------------
DROP TABLE IF EXISTS `t_locale_tag`;
CREATE TABLE `t_locale_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rel_profile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `group` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `key` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2306 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_locale_tag
-- ----------------------------
INSERT INTO `t_locale_tag` VALUES ('1928', 'es', 'admin/\'', '$route.\'.page_title', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1929', 'en', 'admin/\'', '$route.\'.page_title', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1930', 'es', 'admin/\'', '$route.\'.parent_section', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1931', 'en', 'admin/\'', '$route.\'.parent_section', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1932', 'es', 'admin/\'', '$route.\'.subsection', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1933', 'en', 'admin/\'', '$route.\'.subsection', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1934', 'es', 'admin/\'', '$route.\'.edit', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1935', 'en', 'admin/\'', '$route.\'.edit', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1936', 'es', 'admin/\'', '$route.\'.new', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1937', 'en', 'admin/\'', '$route.\'.new', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1938', 'es', 'admin/\'', '$route.\'.modal', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1939', 'en', 'admin/\'', '$route.\'.modal', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1940', 'es', 'test', 'hola-mundo', 'Hola Mundo', '0', '2019-08-07 13:27:03', '2019-08-07 13:28:30');
INSERT INTO `t_locale_tag` VALUES ('1941', 'en', 'test', 'hola-mundo', 'Hello World', '0', '2019-08-07 13:27:03', '2019-08-07 13:28:30');
INSERT INTO `t_locale_tag` VALUES ('1942', 'es', '_json', 'Unauthorized', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1943', 'en', '_json', 'Unauthorized', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1944', 'es', '_json', 'Forbidden', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1945', 'en', '_json', 'Forbidden', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1946', 'es', '_json', 'Not Found', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1947', 'en', '_json', 'Not Found', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1948', 'es', '_json', 'Page Expired', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1949', 'en', '_json', 'Page Expired', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1950', 'es', '_json', 'Too Many Requests', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1951', 'en', '_json', 'Too Many Requests', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1952', 'es', '_json', 'Server Error', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1953', 'en', '_json', 'Server Error', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1954', 'es', '_json', 'Service Unavailable', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1955', 'en', '_json', 'Service Unavailable', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1956', 'es', '_json', 'Oh no', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1957', 'en', '_json', 'Oh no', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1958', 'es', '_json', 'Go Home', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1959', 'en', '_json', 'Go Home', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1960', 'es', '_json', 'All rights reserved.', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1961', 'en', '_json', 'All rights reserved.', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1962', 'es', '_json', 'Whoops!', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1963', 'en', '_json', 'Whoops!', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1964', 'es', '_json', 'Hello!', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1965', 'en', '_json', 'Hello!', '', '0', '2019-08-07 13:27:03', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1966', 'es', '_json', 'Regards', '', '0', '2019-08-07 13:27:04', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1967', 'en', '_json', 'Regards', '', '0', '2019-08-07 13:27:04', '2019-09-03 11:30:53');
INSERT INTO `t_locale_tag` VALUES ('1968', 'es', 'about', 'about', 'Sobre', '0', '2019-09-03 11:30:52', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('1969', 'en', 'about', 'about', 'About', '0', '2019-09-03 11:30:52', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('1970', 'es', 'contact', 'contact', null, '0', '2019-09-03 11:30:52', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1971', 'en', 'contact', 'contact', null, '0', '2019-09-03 11:30:52', '2019-09-03 11:30:52');
INSERT INTO `t_locale_tag` VALUES ('1972', 'es', 'footer', 'ulyses', 'Ulyses', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1973', 'en', 'footer', 'ulyses', 'Ulyses', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1974', 'es', 'footer', 'description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus impedit perferendis, laudantium molestiae ipsam rem veniam facere quos! Temporibus, minima culpa deleniti magnam.', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1975', 'en', 'footer', 'description', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus impedit perferendis, laudantium molestiae ipsam rem veniam facere quos! Temporibus, minima culpa deleniti magnam.', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1976', 'es', 'footer', 'leer-mas', 'Leer Más...', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1977', 'en', 'footer', 'leer-mas', 'Read More...', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1978', 'es', 'footer', 'navegacion', 'Navegación', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1979', 'en', 'footer', 'navegacion', 'Navigation', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1980', 'es', 'footer', 'inicio', 'Inicio', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1981', 'en', 'footer', 'inicio', 'Home', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1982', 'es', 'footer', 'proyectos', 'Proyectos', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1983', 'en', 'footer', 'proyectos', 'Projects', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1984', 'es', 'footer', 'about_us', 'Quienes somos', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1985', 'en', 'footer', 'about_us', 'About Us', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1986', 'es', 'footer', 'contact_us', 'Contacto', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1987', 'en', 'footer', 'contact_us', 'Contact Us', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1988', 'es', 'footer', 'categorias-proyectos', 'Categorías', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1989', 'en', 'footer', 'categorias-proyectos', 'Categories', '0', '2019-09-03 11:30:52', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('1990', 'es', 'header-footer', 'direccion', 'C. Bartomeu Ferrà, 3 - Palma (España)', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('1991', 'en', 'header-footer', 'direccion', 'C. Bartomeu Ferrà, 3 - Palma de Mallorca (Spain)', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('1992', 'es', 'header-footer', 'telefono', '(+34) 696 96 96 96', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('1993', 'en', 'header-footer', 'telefono', '(+34) 696 96 96 96', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('1994', 'es', 'header-footer', 'mail', 'info@ulyses.com', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('1995', 'en', 'header-footer', 'mail', 'info@ulyses.com', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('1996', 'es', 'header-footer', 'anyo', '2019 ', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('1997', 'en', 'header-footer', 'anyo', '2019 ', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('1998', 'es', 'header-footer', 'copyright', ' Copyright - Todos los derechos reservados.', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('1999', 'en', 'header-footer', 'copyright', ' Copyright - All Rights Reserved.', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2000', 'es', 'header-footer', 'made-with', '', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2001', 'en', 'header-footer', 'made-with', '', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2002', 'es', 'header-footer', 'by', '', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2003', 'en', 'header-footer', 'by', '', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2004', 'es', 'header-footer', 'developer', '', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2005', 'en', 'header-footer', 'developer', '', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2006', 'es', 'header', 'espanol', 'Español', '0', '2019-09-03 11:30:52', '2019-09-03 11:33:51');
INSERT INTO `t_locale_tag` VALUES ('2007', 'en', 'header', 'espanol', 'Spanish', '0', '2019-09-03 11:30:52', '2019-09-03 11:33:51');
INSERT INTO `t_locale_tag` VALUES ('2008', 'es', 'header', 'ingles', 'Ingles', '0', '2019-09-03 11:30:52', '2019-09-03 11:33:51');
INSERT INTO `t_locale_tag` VALUES ('2009', 'en', 'header', 'ingles', 'English', '0', '2019-09-03 11:30:52', '2019-09-03 11:33:51');
INSERT INTO `t_locale_tag` VALUES ('2010', 'es', 'header', 'proyectos', 'proyectos', '0', '2019-09-03 11:30:52', '2019-09-03 11:33:51');
INSERT INTO `t_locale_tag` VALUES ('2011', 'en', 'header', 'proyectos', 'projects', '0', '2019-09-03 11:30:52', '2019-09-03 11:33:51');
INSERT INTO `t_locale_tag` VALUES ('2012', 'es', 'header-footer', 'about_us', 'Quienes somos', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2013', 'en', 'header-footer', 'about_us', 'About us', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2014', 'es', 'header-footer', 'contact_us', 'Contacto', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2015', 'en', 'header-footer', 'contact_us', 'Contact', '0', '2019-09-03 11:30:52', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2016', 'es', 'topbar', 'buscar', '', '0', '2019-09-03 11:30:52', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2017', 'en', 'topbar', 'buscar', '', '0', '2019-09-03 11:30:52', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2018', 'es', 'breadcrumbs', 'esta_aqui', '', '0', '2019-09-03 11:30:52', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2019', 'en', 'breadcrumbs', 'esta_aqui', '', '0', '2019-09-03 11:30:52', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2020', 'es', 'breadcrumbs', 'home', '', '0', '2019-09-03 11:30:52', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2021', 'en', 'breadcrumbs', 'home', '', '0', '2019-09-03 11:30:52', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2022', 'es', 'cookies', 'mensaje', '', '0', '2019-09-03 11:30:52', '2019-09-06 10:24:58');
INSERT INTO `t_locale_tag` VALUES ('2023', 'en', 'cookies', 'mensaje', '', '0', '2019-09-03 11:30:52', '2019-09-06 10:24:58');
INSERT INTO `t_locale_tag` VALUES ('2024', 'es', 'cookies', 'saber_mas', '', '0', '2019-09-03 11:30:52', '2019-09-06 10:24:58');
INSERT INTO `t_locale_tag` VALUES ('2025', 'en', 'cookies', 'saber_mas', '', '0', '2019-09-03 11:30:52', '2019-09-06 10:24:58');
INSERT INTO `t_locale_tag` VALUES ('2026', 'es', 'cookies', 'aceptar', '', '0', '2019-09-03 11:30:52', '2019-09-06 10:24:58');
INSERT INTO `t_locale_tag` VALUES ('2027', 'en', 'cookies', 'aceptar', '', '0', '2019-09-03 11:30:52', '2019-09-06 10:24:58');
INSERT INTO `t_locale_tag` VALUES ('2028', 'es', 'home-slider', 'titulo1', 'Título 1', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2029', 'en', 'home-slider', 'titulo1', 'Title 1', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2030', 'es', 'home-slider', 'descripcion1', 'Descripción 1: Brindamos siempre nuestros mejores servicios para nuestros clientes y siempre </br> tratamos de lograr la confianza y satisfacción de nuestros clientes.', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2031', 'en', 'home-slider', 'descripcion1', 'Description 1: We provides always our best services for our clients and always</br>try to achieve our client\'s trust and satisfaction.', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2032', 'es', 'home-slider', 'url-link1-1', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2033', 'en', 'home-slider', 'url-link1-1', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2034', 'es', 'home-slider', 'title-link1-1', 'Título enlace 1.1', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2035', 'en', 'home-slider', 'title-link1-1', 'Title link 1.1', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2036', 'es', 'home-slider', 'url-link1-2', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2037', 'en', 'home-slider', 'url-link1-2', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2038', 'es', 'home-slider', 'title-link1-2', 'Título enlace 1.2', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2039', 'en', 'home-slider', 'title-link1-2', 'Title link 1.2', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2040', 'es', 'home-slider', 'titulo2', 'Título 2', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2041', 'en', 'home-slider', 'titulo2', 'Title 2', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2042', 'es', 'home-slider', 'descripcion2', 'Descripción 2: Brindamos siempre nuestros mejores servicios para nuestros clientes y siempre </br> tratamos de lograr la confianza y satisfacción de nuestros clientes.', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2043', 'en', 'home-slider', 'descripcion2', 'Description 2: We provides always our best services for our clients and always</br>try to achieve our client\'s trust and satisfaction.', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2044', 'es', 'home-slider', 'url-link2-1', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2045', 'en', 'home-slider', 'url-link2-1', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2046', 'es', 'home-slider', 'title-link2-1', 'Título enlace 2.1', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2047', 'en', 'home-slider', 'title-link2-1', 'Title link 2.1', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2048', 'es', 'home-slider', 'url-link2-2', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2049', 'en', 'home-slider', 'url-link2-2', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2050', 'es', 'home-slider', 'title-link2-2', 'Título enlace 2.2', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2051', 'en', 'home-slider', 'title-link2-2', 'Title link 2.2', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2052', 'es', 'home-slider', 'titulo3', 'Título 3', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2053', 'en', 'home-slider', 'titulo3', 'Title 3', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2054', 'es', 'home-slider', 'descripcion3', 'Descripción 1: Brindamos siempre nuestros mejores servicios para nuestros clientes y siempre </br> tratamos de lograr la confianza y satisfacción de nuestros clientes.', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2055', 'en', 'home-slider', 'descripcion3', 'Description 3: We provides always our best services for our clients and always</br>try to achieve our client\'s trust and satisfaction.', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2056', 'es', 'home-slider', 'url-link3-1', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2057', 'en', 'home-slider', 'url-link3-1', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2058', 'es', 'home-slider', 'title-link3-1', 'Título enlace 3.1', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2059', 'en', 'home-slider', 'title-link3-1', 'Title link 3.1', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2060', 'es', 'home-slider', 'url-link3-2', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2061', 'en', 'home-slider', 'url-link3-2', '#', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2062', 'es', 'home-slider', 'title-link3-2', 'Título enlace 3.2', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2063', 'en', 'home-slider', 'title-link3-2', 'Title link 3.2', '0', '2019-09-03 11:30:52', '2019-09-03 11:34:43');
INSERT INTO `t_locale_tag` VALUES ('2064', 'es', 'destacados', 'destacados', 'Destacados', '0', '2019-09-03 11:30:52', '2019-09-03 11:55:13');
INSERT INTO `t_locale_tag` VALUES ('2065', 'en', 'destacados', 'destacados', 'Featured', '0', '2019-09-03 11:30:52', '2019-09-03 11:55:13');
INSERT INTO `t_locale_tag` VALUES ('2066', 'es', 'destacados', 'proyectos', 'Proyectos', '0', '2019-09-03 11:30:52', '2019-09-03 11:55:13');
INSERT INTO `t_locale_tag` VALUES ('2067', 'en', 'destacados', 'proyectos', 'Projects', '0', '2019-09-03 11:30:52', '2019-09-03 11:55:13');
INSERT INTO `t_locale_tag` VALUES ('2068', 'es', 'destacados', 'ver', 'Ver Proyecto', '0', '2019-09-03 11:30:52', '2019-09-03 11:55:13');
INSERT INTO `t_locale_tag` VALUES ('2069', 'en', 'destacados', 'ver', 'View Project', '0', '2019-09-03 11:30:52', '2019-09-03 11:55:13');
INSERT INTO `t_locale_tag` VALUES ('2070', 'es', 'servicios', 'proyecto', 'Proyecto', '0', '2019-09-03 11:30:52', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2071', 'en', 'servicios', 'proyecto', 'Project', '0', '2019-09-03 11:30:52', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2072', 'es', 'servicios', 'services', 'Servicios', '0', '2019-09-03 11:30:52', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2073', 'en', 'servicios', 'services', 'Services', '0', '2019-09-03 11:30:52', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2074', 'es', 'servicios', 'servicio1-titulo', 'Activos en Rentabilidad', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2075', 'en', 'servicios', 'servicio1-titulo', 'Profitability Assets', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2076', 'es', 'servicios', 'servicio1-descripcion', 'Nonec pede justo fringilla vel aliquet nec vulputate eget arcu in enim justo rhoncus ut imperdiet venenatis vitae justo.', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2077', 'en', 'servicios', 'servicio1-descripcion', 'Nonec pede justo fringilla vel aliquet nec vulputate eget arcu in enim justo rhoncus ut imperdiet venenatis vitae justo.', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2078', 'es', 'servicios', 'servicio1_link', '#', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2079', 'en', 'servicios', 'servicio1_link', '#', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2080', 'es', 'servicios', 'leer-mas', 'Leer Más', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2081', 'en', 'servicios', 'leer-mas', 'Read More', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2082', 'es', 'servicios', 'servicio2-titulo', 'Inmobiliaria', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2083', 'en', 'servicios', 'servicio2-titulo', 'Real Estate', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2084', 'es', 'servicios', 'servicio2-descripcion', 'Nonec pede justo fringilla vel aliquet nec vulputate eget arcu in enim justo rhoncus ut imperdiet venenatis vitae justo.', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2085', 'en', 'servicios', 'servicio2-descripcion', 'Nonec pede justo fringilla vel aliquet nec vulputate eget arcu in enim justo rhoncus ut imperdiet venenatis vitae justo.', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2086', 'es', 'servicios', 'servicio2_link', '#', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2087', 'en', 'servicios', 'servicio2_link', '#', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2088', 'es', 'servicios', 'servicio3-titulo', 'Gestión de Negocios/Vivienda', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2089', 'en', 'servicios', 'servicio3-titulo', 'Housing/Business Management', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2090', 'es', 'servicios', 'servicio3-descripcion', 'Nonec pede justo fringilla vel aliquet nec vulputate eget arcu in enim justo rhoncus ut imperdiet venenatis vitae justo.', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2091', 'en', 'servicios', 'servicio3-descripcion', 'Nonec pede justo fringilla vel aliquet nec vulputate eget arcu in enim justo rhoncus ut imperdiet venenatis vitae justo.', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2092', 'es', 'servicios', 'servicio3_link', '#', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2093', 'en', 'servicios', 'servicio3_link', '#', '0', '2019-09-03 11:30:53', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2094', 'es', 'welcome', 'welcome', 'BIENVENIDO A ', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2095', 'en', 'welcome', 'welcome', 'WELCOME TO ', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2096', 'es', 'welcome', 'eslogan', 'EL MEJOR LUGAR PARA ENCONTRAR LA CASA QUE QUIERES.', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2097', 'en', 'welcome', 'eslogan', 'THE BEST PLACE TO FIND THE HOUSE YOU WANT.', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2098', 'es', 'welcome', 'content1', 'ENCUENTRA CASAS', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2099', 'en', 'welcome', 'content1', 'FIND HOUSES', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2100', 'es', 'welcome', 'content2', 'es el mejor lugar para elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation oris nisi ut aliquip ex ea.', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2101', 'en', 'welcome', 'content2', 'is the best place for elit, sed do eiusmod tempor dolor sit amet, conse ctetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et lorna aliquatd minimam, quis nostrud exercitation oris nisi ut aliquip ex ea.', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2102', 'es', 'welcome', 'servicio1-titulo', 'Comprar Propiedad', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2103', 'en', 'welcome', 'servicio1-titulo', 'Buy Property', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2104', 'es', 'welcome', 'servicio1-descripcion', 'Tenemos las mejores propiedades <br/> en elit, sed do eiusmod tempe', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2105', 'en', 'welcome', 'servicio1-descripcion', 'We have the best properties<br> elit, sed do eiusmod tempe', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2106', 'es', 'welcome', 'servicio2-titulo', 'Alquiler de Propiedad', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2107', 'en', 'welcome', 'servicio2-titulo', 'Rent Property', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2108', 'es', 'welcome', 'servicio2-descripcion', 'Tenemos las mejores propiedades <br/> en elit, sed do eiusmod tempe', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2109', 'en', 'welcome', 'servicio2-descripcion', 'We have the best properties<br> elit, sed do eiusmod tempe', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2110', 'es', 'welcome', 'servicio3-titulo', 'Kit Inmobiliario', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2111', 'en', 'welcome', 'servicio3-titulo', 'Real Estate Kit<', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2112', 'es', 'welcome', 'servicio3-descripcion', 'Tenemos las mejores propiedades <br/> en elit, sed do eiusmod tempe', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2113', 'en', 'welcome', 'servicio3-descripcion', 'We have the best properties<br> elit, sed do eiusmod tempe', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2114', 'es', 'welcome', 'servicio4-titulo', 'Vender Propiedad', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2115', 'en', 'welcome', 'servicio4-titulo', 'Sell Property', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2116', 'es', 'welcome', 'servicio4-descripcion', 'Tenemos las mejores propiedades <br/> en elit, sed do eiusmod tempe', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2117', 'en', 'welcome', 'servicio4-descripcion', 'We have the best properties<br> elit, sed do eiusmod tempe', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2118', 'es', 'welcome', 'video', 'https://www.youtube.com/watch?v=2xHQqYRcrx4', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2119', 'en', 'welcome', 'video', 'https://www.youtube.com/watch?v=2xHQqYRcrx4', '0', '2019-09-03 11:30:53', '2019-09-03 11:54:16');
INSERT INTO `t_locale_tag` VALUES ('2120', 'es', 'proyecto', 'ficha', '', '0', '2019-09-03 11:30:53', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2121', 'en', 'proyecto', 'ficha', '', '0', '2019-09-03 11:30:53', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2122', 'es', 'proyecto', 'lista-por-categorias', 'Lista de Prueba por Categorías', '0', '2019-09-03 11:30:53', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2123', 'en', 'proyecto', 'lista-por-categorias', 'Category Test List', '0', '2019-09-03 11:30:53', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2124', 'es', 'proyecto', 'lista', '', '0', '2019-09-03 11:30:53', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2125', 'en', 'proyecto', 'lista', '', '0', '2019-09-03 11:30:53', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2126', 'es', 'test', 'ficha', '', '0', '2019-09-03 11:30:53', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2127', 'en', 'test', 'ficha', '', '0', '2019-09-03 11:30:53', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2128', 'es', 'test', 'lista-por-categorias', '', '0', '2019-09-03 11:30:53', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2129', 'en', 'test', 'lista-por-categorias', '', '0', '2019-09-03 11:30:53', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2130', 'es', 'test', 'lista', '', '0', '2019-09-03 11:30:53', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2131', 'en', 'test', 'lista', '', '0', '2019-09-03 11:30:53', '2019-09-03 17:22:38');
INSERT INTO `t_locale_tag` VALUES ('2132', 'es', 'contacto', 'contacto-us', 'Contáctenos', '0', '2019-09-03 17:22:37', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2133', 'en', 'contacto', 'contacto-us', 'Contact Us', '0', '2019-09-03 17:22:37', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2134', 'es', 'contacto', 'inicio', 'Inicio', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2135', 'en', 'contacto', 'inicio', 'Home ', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2136', 'es', 'contacto', 'nuestra-ubicacion', 'Nuestra Ubicación', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2137', 'en', 'contacto', 'nuestra-ubicacion', 'Our Location', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2138', 'es', 'contacto', 'mensaje-enviado', '¡Su mensaje fue enviado exitosamente!', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2139', 'en', 'contacto', 'mensaje-enviado', 'Your message was sent successfully!', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2140', 'es', 'contacto', 'mensaje-no-enviado', 'Algo salió mal, intente actualizar y enviar el formulario nuevamente.', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2141', 'en', 'contacto', 'mensaje-no-enviado', 'Something went wrong, try refreshing and submitting the form again.', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2142', 'es', 'contacto', 'nombre', 'Nombre', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2143', 'en', 'contacto', 'nombre', 'First Name', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2144', 'es', 'contacto', 'apellidos', 'Apellidos', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2145', 'en', 'contacto', 'apellidos', 'Last Name', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2146', 'es', 'contacto', 'email', 'Email', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2147', 'en', 'contacto', 'email', 'Email', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2148', 'es', 'contacto', 'mensaje', 'Mensaje', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2149', 'en', 'contacto', 'mensaje', 'Message', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2150', 'es', 'contacto', 'enviar', 'Enviar', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2151', 'en', 'contacto', 'enviar', 'Submit', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2152', 'es', 'contacto', 'detalles-contacto', 'Detalles de Contacto', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2153', 'en', 'contacto', 'detalles-contacto', 'Contact Details', '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2154', 'es', 'contacto', 'contacto-slogan', null, '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2155', 'en', 'contacto', 'contacto-slogan', null, '0', '2019-09-03 17:22:38', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2156', 'es', 'header-footer', 'horario', ' 8:00 a.m - 6:00 p.m', '0', '2019-09-03 17:22:38', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2157', 'en', 'header-footer', 'horario', ' 8:00 a.m - 6:00 p.m', '0', '2019-09-03 17:22:38', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2158', 'es', 'about', '\',[\'es\'=>\'', null, '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2159', 'en', 'about', '\',[\'es\'=>\'', null, '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2160', 'es', 'about', 'about-us', 'Quienes Somos', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2161', 'en', 'about', 'about-us', 'About Us', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2162', 'es', 'about', 'inicio', 'Incio', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2163', 'en', 'about', 'inicio', 'Home', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2164', 'es', 'about', '\',[\'es\'=>\'Sobre', null, '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2165', 'en', 'about', '\',[\'es\'=>\'Sobre', null, '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2166', 'es', 'about', 'encuentra', 'Encontrar proyectos', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2167', 'en', 'about', 'encuentra', 'Find Projects', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2168', 'es', 'about', 'parrafo1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum odio id voluptatibus incidunt cum? Atque quasi eum debitis optio ab. Esse itaque officiis tempora possimus odio rerum aperiam ratione, sunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit sunt.', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2169', 'en', 'about', 'parrafo1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum odio id voluptatibus incidunt cum? Atque quasi eum debitis optio ab. Esse itaque officiis tempora possimus odio rerum aperiam ratione, sunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit sunt.', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2170', 'es', 'about', 'parrafo2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum odio id voluptatibus incidunt cum? Atque quasi eum debitis optio ab. Esse itaque officiis tempora possimus odio rerum aperiam ratione, sunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit sunt.', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2171', 'en', 'about', 'parrafo2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum odio id voluptatibus incidunt cum? Atque quasi eum debitis optio ab. Esse itaque officiis tempora possimus odio rerum aperiam ratione, sunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit sunt.', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2172', 'es', 'about', 'leer-mas', 'Leer Más', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2173', 'en', 'about', 'leer-mas', 'Read More', '0', '2019-09-04 08:22:27', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2174', 'es', 'contacto', 'contacto-eslogan', '¡Encuentre a continuación los datos de contacto y contáctenos hoy!', '0', '2019-09-04 08:22:27', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2175', 'en', 'contacto', 'contacto-eslogan', 'Please find below contact details and contact us today!', '0', '2019-09-04 08:22:27', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2176', 'es', '_json', 'about.', null, '0', '2019-09-04 08:22:28', '2019-09-04 08:22:28');
INSERT INTO `t_locale_tag` VALUES ('2177', 'en', '_json', 'about.', null, '0', '2019-09-04 08:22:28', '2019-09-04 08:22:28');
INSERT INTO `t_locale_tag` VALUES ('2178', 'es', 'proyectos', 'lista', '', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2179', 'en', 'proyectos', 'lista', '', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2180', 'es', 'proyectos', 'lista-proyectos', 'Lista de Proyectos', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2181', 'en', 'proyectos', 'lista-proyectos', 'Projects List', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2182', 'es', 'proyectos', '\',[\'es\'=>\'Inicio', null, '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2183', 'en', 'proyectos', '\',[\'es\'=>\'Inicio', null, '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2184', 'es', 'proyectos', '\',[\'es\'=>\'', null, '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2185', 'en', 'proyectos', '\',[\'es\'=>\'', null, '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2186', 'es', 'proyectos', 'precio-mas', 'Precio: Más a menos', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2187', 'en', 'proyectos', 'precio-mas', 'Price: High to low', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2188', 'es', 'proyectos', 'precio-menos', 'Precio: Menos a más', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2189', 'en', 'proyectos', 'precio-menos', 'Price: Low to high', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2190', 'es', 'proyectos', 'fecha-mas', 'Fecha: Más a Menos', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2191', 'en', 'proyectos', 'fecha-mas', 'Date: High to low', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2192', 'es', 'proyectos', 'fecha-menos', 'Fecha: Menos a más', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2193', 'en', 'proyectos', 'fecha-menos', 'Date: Low to high', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2194', 'es', 'proyectos', 'ver-proyecto', 'Ver Proyecto', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2195', 'en', 'proyectos', 'ver-proyecto', 'View Project', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2196', 'es', 'proyectos', 'busca-proyectos', 'Busca Proyectos', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2197', 'en', 'proyectos', 'busca-proyectos', 'Search Projects', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2198', 'es', 'proyectos', 'busca-tus-proyectos', 'Busca tus Proyectos', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2199', 'en', 'proyectos', 'busca-tus-proyectos', 'Search your Projects', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2200', 'es', 'proyectos', 'filtro', 'Filtro', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2201', 'en', 'proyectos', 'filtro', 'Filter', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2202', 'es', 'proyectos', 'todas-categorias', 'Todas las categorías', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2203', 'en', 'proyectos', 'todas-categorias', 'All categories', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2204', 'es', 'proyectos', 'todos-tipos', 'Todos los tipos', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2205', 'en', 'proyectos', 'todos-tipos', 'All types', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2206', 'es', 'proyectos', 'local', 'Local', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2207', 'en', 'proyectos', 'local', 'Premises', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2208', 'es', 'proyectos', 'oficina', 'Oficina', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2209', 'en', 'proyectos', 'oficina', 'Office', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2210', 'es', 'proyectos', 'nave-industrial', 'Nave Industrial', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2211', 'en', 'proyectos', 'nave-industrial', 'Industrial ship', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2212', 'es', 'proyectos', 'vivienda', 'Vivienda', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2213', 'en', 'proyectos', 'vivienda', 'living place', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2214', 'es', 'proyectos', 'todos-tipos-operacion', 'Todos los tipos de operación', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2215', 'en', 'proyectos', 'todos-tipos-operacion', 'All operation types', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2216', 'es', 'proyectos', 'traspaso', 'Traspaso', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2217', 'en', 'proyectos', 'traspaso', 'Transfer', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2218', 'es', 'proyectos', 'venta', 'Venta', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2219', 'en', 'proyectos', 'venta', 'Sale', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2220', 'es', 'proyectos', 'alquiler', 'Alquiler', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2221', 'en', 'proyectos', 'alquiler', 'Rent', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2222', 'es', 'proyectos', 'todos-sectores', 'Todos los sectores', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2223', 'en', 'proyectos', 'todos-sectores', 'All sectors', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2224', 'es', 'proyectos', 'todos-subsectores', 'Todos los subsectores', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2225', 'en', 'proyectos', 'todos-subsectores', 'All subsectors', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2226', 'es', 'proyectos', 'todos-estados', 'Todos los estados', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2227', 'en', 'proyectos', 'todos-estados', 'All states', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2228', 'es', 'proyectos', 'buscar', 'Buscar', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2229', 'en', 'proyectos', 'buscar', 'Search', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2230', 'es', 'proyectos', 'proyectos-recientes', 'Proyectos Recientes', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2231', 'en', 'proyectos', 'proyectos-recientes', 'Recent Projects', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2232', 'es', 'proyectos', 'casa-familiar', 'Casa Familiar', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2233', 'en', 'proyectos', 'casa-familiar', 'Family Home', '0', '2019-09-05 16:50:08', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2234', 'es', '_json', 'proyectos.', null, '0', '2019-09-05 16:50:08', '2019-09-05 16:50:08');
INSERT INTO `t_locale_tag` VALUES ('2235', 'en', '_json', 'proyectos.', null, '0', '2019-09-05 16:50:08', '2019-09-05 16:50:08');
INSERT INTO `t_locale_tag` VALUES ('2236', 'es', 'proyectos', 'inicio', 'Inicio', '0', '2019-09-05 20:59:52', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2237', 'en', 'proyectos', 'inicio', 'Home', '0', '2019-09-05 20:59:52', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2238', 'es', 'proyecto', 'detalles-proyecto', 'Detalles del Proyecto', '0', '2019-09-05 20:59:52', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2239', 'en', 'proyecto', 'detalles-proyecto', 'Property Details', '0', '2019-09-05 20:59:52', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2240', 'es', 'proyecto', 'informacion-general', 'INFORMACION GENERAL', '0', '2019-09-05 20:59:52', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2241', 'en', 'proyecto', 'informacion-general', 'GENERAL INFORMATION', '0', '2019-09-05 20:59:52', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2242', 'es', 'proyecto', 'comentarios', 'Comentarios', '0', '2019-09-05 20:59:52', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2243', 'en', 'proyecto', 'comentarios', 'Comments', '0', '2019-09-05 20:59:52', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2244', 'es', 'proyecto', 'ubicacion', 'Ubicación', '0', '2019-09-05 20:59:52', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2245', 'en', 'proyecto', 'ubicacion', 'Location', '0', '2019-09-05 20:59:52', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2246', 'es', 'proyecto', 'precio', 'Precio: ', '0', '2019-09-06 10:22:06', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2247', 'en', 'proyecto', 'precio', 'Price: ', '0', '2019-09-06 10:22:06', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2248', 'es', 'aviso-legal', 'titulo', '', '0', '2019-09-06 11:38:24', '2019-09-09 10:17:29');
INSERT INTO `t_locale_tag` VALUES ('2249', 'en', 'aviso-legal', 'titulo', '', '0', '2019-09-06 11:38:24', '2019-09-09 10:17:29');
INSERT INTO `t_locale_tag` VALUES ('2250', 'es', 'footer', 'aviso-legal', 'Aviso Legal y Política de Privacidad', '0', '2019-09-06 11:38:24', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('2251', 'en', 'footer', 'aviso-legal', 'Legal Notice and Privacy Policy', '0', '2019-09-06 11:38:24', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('2252', 'es', 'footer', 'politica-cookies', 'Política de Cookies', '0', '2019-09-06 11:38:24', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('2253', 'en', 'footer', 'politica-cookies', 'Cookies policy', '0', '2019-09-06 11:38:24', '2019-09-13 13:13:53');
INSERT INTO `t_locale_tag` VALUES ('2254', 'es', 'politica-cookies', 'titulo', '', '0', '2019-09-06 11:38:24', '2019-09-09 10:17:30');
INSERT INTO `t_locale_tag` VALUES ('2255', 'en', 'politica-cookies', 'titulo', '', '0', '2019-09-06 11:38:24', '2019-09-09 10:17:30');
INSERT INTO `t_locale_tag` VALUES ('2256', 'es', 'servicios', 'titulo', 'Título', '0', '2019-09-09 10:17:30', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2257', 'en', 'servicios', 'titulo', 'Title', '0', '2019-09-09 10:17:30', '2019-09-13 13:20:40');
INSERT INTO `t_locale_tag` VALUES ('2258', 'es', 'proyectos', 'proyectos', 'Proyectos', '0', '2019-09-09 10:17:30', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2259', 'en', 'proyectos', 'proyectos', 'Projects', '0', '2019-09-09 10:17:30', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2260', 'es', 'proyecto', 'referencia', 'Referencia: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2261', 'en', 'proyecto', 'referencia', 'Reference: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2262', 'es', 'proyecto', 'fecha-publicacion', 'Fecha de Publicación: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2263', 'en', 'proyecto', 'fecha-publicacion', 'Publication Date: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2264', 'es', 'proyecto', 'fecha-extincion', 'Fecha de Extinción: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2265', 'en', 'proyecto', 'fecha-extincion', 'Extinction  Date: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2266', 'es', 'proyecto', 'tipo', 'Tipo: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2267', 'en', 'proyecto', 'tipo', 'Type: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2268', 'es', 'proyecto', 'tipo-operacion', 'Tipo de operación: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2269', 'en', 'proyecto', 'tipo-operacion', 'Operation Type: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2270', 'es', 'proyecto', 'persona-contacto', 'Persona de Contacto: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2271', 'en', 'proyecto', 'persona-contacto', 'Contact Person: ', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2272', 'es', 'proyecto', 'documentacion', 'documentación', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2273', 'en', 'proyecto', 'documentacion', 'documentation', '0', '2019-09-09 10:17:30', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2274', 'es', 'proyecto', 'proyectos', 'Proyectos', '0', '2019-09-09 10:22:11', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2275', 'en', 'proyecto', 'proyectos', 'Projects', '0', '2019-09-09 10:22:11', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2276', 'es', 'about', 'parrafo3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum odio id voluptatibus incidunt cum? Atque quasi eum debitis optio ab. Esse itaque officiis tempora possimus odio rerum aperiam ratione, sunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit sunt.', '0', '2019-09-09 10:43:48', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2277', 'en', 'about', 'parrafo3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laborum odio id voluptatibus incidunt cum? Atque quasi eum debitis optio ab. Esse itaque officiis tempora possimus odio rerum aperiam ratione, sunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit sunt.', '0', '2019-09-09 10:43:48', '2019-09-09 10:44:40');
INSERT INTO `t_locale_tag` VALUES ('2278', 'es', 'proyecto', 'texto-busqueda', 'Texto busqueda', '0', '2019-09-10 18:19:18', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2279', 'en', 'proyecto', 'texto-busqueda', 'Search text', '0', '2019-09-10 18:19:18', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2280', 'es', 'contacto', 'saludo', 'Hola', '0', '2019-09-12 09:48:35', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2281', 'en', 'contacto', 'saludo', 'Hello', '0', '2019-09-12 09:48:35', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2282', 'es', 'contacto', 'intoduccion', 'Estoy interesado recibir más informació sobre el proyecto: ', '0', '2019-09-12 09:48:35', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2283', 'en', 'contacto', 'intoduccion', 'I am interested to receive more information about the project:', '0', '2019-09-12 09:48:35', '2019-09-12 09:55:43');
INSERT INTO `t_locale_tag` VALUES ('2284', 'es', 'proyecto', 'fecha-vigencia', 'Fecha vigencia: ', '0', '2019-09-12 09:48:35', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2285', 'en', 'proyecto', 'fecha-vigencia', 'Valedity date: ', '0', '2019-09-12 09:48:35', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2286', 'es', 'proyecto', 'categoria', 'Categoria: ', '0', '2019-09-12 09:48:35', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2287', 'en', 'proyecto', 'categoria', 'Category: ', '0', '2019-09-12 09:48:35', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2288', 'es', 'emails', 'telefono', '', '0', '2019-09-12 09:48:35', '2019-09-12 16:44:52');
INSERT INTO `t_locale_tag` VALUES ('2289', 'en', 'emails', 'telefono', '', '0', '2019-09-12 09:48:35', '2019-09-12 16:44:52');
INSERT INTO `t_locale_tag` VALUES ('2290', 'es', 'proyecto', 'contactar', 'Contactar', '0', '2019-09-12 09:48:36', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2291', 'en', 'proyecto', 'contactar', 'Contact', '0', '2019-09-12 09:48:36', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2292', 'es', 'proyecto', 'contactar-text', 'Para solicitar más información sobre el proyecto y  pulse ', '0', '2019-09-12 09:48:36', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2293', 'en', 'proyecto', 'contactar-text', 'To request more information about the project and press', '0', '2019-09-12 09:48:36', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2294', 'es', 'proyecto', 'aqui', 'aquí', '0', '2019-09-12 09:48:36', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2295', 'en', 'proyecto', 'aqui', 'here', '0', '2019-09-12 09:48:36', '2019-09-18 11:26:52');
INSERT INTO `t_locale_tag` VALUES ('2296', 'es', 'header-footer', 'search', 'Buscar', '0', '2019-09-12 16:44:53', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2297', 'en', 'header-footer', 'search', 'Search', '0', '2019-09-12 16:44:53', '2019-09-13 07:16:12');
INSERT INTO `t_locale_tag` VALUES ('2298', 'es', 'proyectos', 'tags', 'Tags', '0', '2019-09-12 16:55:39', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2299', 'en', 'proyectos', 'tags', 'Tags', '0', '2019-09-12 16:55:39', '2019-09-12 16:58:05');
INSERT INTO `t_locale_tag` VALUES ('2300', 'es', 'master', 'title', 'Ulyses | Gestión inmobiliaria de negocios, viviendas y activos en rentavilidad', '0', '2019-09-16 07:27:19', '2019-09-16 08:56:30');
INSERT INTO `t_locale_tag` VALUES ('2301', 'en', 'master', 'title', 'Ulyses | Real estate management of businesses, homes and assets in profitability', '0', '2019-09-16 07:27:19', '2019-09-16 08:56:30');
INSERT INTO `t_locale_tag` VALUES ('2302', 'es', 'master', 'description', 'Ulyses, empresa dedicada ...', '0', '2019-09-16 07:27:19', '2019-09-16 08:56:30');
INSERT INTO `t_locale_tag` VALUES ('2303', 'en', 'master', 'description', 'Ulyses, dedicated company ...', '0', '2019-09-16 07:27:19', '2019-09-16 08:56:30');
INSERT INTO `t_locale_tag` VALUES ('2304', 'es', 'master', 'keywords', 'gestión de negocios y vivienda, inmobiliaria, activos en rentabilidad', '0', '2019-09-16 07:27:19', '2019-09-16 08:56:30');
INSERT INTO `t_locale_tag` VALUES ('2305', 'en', 'master', 'keywords', 'business and housing management, real estate, profitability assets', '0', '2019-09-16 07:27:19', '2019-09-16 08:56:30');

-- ----------------------------
-- Table structure for t_media
-- ----------------------------
DROP TABLE IF EXISTS `t_media`;
CREATE TABLE `t_media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo_id` int(100) NOT NULL,
  `tipo_medio` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `proyecto_id` int(10) NOT NULL,
  `orden` int(3) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `rel_profile` varchar(3) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=128 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_media
-- ----------------------------
INSERT INTO `t_media` VALUES ('90', 'Emulsión Fluida', '1', 'pdf', '6', '1', '2019-08-22 07:40:43', '2019-09-03 11:04:49', 'es');
INSERT INTO `t_media` VALUES ('91', 'Emulsión fluida propinato', '2', 'pdf', '6', '2', '2019-08-22 07:41:12', '2019-09-03 11:05:16', 'es');
INSERT INTO `t_media` VALUES ('92', 'Fachada', '2', 'img', '6', '0', '2019-08-22 10:41:30', '2019-09-03 11:06:07', 'es');
INSERT INTO `t_media` VALUES ('96', 'Exterior', '2', 'img', '7', '1', '2019-08-22 11:20:52', '2019-09-03 10:47:08', 'es');
INSERT INTO `t_media` VALUES ('97', 'Interior', '1', 'img', '7', '1', '2019-08-22 11:21:21', '2019-09-03 10:47:23', 'es');
INSERT INTO `t_media` VALUES ('105', 'Sala de reuniones', '2', 'img', '38', '1', '2019-08-22 12:19:15', '2019-09-03 11:08:11', 'es');
INSERT INTO `t_media` VALUES ('106', 'Sala Reuniones', '2', 'img', '12', null, '2019-09-03 09:22:56', '2019-09-03 11:07:19', 'es');
INSERT INTO `t_media` VALUES ('110', 'Odontología Gel', '1', 'pdf', '7', '2', '2019-09-03 10:14:05', '2019-09-03 10:46:47', 'es');
INSERT INTO `t_media` VALUES ('111', 'Introducción a la odontología', '2', 'pdf', '7', '2', '2019-09-03 10:17:12', '2019-09-03 10:47:00', 'es');
INSERT INTO `t_media` VALUES ('112', 'Fachada', '2', 'img', '33', '2', '2019-09-03 10:57:10', '2019-09-03 10:57:10', 'es');
INSERT INTO `t_media` VALUES ('113', 'Interior', '2', 'img', '39', '2', '2019-09-03 13:39:03', '2019-09-03 13:39:03', 'es');
INSERT INTO `t_media` VALUES ('114', 'Sala de reuniones', '1', 'img', '12', '1', '2019-09-05 19:00:13', '2019-09-05 19:03:11', 'es');
INSERT INTO `t_media` VALUES ('115', 'despacho', '1', 'img', '12', '2', '2019-09-05 19:01:30', '2019-09-05 19:01:30', 'es');
INSERT INTO `t_media` VALUES ('116', 'Oficinas', '1', 'img', '12', '3', '2019-09-05 19:01:55', '2019-09-05 19:01:55', 'es');
INSERT INTO `t_media` VALUES ('117', 'Documento front 1', '1', 'pdf', '12', '1', '2019-09-09 08:48:12', '2019-09-09 08:48:12', 'es');
INSERT INTO `t_media` VALUES ('118', 'Documento front 2', '1', 'pdf', '12', '2', '2019-09-09 08:48:40', '2019-09-09 08:48:40', 'es');
INSERT INTO `t_media` VALUES ('119', 'Documento front 3', '1', 'pdf', '12', '3', '2019-09-09 08:49:05', '2019-09-09 08:49:05', 'es');
INSERT INTO `t_media` VALUES ('120', 'Documento front 4', '1', 'pdf', '12', '4', '2019-09-09 09:44:34', '2019-09-09 09:44:34', 'es');
INSERT INTO `t_media` VALUES ('121', 'Documento Front 5', '2', 'pdf', '12', '5', '2019-09-09 09:46:33', '2019-09-09 09:47:31', 'es');
INSERT INTO `t_media` VALUES ('123', 'piscina', '1', 'img', '13', null, '2019-09-10 11:00:33', '2019-09-10 11:00:33', 'es');
INSERT INTO `t_media` VALUES ('126', 'piscina', '2', 'img', '13', '1', '2019-09-10 11:01:39', '2019-09-10 11:01:39', 'es');
INSERT INTO `t_media` VALUES ('127', 'aaaaaaaaaaaa', '1', null, '39', null, '2022-01-14 11:24:48', '2022-01-14 11:24:48', 'es');

-- ----------------------------
-- Table structure for t_permission
-- ----------------------------
DROP TABLE IF EXISTS `t_permission`;
CREATE TABLE `t_permission` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `t_permission_slug_unique` (`slug`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_permission
-- ----------------------------
INSERT INTO `t_permission` VALUES ('53', 'Ver GrennyGarden', 'ver.proyectos', 'Sólo pueden visualizar la lista y la ficha de los proyectos', '1', '2019-08-06 12:02:50', '2019-08-07 08:51:50', 'proyectos');
INSERT INTO `t_permission` VALUES ('54', 'Ver configurations', 'ver.configs', 'Ver las configuraciones de la aplicación', '1', '2019-08-07 08:11:59', '2019-08-07 08:11:59', 'configs');
INSERT INTO `t_permission` VALUES ('55', 'Edit GrennyGarden', 'edit.proyectos', 'Editar Proyectos', '1', '2019-09-13 10:17:58', '2019-09-13 10:18:50', 'proyectos');
INSERT INTO `t_permission` VALUES ('56', 'Ver Printers', 'ver.media', 'Ver Media', '1', '2019-09-13 12:52:32', '2019-09-13 12:52:32', 'proyectos');
INSERT INTO `t_permission` VALUES ('57', 'Edit Printers', 'edit.media', 'Edit Media', '1', '2019-09-13 12:53:12', '2019-09-13 12:53:12', 'proyectos');

-- ----------------------------
-- Table structure for t_permission_role
-- ----------------------------
DROP TABLE IF EXISTS `t_permission_role`;
CREATE TABLE `t_permission_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `t_permission_role_permission_id_index` (`permission_id`) USING BTREE,
  KEY `t_permission_role_role_id_index` (`role_id`) USING BTREE,
  CONSTRAINT `t_permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `t_permission` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_permission_role
-- ----------------------------
INSERT INTO `t_permission_role` VALUES ('45', '54', '12', '2019-08-07 08:12:58', '2019-08-07 08:12:58');
INSERT INTO `t_permission_role` VALUES ('49', '56', '11', '2019-09-13 12:53:38', '2019-09-13 12:53:38');
INSERT INTO `t_permission_role` VALUES ('50', '53', '11', '2019-09-13 12:53:38', '2019-09-13 12:53:38');

-- ----------------------------
-- Table structure for t_permission_user
-- ----------------------------
DROP TABLE IF EXISTS `t_permission_user`;
CREATE TABLE `t_permission_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `permission_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `t_permission_user_permission_id_index` (`permission_id`) USING BTREE,
  KEY `t_permission_user_user_id_index` (`user_id`) USING BTREE,
  CONSTRAINT `t_permission_user_ibfk_1` FOREIGN KEY (`permission_id`) REFERENCES `t_permission` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_permission_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_permission_user
-- ----------------------------

-- ----------------------------
-- Table structure for t_proyecto
-- ----------------------------
DROP TABLE IF EXISTS `t_proyecto`;
CREATE TABLE `t_proyecto` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 0,
  `categoria_id` int(10) unsigned NOT NULL,
  `orden` int(11) NOT NULL DEFAULT 0,
  `referencia` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sector` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subsector` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `estado` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `destacado` tinyint(1) DEFAULT NULL,
  `fecha_publicacion` timestamp NULL DEFAULT NULL,
  `fecha_extincion` timestamp NULL DEFAULT NULL,
  `persona_contacto` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observaciones` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `informacion_detallada` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `direccion` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `latitud` decimal(11,8) DEFAULT NULL,
  `longitud` decimal(11,8) DEFAULT NULL,
  `tipo_operacion` varchar(125) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tipo` varchar(125) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `precio` float(20,0) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t_proyecto_categoria_id_foreign` (`categoria_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_proyecto
-- ----------------------------
INSERT INTO `t_proyecto` VALUES ('6', 'Grenny Garden +', '1', '1', '1', '666666666', 'Metalurgia', 'Automoción', 'Sólido', '1', '2019-09-02 00:00:00', '2019-09-26 00:00:00', 'Jhon Doe', null, null, 'Est St, 77 - Central Park South, NYC', '39.57324200', '2.65826900', 'traspaso', 'local', '2100000', '2019-08-08 11:34:48', '2019-09-18 11:47:15');
INSERT INTO `t_proyecto` VALUES ('7', 'Tower indor', '1', '1', '2', '777777777', 'Electricidad', 'Doméstica', 'Gaseoso', '1', '2019-09-02 00:00:00', '2019-09-26 00:00:00', 'Jose Saiz', null, null, 'Pasadizo Reboda  migjorneges, 86B 6ºH', '0.00000000', '0.00000000', 'alquiler', 'nave-industrial', '450000', '2019-08-08 12:48:43', '2019-09-09 11:37:21');
INSERT INTO `t_proyecto` VALUES ('8', 'Tower 2', '1', '2', '3', '888888888', 'Banca', 'Financiacion', 'Líquido', '1', '2019-09-02 00:00:00', '2019-09-03 00:00:00', 'Cristina Martín', null, null, 'C. Comercial Abadani vantat ustió sedassejau, 266 11ºE', '0.00000000', '0.00000000', 'traspaso', 'vivienda', '1500000', '2019-08-08 12:50:30', '2019-09-05 22:26:17');
INSERT INTO `t_proyecto` VALUES ('9', 'Tower  2.2', '1', '2', '4', '999999999', 'Negocios', 'Asesoría', 'Sólido', '1', '2019-07-01 00:00:00', '2019-10-14 00:00:00', 'Jhon Doe', null, null, 'Callejón Pelléssim empaitaríeu nitrocel·lulosa, 253 9ºD', '0.00000000', '0.00000000', 'alquiler', 'oficina', '788000', '2019-08-08 12:50:31', '2019-09-10 11:12:06');
INSERT INTO `t_proyecto` VALUES ('12', 'aaaaaaaaaa', '1', '2', '9', '122222222', 'TI', 'Energía', 'Gaseoso', '1', '2019-09-08 00:00:00', '2019-09-29 00:00:00', 'Cristina Martín', null, null, 'C. Comercial Senyorejà del Campo Sarajará, 45A 3ºH', '40.00890000', '4.19746900', 'venta', 'oficina', '987000', '2019-08-13 10:01:01', '2019-09-13 08:36:56');
INSERT INTO `t_proyecto` VALUES ('13', 'proyecto 3.3', '1', '3', '10', '13333333', 'Carpintería', 'Barcos', 'Líquido', '0', '2019-09-02 00:00:00', '2019-09-26 00:00:00', 'Jose Saiz', null, null, 'Vía Addicions, 203B 16ºC', '0.00000000', '0.00000000', 'alquiler', 'oficina', '3000000', '2019-08-13 10:04:37', '2019-09-09 15:37:20');
INSERT INTO `t_proyecto` VALUES ('32', 'ccccccccc', '0', '3', '3', '32222222', 'Metalurgia', 'Repuestos', 'Sólido', '0', '2019-09-02 00:00:00', '2019-09-19 00:00:00', 'Jhon Doe', null, null, 'Urbanización Esguimbessen, 11B', '0.00000000', '0.00000000', 'alquiler', 'vivienda', '800000', '2019-08-19 14:41:31', '2019-09-18 11:13:36');
INSERT INTO `t_proyecto` VALUES ('33', 'dddddddddd', '1', '1', '1', '333333333', 'Electricidad', 'Electrodomésticos', 'Gaseoso', '1', '2019-08-29 00:00:00', '2019-09-30 00:00:00', 'Cristina Martín', null, null, 'Rambla Desimposéssim argany severa quinqués, 236A', '0.00000000', '0.00000000', 'venta', 'local', '600000', '2019-08-22 11:20:23', '2019-09-09 18:54:37');
INSERT INTO `t_proyecto` VALUES ('34', 'bbbbbbbbbbbbbbbbbbbb', '0', '2', '3', '3444444444', 'Banca', 'Hipotecas', 'Líquido', '0', '2019-07-29 00:00:00', '2019-09-01 00:00:00', ' Jhon Doe', null, null, 'Camino Desabellar deshipotecarà fanguejassis, 214 8ºF', '0.00000000', '0.00000000', 'traspaso', 'oficina', '1870000', '2019-08-22 11:23:53', '2019-09-02 18:26:08');
INSERT INTO `t_proyecto` VALUES ('38', 'e', '1', '3', '2', 'it', 'TI', 'Robótica', 'Sólido', '1', '2019-08-26 00:00:00', '2019-09-30 00:00:00', 'Cristina Martín', 'dddddddddddddddddddddd', 'dddddddddddd', 'dddddddddddddddddddddd', '44.45555444', '44.44000000', 'alquiler', 'oficina', '1700000', '2019-08-22 12:01:27', '2019-09-09 19:11:32');
INSERT INTO `t_proyecto` VALUES ('39', 'gggggggggggggggggggg', '1', '1', '3', '3999999999', 'TI', 'Internet', 'Gaseoso', '0', '2019-08-29 00:00:00', '2019-09-01 00:00:00', 'Jose Saiz', null, null, 'Travesía Evitàssiu macarem, 73', '0.00000000', '0.00000000', 'venta', 'vivienda', '900000', '2019-08-29 10:41:14', '2019-09-05 22:28:02');

-- ----------------------------
-- Table structure for t_proyecto_categoria
-- ----------------------------
DROP TABLE IF EXISTS `t_proyecto_categoria`;
CREATE TABLE `t_proyecto_categoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_proyecto_categoria
-- ----------------------------
INSERT INTO `t_proyecto_categoria` VALUES ('1', 'Categorie 1', '1', '2019-08-08 10:44:30', '2022-01-14 11:20:25');
INSERT INTO `t_proyecto_categoria` VALUES ('2', 'Categorie 4', '1', '2019-08-08 12:46:43', '2022-01-14 11:20:04');
INSERT INTO `t_proyecto_categoria` VALUES ('3', 'Categorie 3', '1', '2019-08-09 11:36:45', '2022-01-14 11:19:45');
INSERT INTO `t_proyecto_categoria` VALUES ('4', 'categoría proyecto 4', '1', '2019-08-16 06:47:21', '2019-08-16 06:47:21');

-- ----------------------------
-- Table structure for t_role
-- ----------------------------
DROP TABLE IF EXISTS `t_role`;
CREATE TABLE `t_role` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `special` enum('all-access','no-access','partial-access') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `t_role_name_unique` (`name`) USING BTREE,
  UNIQUE KEY `t_role_slug_unique` (`slug`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_role
-- ----------------------------
INSERT INTO `t_role` VALUES ('1', 'Admin', 'admin', 'Admin', '1', '2018-07-06 11:29:17', '2018-07-06 11:29:17', 'all-access');
INSERT INTO `t_role` VALUES ('11', 'Profesional', 'profesional', 'Sólo pueden visualizar los proyectos (Listado y Ficha)', '1', '2019-08-06 11:59:15', '2019-09-13 08:50:38', null);
INSERT INTO `t_role` VALUES ('12', 'Configurador', 'config', 'Encargado de configurar la aplicación', '1', '2019-08-07 08:12:58', '2019-08-07 08:12:58', 'partial-access');

-- ----------------------------
-- Table structure for t_role_user
-- ----------------------------
DROP TABLE IF EXISTS `t_role_user`;
CREATE TABLE `t_role_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  KEY `t_role_user_role_id_index` (`role_id`) USING BTREE,
  KEY `t_role_user_user_id_index` (`user_id`) USING BTREE,
  CONSTRAINT `t_role_user_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `t_role` (`id`) ON DELETE CASCADE,
  CONSTRAINT `t_role_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `t_user` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_role_user
-- ----------------------------
INSERT INTO `t_role_user` VALUES ('20', '1', '1', '2018-09-10 15:50:27', '2018-09-10 15:50:27');
INSERT INTO `t_role_user` VALUES ('29', '12', '16', '2019-08-07 08:13:35', '2019-08-07 08:13:35');
INSERT INTO `t_role_user` VALUES ('38', '11', '15', '2020-03-10 22:20:37', '2020-03-10 22:20:37');
INSERT INTO `t_role_user` VALUES ('40', '11', '17', '2020-03-11 17:40:13', '2020-03-11 17:40:13');
INSERT INTO `t_role_user` VALUES ('50', '1', '19', '2020-04-17 15:33:20', '2020-04-17 15:33:20');
INSERT INTO `t_role_user` VALUES ('51', '1', '18', '2022-01-25 10:57:42', '2022-01-25 10:57:42');

-- ----------------------------
-- Table structure for t_test
-- ----------------------------
DROP TABLE IF EXISTS `t_test`;
CREATE TABLE `t_test` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 0,
  `categoria_id` int(10) unsigned NOT NULL,
  `orden` int(11) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `t_test_categoria_id_foreign` (`categoria_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of t_test
-- ----------------------------
INSERT INTO `t_test` VALUES ('6', 'test 1', '1', '1', '1', '2019-08-08 11:34:48', '2019-08-13 07:11:37');
INSERT INTO `t_test` VALUES ('7', 'test 1.1', '1', '1', '2', '2019-08-08 12:48:43', '2019-08-13 09:46:35');
INSERT INTO `t_test` VALUES ('8', 'test 2', '1', '2', '3', '2019-08-08 12:50:30', '2019-08-13 09:54:16');
INSERT INTO `t_test` VALUES ('9', 'test 2.2', '1', '2', '4', '2019-08-08 12:50:31', '2019-08-13 09:53:00');
INSERT INTO `t_test` VALUES ('12', 'test 3', '1', '3', '9', '2019-08-13 10:01:01', '2019-08-13 10:02:34');
INSERT INTO `t_test` VALUES ('13', 'test 3.3', '1', '3', '10', '2019-08-13 10:04:37', '2022-01-11 20:50:45');

-- ----------------------------
-- Table structure for t_test_categoria
-- ----------------------------
DROP TABLE IF EXISTS `t_test_categoria`;
CREATE TABLE `t_test_categoria` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of t_test_categoria
-- ----------------------------
INSERT INTO `t_test_categoria` VALUES ('1', 'categoria test 1', '1', '2019-08-08 10:44:30', '2019-08-08 12:46:00');
INSERT INTO `t_test_categoria` VALUES ('2', 'categoria test 2', '1', '2019-08-08 12:46:43', '2019-08-09 12:38:04');
INSERT INTO `t_test_categoria` VALUES ('3', 'categoria test 3', '1', '2019-08-09 11:36:45', '2019-08-09 11:36:45');

-- ----------------------------
-- Table structure for t_user
-- ----------------------------
DROP TABLE IF EXISTS `t_user`;
CREATE TABLE `t_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `t_user_email_unique` (`email`) USING BTREE,
  KEY `t_user_level_index` (`level`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci ROW_FORMAT=DYNAMIC;

-- ----------------------------
-- Records of t_user
-- ----------------------------
INSERT INTO `t_user` VALUES ('1', 'Jose', 'jose.saiz@gmail.com', '$2y$10$VOp2Fff4UcUt2uiz6v3G8OzvQylKapSzw/UKcVWYJNqrwHIHLL7JS', '', 'oAMcEZytKmAJGqhCWUpIiqolJXmTpI6NdAL5S9qJ7Co9PbWYn92p4i89RCcf', '1', '2018-04-12 10:13:59', '2018-06-12 15:29:12');
INSERT INTO `t_user` VALUES ('15', 'Jose Profesional', 'jose+profesional@gmail.com', '$2y$10$NWlzTFunIyxbyKv1klnEpOYg9QYJtIxrSTXCHpLFr/OPKwGjLwN1u', '', null, '1', '2019-08-06 12:05:52', '2019-09-13 08:45:33');
INSERT INTO `t_user` VALUES ('16', 'Jose Configurador', 'jose+config@gmail.com', '$2y$10$0tFUvPTB7bPq7zpnEO9fw.htm3WEeVwvA5k4dDK/eMFMu6ySEzD6W', '', null, '1', '2019-08-07 08:13:35', '2019-08-07 08:13:35');
INSERT INTO `t_user` VALUES ('17', 'Jose Profesinal 2 new', 'jose+profesional2@gmail.com', '$2y$10$M06vR3ChB0I7whsKPQQ4l.vGIdmSR7FyoW/tjFQwxF7JuB28x7Hca', '', null, '1', '2019-08-12 07:44:32', '2020-03-04 20:22:41');
INSERT INTO `t_user` VALUES ('18', 'Jose Greeny', 'jose.saiz@greenyplus.com', '$2y$10$8IfEcPtZ8xNOQbDvCUTx0O6fPC8s5XPVL0mHX/uCw2YQPIrf6zaym', '', null, '1', '2022-01-25 10:57:42', '2022-01-25 10:57:42');

-- ----------------------------
-- Table structure for users-original
-- ----------------------------
DROP TABLE IF EXISTS `users-original`;
CREATE TABLE `users-original` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- ----------------------------
-- Records of users-original
-- ----------------------------
INSERT INTO `users-original` VALUES ('1', 'Joseeee', 'jose@easydevel.com', '2019-08-06 09:22:48', '$2y$10$VOp2Fff4UcUt2uiz6v3G8OzvQylKapSzw/UKcVWYJNqrwHIHLL7JS', null, '2019-08-05 09:23:13', '2018-04-12 10:13:59');
