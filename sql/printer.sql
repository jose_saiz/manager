/*
Navicat MySQL Data Transfer

Source Server         : localhostMariaDB
Source Server Version : 50505
Source Host           : localhost:3307
Source Database       : greeny-manager

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2022-06-02 11:38:04
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for printer
-- ----------------------------
DROP TABLE IF EXISTS `printer`;
CREATE TABLE `printer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(20) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `neighborhood_factory_id` int(11) DEFAULT NULL,
  `filament_id` int(11) DEFAULT NULL,
  `state_id` int(11) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `name` varchar(50) COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `remark` varchar(255) COLLATE utf8mb3_unicode_ci DEFAULT '',
  `roll_end_datetime` datetime DEFAULT NULL,
  `roll_replacement_datetime` datetime DEFAULT NULL,
  `roll_weight` decimal(10,2) DEFAULT NULL,
  `nozzle_diameter` decimal(10,2) DEFAULT NULL,
  `available` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `orden` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=214 DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

-- ----------------------------
-- Records of printer
-- ----------------------------
INSERT INTO `printer` VALUES ('1', 'T-01', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-05-30 12:26:01', '2159.00', '0.60', '1', null, '2022-05-30 12:36:10', null);
INSERT INTO `printer` VALUES ('2', 'T-02', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-04-07 12:26:01', '2159.00', '0.60', '1', null, '2022-05-04 15:19:06', null);
INSERT INTO `printer` VALUES ('3', 'T-03', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-04-07 12:26:01', '2159.00', '0.60', '1', null, '2022-05-02 10:31:16', null);
INSERT INTO `printer` VALUES ('4', 'T-04', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-04-07 12:26:01', '2159.00', '0.60', '1', null, '2022-05-02 10:32:28', null);
INSERT INTO `printer` VALUES ('5', 'T-05', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-04-07 12:26:01', '2159.00', '0.60', '1', null, '2022-05-02 08:48:07', null);
INSERT INTO `printer` VALUES ('6', 'T-06', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-04-07 12:26:01', '2159.00', '0.60', '1', null, '2022-05-02 08:37:50', null);
INSERT INTO `printer` VALUES ('7', 'T-07', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-30 12:34:36', null);
INSERT INTO `printer` VALUES ('8', 'T-08', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-25 12:51:45', null);
INSERT INTO `printer` VALUES ('9', 'T-09', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-25 12:51:43', null);
INSERT INTO `printer` VALUES ('10', 'T-10', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-25 12:51:39', null);
INSERT INTO `printer` VALUES ('11', 'T-11', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-25 12:52:36', null);
INSERT INTO `printer` VALUES ('12', 'T-12', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-02 20:40:43', null);
INSERT INTO `printer` VALUES ('13', 'T-13', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-02 20:40:59', null);
INSERT INTO `printer` VALUES ('14', 'T-14', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-02 20:41:13', null);
INSERT INTO `printer` VALUES ('15', 'T-15', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-02 20:41:16', null);
INSERT INTO `printer` VALUES ('16', 'T-16', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-02 20:41:19', null);
INSERT INTO `printer` VALUES ('17', 'T-17', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-02 20:41:55', null);
INSERT INTO `printer` VALUES ('18', 'T-18', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, '2022-05-02 20:41:58', null);
INSERT INTO `printer` VALUES ('19', 'T-19', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('20', 'T-20', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('21', 'T-21', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('22', 'T-22', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('23', 'T-23', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('24', 'T-24', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('29', 'T-29', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('56', 'G-56', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('57', 'G-57', '1', null, null, '1', null, '', '2022-05-20 09:45:13', '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('58', 'G-58', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('59', 'G-59', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('60', 'G-60', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('61', 'G-61', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('62', 'G-62', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('63', 'G-63', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('64', 'G-64', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('65', 'G-65', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('66', 'G-66', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('67', 'G-67', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('68', 'G-68', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('69', 'G-69', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('70', 'G-70', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('71', 'G-71', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('72', 'G-72', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('73', 'G-73', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('74', 'G-74', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('75', 'G-75', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('76', 'G-76', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('77', 'G-77', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('78', 'G-78', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('79', 'G-79', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('80', 'G-80', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('81', 'G-81', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('82', 'G-82', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('117', 'G-117', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('137', 'G-137', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('138', 'G-138', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('139', 'G-139', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('140', 'G-140', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('141', 'G-141', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('142', 'G-142', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('143', 'G-143', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('144', 'G-144', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('145', 'G-145', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('146', 'G-146', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('147', 'G-147', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('148', 'G-148', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('149', 'G-149', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('150', 'G-150', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('151', 'G-151', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('152', 'G-152', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('153', 'G-153', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('154', 'G-154', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('155', 'G-155', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('156', 'G-156', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('157', 'G-157', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('158', 'G-158', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('159', 'G-159', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('160', 'G-160', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('161', 'G-161', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('162', 'G-162', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('163', 'G-163', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('178', 'G-178', '1', null, null, '1', null, '', null, '2022-02-02 08:23:30', '2159.00', '0.60', '1', null, null, null);
INSERT INTO `printer` VALUES ('179', 'M-01', '1', null, null, '2', null, '', null, null, '4150.00', '0.60', '0', null, null, null);
INSERT INTO `printer` VALUES ('189', 'E-01', '3', null, null, '1', '', '', '2022-04-02 03:51:24', '2022-04-07 12:26:01', '1502.00', '0.60', '1', '2022-05-04 13:50:43', '2022-05-18 12:48:28', null);
INSERT INTO `printer` VALUES ('190', 'E-02', '3', null, null, '1', '', '', '2022-05-04 13:50:43', '2022-04-07 12:26:01', '1600.00', '0.60', '1', '2022-05-04 13:50:43', '2022-05-04 15:19:06', null);
INSERT INTO `printer` VALUES ('191', 'E-03', '3', null, null, '1', '', '', '2022-05-04 13:50:43', '2022-04-02 02:00:15', '1511.00', '0.60', '1', '2022-05-04 13:50:43', '2022-05-02 10:31:16', null);
INSERT INTO `printer` VALUES ('192', 'E-04', '3', null, null, '1', '', '', '2022-05-04 13:50:43', '2022-04-04 04:04:10', '2500.00', '0.60', '1', '2022-05-04 13:50:43', '2022-05-02 10:32:28', null);
INSERT INTO `printer` VALUES ('193', 'E-05', '3', null, null, '1', '', '', '2022-05-04 13:50:43', '2022-04-07 12:26:01', '2159.00', '0.60', '1', '2022-05-04 13:50:43', '2022-05-02 08:48:07', null);
INSERT INTO `printer` VALUES ('194', 'E-06', '3', null, null, '1', '', '', '2022-05-04 13:50:43', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-05-04 13:50:43', '2022-05-02 08:37:50', null);
INSERT INTO `printer` VALUES ('195', 'E-07', '3', null, null, '1', '', '', '2022-05-04 13:50:43', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-05-04 13:50:43', '2022-05-02 08:37:50', null);
INSERT INTO `printer` VALUES ('196', 'G-247', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('197', 'G-248', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('198', 'G-249', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('199', 'G-250', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('200', 'G-251', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('201', 'G-252', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('202', 'G-253', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('203', 'G-254', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('204', 'G-255', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('205', 'G-256', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('206', 'G-257', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('207', 'G-258', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('208', 'G-259', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('209', 'G-260', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('210', 'G-261', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('211', 'G-278', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('212', 'G-262', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
INSERT INTO `printer` VALUES ('213', 'G-263', '1', null, null, '1', '', '', '2022-06-02 11:33:23', '2022-02-02 08:23:30', '2159.00', '0.60', '1', '2022-06-02 11:33:23', '2022-06-02 11:33:23', null);
