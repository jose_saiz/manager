<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/






use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Route;
use App\Vendor\Saizfact\Locale\LocalizationSeo;

$localization = new LocalizationSeo();









    Route::get('/admin', function () {
        return view('welcome');
    });
    /**
     * URL de supervisión de las rutas.
     * Funciona según el dominio donde te encuentres
     */
    Route::get('routes', function() {
        if (Config::get('app.env') != 'production') {
            $routeCollection = Route::getRoutes();
            
            echo '<h3>Lista de Rutas del dominio <i>' . $_SERVER['SERVER_NAME'] . '</i></h3>';
            echo "<table style='width:100%'>";
            echo "<tr>";
            echo "<td width='33%'><h4>HTTP Method</h4></td>";
            echo "<td width='33%'><h4>Route</h4></td>";
            echo "<td width='33%'><h4>Name</h4></td>";
            echo "</tr>";
            foreach ($routeCollection as $value) {
                if (in_array($value->uri, array('routes', 'routes-all'))) {
                    continue;
                }
                if ($_SERVER['SERVER_NAME'] != $value->domain()) {
                    continue;
                }
                echo "<tr>";
                echo "<td>" . $value->methods[0] . "</td>";
                echo "<td>" . $value->uri. "</td>";
                echo "<td>" . ($value->action['as'] ?? ''). "</td>";
                echo "</tr>";
            }
            echo "</table>";
        }
    });
        Route::get('routes-all', function() {
            if (Config::get('app.env') != 'production') {
                $routeCollection = Route::getRoutes();
                
                echo '<h3>Lista de Rutas de la aplicación</h3>';
                echo "<table style='width:100%'>";
                echo "<tr>";
                echo "<td width='25%'><h4>HTTP Method</h4></td>";
                echo "<td width='25%'><h4>Route</h4></td>";
                echo "<td width='25%'><h4>Name</h4></td>";
                echo "<td width='25%'><h4>Domain</h4></td>";
                echo "</tr>";
                foreach ($routeCollection as $value) {
                    if (in_array($value->uri, array('routes', 'routes-all'))) {
                        continue;
                    }
                    if (strstr($value->uri, '_debugbar')) {
                        continue;
                    }
                    echo "<tr>";
                    echo "<td>" . $value->methods[0] . "</td>";
                    echo "<td>" . $value->uri. "</td>";
                    echo "<td>" . ($value->action['as'] ?? ''). "</td>";
                    echo "<td>" . $value->domain(). "</td>";
                    echo "</tr>";
                }
                echo "</table>";
            }
        });

Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('/', 'Auth\LoginController@showLoginForm')->name('login.home');


Route::group(['prefix' => 'admin'], function () {
    

    
    //Login admin
    //Route::get('/login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::get('/login', 'Auth\LoginController@showLoginForm')->name('admin.login');
    //Route::post('/login', 'Auth\LoginController@login')->name('admin.login.submit');
    Route::post('/login', 'Auth\LoginController@login')->name('admin.login.submit');
    //Route::post('/logins', 'Auth\LoginControllers@login')->name('login');
    Route::get('/logout', 'Auth\LoginController@logout')->name('admin.logout');
    Route::get('/', function () {
        return redirect()->route('admin.login');
    });
        Route::get('/language/{language}', function ($language) {
            App::setLocale($language);
            
            session()->put('locale', $language);
            /*
            $path = Route::getCurrentRoute()->getPath();
            return redirect($path);
            */
            return redirect()->back();
            
            echo 'point: '.App::getLocale();;
    })->name('admin.language');
    // Envio de enlace para reestablecer contraseña
    /*
    Route::post('/admin-remember-password','Admin\ForgotPasswordController@sendVerification')->name('admin-enviar-verificacion');
    Route::get('/admin-remember-password/{token}','Admin\ForgotPasswordController@changeRememberPassword')->name('change-remember-password');
    
    Route::post('/admin-change-password','Admin\ForgotPasswordController@changePassword')->name('admin-change-password');
    */
    //Password reset routes
    /*
    Route::post('/password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('admin.password.email');
    
    Route::get('/password/reset', 'Admin\ForgotPasswordController@showLinkRequestForm')->name('admin.password.request');
    
    Route::post('/password/reset', 'Auth\ResetPasswordController@reset');
    Route::get('/password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('admin.password.reset');
    */
    //Admin Users Roles
    Route::get('/users/roles/json', 'Admin\RoleController@indexJson')->name('roles_json');
    Route::resource('users/roles', 'Admin\RoleController', [
        'names' => [
            'index' => 'roles',
            'create' => 'roles_create',
            'store' => 'roles_store',
            'destroy' => 'roles_destroy',
            'show' => 'roles_show',
        ]
    ]);
    
    //Admin Users Permissions
    Route::get('/users/permissions/json', 'Admin\PermissionController@indexJson')->name('permissions_json');
    Route::resource('users/permissions', 'Admin\PermissionController', [
        'names' => [
            'index' => 'permissions',
            'create' => 'permissions_create',
            'store' => 'permissions_store',
            'destroy' => 'permissions_destroy',
            'show' => 'permissions_show',
        ]
    ]);
    
    //Admin Users
    Route::get('/users/json', 'Admin\UserController@indexJson')->name('users_json');
    Route::resource('users', 'Admin\UserController', [
        'names' => [
            'index' => 'users',
            'create' => 'users_create',
            'store' => 'users_store',
            'destroy' => 'users_destroy',
            'show' => 'users_show',
            'verification' => 'verification',
        ]
    ]);
    
    
    //Admin Printers Categorias
    Route::get('/printers/categorias/json', 'Admin\PrinterCategoryController@indexJson')->name('admin_printers_categorias_json');
    Route::resource('/printers/categorias', 'Admin\PrinterCategoryController', [
        'parameters' => [
            'language' => 'language',
        ],
        'names' => [
            'index' => 'admin_printers_categorias',
            'create' => 'admin_printers_categorias_create',
            'store' => 'admin_printers_categorias_store',
            'destroy' => 'admin_printers_categorias_destroy',
            'show' => 'admin_printers_categorias_show',
        ]
    ]);
    
    
    //Admin Printers 
    Route::get('/printers/parts', 'Admin\PrinterController@indexParts')->name('admin_printers_parts');
    
    Route::get('/printers/json', 'Admin\PrinterController@indexJson')->name('admin_printers_json');
    Route::post('/printers/reordertable', 'Admin\PrinterController@reorderTable')->name('printers_reorder');
    Route::resource('/printers', 'Admin\PrinterController', [
            'parameters' => [
                'language' => 'language',
            ],
            'names' => [
            'index' =>   'admin_printers',
            'create' =>  'admin_printers_create',
            'store' =>   'admin_printers_store',
            'destroy' => 'admin_printers_destroy',
            'show' =>    'admin_printers_show',
        ]
    ]);
    
    //Admin Orders
    Route::get('/orders/json', 'Admin\OrderController@indexJson')->name('admin_orders_json');
    Route::post('/orders/reordertable', 'Admin\OrderController@reorderTable')->name('orders_reorder');
    Route::resource('/orders', 'Admin\OrderController', [
        'parameters' => [
            'language' => 'language',
        ],
        'names' => [
            'index' =>   'admin_orders',
            'create' =>  'admin_orders_create',
            'store' =>   'admin_orders_store',
            'destroy' => 'admin_orders_destroy',
            'show' =>    'admin_orders_show',
        ]
    ]);
    
    
    //Admin Order Lines
    Route::get('/order_lines/{order_id}/json', 'Admin\OrderController@indexOrderLinesJson')->name('admin_order_lines_json');
    //Admin Final Products
    Route::get('/final_products/{order_id}/json', 'Admin\OrderController@indexFinalProductsJson')->name('admin_final_products_json');
    //Admin Parts
    Route::get('/parts_order/{order_id}/json', 'Admin\OrderController@indexPartsJson')->name('admin_parts_order_json');
    
    //Admin Parts Graph
    Route::get('/parts/graph',  'Admin\PartController@graph')->name('admin_parts_graph');
    //Admin Parts State
    Route::get('/parts/state', 'Admin\PartController@state')->name('admin_parts_state');
    //Admin Parts State
    Route::post('/parts/state', 'Admin\PartController@state')->name('admin_parts_state_post');
    //Admin Parts:  Assing + Unassign
    Route::get('/parts/save_assign',  'Admin\PartController@saveAssign')->name('admin_parts_save_assign');
    Route::get('/parts/clear_assign',  'Admin\PartController@clearAssign')->name('admin_parts_clear_assign');
    //Admin Parts:  Paly + Cancel
    //Route::get('/parts/play/{$start_date_time}/{printer_id}/{state_id}/{button}/{start}/{end}/{how}/{part_id}','Admin\PartController@play')
                  
    Route::post('/parts/can_play','Admin\PartController@canPlay')->name('admin_parts_can_play');
    Route::get('/parts/play/{start_date_time}/{printer_id}/{part_id}/{state_id}','Admin\PartController@play')->name('admin_parts_play');
    Route::get('/parts/cancel/{start_date_time}/{part_id}/{printer_id}','Admin\PartController@cancel')->name('admin_parts_cancel');
    
    Route::post('/parts/can_play_state','Admin\PartController@canPlayState')->name('admin_parts_can_play_state');
    Route::get('/parts/play_state/{start_date_time}/{printer_id}/{part_id}/{state_id}','Admin\PartController@playStateGet')->name('admin_parts_play_state_get');
    Route::post('/parts/play_state','Admin\PartController@playStatePost')->name('admin_parts_play_state_post');
    Route::get('/parts/cancel_state/{start_date_time}/{part_id}/{printer_id}','Admin\PartController@cancelState')->name('admin_parts_cancel_state');
    Route::post('/parts/cancel_state','Admin\PartController@cancelStatePost')->name('admin_parts_cancel_state_post');
    
    //Set start datetime
    Route::post('/set_start_datetime', 'Admin\PartController@setStartDatetime')->name('admin_set_start_datetime');
    Route::post('/set_start_datetime_state', 'Admin\PartController@setStartDatetimeState')->name('admin_set_start_datetime_state');
    
    
    //Admin Parts
    Route::get('/parts/json', 'Admin\PartController@indexJson')->name('admin_parts_json');
    Route::post('/parts/reordertable', 'Admin\PartController@reorderTable')->name('parts_reorder');
    Route::resource('/parts', 'Admin\PartController', [
        'parameters' => [
            'language' => 'language',
        ],
        'names' => [
            'index' => 'admin_parts',
            'create' => 'admin_parts_create',
            'store' => 'admin_parts_store',
            'destroy' => 'admin_parts_destroy',
            'show' => 'admin_parts_show',
        ]
    ]);
    
    
    Route::get('/files/json', 'Admin\FileController@indexJson')->name('admin_files_json');
    Route::post('/files/reordertable', 'Admin\FileController@reorderTable')->name('files_reorder');
    Route::resource('/files', 'Admin\FileController', [
        'parameters' => [
            'language' => 'language',
        ],
        'names' => [
            'index' => 'admin_files',
            'create' => 'admin_files_create',
            'store' => 'admin_files_store',
            'destroy' => 'admin_files_destroy',
            'show' => 'admin_files_show',
        ]
    ]);
    Route::get('/files/api', 'Admin\FileController@indexApi')->name('admin_files_api');
    
    //Admin Tests Categorias
    Route::get('/tests/categorias/json', 'Admin\TestCategoriaController@indexJson')->name('admin_tests_categorias_json');
    Route::resource('tests/categorias', 'Admin\TestCategoriaController', [
        'names' => [
            'index' => 'admin_tests_categorias',
            'create' => 'admin_tests_categorias_create',
            'store' => 'admin_tests_categorias_store',
            'destroy' => 'admin_tests_categorias_destroy',
            'show' => 'admin_tests_categorias_show',
        ]
    ]);
    
    //Admin Tests
    Route::get('/tests/json', 'Admin\TestController@indexJson')->name('admin_tests_json');
    Route::post('/tests/reordertable', 'Admin\TestController@reorderTable')->name('tests_reorder');
    Route::resource('tests', 'Admin\TestController', [
        'names' => [
            'index' => 'admin_tests',
            'create' => 'admin_tests_create',
            'store' => 'admin_tests_store',
            'destroy' => 'admin_tests_destroy',
            'show' => 'admin_tests_show',
        ]
    ]);
    
    //Admin Media
    Route::post('/media/filter', 'Admin\MediaController@filter')->name('media_filter');
    Route::resource('media', 'Admin\MediaController', [
        'parameters' => [
            'media' => 'media',
        ],
        'names' => [
            'index' => 'media',
            'create' => 'media_create',
            'store' => 'media_store',
            'destroy' => 'media_destroy',
            'show' => 'media_show',
        ]
    ]);
    
    //Admin Proyectos Categorias
    Route::get('/proyectos/categorias/json', 'Admin\ProyectoCategoriaController@indexJson')->name('admin_proyectos_categorias_json');
    Route::resource('proyectos/categorias', 'Admin\ProyectoCategoriaController', [
        'names' => [
            'index' => 'admin_proyectos_categorias',
            'create' => 'admin_proyectos_categorias_create',
            'store' => 'admin_proyectos_categorias_store',
            'destroy' => 'admin_proyectos_categorias_destroy',
            'show' => 'admin_proyectos_categorias_show',
        ]
    ]);
    
    //Admin Proyectos
    Route::get('/proyectos/json', 'Admin\ProyectoController@indexJson')->name('admin_proyectos_json');
    Route::post('/proyectos/reordertable', 'Admin\ProyectoController@reorderTable')->name('proyectos_reorder');
    Route::resource('proyectos', 'Admin\ProyectoController', [
        'names' => [
            'index' => 'admin_proyectos',
            'create' => 'admin_proyectos_create',
            'store' => 'admin_proyectos_store',
            'destroy' => 'admin_proyectos_destroy',
            'show' => 'admin_proyectos_show',
        ]
    ]);
    
    
    
    //Admin Configuraciones
    Route::get('/config/json', 'Admin\ConfigController@indexJson')->name('config_json');
    Route::post('/config/reordertable', 'Admin\ConfigController@reorderTable')->name('config_reorder');
    Route::resource('configs', 'Admin\ConfigController', [
        'names' => [
            'index' => 'config',
            'create' => 'config_create',
            'store' => 'config_store',
            'destroy' => 'config_destroy',
            'show' => 'config_show',
        ]
    ]);
    
    
    //Admin Workdays
    Route::get('/workdays/json', 'Admin\WorkdayController@indexJson')->name('admin_workdays_json');
    Route::post('/workdays/reordertable', 'Admin\WorkdayController@reorderTable')->name('workdays_reorder');
    Route::resource('/workdays', 'Admin\WorkdayController', [
        'parameters' => [
            'language' => 'language',
        ],
        'names' => [
            'index' =>   'admin_workdays',
            'create' =>  'admin_workdays_create',
            'store' =>   'admin_workdays_store',
            'destroy' => 'admin_workdays_destroy',
            'show' =>    'admin_workdays_show',
        ]
    ]);
    

});
    //FRONT
    
    //Errores
    // TODO En la página 404, mostrar los destacados, tal y como sale en Invision
    // https://projects.invisionapp.com/d/main#/console/15283866/334294291/preview
    
    Route::view('/error/acceso-restringido', 'errors.401')->name('401');
    Route::view('/error/pagina-no-encontrada', 'errors.404')->name('404');
    Route::view('/error/pagina-expirada', 'errors.419')->name('419');
    Route::view('/error/demasiadas-peticiones', 'errors.429')->name('429');
    Route::view('/error/fallo-del-servidor', 'errors.500')->name('500');
    Route::view('/error/servicio-no-disponible', 'errors.503')->name('503');
    
    Route::domain(Config::get('app.app_url'))->prefix($localization->setLocale())->middleware(['localize'])->group(function () use ($localization) {
    //Route::group(['prefix' => '{locale}'], function() {
        
        Route::get($localization->transRoute('routes.printers'),'Admin\PrinterController@index' )->name('printers');
        
        Route::get($localization->transRoute('routes.test'),'Front\TestController@index' )->name('test');
        Route::get($localization->transRoute('routes.tests-category'),'Front\TestCategoriaController@index' )->name('tests-category');
        Route::get($localization->transRoute('routes.tests'),'Front\TestsController@index' )->name('tests');
        
        Route::get($localization->transRoute('routes.project'),'Front\ProyectoController@index' )->name('project');
        Route::get($localization->transRoute('routes.projects-category'),'Front\ProyectoCategoriaController@index' )->name('projects-category');
        Route::get($localization->transRoute('routes.projects'),'Front\ProyectosController@index' )->name('projects');
        
    });
    
    //Route::get($localization->transRoute('routes.academy-noticia'), 'Front\Academy\NoticiaController@noticia')->name('academy-noticia');