<?php 
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////  S O R T //////////////////////////////////////////////////
function sort_by_hours_asc ($a, $b) {
    return $a['estimated_printing_hours'] - $b['estimated_printing_hours'];
}

function sort_by_hours_desc ($a, $b) {
    return  $b['estimated_printing_hours'] - $a['estimated_printing_hours'];
}

function sort_by_hours_minutes_asc ($a, $b) {
    $a_total_minutes = $a['estimated_printing_hours']*60+$a['estimated_printing_minutes'];
    $b_total_minutes = $a['estimated_printing_hours']*60+$b['estimated_printing_minutes'];
    return $a_total_minutes - $b_total_minutes;
}

function sort_by_hours_minutes_desc ($a, $b) {
    $a_total_minutes = $a['estimated_printing_hours']*60+$a['estimated_printing_minutes'];
    $b_total_minutes = $a['estimated_printing_hours']*60+$b['estimated_printing_minutes'];
    return $b_total_minutes - $a_total_minutes;
}

function sort_by_minutes_asc ($a, $b) {
    return  $a['minutes'] - $b['minutes'];
}

function sort_by_minutes_desc ($a, $b) {
    return  $b['minutes'] - $a['minutes'];
}



function sort_by_datetime ($a, $b) {
    $t1 = strtotime($a['datetime']->format('Y-m-d H:i:s'));
    $t2 = strtotime($b['datetime']->format('Y-m-d H:i:s'));
    return $t1 - $t2;
    //return $a['datetime'] - $b['datetime'];
    /*
     if ($a['datetime'] == $b['datetime']) {
     return 0;
     }
     
     return $b['datetime'] > $b['datetime'] ? -1 : 1;
     */
    
}

function cmp($a, $b) {
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}


function sort_timeline_by_slot_size_asc ($a,$b)
{
    if ($a->minutes == $b->minutes) {
        return 0;
    } else if ($a->minutes > $b->minutes)
        return 1;
        else
            return - 1;
            
}
function sort_timeline_by_slot_size_desc ($a,$b)
{
     
    if ($a->minutes == $b->minutes) {
        return 0;
    } else if ($a->minutes < $b->minutes)
        return 1;
        else
            return - 1;
            
}
/*
function sort_timeline_by_slot_size_asc ($a,$b)
{
    $minutes_a = DateTimeG::newDateTime($a->start)->diffMinutes( DateTimeG::newDateTime($a->end));
    $minutes_b = DateTimeG::newDateTime($b->start)->diffMinutes( DateTimeG::newDateTime($b->end));
    
    if ($minutes_a == $minutes_b) {
        return 0;
    } else if ($minutes_a > $minutes_b)
        return 1;
        else
            return - 1;
    
}
function sort_timeline_by_slot_size_desc ($a,$b)
{
    $minutes_a = DateTimeG::newDateTime($a->start)->diffMinutes( DateTimeG::newDateTime($a->end));
    $minutes_b = DateTimeG::newDateTime($b->start)->diffMinutes( DateTimeG::newDateTime($b->end));
    
    if ($minutes_a == $minutes_b) {
        return 0;
    } else if ($minutes_a < $minutes_b)
        return 1;
        else
            return - 1;
            
}
*/
function startAscendingComparison($val1, $val2)
{
    if ($val1->start == $val2->start) {
        return 0;
    } else if ($val1->start > $val2->start)
        return 1;
        else
            return - 1;
}


function startDescendingComparison($val1, $val2)
{
    if ($val1->start == $val2->start) {
        return 0;
    } else if ($val1->start < $val2->start)
        return 1;
        else
            return - 1;
}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// D A T A   T I M E //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DateTimeG extends DateTime
{
    
    
    public function __construct($time='now', $timezone='Europe/Madrid')
    {
        parent::__construct($time, new DateTimeZone($timezone));
        
    }
    
    
    /**
     * Return Date in ISO8601 format
     *
     * @return String
     */
    public function __toString(): String {
        return $this->format('Y-m-d H:i:s');
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////  B A S I C S /////////////////////////////////////////////////
    public function createDateTime():DateTimeG{
        //return new DateTimeG($this);
        return new DateTimeG($this);
    }
    
    public static function newDateTime(string $date_time):DateTimeG{
        //$rdo = new DateTimeG($date_time);
        $rdo = new DateTimeG($date_time);
        return $rdo;
    }
    
    
    public static function newFromformatUTC (string $datetime){
        $new = DateTimeG::newDateTime($datetime);
        if (strpos($datetime, 'T')) {
            $start = explode("T", $datetime);
            $date = explode('-', $start[0]);
            $time = explode(':', $start[1]);
            $new->setDate($date[0], $date[1], $date[2]);
            $new->setTime($time[0], $time[1]);
        }
        return $new;
    }
    
    
    public static function CleanDatetimeForBD(string $date_time):string{
        $dateTime = new DateTimeG($date_time);
        $date = $dateTime->format('Y-m-d');
        $time = $dateTime->format('H:i:s');
        return $date .' ' . $time;
    }
    
    public function minuteOfTheDay():int{
        return  $this->format('H')*60+$this->format('i');
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////  O V E R L A P S /////////////////////////////////////////////
    
    
    public static function isOverlap(DateTimeG $start_time1, DateTimeG $end_time1, DateTimeG $start_time2, DateTimeG $end_time2)
    {
        $result = (($start_time1) <= ($end_time2) && ($start_time2) <= ($end_time1) ? true : false);
        return $result;
    }
    
    public static function isOverlapOrWrap(DateTimeG $start_time1, DateTimeG $end_time1, DateTimeG $start_time2, DateTimeG $end_time2)
    {
        //Si la datetime  2 envuelve al 1
        if (($start_time2 < $start_time1) && ($end_time2 > $end_time1)) {
            return true;
        }
        //Si la datetime  1 envuelve al 2
        if (($start_time2 > $start_time1) && ($end_time2 < $end_time1)) {
            return true;
        }
        return DateTimeG::isOverlap($start_time1, $end_time1, $start_time2, $end_time2);
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////  N I G H T S /////////////////////////////////////////////////
    
    function isOverlapAtNight()
    {
        
        $night_from = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_from = new DateTime($this->date);
        $day_from = $night_from->format('w');
        $night_from->setTime(conf_working_days()[$day_from]['to']['hour'], conf_working_days()[$day_from]['to']['minute']);
        if ($this->format('H') <= 12) {
            $night_from->modify('-1 days');
        }
        
        $night_to = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_to = new DateTime($this->date);
        $day_to = $night_to->format('w');
        $night_to->setTime(conf_working_days()[$day_to]['from']['hour'], conf_working_days()[$day_to]['from']['minute']);
        if ($this->format('H') > 12) {
            $night_to->modify('+1 days');
        }
        
        
        $result = (($this) <= ($night_to) && ($night_from) <= ($this) ? true : false);
        return $result;
    }
    function isOverlapAtNightFlex($minutesFlex=0)
    {
        
        $night_from = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_from = new DateTime($this->date);
        $day_from = $night_from->format('w');
        $night_from->setTime(conf_working_days()[$day_from]['to']['hour'], conf_working_days()[$day_from]['to']['minute']);
        if ($this->format('H') <= 12) {
            $night_from->modify('-1 days');
        }
        
        $night_to = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_to = new DateTime($this->date);
        $day_to = $night_to->format('w');
        $night_to->setTime(conf_working_days()[$day_to]['from']['hour'], conf_working_days()[$day_to]['from']['minute']);
        if ($this->format('H') > 12) {
            $night_to->modify('+1 days');
        }
        
        //$night_to->modify('-1 hours');
        $night_to->modify('-' .$minutesFlex.' minutes');
        
        $result = (($this) <= ($night_to) && ($night_from) <= ($this) ? true : false);
        return $result;
    }
    

    function OverlapAtNight():array
    {
        get_object_vars($this);
        
        //$night_from = new DateTimeG($this->format('Y-m-d H:i:s'));
        $night_from = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_from = new DateTime($this->date);
        $day_from = $night_from->format('w');
        $night_from->setTime(conf_working_days()[$day_from]['to']['hour'], conf_working_days()[$day_from]['to']['minute']);
        if ($this->format('H') <= 12) {
            $night_from->modify('-1 days');
        }
        
        //$night_to = new DateTimeG($this->format('Y-m-d H:i:s'));
        $night_to = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_to = new DateTime($this->date);
        $day_to = $night_to->format('w');
        $night_to->setTime(conf_working_days()[$day_to]['from']['hour'], conf_working_days()[$day_to]['from']['minute']);
        if ($this->format('H') > 12) {
            $night_to->modify('+1 days');
        }
        
        $result = (($this) <= ($night_to) && ($night_from) <= ($this) ? true : false);
        
        if ($result){
            return array('night_from'=>$night_from,'night_to'=>$night_to);
        }
        return  false;
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////  D I F F ///////////////////////////////////////////////////
    function rangeTotalM(DateTimeG $end)
    {
        $interval = $this->diff($end);
        $days = $interval->format('%d');
        $hours = $interval->format('%h');
        $minutes = $interval->format('%i');
        return $days * 24 * 60 + $hours * 60 + $minutes;
    }
    
    
    function diffRangeHM(DateTimeG $end):array
    {
        $hours = $this->diff($end)->format('%h');
        $minutes = $this->diff($end)->format('%i');
        
        if ($this->diff($end)->format('%d') >=1){
            $hours += $this->diff($end)->format('%d')*24;
        }
        
        return array(
            'hours' => $hours,
            'minutes' => $minutes
        );
    }
    
    function diffMinutes(DateTimeG $end):int
    {
        $hours = $this->diff($end)->format('%h');
        $minutes = $this->diff($end)->format('%i');
        
        if ($this->diff($end)->format('%d') >=1){
            $hours += $this->diff($end)->format('%d')*24;
        }
        
        
        return $hours*60 + $minutes;
    }
    
    
    function diffDaysOnly(DateTimeG $end):int
    {
        $days = $this->diff($end)->format('%a');
        
        return $days;
    }
    function diffHoursOnly(DateTimeG $end):int
    {
        $days = $this->diff($end)->format('%h');
        
        return $days;
    }
    
    function diffMinutesOnly(DateTimeG $end):int
    {
        $days = $this->diff($end)->format('%i');
        
        return $days;
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////// E S T I M A T E D ///////////////////////////////////////////////
    
    function estimatedPrintingEnd(array $pending_part): DateTimeG
    {
        $estimated_printing_end = new DateTimeG($this->format('Y-m-d H:i:s'));
        $estimated_printing_end->modify("+" . $pending_part['estimated_printing_hours'] . " hours");
        $estimated_printing_end->modify("+" . $pending_part['estimated_printing_minutes'] . " minutes");
        return $estimated_printing_end;
    }
    function estimatedPrintingEndSlot(PrinterSlot $slot): DateTimeG
    {
        return estimatedPrintingEnd($this, $slot->toPendingPart());
    }
    
    function estimatedPrintingStart(array $pending_part): DateTimeG
    {
        $estimated_printing_start = new DateTimeG($this->format('Y-m-d H:i:s'));
        $estimated_printing_start->modify("-" . $pending_part['estimated_printing_hours'] . " hours");
        $estimated_printing_start->modify("-" . $pending_part['estimated_printing_minutes'] . " minutes");
        return $estimated_printing_start;
    }
    
    function estimatedPrintingStartSlot(PrinterSlot $slot): DateTimeG
    {
        return estimatedPrintingStart($this, $slot->toPendingPart());
    }
    
    
    function workDay(): array
    {
        $workday_start = new DateTimeG($this->format('Y-m-d'));
        $workday_start->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
        $workday_end = new DateTimeG($this->format('Y-m-d'));
        $workday_end->setTime(conf_workday()['to_hour'], conf_workday()['to_minute']);
        
        return array(
            'start' => $workday_start,
            'end' => $workday_end
        );
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    
    

    
    
    
    /**
     * Return Age in Years
     *
     * @param Datetime|String $now
     * @return Integer
     */
    public function getAge( $now = 'NOW'):Int {
        return $this->diff($now)->format('%y');
    }
    
    
    
    
    
    
}

?>