<?php
include 'part.php';
include 'database.php';
//include 'datetime.php';
include 'DateTimeG.php';
include 'printerslot.php';
include 'printertimeline.php';
include 'printermatrix.php';
include 'configuration.php';
include 'permutations.php';

set_time_limit(120);
////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////// REASSIGN PART  SEND REQUEST INIT FORM ////////////////////////
if (isset($_GET['part']) && isset($_GET['start-date-time'])) {
    //Inicializamos el formulario con el id de la pieza seleccionada.
    $init_form = PrintersMatrix::reassignPartsAllSendRequestInitForm($_GET['part']);
    
    /*
    $sql = DataBase::querySelectReassignPart($_GET['part']);
    $init_form['part'] = DataBase::getConnection()->query($sql)->fetch_assoc();

    $init_form['start'] = new DateTimeG($init_form['part']['start_datetime']);
    //$init_form['start'] = new DateTimeG($init_form['part']['start_datetime']);
    $init_form['end'] = new DateTimeG($init_form['part']['end_datetime']);
    //$init_form['end'] = new DateTimeG($init_form['part']['end_datetime']);
    get_object_vars($init_form['start']);
    get_object_vars($init_form['end']);
    $init_form['min'] = new DateTimeG($init_form['start']->format('Y-m-d H:i:s'));
    //$init_form['min'] = new DateTimeG($init_form['start']->date);
    $init_form['min']->modify('-1 years');
    $init_form['max'] =  new DateTimeG($init_form['start']->format('Y-m-d H:i:s'));
    //$init_form['max'] = new DateTimeG($init_form['start']->date);
    $init_form['max']->modify('+10 years');

    $init_form['slot_interval'] = $init_form['start']->diff($init_form['end']);
    $init_form['slot_total_hours'] = $init_form['slot_interval']->format('%d') * 24 + $init_form['slot_interval']->format('%h');

    $sql = sprintf('
            SELECT  *
            FROM    printer AS p
            WHERE   available = 1
            ORDER BY id
            ;
        ');
    $init_form['printers'] = DataBase::getConnection()->query($sql);
    foreach ($init_form['printers'] as $printer) {
    }
    $sql = sprintf('
            SELECT  *
            FROM    state AS s
            WHERE   state_id_type = 3
            ORDER BY id
            ;
        ');
    $init_form['states'] = DataBase::getConnection()->query($sql);
    */
////////////////////////////////////////////////////////////////////////////////////////////////    
///////////////////////////////////// REASSIGN PART SEND REQUEST ///////////////////////////////
} elseif (isset($_GET['part_id']) && isset($_GET['printer_id']) && isset($_GET['state_id']) && isset($_GET['start']) && isset($_GET['end']) && isset($_GET['start-date-time']) && isset($_GET['how'])) {
    //Estamos de vuelta del formulario o hemos pulsado el botón de play o cancel.
    /////////////////// reasing part send request Init /////////////////////////
    $sql = DataBase::querySelectReassignPart($_GET['part_id']);
    $pending_part = DataBase::getConnection()->query($sql)->fetch_assoc();
    
    $start_new = DateTimeG::newFromformatUTC($_GET['start']);
    $end_new = DateTimeG::newFromformatUTC($_GET['end']);
    
    $start_datetime = new DateTimeG($_GET['start-date-time']);
    
    if (isset($_GET['button']) && ($_GET['button'] == 'cancel')){
    ////////////////// reasing part send request Cancel ////////////////////////
    //Pulsamos el botón Cancelar: La pieza ha fallado, no se ha impreso correctamente.
        
        reassignAll($start_datetime,$_GET['button'], conf_neighborhood_factory_id());
        //Guardar la nueva printer time line como asignada
    }elseif (isset($_GET['button']) && ($_GET['button'] == 'play')){
    /////////////////// reasing part send request Play /////////////////////////
    //Pulsamos el botón Play: Se ha iniciado la impresión de la pieza
        $end_new = $start_datetime->estimatedPrintingEnd($pending_part);
        
        $sql = sprintf("UPDATE part
                        SET printer_id = " . $_GET['printer_id'] . ", state_id = " . $_GET['state_id'] . " ,
                            start_datetime = '" . $start_datetime->format('Y-m-d H:i:s') . "', end_datetime = '" . $end_new->format('Y-m-d H:i:s') . "', initiated = 1
                        WHERE id = " . $_GET['part_id']);
        $result = DataBase::getConnection()->query($sql);
        
        //Primero intentamos desplazar todas la piezas de la impresora hacia la derecha. Teniendo en cuenta las noches.
        //Si el rendimiento no  es aceptable in
        reassignAll($start_datetime,$_GET['button'], conf_neighborhood_factory_id());
       
    }else{
    
    ////////////////// reasing part send request Form //////////////////////////
    //Se ha enviado el formulario y hay que porcesarlo
        if ($_GET['how'] == 'from_start') {
            $end_new = $start_new->estimatedPrintingEnd($pending_part);
        } else {
            $start_new = $end_new->estimatedPrintingStart($pending_part);
        }
    
        get_object_vars($start_new);
        get_object_vars($end_new);
    
    
        $sql = sprintf("UPDATE part
                        SET printer_id = " . $_GET['printer_id'] . ", state_id = " . $_GET['state_id'] . " , initiated = 1  
                            start_datetime = '" . $start_new->format('Y-m-d H:i:s') . "', end_datetime = '" . $end_new->format('Y-m-d H:i:s') . "' 
                        WHERE id = " . $_GET['part_id']);
        $result = DataBase::getConnection()->query($sql);
        
        //Apartar las pendientes de asignar
        $sql = sprintf("UPDATE part
                    SET state_id = 7
                    WHERE state_id = 8");
        $result = DataBase::getConnection()->query($sql);
        //ORDER BY p.weight DESC,p.order_id  ASC
        
        $neighborhood_factory_id = 1;
        $printersMatrix = new PrintersMatrix($neighborhood_factory_id, $start_datetime);
        
        $printersMatrix->chargePrintersMatrix($start_datetime);
        
        
        
        //reassignAll($printersMatrix,$start_datetime);
        
        $printersMatrix->clearAssignPrintersMatrix();
        $printersMatrix->recoverSeparatedUnssignedParts();
        $printersMatrix->chargePrintersMatrix($start_datetime);
        $printersMatrix->assignPrintersSlots();
        $printersMatrix->saveAssignPrintersMatrix();
        
        
        
    }
    header("Location: ".URL."/index.php?reassign=true&start-date-time=" . $_GET['start-date-time']);
} else {
    echo "Error: Data Missing";
    die();
}


function reassignAll($start_datetime,$action, $neighborhood_factory_id){
    
    $printersMatrix = new PrintersMatrix($neighborhood_factory_id, $start_datetime);
    
    //Apartar las unassinged: state 8->7
    $printersMatrix->separatedUnassignedParts();
    
    //Solo se cargan los asignados: 9
    $printersMatrix->chargePrintersMatrix($start_datetime);
    
    prepareToReassignAll($printersMatrix,$start_datetime,$action);
    
    //$printersMatrix->clearAssignPrintersMatrix();
    
    $printersMatrix->chargePrintersMatrix($start_datetime);
    $printersMatrix->assignPrintersSlots();
    $printersMatrix->saveAssignPrintersMatrix();
    $printersMatrix->recoverSeparatedUnssignedParts();
    
}


function prepareToReassignAll($printersMatrix,$start_datetime,$action){
    foreach ($printersMatrix as $printeTimeline){
        foreach ($printeTimeline as $slot){
            //Si no se ha inicado su impression y no es la pieza que estamos arrancando
            if (($slot->initiated == 0) && ($slot->part <> $_GET['part_id'] )){
                //Desasignamos la pieza
                $sql = sprintf("UPDATE part
                    SET state_id = 8 ,
                        printer_id = NULL, start_datetime = NULL, end_datetime = NULL
                    WHERE id = " . $slot->part);
                $result = DataBase::getConnection()->query($sql);
            }elseif (($slot->initiated == 1) && ($slot->part == $_GET['part_id'])){
                //Falta Asignar la nueva pieza a partir de la fecha actual.
                //Hay que recupera la pieza de la bdd, para disponer del tiempo exacto, no el de la timeline
                $sql = DataBase::querySelectReassignPart($_GET['part_id']);
                $init_form['part'] = DataBase::getConnection()->query($sql)->fetch_assoc();
                
                $init_form['end'] = $start_datetime->estimatedPrintingEnd($init_form['part']);
                $initiated = 0;
                $state_id = 8;
                if ($action == "play"){
                    $initiated = 1;
                    $state_id = 9;
                }
                $sql = sprintf('UPDATE part
                    SET state_id = '.$state_id.' , initiated = '.$initiated.' , start_datetime ="'.$start_datetime->format('Y-m-d H:i:s').'", end_datetime = "'.$init_form['end']->format('Y-m-d H:i:s').'"
                    WHERE id = ' . $slot->part);
                $result = DataBase::getConnection()->query($sql);
            }
        }
    }
}

?>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="js/jquery-3.6.0.js"></script>
<meta charset="utf-8" />
<title>Reassign Part</title>
<style>
form {
	/* Just to center the form on the page */
	margin: 0 auto;
	width: 500px;
	/* To see the outline of the form */
	padding: 1em;
	border: 1px solid #ccc;
	border-radius: 1em;
	box-shadow: 0 0 20px #666;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

legend {
	/* Just to center the form on the page */
	margin: 0 auto;
	width: 300px;
	/* To see the outline of the form */
	padding: 1em;
	border: 1px solid #ccc;
	border-radius: 1em;
	background: white;
	box-shadow: 0 0 5px #666;
}

fieldset {
	margin: 20px;
	padding: 0 10px 10px;
	border: 1px solid #ccc;
	border-radius: 8px;
	box-shadow: 0 0 10px #666;
	padding-top: 10px;
}

ul {
	list-style: none;
	padding: 0;
	margin: 0;
}

form li+li {
	margin-top: 1em;
}

label {
	/* To make sure that all labels have the same size and are properly aligned */
	display: inline-block;
	width: 90px;
	text-align: right;
	margin-top: 7px;
	margin-bottom: 7px;
}

input, select, textarea {
	/* To make sure that all text fields have the same font settings By default, textareas have a monospace font */
	font: 1em sans-serif;
	/* To give the same size to all text fields */
	width: 300px;
	box-sizing: border-box;
	/* To harmonize the look & feel of text field border */
	border: 1px solid #999;
	box-shadow: 0 0 5px #666;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	-o-border-radius: 5px;
	-ms-border-radius: 5px;
	border-radius: 5px;
	height: 26px;
}

input:focus, textarea:focus {
	/* To give a little highlight on active elements */
	border-color: #000;
}

textarea {
	/* To properly align multiline text fields with their labels */
	vertical-align: top;
	/* To give enough room to type some text */
	height: 5em;
}

.button {
	/* To position the buttons to the same position of the text fields */
	padding-left: 90px;
	/* same size as the label elements */
}

button, input[type=submit],#cancel {
	/* This extra margin represent roughly the same space as the space between the labels and their text fields */
	margin-left: 0.5em;
	/* To make sure that all text fields have the same font settings By default, textareas have a monospace font */
	font: 1em sans-serif;
	/* To give the same size to all text fields */
	width: 300px;
	box-sizing: border-box;
	/* To harmonize the look & feel of text field border */
	border: 1px solid #999;
	box-shadow: 0 0 5px #666;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	-o-border-radius: 5px;
	-ms-border-radius: 5px;
	border-radius: 5px;
	margin-top: 7px;
	margin-bottom: 7px;
	height: 43px;
	background-color: #c3c1c1;
}

        #loading {
          position: fixed;
          display: block;
          width: 100%;
          height: 100%;
          top: 0;
          left: 0;
          text-align: center;
          opacity: 0.7;
          background-color: #fff;
          z-index: 99;
        }
        
        #loading-image {
          position: absolute;
          /*top: 30%;*/
          left: 37%;
          z-index: 100;
        }
        
        
</style>
</head>

<body>
    <div id="loading">
        <img id="loading-image" src="/loading.gif" alt="Loading..." />
    </div>
    <form id="reassign_part" name="reassign_part"
        action="reassign_part.php" method="get">
        <input type="hidden" id="start-date-time" name="start-date-time" value="<?=$_GET['start-date-time']?>" />
        <input type="hidden" id="part_id" name="part_id" value="<?=$init_form['part']['id']?>" />
        <h2>Reasign Part: <?= $init_form['part']['id']?></h2>
        <h3><?= $init_form['part']['file_name'] ?></h3>
        <fieldset>
            <legend>Days: <?= $init_form['slot_interval']->format('%d')?> - Time: <?= $init_form['slot_total_hours']?>:<?=$init_form['slot_interval']->format('%i')?></legend>
            <ul>
                <li><label for="printer_id">Printer:</label> 
                    <select id="printer_id" name="printer_id" required>
                        <!--  <option disabled selected>Select Printer </option> -->
                        <?php

                        foreach ($init_form['printers'] as $printer) {
                            //var_dump($printer['code']);
                            if ($printer['id'] == $init_form['part']['printer_id']) {
                                echo ('<option value="' . $printer['id'] . '" selected>' . $printer['code'] . '</option>');
                            } else {
                                echo ('<option value="' . $printer['id'] . '">' . $printer['code'] . '</option>');
                            }
                        }

                        ?>
                    </select></li>
                <li><label for="state_id">Part State:</label> 
                    <select id="state_id" name="state_id" required>
                        <!-- <option disabled selected>Select Part State </option> -->
                        <?php

                        foreach ($init_form['states'] as $state) {
                            if($state['id'] != 7){ //No mostramos el estado apartado, ya que es de uso interno
                                if ($state['id'] == $init_form['part']['state_id']) {
                                    echo ('<option value="' . $state['id'] . '" selected>' . $state['name'] . '</option>');
                                } else {
                                    echo ('<option value="' . $state['id'] . '">' . $state['name'] . '</option>');
                                }
                            }
                        }

                        ?>
                    </select></li>
                <li><label for="start">Start:</label> <input
                    type="datetime-local" id="start" name="start"
                    value="<?=$init_form['start']->format('Y-m-d').'T'.$init_form['start']->format('H:i')?>"
                    min="<?=$init_form['min']->format('Y-m-d').'T'.$init_form['min']->format('H:i')?>"
                    max="<?=$init_form['max']->format('Y-m-d').'T'.$init_form['max']->format('H:i')?>" />
                </li>
                <li><label for="customer_id">End:</label> <input
                    type="datetime-local" id="end" name="end"
                    value="<?=$init_form['end']->format('Y-m-d').'T'.$init_form['end']->format('H:i')?>"
                    min="<?=$init_form['min']->format('Y-m-d').'T'.$init_form['min']->format('H:i')?>"
                    max="<?=$init_form['max']->format('Y-m-d').'T'.$init_form['max']->format('H:i')?>" />
                </li>
                <li><label for="how">How:</label> <select id="how"
                    name="how" required>
                        <option value="from_start">With Start Date</option>
                        <option value="to_end">With End Date</option>
                </select></li>

            </ul>
            <ul>
                <li class="button">
                    <input type="submit" value="Send Order" />
                    <input id="cancel" type="button" value="Cancel" />
                </li>
            </ul>
        </fieldset>
    </form>

</body>
<script>

window.onload = function () {
     $('#loading').hide();
     
     $(".button").click(function(){
         $('#loading').show();
     })
     
     $("#cancel").click(function(){
         document.location.href = 'http://<?= URL?>/index.php?start-date-time=<?=$_GET['start-date-time']?>';
     });
     
     
}

</script>



</html>