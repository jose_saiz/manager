<?php
include 'part.php';
include 'database.php';
include 'datetime.php';
include 'printerslot.php';
include 'printertimeline.php';
include 'printermatrix.php';
include 'configuration.php';

error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set('Europe/Madrid');
setlocale(LC_ALL, "es_ES");
$neighborhood_id = conf_neighborhood_factory_id();


$sql = sprintf('
            SELECT  *
            FROM    product AS p
            WHERE active = 1
            ORDER BY id;
        ');
//ORDER BY p.weight DESC,p.order_id  ASC
//DataBase::getConnection()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE);
$products = DataBase::getConnection()->query($sql);


$sql = sprintf('
            SELECT  *
            FROM  contact AS c
            WHERE contact_type_id = 2
            ORDER BY id;
        ');
//ORDER BY p.weight DESC,p.order_id  ASC
//DataBase::getConnection()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE);

$customers = DataBase::getConnection()->query($sql);


$products_selected = array();

foreach ($products as $product){
    if (isset ($_GET [$product['id']]) && ($_GET [$product['id']]!=0)){
        $products_selected [$product['id']] = $_GET [$product['id']];
    }
}
/*
var_dump($products_selected);
*/

if ((count($products_selected) > 0) && isset($_GET['customer_id'])) {

    //var_dump($_GET['start-date-time']);
    //echo "Product_id:".$_GET['product_id'];
    

    $sql = sprintf('INSERT INTO order_g (state_id, customer_id, priority,neighborhood_id)
                            VALUES (12,' . $_GET['customer_id'] . ',' . $_GET['priority'] .',' . $neighborhood_id . ')');
    //var_dump($sql);
    $rdo = DataBase::getConnection()->query($sql);
    $order = DataBase::getConnection()->query('SELECT MAX(id) AS id FROM order_g')->fetch_assoc();
    
    foreach($products_selected as $product_id => $quantity){
        for ($h=0;$h< $quantity;$h++){
            
            
            $sql = sprintf('INSERT INTO final_product (product_id)
                                VALUES ('.$product_id.')');
            //var_dump($sql);
            $rdo = DataBase::getConnection()->query($sql);
            $final_product = DataBase::getConnection()->query('SELECT MAX(id) AS id FROM final_product')->fetch_assoc();
            
            
            $sql = sprintf('
                    SELECT p.file_id, p.file_part_name, p.version_part_name, p.quantity, f.quantity AS quantity_print_x_order_line,filament_used AS weight
                    FROM    product_quantity_parts AS p
                    INNER JOIN file AS f ON f.id =  p.file_id
                    WHERE product_id = ' . $product_id, '
                    ORDER BY id;
                ');
            //ORDER BY p.weight DESC,p.order_id  ASC
            //DataBase::getConnection()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE);
            $product_quantity_parts = DataBase::getConnection()->query($sql);
    
        //for ($i = 1; $i <= $product_selected; $i ++) {
            
            foreach ($product_quantity_parts as $product_quantity_part) {
                
                
                $ids_parts_array = array();
                //for ($j = 0; $j < $product_quantity_part['quantity_print_x_order_line']; $j ++) {
                for ($j = 0; $j < $product_quantity_part['quantity']; $j ++) {
                    $sql = sprintf('INSERT INTO part (order_id, file_id, file_part_name, version_part_name,state_id,final_product_id,weight,printer_id) 
                                    VALUES (' . 
                                            $order['id'] . ',' . 
                                            $product_quantity_part['file_id'] . ',"' .
                                            $product_quantity_part['file_part_name'] . '","' . 
                                            $product_quantity_part['version_part_name'] . '",' . '8 ,' . 
                                            $final_product['id'].' , '.
                                            $product_quantity_part['weight'] . ',0);');
                    DataBase::getConnection()->query($sql);
                    $id_part = DataBase::getConnection()->query('SELECT MAX(id) AS id FROM part')->fetch_assoc();
                    $ids_parts_array[$j] = $id_part;
                }
                $ids_parts = json_encode($ids_parts_array);
                $sql = sprintf('INSERT INTO order_line (order_id, file_id, file_part_name, version_part_name, ids_parts, final_product_id, quantity,  quantity_print_x_order_line) 
                                VALUES (' . 
                                    $order['id'] . ',' . 
                                    $product_quantity_part['file_id'] . ',"' . 
                                    $product_quantity_part['file_part_name'] . '","' . 
                                    $product_quantity_part['version_part_name'] . '",' . '\'' . 
                                    $ids_parts . '\',' .
                                    $final_product['id'] .' , '.
                                    $product_quantity_part['quantity'] . ',' . 
                                    $product_quantity_part['quantity_print_x_order_line'] . ');');
                $result = DataBase::getConnection()->query($sql);
            }
        }
    }
    header("Location: ".URL."/index.php?start-date-time=" . $_GET['start-date-time']);
}

?>
<!DOCTYPE html>
<html>
<head>
<script type="text/javascript" src="js/jquery-3.6.0.js"></script>

<meta charset="utf-8" />
<title>Create Order</title>
<style>
form {
	/* Just to center the form on the page */
	margin: 0 auto;
	width: 500px;
	/* To see the outline of the form */
	padding: 1em;
	border: 1px solid #ccc;
	border-radius: 1em;
	box-shadow: 0 0 20px #666;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
}

legend {
	/* Just to center the form on the page */
	margin: 0 auto;
	width: 300px;
	/* To see the outline of the form */
	padding: 1em;
	border: 1px solid #ccc;
	border-radius: 1em;
	background: white;
	box-shadow: 0 0 5px #666;
}

fieldset {
	margin: 20px;
	padding: 0 10px 10px;
	border: 1px solid #ccc;
	border-radius: 8px;
	box-shadow: 0 0 10px #666;
	padding-top: 10px;
}

ul {
	list-style: none;
	padding: 0;
	margin: 0;
}

form li+li {
	margin-top: 1em;
    margin-bottom: 15px;
}

label {
	/* To make sure that all labels have the same size and are properly aligned */
	display: inline-block;
	width: 166px;
	text-align: left;
	margin-top: 7px;
	margin-bottom: 7px;
    padding-right: 11px;
    padding-left: 30px;
}

input{
	/* To make sure that all text fields have the same font settings By default, textareas have a monospace font */
	font: 1em sans-serif;
	/* To give the same size to all text fields */
	width: 100px;
	box-sizing: border-box;
	/* To harmonize the look & feel of text field border */
	border: 1px solid #999;
	box-shadow: 0 0 5px #666;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	-o-border-radius: 5px;
	-ms-border-radius: 5px;
	border-radius: 5px;
	height: 26px;
}


select, textarea {
    /* To make sure that all text fields have the same font settings By default, textareas have a monospace font */
    font: 1em sans-serif;
    /* To give the same size to all text fields */
    width: 184px;
    box-sizing: border-box;
    /* To harmonize the look & feel of text field border */
    border: 1px solid #999;
    box-shadow: 0 0 5px #666;
    font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    -webkit-border-radius: 5px;
    -moz-border-radius: 5px;
    -o-border-radius: 5px;
    -ms-border-radius: 5px;
    border-radius: 5px;
    height: 26px;
}


input:focus, textarea:focus {
	/* To give a little highlight on active elements */
	border-color: #000;
}

textarea {
	/* To properly align multiline text fields with their labels */
	vertical-align: top;
	/* To give enough room to type some text */
	height: 5em;
}

.button {
	/* To position the buttons to the same position of the text fields */
	padding-left: 90px;
	/* same size as the label elements */
}

button, input[type=submit],#cancel {
	/* This extra margin represent roughly the same space as the space between the labels and their text fields */
	margin-left: 0.5em;
	/* To make sure that all text fields have the same font settings By default, textareas have a monospace font */
	font: 1em sans-serif;
	/* To give the same size to all text fields */
	width: 300px;
	box-sizing: border-box;
	/* To harmonize the look & feel of text field border */
	border: 1px solid #999;
	box-shadow: 0 0 5px #666;
	font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
	-webkit-border-radius: 5px;
	-moz-border-radius: 5px;
	-o-border-radius: 5px;
	-ms-border-radius: 5px;
	border-radius: 5px;
	margin-top: 7px;
	margin-bottom: 7px;
	height: 43px;
	background-color: #c3c1c1;
}
        #loading {
          position: fixed;
          display: block;
          width: 100%;
          height: 100%;
          top: 0;
          left: 0;
          text-align: center;
          opacity: 0.7;
          background-color: #fff;
          z-index: 99;
        }
        
        #loading-image {
          position: absolute;
          top: -86px;
          left: 36%;
          z-index: 100;
        }
</style>
</head>

<body>
    <div id="loading">
        <img id="loading-image" src="/loading.gif" alt="Loading..." />
    </div>
    <form id="create_order" name="create_order"
        action="create_order.php" method="get">
        <input type="hidden" id="start-date-time" name="start-date-time"
            value="<?=$_GET['start-date-time']?>" />
        <h2>Create Order & Parts</h2>
        <fieldset>
            <!--  <legend>Select Type</legend>-->
            <ul>
                <h3>Products:</h3>
                <?php foreach ($products as $product) : ?> 
                <li>
                    <label for=""<?=$product['id']?>"><?= $product['name']?>: </label> 
                    <input type="number" id="<?=$product['id']?>" name="<?=$product['id']?>" value="0" min="0" max="30" required placeholder="Input quantity" /></li>
                </li>
                <?php endforeach;?>
                
                <li><label for="priority">Priority:</label> <input
                    type="number" id="priority" name="priority"
                    value="1" min="1" max="10" required
                    placeholder="Input priority" /></li>
                <li>
                    <label for="customer_id">Customer:</label> 
                    <select id="customer_id" name="customer_id" required>
                        <!-- <option disabled selected>Select Customer</option> -->
                        <?php
                        foreach ($customers as $customer) {
                            echo ('<option value="' . $customer['id'] . '">' . $customer['name'] . '</option>');
                        }
                        ?>
                    </select>
                </li>

            </ul>
            <ul>
                <li class="button">
                    <input id="submit_button" type="submit" value="Send Order" />
                    <input id="cancel" type="button" value="Cancel" />
                </li>
            </ul>
        </fieldset>
    </form>
    
<script>

window.onload = function () {
    $('#loading').hide();
    
    $("#submit_button").click(function(){
        $('#loading').show();
    })
    /*
    $("#cancel").click(function(){
        $('#loading').show();
    })
    */

    
    $("#cancel").click(function(){
        document.location.href = '<?=URL?>/index.php?start-date-time=<?=$_GET['start-date-time']?>';
    });
    
}

</script>
</body>
</html>