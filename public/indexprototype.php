<?php
include 'part.php';
include 'database.php';
//include 'datetime.php';
include 'DateTimeG.php';
include 'printerslot.php';
include 'printertimeline.php';
include 'printermatrix.php';
include 'configuration.php';
include 'permutations.php';
 
error_reporting(E_ALL);
ini_set('display_errors', 1);
date_default_timezone_set('Europe/Madrid');
setlocale(LC_ALL, "es_ES");


header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

set_time_limit(120);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////// C R E A T E   A N D   A S S I G N   P R I N T E R S   M A T R I X  ////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$start_date_time = new DateTimeG();
if (isset($_GET['start-date-time'])) {
    //var_dump($_GET['start-date-time']);
    $start_date_time = DateTimeG::newFromformatUTC($_GET['start-date-time']);
}

$min = new DateTimeG($start_date_time->format('Y-m-d H:i:s'));
//$min = new DateTime($now_datetime);
$min->modify('-1 years');

$max = new DateTimeG($start_date_time->format('Y-m-d H:i:s'));
//$max = new DateTime($start_date_time->date);
$max->modify('+10 years');

$neighborhood_factory_id = conf_neighborhood_factory_id();

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////// H T M L  -  B E G I N  ///////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


$printersMatrix = new PrintersMatrix($neighborhood_factory_id, $start_date_time);
$printersMatrix->chargePrintersMatrix();

if (isset($_GET['part_id']) && isset($_GET['printer_id']) && isset($_GET['state_id']) && 
    isset($_GET['start'])   && isset($_GET['end']) && 
    isset($_GET['button'])  && isset($_GET['how'])) {
      
        
    
    if ($_GET['button'] == 'cancel'){
        ////////////////// reasing part send request Cancel ////////////////////////
        //Pulsamos el botón Cancelar: La pieza ha fallado, no se ha impreso correctamente.
        
        $sql = sprintf("UPDATE part
                        SET initiated = 0
                        WHERE id = " . $_GET['part_id']);
        $result = DataBase::getConnection()->query($sql);
        
        
        $printersMatrix->reassignPartsAll($start_date_time,$_GET['button'], conf_neighborhood_factory_id(),$_GET['part_id']);
    }elseif ($_GET['button'] == 'play'){
        /////////////////// reasing part send request Play /////////////////////////
        //Pulsamos el botón Play: Se ha iniciado la impresión de la pieza
        $sql = DataBase::querySelectReassignPart($_GET['part_id']);
        $pending_part = DataBase::getConnection()->query($sql)->fetch_assoc();
        
        $end_new = $start_date_time->estimatedPrintingEnd($pending_part);
        
        $sql = sprintf("UPDATE part
                        SET printer_id = " . $_GET['printer_id'] . ", state_id = " . $_GET['state_id'] . " ,
                            start_datetime = '" . $start_date_time->format('Y-m-d H:i:s') . "', end_datetime = '" . $end_new->format('Y-m-d H:i:s') . "', initiated = 1
                        WHERE id = " . $_GET['part_id']);
        $result = DataBase::getConnection()->query($sql);
        
        //Primero intentamos desplazar todas la piezas de la impresora hacia la derecha. Teniendo en cuenta las noches.
        //Si el rendimiento no  es aceptable in
        $printersMatrix->reassignPartsAll($start_date_time,$_GET['button'], conf_neighborhood_factory_id(),$_GET['part_id']);
        
        
    }
    header("Location: ".URL."/index.php?reassign=true&start-date-time=" . $_GET['start-date-time']);
}else{
    $printersMatrix->assignPrintersSlots();
}

$printersMatrix->filamentCalculations();
$rdo = $printersMatrix->summary();


?>
<html>
    <head>
        <!-- <link rel="stylesheet" href="//code.jquery.com/ui/1.13.1/themes/base/jquery-ui.css">  -->
        <link rel="stylesheet" href="jquery-ui.css">
        <!--  <link rel="stylesheet" href="jquery.tipsy.css">-->
        <link rel="stylesheet" href="jquery.tipsy1.0.3.css">
  
        <script type="text/javascript" src="js/jquery-3.6.0.js"></script>
        <!--<script type="text/javascript" src="js/jquery.tipsy.js"></script> -->
         <!-- <script type="text/javascript" src="js/jquery.tipsy1.0.3.js"></script> -->
        
        <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.js"></script>
        <link  rel="icon" href="favicon.png" type="image/png" />
        <script>
          $( function() {
            var dialog, form,
         
              // From http://www.whatwg.org/specs/web-apps/current-work/multipage/states-of-the-type-attribute.html#e-mail-state-%28type=email%29
              emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
              name = $( "#name" ),
              email = $( "#email" ),
              password = $( "#password" ),
              allFields = $( [] ).add( name ).add( email ).add( password ),
              tips = $( ".validateTips" );
         
            function updateTips( t ) {
              tips
                .text( t )
                .addClass( "ui-state-highlight" );
              setTimeout(function() {
                tips.removeClass( "ui-state-highlight", 1500 );
              }, 500 );
            }
         
            function checkLength( o, n, min, max ) {
              if ( o.val().length > max || o.val().length < min ) {
                o.addClass( "ui-state-error" );
                updateTips( "Length of " + n + " must be between " +
                  min + " and " + max + "." );
                return false;
              } else {
                return true;
              }
            }
         
            function checkRegexp( o, regexp, n ) {
              if ( !( regexp.test( o.val() ) ) ) {
                o.addClass( "ui-state-error" );
                updateTips( n );
                return false;
              } else {
                return true;
              }
            }
         
            function addUser() {
              var valid = true;
              allFields.removeClass( "ui-state-error" );
         
              valid = valid && checkLength( name, "username", 3, 16 );
              valid = valid && checkLength( email, "email", 6, 80 );
              valid = valid && checkLength( password, "password", 5, 16 );
         
              valid = valid && checkRegexp( name, /^[a-z]([0-9a-z_\s])+$/i, "Username may consist of a-z, 0-9, underscores, spaces and must begin with a letter." );
              valid = valid && checkRegexp( email, emailRegex, "eg. ui@jquery.com" );
              valid = valid && checkRegexp( password, /^([0-9a-zA-Z])+$/, "Password field only allow : a-z 0-9" );
         
              if ( valid ) {
                $( "#users tbody" ).append( "<tr>" +
                  "<td>" + name.val() + "</td>" +
                  "<td>" + email.val() + "</td>" +
                  "<td>" + password.val() + "</td>" +
                "</tr>" );
                dialog.dialog( "close" );
              }
              return valid;
            }
         
            dialog = $( "#dialog-form" ).dialog({
              autoOpen: false,
              height: 400,
              width: 350,
              modal: true,
              buttons: {
                "Charge fillament": addUser,
                Cancel: function() {
                  dialog.dialog( "close" );
                }
              },
              close: function() {
                form[ 0 ].reset();
                allFields.removeClass( "ui-state-error" );
              }
            });
         
            form = dialog.find( "form" ).on( "submit", function( event ) {
              event.preventDefault();
              addUser();
            });
            
            $( "#create-user" ).button().on( "click", function() {
              dialog.dialog( "open" );
            });
            $( ".fillament" ).click(function() {
                dialog.dialog( "open" );
            });
            




            
          } );
        </script>
    
        <style>
        

        
            body {
                background: #FFFFFF;
                margin:0px;
                font-size: small;
                font-family: Arial, Helvetica, sans-serif;
            }
            div.inline { float:left;padding-left: 12px; }
            .clearBoth { clear:both;padding-left: 12px; }
            .extra {margin-left: 995px;margin-top: -53px;}
    
            .my-fixed-item {
                position: fixed;
                /*min-height: 280px;*/
                width: 252px;
                text-align: center;
                word-wrap: break-word;
                background-color: #d1f780;
                z-index:1000;
    
            }
    
            .my-fixed-item2 {
    
                position: fixed;
                min-height: 280px;
                width: 252px;
                text-align: center;
                word-wrap: break-word;
                background-color: #d1f780;
                z-index:100;
            }
            #footer {
                position: fixed;
                bottom: 0;
                width: 3000px;
            }
            
            
            
            
            
            #footer {
                background: #0070FF;
                line-height: 2;
                text-align: center;
                color: #042E64;
                font-size: 30px;
                font-family: sans-serif;
                font-weight: bold;
                text-shadow: 0 1px 0 #84BAFF;
                box-shadow: 0 0 15px #00214B;
                overflow-x: auto;
            }
            
            
            #loading {
              position: fixed;
              display: block;
              width: 100%;
              height: 100%;
              top: 0;
              left: 0;
              text-align: center;
              opacity: 0.7;
              background-color: #fff;
              z-index: 99;
            }
            
            #loading-image {
              position: absolute;
              top: 30%;
              left: 33%;
              z-index: 100;
            }
            
            
            table {
                font-size: 1em;
            }
            
            .ui-draggable, .ui-droppable {
                background-position: top;
            }
            
            .dlgfixed{
                position:fixed;
                
            }
            
            .fillament:hover{
                cursor: pointer;
            }
            .fillament{
                text-decoration: underline;
            }
            .ui-tooltip {
              white-space: pre-line;
            }
            
            
        </style>
    </head>
    
    <body>
        <div id="loading">
            <img id="loading-image" src="/loading.gif" alt="Loading..." />
        </div>
        <div 
            class="my-fixed-item" 
            style="width: 100%;height: 196px;float: left;margin-top: 0px;">
            <div style="width: 10%;float: left;text-align: left;padding: 18px;height: 82px;"><img src="/greeny_logo.svg"></div>
            <div style="width: 96%;float: left;text-align: left;padding: 31px;">
                <div class="inline" style="margin-top: -100px;padding-left: 248px;">
                <form action = "index.php" method="get">
                    <div>
                        <div class="inline">
                            Monday:
                            <input type="time" id="workday_to_monday" name="workday_to_monday"
                                    value="<?= conf_working_days()[1]['from']['hour']?>:<?= conf_working_days()[1]['from']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                            <small></small>
                            <input type="time" id="workday_to_monday" name="workday_to_monday"
                                    value="<?= conf_working_days()[1]['to']['hour']?>:<?= conf_working_days()[1]['to']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                        </div>
                        <div class="inline">
                            Tuesday
                            <input type="time" id="workday_from" name="workday_from"
                                    value="<?= conf_working_days()[2]['from']['hour']?>:<?=conf_working_days()[2]['from']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                            <small></small>
                            <input type="time" id="workday_to_monday" name="workday_to_monday"
                                    value="<?= conf_working_days()[2]['to']['hour']?>:<?= conf_working_days()[2]['to']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                        </div>
                        <div class="inline">
                            Wednesday
                            <input type="time" id="workday_from" name="workday_from"
                                    value="<?= conf_working_days()[3]['from']['hour']?>:<?=conf_working_days()[3]['from']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                            <small></small>
                            <input type="time" id="workday_to_monday" name="workday_to_monday"
                                    value="<?= conf_working_days()[3]['to']['hour']?>:<?= conf_working_days()[3]['to']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                        </div>
                        <div class="inline">
                            Thursday
                            <input type="time" id="workday_from" name="workday_from"
                                    value="<?= conf_working_days()[4]['from']['hour']?>:<?=conf_working_days()[4]['from']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                            <small></small>
                            <input type="time" id="workday_to_monday" name="workday_to_monday"
                                    value="<?= conf_working_days()[4]['to']['hour']?>:<?= conf_working_days()[4]['to']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                        </div>
                        <div class="inline">
                            Friday
                            <input type="time" id="workday_from" name="workday_from"
                                    value="<?= conf_working_days()[5]['from']['hour']?>:<?=conf_working_days()[5]['from']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                            <small></small>
                            <input type="time" id="workday_to_monday" name="workday_to_monday"
                                    value="<?= conf_working_days()[5]['to']['hour']?>:<?= conf_working_days()[5]['to']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                        </div>
                        <div class="inline">
                            Saturday
                            <input type="time" id="workday_from" name="workday_from"
                                    value="<?= conf_working_days()[6]['from']['hour']?>:<?=conf_working_days()[6]['from']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                            <small></small>
                            <input type="time" id="workday_to_monday" name="workday_to_monday"
                                    value="<?= conf_working_days()[6]['to']['hour']?>:<?= conf_working_days()[6]['to']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                        </div>
                                                <div class="inline">
                            Sunday
                            <input type="time" id="workday_from" name="workday_from"
                                    value="<?= conf_working_days()[0]['from']['hour']?>:<?=conf_working_days()[0]['from']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                            <small></small>
                            <input type="time" id="workday_to_monday" name="workday_to_monday"
                                    value="<?= conf_working_days()[0]['to']['hour']?>:<?= conf_working_days()[0]['to']['minute']?>"
                                    min="00:00" 
                                    max="23:59" 
                                    required/>
                        </div>
                    </div>
                  
                  
                  
                  
                    
                    <br><br><br>
                    <div class="inline">
                        <!-- 
                        <label for="workday_from">Workday from:</label>
                        <input type="time" id="workday_from" name="workday_from"
                                value="<?= conf_workday()['from_hour']?>:<?=conf_workday()['from_minute']?>"
                                min="00:00" 
                                max="23:59" 
                                required/>
                        <small></small>
                        <label for="appt">Workday   to:</label>
                        <input type="time" id="workday_to" name="workday_to"
                                value="<?= conf_workday()['to_hour']?>:<?= conf_workday()['to_minute']?>"
                                min="00:00" 
                                max="23:59" 
                                required/>
                        <small></small>
                         -->
                         <input type="hidden" id="workday_from" name="workday_from"
                                value="<?= conf_workday()['from_hour']?>:<?=conf_workday()['from_minute']?>"
                                />
                         <input type="hidden" id="workday_to" name="workday_to"
                                value="<?= conf_workday()['to_hour']?>:<?=conf_workday()['to_minute']?>"
                                />
                                
                        <label for="start">Date Time Start:</label>
                        <input type="datetime-local" id="start-date-time" name="start-date-time"
                            value=<?= $start_date_time->format('Y-m-d') ?>T<?= $start_date_time->format('H:i') ?>
                            min="<?= $min->format('Y-m-d') ?>T<?= $min->format('H:i') ?>" 
                            max="<?= $max->format('Y-m-d') ?>T<?= $max->format('H:i') ?>"
                        />
                        <input class="submit" type="submit" value="Set Current Time"/>
                    </div>
                    <br><br>
                    
                </form>
                </div>
                
                
                
                
                

                <div class="inline"  style="margin-left: 695px;margin-top: -53px;">
                    <form action = "save_assign_printers_matrix.php" method="get">
                            <input id="neighborhood_factory_id" name="neighborhood_factory_id" type="hidden" value="<?= $neighborhood_factory_id?>">
                            <input id="start_date_time" name="start_date_time" type="hidden" value="<?= $start_date_time->format('Y-m-d H:i:s') ?>">
                            <input class="submit" type="submit" value="Save Assignment"/>
                        
                    </form>
                </div>
                <div class="inline"  style="margin-left: 822px;margin-top: -53px;">
                    <form action = "clear_assign_printers_matrix.php" method="get">
                            <input id="neighborhood_factory_id" name="neighborhood_factory_id" type="hidden" value="<?= $neighborhood_factory_id?>">
                            <input id="start_date_time" name="start_date_time" type="hidden" value="<?= $start_date_time->format('Y-m-d H:i:s') ?>">
                            <input class="submit" type="submit" value="Clear Assignment"/>
                        
                    </form>
                </div>
                <div class="inline"  style="margin-left: 1054px;margin-top: -53px;">
                    <form action = "delete_unassigned_printers_matrix.php" method="get">
                            <input id="neighborhood_factory_id" name="neighborhood_factory_id" type="hidden" value="<?= $neighborhood_factory_id?>">
                            <input id="start_date_time" name="start_date_time" type="hidden" value="<?= $start_date_time->format('Y-m-d H:i:s') ?>">
                            <input style="background-color: red;color:white" class="submit" type="submit" value="Delete Unsassigned"/>
                        
                    </form>
                </div>
                <div class="inline"  style="margin-left: 1200px;margin-top: -53px;">
                    <form action = "delete_orders_printers_matrix.php" method="get">
                            <input id="neighborhood_factory_id" name="neighborhood_factory_id" type="hidden" value="<?= $neighborhood_factory_id?>">
                            <input id="start_date_time" name="start_date_time" type="hidden" value="<?= $start_date_time->format('Y-m-d H:i:s') ?>">
                            <input style="background-color: red;color:white" class="submit" type="submit" value="Delete Orders"/>
                        
                    </form>
                </div>
                <div class="inline"  style="margin-left: 950px;margin-top: -53px;">
                    <form method="get" action="create_order.php" method="get">
                        <input id="start-date-time" name="start-date-time" type="hidden" value="<?= $start_date_time->format('Y-m-d H:i:s')?>">
                        <input  class="submit" type="submit" value="Create Order"/>
                    </form>
                </div>
                
                

                <!-- <button id="create-user">Create new user</button> -->
            </div>
            <div class="inline"  style="margin-top: -47px;margin-left: 267px;width: 84%;">
                <b><?= $rdo['printers_count']?> Printers: </b> 
                <?php 
                    foreach ($printersMatrix as $PrinterLine){
                        usort($PrinterLine->rolls_end, 'sort_by_datetime');
                        
                        $printerTimeLineAux = new PrinterTimeline($PrinterLine->printer_id,
                            $PrinterLine->code_printer,
                            $PrinterLine->roll_weight,
                            $PrinterLine->roll_replacement_datetime);
                        
                        $printerTimeLineAux->chargePrinterTimeline($PrinterLine->roll_replacement_datetime);
                        
                        $printerTimeLineAux->filamentCalculations ($PrinterLine->roll_replacement_datetime);
                        
                        if (count($printerTimeLineAux) == 0){
                            $printerTimeLineAux = $PrinterLine;
                        }
                        
                        $color="";
                        $blink_class =  "";
                        foreach ($printerTimeLineAux->rolls_end as $roll_end){
                            $diff = $roll_end['datetime']->diffRangeHM($printersMatrix->start_date_time); 
                            $color="";
                            $blink_class =  "";
                            if ($diff['hours']<10){
                                $color =  "red";
                            }
                            if (($diff['hours']<1)&& ($diff['minutes']<30)){
                                $blink_class =  "blink";
                            }
                            break;
                            
                        }
                        
                        
                        $href = '#printer'.strval($PrinterLine->printer_id-1);
                        if ($PrinterLine->printer_id == 1){
                            $href ="#arriba";
                        }
                        if (!isset($color)){
                            $color = "black"; 
                        }
                        echo '&nbsp;<a class="'.$blink_class.'" style="color:'.$color.';display:inline-block" href="'.$href.'"/>'.$PrinterLine->code_printer.'</a>&nbsp;';
                        
                    }
                    
                ?>
                
                <?php 
                 $next_part = $printersMatrix->nextPart();
                 if (count($next_part)>0){
                     echo "<b>- Next Part: </b>";
                     echo '<a  style="color:'.$color.';display:inline-block" href="#printer'.strval($next_part['printer']->printer_id-1).'"/>'.$next_part['slot']->part.'</a>';
                     echo '<b> - Printer: </b>'.$next_part['printer']->code_printer.' <b> - Estimated start time: </b>'.$next_part['slot']->start.'';
                 }
                ?>

            </div>
            <div class="inline"  style="    margin-top: -11px;width: 100%;text-align: center;">
              - <b> Greenies: <?=   $rdo['greenies']['total']?> </b>
                        (Assigned: <?= count($rdo['greenies']['assigned'])?>, 
                        Unassigned: <?= count($rdo['greenies']['unassigned'])?>,
                        Partially assigned: <?= count($rdo['greenies']['partially_assigned'])?>, 
                        Other <?= count($rdo['greenies']['other'])?>) 
              - <b> Parts: <?=   count($rdo['parts']['assigned']) + count($rdo['parts']['unassigned'])?></b>
                        (Assigned: <?= count($rdo['parts']['assigned'])?>, 
                        Unassigned: <?=count($rdo['parts']['unassigned'])?>,
                        Missing: <?=count($rdo['parts']['missing']) ?>,
                        Pending: <?=$printersMatrix->pending_parts_count - $rdo['parts']['total']?> ,  
                        Other <?= count($rdo['parts']['other'])?>) 
              - <b> Efficiency: <?=   $printersMatrix->percent()?>% </b>
              
            </div>
            <div class="inline" style="float: right;margin-top: -88px;margin-right: 75px;">
                <label for="zoom-range">Zoom</label>
                <input id="zoom-range" type="range" step="5" min="5" max="100" onChange="changeZoom(this.value)" />
            </div>
            <div class="inline" style="float: right;margin-top: -130px;">
                <div>
                    <a href="#arriba"><img src="arriba.png" alt="Top" width="25" height="25" style="padding-bottom: 10px;padding-right:30px"/></a>
                </div>
                <div>        
                    <a href="#abajo" ><img src="abajo.png" alt="Bottom" width="25" height="25" style="padding-bottom: 30px;padding-right:30px"/></a>
                </div>
            </div>
            
    
        </div>
        
<div  style="top:-200px">        
<div id="dialog-form" title="Fillament Charge" style="">
  <p class="validateTips">Filament to  Finish  Current Job: 750gr.</p>
 
  <form>
    <fieldset>
      <label for="Code">Code Printer</label>
      <input type="text" name="code" id="code" value="T-01" class="text ui-widget-content ui-corner-all" disabled/>
      <br/><br/>
      <label for="roll_weight">Roll Weight(g.)</label>
      <input type="text" name="roll_weight" id="roll_weight" value="2159" class="text ui-widget-content ui-corner-all">
      <br/><br/>
      <label for="roll_replacement_datetime">Date Time Roll Replacement</label>
      <input type="datetime-local" id="roll_replacement_datetime" name="roll_replacement_datetime"
            value=<?= $start_date_time->format('Y-m-d') ?>T<?= $start_date_time->format('H:i') ?>
            min="<?= $min->format('Y-m-d') ?>T<?= $min->format('H:i') ?>" 
            max="<?= $max->format('Y-m-d') ?>T<?= $max->format('H:i') ?>"
      />                                    
      <!-- Allow form submission with keyboard without duplicating the dialog button -->
      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
    </fieldset>
  </form>
</div>
</div> 
    <!-- 
    <div class="my-fixed-item2" style="width: 100%;height: 208px;float: left;">
        <?php /*echo $printersMatrix ->printRuleToString($zoom, 3000, $printer )*/?>
    </div>
    -->
    <a name="arriba"></a>
    <br/><br/>
    <div style = "margin-left:48px;">
        <br></br><br></br><br></br><br></br>
        <?php /*$printersMatrix->printAll(); */?>
         <br></br><!--  <b>Printer TimeLine: </b>--><br></br>
        <?php  
        //$printersMatrix->printAllMissing();
        //$printersMatrix->printOverlaps();
        //$printersMatrix->printNightOverlaps();
        //$printersMatrix->printBlocks();
        $printersMatrix->printHtmlAll();
        ?>
        <?php
        $post_assigned = array();
        foreach($printersMatrix as $printerLine){
            $printerLine->uasort('startAscendingComparison');
            foreach($printerLine as $slot){
                if (($slot->part == 4496)||($slot->part == 4501)){
                    //var_dump($slot);
                }
                if ($slot->state == 9){
                    array_push($post_assigned, $slot->part);
                }
            }
        }
        ?>
        <!-- 
        <div id="footer">
            Footer - Just scroll... 
        </div>;
         -->
    </div>
    
    <a name="abajo"></a>
<!-- 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////// H T M L  -  E N D  ///////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 -->
<script>
function changeZoom(value){
    $(".zoom").css("zoom",value+'%' );
}


window.onload = function () {


    //$('.demo1').tipsy({position: 'top-right',gravity: 'w', html: true, fade: true, });
    
    $.widget("ui.tooltip", $.ui.tooltip, {
         options: {
             content: function () {
                 return $(this).prop('title');
             }
         }
     });

    /*
    $(document).tooltip({
        position: {
            my: "center bottom-20",
            at: "center top",
            using: function (position, feedback) {
                $(this).css(position);
                $("<div>")
                    .addClass("arrow")
                    .addClass(feedback.vertical)
                    .addClass(feedback.horizontal)
                    .appendTo(this);
            }
        }
    });
     $("zoom-range").on("change", function() {
        $(".zoom").css({"zoom": $(this).val() });
    });
     */


     function blink_text() {
         $('.blink').fadeOut(8);
         $('.blink').fadeIn(8).delay( 8);
         //$('.blink')..toggleClass("blink");
     }
     setInterval(blink_text, 16);

     function blink_text() {
         $('.blink_next_part').fadeOut(128);
         $('.blink_next_part').fadeIn(128).delay(128);
         //$('.blink')..toggleClass("blink");
     }
     setInterval(blink_text, 16);


     $('#loading').hide();

     $(".submit").click(function(){
         $('#loading').show();
     })
     
      $(".button-time-line").click(function(){
         $('#loading').show();
     })

     //$('#dialog-form').dialog({position: 'top'});
     
     $('#dialog-form').dialog({
         autoOpen: false,
         //maxHeight: 50,
         height: 300,
         dialogClass: 'dlgfixed',
         position: { my: "center ", at: "top+450", of: window },
         
     });

     //$(".dlgfixed").center(false);


     
}


</script>


</body>
</html>