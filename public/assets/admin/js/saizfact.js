/*
|--------------------------------------------------------------------------
| JQuery 
|--------------------------------------------------------------------------
| SaizFact, última actualización de esta documentación realizada el 27/04/2022
|
| //variables universales: 
|
|   **languages: Recibida a través de la vista /admin/layouts/master, se recogen los idiomas de la página.
|
| //funciones:
|
|   **redirección a url superior: Devuelve la url inmediatamente superior de la url pasada por parametro.
|   
|   **cerrar ventana modal: Cierra las ventanas modales de bootstrap una vez renderizada una sección
|     con Laravel.
|  
|   **notificaciones: Mediante jquery.toast.js, aparece una notificación emergente con el mensaje que le 
|     pasemos por parametro. 
|
|   **Bootstrap Switch: Mediante bootstrap.switch.js, renderiza los checkboxes y radio buttons para 
|     darles un estilo animado. 
|
|   **Select2: Mediante custom.select.js, renderiza los select para darles funcionalidades extra. 
||
|   **CKEditor: Convierte automáticamente todas los textarea que hay en la vista en editores completos.  
|
|   **Renderizado de elementos: Renderiza todos los elementos que dependen de algún archivo externo js
|     para que al renderizar una sección con Laravel vuelvan a estar disponibles sus funcionalidades. Es
|     importante introducir aquí la llamada a nuevos elementos si se incluyeran en el futuro.  
|
| //navigation_buttons (dentro de document_ready para que estén disponibles después de llamadas AJAX):
|
|   **Boton 'Nuevo': Evento para el componente ubicado en /views/admin/partials/create_button llama a la 
|     ruta 'create' definida en /routes/web y recibe secciones renderizas que introduce dentro la vista.
|     En el caso de estar en una plantilla de dos columnas además modifica la url para que NO aparezca la 
|     palabra 'create' en ella (importante para poder usar la url en el resto de llamadas ajax).
|
|
| //crud_buttons (dentro de document_ready para que estén disponibles después de llamadas AJAX):
|
|   **Boton 'Guardar': Evento disparado mediante el componente ubicado en /views/admin/partials/crud_buttons, 
|     busca el formulario que hay en la vista y recoge su id para poder validarlo mediante jquery.validate.js 
|     (reglas definidas en saizfact.validations.js) y recoge además su etiqueta 'action' que llama a la ruta 
|     'store' definida en /routes/web. A continuación formData crea un objeto con todos los datos recogidos en
|      el formulario. Se le añade mediante append el nombre del formulario (que será usado para realizar
|      el guardado de las traducciones en LocaleController), opcionalmente si existe algún textarea en la vista
|      también se añade su contenido mediante append a formData (CKEditor convierte automáticamente todos los 
|      textarea). A continuación la forma de comprobar los valores introducidos en formData a través de la 
|      consola:
|
|      for(var pair of formData.entries()) {
|         console.log(pair[0]+ ', '+ pair[1]);
|      }
|
|   **Boton 'Reset': Evento disparado mediante el componente ubicado en /views/admin/partials/crud_buttons, 
|     llama a la ruta 'show' definida en /routes/web y recibe secciones renderizas que introduce dentro la vista.
|
|   **Boton 'Eliminar': Evento disparado mediante el componente ubicado en /views/admin/partials/delete_modal, 
|     llama a la ruta 'destroy' definida en /routes/web y al terminar de eliminar el elemento cierra la ventana modal
|     y modifica la url para volver al directorio superior.
|
| //select_multiple: 
|
|    Componente formado por un Select2 y una lista invisible de checkboxes, cuando se selecciona un elemento en 
|    la lista de select se marca como checked en la lista de checkboxes. Los estilos están definidos en 
|    resources/assets/less/saizfact.less 
|
|   
*/

"use strict";

//Funciones

/* Comprobar boton Enter */

function buttonEnter(e) {
    var keynum;
    if (window.event) { // IE
        keynum = e.keyCode;
    } else { // Firefox
        keynum = e.which;
    }
    return (keynum == 13);
}

/* redirección a url superior */

function getParent(url){
    var parentUrl = url.split('/');
    parentUrl.pop();
    return(parentUrl.join('/') );
}

/* cerrar ventana modal */

function closeModal(){
    debugger;
    $('.modal').modal('hide');
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
}

/* notificaciones */

function notification(message){
    $.toast({
        heading: message,
        text: '',
        position: 'top-right',
        loaderBg: '#00a8af',
        icon: 'info',
        hideAfter: 3500,
        stack: 6
    });
}

/* Bootstrap Switch */

function renderSwitch(){
    $("input.bt-switch[type='checkbox'], input.bt-switch[type='radio']").bootstrapSwitch();
}

/* Select2 */

function renderSelect(){
    $('.select2').each(function (i, obj) {
        if (!$(obj).data('select2')) {
            $(obj).select2({
                language: "es",
                // allowClear: El select2 debería pillar el atributo "data-allow-clear", pero no lo hace, así que lo pasamos por aquí.
                allowClear: ($(obj).data('allow-clear') === undefined) ? false : $(obj).data('allow-clear'),
                templateResult: function (data) {
                    // We only really care if there is an element to pull classes from
                    if (!data.element) {
                      return data.text;
                    }

                    var $element = $(data.element);

                    var $wrapper = $('<span></span>');
                    $wrapper.addClass($element[0].className);

                    $wrapper.text(data.text);

                    return $wrapper;
                  }
            });
        }
    });
}

/*CKEditor*/

CKEDITOR.stylesSet.add('my_custom_style', [
    { name: 'Botón', element: 'div', attributes: {'class': 'btn-primary-01'} }
]);

function renderCKEditor(){
    
    CKEDITOR.replaceAll(function(textarea,config) {
        var $textarea = $(textarea);
        if (!$textarea.hasClass('rich')) {
            return false;
        }
        
        if ($textarea.hasClass('advanced')) {
            config.removeButtons = 'About';
            config.uploadUrl = '';
        } else if ($textarea.hasClass('basic')) {
            config.height = '7em';
            config.toolbar = [
                [ 'Source','Format','Bold','Italic','Underline','BulletedList','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-', 'Outdent', 'Indent'],
                { name: 'links', items: [ 'Link', 'Unlink' ] },
                { name: 'colors', items: [ 'TextColor', 'BGColor' ] },
                { name: 'insert', items: [ 'Image'] }
                ] ;
            config.uploadUrl = '';
        }
        return true;
    });
}

/* Dropify */

function renderDropify(){

   $('.dropify').dropify({
        messages: {
            default: 'Arrastra y suelta o haz clic para añadir el archivo',
            replace: 'Arrastra y suelta o haz clic para reemplazar el archivo',
            remove: 'Eliminar',
            error: 'El fichero es demasiado grande'
        }
    });
    
   //Saizfact: mostrar nombre de la imagen original y no el que tiene el fichero en el sistema (hash)
   $( ".dropify-wrapper" ).each(function() {
       $(this).parent().children().find(".dropify-filename-inner").text($(this).parent().children().find("input").attr('nombre'));
   });
}

function renderNestable(){
    $('.menu-nestable').nestable(
        {
            rootClass: 'menu-nestable',
            listClass: 'menu-nestable-list',
            itemClass: 'menu-nestable-item',
            dragClass: 'menu-nestable-dragel',
            handleClass: 'menu-nestable-handle',
            collapsedClass: 'menu-nestable-collapsed',
            placeClass: 'menu-nestable-placeholder',
            emptyClass: 'menu-nestable-empty'
        }
    );
}

function renderRangeDatePicker(element){
    if (element != null && element.startDate != undefined && element.startDate != '') {
        var startDate = element.startDate;
    } else {
        var startDate = new Date();
        var year = startDate.getFullYear();
        var month = startDate.getMonth();
        var day = startDate.getDate();
    }

    if (element != null && element.endDate != undefined && element.endDate != '') {
        var endDate = element.endDate;
    } else {
        var endDate = new Date();
        var year = startDate.getFullYear();
        var month = startDate.getMonth();
        var day = startDate.getDate();
    }

    $('.range-datepicker').daterangepicker({
        locale: {
            "format": "DD-MM-YYYY",
            "separator": " / ",
            "applyLabel": "Aceptar",
            "cancelLabel": "Cancelar",
            "fromLabel": "Desde",
            "toLabel": "Hasta",
            "customRangeLabel": "Custom",
            "daysOfWeek": ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],                   
            "monthNames": [
                'Enero','Febrero','Marzo','Abril','Mayo','Junio',
                'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'
            ],
            "firstDay": 1
        },
        drops: 'up',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        startDate: startDate,
        endDate: endDate,
    });
}

function renderRangeDatePicker2() {
    if( $('.range-datepicker2').length > 0){
        $('.range-datepicker2').each(function (i, obj) {
            
            var startDate = $(obj).data('fecha-desde') ? $(obj).data('fecha-desde') : new Date();
            var endDate = $(obj).data('fecha-hasta') ? $(obj).data('fecha-hasta') : new Date();
            
            $(obj).daterangepicker({
                locale: {
                    "format": "DD-MM-YYYY",
                    "separator": " / ",
                    "applyLabel": "Aceptar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Custom",
                    "daysOfWeek": ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                    "monthNames": [
                        'Enero','Febrero','Marzo','Abril','Mayo','Junio',
                        'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'
                    ],
                    "firstDay": 1
                },
                drops: 'up',
                buttonClasses: ['btn', 'btn-sm'],
                applyClass: 'btn-danger',
                cancelClass: 'btn-inverse',
                startDate: startDate,
                endDate: endDate
            });
        });
    }
}

function renderRangeDatePickerSingle() {
    if( $('.range-datepicker-single').length > 0){
        $('.range-datepicker-single').each(function (i, obj) {
            
            var startDate = $(obj).data('fecha') ? $(obj).data('fecha') : new Date();
            
            $(obj).daterangepicker({
                locale: {
                    "format": "DD-MM-YYYY",
                    "separator": " / ",
                    "applyLabel": "Aceptar",
                    "cancelLabel": "Cancelar",
                    "fromLabel": "Desde",
                    "toLabel": "Hasta",
                    "customRangeLabel": "Custom",
                    "daysOfWeek": ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
                    "monthNames": [
                        'Enero','Febrero','Marzo','Abril','Mayo','Junio',
                        'Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'
                    ],
                    "firstDay": 1
                },
                singleDatePicker: true,
                startDate: startDate
            });
        });
    }
}

function renderAutocomplete(){

    var form = '#' + $('.admin-form').attr('id');
    var url = $(form).prop('action');

    var options = {

        url: function(search) {
            return url + '/autocomplete/' + search;
        },

        list: {

            showAnimation: {
                type: "fade", 
                time: 400,
                callback: function() {}
            },
    
            hideAnimation: {
                type: "slide", 
                time: 400,
                callback: function() {}
            },

            maxNumberOfElements: 6,
            
            match: {
                enabled: false
            },

            sort: {
                enabled: true
            },

            onChooseEvent:  function() {
                var selectedItem = $(".autocomplete").getSelectedItemData();
                $('.producto_descripcion').val(selectedItem.descripcion);
                $('.producto_pvf').val(selectedItem.pvf);
            }   

        },

        template: {

            type: "custom",

            method: function(value, item) {
                
                if(item){
                    return  value + " " + item.descripcion;    
                }
            }
        },

        adjustWidth: false,
        highlightPhrase: false,
        getValue: "id" 
    };

    $(".autocomplete").easyAutocomplete(options);
}

function renderDatatable(){
    if( $('.datatable-simple').length > 0){
        $('.datatable-simple').each(function (i, obj) {
            $(obj).DataTable();
        });
    }
}

/* renderizado de elementos */

function renderElements(element){

    if( $(".select2").length > 0 ){   
        renderSelect();
    }

    if( $("input.bt-switch[type='checkbox'], input.bt-switch[type='radio']").length > 0 ){  
        renderSwitch();
    }

    if( $("textarea.rich").length > 0 ){
        renderCKEditor();
    }
    
    if( $(".dropify").length > 0 ){
        renderDropify();
    }

    if( $(".menu-nestable").length > 0){
        renderNestable();
    }

    if( $('.range-datepicker').length > 0){
        renderRangeDatePicker(element);
    }

    if( $('.range-datepicker2').length > 0){
        renderRangeDatePicker2();
    }

    if( $('.range-datepicker-single').length > 0){
        renderRangeDatePickerSingle();
    }

    if( $('.autocomplete').length > 0){
        renderAutocomplete();
    }

    if( $('.datatable-simple').length > 0){
        renderDatatable();
    }
}

function focusEdit(el) {
	setTimeout(function() {
		$(el).putCursorAtEnd();
	}, 300);
}

//Document Ready

$(document).ready(function(){
    debugger;
    renderTable();
    renderTable2();
    renderTable3();
    renderElements(null);
    //Floating  Scroll 10/05/2022
    $(".overflow-panel").floatingScroll();
    //Typsi with img 30/06/2022
    //$( ".tooltip-text" ).tooltip({ content: ' <img src="/images/favicon.png"/>' });
    
    //$( ".tooltip-text-aux" ).tooltip({ position: relative });
    
    //$( ".tooltip-text-aux" ).tooltip({ content: ' <img src="http://localhost:9002/images/favicon.png"/>' });
    //$( ".submit" ).tooltip({ content: ' <img src="http://localhost:9002/images/favicon.png"/>' });
    /*
    $("#create-modal-part").validate({
    rules: {
      employe_id : {
        required: true,
      },
    }
    */
    setInterval("update()",30000); //30 segundos

});


//navigation_buttons

/* Boton 'Nuevo' */

$(document).on('click', '#create-button', function(){

debugger;
    var url = $(this).attr('url');
    var template = $('#two-columns').length ? 'two-columns' : 'one-column';

    $.get( url , function(result){
        
        switch(template) {
            
            case 'one-column':
                $('.master-layout').html(result.layout);
                renderElements(result);
                if ($('.master-layout input[name="username"]').length) {
                	focusEdit('.master-layout input[name="username"]');
                } else {
                    focusEdit('.master-layout input[name="nombre"]');
                }
                break;

            case 'two-columns':
                $('#two-columns-form').html(result.form);
                window.history.pushState('', '', getParent(url));
                renderElements(result);
                focusEdit('#two-columns-form input[name="nombre"]');
                break;
        }
    });
});

//Available button

$(document).on('click', '.available', function(event){
  
  
  
    debugger;
    event.preventDefault();
    
    var template = $('#two-columns').length ? 'two-columns' : 'one-column';
    var form =  $(this).closest("form");
    var formData = new FormData($(form)[0]);
    
    
    $('#modal_change_available_form').find('input[name="id"]').val(formData.get('id'));
    $('#modal_change_available_form').find('input[name="code"]').val(formData.get('code'));
    $('#modal_change_available_form').find('input[name="action"]').val(formData.get('action'));
    $('#modal_change_available_form').find('input[name="available"]').val($('#available-'+formData.get('id')).prop('checked') );
    
    var text =$('#confirmCreateModalAvailableTitle').text();
    if ($('#modal_change_available_form').find('input[name="available"]').val() == 'false'){
        text = text.replace ("unavailable","available");
        text = text.replace ("available","unavailable");
        
    }else if ($('#modal_change_available_form').find('input[name="available"]').val() == 'true'){
        text = text.replace ("unavailable","available");
    } 
    $('#confirmCreateModalAvailableTitle').text(text);
    
});


//Available button


$(document).on('click', '#api-available', function(event){
    
    debugger;
    //Quito los posibles los mensajes de error anteriores 
    $('.error-container').removeClass("active");
    event.preventDefault();
    

    
  var laravelToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
  
  
  var template = $('#two-columns').length ? 'two-columns' : 'one-column';
  //var form = '#' + $('.admin-form').attr('id');
  var form =  $(this).closest("form");
  var formData = new FormData($(form)[0]);
  
  
  
  axios({
    method: 'post',
    url: '/admin/printers',
    data: formData,
    headers: {'Content-Type': 'multipart/form-data' }
    })
    .then(function (result) {
        //handle success
        //console.log(response);
        switch(template) {
                case 'one-column':
                   let input_name = "input[name='available-"+result.data.id+"']";
                   if (result.data.available == 'true'){
                        //$('#available'+result.id).prop("checked",true);
                        $( input_name ).prop("checked",true);
                    }else{
                        $( input_name ).prop("checked",false);
                    }
                     closeModal();
                   
                    break;
                case 'two-columns':
                    $('#two-columns-form').html(result.form);
                    window.history.pushState('', '', url + '/' + result.id);
                    datatableDraw();
                    renderElements(result);
                    notification(result.message);
                    break;

                case 'portfolio':
                    closeModal();
                    $('.master-layout').html(result.layout);
                    $('#one-column-portfolio').html(result.portfolio);
                    window.history.pushState('', '', result.url + '/' + result.id);
                    renderTable();
                    renderElements(result);
                    notification(result.message);
                    break;
            }
        
    })
    .catch(function (response) {
        //handle error
        console.log(response);
    });
  

});

$(document).on('click', '.change-roll', function(e){

    
    debugger;
    
    e.preventDefault();
    
   
    //Mandar varibles: 
    var laravelToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
  
  
  var template = $('#two-columns').length ? 'two-columns' : 'one-column';
  //var form = '#' + $('.admin-form').attr('id');
  var form =  $('#modal_change_roll_form');
  
  $('#modal_change_roll_form').find('input[name="id"]').val($(this).data('id'));
  $('#modal_change_roll_form').find('input[name="code"]').val($(this).data('code'));
  $('#modal_change_roll_form').find('input[name="weight"]').val($(this).data('weight'));
  $('#modal_change_roll_form').find('input[name="datetime_replace"]').val($(this).data('datetime_replace'));
  $('#modal_change_roll_form').find('input[name="roll_end"]').val($(this).data('roll_end'));
  
});


$(document).on('click', '#api-change-roll', function(e){

    debugger;
    e.preventDefault();
    
    var laravelToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    var template = $('#two-columns').length ? 'two-columns' : 'one-column';
    //var form = '#' + $('.admin-form').attr('id');
    var form =  $(this).closest("form");
    var formData = new FormData($(form)[0]);
    
    axios({
    method: 'post',
    url: '/admin/printers',
    data: formData,
    headers: {'Content-Type': 'multipart/form-data' }
    })
    .then(function (result) {
        //handle success
        //console.log(response);
            switch(template) {
                case 'one-column':
                    debugger;
                    $('#weight-'+result.data.id).html(result.data.roll_weight);
                    var currentRow=$('#weight-'+result.data.id).closest("tr");
                    
                    currentRow.find("td:eq(5)").html(result.data.roll_replacement_datetime); // roll_replacement
                    
                    closeModal();
                    break;

                case 'two-columns':
                    $('#two-columns-form').html(result.form);
                    window.history.pushState('', '', url + '/' + result.id);
                    datatableDraw();
                    renderElements(result);
                    notification(result.message);
                    break;

                case 'portfolio':
                    closeModal();
                    $('.master-layout').html(result.layout);
                    $('#one-column-portfolio').html(result.portfolio);
                    window.history.pushState('', '', result.url + '/' + result.id);
                    renderTable();
                    renderElements(result);
                    notification(result.message);
                    break;
            }
        
    })
    .catch(function (response) {
        //handle error
        console.log(response);
    });

    
});



$(document).on('click', '.play-button', function(e){

    debugger;
    e.preventDefault();
    

    $('.preloader').css('display','block');
    var part_id = $(this).data('part');
    var start = $(this).data('start');
    var printer_id = $(this).data('printer');
    var state=$(this).data('state');
    
 
    axios({
    method: 'POST',
    url: '/admin/parts/can_play',
    data: {part_id:part_id,start:start,printer_id:printer_id,state:state },
    headers: {'Content-Type': 'multipart/form-data' }
    })
    .then(function (result) {
        //handle success
        //console.log(response);
        if (result.data.can_play == false){
            debugger;
            
            $('.preloader').css('display','none');
            $('#start_date_time').val(start);
            $('#printer_id').val(printer_id);
            $('#part_id').val(part_id);
            $('#state_id').val(state);
            
            //$("#modal_start_part").attr("href","/admin/parts/play/"+start+"/"+printer_id+"/"+part_id+"/"+state );
            $('.modal').modal('show');
            $('.preloader').css('display','none');
            
            
            
        }else{
            axios({
            method: 'GET',
            url: '/admin/parts/play/'+start+'/'+printer_id+'/'+part_id+'/'+state,
            headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (result_play) {
                debugger;
                $('.master-layout').html(result_play.data.layout);
                //window.history.pushState('', '', url + '/' + result_play.id);
                renderElements(result_play);
                $('.preloader').css('display','none');
            })    
            .catch(function (response) {
                //handle error
                console.log(response);
            })
        }
    })
    .catch(function (response) {
        //handle error
        console.log(response);
    });

    
});

$(document).on ('click','#left-to-right-direction',function(e){
    debugger;
    e.preventDefault();
    if (($(this).val()== 'false')||($(this).val()== 'initial')){
        $(this).val('true');
    }else{
        $(this).val('false'); 
    }
    
    var left_to_right_direction = $(this).val();
    $('.preloader').css('display','block');
    axios({
        method: 'POST',
        url: '/admin/parts/state',
        data: {left_to_right_direction: left_to_right_direction},
        headers: {'Content-Type': 'multipart/form-data' }
    })
    .then(function (result) {
        debugger;
        
        var left_to_right = false; 
        
        if ($('#left-to-right-direction').val()== 'true') {
            left_to_right = true;
        }
        
        $('.master-layout').html(result.data.layout);
        renderElements(result);
        
        if ( left_to_right ) {
            $('#left-to-right-direction').prop('checked',true);
            $('#left-to-right-direction').val('true');
        }
        
        
        $('.preloader').css('display','none');
        //location.reload();
    })    
    .catch(function (response) {
        //handle error
        console.log(response);
    });

});

/*
$('#left-to-right-direction').change(function(){
    debugger;
    //alert($(this).prop('checked'));
    var left_to_right_direction = $(this).prop('checked');
    $('.preloader').css('display','block');
    axios({
        method: 'POST',
        url: '/admin/parts/state',
        data: {left_to_right_direction: left_to_right_direction},
        headers: {'Content-Type': 'multipart/form-data' }
    })
    .then(function (result) {
        debugger;
        $('.master-layout').html(result.data.layout);
        renderElements(result);
        
        $('.preloader').css('display','none');
        //location.reload();
    })    
    .catch(function (response) {
        //handle error
        console.log(response);
    });
});
*/
$(document).on('click', '.show-button-modal-part', function(e){
    debugger;
    e.preventDefault();
    $('#start_date_time').val($(this).data('start_date_time'));
    $('#start_date_time_print').val($(this).data('start_date_time_print'));
    $('#initiated_current').val($(this).data('initiated_current'));
    $('#file_id').val($(this).data('file_id'));
    
    var button_current = 'printed';
    
    
    if ($(this).data('button_current')=='play'){
         button_current = 'play';
         /*
         button ='<a title="" id="button-time-line-play-'+ 
                        $(this).data('part_current')+'" class="button-time-line play-button-state" data-start ="'+ 
                        $(this).data('start_date_time') + '" data-printer = "' + 
                        $(this).data('printer')+'" data-part = "'+
                        $(this).data('part_current')+'" data-state = "9" ><img  style="width:25px" src="/assets/admin/images/play.png"></a>';
         */
         

    }else if ($(this).data('button_current')=='cancel'){
        
        button_current = 'cancel';
        
    }
    $('#button_current').html(button_current);
    
    var action = 'Printed';
    if ($(this).data('action_current')=='play'){
        action = 'Start Printing';
        //$('#label_error','#select_error','#printing-real-time','#incident-description').css('display','none') ;
        //if ($(this).data('state_current') == ')
        $('.cancel-part-group-current').css('display','none');
        $('#submit-current-start').css('display','block');
        $('#current-start').css('display','block');
        $('#submit-next-start').css('display','none');
        $('#submit-current-cancel').css('display','none');
    }else{
        action = 'Cancel Printing';
        $('.cancel-part-group-current').css('display','block');
        $('#submit-current-cancel').css('display','block');
        $('#current-cancel').css('display','inline-block;');
        $('#current-cancel').attr('href','/admin/parts/cancel_state/'+$(this).data('start_date_time')+'/'+$(this).data('part_current')+'/'+$(this).data('printer')+'/')
        $('#submit-current-start').css('display','none');
        $('#submit-next-start').css('display','block');
    }
    $('#current_action').val($(this).data('action_current'));
    //$('#submit').html(action);
    
    
    
    $('#printer').html($(this).data('printer'));
    $('#printer_id').val($(this).data('printer'));
    $('#printer_id_post').val($(this).data('printer'));
    $('#id_to_disable_printer').val($(this).data('printer'));
    
    $('#code_printer').val($(this).data('code_printer'));
    $('#available_printer').val($(this).data('available_printer'));
    
    $('#order_id').val($(this).data('order_id'));
    
    $('#ModalPartTitleAux').html($(this).data('code_printer'));
    $('#order_current').html($(this).data('order_current'));
    $('#product_current').html($(this).data('product_current')+' - '+$(this).data('final_product_current'));
    
    $('#part_current').html('<strong>'+"Part: "+$(this).data('part_current')+'</strong>');
    $('#current_part_id').val($(this).data('part_current'));
    
    $('#file_current').html('<strong>'+$(this).data('file_name_current')+'</strong>');
    $('#days_current').html($(this).data('days_current'));
    $('#time_current').html($(this).data('time_current'));
    $('#start_current').html($(this).data('start_compact_current'));
    $('#end_current').html($(this).data('end_compact_current'));
    $('#time_left_current').html($(this).data('time_left_current'));
    $('#time_printing_current').html($(this).data('time_printing_current'));
    $('#percent_printing_current').html($(this).data('percent_printing_current')+'%');
    
    $('#current_state_id').val('9');
     
    
    
    
    $('#img-product-current').attr("src",'/images/products/'+$(this).data('file_name_current')+'.png');
    //$('#img-product').attr("src",'/images/products/P10001_v2_g9_2x.png'); //Apaisada
    //$('#img-product').attr("src",'/images/products/S10071_v6_g17_6x.png'); //Menos apaisada - casi cuadrada
    //$('#img-product').attr("src",'/images/products/S10055_v9_g17.png'); //Cuadrada
    
    
    
    
    
    
    $('#initiated_next').val($(this).data('initiated_next'));
    
    var button_next = 'printed';
    
    
    if ($(this).data('button_next')=='play'){
         button_next = 'play';
         /*
         button ='<a title="" id="button-time-line-play-'+ 
                        $(this).data('part_next')+'" class="button-time-line play-button-state" data-start ="'+ 
                        $(this).data('start_date_time') + '" data-printer = "' + 
                        $(this).data('printer')+'" data-part = "'+
                        $(this).data('part_next')+'" data-state = "9" ><img  style="width:25px" src="/assets/admin/images/play.png"></a>';
         */

    }else if ($(this).data('button_next')=='cancel'){
        
        button_next = 'cancel';
        
    }
    $('#button_next').html(button_next);
    
    var action = 'Printed';
    if ($(this).data('action_next')=='play'){
        action = 'Start Printing';
        //$('#label_error','#select_error','#printing-real-time','#incident-description').css('display','none') ;
        $('.cancel-part-group-next').css('display','none');
        if ($(this).data('action_current') =='play'){
            $('#submit-next-start').css('display','none');
        }
        $('#submit-next-cancel').css('display','none');
    }else{
        action = 'Cancel Printing';
        $('.cancel-part-group-next').css('display','block');
        $('#submit-next-cancel').css('display','block');
        $('#submit-next-start').css('display','none');
    }
    $('#next_action').val($(this).data('action_next'));
    //$('#submit').html(action);
    
    $('#printer').html($(this).data('printer'));
    $('#printer_id').val($(this).data('printer'));
    
    $('#ModalPartTitleAux').html($(this).data('code_printer'));
    
    
    
    
    $('#order_next').html($(this).data('order_next'));
    $('#product_next').html($(this).data('product_next')+' - '+$(this).data('final_product_next'));
    
    $('#part_next').html('<strong>'+"Part: "+$(this).data('part_next')+'</strong>');
    $('#next_part_id').val($(this).data('part_next'));
    
    $('#file_next').html('<strong>'+$(this).data('file_name_next')+'</strong>');
    $('#days_next').html($(this).data('days_next'));
    $('#time_next').html($(this).data('time_next'));
    $('#start_next').html($(this).data('start_compact_next'));
    $('#end_next').html($(this).data('end_compact_next'));
    $('#time_left_next').html($(this).data('time_left_next'));
    $('#time_printing_next').html($(this).data('time_printing_next'));
    $('#percent_printing_next').html($(this).data('percent_printing_next')+'%');
    
    $('#next_state_id').val('9');
    
    $('#img-product-next').attr("src",'/images/products/'+$(this).data('file_name_next')+'.png');
    
    $('#create-modal-part').modal('show');
    
    $('.preloader').css('display','none');
    
    $('#start_date_time_not_possible_state_post').val($(this).data('start_date_time'));
    $('#printer_id_not_possible_state_post').val($(this).data('printer'));
    $('#part_id_not_possible_state_post').val($(this).data('part_next'));
    $('#state_id_not_possible_state_post').val($(this).data('state_next'));
    
    if (
        (
         ($(this).data('button_current')=='last_slot_out_range') 
         || 
         (($(this).data('button_current')=='cancel')&& ($(this).data('file_name_next')== null))
        )
        &&
        ($(this).data('state_current') != 10)
       ){
        //Ponemos el botón de finished y botón de Cancel
        $('#submit-finish').css('display','block');
        $('.cancel-post-button-state').css('display','block');
        $('#select_error').parent().css('display','block');
        $('#incident_description').parent().parent().css('display','block');
        
    
    }else{
        if (($(this).data('button_current')!='play') && ($(this).data('button_current')!='cancel')){
            //Quitamos el botón de Cancel
            $('.cancel-post-button-state').css('display','none');
            $('#select_error').parent().css('display','none');
            $('#incident_description').parent().parent().css('display','none');
            $('#final_hours').parent().parent().css('display','none');
             
        }else{
            $('#submit-finish').css('display','none');
        }
    }
    
    if ($(this).data('file_name_next') == null){
        $('#next_div').css('display','none');
    }else{
        $('#next_div').css('display','block');
    }

});


//Disable Printer
$(document).on('click', '#disable-printer-button', function(event){
    
    debugger;
    //Quito los posibles los mensajes de error anteriores 
    $('.error-container').removeClass("active");
    event.preventDefault();
    

    
  
  
  var form =  $(this).closest("form");
  var formData = new FormData($(form)[0]);
  
  $('.preloader').css('display','block');
  
  axios({
    method: 'post',
    url: '/admin/printers',
    data: formData,
    headers: {'Content-Type': 'multipart/form-data' }
    })
    .then(function (result) {
        
        $('.preloader').css('display','none');
        closeModal();
        location.reload();
        //Redirect
        
    })
    .catch(function (response) {
        //handle error
        console.log(response);
    });
});


//Enable Printer
$(document).on('click', '.enable-printer-button', function(e){
    debugger;
    e.preventDefault();
    var action = 'available';
    var available = 'true';
    var printer_id = $(this).data('printer');
    $.confirm({
        title: 'Enable Printer',
        content: 'Are you sure you want to enable the printer?',
        type: 'green',
        draggable: true,
        dragWindowBorder: false,
        buttons: {   
            ok: {
                text: "Ok",
                btnClass: 'btn-primary',
                keys: ['enter'],
                action: function(){
                    debugger;
                    $('.preloader').css('display','block');
                    
                    
                    
                    axios({
                    method: 'POST',
                    url: '/admin/printers',
                    data: {action:action,available:available,id:printer_id },
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (result_cancel) {
                        debugger;
                        $('.preloader').css('display','none');
                        location.reload();
                
                    })    
                    .catch(function (response) {
                        //handle error
                        console.log(response);
                    })
                }
            },
            cancel: function(){
                console.log('the user clicked cancel');
            }
        }
    });

});


//Disable Printer
$(document).on('click', '.disable-printer-button', function(e){
    debugger;
    e.preventDefault();
    var action = 'disable';
    var available = 'false';
    var printer_id = $(this).data('printer');
    $.confirm({
        title: 'Disable Printer',
        content: 'Are you sure you want to disable the printer?',
        type: 'green',
        draggable: true,
        dragWindowBorder: false,
        buttons: {   
            ok: {
                text: "Ok",
                btnClass: 'btn-primary',
                keys: ['enter'],
                action: function(){
                    debugger;
                    $('.preloader').css('display','block');
                    
                    
                    
                    axios({
                    method: 'POST',
                    url: '/admin/printers',
                    data: {action:action,available:available,id:printer_id },
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (result) {
                        debugger;
                        $('.preloader').css('display','none');
                        location.reload();
                
                    })    
                    .catch(function (response) {
                        //handle error
                        console.log(response);
                    })
                }
            },
            cancel: function(){
                console.log('the user clicked cancel');
            }
        }
    });

});
    
$('.enable-printer-button-2').confirm({
        title: 'Enable Printer',
        content: 'Are you sure you want to enable the printer?',
        type: 'green',
        draggable: true,
        dragWindowBorder: false,
        buttons: {   
            ok: {
                text: "Ok",
                btnClass: 'btn-primary',
                keys: ['enter'],
                action: function(){
                    debugger;
                    $('.preloader').css('display','block');
                    
                    var action = 'available';
                    var available = 'true';
                    var printer_id = $(this).data('printer');
                    
                    axios({
                    method: 'POST',
                    url: '/admin/printers',
                    data: {action:action,available:available,id:printer_id },
                    headers: {'Content-Type': 'multipart/form-data' }
                    })
                    .then(function (result_cancel) {
                        debugger;
                        $('.preloader').css('display','none');
                        location.reload();
                
                    })    
                    .catch(function (response) {
                        //handle error
                        console.log(response);
                    })
                }
            },
            cancel: function(){
                console.log('the user clicked cancel');
            }
        }
    });

$(document).on('click', '.cancel-post-button-state', function(e){

    debugger;
    e.preventDefault();
    
    var laravelToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    var template = $('#two-columns').length ? 'two-columns' : 'one-column';
    //var form = '#' + $('.admin-form').attr('id');
    var form =  $(this).closest("form");
    var formData = new FormData($(form)[0]);
    
    var employe_id = $('#employe_id').val();
    
    if ($('#employe_id').val() == '') {
        //alert ("Operator is required");
        $.alert({
            title: 'Alert!',
            content: 'Operator is required!',
        });
        
        return false;
        
    }
    
    if ($('#error_id').val() == '') {
        //alert ("Error type is required");
        $.alert({
            title: 'Alert!',
            content: 'Error type is required!',
        });
        return false;
        
    }
    
    
    if ($('#final_hours').val() == '') {
        //alert ("Hours is required");
        $.alert({
            title: 'Alert!',
            content: 'Hours is required!',
        });
        return false;
        
    }else if (isNaN(parseInt($('#final_hours').val()))){
         //alert ("Hours must be an integer");
         $.alert({
            title: 'Alert!',
            content: 'Hours must be an integer!',
        });
         return false;
    }
    
    if ($('#final_minutes').val() == '') {
        //alert ("Minutes is required");
        $.alert({
            title: 'Alert!',
            content: 'Minutes is required!',
        });
        return false;
    }else if(isNaN(parseInt($('#final_minutes').val()))){
        //alert ("Minutes must be an integer");
        $.alert({
            title: 'Alert!',
            content: 'Minutes must be an integer!',
        });
        return false;
    }else if (parseInt ($('#final_minutes').val()) > 59){
        //alert ("Minutes cannot be greater than 59");
        $.alert({
            title: 'Alert!',
            content: 'Minutes cannot be greater than 59!',
        });
        return false;
    }
    
    
    
    $('.preloader').css('display','block');
    
    
    axios({
    method: 'POST',
    data: formData,
    url: '/admin/parts/cancel_state',
    headers: {'Content-Type': 'multipart/form-data' }
    })
    .then(function (result_cancel) {
        debugger;
        $('.master-layout').html(result_cancel.data.layout);
        //window.history.pushState('', '', url + '/' + result_play.id);
        renderElements(result_cancel);
        $('.preloader').css('display','none');
        window.history.pushState('','','/admin/parts/state');
        $('.modal-backdrop').remove();
    })    
    .catch(function (response) {
        //handle error
        console.log(response);
    })
    
});


$(document).on('click', '.play-post-button-state, #finish-post-button-state', function(e){
    debugger;
    e.preventDefault();
    
    //Validations
    if ($('#employe_id').val() == '') {
        alert ("Operator is required");
        return false;
        
    }

    if (
        (($(this).attr('id') == 'next_play-post-button-state') && ($('#button_current').html()!='printed'))
        ||
        ($(this).attr('id') == 'finish-post-button-state')
       ){
        if ($('#final_hours').val() == '') {
            //alert ("Hours is required");
            $.alert({
                title: 'Alert!',
                content: 'Hours is required!',
            });
            return false;
            
        }else if (isNaN(parseInt($('#final_hours').val()))){
             //alert ("Hours must be an integer");
             $.alert({
                title: 'Alert!',
                content: 'Hours must be an integer!',
            });
                 return false;
        }
        
        if ($('#final_minutes').val() == '') {
            //alert ("Minutes is required");
            $.alert({
                title: 'Alert!',
                content: 'Minutes is required!',
            });
            return false;
        }else if(isNaN(parseInt($('#final_minutes').val()))){
            //alert ("Minutes must be an integer");
            $.alert({
                title: 'Alert!',
                content: 'Minutes must be an integer!',
            });
            return false;
        }else if (parseInt ($('#final_minutes').val()) > 59){
            //alert ("Minutes cannot be greater than 59");
            $.alert({
                title: 'Alert!',
                content: 'Minutes cannot be greater than 59!',
            });
            return false;
        }
    }
    
    var laravelToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
    var template = $('#two-columns').length ? 'two-columns' : 'one-column';
    //var form = '#' + $('.admin-form').attr('id');
    var form =  $(this).closest("form");
    var formData = new FormData($(form)[0]);
    
    
    if (($(this).attr('id') == 'finish-post-button-state')){
       $('#current_action').val = 'printed';
    }
    
    
    $('.preloader').css('display','block');
    var part_id = $(this).data('part');
    var start = $(this).data('start');
    var printer_id = $(this).data('printer');
    var state=$(this).data('state');
    
    
    if ( 
        ($(this).attr('id') == 'next_play-post-button-state')
        ||
        ($(this).attr('id') == 'finish-post-button-state')
       ){
        
        axios({
        method: 'POST',
        url: '/admin/parts/can_play_state',
        //data: {part_id:part_id,start:start,printer_id:printer_id,state:state },
        data: formData,
        headers: {'Content-Type': 'multipart/form-data' }
        })
        .then(function (result) {
            //handle success
            //console.log(response);
            if (result.data.can_play_state == false){
                debugger;
                
                $('.preloader').css('display','none');
                /*
                $('#start_date_time').val(start);
                $('#printer_id').val(printer_id);
                $('#part_id').val(part_id);
                $('#state_id').val(state);
                */
                
                //$("#modal_start_part").attr("href","/admin/parts/play/"+start+"/"+printer_id+"/"+part_id+"/"+state );
                $('#create-modal-available').modal('show');
                //$('.preloader').css('display','none');
                //$('.modal-backdrop').remove();
                
            }else{
                axios({
                method: 'POST',
                data: formData,
                url: '/admin/parts/play_state',
                headers: {'Content-Type': 'multipart/form-data' }
                })
                .then(function (result_play) {
                    debugger;
                    $('.master-layout').html(result_play.data.layout);
                    //window.history.pushState('', '', url + '/' + result_play.id);
                    renderElements(result_play);
                    
                    $('.preloader').css('display','none');
                    //create-modal-part
                    
                    window.history.pushState('','','/admin/parts/state');
                    //$('.modal-backdrop').remove();
                    closeModal();
                })    
                .catch(function (response) {
                    //handle error
                    console.log(response);
                })
            }
        })
        .catch(function (response) {
            //handle error
            console.log(response);
        });
    }else{
        axios({
        method: 'POST',
        data: formData,
        url: '/admin/parts/play_state',
        headers: {'Content-Type': 'multipart/form-data' }
        })
        .then(function (result_play) {
            debugger;
            $('.master-layout').html(result_play.data.layout);
            //window.history.pushState('', '', url + '/' + result_play.id);
            renderElements(result_play);
            $('.preloader').css('display','none');
            window.history.pushState('','','/admin/parts/state');
            //$('.modal-backdrop').remove();
            closeModal();
        })    
        .catch(function (response) {
            //handle error
            console.log(response);
        })
    }
    
});



$(document).on('click', '.play-button-state', function(e){

    debugger;
    e.preventDefault();
    

    $('.preloader').css('display','block');
    var part_id = $(this).data('part');
    var start = $(this).data('start');
    var printer_id = $(this).data('printer');
    var state=$(this).data('state');
    
    axios({
    method: 'POST',
    url: '/admin/parts/can_play_state',
    data: {part_id:part_id,start:start,printer_id:printer_id,state:state },
    headers: {'Content-Type': 'multipart/form-data' }
    })
    .then(function (result) {
        //handle success
        //console.log(response);
        if (result.data.can_play_state == false){
            debugger;
            
            $('.preloader').css('display','none');
            $('#start_date_time').val(start);
            $('#printer_id').val(printer_id);
            $('#part_id').val(part_id);
            $('#state_id').val(state);
            
            //$("#modal_start_part").attr("href","/admin/parts/play/"+start+"/"+printer_id+"/"+part_id+"/"+state );
            $('.modal').modal('show');
            $('.preloader').css('display','none');
            $('.modal-backdrop').remove();
            
        }else{
            axios({
            method: 'GET',
            url: '/admin/parts/play_state/'+start+'/'+printer_id+'/'+part_id+'/'+state,
            headers: {'Content-Type': 'multipart/form-data' }
            })
            .then(function (result_play) {
                debugger;
                $('.master-layout').html(result_play.data.layout);
                //window.history.pushState('', '', url + '/' + result_play.id);
                renderElements(result_play);
                $('.preloader').css('display','none');
                window.history.pushState('','','/admin/parts/state');
                $('.modal-backdrop').remove();
            })    
            .catch(function (response) {
                //handle error
                console.log(response);
            })
        }
    })
    .catch(function (response) {
        //handle error
        console.log(response);
    });

    
});




$(document).on('click', '#modal_start_part', function(e){
    debugger;
    e.preventDefault();
    $('.preloader').css('display','block');
    /*
    var part_id = $(this).data('part');
    var start = $(this).data('start');
    var printer_id = $(this).data('printer');
    var state = $(this).data('state');
    
    
    
    $('#start_date_time').val(start);
    $('#printer_id').val(printer_id);
    $('#part_id').val(part_id);
    $('#state_id').val(state);
    $('#start_date_time').val(start);
    $('#printer_id').val(printer_id);
    $('#part_id').val(part_id);
    $('#state_id').val(state);
    */
    var start =  $('#start_date_time').val();
    var printer_id = $('#printer_id').val();
    var part_id = $('#part_id').val();
    var state= $('#state_id').val();
    
    axios({
        method: 'GET',
        url: '/admin/parts/play/'+start+'/'+printer_id+'/'+part_id+'/'+state,
        headers: {'Content-Type': 'multipart/form-data' }
        })
        .then(function (result_play) {
            debugger;
            $('.master-layout').html(result_play.data.layout);
            //window.history.pushState('', '', url + '/' + result_play.id);
            renderElements(result_play);
            $('.preloader').css('display','none');
            closeModal();
        })    
        .catch(function (response) {
            //handle error
            console.log(response);
        })
});

$(document).on('click', '#modal_start_part_state', function(e){
    debugger;
    e.preventDefault();
    $('.preloader').css('display','block');
    /*
    var part_id = $(this).data('part');
    var start = $(this).data('start');
    var printer_id = $(this).data('printer');
    var state = $(this).data('state');
    
    
    
    $('#start_date_time').val(start);
    $('#printer_id').val(printer_id);
    $('#part_id').val(part_id);
    $('#state_id').val(state);
    $('#start_date_time').val(start);
    $('#printer_id').val(printer_id);
    $('#part_id').val(part_id);
    $('#state_id').val(state);
    */
    var start =  $('#start_date_time').val();
    var printer_id = $('#printer_id').val();
    var part_id = $('#part_id').val();
    var state= $('#state_id').val();
    
    axios({
        method: 'GET',
        url: '/admin/parts/play_state/'+start+'/'+printer_id+'/'+part_id+'/'+state,
        headers: {'Content-Type': 'multipart/form-data' }
        })
        .then(function (result_play) {
            debugger;
            $('.master-layout').html(result_play.data.layout);
            //window.history.pushState('', '', url + '/' + result_play.id);
            renderElements(result_play);
            $('.preloader').css('display','none');
            closeModal();
        })    
        .catch(function (response) {
            //handle error
            console.log(response);
        })
});


$(document).on('click', '#modal_start_part_state_post', function(e){
    debugger;
    e.preventDefault();
    $('.preloader').css('display','block');
    
    
    
    var form =  $('#modal_part_form');
    var formData = new FormData($(form)[0]);

    axios({
        method: 'POST',
        data: formData,
        url: '/admin/parts/play_state',
        headers: {'Content-Type': 'multipart/form-data' }
        })
        .then(function (result_play) {
            debugger;
            $('.master-layout').html(result_play.data.layout);
            //window.history.pushState('', '', url + '/' + result_play.id);
            renderElements(result_play);
            $('.preloader').css('display','none');
            closeModal();
        })    
        .catch(function (response) {
            //handle error
            console.log(response);
        })
});


//crud_buttons

/* Boton 'Guardar' */

$(document).on('click', '#store-button', function(event){
    debugger;
    //Quito los posibles los mensajes de error anteriores 
    $('.error-container').removeClass("active");
    event.preventDefault();

    var template = $('#two-columns').length ? 'two-columns' : 'one-column';
    var subtemplate = $('#three-panels').length ? 'three-panels' : 'normal';
    var form = '#' + $('.admin-form').attr('id');
    var url = $(form).prop('action');

    formValidation(form);

    if($(form).valid()){

    var formData = new FormData($(form)[0]);
    var idform = $(form)[0].getAttribute('id');
    if( $("#"+idform+" textarea.rich").length > 0 ){
        $("#"+idform+" textarea.rich").each(function() {
        	formData.append($(this).attr('name'), CKEDITOR.instances[$(this).attr('id')].getData());
        });
    }

    $.ajax({
        type: 'post',
        url: url,
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            
            switch(template) {

                case 'one-column':
                    $('.master-layout').html(result.layout);
                    window.history.pushState('', '', url + '/' + result.id);
                    renderTable();
                    renderElements(result);
                    notification(result.message);
                    break;

                case 'two-columns':
                    if (subtemplate == 'normal'){
                        $('#two-columns-form').html(result.form);
                        window.history.pushState('', '', url + '/' + result.id);
                        datatableDraw();
                        renderElements(result);
                        notification(result.message);
                     }else{
                        $('.master-layout').html(result.layout);
                        window.history.pushState('', '', url + '/' + result.id);
                        renderTable();
                        renderTable2();
                        renderTable3();
                        renderElements(result);
                        notification(result.message);
                     }   
                    break;

                case 'portfolio':
                    closeModal();
                    $('.master-layout').html(result.layout);
                    $('#one-column-portfolio').html(result.portfolio);
                    window.history.pushState('', '', result.url + '/' + result.id);
                    renderTable();
                    renderElements(result);
                    notification(result.message);
                    break;
            }
        },

        error: function(response) {
            // var parsedJson = JSON.parse(response.responseText);
            var parsedJson = response.responseJSON;
            var errorString = '';

            if (parsedJson.errors) {
                $.each(parsedJson.errors, function( key, value) {
                    errorString += '<li>' + value + '</li>';
                });
            } else if (parsedJson.message) {
                errorString += '<li>' + parsedJson.message + '</li>';
            }

            $('.error-container').addClass('active');
            $('.error-container').html(errorString);
        }
    });

    return false;
    
    }
});

/* Boton 'Reset' */

$(document).on('click', '#reset-button', function(){

    debugger;
    var url = window.location.href;
    var template = $('#two-columns').length ? 'two-columns' : 'one-column';

    $.get( url , function( result ) {

        switch(template) {
	        
	        case 'one-column':
	            $('#one-column-form').html(result.form);
	            renderElements(result);
	            break;
	
	        case 'two-columns':
	            $('#two-columns-form').html(result.form);
	            renderElements(result);
	            break;
	    }
	});
	
	return false;

});

/* Boton 'Eliminar' */

$(document).on('click', '#delete-button', function(){
    debugger;
    //e.preventDefault();

    var url = window.location.href;
    var token = $(this).attr('token');
    var template = $('#two-columns').length ? 'two-columns' : 'one-column';
    var subtemplate = $('#three-panels').length ? 'three-panels' : 'normal';

    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        type: 'delete',
        url: url,
        dataType: 'json',
        data: {_method: 'delete'},
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {

            switch(template) {
                
                case 'one-column':
                    closeModal();
                    $('.master-layout').html(result.layout);
                    window.history.pushState('', '', getParent(url));
                    notification(result.message);
                    renderTable();
                    renderElements(result);
                    break;

                case 'two-columns':
                    closeModal();
                    if (subtemplate == 'normal'){
                        $('#two-columns-form').html(result.form);
                        window.history.pushState('', '', getParent(url));
                        notification(result.message);
                        datatableDraw();
                        renderElements(result);
                    }else{
                        $('.master-layout').html(result.layout);
                        window.history.pushState('', '', getParent(url));
                        notification(result.message);
                        renderTable();
                    renderElements(result);
                    }
                    break;
            }
        },

        error: function(response) {
            // var parsedJson = JSON.parse(response.responseText);
            var parsedJson = response.responseJSON;
            closeModal();
            var errorString = '';

            if (parsedJson.errors) {
                $.each(parsedJson.errors, function( key, value) {
                    errorString += '<li>' + value + '</li>';
                });
            } else if (parsedJson.message) {
                errorString += '<li>' + parsedJson.message + '</li>';
            }

            $('.error-container').addClass('active');
            $('.error-container').html(errorString);
        }
    });

return false;

});

//Añadir Fichero docmental del Cliente
$(document).on('click', '#store-button-document', function(event){
    debugger;
    //Quito los posibles los mensajes de error anteriores 
    $('.error-container').removeClass("active");

    event.preventDefault();

    $("#documento").val("true");

    var template = $('#two-columns').length ? 'two-columns' : 'one-column';
    var form = '#' + $('.admin-form').attr('id');
    var url = $(form).prop('action');

    formValidation(form);

    if($(form).valid()){

        var formData = new FormData($(form)[0]);
        $.ajax({
            type: 'post',
            url: url,
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                //Nueva linea
                 var html = 
                    '<div id="document-'+result.document_id+'" class="col-lg-18 col-md-18 row" >'+
                        '<label>'+ result.document_type+': </label>&nbsp;&nbsp;'+
                        '<a href="/admin/clientes/documento/'+result.document_id+'" target="_blank">'+result.document_name+'</a>&nbsp;&nbsp;'+
                        '<a href="#"'+ 
                                    'class="b_remove-file  fa fa-trash text-inverse m-r-10" '+
                                    'title="Borrar"'+ 
                                    'id="'+result.document_id+'"'+ 
                                    'name="'+result.name+'"'+ 
                                    'data-toggle="modal"'+
                                    'data-target="#delete-document-modal"'+
                       ' />'+ 
                    '</div>'+
                    '<div class="col-lg-18 col-md-18 row"></div>';

                 var document_div = "#document-"+result.document_id;
                
                if ( $( document_div ).length ) {
                    //Si trata de un cambio de documento (mismo tipo de documento)
                    $( document_div ).replaceWith( html );
                }else{
                    ///Si se trada de un nuevo documento(distinto tipo)
                    $('span.document').append(html);
                }
                //Reinicio el input hidden
                $("#documento").val("false");
                
                //Reseteo el input hidden
                var $el = $('#input_file_document');
                $el.wrap('<form>').closest('form').get(0).reset();
                $el.unwrap();
                
                //Quito los posibles los mensajes de error anteriores 
                $('.error-container').removeClass("active");
                
                result.layout
                
                $('.master-layout').html(result.layout);
                window.history.pushState('', '', url + '/' + result.id);
                renderTable();
                renderElements(result);
                notification(result.message);

            },

            error: function(response) {
                // var parsedJson = JSON.parse(response.responseText);
                var parsedJson = response.responseJSON;
                var errorString = '';
                
                //Reinicio el input hidden
                $("#documento").val("false");

                if (parsedJson.errors) {
                    $.each(parsedJson.errors, function( key, value) {
                        errorString += '<li>' + value + '</li>';
                    });
                } else if (parsedJson.message) {
                    errorString += '<li>' + parsedJson.message + '</li>';
                }

                $('.error-container').addClass('active');
                $('.error-container').html(errorString);
            }
        });
        return false;
    }
});

//Importar ficharo de ofertas
$(document).on('click', '#import-offers-button', function(event){
    //Quito los posibles los mensajes de error anteriores 
    $('.error-container').removeClass("active");

    event.preventDefault();
    
    var form = '#' + $('.admin-form').attr('id');
    var url = $(form).prop('action');

    formValidation(form);

    var formData = new FormData($(form)[0]);
    
    if($(form).valid()){

        var formData = new FormData($(form)[0]);
        $.ajax({
            type: 'post',
            url: url,
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                //Nueva linea
                $('.error-container').removeClass("active");
                notification(result.message);

            },

            error: function(response) {
                // var parsedJson = JSON.parse(response.responseText);
                var parsedJson = response.responseJSON;
                var errorString = '';
                
                //Reinicio el input hidden
                $("#documento").val("false");

                if (parsedJson.errors) {
                    $.each(parsedJson.errors, function( key, value) {
                        errorString += '<li>' + value + '</li>';
                    });
                } else if (parsedJson.message) {
                    errorString += '<li>' + parsedJson.message + '</li>';
                }

                $('.error-container').addClass('active');
                $('.error-container').html(errorString);
            }
        });
        return false;
    }
});

//Borrar Fichero docmental del Cliente
$(document).on('click', '.b_remove-file', function(event){
    var documento_a_borrar_id = $(this).attr('id');
    $("#documento-a-borrar").val(documento_a_borrar_id);
    var documento_a_borrar_name = $(this).attr('name'); 
    $("#document-name").html(documento_a_borrar_name);
    $("#documento").val("delete");
});



//Borrar Fichero docmental del Cliente
$(document).on('click', '#delete-document-button-confirm', function(event){
    //Quito los posibles los mensajes de error anteriores 
    $('.error-container').removeClass("active");

    event.preventDefault();

    var form = '#' + $('.admin-form').attr('id');
    var url = $(form).prop('action');

    var formData = new FormData($(form)[0]);
    
    formValidation(form);

    if($(form).valid()){

        var formData = new FormData($(form)[0]);
        $.ajax({
            type: 'post',
            url: url,
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                
                var document_div = "#document-"+result.document_id;
                $( document_div ).remove();
                
                closeModal();
                
                result.layout
                
                $('.master-layout').html(result.layout);
                window.history.pushState('', '', url + '/' + result.id);
                renderTable();
                renderElements(result);
                notification(result.message);
                          
            },
    
            error: function(response) {
            	// var parsedJson = JSON.parse(response.responseText);
                var parsedJson = response.responseJSON;
                var errorString = '';

                if (parsedJson.errors) {
                    $.each(parsedJson.errors, function( key, value) {
                        errorString += '<li>' + value + '</li>';
                    });
                } else if (parsedJson.message) {
                    errorString += '<li>' + parsedJson.message + '</li>';
                }

                $('.error-container').addClass('active');
                $('.error-container').html(errorString);
            }
        });
        return false;
    }
});    
	

//select_multiple

//falta para que detecte change, validar y añadir esconder options elegidas, pointer sobre todo el elemento

$(document).on('change', '.multiple-option', function(){
    debugger;
    var checkbox = $('.multiple-checkbox[value=' + $(this).val() + ']')
    checkbox.attr( 'checked', 'checked' );
    checkbox.parent().addClass('active');
});

$(document).on('click', '.multiple-checkbox', function(){
    debugger;
    $(this).prop( 'checked', false );
    $(this).parent().removeClass('active');
    $('#default-option').attr('selected', 'selected');
});

//select_related

$(document).on('change', '.primary-select-related', function(){
    $(".secondary-select-related").select2("val", "");
    selectRelated($(this).val());
});

function selectRelated(optionId){
    $('.secondary-select-related option').addClass('hidden');
    $('.secondary-select-related').prop("disabled", false); 
    $('.primary-select-related option[value="' + optionId + '"]').prop('selected', true);

    var matching = $('.secondary-select-related option').filter(function(){
        return $(this).attr('related-option-id') == optionId;
    });

    $(matching).removeClass('hidden'); 
}

// select_display

$(document).on('change', '.select-display', function(){

    $('.select-display-option').removeClass('visible');
    $('.select-display-option :input').val('');
    $('.select-option').prop("disabled", true);

    var select = $('.select-display-option#' + $(this).children(":selected").attr("id"));

    select.addClass('visible');
    select.find('.select-option').prop("disabled", false);
});

// Menus

/* 'Nuevo' item menu */

$(document).on('click', '#new-item', function(){
   
    var alias = $(this).data('language');
    var url = $(this).data('url');

    var drEvent = $('.dropify').dropify();
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();

    $.get(url, {
        _token: $('meta[name="csrf-token"]').attr('content')
    }, function (result) {
        $('.menu-modal').html(result.modal);
        renderElements(result);
        $('#item-rel-profile').val(alias);
        $('#new-item-modal').modal("toggle");
    });
});

/* 'Guardar' item menu */

$(document).on('click', '.modal-store', function(){
    debugger;
    event.preventDefault();
    
    var form = '#' + $('.modal-form').attr('id');
    
    if ($(this).data('type') == 'media'){
        form = '#media-item-form';
    }else if ($(this).data('type') == 'doc'){
        form = '#doc-item-form';
    }
    var url = $(form).prop('action');

    
    
    
    formValidation(form);
    if ($('#input_file').data('default-file') ==''){
        
        $(form).validate(); //sets up the validator
        if (form == '#media-item-form' ){
            $("#input_file.media").rules("add", "required");
        }else if (form == '#doc-item-form' ){
            $("#input_file.doc").rules("add", "required");
        }
       
    }
    

    if($(form).valid()){
        
        var formData = new FormData($(form)[0]);
        debugger;
        $.ajax({
            type: 'post',
            url: url,
            dataType: 'json',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                $('#new-item-modal').modal('hide');
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $('.master-layout').html(result.layout);
                notification(result.message);
                renderElements(result);
            },
    
            error: function(response) {
            	// var parsedJson = JSON.parse(response.responseText);
                var parsedJson = response.responseJSON;
                var errorString = '';

                if (parsedJson.errors) {
                    $.each(parsedJson.errors, function( key, value) {
                        errorString += '<li>' + value + '</li>';
                    });
                } else if (parsedJson.message) {
                    errorString += '<li>' + parsedJson.message + '</li>';
                }

                $('.error-container').addClass('active');
                $('.error-container').html(errorString);
            }
        });
    
        return false;
        
    }
});

/* 'Eliminar' item menu */

$(document).on('click', '.delete-menu-item', function(){

    var route = $(this).data().route;
    var id = $(this).data().id;
    var url = route + '/' + id;
    var token =  $('meta[name="csrf-token"]').attr('content');
    
        $.ajax({
            headers: {'X-CSRF-TOKEN': token},
            type: 'delete',
            url: url,
            dataType: 'json',
            data: {_method: 'delete'},
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                $('.master-layout').html(result.layout);
                $('#one-column-form').html(result.view);
                renderTable();
                renderElements(result);
            },
    
            error: function(response){
                  
            }
        });
    
    return false;
    
});

/* 'Editar' item menu */

$(document).on('click', '.edit-menu-item', function(){
        
    var route =  $(this).data().route;
    var id = $(this).data().id;
    var url = route + '/' + id;

    $.get( url , function(result){
        $('.menu-modal').html(result.modal);
        renderElements(result);
        $('#new-item-modal').modal('toggle');
    });
});

/* order_menu_items */

$(document).on('change', '.menu-nestable', function (e) {

    var url = $('.menu-nestable').attr('route');

    $.post(url, {
        order: JSON.stringify($(e.currentTarget).nestable('serialize')),
        _token: $('meta[name="csrf-token"]').attr('content')
    }, function (response) {});
});

/*select menu section*/

$(document).on('change', '#menu-section', function(){


    var selectBox = document.getElementById("menu-section");
    var route = selectBox.options[selectBox.selectedIndex].dataset.route;
    var url = route;

    $.get( url , function(result){
        $('.sidebar-routes-container').html(result.modal);

        if(result.menu_links.length > 0 ){
            $('#select-menu-links').addClass('visible');
        }
    });
});

//Filtro archivos portfolio

$('#filter-files').on('change', function () {

    var route =  $('.filter-files').attr('route');

    $.post(route, {
        filter: $('#filter-files').val(),
        _token: $('meta[name="csrf-token"]').attr('content')
    }, function (result) {
        $('.master-layout').html(result.content);
        $('#one-column-portfolio').html(result.portfolio);
        renderElements(result);
    });
});

//Filtro idioma slider

$('.nav-link-slide').on('click', function () {

    var route =  $('.nav-link-slide').attr('route');

    $.post(route, {
        filter: $(this).attr('aria-controls'),
        slide: $(this).attr('entity_type'),
        _token: $('meta[name="csrf-token"]').attr('content')
    }, function (result) {
        $('#one-column-portfolio').html(result.portfolio);
        renderElements(result);
    });
});

//Sección Media

/*Nuevo archivo*/

$(document).on('click', '#new-media', function(){
    
    debugger;
   
    var alias = $(this).data('idioma');
    var proyecto_id = $(this).data('proyecto-id');

    var drEvent = $('.dropify').dropify();
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();
    

    $('#media-rel-profile').val(alias);
    $('#proyecto-id').val(proyecto_id);
    $('#media-link').text('');
    $('#media-name').val('');
    $("#media-id").val('');
    $('#new-media-modal').modal("toggle");
});


$(document).on('click', '#new-doc', function(){
    debugger;
    var alias = $(this).data('idioma');
    var proyecto_id = $(this).data('proyecto-id');

    var drEvent = $('.dropify').dropify();
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();
    

    $('#doc-rel-profile').val(alias);
    $('#proyecto-id-doc').val(proyecto_id);
    $('#doc-link').text('');
    $('#doc-name').val('');
    $("#doc-id").val('');
    $('#new-doc-modal').modal("toggle");
});


/*Editar archivo*/

$(document).on('click', '.media-input, .media-title', function(event){
    debugger;
    event.preventDefault();

    var route =  $(this).attr('route');
    var id = $(this).attr('id');
    var url = route + '/' + id;

    $.get( url , function(result){
        
        $('.media-modal').html(result.modal);
        renderDropify();
        $('#new-media-modal').modal("toggle");
        $('#new-doc-modal').modal("toggle");
    });
});

/*Eliminar archivo*/

$(document).on('click', '.media-close-button', function(){
    debugger;
    var id = $(this).attr('data-id');
    $('.delete-media').attr('data-id', id);
});

$(document).on('click', '.delete-media', function(){

    var id = $(this).attr('data-id');
    console.log(id);
    var route = $('#'+id).attr('route');
    console.log(route);
    var url = route + '/' + id;
    console.log(route);
    var token =  $('meta[name="csrf-token"]').attr('content');
    
        $.ajax({
            headers: {'X-CSRF-TOKEN': token},
            type: 'delete',
            url: url,
            dataType: 'json',
            data: {_method: 'delete'},
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                closeModal();
                $('.master-layout').html(result.layout);
                notification(result.message);
                renderElements(result);
            },
    
            error: function(response){
                console.log(response);
            }
        });
    
    return false;
});


//Sección Vademecum Documentos

/*Nuevo archivo*/

$(document).on('click', '#new-document', function(){
   
    var alias = $(this).data('language');

    var drEvent = $('.dropify').dropify();
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();

    $('#document-rel-profile').val(alias);
    $('#document-link').text('');
    $('#document-name').val('');
    $("#document-id").val('');
    $('#new-document-modal').modal("toggle");
});

/*Editar archivo*/

$(document).on('click', '.media-item', function(event){

    debugger;
    event.preventDefault();

    var route =  $(this).attr('route');
    var id = $(this).attr('id');
    var url = route + '/' + id;

    $.get( url , function(result){
        
        $('.media-modal').html(result.modal);
        renderDropify();
        $('#new-document-modal').modal("toggle");
    });
});


$(document).on('click', '.doc-item', function(event){
    debugger;
    
    event.preventDefault();

    var route =  $(this).attr('route');
    var id = $(this).attr('id');
    var url = route + '/' + id;
    
    $.get( url , function(result){
        
        $('.doc-modal').html(result.modal);
        renderDropify();
        $('#new-document-modal').modal("toggle");
    });
    
});

/*Eliminar archivo*/

$(document).on('click', '.media-close-button', function(){
    debugger;
    var id = $(this).attr('id');
    $('.delete-media').attr('id', id);
});

$(document).on('click', '.delete-media', function(){

    var route = $('.media-item').attr('route');
    var id = $(this).attr('id');
    var url = route + '/' + id;
    var token =  $('meta[name="csrf-token"]').attr('content');
    
        $.ajax({
            headers: {'X-CSRF-TOKEN': token},
            type: 'delete',
            url: url,
            dataType: 'json',
            data: {_method: 'delete'},
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                closeModal();
                $('.master-layout').html(result.layout);
                notification(result.message);
                renderElements(result);
            },
    
            error: function(response){
                  
            }
        });
    
    return false;
});

//Sección Slider

/*Nuevo slide*/

$(document).on('click', '#new-slide', function(){
   
    var drEvent = $('.dropify').dropify();
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();

    $('#new-slide-modal').modal("toggle");
});

/*Editar slide*/

$(document).on('click', '.slide-item', function(event){

    event.preventDefault();

    var route =  $(this).attr('route');
    var id = $(this).attr('id');
    var url = route + '/' + id;

    $.get( url , function(result){
        $('.slides-modal').html(result.modal);
        renderElements(result);
        $('#new-slide-modal').modal("toggle");
    });
});

/*Eliminar slide*/

$(document).on('click', '.slide-close-button', function(){
    var id = $(this).attr('id');
    $('.delete-slide').attr('id', id);
});

$(document).on('click', '.delete-slide', function(){

    var route = $('.slide-item').attr('route');
    var id = $(this).attr('id');
    var url = route + '/' + id;
    var token =  $('meta[name="csrf-token"]').attr('content');
    
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        type: 'delete',
        url: url,
        dataType: 'json',
        data: {_method: 'delete'},
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            closeModal();
            $('.master-layout').html(result.layout);
            notification(result.message);
            renderElements(result);
        },

        error: function(response){
                
        }
    });
    
    return false;
});

//Sección rotulos

/*Nuevo rotulo*/

$(document).on('click', '#new-rotulo', function(){

    var alias = $('#portfolio-alias').val();
    var rotulo_id = $('#rotulo_id').val()
    var route = $(this).attr('route');

    $.get( route , { alias: alias, rotulo_id: rotulo_id, _token: $('meta[name="csrf-token"]').attr('content') }, function(result){
        $('.rotulos-modal').html(result.modal);
        renderElements(result);
        $('#new-rotulos-modal').modal("toggle");
    });
});

/*Editar rotulo*/

$(document).on('click', '.rotulo-item', function(event){

    event.preventDefault();

    var route = $(this).attr('route');

    $.get( route , function(result){
        $('.rotulos-modal').html(result.modal);
        renderElements(result);
        $('#new-rotulos-modal').modal("toggle");
    });
});

/*Eliminar rotulo*/

$(document).on('click', '.rotulo-close-button', function(){
    var id = $(this).attr('id');
    $('.delete-rotulo').attr('id', id);
});

$(document).on('click', '.delete-rotulo', function(){

    var route = $(this).attr('delete-route');
    var alias = $(this).attr('alias');
    var id = $('.delete-rotulo').attr('id');
    
    $.post( route , { id: id, alias: alias, _token: $('meta[name="csrf-token"]').attr('content') }, function(result){
        closeModal();
        $('.master-layout').html(result.layout);
        notification(result.message);
        renderElements(result);
    });
    
    return false;
});

//Definir idioma del portfolio a la ventana modal cuando se cambia de pestaña de idioma

$(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) {

    if( $("#rotulo_id").length > 0 ){   
        var href = $(e.target).attr("href");
        var alias = href.replace('#',' ');
        var route = $('.nav-tabs').attr('route');
    
        $.get( route , { alias: alias, _token: $('meta[name="csrf-token"]').attr('content') }, function(result){
            $('.master-layout').html(result.layout);
            $('#one-column-portfolio').html(result.portfolio);
        });
    }
});

//Bloquear borrar parámetros en sección de traducciones y SEO

$(document).on('keydown', '.block-parameters', function() {
    var value = $(this).val();
    var rgxParameters = /\{.*?\}/g;
    // var rgxSlash = /\//ig;
    var parameters = value.match(rgxParameters);
    // var slashes = value.match(rgxSlash);

    $(this).data('url', value );
    $(this).data('parameters', parameters );
    // $(this).data('slashes', slashes);
});   

$(document).on('keyup', '.block-parameters', function(){
    var value = $(this).val();
    var rgxParameters = /\{.*?\}/g;
    var parameters = value.match(rgxParameters);

    var previousParameters = $(this).data('parameters');

    if(previousParameters == null && parameters == null){
        var is_same = true;
    }
    
    if(previousParameters != null && parameters == null){
        var is_same = false;
    }

    if(previousParameters != null && parameters != null){
        var is_same = (parameters.length == previousParameters.length) && parameters.every(function(element, index) {
            return element === previousParameters[index]; 
        });
    }

    if(!is_same){
        $(this).val($(this).data('url'));
    }
});

//Selector de página padre en PageController

$(document).on('change', '#select-domain', function() {

    $('.locale-seo-selector').prop("disabled", false);  

    var domain = $(this).val();    
    var parents = 0;

    $(".locale-seo-selector option").each(function()
    {
        if($(this).data('domain') == domain ){
            $(this).show()
        }else{
            $(this).hide()
        }
    });
});
$(document).on('click', '#close-modal-button.media-modal', function() {
    debugger;
    $('.error-container').html('');
    $('.error-container').removeClass('active');
    $('#orden').val('');
    if ($(this).data('type') == 'media'){
        $('#modal-title.media').html('Nueva Imagen');
    }else if ($(this).data('type') == 'doc'){
        $('#modal-title.doc').html('Nuevo Documento');
    } 
    $("#tipo_id").val("0");
    //Limpiar el modal
});



//DateTimePicker 27/04/2022

$(".datetime-picker").datetimepicker({
    format: 'YYYY-MM-d hh:mm:ss'
});

//Floating  Scroll 10/05/2022

$(function() {
  $('.overflow-panel').floatingScroll();
});



//Spinner load 11/05/2022
$(document).on('click', '#graph-link, .button-time-line, #button-time-line-play, #button-time-line-cancel,#button-start-date-time', function() {
    debugger;
    $('.preloader').css('display','block');
        
});

//Spinner load 11/05/2022
$(document).on('click', '#save-assign, #clear-assign', function() {
    debugger;
    $('.preloader').css('display','block');
        
});

//Zoom 12/05/2022
function changeZoom(value){
    $(".zoom").css("zoom",value+'%' );
}

//Blinks 13/05/2022
 function blink_text() {
     $('.blink').fadeOut(128);
     $('.blink').fadeIn(128).delay( 128);
     //$('.blink')..toggleClass("blink");
 }
 setInterval(blink_text, 16);

 function blink_text_part() {
     $('.blink_next_part').fadeOut(128);
     $('.blink_next_part').fadeIn(128).delay(128);
     //$('.blink')..toggleClass("blink");
 }
 setInterval(blink_text_part, 16);
 
 
  function blink_lamp() {
     $('.blink_lamp').fadeOut(800);
     $('.blink_lamp').fadeIn(800).delay(800);
     //$('.blink')..toggleClass("blink");
 }
 setInterval(blink_lamp, 80);

 
 
//Printer Detail Modal State 
 $('.btn-mais-info-current').on('click', function(event) {
    $( '.open_info_current' ).toggleClass( "hide" );
})
 $('.btn-mais-info-next').on('click', function(event) {
    $( '.open_info-next' ).toggleClass( "hide" );
})



$('#statePanel [data-toggle="tooltip"]').tooltip({
    animated: 'fade',
    placement: 'bottom',
    html: true
});

//Actualizar cada 30 segundos las páginas que nos muestran el estado 19/08/2022
//La función es llamada desde el on ready: setInterval("update()",30000); //30 segundos

function update(){
    //debugger;
    var current_location =  window.location.href;
    
    if (($('#create-modal-part').css('display')=='none')&&(current_location.includes('state'))){
        location.reload(true);
    }
}

