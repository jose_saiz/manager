/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	//config.extraPlugins = 'uploadimage';
	//config.uploadUrl = '/uploader/upload.php';
	//config.extraPlugins = 'myplugin,anotherplugin';
	
	
	config.filebrowserBrowseUrl = '/assets/admin/js/kcfinder/browse.php?opener=ckeditor&type=files';
    config.filebrowserImageBrowseUrl = '/assets/admin/js/kcfinder/browse.php?opener=ckeditor&type=images';
    config.filebrowserFlashBrowseUrl = '/assets/admin/js/kcfinder/browse.php?opener=ckeditor&type=flash';
    config.filebrowserUploadUrl = '/assets/admin/js/kcfinder/upload.php?opener=ckeditor&type=files';
    config.filebrowserImageUploadUrl = '/assets/admin/js/kcfinder/upload.php?opener=ckeditor&type=images';
    config.filebrowserFlashUploadUrl = '/assets/admin/js/kcfinder/upload.php?opener=ckeditor&type=flash';
	
	//Las siguientes configuraciones dan error
	
	//config.height: '300px',


	// Configure your file manager integration. This example uses CKFinder 3 for PHP.
	//config.browseUrl: '/ckfinder/ckfinder.html',
	//config.imageBrowseUrl: '/vendor/ckfinder/ckfinder.html?type=Images',
	//config.uploadUrl: '/vendor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
	//config.imageUploadUrl: '/vendor/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images'
	
	
	//Fin Las siguientes configuraciones dan error
    
    config.stylesSet = 'my_custom_style';
	
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' },
		{ name: 'about' }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Underline,Subscript,Superscript,Save,Iframe';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

    /**
     * No permitir algunos atributos
     * los margin-left y margin-right los provoca el Word al pegar
     */
     config.disallowedContent = 'p{margin*}; span{font*, line-height}';
     
     /**
      * Cambiar los tags de Negrita y Cursiva para que usen "b" e "i"
      */
     config.coreStyles_bold = { element : 'b', overrides : 'strong' };
     config.coreStyles_italic = { element : 'i', overrides : 'em' };
};
