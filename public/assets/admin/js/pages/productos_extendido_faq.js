
/**
 * Botón de Nueva FAQ
 */
$(document).on('click', '#new-faq-button', function(){
    $('#edit-faq-modal .modal-title').text('Temario');
    $('#edit-faq-modal .preloader-modal').hide();
    
    $('#faqId').val('');
    $('#edit-faq-modal input[name="nombre"]').val('');
    $('#edit-faq-modal input[name="activo"]').bootstrapSwitch('state', true);
    $('#edit-faq-modal input[name="orden"]').val('');
    $('#edit-faq-modal input[name^="locale"]').val('');
    $('#edit-faq-modal textarea[name^="locale"]').each(function(i, obj) {
        var $obj = $(obj);
        if ($obj.hasClass('rich')) {
            CKEDITOR.instances[$obj.attr('id')].setData('');
        } else {
            $obj.val('');
        }
    });
    setTimeout(function() {
        $('#faq-form input').first().focus();
    }, 550);
    return false;
});

/**
 * Botón de Editar FAQ en la tabla, para rellenar los datos del modal
 */
$(document).on('click', '#temario-table .edit-button', function(){
    $('#edit-faq-modal .modal-title').text('Editar FAQ');
    $('#edit-faq-modal .preloader-modal').show();
    
    var $row = $(this).parents('tr');
    var productoId = $('#id').val();
    var faqId = $row.data('id');
    $('#faqId').val(faqId);
    
    $.get('/admin/cursos/' + productoId + '/faqs/' + faqId + '/json', function( faq ) {
        $('#edit-faq-modal input[name="nombre"]').val(faq.nombre);
        $('#edit-faq-modal input[name="activo"]').bootstrapSwitch('state', faq.activo);
        $('#edit-faq-modal input[name="orden"]').val(faq.orden);
        $('#edit-faq-modal input[name^="locale"]').each(function(i, obj) {
            var $obj = $(obj);
            var anchor = $(obj).data('anchor');
            $obj.val(faq.locale[anchor]);
        });
        $('#edit-faq-modal textarea[name^="locale"]').each(function(i, obj) {
            var $obj = $(obj);
            var anchor = $(obj).data('anchor');
            if ($obj.hasClass('rich')) {
                CKEDITOR.instances[$obj.attr('id')].setData(faq.locale[anchor]);
            } else {
                $obj.val(faq.locale[anchor]);
            }
        });
        setTimeout(function() {
            $('#edit-faq-modal .preloader-modal').hide();
            $('#faq-form input').first().focus();
        }, 500);
    });
    return false;
});

/**
 * Botón de Editar FAQ en el modal
 */
$(document).on('click', '#edit-faq-button', function(){
    var productoId = $('#id').val();
    var faqId = $('#faqId').val();
    
    // Validación de errores
    $('#faq-form .text-danger').removeClass('text-danger');
    $('#faq-form .is-invalid').removeClass('is-invalid');
    
    var $nombre = $('#edit-faq-modal input[name="nombre"]');
    if ($.trim($nombre.val()) == '') {
        $nombre.parent().siblings('label').addClass('text-danger');
        $nombre.addClass('is-invalid');
    }
    var $orden = $('#edit-faq-modal input[name="orden"]');
    if ($.trim($orden.val()) == '') {
        $orden.parent().siblings('label').addClass('text-danger');
        $orden.addClass('is-invalid');
    }
    if ($('#faq-form .is-invalid').length) {
        return false;
    }
    
    var token = $(this).attr('token');
    var formData = new FormData($('#faq-form')[0]);
    formData.append('faq_id', faqId);
    formData.append('producto_id', productoId);
    
    if( $("#faq-form textarea.rich").length > 0 ){
        $("#faq-form textarea.rich").each(function() {
            formData.append($(this).attr('name'), CKEDITOR.instances[$(this).attr('id')].getData());
        });
    }
    
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        type: 'post',
        url: '/admin/productos_extendido/' + productoId + '/faqs/store',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            closeModal();
            notification(result.message);
            setTimeout(function() {
                window.location.href = '/admin/productos_extendido/' + productoId + '?show=faqs';
            }, 1000);
        },
        error: function(response) {
            var parsedJson = JSON.parse(response.responseText);
            closeModal();
            $('.error-container').addClass('active');
            $('.error-container').html(parsedJson.errors);
        }
    });
    return false;
});

/**
 * Botón de Borrar FAQ en la tabla, para cuando salga el modal de confirmación
 */
$(document).on('click', '#faqs-table .delete-button', function(){
    var $row = $(this).parents('tr');
    var faqId = $row.data('id');
    var faqNombre = String($row.data('nombre'));
    
    $('#faqId').val(faqId);
    $('#delete-faq-modal .modal-body p span').text(faqNombre.replace('\\"', '"'));
    return false;
});

/**
 * Botón de Borrar FAQ en el modal
 */
$(document).on('click', '#delete-faq-button', function(){
    var productoId = $('#id').val();
    var faqId = $('#faqId').val();
    
    var token = $(this).attr('token');
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        type: 'delete',
        url: '/admin/productos_extendido/' + productoId + '/faqs/' + faqId,
        dataType: 'json',
        data: {_method: 'delete'},
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            closeModal();
            notification(result.message);
            setTimeout(function() {
                window.location.href = '/admin/productos_extendido/' + productoId + '?show=faqs';
            }, 1000);
        },
        error: function(response) {
            var parsedJson = JSON.parse(response.responseText);
            closeModal();
            $('.error-container').addClass('active');
            $('.error-container').html(parsedJson.errors);
        }
    });
    return false;
});
