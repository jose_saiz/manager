
/**
 * Botón de Nueva Opinión
 */
$(document).on('click', '#new-opinion-button', function(){
    $('#edit-opinion-modal .modal-title').text('Nueva opinión');
    $('#edit-opinion-modal .preloader-modal').hide();
    
    $('#opinionId').val('');
    $('#edit-opinion-modal input[name="nombre"]').val('');
    $('#edit-opinion-modal input[name="activo"]').bootstrapSwitch('state', true);
    $('#edit-opinion-modal input[name="fecha"]').data('daterangepicker').setStartDate(new Date());
    $('#edit-opinion-modal input[name="experto"]').bootstrapSwitch('state', false);
    $('#edit-opinion-modal input[name="orden"]').val('');
    $('#edit-opinion-modal input[name^="locale"]').val('');
    $('#edit-opinion-modal textarea[name^="locale"]').each(function(i, obj) {
        var $obj = $(obj);
        if ($obj.hasClass('rich')) {
            CKEDITOR.instances[$obj.attr('id')].setData('');
        } else {
            $obj.val('');
        }
    });
    setTimeout(function() {
        $('#opinion-form input').first().focus();
    }, 550);
    return false;
});

/**
 * Botón de Editar Opinión en la tabla, para rellenar los datos del modal
 */
$(document).on('click', '#opiniones-table .edit-button', function(){
    $('#edit-opinion-modal .modal-title').text('Editar opinión');
    $('#edit-opinion-modal .preloader-modal').show();
    
    var $row = $(this).parents('tr');
    var productoId = $('#id').val();
    var opinionId = $row.data('id');
    $('#opinionId').val(opinionId);
    
    $.get('/admin/productos_extendido/' + productoId + '/opiniones/' + opinionId + '/json', function( opinion ) {
        $('#edit-opinion-modal input[name="nombre"]').val(opinion.nombre);
        $('#edit-opinion-modal input[name="activo"]').bootstrapSwitch('state', opinion.activo);
        $('#edit-opinion-modal input[name="fecha"]').data('daterangepicker').setStartDate(opinion.fecha);
        $('#edit-opinion-modal input[name="experto"]').bootstrapSwitch('state', opinion.experto);
        $('#edit-opinion-modal input[name="orden"]').val(opinion.orden);
        $('#edit-opinion-modal input[name^="locale"]').each(function(i, obj) {
            var $obj = $(obj);
            var anchor = $(obj).data('anchor');
            $obj.val(opinion.locale[anchor]);
        });
        $('#edit-opinion-modal textarea[name^="locale"]').each(function(i, obj) {
            var $obj = $(obj);
            var anchor = $(obj).data('anchor');
            if ($obj.hasClass('rich')) {
                CKEDITOR.instances[$obj.attr('id')].setData(opinion.locale[anchor]);
            } else {
                $obj.val(opinion.locale[anchor]);
            }
        });
        setTimeout(function() {
            $('#edit-opinion-modal .preloader-modal').hide();
            $('#opinion-form input').first().focus();
        }, 500);
    });
    return false;
});

/**
 * Botón de Editar Opinión en el modal
 */
$(document).on('click', '#edit-opinion-button', function(){
    var productoId = $('#id').val();
    var opinionId = $('#opinionId').val();
    
    // Validación de errores
    $('#opinion-form .text-danger').removeClass('text-danger');
    $('#opinion-form .is-invalid').removeClass('is-invalid');
    
    var $nombre = $('#edit-opinion-modal input[name="nombre"]');
    if ($.trim($nombre.val()) == '') {
        $nombre.parent().siblings('label').addClass('text-danger');
        $nombre.addClass('is-invalid');
    }
    var $orden = $('#edit-opinion-modal input[name="orden"]');
    if ($.trim($orden.val()) == '') {
        $orden.parent().siblings('label').addClass('text-danger');
        $orden.addClass('is-invalid');
    }
    if ($('#opinion-form .is-invalid').length) {
        return false;
    }
    
    var token = $(this).attr('token');
    var formData = new FormData($('#opinion-form')[0]);
    formData.append('opinion_id', opinionId);
    formData.append('producto_id', productoId);
    
    if( $("#opinion-form textarea.rich").length > 0 ){
        $("#opinion-form textarea.rich").each(function() {
            formData.append($(this).attr('name'), CKEDITOR.instances[$(this).attr('id')].getData());
        });
    }
    
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        type: 'post',
        url: '/admin/productos_extendido/' + productoId + '/opiniones/store',
        dataType: 'json',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            closeModal();
            notification(result.message);
            setTimeout(function() {
                window.location.href = '/admin/productos_extendido/' + productoId + '?show=opiniones';
            }, 1000);
        },
        error: function(response) {
            var parsedJson = JSON.parse(response.responseText);
            closeModal();
            $('.error-container').addClass('active');
            $('.error-container').html(parsedJson.errors);
        }
    });
    return false;
});

/**
 * Botón de Borrar Opinión en la tabla, para cuando salga el modal de confirmación
 */
$(document).on('click', '#opiniones-table .delete-button', function(){
    var $row = $(this).parents('tr');
    var opinionId = $row.data('id');
    var opinionNombre = String($row.data('nombre'));
    
    $('#opinionId').val(opinionId);
    $('#delete-opinion-modal .modal-body p span').text(opinionNombre.replace('\\"', '"'));
    return false;
});

/**
 * Botón de Borrar Opinión en el modal
 */
$(document).on('click', '#delete-opinion-button', function(){
    var productoId = $('#id').val();
    var opinionId = $('#opinionId').val();
    
    var token = $(this).attr('token');
    $.ajax({
        headers: {'X-CSRF-TOKEN': token},
        type: 'delete',
        url: '/admin/productos_extendido/' + productoId + '/opiniones/' + opinionId,
        dataType: 'json',
        data: {_method: 'delete'},
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            closeModal();
            notification(result.message);
            setTimeout(function() {
                window.location.href = '/admin/productos_extendido/' + productoId + '?show=opiniones';
            }, 1000);
        },
        error: function(response) {
            var parsedJson = JSON.parse(response.responseText);
            closeModal();
            $('.error-container').addClass('active');
            $('.error-container').html(parsedJson.errors);
        }
    });
    return false;
});
