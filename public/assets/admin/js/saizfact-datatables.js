/* 
|--------------------------------------------------------------------------
| DataTables
|--------------------------------------------------------------------------
| SaizFact, última actualización de esta documentación realizada el 21/12/2017
|
| Mediante jquery.dataTables.js renderiza las tablas para darles funcionalidades extra. Para usarlo
| debemos etiquetar la tabla con la id #main-table y añadir la etiqueta route con la ruta a la acción indexJson
| del controlador (por ejemplo vademecum_json). Se recorre los th de la tabla en busca de los elementos
| data que sirve para construir la presentación de las columnas. Las opciones disponibles para usar son:
|
| data-data [string]: se especifica el nombre del campo de la tabla que debe ser recogido de la base de datos.
|
| data-name [string]: el nombre que recibirá la columna creada para uso interno de datatables.
|
| data-searchable [boolean]: si la columna debe aparecer o no en los resultados del buscador general.
|
| data-orderable [boolean]: si la columna debe tener las flechas de ordenación junto al título de la columna.
|
| data-visible [boolean]: si la columna debe ser visible. 
|
| data-className [string]: añadir una clase a todos los elementos de la columna. 
|
| data-defaultContent [string]: añadir elementos a la columna que no estrictamente datos, por ejemplo, podemos
| escribir código y añadir un botón de editar. 
|
| **Boton 'Editar': Evento para los enlaces del botón editar ubicado en las tablas, llama a la 
|   ruta 'show' definida en /routes/web y recibe secciones renderizas que introduce dentro la vista.
|   Modifica la url para que aparezca la id del elemento que se edita en ella.   
|
| **Buscador general: Evento para realizar una búsqueda general sobre toda la tabla. Buscador generado en
|   la vista /admin/partials/filters
|
| **Filtro secundario: Evento para realizar una búsqueda filtrada sobre toda la tabla. Filtro generado en
|   la vista /admin/partials/filters bajo la denominación $subfilter.
|
| **Filtro primario: Evento para realizar una búsqueda filtrada sobre toda la tabla. Filtro generado en
|   la vista /admin/partials/filters bajo la denominación $filter.
|
| **Filtro de novedades: Evento para realizar una búsqueda filtrada por novedades. Filtro generado en
|   la vista /admin/partials/filters bajo la denominación $new.
|
| **Botón de ordenación:  Evento para editar el orden en el que aparecerán los elementos en el front. 
|   Opción generada en la vista /admin/partials/filters bajo la denominación $order_route. Al activar
|   el botón se desactiva la paginación de resultados y se muestran todos los resultados datatable.page.len(-1)
|   y se activa el drag&drop .rowReorder.enable(). Al mover un elemento se genera un array con la id de 
|   cada fila, la posición en el index del array será la nueva posición. Dicho array se envía a la acción
|   reorderTable del controlador. Si el filtro de novedad está activado se ordenará exclusivamente las 
|   novedades.   
|
*/

var datatable;
var datatable2;
var datatable3;

$(document).ready(function(){
    
    
        
    //Export Plannig table
    $(".dataExport").click(function() {
        debugger;
        /*
        $("#data-table-planning").table2excel({
          //exclude: ".noExl", // exclude CSS class noExl
          name: "Worksheet Name",
          filename: "SomeFile",//do not include extension
          fileext: ".xlsx",// file extension
          //preserveColors: true
        }); 
        */
        
        var exportType = $(this).data('type');      
        $('#data-table-planning').tableExport({
            type : exportType,          
            escape : 'false',
            ignoreColumn: []
        });
        
        
    });  

    //Botón de editar

    $(document).on('click', '.edit-button', function(){
        debugger;
        if (datatable === undefined) {
            return;
        }

        var route =  $(this).attr('route');
        var id = datatable.row($(this).parents('tr')).data().id;
        var url = route + '/' + id;
        var changetab = $(this).data('changetab');
        var template = $('#two-columns').length ? 'two-columns' : 'one-column';

        $.get( url , function(result){

            switch(template) {

                case 'one-column':
                    $('.master-layout').html(result.layout);
                    window.history.pushState('', '', url);
                    renderTable();
                    renderTable2();
                    renderTable3();
                    renderElements(result);
                    
                    $('#roll_replacement_datetime').datetimepicker();
                    $('#roll_end_datetime').datetimepicker();
                    break;

                case 'two-columns':
                    $('#two-columns-form').html(result.form);
                    window.history.pushState('', '', url);
                    if (changetab != undefined && changetab != '') {
                        $('.nav-tabs li a[href="#' + changetab + '"').click();
                    }
                    renderElements(result);
                    break;
            }
        });
    });

    //Buscador general

    var sDelay = 400;
    var sMinLength = 1;
    var oTimerId = null;
    var sPreviousSearch = null;

    $('.buscador').on( 'keydown', function (event) {
        if (datatable === undefined) {
            return;
        }

        if (buttonEnter(event)) {
            event.preventDefault();
            return false;
        }
    });

    $('.buscador').on( 'keyup', function (event) {
        if (datatable === undefined) {
            return;
        }

        var searchStr = $('.buscador').val();

        if (!buttonEnter(event)) {
            if ((sPreviousSearch === null || sPreviousSearch != searchStr) && (searchStr.length == 0 || searchStr.length >= sMinLength)) {
                window.clearTimeout(oTimerId);
                sPreviousSearch = searchStr; 
                oTimerId = window.setTimeout(function() {
                    datatable.search(searchStr).draw();
                }, sDelay);
            }
        }
    });

    //Filtro secundario

    $('#subfilter').on( 'change', function () {
        if (datatable === undefined) {
            return;
        }

        var index = datatable.column('#subfilter-column').index()

        var searchString = $.fn.dataTable.util.escapeRegex(
            $(this).find('option:selected').attr('name')
        );

        if (searchString == "todas") {
            datatable.search( '' ).columns().search( '' ).draw();
        } else {
            datatable.search( '' ).columns().search( '' ).draw();
            datatable
                .column(index)
                .search(searchString)
                .draw(); 
        }
    });

    //Filtro primario

    $('#filter').on( 'change', function () {
        if (datatable === undefined) {
            return;
        }

        var index = datatable.column('#filter-column').index()

        var searchString = $.fn.dataTable.util.escapeRegex(
            $(this).find('option:selected').attr('name')
        );

        datatable
            .column(index)
            .search(searchString ? searchString : '', true, false )
            .draw();

        if (!$('.order-button').hasClass('visible')){
            $('.order-button').addClass('visible');
        }
    });

    //Filtro de novedades

    $('.new-button').on('click', function () {
        if (datatable === undefined) {
            return;
        }

        if ($(this).hasClass('active')) {
            $('.new-button').removeClass('active');
            $('.order-button').removeClass('visible');
            $('.order-button').removeClass('active');
            $(".select2").select2("val", "");

            var index = datatable.column('id:name').index();

            datatable
                .search( '' )
                .columns()
                .search( '' )
                .draw();

            datatable.order([index, 'asc']);
        } else {
            $('.new-button').addClass('active');
            $('.order-button').removeClass('active');
            $('.order-button').addClass('visible');
            $(".select2").select2("val", "");
        
            datatable.search( '' ).columns().search( '' );

            datatable
                .column('novedad:name')
                .search(1 ? 1 : 1, true, false )
                .draw();
        }
    });

    //Filtro de vacíos

    $('.empty-button').on('click', function () {
        if (datatable === undefined) {
            return;
        }

        if ($(this).hasClass('active')) {
            $('.empty-button').removeClass('active');
            $(".select2").select2("val", "");

            var index = datatable.column('id:id').index();

            datatable
                .search( '' )
                .columns()
                .search( '' )
                .draw();

            datatable.order([index, 'desc']);
        } else {
            $('.empty-button').addClass('active');
            $(".select2").select2("val", "");
        
            datatable.search( '' ).columns().search( '' );

            datatable
                .columns()
                .search(null ? null : null, true, false )
                .draw();
        }
    });

    //Botón de ordenación

    $('.order-button').on('click', function () {
        if (datatable === undefined) {
            return;
        }

        if ($(this).hasClass('active')) {
            var index = datatable.column('id:name').index();

            datatable.page.len( 10 ).draw();
            datatable.rowReorder.disable();
            $('.order-button').removeClass('active');
            datatable.order([index, 'desc']).draw();

        } else {
            if($('.new-button').hasClass('active')){
                var index = datatable.column('orden_novedad:name').index();
            }else{
                var index = datatable.column('orden:name').index();
            }

            datatable.page.len( -1 ).draw();
            datatable.rowReorder.enable();
            datatable.order([index, 'asc']).draw();
            $('.order-button').addClass('active');
        }
    });

    //DateRangePicker

    $('#daterange').on('apply.daterangepicker', function (ev, picker) { 
        //alert('hola: '+picker.startDate.format('DD/MM/YYYY'));
        //alert('hola: '+picker.endDate.format('DD/MM/YYYY'));
        var startdate = picker.startDate.format('DD/MM/YYYY');
        var enddate = picker.endDate.format('DD/MM/YYYY');
        $.fn.dataTableExt.afnFiltering.push(
            function (oSettings, aData, iDataIndex) {
               alert("Startdate: "+startdate);
               if (startdate != undefined) {
                  var coldate = aData[5].split("/");
                  var d = new Date(coldate[2], coldate[1] - 1, coldate[1]);
                  var date = moment(d.toISOString());
                  date = date.format('DD/MM/YYYY');
                  dateMin = startdate.replace(/-/g, "");
                  dateMax = enddate.replace(/-/g, "");
                  date = date.replace(/-/g, "");
                  if (dateMin == "" && date <= dateMax) {
                     return true;
                  } else if (dateMin == "" && date <= dateMax) {
                     return true;
                  } else if (dateMin <= date && "" == dateMax) {
                     return true;
                  } else if (dateMin <= date && date <= dateMax) {
                     return true;
                  }
                  return false;
               }
            });
        datatable.draw()
    });
});

function renderTable(){
    if( $("#main-table").length == 0 ){  
        return;
    }

    var table = $('#main-table');
    var url = table.attr('route');
    var columns = [];

    table.find('thead th').each(function(idx, el) {
        
        var col = {};

        if ($(el).data('data') !== undefined) {
            col.data = $(el).data('data');
        }

        if ($(el).data('name') !== undefined) {
            col.name = $(el).data('name');
        }

        if ($(el).data('searchable') !== undefined) {
            col.searchable = $(el).data('searchable');
        }

        if ($(el).data('orderable') !== undefined) {
            col.orderable = $(el).data('orderable');
        }

        if ($(el).data('visible') !== undefined) {
            col.visible = $(el).data('visible');
        }

        if ($(el).data('className') !== undefined) {
            col.className = $(el).data('className');
        }

        if ($(el).attr('data-defaultContent') !== undefined) {
            col.defaultContent = $(el).attr('data-defaultContent');
        }
        
        columns.push(col);
    });

    datatable = table.DataTable({
        pagingType: 'full_numbers',
        processing: true,
        serverSide: true,
        language: {
            //url: "/js/jquery.dataTables.es_ES.json"
            url: "/js/jquery.dataTables.en_GB.json"
        },
        createdRow: function (row, data, index) {
            if (data['activo'] == "0" || data['activo'] == "No") {
                $(row).addClass('desactivado');
            }

            if ($('#main-table').hasClass('highlight') && data['keywords'] == null) {
                $(row).addClass('highlight');
            }
        },
        rowReorder: {
            enable: false,
            selector: 'tr',
            update: false,
            dataSrc: 'orden',
        },
        ajax: {
            url: url,
            data: function(d) {
                // console.log('d', d);
                d.fecha_inicio = $('input[name=daterangepicker_start]').val();
                d.fecha_fin = $('input[name=daterangepicker_end]').val();
            },
            type:"GET"
        },  
        columns: columns,
        order: [[0, 'desc']],
        sDom: '<"top"i>rt<"bottom"lp><"clear">',
        "initComplete": function(settings, json) {
        	//console.log(json);
        
            //for (i = 0; i < json.data)
        	
        },
        "lengthMenu":  [ [10, 25, 50, 100, -1], [10, 25, 50,100, "All"] ]
    });
    datatable.on( 'length.dt', function ( e, settings, len ) {
        if  ($('.preloader').css('display') == 'none'){
                $('.preloader').css('display','block');
                $('.preloader').css('opacity','0.6');
        }
        
    });
    datatable.on('draw.dt', function (e, settings, len) {
        if ($('.preloader').css('display') != 'none'){
                $('.preloader').css('display','none');
                $('.preloader').css('opacity','1');
        }
    });
    datatable.on('row-reorder', function ( e, diff, edit)  {

        var array = [];
        var url = $('.order-button').attr('route'); 
            
        if ($('.new-button').hasClass('active')){
            var news = 'active';
        }else{
            var news = ''; 
        }

        $.each(diff,  function( key, value) {
            array.push(value.node.cells[0].textContent);
        });
    
        $.post(url, {
            orden: array,
            orden_novedad: news,
            _token: $('meta[name="csrf-token"]').attr('content')
        }, function (result) {

        });
    });
}
function renderTable2(){
    if( $("#main-table-2").length == 0 ){  
        return;
    }

    var table = $('#main-table-2');
    var url = table.attr('route');
    var columns = [];

    table.find('thead th').each(function(idx, el) {
        
        var col = {};

        if ($(el).data('data') !== undefined) {
            col.data = $(el).data('data');
        }

        if ($(el).data('name') !== undefined) {
            col.name = $(el).data('name');
        }

        if ($(el).data('searchable') !== undefined) {
            col.searchable = $(el).data('searchable');
        }

        if ($(el).data('orderable') !== undefined) {
            col.orderable = $(el).data('orderable');
        }

        if ($(el).data('visible') !== undefined) {
            col.visible = $(el).data('visible');
        }

        if ($(el).data('className') !== undefined) {
            col.className = $(el).data('className');
        }

        if ($(el).attr('data-defaultContent') !== undefined) {
            col.defaultContent = $(el).attr('data-defaultContent');
        }
        
        columns.push(col);
    });

    datatable2 = table.DataTable({
        pagingType: 'full_numbers',
        processing: true,
        serverSide: true,
        language: {
            //url: "/js/jquery.dataTables.es_ES.json"
            url: "/js/jquery.dataTables.en_GB.json"
        },
        createdRow: function (row, data, index) {
            if (data['activo'] == "0" || data['activo'] == "No") {
                $(row).addClass('desactivado');
            }

            if ($('#main-table').hasClass('highlight') && data['keywords'] == null) {
                $(row).addClass('highlight');
            }
        },
        rowReorder: {
            enable: false,
            selector: 'tr',
            update: false,
            dataSrc: 'orden',
        },
        ajax: {
            url: url,
            data: function(d) {
                // console.log('d', d);
                d.fecha_inicio = $('input[name=daterangepicker_start]').val();
                d.fecha_fin = $('input[name=daterangepicker_end]').val();
            },
            type:"GET"
        },  
        columns: columns,
        order: [[0, 'desc']],
        sDom: '<"top"i>rt<"bottom"lp><"clear">',
        "initComplete": function(settings, json) {
            //console.log(json);
        
            //for (i = 0; i < json.data)
            
        },
        "lengthMenu": [ 10, 25, 50, 100 ]
    });
    datatable2.on( 'length.dt', function ( e, settings, len ) {
        if  ($('.preloader').css('display') == 'none'){
                $('.preloader').css('display','block');
                $('.preloader').css('opacity','0.6');
        }
        
    });
    datatable2.on('draw.dt', function (e, settings, len) {
        if ($('.preloader').css('display') != 'none'){
                $('.preloader').css('display','none');
                $('.preloader').css('opacity','1');
        }
    });
    datatable2.on('row-reorder', function ( e, diff, edit)  {

        var array = [];
        var url = $('.order-button').attr('route'); 
            
        if ($('.new-button').hasClass('active')){
            var news = 'active';
        }else{
            var news = ''; 
        }

        $.each(diff,  function( key, value) {
            array.push(value.node.cells[0].textContent);
        });
    
        $.post(url, {
            orden: array,
            orden_novedad: news,
            _token: $('meta[name="csrf-token"]').attr('content')
        }, function (result) {

        });
    });
}
function renderTable3(){
    if( $("#main-table-3").length == 0 ){  
        return;
    }

    var table = $('#main-table-3');
    var url = table.attr('route');
    var columns = [];

    table.find('thead th').each(function(idx, el) {
        
        var col = {};

        if ($(el).data('data') !== undefined) {
            col.data = $(el).data('data');
        }

        if ($(el).data('name') !== undefined) {
            col.name = $(el).data('name');
        }

        if ($(el).data('searchable') !== undefined) {
            col.searchable = $(el).data('searchable');
        }

        if ($(el).data('orderable') !== undefined) {
            col.orderable = $(el).data('orderable');
        }

        if ($(el).data('visible') !== undefined) {
            col.visible = $(el).data('visible');
        }

        if ($(el).data('className') !== undefined) {
            col.className = $(el).data('className');
        }

        if ($(el).attr('data-defaultContent') !== undefined) {
            col.defaultContent = $(el).attr('data-defaultContent');
        }
        
        columns.push(col);
    });

    datatable3 = table.DataTable({
        pagingType: 'full_numbers',
        processing: true,
        serverSide: true,
        language: {
            //url: "/js/jquery.dataTables.es_ES.json"
            url: "/js/jquery.dataTables.en_GB.json"
        },
        createdRow: function (row, data, index) {
            if (data['activo'] == "0" || data['activo'] == "No") {
                $(row).addClass('desactivado');
            }

            if ($('#main-table').hasClass('highlight') && data['keywords'] == null) {
                $(row).addClass('highlight');
            }
        },
        rowReorder: {
            enable: false,
            selector: 'tr',
            update: false,
            dataSrc: 'orden',
        },
        ajax: {
            url: url,
            data: function(d) {
                // console.log('d', d);
                d.fecha_inicio = $('input[name=daterangepicker_start]').val();
                d.fecha_fin = $('input[name=daterangepicker_end]').val();
            },
            type:"GET"
        },  
        columns: columns,
        order: [[0, 'desc']],
        sDom: '<"top"i>rt<"bottom"lp><"clear">',
        "initComplete": function(settings, json) {
            //console.log(json);
        
            //for (i = 0; i < json.data)
            
        },
        "lengthMenu": [ 10, 25, 50, 100 ]
    });
    datatable.on( 'length.dt', function ( e, settings, len ) {
        if  ($('.preloader').css('display') == 'none'){
                $('.preloader').css('display','block');
                $('.preloader').css('opacity','0.6');
        }
        
    });
    datatable3.on('draw.dt', function (e, settings, len) {
        if ($('.preloader').css('display') != 'none'){
                $('.preloader').css('display','none');
                $('.preloader').css('opacity','1');
        }
    });
    datatable3.on('row-reorder', function ( e, diff, edit)  {

        var array = [];
        var url = $('.order-button').attr('route'); 
            
        if ($('.new-button').hasClass('active')){
            var news = 'active';
        }else{
            var news = ''; 
        }

        $.each(diff,  function( key, value) {
            array.push(value.node.cells[0].textContent);
        });
    
        $.post(url, {
            orden: array,
            orden_novedad: news,
            _token: $('meta[name="csrf-token"]').attr('content')
        }, function (result) {

        });
    });
}
function datatableDraw() {
    debugger;
    if (datatable === undefined) {
        return;
    }
    
    datatable.draw();
    
    if (datatable2 === undefined) {
        return;
    }
    
    datatable2.draw();
    
    if (datatable3 === undefined) {
        return;
    }
    
    datatable3.draw();
}
