/*
|--------------------------------------------------------------------------
| Reglas de validaciones de formularios mediante jquery.validate.js
|--------------------------------------------------------------------------
| SaizFact, última actualización de esta documentación realizada el 16/11/2017
|
| Función utilizada por el evento " Boton 'Guardar' " definido en saizfact.js
|
| El parametro form es la etiqueta 'id' que le hemos dado al formulario (ej: <form id='faqs-form'></form>) 
| y que será evaluada en cada case del switch. Por tanto, cada case será un formulario con sus propias reglas 
| de validación. Es importante incluir el # en el case a parte de del nombre de la etiqueta id.
|
| Se deben repetir las mismas opciones en cada 'case', lo único que se debe modificar es el apartado 'rules' y 
| el apartado 'messages'. Es recomendable que estas reglas sean iguales a las reglas de validación que se 
| realizarán en el servidor en /app/HTTP/Requests, ya que si las reglas del front son esquivadas, las del 
| servidor actuarán: 
|
| ignore: [], permite utilizar la validación con custom-select.js
| onkeyup: false, impide que se compruebe la validación al teclear.
| onfocusout: false, impide que se compruebe la validación al cambiar de foco a otro input.
| wrapper: Define si a cada mensaje de error de validación lo queremos dentro de una etiqueta html.
| errorClass: Define si a cada input con error de validación le queremos dar una clase. 
|
| ShowErrors: Define la respuesta al ocurrir un error de validación. Los errores dentro de admin se 
| mostrarán a través del componente ubicado en /resources/views/admin/partials/errors, se añade la clase 
| 'active'que se encuentran definida en /resources/assets/less/saizfact
|
*/

function formValidation(form) {
    
    'use strict';

    /*Validador de input personalizado*/

    jQuery.validator.addMethod("lettersonly", function(value, element) {
        return this.optional(element) || /^[a-z0-9áéíóúàèìòùäëïöüñ\s]+$/i.test(value);
    }, "Sólo se aceptan letra"); 

    /*Validaciones de los formularios*/

    switch(form) {

        case "#faqs-form":

            $("#faqs-form").validate({
            
                ignore: [],
                onkeyup: false,
                onfocusout: false,
                wrapper: "li",
                errorClass: "is-invalid",
        
                rules: {
                    nombre: {
                        required: true,
                    },
                    categoria_id: {
                        required: true
                    }
                },
        
                messages: {
                    nombre: {
                        required: "El nombre es obligatorio"
                    },
                    categoria_id: {
                        required: "Debe seleccionar una categoría"
                    }
                },

                showErrors: function(errorMap, errorList) {
                    
                    var errorString = '';

                    $.each(errorMap, function(key,value) {
                        errorString += '<li>' + value + '</li>';                        
                    });

                    if(errorString != ''){
                        $('.error-container').addClass('active');
                        $('.error-container').html(errorString);
                    }
                }
            });

            break;

        case "#faq-categoria-form":
        
            $("#faq-categoria-form").validate({
            
                ignore: [],
                onkeyup: false,
                onfocusout: false,
                wrapper: "li",
                errorClass: "is-invalid",
        
                rules: {
                    nombre: {
                        required: true,
                    }
                },
        
                messages: {
                    nombre: {
                        required: "El nombre es obligatorio"
                    }
                },

                showErrors: function(errorMap, errorList) {
                    
                    var errorString = '';

                    $.each(errorMap, function(key,value) {
                        errorString += '<li>' + value + '</li>';                        
                    });

                    if(errorString != ''){
                        $('.error-container').addClass('active');
                        $('.error-container').html(errorString);
                    }
                }
            });

            break;
        
        case "#users-form":
        
            $("#users-form").validate({
            
                ignore: [],
                onkeyup: false,
                onfocusout: false,
                wrapper: "li",
                errorClass: "is-invalid",
    
                rules: {
                    name: {
                        required: true,
                        minlength: 3,
                        maxlength: 64,
                        lettersonly: true,
                    },

                    email: {
                        required: true,
                        email: true,
                        maxlength: 255,
                    },

                    role_id: {
                        required: true,
                    }
                },
                
                messages: {
                    name: {
                        required: "El nombre es obligatorio",
                        minlength: 'El mínimo de caracteres permitidos para el nombre son 3',
                        maxlength: 'El máximo de caracteres permitidos para el nombre son 64',
                        //lettersonly: 'Sólo se aceptan letras en el nombre',
                    },
                    email: {
                        required: 'El email es obligatorio',
                        email: 'Debe introducir un email con un formato valido',
                        //maxlength: 'El máximo de caracteres permitidos para el email son 255',
                    },
                },

                showErrors: function(errorMap, errorList) {
                    
                    var errorString = '';

                    $.each(errorMap, function(key,value) {
                        errorString += '<li>' + value + '</li>';                        
                    });

                    if(errorString != ''){
                        $('.error-container').addClass('active');
                        $('.error-container').html(errorString);
                    }
                }
            });

            break;

        case "#roles-form":
        
            $("#roles-form").validate({
            
                ignore: [],
                onkeyup: false,
                onfocusout: false,
                wrapper: "li",
                errorClass: "is-invalid",
    
                rules: {
                    name: {
                        required: true,
                        minlength: 3,
                        maxlength: 64,
                        lettersonly: true,

                    },
                    slug: {
                        required: true,
                        maxlength: 255,
                    }
                },
                
                messages: {
                    name: {
                        required: "El nombre es obligatorio",
                        minlength: 'El mínimo de caracteres permitidos para el nombre son 3',
                        maxlength: 'El máximo de caracteres permitidos para el nombre son 64',
                        lettersonly: 'Sólo se aceptan letras en el nombre',
                    },
                    slug: {
                        required: 'El slug es obligatorio',
                        maxlength: 'El máximo de caracteres permitidos para el slug son 255',
                    }
                },

                showErrors: function(errorMap, errorList) {
                    
                    var errorString = '';

                    $.each(errorMap, function(key,value) {
                        errorString += '<li>' + value + '</li>';                        
                    });

                    if(errorString != ''){
                        $('.error-container').addClass('active');
                        $('.error-container').html(errorString);
                    }
                }
            });

            break;
        
        case "#permissions-form":
        
            $("#permissions-form").validate({
            
                ignore: [],
                onkeyup: false,
                onfocusout: false,
                wrapper: "li",
                errorClass: "is-invalid",
  
                rules: {
                    name: {
                        required: true,
                        minlength: 3,
                        maxlength: 64,
                        lettersonly: true,

                    },
                    slug: {
                        required: true,
                        maxlength: 255,
                    }
                },
                
                messages: {
                    name: {
                        required: "El nombre es obligatorio",
                        minlength: 'El mínimo de caracteres permitidos para el nombre son 3',
                        maxlength: 'El máximo de caracteres permitidos para el nombre son 64',
                        lettersonly: 'Sólo se aceptan letras en el nombre',
                    },
                    slug: {
                        required: 'El slug es obligatorio',
                        maxlength: 'El máximo de caracteres permitidos para el slug son 255',
                    }
                },

                showErrors: function(errorMap, errorList) {
                    
                    var errorString = '';

                    $.each(errorMap, function(key,value) {
                        errorString += '<li>' + value + '</li>';                        
                    });

                    if(errorString != ''){
                        $('.error-container').addClass('active');
                        $('.error-container').html(errorString);
                    }
                }
            });

            break;

        case "#paginas-form":
        
            $("#paginas-form").validate({
            
                ignore: [],
                onkeyup: false,
                onfocusout: false,
                wrapper: "li",
                errorClass: "is-invalid",
        
                rules: {
                    titulo: {
                        required: true,
                    },
                    titulo_pagina: {
                        required: true
                    },
                    slug: {
                        required: true,
                    },
                    editor: {
                        required: true
                    }
                },
        
                messages: {
                    titulo: {
                        required: "El título es obligatorio"
                    },
                    titulo_pagina: {
                        required: "El título para la página es obligatorio"
                    },
                    slug: {
                        required: 'El slug es obligatorio',
                    },
                    editor: {
                        required: 'Debe introducir alguna información en el editor de la página',
                    }
                },

                showErrors: function(errorMap, errorList) {

                    var errorString = '';

                    $.each(errorMap, function(key,value) {
                        errorString += '<li>' + value + '</li>';                        
                    });

                    if(errorString != ''){
                        $('.error-container').addClass('active');
                        $('.error-container').html(errorString);
                    }
                }
            });

            break;
            
        case "#descargas-form":

            $("#descargas-form").validate({
            
                ignore: [],
                onkeyup: false,
                onfocusout: false,
                wrapper: "li",
                errorClass: "is-invalid",
        
                rules: {
                    nombre: {
                        required: true,
                    },
                    categoria_id: {
                        required: true
                    }
                },
        
                messages: {
                    nombre: {
                        required: "El nombre es obligatorio"
                    },
                    categoria_id: {
                        required: "Debe seleccionar una categoría"
                    }
                },

                showErrors: function(errorMap, errorList) {
                    
                    var errorString = '';

                    $.each(errorMap, function(key,value) {
                        errorString += '<li>' + value + '</li>';                        
                    });

                    if(errorString != ''){
                        $('.error-container').addClass('active');
                        $('.error-container').html(errorString);
                    }
                }
            });

            break;
        case "#descargas-categorias-form":
            
            $("#descargas-categorias-form").validate({
            
                ignore: [],
                onkeyup: false,
                onfocusout: false,
                wrapper: "li",
                errorClass: "is-invalid",
        
                rules: {
                    nombre: {
                        required: true,
                    }
                },
        
                messages: {
                    nombre: {
                        required: "El nombre es obligatorio"
                    }
                },

                showErrors: function(errorMap, errorList) {
                    
                    var errorString = '';

                    $.each(errorMap, function(key,value) {
                        errorString += '<li>' + value + '</li>';                        
                    });

                    if(errorString != ''){
                        $('.error-container').addClass('active');
                        $('.error-container').html(errorString);
                    }
                }
            });

            break;

            case "#vademecum-form":
            
                $("#vademecum-form").validate({
                
                    ignore: [],
                    onkeyup: false,
                    onfocusout: false,
                    wrapper: "li",
                    errorClass: "is-invalid",
            
                    rules: {
                        nombre: {
                            required: true,
                        },
                        /* categoria_id: {
                            required: true
                        },
                        subcategoria_id: {
                            required: true
                        } */
                    },
            
                    messages: {
                        nombre: {
                            required: "El nombre es obligatorio"
                        },
                        /* categoria_id: {
                            required: "Debe seleccionar una categoría"
                        },
                        subcategoria_id: {
                            required: "Debe seleccionar una subcategoría"
                        } */
                    },

                    showErrors: function(errorMap, errorList) {

                        var errorString = '';

                        $.each(errorMap, function(key,value) {
                            errorString += '<li>' + value + '</li>';
                        });
    
                        if(errorString != ''){
                            $('.error-container').addClass('active');
                            $('.error-container').html(errorString);
                        }
                    }
                });
    
                break;
            
            case "#vademecum-categorias-form":
            
                $("#vademecum-categorias-form").validate({
                
                    ignore: [],
                    onkeyup: false,
                    onfocusout: false,
                    wrapper: "li",
                    errorClass: "is-invalid",
            
                    rules: {
                        nombre: {
                            required: true,
                        }
                    },
            
                    messages: {
                        nombre: {
                            required: "El nombre es obligatorio"
                        }
                    },
    
                    showErrors: function(errorMap, errorList) {
                        
                        var errorString = '';
    
                        $.each(errorMap, function(key,value) {
                            errorString += '<li>' + value + '</li>';                        
                        });
    
                        if(errorString != ''){
                            $('.error-container').addClass('active');
                            $('.error-container').html(errorString);
                        }
                    }
                });
    
                break;

            case "#vademecum-agrupaciones-form":
            
                $("#vademecum-agrupaciones-form").validate({
                
                    ignore: [],
                    onkeyup: false,
                    onfocusout: false,
                    wrapper: "li",
                    errorClass: "is-invalid",
            
                    rules: {
                        nombre: {
                            required: true,
                        },
                        categoria_id: {
                            required: true
                        }
                    },
            
                    messages: {
                        nombre: {
                            required: "El nombre es obligatorio"
                        },
                        categoria_id: {
                            required: "Debe seleccionar una categoría"
                        }
                    },
    
                    showErrors: function(errorMap, errorList) {
                        
                        var errorString = '';
    
                        $.each(errorMap, function(key,value) {
                            errorString += '<li>' + value + '</li>';                        
                        });
    
                        if(errorString != ''){
                            $('.error-container').addClass('active');
                            $('.error-container').html(errorString);
                        }
                    }
                });
    
                break;

            case "#vademecum-subcategorias-form":
            
                $("#vademecum-subcategorias-form").validate({
                
                    ignore: [],
                    onkeyup: false,
                    onfocusout: false,
                    wrapper: "li",
                    errorClass: "is-invalid",
            
                    rules: {
                        nombre: {
                            required: true,
                        },
                        agrupacion_id: {
                            required: true
                        }
                    },
            
                    messages: {
                        nombre: {
                            required: "El nombre es obligatorio"
                        },
                        categoria_id: {
                            agrupacion_id: "Debe seleccionar una agrupación"
                        }
                    },
    
                    showErrors: function(errorMap, errorList) {
                        
                        var errorString = '';
    
                        $.each(errorMap, function(key,value) {
                            errorString += '<li>' + value + '</li>';                        
                        });
    
                        if(errorString != ''){
                            $('.error-container').addClass('active');
                            $('.error-container').html(errorString);
                        }
                    }
                });
    
                break;

            case "#producto-puntos-form":
                
                $("#producto-puntos-form").validate({
                
                    ignore: [],
                    onkeyup: false,
                    onfocusout: false,
                    wrapper: "li",
                    errorClass: "is-invalid",
            
                    rules: {
                        codigo: {
                            required: true,
                        },
                        puntos: {
                            required: true,
                            number:true
                        },
                    },
            
                    messages: {
                        codigo: {
                            required: "El código es obligatorio"
                        },
                        puntos: {
                            required: "Los puntos son obligatorios",
                            number: "Los puntos deben ser números"
                        }
                    },
    
                    showErrors: function(errorMap, errorList) {
                        
                        var errorString = '';
    
                        $.each(errorMap, function(key,value) {
                            errorString += '<li>' + value + '</li>';                        
                        });
    
                        if(errorString != ''){
                            $('.error-container').addClass('active');
                            $('.error-container').html(errorString);
                        }
                    }
                });
    
                break;

            case "#banner-item-form":

                $("#banner-item-form").validate({
                
                    ignore: [],
                    onkeyup: false,
                    onfocusout: false,
            
                    rules: {
                        input_file: {
                            required: true,
                            extension: "png|jpg|jpeg|gif"
                        }
                    },
            
                    messages: {
                        input_file: {
                            required: "La imagen es obligatoria",
                            extension: "Formato de archivo no válido (sólo jpg, png o gif)"
                        }
                    },

                    showErrors: function(errorMap, errorList) {
                        
                        var errorString = '';

                        $.each(errorMap, function(key,value) {
                            errorString += '<li>' + value + '</li>';                        
                        });

                        if(errorString != ''){
                            $('.error-container-alert').addClass('active');
                            $('.error-container-alert').html(errorString);
                        }
                    }
                });

            break;
            
            case "#letreros-form":

                $("#letreros-form").validate({
                
                    ignore: [],
                    onkeyup: false,
                    onfocusout: false,
                    wrapper: "li",
                    errorClass: "is-invalid",
            
                    rules: {
                        nombre: {
                            required: true,
                        },
                        dominio: {
                            required: true
                        },
                        grupo: {
                            required: true
                        },
                        orden: {
                            required: true
                        }
                    },
            
                    messages: {
                        nombre: {
                            required: "El nombre es obligatorio"
                        },
                        dominio: {
                            required: "Debe seleccionar un dominio"
                        },
                        grupo: {
                            required: "Debe seleccionar un grupo"
                        },
                        orden: {
                            required: "El orden es obligatorio"
                        }
                    },

                    showErrors: function(errorMap, errorList) {
                        var errorString = '';

                        $.each(errorMap, function(key,value) {
                            errorString += '<li>' + value + '</li>';
                        });

                        if(errorString != ''){
                            $('.error-container').addClass('active');
                            $('.error-container').html(errorString);
                        }
                    }
                });

                break;
                
            case "#media-item-form":
                //var whitelist = ['png','jpe?g','gif'];
                debugger;
                $("#media-item-form").validate({
                
                    ignore: [],
                    onkeyup: false,
                    onfocusout: false,
                    wrapper: "li",
                    errorClass: "is-invalid",
                    
                    rules: {
                        nombre: {
                            required: true,
                            
                        },
                        input_file:{
                            //accept: whitelist.join('|'),
                            extension: "png|jpg|jpeg|gif",
                            //required: true,
                        },
                        
                    },
            
                    messages: {
                        nombre: {
                            required: "El nombre es obligatorio"
                        },
                        input_file: {
                            extension: "Sólo se admiten imagenes",
                            //accept: "Sólo se admiten imagenessssssssssssssss",
                            required: "El fichero es obligatorio"
                        },
                       
                    },

                    showErrors: function(errorMap, errorList) {
                        var errorString = '';

                        $.each(errorMap, function(key,value) {
                            errorString += '<li>' + value + '</li>';
                        });

                        if(errorString != ''){
                            $('.error-container').addClass('active');
                            $('.error-container').html(errorString);
                        }
                    }
                });

                break;
                
            case "#doc-item-form":
                debugger;

                $("#doc-item-form").validate({
                
                    ignore: [],
                    onkeyup: false,
                    onfocusout: false,
                    wrapper: "li",
                    errorClass: "is-invalid",
                    
                    rules: {
                        nombre: {
                            required: true,
                        },
                        input_file:{
                            extension: "pdf",
                            //required: true,
                        }
                        
                    },
            
                    messages: {
                        nombre: {
                            required: "El nombre es obligatorio"
                        },
                        input_file: {
                            extension: "Sólo se admiten pdf´s",
                            //accept: "Sólo se admiten pdf´ssssssssssssss",
                            required: "El fichero es obligatorio"
                        },
                       
                    },

                    showErrors: function(errorMap, errorList) {
                        debugger;
                        var errorString = '';

                        $.each(errorMap, function(key,value) {
                            errorString += '<li>' + value + '</li>';
                        });

                        if(errorString != ''){
                            $('.error-container').addClass('active');
                            $('.error-container').html(errorString);
                        }
                    }
                });

                break;
                
    }
}