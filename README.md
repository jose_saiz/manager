# Descripci�n

Web de Base realizada con Laravel 5.8.3 desarrollado por Saizfact


# Versiones de software

| Componente | Desarrollo | Producci�n |
| -------- | -------- | -------- |
| PHP | 7.1 | 7.1 |
| Laravel | 5.5 | 5.5 |


# Formato de C�digo

PHP y Javascript: No usar tabs para identar, usar espacios.

| Componente | Formato |
| -------- | -------- |
| PHP | PSR-2 |


# Comandos de Consola

## 1. Generar assets

Al modificar alg�n archivo de resources/assets, para publicarlo en la web hay que ejecutar este comando:

```
npm run dec  
����� Ya no se hace as�, se hace con el comando que aparece a continuaci�n !!!!!
```

## 2. Compilar SASS Front

Comando para compilar SASS
==============================================

```
node-sass --output-style="nested" resources/assets/sass/front/app.scss public/assets/front/css/app.css
```


# Comandos Artisan m�s usados en este proyecto

## 1. General 
### 1.1. Crear un modelo

Forma tradicional
```
php artisan make:model Models/DB/[Nombre]
```

Asignando la tabla relacionada
```
php artisan sf:make:model Models/DB/[Nombre] 
```

### 1.2. Crear una migraci�n

Migraci�n tradicional
```
php artisan make:migration create_t_[nombre]_table
```

Migraci�n con algunos campos adicionales
```
php artisan sf:make:migration create_t_[nombre]_table
```

## 2. Admin

### 2.1. Crear un controlador para el admin

```
php artisan make:controller Admin/[Nombre]Controller --resource --model=Models/DB/[Nombre]

```

# ViewComposer.  

ViewComposer permite centralizar en un �nico lugar llamadas recurrentes a datos que queremos que est�n disponibles al renderizarse las vistas. 

## 1.1 Crear viewcomposer

Dentro de la carpeta App\Http\ViewComposers se organizan los viewcomposers. El archivo es una clase que debe usar Illuminate\View\View (no hay un comando en artisan para crear estos archivos). En el constructor ubicar las llamadas a la base de datos y asignarlos a los atributos de la clase. En el m�todo "compose" inyectamos View y la utilizamos para pasar a la vista los datos recogidos en el constructor. El primer par�metro ser� el nombre de la variable que estar� disponible al renderizarse la vista, el segundo par�metro es el atributo de la clase que contiene los datos. 

```
public function compose(View $view)
{
    $view->with('categorias', $this->categorias);
}
```

## 1.2 Dar de alta el nuevo viewcomposer 

Dentro de /app/Providers/ViewComposerServiceProvider daremos de alta el nuevo viewcomposer dentro de boot, como primer par�metro pasamos la vista o vistas (en forma de array) donde queremos que se utilice el viewcomposer creado, y como segundo parametro pasamos la ruta donde se encuentra el viewcomposer. 

```
view()->composer(
    'admin.users.edit',
    'App\Http\ViewComposers\Admin\UsersRoles'
);

```

# Creaci�n de nuevas secciones en /admin mediante plantilla 

## 1. Creaci�n de rutas, carpetas y archivos

### 1.1. Crear rutas

Crear en /routes/web un Route::resource para la nueva secci�n, debemos definir el nombre de las rutas siguiendo siempre el mismo patr�n (prefijo_m�todo). Usaremos los m�todos index, create, store, destroy y show:

```
Route::resource('faqs/categorias', 'Admin\FaqCategoriaController', [
    'names' => [
        'index' => 'faqs_categorias',
        'create' => 'faqs_categorias_create',
        'store' => 'faqs_categorias_store',
        'destroy' => 'faqs_categorias_destroy',
        'show' => 'faqs_categorias_show',
    ]
]);
```
### 1.2. Crear carpeta para las vistas

Crear la carpeta que contiene las vistas de la nueva secci�n en /resources/views/admin con el nombre elegido para las rutas.

```
faqs_categorias
```
### 1.3. Crear archivo para los contenidos est�ticos de la vista

Crear un archivo /resources/lang/es/admin con el mismo nombre elegido para la ruta, rellenar dicho archivo con los siguientes campos que completan el contenido est�tico de la nueva secci�n: 

```
'page_title' => "Categor�as de las FAQ�s",
'parent_section' => "FAQ's",
'subsection' => 'Categor�as',
'new' => 'Nueva Categor�a',
'edit' => 'Editando :name',
'modal'=> '�Est� seguro de borrar la categor�a :name ?'
```
page_title: Etiqueta del t�tulo de la p�gina utilizada en el archivo /views/partials/breadcrumbs

parent_section: Etiqueta del nombre de la secci�n padre utilizada en el archivo /views/partials/breadcrumbs

subsection: Etiqueta del nombre de la subsecci�n (en el caso de que esta vista tuviera una secci�n por encima, por ejemplo, faqs_categorias es una subsecci�n de faqs), puede dejarse en blanco si no fuera una subsecci�n, utilizada en el archivo /views/partials/breadcrumbs

new: Etiqueta de nuevo elemento, utilizada en el archivo /views/partials/breadcrumbs.

edit: Etiqueta de edici�n de elemento, utilizada en el archivo  /views/partials/breadcrumbs. Recibe como parametro el nombre del elemento (:name).

modal: Mensaje de aviso en la ventana modal al eliminar un elemento, utilizada en el archivo /views/partials/delete_modal.

## 2. Elecci�n de plantilla 

### 2.1.1 Plantilla de tabla

Para utilizar la plantilla de tabla tenemos que extender la plantilla ubicada en /resources/views/admin/layout/table, y pasar como parametro el nombre de la ruta de la nueva secci�n:

```
@extends('admin.layout.table', [
    'route' => 'faqs',
])
```

Mediante jquery.dataTables.js y saizfact-datatables.js se renderiza las tablas para darles funcionalidades extra. Para usar datatables debemos etiquetar la tabla con la id #main-table y a�adir la etiqueta route con la ruta a la acci�n indexJson del controlador.

```
<table id="main-table" class="table table-striped"  route="{{route('modelo_de_datos_json')}}">
```

A continuaci�n creamos la ruta a la acci�n indexJson en el controlador.

```
Route::get('/modelo_de_datos/json', 'Admin\modelo_de_datosController@indexJson')->name('modelo_de_datos_json');
```

Laravel se integra con datatables a trav�s de la librer�a /vendor/yajra. 

https://datatables.yajrabox.com/
http://yajrabox.com/docs/laravel-datatables/master

Con ella podemos servir los datos a trav�s de AJAX, la usamos en el controlador:

```
use Yajra\Datatables\Datatables;
```

Creamos la acci�n indexJson con el que se comunicar� la tabla, podemos traer las relaciones con otras tablas mediante with (previamente se deben haber creado las relaciones en el modelo), y es importante hacer un select de la tabla principal de la que estamos trayendo los datos para evitar problemas de ambig�edad de nombres:

```
 $query = $this->producto
    ->with('categoria')
    ->with('subcategoria')
    ->select('t_modelo_de_datos.*');

    return $this->datatables->of($query)->toJson();   

```

Para construir la presentaci�n de las columnas disponemos de las siguientes opciones:

data-data [string]: se especifica el nombre del campo de la tabla que debe ser recogido de la base de datos.

data-name [string]: el nombre que recibir� la columna creada para uso interno de datatables.

data-searchable [boolean]: si la columna debe aparecer o no en los resultados del buscador general.

data-orderable [boolean]: si la columna debe tener las flechas de ordenaci�n junto al t�tulo de la columna.

data-visible [boolean]: si la columna debe ser visible. 

data-className [string]: a�adir una clase a todos los elementos de la columna. 

data-defaultContent [string]: a�adir elementos a la columna que no estrictamente datos, por ejemplo, podemos
escribir c�digo y a�adir un bot�n de editar.

```
<th data-data="nombre" data-name="nombre">Nombre</th>
```

Para a�adir a los botones que dirigen a la edici�n de cada elemento de la tabla, mediante data-defaultContent introducimos un enlace que usa la clase 'edit-button' y la etiqueta 'route' apuntando al nombre de la ruta index (internamente el javascript combina esta ruta con la id del elemento seleccionado y utiliza la ruta_show). Con ello conseguimos que la llamada AJAX act�e.  

```
<th data-orderable="false" data-defaultContent="
    <a class='edit-button' route='{{route('modelo_de_datos')}}'> 
        <i class='fa fa-pencil text-inverse m-r-10'></i> 
    </a>">
</th>
```

### 2.1.2 Filtros para las tablas

Podemos a�adir filtros y opciones de b�squeda a las tablas mediante:

```
@include('admin.partials.filters')
```

Por defecto mostrar� siempre el buscador general. Par�metros opcionales que acepta:

$new [string]: si es 'true' aparecer� el filtro de novedades. Debe existir un campo 'novedad' en la tabla para funcionar. 

$order_route [string]: la ruta a la acci�n reorderTable en el controlador (por ejemplo, modelo_de_datos_reorder).Hace disponible el bot�n de ordenaci�n, debe existir el campo 'orden' en la tabla para usarlo y ser agregada la columna (th) orden en la tabla. Tambi�n se puede usar exclusivamente para las novedades, para ello deber� existir el campo 'orden_novedad' en la tabla y ser agregada como columna en la tabla. Ver ejemplo en /admin/modelo_de_datos/index.

$order_visible [string]: si es 'true' hace que el bot�n de ordenaci�n est� visible en todo momento.

$subfilter [collection]: Filtro secundario que depende de $filter haciendo uso de select_related 
(/js/saizfact.js), al elegir un elemento del subfiltro se actualizan las opciones de $filter.Si 
usamos esta opci�n debemos etiquetar la columna que ser� filtrada con la id subfilter-column.
    
$subfilter_placeholder [string]: Mensaje para el placeholder de $subfilter.
    
$filter [collection]: Filtro primario, se puede utilizar sin necesidad de utilizar $subfilter. Si 
usamos esta opci�n debemos etiquetar la columna que ser� filtrada con la id filter-column.

$filter_placeholder [string]: Mensaje para el placeholder de $subfilter.

### 2.2. Plantilla de formulario

Para utilizar la plantilla de formulario tenemos que extender la plantilla ubicada en /resources/views/admin/layout/form, y pasar como parametro el nombre de la ruta de la nueva secci�n y el nombre y la id del objeto devuelto por el 'index 'del controlador:

```
@extends('admin.layout.form', [
    'route' => 'faqs', 
    'name' => $faq->nombre, 
    'id' => $faq->id
])
```

A�adir la clase 'admin-form' y la etiqueta 'action' con la ruta a 'store' para que la llamada AJAX act�e, a�adir tambi�n una id que servir� para identificar el formulario en el archivo de validaciones saizfact.validations.js que hace uso de jquery.validate.js:

```
 <form class='admin-form' id='faqs-form' action="{{route('faqs_store')}}">
```
Incluir dentro del formulario el csrf_field y un input hidden recogiendo la id del objeto si esta existiera. Este input hidden sirve al m�todo 'store' del controlador que hace uso de updateOrCreate identificar si el elemento que hemos guardado ya existe en la base de datos o debe crearlo.  

```
 {{ csrf_field() }}

@isset ($faq->id)
    <input type="hidden" name="id" value="{{$faq->id}}">
@endisset
```

A�adir las reglas de validaci�n en el archivo saizfact.validation.js (ATENCI�N, por ahora editar el archivo que se encuentra en /public/js ya que el precompilador Laravel Mix no est� leyendo el ubicado en /resources/assets/js)

### 2.3. Plantilla de tabla y formulario

Suponemos que esta plantilla se utiliz� para subsecciones (por ejemplo faqscategorias es una subsecci�n de faqs). Para utilizar la plantilla de tabla y formulario tenemos que extender la plantilla ubicada en /resources/views/admin/layout/table_form, y pasar como parametro el nombre de la ruta de la nueva secci�n, el nombre de la ruta de la secci�n padre y el nombre y la id del objeto devuelto por el 'index 'del controlador:

```
@extends('admin.layout.table_form', [
    
    'route' => 'faqs_categorias', 
    'parent_section' => 'faqs', 
    'name' => $categoria->nombre, 
    'id' => $categoria->id
])
```

Seguir las reglas se�aladas anteriormente para las plantillas tabla y form para completar los pasos necesarios. 

# Validaciones de formularios y gesti�n de errores

## 1. Validaci�n de formularios

### 1.1. Validaciones a trav�s del cliente

Las validaciones a trav�s del cliente se realizan con jquery.validate.js. Para utilizar esta librer�a hay que etiquetar el formulario con una id, y recoger �sta mediante un evento en el archivo saizfact.js, la funci�n que activa la validaci�n es:

```
formValidation(formId)
```
La funci�n formValidation est� definida en el archivo saizfact.validations.js. 

IMPORTANTE: Laravel Mix no est� compilando correctamente el archivo que se encuentra en /resources/assets/js, por lo que hay que trabajar directamente con el ubicado en /public/js

Dentro del archivo saizfact.validations.js se definen las reglas de validaci�n de cada formulario y los mensajes de error. 

### 1.2. Validaciones a trav�s del servidor

Las validaciones a trav�s del servidor se encuentran en /app/http/requests y se crean a trav�s de artisan:

```
php artisan make:request (nombre)
```

Igual que en las validaciones a trav�s del cliente, dentro del nuevo archivo especificaremos las reglas y mensajes de error que deben ser las mismas que hayamos especificado en la validaci�n de cliente.

Para utilizar la validaci�n creada la importamos dentro del controlador y la inyectamos en el m�todo:

```
use App\Http\Requests\Admin\FaqRequest;

public function store(FaqRequest $request)
```

Los errores de validaci�n son devueltos en forma de objeto JSON con c�digo de error 422. Para capturarlos en la respuesta error de AJAX debemos parsearlos:

```
error: function(response){
                
    var parsedJson = JSON.parse(response.responseText);
    var errorString = '';

    $.each( parsedJson.errors, function( key, value) {
        errorString += '<li>' + value + '</li>';
    });
    
    $('.error-container').addClass('active');
    $('.error-container').html(errorString);
}
```

## 2. Gesti�n de errores

### 2.1. Errores HTTP

Los errores HTTP y la respuesta que debe dar la aplicaci�n a ellos se encuentran definidos en /app/Exceptions/Handler. Dentro de la secci�n render se define las redirecciones para los errores 400, 419, 429, 500 y 503 que apuntan a las vistas ubicadas en /resources/views/errors

### 2.2 Errores Mysql

La captura de los errores Mysql se definen dentro de cada m�todo mediante un try/catch, en el catch inyectamos QueryException que devuelve un objeto exception donde errorInfo[0] es el c�digo del error y errorInfo[2] el mensaje del error. Igual que en el punto Validaciones a trav�s del servidor, podemos definir que la respuesta al evento AJAX sea un objeto json con c�digo de error 422 para que pueda ser capturado. 

```
try {
    $categoria->delete();

}catch(QueryException $exception) {

    if($exception->errorInfo[0] == '23000'){
        return response()->json([
            'errors' => "Est� intentando eliminar una categor�a que est� siendo usada por algunas 
            FAQ's , asigne una nueva categor�a a las siguientes FAQ's para poder eliminar esta categor�a:",
        ], 422);
    }else{
        return response()->json([
            'errors' => $exception->errorInfo[2],
        ], 422);
    }
}
```

# Componente de traducciones

## 0 Instalaci�n

Para hacer uso del componente de traducciones debemos:

1� Copiar la carpeta Locale dentro de la ruta /app/Vendor/Saizfact

2� A�adir la siguiente l�nea dentro del archivo /app/Http/Kernel.php en la secci�n middlewareGroups:

   \App\Http\Middleware\Localization::class,

3� A�adir las siguientes l�neas dentro de /config/app.php en Package Service Providers:

App\Vendor\Saizfact\Locale\ManagerServiceProvider::class,
App\Vendor\Saizfact\Locale\TranslationServiceProvider::class,
App\Vendor\Saizfact\Locale\LocalizationSeoServiceProvider::class,

y dar de alta el siguiente alias en el mismo archivo:

'LocalizationSeo' => App\Vendor\Saizfact\Locale\Facades\LocalizationSeo::class,

4� Lanzar las migraciones que se encuentran dentro de App\Vendor\Saizfact\Locale\migrations que crear�n las tablas necesarias. 

5� Finalmente lanzaremos los siguientes comandos en la consola:

composer dump-autoload
php artisan config:cache

Importante: El archivo app/Vendor/Saizfact/Locale/Translator.php extiende el archivo propio de Laravel que se ocupa de gestionar la carga de traducciones
al detectar etiquetas de traducci�n (@lang) de Blade. En concreto, la funci�n get modifica su funci�n para poder guardar en la base de datos los valores al
cargar las traducciones. Si se instalan otros paquetes que afecten a las traducciones hay que vigilar que no entre en conflicto con dicha funci�n.

## 1 Locale, siistema de formularios multiidioma

Esta secci�n depende de la clase App\Vendor\Saizfact\Locale\Locale cuyo cometido es facilitar la
creaci�n de formularios en multiidioma y el tratamiento y almacenamiento de los datos en la tabla
t_locale

## 1.1 A�adir pesta�as de traducci�n a los formularios

Para usar el componente de traducciones en un formulario debemos introducir los inputs que queremos hacer traducibles dentro del siguiente c�digo.

```
@component('admin.partials.locale')

@foreach ($idiomas as $idioma)

inputs que queremos hacer traducibles

@endforeach

@endcomponent
                
```

$idiomas es una variable que trae los registros de la tabla t_language y se recoge a trav�s del viewcomposer /app/HTTP/ViqwComposers/Admin/Locale que la hace disponible en todas las vistas de la carpeta admin. 

Opcionalmente, si las pesta�as de traducci�n se encuentran a su vez dentro de pesta�as (ver ejemplo en /views/admin/paginas/edit) debemos pasar como par�metro el nombre de la pesta�a que la contiene.  

@component('admin.partials.locale', ['tab' => 'contenido'])

## 1.2 Recogida de valores de los inputs

Para poder recoger los valores de los inputs les daremos como name siempre la misma estructura, vease en los siguientes ejemplos:

```
name="locale[titulo.{{$idioma->alias}}]"
name="locale[descripcion.{{$idioma->alias}}]"
name="locale[categoria.{{$idioma->alias}}]"
```

El array locale recoger� todos los inputs traducibles para ser manipulado por el controlador. 

## 1.3 Guardar los valores en el controlador

Dentro de cada controlador manipularemos las traducciones haciendo uso de la clase Locale.

```
use App\Vendor\Saizfact\Locale\Locale;
```
Inyectaremos la dependencia en el constructor del controlador seteando el atributo rel_parent mediante el m�todo setParent que definir� para que entidad estamos haciendo las traducciones, por convenci�n separamos las palabras con puntos:

```
protected $locale;

function __construct(Locale $locale)
{
    $this->locale = $locale;
    $this->locale->setParent('modelo_de_datos.form');   
}
```

Para guardar los datos usaremos la siguiente llamada al m�todo store: 

```
$locale = $this->locale->store(request('locale'), $categoria->id);
```

Donde el primer par�metro ser� el array locale recogido en el formulario, y el segundo
p�rametro ser� la id del objeto creado.

## 1.4 Otros m�todos de la clase Locale

La clase Locale dispone de m�todos create, show y destroy. A los dos �ltimos les debemos pasar como par�metro par�metro la id del objeto:

```
$this->locale->show($categoria->id); 
$this->locale->delete($categoria->id);

```
## 2 LocaleTag, sistema de almacenamiento y manipulaci�n de etiquetas. 

Esta secci�n depende principalmente de la clase App\Vendor\Saizfact\Locale\Manager la cual es 
llamada  App\Vendor\Saizfact\Locale\Translator cuando se cargan etiquetas de traducci�n de 
Blade: 'trans', 'trans_choice', 'Lang::get', 'Lang::choice', 'Lang::trans', 'Lang::transChoice',
'@lang'. Su cometido es cargar el contenido fijo de la web en multiidioma a trav�s los archivos
ubicados en /resources/lang a la vez que se almacenan en la tabla t_locale_tag

El archivo de configuraci�n de LocaleSeo se encuentra en App\Vendor\Saizfact\Locale\config\translation-manager

Importante: Para cargar por primera vez el contenido de una etiqueta es necesario recargar dos
veces la p�gina que contiene la etiqueta. ES FUNDAMENTAL A�ADIR UN GITIGNORE en las carpetas /resources/lang que se ver�n afectadas por este sistema. 

## 2.1 Funcionamiento b�sico

Partimos del funcionamiento normal de la etiqueta @lang

https://laravel.com/docs/5.6/localization

LocaleTag nos permite no tener que crear ni editar en los archivos /resources/lang ya que autom�ticamente cuando Blade cargue las etiquetas, si el archivo y la clave no existen, los crear�. A la vez se almacenar� el valor en la tabla t_locale_tag si este registro no existiera. 

Desde el panel de administrador, en la secci�n de traducciones, si el usuario cambia un registro, 
se modificar� el valor en el archivo, de tal forma se facilita la gesti�n del contenido est�tico de 
la web. 

El contenido est�tico Laravel no lo carga haciendo llamadas a la base de datos, sino desde los archivos ubicados en /resources/lang

Los campos definidos en la tabla t_locale_tag son:

- rel_profile : alias de idioma
- group: nombre del archivo que contiene las claves, por ejemplo si el archivo estuviera en /resources/lang/es/admin/faqs el group ser� admin/faqs  
- key: la clave utilizada para cargar la traducci�n
- value: la traducci�n

## 2.2 Modo de empleo para el desarrollador

Si queremos escribir contenido est�tico utilizaremos la etiqueta @lang() en las vistas, y podemos utilizar la etiqueta Lang::trans() dentro de los controladores si fuera necesario. Como par�metro
es obligatorio pasarle el nombre de archivo ubicado dentro de /resources/lang y la clave que queremos
cargar. Por ejemplo: 

```
@lang('topbar.buscar') 
``` 

Cargar� el archivo topbar y el valor de la clave buscar. Si este archivo o clave no existiera los crear�.

Opcionalmente podemos explicitar un valor si la etiqueta es nueva:

```
@lang('topbar.buscar', ['es' => 'buscar, 'pt' => 'pesquisar']) 
``` 

Estar�amos creando una nueva clave buscar, cuyo valor en espa�ol ser� buscar y en portugues pesquisar. El problema de darle un valor en el c�digo, fuera del archivo topbar, es que el valor que
le demos puede quedar desfasado si el usuario decide cambiarlo desde el panel de administraci�n.

_Comentarios_

1. No se puede usar m�s de un punto "." => `@lang('saizfact.test1.b')`
Lo correcto es `@lang('saizfact.test1_b')`

# 2.3 Comandos de consola

Podemos importar (llevar a la base de datos todos los valores que existan en los archivos y que no existan en la tabla t_locale_tag) mediante el siguiente comando en la consola:

```
php artisan translations:import
``` 

Podemos exportar (modificar los archhivos ubicados es /resources/lang a partir de los registros existentes en la tabla t_locale_tag) mediante el siguiente comando en la consola:

```
php artisan translations:export topbar
``` 

Siendo topbar el group (ver secci�n 2.1), se exportar�n todas las claves y valores de este archivo.

## 3 LocaleSEO, sistema de almacenamiento y manipulaci�n de rutas. 

Esta secci�n depende principalmente de la clase App\Vendor\Saizfact\Locale\LocalizationSeo. Su cometido es cargar las rutas de la web en multiidioma a trav�s los archivos
ubicados en /resources/lang/{alias}/routes.php a la vez que se almacenan en la tabla t_locale_seo

El archivo de configuraci�n de LocaleSeo se encuentra en App\Vendor\Saizfact\Locale\config\config_seo y en �l podemos definir los idiomas habilitados en nuestra p�gina. 

Importante: Hay que crear el archivo routes.php en la carpeta raiz de cada alias de idioma. Por ejemplo, en /resources/lang/es/routes.php se almacenar�n los enlaces multiidioma en espa�ol.

## 3.1 Funcionamiento b�sico

En el archivo de rutas /routes/web.php instanciaremos la clase LocalizationSeo:

```
use App\Vendor\Saizfact\Locale\LocalizationSeo;

$localization = new LocalizationSeo();

```
A partir de aqu�, cada vez que queramos hacer una ruta traducible tenemos que cargar el middleware:

```
'prefix' => $localization->setLocale(),  
'middleware' => ['localize']
```
- prefix a�ade autom�ticamente el alias de idioma a las urls.

Finalmente a�adir�amos lo siguiente a cada nueva ruta:

```
Route::get($localization->transRoute('routes.compra-online'), 'Front\HomeController@index')->name('compra-online');
```

&localization->transRoute() activa el sistema multiidioma y cargar� dentro del archivo routes.php ubicado en resources/lang/{alias} la clave compra-online, cuyo valor ser� la url.


```
'compra-online' => 'compra-online/productos/familias/{id}',
```

Por ejemplo, la clave compra-online cargar�a la url indicada, siendo id la indicaci�n que se pasar� un par�metro a trav�s de la url.

IMPORTANTE: Debemos dar a la ruta el mismo "name" que la etiqueta que estemos utilizando, esto permite registrar en qu� subdominio se encuentra la ruta en la base de datos.


## 3.2 Modo de empleo para el desarrollador

Una vez que hayamos escrito en el archivo routes/web.php la nueva ruta y la nueva clave a la que apunta (ver punto 3.1) es importante ir a los distintos archivos por idioma routes.php ubicados en /resources/lang para a�adir la nueva etiqueta creada (por seguridad no existe como en LocaleTag un mecanismo que automatice este proceso). 

En cualquier momento podemos acceder al idioma en el que se encuentra el usuario con la funci�n
app()->getLocale()

La importaci�n a la base de datos se realizar� exclusivamente cuando el usuario acceda a la secci�n SEO del panel de administraci�n, lo cuual har� que se retarde levemente la carga de esta secci�n. Una vez dentro si el usuario cambia un valor desde el panel, autom�ticamente se modificar� en el archivo.

IMPORTANTE: Como el usuario puede modificar las urls no se debe escribir en los enlaces que usemos en las vistas una url (ya que puede quedar desfasada), en vez de ello daremos nombre a todas las rutas en el archivo web.php con la funci�n name() y utilizaremos la funci�n route() en las vistas para escribir la ruta del enlace.

# Tratamiento de im�genes

## 1.1 Input de im�genes

Utilizamos la librer�a dropify.js, configurada en saizfact.js. Convertimos los input-file
d�ndoles la siguiente clase:

```
class="dropify"
``` 

## 1.2 Uso de la clase Imagen

Dentro de cada controlador trataremos las imagenes haciendo uso de la clase Imagen.

```
use App\Vendor\Saizfact\Imagen\Imagen;
```
Inyectaremos la dependencia en el constructor del controlador seteando el atributo entity_type mediante el m�todo setType que definir� para que entidad estamos guardando la imagen, por convenci�n separamos las palabras con puntos:

```
protected $imagen;

function __construct(Imagen $imagen)
{
    $this->imagen = $imagen;

    $this->imagen->setType('modelo_de_datos');  
}

```
La clase Imagen hace uso de la siguiente librer�a para el tratamiento de im�genes: http://image.intervention.io/

La clase Imagen recoge la configuraci�n con la que debe ser redimensionada cada imagen de la tabla de la base de datos t_imagen_configuracion donde debemos especificar:

-entity_type: nombre de la entidad que queremos configurar. 
-directory: la carpeta donde ser� guardada la imagen (se tendr� que crear la carpeta en la ruta que demos).
-grid: para que tama�o de grid de bootstrap est� redimensionada la imagen.  
-content_type: la extensi�n que tendr� el archivo guardado.
-width: ancho en pixeles que queremos redimensionar la imagen.
-height: alto en pixeles que queremos redimensional la imagen.
-quality: la calidad de la imagen (100 m�ximo).

Por ahora la clase Imagen redimensiona las im�genes sin necesidad de especificar el ancho, ya que usa el m�todo heighten que respeta las proporciones de la original. http://image.intervention.io/api/heighten

Para guardar la imagen usaremos la siguiente llamada al m�todo store: 

```
$imagen = $this->imagen->store(request('input-file'), $producto->id);
```

Donde el primer par�metro ser� el fichero recogido en el formulario, y el segundo
p�rametro ser� la id del objeto creado. Opcionalmente se puede a�adir un tercer parametro donde definir para qu� idioma se est� guardando la imagen, por defecto 'es'.

La clase Imagen dispone de m�todos show y destroy, a los cuales debemos pasar como par�metro la id del objeto:

```
$this->locale->show($categoria->id); 
$this->locale->delete($categoria->id);

```
#Generaci�n autom�tica de enlaces a partir de t�tulo

## 1.1 Input de t�tulo

La generaci�n autom�tica de enlaces hace uso del componente de traducciones para crear un enlace en cada idioma a partir del t�tulo introducido. Para poder recoger los valores de los inputs les daremos como name siempre la misma estructura:

```
name="locale[titulo.{{$idioma->alias}}]"
```

El array locale recoger� todos los inputs traducibles para ser manipulado por el controlador. 

## 1.2 Uso de la clase Sitemap

Dentro de cada controlador manipularemos las traducciones haciendo uso de la clase Sitemap.

```
use App\Vendor\Saizfact\Sitemap\Sitemap;
```
Inyectaremos la dependencia en el constructor del controlador seteando el atributo rel_parent mediante el m�todo setParent que definir� para que entidad estamos generando un enlace:

```
protected $sitemap;

function __construct(Sitemap $sitemap)
{
    $this->sitemap = $sitemap;
    $this->sitemap->setParent('pagina');   
}
```

Para guardar los datos usaremos la siguiente llamada al m�todo store: 

```
$this->sitemap->store(request('locale'), $pagina->id);
```

Donde el primer par�metro ser� el array locale recogido en el formulario, y el segundo
p�rametro ser� la id del objeto creado. Opcionalmente podemos a�adir un tercer par�metro para a�adir al enlace un string, por ejemplo podemos que a parte de usar el t�tulo para generar el enlace se a�ada adem�s un c�digo personalizado que se haya introducido en el formulario: 

```
$this->sitemap->store(request('locale'), $producto->id, request('codigo'));
```
## 1.3 Otros m�todos de la clase Sitemap

La clase Sitemap dispone de m�todos create, show y destroy. A los dos �ltimos les debemos pasar como par�metro par�metro la id del objeto:

```
$this->sitemap->show($pagina->id); 
$this->sitemap->delete($pagina->id);

```

## 1.4 Proceso de solicitud del contenido de un enlace 

(Documentar cuando se desarrolle el front)

#Tratamiento de multiusuario y login

## 1.1 Configuraci�n de multiusuario

Se ha seguido la siguiente documentaci�n para desarrollar el multiusuario:

https://www.youtube.com/watch?v=iKRLrJXNN4M&list=PLwAKR305CRO9S6KVHMJYqZpjPzGPWuQ7Q

Siguiendo adem�s el siguiente ejemplo:

https://github.com/DevMarketer/multiauth_tutorial

Los archivos cr�ticos donde se configura el multiusuario son:

/config/auth.php 

Donde podemos generar "guards" que equivale a tipo de usuarios diferentes en la aplicaci�n. En este caso el guard "web" es para los administradores, y el guard "cliente" para los clientes. 

/app/Exceptions/Handler.php

Donde se declaran en la funci�n unauthenticated las excepciones cuando el usuario no est� logueado y no tiene permiso para acceder a una p�gina, en este caso se redirecciona a la ruta del formulario de login. 

/app/Http/Middleware/RedirectifAuthenricated

Donde se declara donde redireccionar al usuario una vez que se ha logueado. 

/app/Http/Controllers/Auth/LoginController y ClienteLoginController

Donde se declara en la funci�n logout donde redireccionar al usuario cuando sale de la sesi�n. 

## 1.2 Permisos de acceso y captura del usuario logueado

Para restringir el acceso a un controlador declaramos en su constructor:

```
$this->middleware('auth');
```
Que por defecto s�lo permitir� el acceso a los usuarios administrador. Si queremos que s�lo se permita el acceso a los usuarios cliente declaramos:

```
$this->middleware('auth:cliente');
```

Podemos preguntar por la id del usuario logueado a trav�s de:

```
Auth::guard('web')->id() //para administradores
Auth::guard('cliente')->id() //para clientes
```
Tambi�n podemos acceder a los datos del usuario logueado a trav�s de:

```
Auth::guard('web')->user()->name  //se solicita el nombre del usuario 'administrador' logueado. 
Auth::guard('cliente')->user()->name  //se solicita el nombre del usuario 'cliente' logueado. 
```



