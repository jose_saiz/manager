<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->check()) {
            if (Auth::guard('web')->user()->canAtLeast(['ver.proyectos'])){
                return redirect()->route('admin_printers');
            }elseif (Auth::guard('web')->user()->canAtLeast(['ver.configs'])){
                return redirect()->route('configs');
            }
        }
        return $next($request);
    }
}
