<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;
use App\Models\DB\Product as Product;

class Products
{
    public $products;

    public function __construct()
    {
        //$this->categorias = Category::Printers()->orderBy('name', 'asc')->get();
         
        $rdo = Product::where('active',1)->get();
        $this->products = $rdo;
    }

    public function compose(View $view)
    {
        $view->with('products', $this->products);
    }
}