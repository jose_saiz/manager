<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;
use App\Models\DB\Product as Product;

class Summary
{
    public $greenies_summary;
    public $parts_summary;
    public $printers_summary;
    public $products_summary;
    public function __construct()
    {

        $this->products_summary['total'] = '6';
        $this->products_summary['assigned'] = '3';
        $this->products_summary['unassigned'] = '3';
        $this->parts_summary['total'] = '80';
        $this->parts_summary['assigned'] = '40';
        $this->parts_summary['unassigned'] = '40';
        $this->printers_summary = 25;
        
        
    }

    public function compose(View $view)
    {
        $view
        ->with('products_summary', $this->products_summary)
        ->with('parts_summary', $this->parts_summary)
        ->with('printers_summary', $this->printers_summary);
    }
}