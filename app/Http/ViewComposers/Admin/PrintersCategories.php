<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;
use App\Models\DB\Category as Category;

class PrintersCategories
{
    public $categorias;

    public function __construct()
    {
        //$this->categorias = Category::Printers()->orderBy('name', 'asc')->get();
         
        $rdo = Category::where('category_type_id',1)->get();
        $this->categorias = $rdo;
    }

    public function compose(View $view)
    {
        $view->with('categorias', $this->categorias);
    }
}