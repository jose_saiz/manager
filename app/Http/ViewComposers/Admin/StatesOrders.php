<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;
use App\Models\DB\State as State;

class StatesOrders
{
    public $states_order;

    public function __construct()
    {
        //$this->categorias = Category::Printers()->orderBy('name', 'asc')->get();
         
        $rdo = State::where('state_type_id',4)->get();
        $this->states_order = $rdo;
    }

    public function compose(View $view)
    {
        $view->with('states_order', $this->states_order);
    }
}