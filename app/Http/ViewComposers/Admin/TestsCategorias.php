<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;
use App\Models\DB\TestCategoria as TestCategoria;

class TestsCategorias
{
    public $categorias;

    public function __construct()
    {
        $this->categorias = TestCategoria::orderBy('nombre', 'asc')->get();
    }

    public function compose(View $view)
    {
        $view->with('categorias', $this->categorias);
    }
}