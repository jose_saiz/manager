<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;
use App\Models\DB\Customer as Customer;

class Customers
{
    public $customers;

    public function __construct()
    {
        //$this->categorias = Category::Printers()->orderBy('name', 'asc')->get();
         
        $rdo = Customer::get();
        $this->customers = $rdo;
    }

    public function compose(View $view)
    {
        $view->with('customers', $this->customers);
    }
}