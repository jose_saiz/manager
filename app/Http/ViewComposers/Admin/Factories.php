<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;
use App\Models\DB\Factory as Factory;

class Factories
{
    public $factories;

    public function __construct()
    {
        //$this->categorias = Category::Printers()->orderBy('name', 'asc')->get();
         
        $rdo = Factory::get();
        $this->factories = $rdo;
    }

    public function compose(View $view)
    {
        $view->with('factories', $this->factories);
    }
}