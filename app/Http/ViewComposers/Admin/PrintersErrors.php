<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;
use App\Models\DB\PrinterError as PrinterError;

class PrintersErrors
{
    public $printers_errors;

    public function __construct()
    {
        //$this->categorias = Category::Printers()->orderBy('name', 'asc')->get();
         
        $rdo = PrinterError::get();
        $this->printers_errors = $rdo;
    }

    public function compose(View $view)
    {
        $view->with('printers_errors', $this->printers_errors);
    }
}