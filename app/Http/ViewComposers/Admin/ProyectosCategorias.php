<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;
use App\Models\DB\ProyectoCategoria as ProyectoCategoria;

class ProyectosCategorias
{
    public $categorias;

    public function __construct()
    {
        $this->categorias = ProyectoCategoria::orderBy('nombre', 'asc')->get();
    }

    public function compose(View $view)
    {
        $view->with('categorias', $this->categorias);
    }
}