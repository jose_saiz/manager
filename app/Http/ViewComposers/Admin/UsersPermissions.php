<?php

namespace App\Http\ViewComposers\Admin;

use App\Vendor\Caffeinated\Shinobi\Models\Permission;
use Illuminate\View\View;

class UsersPermissions
{
    public $permissions;

    public function __construct()
    {
        $this->permissions = Permission::orderBy('name', 'asc')->get();
    }

    public function compose(View $view)
    {
        $view->with('permissions', $this->permissions);
    }
}
