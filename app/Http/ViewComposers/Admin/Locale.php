<?php

namespace App\Http\ViewComposers\Admin;

use Illuminate\View\View;
use App\Models\DB\Language;

class Locale
{
    public $languages;

    public function __construct()
    {
        $this->languages = Language::where('active', 1)->orderBy('id', 'asc')->get();
    }

    public function compose(View $view)
    {
        $view->with('languages', $this->languages);
    }
}