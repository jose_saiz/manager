<?php

namespace App\Http\ViewComposers\Admin;

use App\Vendor\Caffeinated\Shinobi\Models\Role;
use Illuminate\View\View;

class UsersRoles
{
    public $roles;
    public $roles_assigned;

    public function __construct()
    {
        $this->roles = Role::orderBy('name', 'asc')->get();
    }

    public function compose(View $view)
    {
        $view->with('roles', $this->roles);
    }
}
