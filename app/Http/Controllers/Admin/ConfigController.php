<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
//use Illuminate\Http\Request;
use App\Http\Requests\Admin\ConfigRequest;
use App\Models\DB\Config;

class ConfigController extends Controller
{
    protected $permissions  = ['ver.configs', 'edit.configs'];
    protected $config;
    protected $datatables;

    function __construct(Config $config,Datatables $datatables)
    {
        $this->middleware('auth');
        
        $this->config = $config;
        $this->datatables = $datatables;

    }

    public function index()
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        return view('admin.configs.index');
    }

    public function indexJson()
    {
        $query = $this->config
        ->select('t_config.*');

        return $this->datatables->of($query)->toJson();
    }

    public function create()
    {
        $view = View::make('admin.configs.edit')
        ->with('config', $this->config)
        ->renderSections();

        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
        ]);
    }

    public function store(ConfigRequest $request)
    {
        $fields = [
            'dominio' => request('dominio'),
            'nombre' => request('nombre'),
            'valor' => request('valor')
        ];
        if (Auth::guard('web')->user()->isSaizfact() && request('clave')) {
            $fields['clave'] = request('clave');
        }
        $config = Config::updateOrCreate(['id' => request('id')], $fields);

        if (request('id')){
            $message = "Config actualizada ". $config->nombre;
        }else{
            $message = "Config creada ". $config->nombre;
        }

        $view = View::make('admin.configs.edit')
        ->with('config', $config)
        ->renderSections();

        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
            'id' => $config->id,
            'message' => $message,
        ]);
    }

    public function show(Config $config)
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }     
      
        $view = View::make('admin.configs.edit')
        ->with('config', $config);

        if(request()->ajax()) {
            $sections = $view->renderSections(); 
    
            return response()->json([
                'layout' => $sections['content'],
                'form' => $sections['form'],
            ]); 
        }
                
        return $view;
    }

    public function destroy(Config $config)
    {   
        $config->delete();
        
        $message = "Config borrada ". $config->nombre;

        $view = View::make('admin.configs.index')
        ->with('configs', $this->config->get())
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'view' => $view['table'],
            'message' => $message,
        ]);
    }
}
