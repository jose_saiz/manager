<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

use Yajra\Datatables\Datatables;

use App\Http\Requests\Admin\PrinterRequest;
use App\Models\DB\Printer;
use App\Models\DB\Language;
use App\Models\DB\Part;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;

use App\Vendor\Greeny\Main\DateTimeG;
use App\Vendor\Greeny\Main\PrinterTimeline;
use App\Vendor\Greeny\Main\PrintersMatrix;


class PrinterController extends Controller
{
    //
    protected $permissions  = ['ver.printers', 'edit.printers'];
    protected $printer;
    protected $language;
    protected $locale;
    protected $locale_categorias;
    protected $locale_slug_seo;
    protected $locale_slug_seo_categorias;
    protected $datatables;
    protected $start_date_time;
    
    function __construct(Printer $printer,Language $language, Locale $locale, Datatables $datatables,Locale $locale_categorias, LocaleSlugSeo $locale_slug_seo, LocaleSlugSeo $locale_slug_seo_categorias)
    {
        set_time_limit(0);
        $this->middleware('auth');
        
        $this->printer = $printer;
        $this->language = $language;
        
        $this->locale = $locale;
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_categorias = $locale_categorias;
        $this->locale_slug_seo_categorias = $locale_slug_seo;
        
        $this->datatables = $datatables;
        
        $this->printer->active = 1;
        $this->locale->setParent('printers.form');
        $this->locale_slug_seo->setParent('printers.form');
        $this->locale_categorias->setParent('printers.categorias.form');
        $this->locale_slug_seo_categorias->setParent('printers.categorias.form');
        
        $this->start_date_time = new DateTimeG ();
        
    }
    
    public function index()
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        //session()->put('locale', $language);
        //App::setLocale($language);
        //return  View::make('admin.printers.index')->with('language', $language);
        return view('admin.printers.index');
    }
    
    public function indexJson()
    {
        $query = $this->printer
        ->with('category')
        ->select('printer.*')
        ->where('neighborhood_factory_id',Auth::guard('web')->user()->factory_id)
        //->orderBy('id','asc')
        ;
        
        
        
        return $this->datatables->of($query)
        ->editColumn('available',function($query){
            //return "-------".$query->available."-----------";
            $ckecked = '';
            if ($query->available) $ckecked = 'checked'; 
            return  '
                    <form class="admin-form" id="printers-form-'.$query->id.'" name="printers-form-'.$query->id.'" action="'.route('admin_printers_store').'" method = "post">
                         '.csrf_field().'
                        <input type="hidden" id="id"              name="id"             value="'.$query->id.'">
                        <input type="hidden" id="code"            name="code"            value="'.$query->code.'">
                        <input type="hidden" id="start_date_time" name="start_date_time" value="'.$this->start_date_time.'">
                        <input type="hidden" id="action"  name="action"   value="available">
                        <div  style="width:20px;margin-top: -13px;margin-left: 15px;">
                        <input  style="visibility: visible;" 
                                data-toggle="modal" 
                                data-target="#create-modal-available" 
                                id = "available-'.$query->id.'" 
                                name = "available-'.$query->id.'" 
                                type="checkbox" 
                                value="true" '.$ckecked.'  
                                class="available  bt-switch form-control" 
                                data-size="small">
                        </div>
                    <form>
            ';
        })
        ->addColumn('roll_weight',function($query){
            return '
                    <a class = "change-roll" id="change-roll-'.$query->id.'"'  .
                   '" data-id="'.$query->id.
                   '" data-code="'.$query->code.
                   '" data-weight="'.$query->roll_weight.
                   '" data-end="'.$query->roll_end_datetime.
                   '" data-datetime_replace="'.$query->roll_replacement_datetime.
                   '" data-toggle="modal" data-target="#create-modal-change-roll"> 
                       <i class="fa fa-life-ring text-inverse m-r-10"></i>
                    </a>
            ';
        })
        ->editColumn('weight',function($query){
            return '<div id="weight-'.$query->id.'">'.number_format(($query->roll_weight/1000), 2, '.', ' ').'</div>';
        })
        ->escapeColumns([])
        ->make(true);
        
    }
    
    
    public function indexParts()
    {
        $query = $this->printer
        ->with('category')
        ->with('parts')
        ->select('printer.*')
        ->where('neighborhood_factory_id',Auth::guard('web')->user()->factory_id)
        ->where('available',1)
        //->orderBy('id','asc')
        ->get();
        $printerMatrix = array();
        
        $cont_max = 0;
        foreach ($query as $printer){
            $cont = 0;
            $printerArray = array();
            array_push($printerArray,$printer->code); 
            $printer->parts->sortByDesc('start_datetime');
            foreach ($printer->parts as $part){
                array_push($printerArray,$part);
                $cont++;
            }
            array_push($printerMatrix,$printerArray);
            if ($cont > $cont_max){
                $cont_max = $cont;
            }
        }
        /*
        foreach ($printerMatrix as $printer){
            foreach ($printer as $key => $value){
                if ($key == 0){
                    echo $value;
                }else{
                    echo '['.$value->start_datetime.' - '.$value->end_datetime.']'.' Part: '.$value->id;
                }
                
            }
            echo '<br>';
        }
        */
        return view('admin.printers.index_parts', compact('printerMatrix', 'cont_max'));
    }
    
    
    public function create()
    {
        $view = View::make('admin.printers.edit')
        ->with('printer', $this->printer)
        //->with('language', $language)
        ->with('locale', $this->locale->create())
        ->with('seo', $this->locale_slug_seo->create())
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
        ]);
    }
    
    public function store(PrinterRequest $request)
    {
        $message = "";

        
        switch (request('action')) {
            case 'change_roll':
                $printer = Printer::updateOrCreate([
                'id' => request('id')],[
                //'roll_end_datetime' => request('end'),
                'roll_replacement_datetime' => request('datetime_replace'),
                'roll_weight' => request('weight'),
                //'code' => request('code'),
                ]);
                break;
            case 'available':
                $printer = Printer::updateOrCreate([
                    'id' => request('id')],[
                    'available' => request('available')=='true' ? 1 : 0,
                ]);
                $parts_unassigned = Part::where('printer_id',request('id'))
                             ->where('state_id',8)
                             ->get();
                $parts_assigned = Part::where('printer_id',request('id'))
                             ->where('state_id',9)
                             ->get();

               if (($parts_unassigned->count() == 0) &&( $parts_assigned->count() !=0 )){
                    if ((request('available-radio') == 'unassing') && (request('available') != 'true')){
                        Part::where('printer_id',request('id'))
                            ->where('state_id','9')
                            //Filtro Order_id
                            ->update (['printer_id'=>NULL,'start_datetime'=>NULL,'end_datetime'=>NULL,'state_id'=>8,'initiated'=>0]);
                    }elseif ((request('available-radio') == 'stop') && (request('available') != 'true')){
                        Part::where('printer_id',request('id'))
                            ->where('state_id','9')
                            ->where('inititated','0')
                            //Filtro Order_id
                             ->update (['printer_id'=>NULL,'start_datetime'=>NULL,'end_datetime'=>NULL,'state_id'=>8,'initiated'=>0]);
                    }
                    $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(), $this->start_date_time);
                    $printersMatrix->chargePrintersMatrix();
                    $printersMatrix->assignPrintersSlots();
                    $printersMatrix->updateAssignPrintersMatrix('saveAssign');
                    
                    $printersMatrix->filamentCalculations();
                }
                
                break;
            case 'disable': //Esta opción es en  el caso que la impresora no tenga piezas asignadas y esté disponible.
                $printer = Printer::updateOrCreate([
                'id' => request('id')],[
                'available' => request('available')=='true' ? 1 : 0,
                ]);
                
                break;
            default:
                $printer = Printer::updateOrCreate([
                'id' => request('id')],[
                'code' => request('code'),
                'category_id' => request('categoria_id'),
                'available' => request('available') ? 1 : 0,
                'roll_end_datetime' => request('roll_end_datetime'),
                'roll_replacement_datetime' => request('roll_replacement_datetime'),
                'roll_weight' => request('roll_weight'),
                'orden' => request('orden'),
                'neighborhood_factory_id' => Auth::guard('web')->user()->factory_id
                ]);
                $locale = $this->locale->store(request('locale'), $printer->id);
        }
        
        
        $printer->touch();
        
        if (request('id')){
            $message = "Printer updated ". $printer->nombre;
        }else{
            $message = "Printer created ". $printer->nombre;
        }
        
        switch (request('action')) {
            case 'change_roll':
                return response()->json([
                'id' => request('id'),
                'roll_replacement_datetime' => request('datetime_replace'),
                'roll_weight' => number_format(( request('weight')/1000), 2, '.', ' '),
                ]);
            case 'available':
                return response()->json([
                'id' => request('id'),
                'available' => request('available'),
                'available_radio' => request('available-radio'),
                ]);
                break;
            case 'disable':
                return response()->json([
                'id' => request('id'),
                'available' => request('available'),
                'available_radio' => request('available-radio'),
                ]);
                break;
            default:
                $view = View::make('admin.printers.edit')
                ->with('category')
                ->with('printer', $printer)
                ->with('locale', $locale)
                ->renderSections();
                
                return response()->json([
                    'layout' => $view['content'],
                    'form' => $view['form'],
                    'id' => $printer->id,
                    'message' => $message,
                ]);
        }

    }
    
    public function show(Printer $printer)
    {

        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        
        $locale = $this->locale->show($printer->id);
        $locale_slug_seo = $this->locale_slug_seo->show($printer->id);
        
        $view = View::make('admin.printers.edit')
        ->with('category')
        ->with('printer', $printer)
        ->with('locale', $locale)
        ->with('seo', $locale_slug_seo);
        
        if(request()->ajax()) {
            $sections = $view->renderSections();
            
            return response()->json([
                'layout' => $sections['content'],
                'form' => $sections['form'],
            ]);
        }
        
        return $view;
    }
    
    public function destroy(Printer $printer)
    {
        $printer->delete();
        $this->locale->delete($printer->id);
        
        $message = "Printer borrado ". $printer->code;
        
        $view = View::make('admin.printers.index')
        ->with('printers', $this->printer->get())
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'view' => $view['table'],
            'message' => $message,
        ]);
    }
    
    public function reorderTable(Request $request)
    {
        $order = request('orden');
        
        if (is_array($order)) {
            foreach ($order as $index => $tableItem) {
                $item = $this->printer->findOrFail($tableItem);
                $item->orden = $index + 1;
                $item->save();
            }
        }
    }
}
