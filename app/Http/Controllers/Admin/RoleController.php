<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Vendor\Caffeinated\Shinobi\Models\Role;
use Yajra\Datatables\Datatables;
use App\Models\DB\PermissionRole;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserRoleRequest;

class RoleController extends Controller
{
    protected $permissions  = ['ver.roles', 'edit.roles'];
    protected $role;
    protected $datatables;
    
    public function __construct(Role $role, Datatables $datatables)
    {
        $this->role = $role;
        $this->datatables = $datatables;
        
        $this->role->activo = 1;
    }

    public function index()
    {
        if (! Auth::guard('web')->user()->isSaizfact()) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        return view('admin.roles.index');
    }

    public function indexJson()
    {
        $query = $this->role
        ->with('permissions')
        ->select('t_role.*');

        return $this->datatables->of($query)->toJson();
    }

    public function create()
    {
        $view = View::make('admin.roles.edit')
        ->with('role', $this->role)
        ->with('permissions_assigned', $this->role->permissions->pluck('id')->all())
        ->renderSections();

        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
        ]);
    }

    public function store(UserRoleRequest $request)
    {
        $role = Role::updateOrCreate([
            'id' => request('id')],[
            'name' => request('name'),
            'activo' => request('activo') ? 1 : 0,
            'slug' => request('slug'),
            'description' => request('description'),
            'special' => request('special')
        ]);

        $role->revokeAllPermissions();
        if(request('permission_id') != null){
            foreach (request('permission_id') as $key => $value) {
                
                PermissionRole::create([
                    'permission_id' => $value, 
                    'role_id'  => $role->id 
                ]);
            }
        }

        if (request('id')){
            $message = "Rol actualizado ". $role->name;
        }else{
            $message = "Rol creado ". $role->name;
        }

        $view = View::make('admin.roles.edit')
        ->with('role', $role)
        ->with('permissions_assigned', $role->permissions->pluck('id')->all())
        ->renderSections();        

        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
            'id' => $role->id,
            'message' => $message,
        ]);
    }

    public function show(Role $role)
    {
        if (! Auth::guard('web')->user()->isSaizfact()) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        
        $view = View::make('admin.roles.edit')
        ->with('role', $role)
        ->with('permissions_assigned', $role->permissions->pluck('id')->all());
    
        if(request()->ajax()) {
            $sections = $view->renderSections(); 
    
            return response()->json([
                'layout' => $sections['content'],
                'form' => $sections['form'],
            ]); 
        }
                
        return $view;
    }

    public function destroy(Role $role)
    {
        $role->delete();

        $message = "Rol borrado ". $role->name;

        $view = View::make('admin.roles.index')
        ->with('roles', $this->role->get())
        ->with('permissions_assigned', $role->permissions->pluck('id')->all())
        ->renderSections();    
        
        return response()->json([
            'layout' => $view['content'],
            'view' => $view['table'],
            'message' => $message,
        ]);
    }
}
