<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\PartRequest;

use App\Models\DB\Part;
use App\Models\DB\PartLog;
use App\Models\DB\Language;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Yajra\Datatables\Datatables;
use Exception;

use App\Vendor\Saizfact\Locale\Locale;

use App\Vendor\Greeny\Main\PrintersMatrix;
use App\Vendor\Greeny\Main\PrinterTimeline;
use App\Vendor\Greeny\Main\PrinterSlot;
use App\Vendor\Greeny\Main\PrinterState;
use App\Vendor\Greeny\Main\DateTimeG;
use App\Vendor\Greeny\Main\DataBase;

use Illuminate\Support\Facades\View;
use App\Models\DB\Employee;
use App\Models\DB\Incident;


class PartController extends Controller
{    
    protected $permissions  = ['ver.parts', 'edit.parts'];
    protected $part;
    protected $language;
    protected $locale;
    protected $locale_categorias;

    protected $datatables;
    protected $start_date_time;
    
    
    
    
    
    function __construct(Part $part,Language $language, Locale $locale, Datatables $datatables,Locale $locale_categorias)
    {
        $this->middleware('auth');
        
        $this->part = $part;
        $this->language = $language;
        
        $this->locale = $locale;
        $this->locale_categorias = $locale_categorias;
        
        $this->datatables = $datatables;
        
        $this->locale->setParent('orders.form');
        
        $this->start_date_time = new DateTimeG ();
        //session(['start_date_time' => $this->start_date_time]);
        
        
    }
    
    
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        session(['working_days' => conf_working_days()]);
        return view('admin.parts.index');
    }
    
    
    public function indexJson()
    {
        /*
        if (session('start_date_time') != null){
            $this->start_date_time = session('start_date_time');
        } else {
            $this->start_date_time = new DateTimeG ();
        }
        */
        $this->start_date_time = new DateTimeG ();
        
        
        $query = $this->part
        ->with (['order' => fn ($query) =>$query->where('neighborhood_id',Auth::guard('web')->user()->factory_id)])
        ->whereHas
            ('order', 
             fn ($query) => $query->where('neighborhood_id',Auth::guard('web')->user()->factory_id)
            )
        ->with('file')
        ->with('printer')
        ->with('state')
        ->select('part.*')
        ->orderBy('start_datetime','asc')
        ;
        return $this->datatables->of($query)
            ->editColumn('printer.code',function($query){
                if (isset($query->printer_id)){
                    return $query->printer->code;
                }else{
                    return "";
                }
            })
            ->editColumn('initiated',function($query){
                if ($query->initiated == 1 ){
                    return "Yes";
                }else{
                    return "No";
                }
            })
            ->editColumn('state_id',function($query){
                switch ($query->state_id) {
                    case '8':
                        return "0%";
                        break;
                    case '9':
                        if ($this->start_date_time > $query->start_datetime){
                            
                            $total =strtotime( $query->end_datetime) - strtotime( $query->start_datetime);
                            $printed = strtotime( $this->start_date_time) -strtotime($query->start_datetime);
                            $percent = 100*$printed/$total;
                            if ($percent <= 100){
                                return number_format($percent,2)."%";
                            }else{
                                return "100%";
                            }
                        }else{
                            return "0%";
                        }
                        
                        break;
                    case '10':
                        return "100%";
                        break;
                }
                
            })
            ->make(true);
    }
    
    
    public function graph()
    {
        //
        set_time_limit(120);
        ini_set("memory_limit","256M");
        
        session(['working_days' => conf_working_days()]);
        
        
        if (count(session('working_days'))<7){
            $view = View::make('admin.parts.index')
            ->with('message' , 'Before assigning parts you must configure the working hours.')
            ;
            return $view;
        }
       
        $neighborhood_factory_id = conf_neighborhood_factory_id();
        /*
        if (session('start_date_time') != null){
            $this->start_date_time = session('start_date_time');
        }
        */
        
        $this->start_date_time = new DateTimeG ();
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        
        $printersMatrix = new PrintersMatrix($neighborhood_factory_id, $this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        $printersMatrix->assignPrintersSlots();
        
        $printersMatrix->filamentCalculations();
        
        //Comprobamos si notificaciones de fin de filamento y asignamos los estilos
        //Para ello hay que calcular una Printer Matrix auxiliar en la que cargamos todo los consumos desde que se cambió el roll
        foreach ($printersMatrix as $PrinterLine){
            //Calculo el DateTime en que se empieza a dibujar la regla (marcas de tiempo)
           
            $start_workday = new DateTimeG($printersMatrix->start_date_time_print->format('Y-m-d H:i:s'));
            if ((intval($start_workday->format('H')) < intval(conf_workday($start_workday)['from']['hour']))) {
                $start_workday->modify('-1 days');
            }
            $start_workday->setTime(conf_workday($start_workday)['from']['hour'], conf_workday($start_workday)['from']['minute']);
            
            usort($PrinterLine->rolls_end, 'sort_by_datetime');
            
            
            $printerTimeLineAux = new PrinterTimeline($PrinterLine->printer_id,
                $PrinterLine->code_printer,
                $PrinterLine->roll_weight,
                $PrinterLine->roll_replacement_datetime);
            
            $printerTimeLineAux->chargePrinterTimeline($PrinterLine->roll_replacement_datetime,$this->start_date_time,conf_neighborhood_factory_id());
            //$printerTimeLineAux->chargePrinterTimeline($this->start_date_time_print);
            //$this->assignPrintersSlots();
            
            //Jose 20/05/2022 Análisis
            $printerTimeLineAux->filamentCalculations ($PrinterLine->roll_replacement_datetime);
            
            //$printerTimeLineAux->filamentCalculations ($this->start_date_time_print);
            
            
            if (count($printerTimeLineAux) == 0){
                $printerTimeLineAux = $PrinterLine;
            }
            
            //Asigno esatilos
            $color="";
            $blink_class =  "";
            foreach ($printerTimeLineAux->rolls_end as $roll_end){
                $diff = $roll_end['datetime']->diffRangeHM($this->start_date_time);
                $color="";
                $blink_class =  "";
                if ($diff['hours']<10){
                    $color =  "red";
                }
                if (($diff['hours']<1)&& ($diff['minutes']<30)){
                    $blink_class =  "blink";
                }
                $PrinterLine->color= $color;
                $PrinterLine->blink_class= $blink_class;
                break;
            }
        }
        
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        
        $view = View::make('admin.parts.graph')
        ->with('printersMatrix', $printersMatrix)
        ->with('start_date_time', $this->start_date_time)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max);
        
        return $view;
    }
    public function state(Request $request)
    {
        //
        
        
        set_time_limit(120);
	ini_set("memory_limit","256M");
        
        $this->start_date_time =  new DateTimeG(request('start-date-time'));
        //$this->start_date_time = new DateTimeG ();
        session(['working_days' => conf_working_days()]);
        
        if (count(session('working_days'))<7){
            $view = View::make('admin.parts.index')
            ->with('message' , 'Before assigning parts you must configure the working hours.')
            ;
            return $view;
        }
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        
        $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(), $this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        $printersMatrix->assignPrintersSlots();
        
        $printersMatrix->filamentCalculations();
        
        $rows = 2;
        if (request('left_to_right_direction') == 'false'){
            session(['left_to_right_direction' => false]);
        }else if (request('left_to_right_direction') == 'true'){
            session(['left_to_right_direction' => true]);
        }if ((request('left_to_right_direction') == '')&&(session('left_to_right_direction')==null)){
            session(['left_to_right_direction' => false]);
        }
        
        
        $reverse = !session('left_to_right_direction');
        $state_printer_matrix = $printersMatrix->createStatePrintersMatrix($reverse ,$rows);//Default parameters: reverse = true, rows = 3
        /*
        for ($i=0;$i<$rows;$i++){
            for ($j=0;$j<=$printersMatrix->columns($rows);$j++){
                if (array_key_exists($j,$state_printer_matrix[$i])){
                    echo( $state_printer_matrix[$i][$j]->printer_id.',');
                    
                }
            }
        }
        */
        
        
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        $factory_employees = Employee::factoryEmployees(conf_neighborhood_factory_id())->get();
        
        

        
        $view = View::make('admin.parts.state')
        ->with('printersMatrix', $printersMatrix)
        ->with('statePrinterMatrix', $state_printer_matrix)
        ->with('rows', $rows)
        ->with('columns', $printersMatrix->columns($rows))
        ->with('start_date_time', $this->start_date_time)
        ->with('start_date_time_print',$printersMatrix->start_date_time_print)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max)
        ->with('factory_employees',$factory_employees);
        //return $view;
        
        if (request('left_to_right_direction')==''){
            return $view;
        }else{
            $view= $view->renderSections();
            //return $view;
            return response()->json([
                'layout' => $view['content'],
                'reverse'=> $reverse,
            ]);
        }
    }
    
    public function showState(Request $request)
    {
        //
        set_time_limit(120);
        
        session(['working_days' => conf_working_days()]);
        
        $this->start_date_time =  new DateTimeG(request('start-date-time'));
        //$this->start_date_time = new DateTimeG ();
        
        if (count(session('working_days'))<7){
            $view = View::make('admin.parts.index')
            ->with('message' , 'Before assigning parts you must configure the working hours.')
            ;
            return $view;
        }
        
        $neighborhood_factory_id = conf_neighborhood_factory_id();
        /*
         if (session('start_date_time') != null){
         $this->start_date_time = session('start_date_time');
         }
         */
        
        
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        
        $printersMatrix = new PrintersMatrix($neighborhood_factory_id, $this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        //$printersMatrix->assignPrintersSlots();
        
        $printersMatrix->filamentCalculations();
        
        //Comprobamos si notificaciones de fin de filamento y asignamos los estilos
        //Para ello hay que calcular una Printer Matrix auxiliar en la que cargamos todo los consumos desde que se cambió el roll
        $state_printer_matrix = array();
        
        $reverse =true;
        $rows = 2; //En realidad son 3 filas
        $columns = ($printersMatrix->count()/($rows+1))-1;
        if (($printersMatrix->count() % ($rows+1))  != 0){
            $columns = intdiv($printersMatrix->count(),$rows+1);
        }
        $cont = 1;
        $row=0;
        $column=0;
        
        if ($reverse){
            $column=$columns;
        }
        /*
         var_dump($printersMatrix->count());
         die;
         */
        foreach ($printersMatrix as $PrinterLine){
            
            //Calculo el DateTime en que se empieza a dibujar la regla (marcas de tiempo)
            
            $start_workday = new DateTimeG($printersMatrix->start_date_time_print->format('Y-m-d H:i:s'));
            if ((intval($start_workday->format('H')) < intval(conf_workday($start_workday)['from']['hour']))) {
                $start_workday->modify('-1 days');
            }
            $start_workday->setTime(conf_workday($start_workday)['from']['hour'], conf_workday($start_workday)['from']['minute']);
            
            usort($PrinterLine->rolls_end, 'sort_by_datetime');
            
            
            $printerTimeLineAux = new PrinterTimeline($PrinterLine->printer_id,
                $PrinterLine->code_printer,
                $PrinterLine->roll_weight,
                $PrinterLine->roll_replacement_datetime);
            
            $printerTimeLineAux->chargePrinterTimeline($PrinterLine->roll_replacement_datetime,$this->start_date_time,conf_neighborhood_factory_id());
            //$printerTimeLineAux->chargePrinterTimeline($this->start_date_time_print);
            //$this->assignPrintersSlots();
            
            //TODO Jose 20/05/2022 Análisis
            $printerTimeLineAux->filamentCalculations ($PrinterLine->roll_replacement_datetime);
            
            //$printerTimeLineAux->filamentCalculations ($this->start_date_time_print);
            
            
            if (count($printerTimeLineAux) == 0){
                $printerTimeLineAux = $PrinterLine;
            }
            
            //Asigno esatilos
            $color="";
            $blink_class =  "";
            foreach ($printerTimeLineAux->rolls_end as $roll_end){
                $diff = $roll_end['datetime']->diffRangeHM($this->start_date_time);
                $color="";
                $blink_class =  "";
                if ($diff['hours']<10){
                    $color =  "red";
                }
                if (($diff['hours']<1)&& ($diff['minutes']<30)){
                    $blink_class =  "blink";
                }
                $PrinterLine->color= $color;
                $PrinterLine->blink_class= $blink_class;
                break;
            }
            
            $printerState = new PrinterState($PrinterLine->printer_id, $PrinterLine->code_printer, $PrinterLine->roll_weight, $PrinterLine->roll_replacement_datetime,$PrinterLine->rolls_end);
            
            $is_next_slot = false;
            $is_current_slot = false;
            
            
            foreach ($PrinterLine as $printer_slot){
                $slot_start = new DateTimeG($printer_slot->start);
                $slot_end = new DateTimeG($printer_slot->end);
                
                if  (
                    (($printer_slot->state == 9)||($printer_slot->state == 10))
                    &&
                    ($printer_slot->initiated == 1)
                    &&
                    ($this->start_date_time >= $slot_start)
                    
                    ){
                        $printerState->current_slot = $printer_slot;
                        $is_current_slot = true;
                }elseif (
                    (($printer_slot->state == 9))
                    &&
                    ($printer_slot->initiated == 1)
                    &&
                    ($this->start_date_time < $slot_start)
                    
                    ){
                        //Esta situación no debería ocurrir si no se ha usado set current time
                        $printerState->current_slot = $printer_slot;
                        $is_current_slot = true;
                }elseif (
                    ($printer_slot->state == 9)
                    &&
                    ($printer_slot->initiated == 0)
                    
                    &&
                    (!$is_current_slot)
                    ){
                        $printerState->current_slot = $printer_slot;
                        $is_current_slot = true;
                }elseif ($is_current_slot && !$is_next_slot){
                    $printerState->next_slot = $printer_slot;
                    $is_next_slot = true;
                } elseif ($is_current_slot && $is_next_slot){
                    break;
                }
            }
            
            $light = "green";
            $current_start = new DateTimeG($printerState->current_slot->start);
            $current_end = new DateTimeG($printerState->current_slot->end);
            $next_start = new DateTimeG($printerState->current_slot->start);
            
            $current_end_minus_10 = new DateTimeG($printerState->current_slot->end);
            $current_end_minus_10->modify('-10 minutes');
            
            if ($printerState->current_slot->initiated == 0){
                $light ="red";
            }elseif (( $this->start_date_time >= $current_start )&&($this->start_date_time <= $current_end)){
                if (($this->start_date_time >= $current_end_minus_10) && ($this->start_date_time <= $current_end)){
                    $light = "blue";
                }else{
                    $light="green";
                }
            }else{
                if (($this->start_date_time > $current_end) && ($this->start_date_time < $next_start)){
                    $light ="red";
                }
            }
            
            $printerState->lamp = $light;
            
            
            $state_printer_matrix[$row][$column] = $printerState;
            //$state_printer_matrix[$row][$column] = $cont;
            if ($cont%($rows+1) == 0){
                if ($reverse){
                    $column --;
                }else{
                    $column ++;
                }
            }
            
            if ($row == $rows){
                $row = 0;
            }else{
                $row ++;
            }
            
            $cont ++;
            
        }
        //var_dump($state_printer_matrix);
        //die;
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        
        /*
         for ($i=0;$i<=$rows;$i++){
         for ($j=0;$j<=$columns;$j++){
         if (array_key_exists($j,$state_printer_matrix[$i])){
         $state_printer_matrix[$i][$j];
         }
         }
         }
         */
        
        
        
        $view = View::make('admin.parts.state')
        ->with('printersMatrix', $printersMatrix)
        ->with('statePrinterMatrix', $state_printer_matrix)
        ->with('rows', $rows)
        ->with('columns', $columns)
        ->with('start_date_time', $this->start_date_time)
        ->with('start_date_time_print',$printersMatrix->start_date_time_print)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max);
        
        return $view;
    }
    
    
    public function saveAssign()
    {
        /*
        if (session('start_date_time') != null){
            $this->start_date_time = session('start_date_time');
        }
        */
        set_time_limit(120);
        ini_set("memory_limit","256M");
        $this->start_date_time = new DateTimeG ();
        
        session(['working_days' => conf_working_days()]);
        set_time_limit(0);
        
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        
        $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(), $this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        $printersMatrix->assignPrintersSlots();
        $printersMatrix->updateAssignPrintersMatrix('saveAssign');
        
        $printersMatrix->filamentCalculations();
        
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        
        $view = View::make('admin.parts.graph')
        ->with('printersMatrix', $printersMatrix)
        ->with('start_date_time', $this->start_date_time)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max);
        
        return $view;
    }
    
    public function clearAssign()
    {
        /*
        if (session('start_date_time') != null){
            $this->start_date_time = session('start_date_time');
        }
        */
        $this->start_date_time = new DateTimeG ();
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        
        $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(), $this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        $printersMatrix->clearAssignPrintersMatrix();
        
        $printersMatrix->assignPrintersSlots();
        
        $printersMatrix->filamentCalculations();
        
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        
        $view = View::make('admin.parts.graph')
        ->with('printersMatrix', $printersMatrix)
        ->with('start_date_time', $this->start_date_time)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max);
        
        return $view;
    }
    
    public function playStatePost(Request $request)
    //public function playState($start_date_time_txt, $printer_id,$part_id,$state_id)
    {
       
        /*
        if (session('start_date_time') != null){
            $this->start_date_time = session('start_date_time');
        } else {
            $this->start_date_time = new DateTimeG ($start_date_time_txt);
        }
        */
        
        
        try {
            $printer_id = "";
            if (request('printer_id') != "") {
                $printer_id =  request('printer_id');
            }
            if (request('printer_id_post') != "") {
                $printer_id =  request('printer_id_post');
            }
           
            if ((request('current_action')=="play") /*|| (request('current_action') == "printed") */|| (request('current_action') == "last_slot_out_range")){
                $part_id =  request('current_part_id');
                $state_id =  request('current_state_id');
            }else{
            //Es el play de next part
                if (request('next_part_id') == ""){
                    $part_id =  request('current_part_id');
                    $state_id =  request('current_state_id');
                }else{
                    $part_id =  request('next_part_id');
                    $state_id =  request('next_state_id');
                }
            }
            
            
            //$action =  request('action');
            $action = 'play';
            
            $this->start_date_time = new DateTimeG ();
            
            //Buscar la pieza anterior
    
            $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
            //$min = new DateTime($now_datetime);
            $min->modify('-1 years');
            
            $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
            //$max = new DateTime($start_date_time->date);
            $max->modify('+10 years');
            
            $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(), $this->start_date_time);
            $printersMatrix->chargePrintersMatrix();
            
            
            $PartLog = $this->insertPartLog($printersMatrix,$action,$printer_id,$part_id);
            
            
            $sql = DataBase::querySelectReassignPart($part_id,conf_neighborhood_factory_id());
            $pending_part = DataBase::getConnection()->query($sql)->fetch_assoc();
            
            $end_new = $this->start_date_time->estimatedPrintingEnd($pending_part);
            
            
            //Actualizaciones
            $printer_timeline = $printersMatrix->getPrinter($printer_id);
            
            if  (request('current_action')!="play"){
                $printer_timeline->setPrintedCurrentPrintingPart(request('final_hours'),request('final_minutes'));
            }
            if ( 
                ((request('current_action') == "play")&&(request('current_part_id') == $part_id))
                ||
                ((request('next_action') == "play")&&(request('next_part_id') == $part_id))
               ){
                $printer_timeline->setInitiatedPrintedNextPrintingPart($part_id, $this->start_date_time, $end_new);
            }
            $printersMatrix->reassignPartsAll($this->start_date_time,'play', conf_neighborhood_factory_id(),$part_id);
            $printersMatrix->filamentCalculations();
            
            $rows = 2;
            $state_printer_matrix = $printersMatrix->createStatePrintersMatrix(true,$rows);//Default parameters: reverse = true, rows = 3 
            
            $next_part = $printersMatrix->nextPart();
            $summary = $printersMatrix->summary();
            
            $factory_employees = Employee::factoryEmployees(conf_neighborhood_factory_id())->get();
            
            $this->insertIncident($request,$PartLog);
            
            $view = View::make('admin.parts.state')
            ->with('printersMatrix', $printersMatrix)
            ->with('statePrinterMatrix', $state_printer_matrix)
            ->with('rows', $rows)
            ->with('columns', $printersMatrix->columns($rows))
            ->with('start_date_time', $this->start_date_time)
            ->with('start_date_time_print',$printersMatrix->start_date_time_print)
            ->with('next_part', $next_part)
            ->with('summary', $summary)
            ->with('min', $min)
            ->with('max', $max)
            ->with('factory_employees',$factory_employees)
            ->renderSections();
            //return $view;
            return response()->json([
                'layout' => $view['content'],
            ]);
        }catch (Exception $e){
            //echo ('Excepción capturada: '.  $e->getMessage(). "\n");
            return response()->json([
                'error' => 'Excepción capturada: '.  $e->getMessage(). "\n",
            ]);
        };
        
    }
    
    
    public function playStateGet($start_date_time_txt, $printer_id,$part_id,$state_id)
    {
        
        /*
         if (session('start_date_time') != null){
         $this->start_date_time = session('start_date_time');
         } else {
         $this->start_date_time = new DateTimeG ($start_date_time_txt);
         }
         */
        
        
        
        
        $this->start_date_time = new DateTimeG ();
        
        //Buscar la pieza anterior
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        
        $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(), $this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        
        foreach ($printersMatrix as $printerTimeline){
            foreach ($printerTimeline as $printerSlot){
                $partLog = new PartLog();
                
                $partLog->part_id = $printerSlot->part;
                $partLog->file_id = $printerSlot->file;
                $partLog->file_part_name = $printerSlot->file_part_name;
                $partLog->version_part_name = $printerSlot->version_part_name;
                $partLog->order_id = $printerSlot->order;
                $partLog->final_product_id = $printerSlot->final_product;
                //$partLog->filament_id = $printerSlot->filament_id;
                $partLog->printer_id = $printerTimeline->printer_id;
                $partLog->state_id = $printerSlot->state;
                $partLog->name = $printerSlot->file_name;
                $partLog->start_datetime = $printerSlot->start;
                $partLog->end_datetime = $printerSlot->end;
                $partLog->weight = $printerSlot->weight;
                $partLog->initiated = $printerSlot->initiated;
                $partLog->action = "'Play' Part:". $part_id.". printer:". $printer_id.".";
                
                $partLog->save();
            }
            
        }
        
        
        
        $sql = DataBase::querySelectReassignPart($part_id,conf_neighborhood_factory_id());
        $pending_part = DataBase::getConnection()->query($sql)->fetch_assoc();
        
        $end_new = $this->start_date_time->estimatedPrintingEnd($pending_part);
        
        //Inicio al pieza
        //Actualizo bdd
        Part::where('id', $part_id)
        ->update([
            'printer_id' => $printer_id,
            'state_id'=> $state_id,
            'start_datetime' => $this->start_date_time->format('Y-m-d H:i:s'),
            'end_datetime' => $end_new->format('Y-m-d H:i:s'),
            'initiated' => 1]);
        //Actualizo memoria
        $printer_timeline = $printersMatrix->getPrinter($printer_id);
        $printer_timeline->setPrintedCurrentPrintingPart();
        
        
        
        //Primero intentamos desplazar todas la piezas de la impresora hacia la derecha. Teniendo en cuenta las noches.
        //Si el rendimiento no  es aceptable in
        $printersMatrix->reassignPartsAll($this->start_date_time,'play', conf_neighborhood_factory_id(),$part_id);
        
        $printersMatrix->filamentCalculations();
        
        
        
        
        
        
        
        
        
        
        
        
        
        $state_printer_matrix = array();
        
        $reverse =true;
        $rows = 2; //En realidad son 3 filas
        $columns = ($printersMatrix->count()/($rows+1))-1;
        if (($printersMatrix->count() % ($rows+1))  != 0){
            $columns = intdiv($printersMatrix->count(),$rows+1);
        }
        $cont = 1;
        $row=0;
        $column=0;
        
        if ($reverse){
            $column=$columns;
        }
        
        foreach ($printersMatrix as $PrinterLine){
            $printerState = new PrinterState($PrinterLine->printer_id, $PrinterLine->code_printer, $PrinterLine->roll_weight, $PrinterLine->roll_replacement_datetime,$PrinterLine->rolls_end);
            
            $is_next_slot = false;
            $is_current_slot = false;
            
            
            foreach ($PrinterLine as $printer_slot){
                $slot_start = new DateTimeG($printer_slot->start);
                $slot_end = new DateTimeG($printer_slot->end);
                
                if  (
                    (($printer_slot->state == 9)||($printer_slot->state == 10))
                    &&
                    ($printer_slot->initiated == 1)
                    &&
                    ($this->start_date_time >= $slot_start)
                    
                    ){
                        $printerState->current_slot = $printer_slot;
                        $is_current_slot = true;
                }elseif (
                    (($printer_slot->state == 9))
                    &&
                    ($printer_slot->initiated == 1)
                    &&
                    ($this->start_date_time < $slot_start)
                    
                    ){
                        //Esta situación no debería ocurrir si no se ha usado set current time
                        $printerState->current_slot = $printer_slot;
                        $is_current_slot = true;
                }elseif (
                    ($printer_slot->state == 9)
                    &&
                    ($printer_slot->initiated == 0)
                    
                    &&
                    (!$is_current_slot)
                    ){
                        $printerState->current_slot = $printer_slot;
                        $is_current_slot = true;
                }elseif ($is_current_slot && !$is_next_slot){
                    $printerState->next_slot = $printer_slot;
                    $is_next_slot = true;
                } elseif ($is_current_slot && $is_next_slot){
                    break;
                }
            }
            
            $light = "red";
            if (isset($printerState->current_slot->start) && ($printerState->current_slot->start != null)){
                $current_start = new DateTimeG($printerState->current_slot->start);
                $current_end = new DateTimeG($printerState->current_slot->end);
                
                $current_end_minus_10 = new DateTimeG($printerState->current_slot->end);
                $current_end_minus_10->modify('-10 minutes');
                
                if ($printerState->current_slot->initiated == 0){
                    $light ="red";
                }elseif (( $this->start_date_time >= $current_start )&&($this->start_date_time <= $current_end)){
                    if (($this->start_date_time >= $current_end_minus_10) && ($this->start_date_time <= $current_end)){
                        $light = "blue";
                    }else{
                        $light="green";
                    }
                }else{
                    if (($this->start_date_time > $current_end) && ($this->start_date_time < $next_start)){
                        $light ="red";
                    }
                }
                
                $printerState->lamp = $light;
                
                $printerState->current_time_printing = $current_start->diffRangeHM($this->start_date_time);
                $printerState->current_time_left = $this->start_date_time->diffRangeHM($current_end);
                $current_time_total_minutes = $current_start->diffMinutes($current_end);
                $current_time_printing_minutes = $current_start->diffMinutes($this->start_date_time);
                $printerState->current_percent_printing = number_format($current_time_printing_minutes * 100/$current_time_total_minutes, 2, ',','.');
                
                $slot_interval_last_current = $current_start->diff($current_end);
                $slot_total_hours_real_current = $slot_interval_last_current->format('%d') * 24 + $slot_interval_last_current->format('%H');
                
                $printerState->current_days = $slot_interval_last_current->format('%d');
                $printerState->current_time = $slot_total_hours_real_current. ':' . $slot_interval_last_current->format('%i');
                
                
                $printerState->current_start_compact = $current_start->format('H:i - d F ');
                $printerState->current_end_compact = $current_end->format('H:i - d F');
                
                
                $printerState->current_button = "printer";
                if ($printerState->current_slot->state == 9){
                    if ($printerState->current_slot->initiated == 0){
                        $printerState->current_button = "play";
                    }else{
                        $printerState->current_button = "cancel";
                    }
                }
                
                $printerState->current_action = "printed";
                if ($printerState->current_slot->state == 9){
                    if ($printerState->current_slot->initiated == 0){
                        $printerState->current_action = "play";
                    }else{
                        $printerState->current_action = "cancel";
                    }
                }
                
            }
            
            
            
            if (isset($printerState->next_slot->start) && ($printerState->next_slot->start != null)){
                $next_start = new DateTimeG($printerState->next_slot->start);
                $next_end = new DateTimeG($printerState->next_slot->end);
                
                $next_end_minus_10 = new DateTimeG($printerState->next_slot->end);
                $next_end_minus_10->modify('-10 minutes');
                
                if ($printerState->next_slot->initiated == 0){
                    $light ="red";
                }elseif (( $this->start_date_time >= $next_start )&&($this->start_date_time <= $next_end)){
                    if (($this->start_date_time >= $next_end_minus_10) && ($this->start_date_time <= $next_end)){
                        $light = "blue";
                    }else{
                        $light="green";
                    }
                }else{
                    if (($this->start_date_time > $next_end) && ($this->start_date_time < $next_start)){
                        $light ="red";
                    }
                }
                
                $printerState->lamp = $light;
                
                $printerState->next_time_printing = $next_start->diffRangeHM($this->start_date_time);
                $printerState->next_time_left = $this->start_date_time->diffRangeHM($next_end);
                $next_time_total_minutes = $next_start->diffMinutes($next_end);
                $next_time_printing_minutes = $next_start->diffMinutes($this->start_date_time);
                $printerState->next_percent_printing = number_format($next_time_printing_minutes * 100/$next_time_total_minutes, 2, ',','.');
                
                $slot_interval_last_next = $next_start->diff($next_end);
                $slot_total_hours_real_next = $slot_interval_last_next->format('%d') * 24 + $slot_interval_last_next->format('%H');
                
                $printerState->next_days = $slot_interval_last_next->format('%d');
                $printerState->next_time = $slot_total_hours_real_next. ':' . $slot_interval_last_next->format('%i');
                
                
                $printerState->next_start_compact = $next_start->format('H:i - d F ');
                $printerState->next_end_compact = $next_end->format('H:i - d F');
                
                
                $printerState->next_button = "printer";
                if ($printerState->next_slot->state == 9){
                    if ($printerState->next_slot->initiated == 0){
                        $printerState->next_button = "play";
                    }else{
                        $printerState->next_button = "cancel";
                    }
                }
                
                $printerState->next_action = "printed";
                if ($printerState->next_slot->state == 9){
                    if ($printerState->next_slot->initiated == 0){
                        $printerState->next_action = "play";
                    }else{
                        $printerState->next_action = "cancel";
                    }
                }
                
            }
            
            
            
            $state_printer_matrix[$row][$column] = $printerState;
            //$state_printer_matrix[$row][$column] = $cont;
            if ($cont%($rows+1) == 0){
                if ($reverse){
                    $column --;
                }else{
                    $column ++;
                }
            }
            
            if ($row == $rows){
                $row = 0;
            }else{
                $row ++;
            }
            
            $cont ++;
            
        }
        
        
        //
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        
        
        
        
        
        
        $factory_employees = Employee::factoryEmployees(conf_neighborhood_factory_id())->get();
        
        
        
        $view = View::make('admin.parts.state')
        ->with('printersMatrix', $printersMatrix)
        ->with('statePrinterMatrix', $state_printer_matrix)
        ->with('rows', $rows)
        ->with('columns', $columns)
        ->with('start_date_time', $this->start_date_time)
        ->with('start_date_time_print',$printersMatrix->start_date_time_print)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max)
        ->with('factory_employees',$factory_employees)
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
        ]);
        
        
        
        
        
    }
    
    
    
    
    
    public function play($start_date_time_txt, $printer_id,$part_id,$state_id)
    {
        
        /*
         if (session('start_date_time') != null){
         $this->start_date_time = session('start_date_time');
         } else {
         $this->start_date_time = new DateTimeG ($start_date_time_txt);
         }
         */
        
        
        
        
        $this->start_date_time = new DateTimeG ();
        
        //Buscar la pieza anterior
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        
        $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(), $this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        
        foreach ($printersMatrix as $printerTimeline){
            foreach ($printerTimeline as $printerSlot){
                $partLog = new PartLog();
                
                $partLog->part_id = $printerSlot->part;
                $partLog->file_id = $printerSlot->file;
                $partLog->file_part_name = $printerSlot->file_part_name;
                $partLog->version_part_name = $printerSlot->version_part_name;
                $partLog->order_id = $printerSlot->order;
                $partLog->final_product_id = $printerSlot->final_product;
                //$partLog->filament_id = $printerSlot->filament_id;
                $partLog->printer_id = $printerTimeline->printer_id;
                $partLog->state_id = $printerSlot->state;
                $partLog->name = $printerSlot->file_name;
                $partLog->start_datetime = $printerSlot->start;
                $partLog->end_datetime = $printerSlot->end;
                $partLog->weight = $printerSlot->weight;
                $partLog->initiated = $printerSlot->initiated;
                $partLog->action = "'Play' Part:". $part_id.". printer:". $printer_id.".";
                
                $partLog->save();
            }
            
        }
        
        
        
        $sql = DataBase::querySelectReassignPart($part_id,conf_neighborhood_factory_id());
        $pending_part = DataBase::getConnection()->query($sql)->fetch_assoc();
        
        $end_new = $this->start_date_time->estimatedPrintingEnd($pending_part);
        
        //Inicio al pieza
        //Actualizo bdd
        Part::where('id', $part_id)
        ->update([
            'printer_id' => $printer_id,
            'state_id'=> $state_id,
            'start_datetime' => $this->start_date_time->format('Y-m-d H:i:s'),
            'end_datetime' => $end_new->format('Y-m-d H:i:s'),
            'initiated' => 1]);
        //Actualizo memoria
        $printer_timeline = $printersMatrix->getPrinter($printer_id);
        $printer_timeline->setPrintedCurrentPrintingPart();
        
        
        
        //Primero intentamos desplazar todas la piezas de la impresora hacia la derecha. Teniendo en cuenta las noches.
        //Si el rendimiento no  es aceptable in
        $printersMatrix->reassignPartsAll($this->start_date_time,'play', conf_neighborhood_factory_id(),$part_id);
        
        $printersMatrix->filamentCalculations();
        
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        
        $view = View::make('admin.parts.graph')
        ->with('printersMatrix', $printersMatrix)
        ->with('start_date_time', $this->start_date_time)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max)
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
        ]);
        
        
        //return redirect()->route('admin_parts_graph');
        //return redirect()->back();
        //return redirect()->action('Admin\PartController@graph');
        //return redirect('/admin/parts/graph');
        
        
        
    }
    
    public function canPlayState(Request $request){
        
        $part_id  = request('part_id');
        
        if ($part_id ==""){
            $part_id =  request('next_part_id');
        }
        
        if ($part_id ==""){
            $part_id =  request('current_part_id');
        }
        
        $this->start_date_time = new DateTimeG ();
        //$this->start_date_time = new DateTimeG (request('start'));
        
        $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(), $this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        
        //$sql = DataBase::querySelectReassignPart($part_id,conf_neighborhood_factory_id());
        //$pending_part = DataBase::getConnection()->query($sql)->fetch_assoc();
        $can_play = true;
        foreach ($printersMatrix as $printerTimeline){
            if (($printerTimeline->printer_id == request('printer_id'))|| ($printerTimeline->printer_id == request('printer_id_post'))){
                
                $printer_slot_aux = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
                $printerTimeline->uasort('startAscendingComparison');
                
                foreach ($printerTimeline as $printerSlot){
                    if ($printerTimeline ->count()==1){
                        /*
                        $aux_end = new DateTimeG($printerSlot->end);
                        if ( $this->start_date_time < $aux_end ){
                            $can_play = false;
                            break 2;
                        }
                        */
                        break 2;
                    }
                    if (($printerSlot->part == $part_id) && ($printer_slot_aux->part != '')){
                        $aux_end = new DateTimeG($printer_slot_aux->end);
                        if ( $this->start_date_time < $aux_end ){
                            $can_play = false;
                            break 2;
                        }
                        
                    }
                    $printer_slot_aux = $printerSlot;
                }
                
            }
        }
        return response()->json(['can_play_state' => $can_play]);
        
    }
    
    public function canPlay(Request $request){
        $this->start_date_time = new DateTimeG ();
        //$this->start_date_time = new DateTimeG (request('start'));
        
        $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(), $this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        
        $sql = DataBase::querySelectReassignPart(request('part_id'),conf_neighborhood_factory_id());
        $pending_part = DataBase::getConnection()->query($sql)->fetch_assoc();
        $can_play = true;
        foreach ($printersMatrix as $printerTimeline){
            if ($printerTimeline->printer_id == request('printer_id')){
                
                $printer_slot_aux = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
                $printerTimeline->uasort('startAscendingComparison');
                
                foreach ($printerTimeline as $printerSlot){
                    if (($printerSlot->part == request('part_id')) && ($printer_slot_aux->part != '')){
                        $aux_end = new DateTimeG($printer_slot_aux->end);
                        if ( $this->start_date_time < $aux_end ){
                            $can_play = false;
                            break 2;
                        }
                        
                    }
                    $printer_slot_aux = $printerSlot;
                }
                
            }
        }
        return response()->json(['can_play' => $can_play]);
    
    }
    
    
    public function cancelStatePost(Request $request)
    {
        
        
        
        $printer_id =  request('printer_id_post');
        $part_id =  request('current_part_id');
        //$state_id =  request('current_state_id');
        //$action =  request('action');
        $action = 'cancel';
        /*
         if (session('start_date_time') != null){
         $this->start_date_time = session('start_date_time');
         } else {
         $this->start_date_time = new DateTimeG ($start_date_time_txt);
         }
         */
        $this->start_date_time = new DateTimeG ();
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        
        $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(),$this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        
        
        
        $PartLog = $this->insertPartLog($printersMatrix,$action,$printer_id,$part_id);
        
        $printersMatrix->reassignPartsAll($this->start_date_time,'cancel', conf_neighborhood_factory_id(),$part_id);
        
        $printersMatrix->filamentCalculations();
        
        $rows = 2;
        $state_printer_matrix = $printersMatrix->createStatePrintersMatrix(true,$rows);//Default parameters: reverse = true, rows = 3 
        
        $this->insertIncident($request,$PartLog);
        
        //echo "<script>window.history.pushState('','','/admin/parts/state');</script>";
        
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        
        $factory_employees = Employee::factoryEmployees(conf_neighborhood_factory_id())->get();
        
        $view = View::make('admin.parts.state')
        ->with('printersMatrix', $printersMatrix)
        ->with('statePrinterMatrix', $state_printer_matrix)
        ->with('rows', $rows)
        ->with('columns', $printersMatrix->columns($rows))
        ->with('start_date_time', $this->start_date_time)
        ->with('start_date_time_print',$printersMatrix->start_date_time_print)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max)
        ->with('factory_employees',$factory_employees)
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
        ]);
    }
    
    
    
    public function cancelState($start_date_time_txt, $part_id,$printer_id)
    {
        /*
        if (session('start_date_time') != null){
            $this->start_date_time = session('start_date_time');
        } else {
            $this->start_date_time = new DateTimeG ($start_date_time_txt);
        }
        */
        $this->start_date_time = new DateTimeG ();
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        
        $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(),$this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        
        
        
        
        foreach ($printersMatrix as $printerTimeline){
            foreach ($printerTimeline as $printerSlot){
                $partLog = new PartLog();
                
                $partLog->part_id = $printerSlot->part;
                $partLog->file_id = $printerSlot->file;
                $partLog->file_part_name = $printerSlot->file_part_name;
                $partLog->version_part_name = $printerSlot->version_part_name;
                $partLog->order_id = $printerSlot->order;
                $partLog->final_product_id = $printerSlot->final_product;
                //$partLog->filament_id = $printerSlot->filament_id;
                $partLog->printer_id = $printerTimeline->printer_id;
                $partLog->state_id = $printerSlot->state;
                $partLog->name = $printerSlot->file_name;
                $partLog->start_datetime = $printerSlot->start;
                $partLog->end_datetime = $printerSlot->end;
                $partLog->weight = $printerSlot->weight;
                $partLog->initiated = $printerSlot->initiated;
                $partLog->action = "'Cancel' Part:". $part_id.". printer:". $printer_id.".";
                $partLog->save();
            }
            
        }
        
        $printersMatrix->reassignPartsAll($this->start_date_time,'cancel', conf_neighborhood_factory_id(),$part_id);
        /*
        Part::where('id', $part_id)
            ->update(['initiated' => 0]);
        
        $printer_timeline = $printersMatrix->getPrinter($printer_id);
        $printer_timeline->setPartAsNotInitiated($part_id);
        */
        
        
        $printersMatrix->filamentCalculations();
        
        
        
        
        
        
        
        
        
        
        $state_printer_matrix = array();
        
        $reverse =true;
        $rows = 2; //En realidad son 3 filas
        $columns = ($printersMatrix->count()/($rows+1))-1;
        if (($printersMatrix->count() % ($rows+1))  != 0){
            $columns = intdiv($printersMatrix->count(),$rows+1);
        }
        $cont = 1;
        $row=0;
        $column=0;
        
        if ($reverse){
            $column=$columns;
        }
        
        foreach ($printersMatrix as $PrinterLine){
            $printerState = new PrinterState($PrinterLine->printer_id, $PrinterLine->code_printer, $PrinterLine->roll_weight, $PrinterLine->roll_replacement_datetime,$PrinterLine->rolls_end);
            
            $is_next_slot = false;
            $is_current_slot = false;
            
            
            foreach ($PrinterLine as $printer_slot){
                $slot_start = new DateTimeG($printer_slot->start);
                $slot_end = new DateTimeG($printer_slot->end);
                
                if  (
                    (($printer_slot->state == 9)||($printer_slot->state == 10))
                    &&
                    ($printer_slot->initiated == 1)
                    &&
                    ($this->start_date_time >= $slot_start)
                    
                    ){
                        $printerState->current_slot = $printer_slot;
                        $is_current_slot = true;
                }elseif (
                    (($printer_slot->state == 9))
                    &&
                    ($printer_slot->initiated == 1)
                    &&
                    ($this->start_date_time < $slot_start)
                    
                    ){
                        //Esta situación no debería ocurrir si no se ha usado set current time
                        $printerState->current_slot = $printer_slot;
                        $is_current_slot = true;
                }elseif (
                    ($printer_slot->state == 9)
                    &&
                    ($printer_slot->initiated == 0)
                    
                    &&
                    (!$is_current_slot)
                    ){
                        $printerState->current_slot = $printer_slot;
                        $is_current_slot = true;
                }elseif ($is_current_slot && !$is_next_slot){
                    $printerState->next_slot = $printer_slot;
                    $is_next_slot = true;
                } elseif ($is_current_slot && $is_next_slot){
                    break;
                }
            }
            $light = "green";
            if (isset($printerState->current_slot->start) && ($printerState->current_slot->start != null)){
                $current_start = new DateTimeG($printerState->current_slot->start);
                $current_end = new DateTimeG($printerState->current_slot->end);
                
                $current_end_minus_10 = new DateTimeG($printerState->current_slot->end);
                $current_end_minus_10->modify('-10 minutes');
                
                if ($printerState->current_slot->initiated == 0){
                    $light ="red";
                }elseif (( $this->start_date_time >= $current_start )&&($this->start_date_time <= $current_end)){
                    if (($this->start_date_time >= $current_end_minus_10) && ($this->start_date_time <= $current_end)){
                        $light = "blue";
                    }else{
                        $light="green";
                    }
                }else{
                    if (($this->start_date_time > $current_end) && ($this->start_date_time < $next_start)){
                        $light ="red";
                    }
                }
                
                $printerState->lamp = $light;
                
                $printerState->current_time_printing = $current_start->diffRangeHM($this->start_date_time);
                $printerState->current_time_left = $this->start_date_time->diffRangeHM($current_end);
                $current_time_total_minutes = $current_start->diffMinutes($current_end);
                $current_time_printing_minutes = $current_start->diffMinutes($this->start_date_time);
                $printerState->current_percent_printing = number_format($current_time_printing_minutes * 100/$current_time_total_minutes, 2, ',','.');
                
                $slot_interval_last_current = $current_start->diff($current_end);
                $slot_total_hours_real_current = $slot_interval_last_current->format('%d') * 24 + $slot_interval_last_current->format('%H');
                
                $printerState->current_days = $slot_interval_last_current->format('%d');
                $printerState->current_time = $slot_total_hours_real_current. ':' . $slot_interval_last_current->format('%i');
                
                
                $printerState->current_start_compact = $current_start->format('H:i - d F ');
                $printerState->current_end_compact = $current_end->format('H:i - d F');
                
                
                $printerState->current_button = "printed";
                if ($printerState->current_slot->state == 9){
                    if ($printerState->current_slot->initiated == 0){
                        $printerState->current_button = "play";
                    }else{
                        $printerState->current_button = "cancel";
                    }
                }
                
                $printerState->current_action = "printed";
                if ($printerState->current_slot->state == 9){
                    if ($printerState->current_slot->initiated == 0){
                        $printerState->current_action = "play";
                    }else{
                        $printerState->current_action = "cancel";
                    }
                }
            }
            
            
            
            
            if (isset($printerState->next_slot->start) && ($printerState->next_slot->start != null)){
                $next_start = new DateTimeG($printerState->next_slot->start);
                $next_end = new DateTimeG($printerState->next_slot->end);
                
                $next_end_minus_10 = new DateTimeG($printerState->next_slot->end);
                $next_end_minus_10->modify('-10 minutes');
                
                if ($printerState->next_slot->initiated == 0){
                    $light ="red";
                }elseif (( $this->start_date_time >= $next_start )&&($this->start_date_time <= $next_end)){
                    if (($this->start_date_time >= $next_end_minus_10) && ($this->start_date_time <= $next_end)){
                        $light = "blue";
                    }else{
                        $light="green";
                    }
                }else{
                    if (($this->start_date_time > $next_end) && ($this->start_date_time < $next_start)){
                        $light ="red";
                    }
                }
                
                $printerState->lamp = $light;
                
                $printerState->next_time_printing = $next_start->diffRangeHM($this->start_date_time);
                $printerState->next_time_left = $this->start_date_time->diffRangeHM($next_end);
                $next_time_total_minutes = $next_start->diffMinutes($next_end);
                $next_time_printing_minutes = $next_start->diffMinutes($this->start_date_time);
                $printerState->next_percent_printing = number_format($next_time_printing_minutes * 100/$next_time_total_minutes, 2, ',','.');
                
                $slot_interval_last_next = $next_start->diff($next_end);
                $slot_total_hours_real_next = $slot_interval_last_next->format('%d') * 24 + $slot_interval_last_next->format('%H');
                
                $printerState->next_days = $slot_interval_last_next->format('%d');
                $printerState->next_time = $slot_total_hours_real_next. ':' . $slot_interval_last_next->format('%i');
                
                
                $printerState->next_start_compact = $next_start->format('H:i - d F ');
                $printerState->next_end_compact = $next_end->format('H:i - d F');
                
                
                $printerState->next_button = "printed";
                if ($printerState->next_slot->state == 9){
                    if ($printerState->next_slot->initiated == 0){
                        $printerState->next_button = "play";
                    }else{
                        $printerState->next_button = "cancel";
                    }
                }
                
                $printerState->next_action = "printed";
                if ($printerState->next_slot->state == 9){
                    if ($printerState->next_slot->initiated == 0){
                        $printerState->next_action = "play";
                    }else{
                        $printerState->next_action = "cancel";
                    }
                }
            }
            
            $state_printer_matrix[$row][$column] = $printerState;
            //$state_printer_matrix[$row][$column] = $cont;
            if ($cont%($rows+1) == 0){
                if ($reverse){
                    $column --;
                }else{
                    $column ++;
                }
            }
            
            if ($row == $rows){
                $row = 0;
            }else{
                $row ++;
            }
            
            $cont ++;
            
        }
        
        
        
        
        
        echo "<script>window.history.pushState('','','/admin/parts/state');</script>";
        
        
        
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        
        
        $factory_employees = Employee::factoryEmployees(conf_neighborhood_factory_id())->get();
        
        $view = View::make('admin.parts.state')
        ->with('printersMatrix', $printersMatrix)
        ->with('statePrinterMatrix', $state_printer_matrix)
        ->with('rows', $rows)
        ->with('columns', $columns)
        ->with('start_date_time', $this->start_date_time)
        ->with('start_date_time_print',$printersMatrix->start_date_time_print)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max)
        ->with('factory_employees',$factory_employees);
        //->renderSections();
        return $view;
        return response()->json([
            'layout' => $view['content'],
        ]);
        
        
        
        
        
        
        
        
        
        
        
        $view = View::make('admin.parts.state')
        ->with('printersMatrix', $printersMatrix)
        ->with('statePrinterMatrix', $state_printer_matrix)
        ->with('rows', $rows)
        ->with('columns', $columns)
        ->with('start_date_time', $this->start_date_time)
        ->with('start_date_time_print',$printersMatrix->start_date_time_print)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max);
        
        return $view;
    }

    public function cancel($start_date_time_txt, $part_id,$printer_id)
    {
        /*
         if (session('start_date_time') != null){
         $this->start_date_time = session('start_date_time');
         } else {
         $this->start_date_time = new DateTimeG ($start_date_time_txt);
         }
         */
        $this->start_date_time = new DateTimeG ();
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        
        $printersMatrix = new PrintersMatrix(conf_neighborhood_factory_id(),$this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        
        
        
        
        foreach ($printersMatrix as $printerTimeline){
            foreach ($printerTimeline as $printerSlot){
                $partLog = new PartLog();
                
                $partLog->part_id = $printerSlot->part;
                $partLog->file_id = $printerSlot->file;
                $partLog->file_part_name = $printerSlot->file_part_name;
                $partLog->version_part_name = $printerSlot->version_part_name;
                $partLog->order_id = $printerSlot->order;
                $partLog->final_product_id = $printerSlot->final_product;
                //$partLog->filament_id = $printerSlot->filament_id;
                $partLog->printer_id = $printerTimeline->printer_id;
                $partLog->state_id = $printerSlot->state;
                $partLog->name = $printerSlot->file_name;
                $partLog->start_datetime = $printerSlot->start;
                $partLog->end_datetime = $printerSlot->end;
                $partLog->weight = $printerSlot->weight;
                $partLog->initiated = $printerSlot->initiated;
                $partLog->action = "'Cancel' Part:". $part_id.". printer:". $printer_id.".";
                $partLog->save();
            }
            
        }
        
        $printersMatrix->reassignPartsAll($this->start_date_time,'cancel', conf_neighborhood_factory_id(),$part_id);
        /*
         Part::where('id', $part_id)
         ->update(['initiated' => 0]);
         
         $printer_timeline = $printersMatrix->getPrinter($printer_id);
         $printer_timeline->setPartAsNotInitiated($part_id);
         */
        
        
        $printersMatrix->filamentCalculations();
        
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        
        $view = View::make('admin.parts.graph')
        ->with('printersMatrix', $printersMatrix)
        ->with('start_date_time', $this->start_date_time)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max);;
        return $view;
    }
    
    
    
    
    public function setStartDatetime(Request $request){
        
        $this->start_date_time =  new DateTimeG(request('start-date-time'));
        //$this->start_date_time =  new DateTimeG();
        
        //session(['start_date_time' => $this->start_date_time]);
        session(['working_days' => conf_working_days()]);
        set_time_limit(0);
        
        $neighborhood_factory_id = conf_neighborhood_factory_id();
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////// H T M L  -  B E G I N  ///////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        $printersMatrix = new PrintersMatrix($neighborhood_factory_id, $this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        $printersMatrix->assignPrintersSlots();
        
        $printersMatrix->filamentCalculations();
        
        foreach ($printersMatrix as $PrinterLine){
            usort($PrinterLine->rolls_end, 'sort_by_datetime');
            
            $color="";
            $blink_class =  "";
            foreach ($PrinterLine->rolls_end as $roll_end){
                $diff = $roll_end['datetime']->diffRangeHM($printersMatrix->start_date_time);
                $color="";
                $blink_class =  "";
                if ($diff['hours']<10){
                    $color =  "red";
                }
                if (($diff['hours']<1)&& ($diff['minutes']<30)){
                    $blink_class =  "blink";
                }
                $PrinterLine->color= $color;
                $PrinterLine->blink_class= $blink_class;
                break;
                
            }
        }
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        
        $view = View::make('admin.parts.graph')
        ->with('printersMatrix', $printersMatrix)
        ->with('start_date_time', $this->start_date_time)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max);
        
        return $view;
    }
    
    public function setStartDatetimeState(Request $request){
        
        $this->start_date_time =  new DateTimeG(request('start-date-time'));
        //$this->start_date_time =  new DateTimeG();
        
        //session(['start_date_time' => $this->start_date_time]);
        session(['working_days' => conf_working_days()]);
        set_time_limit(0);
        
        $neighborhood_factory_id = conf_neighborhood_factory_id();
        
        $min = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$min = new DateTime($now_datetime);
        $min->modify('-1 years');
        
        $max = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$max = new DateTime($start_date_time->date);
        $max->modify('+10 years');
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////////////// H T M L  -  B E G I N  ///////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        $printersMatrix = new PrintersMatrix($neighborhood_factory_id, $this->start_date_time);
        $printersMatrix->chargePrintersMatrix();
        $printersMatrix->assignPrintersSlots();
        
        $printersMatrix->filamentCalculations();
        
        foreach ($printersMatrix as $PrinterLine){
            usort($PrinterLine->rolls_end, 'sort_by_datetime');
            
            $color="";
            $blink_class =  "";
            foreach ($PrinterLine->rolls_end as $roll_end){
                $diff = $roll_end['datetime']->diffRangeHM($printersMatrix->start_date_time);
                $color="";
                $blink_class =  "";
                if ($diff['hours']<10){
                    $color =  "red";
                }
                if (($diff['hours']<1)&& ($diff['minutes']<30)){
                    $blink_class =  "blink";
                }
                $PrinterLine->color= $color;
                $PrinterLine->blink_class= $blink_class;
                break;
                
            }
        }
        
        $rows = 2;
        $state_printer_matrix = $printersMatrix->createStatePrintersMatrix(true,$rows);//Default parameters: reverse = true, rows = 3 
        
        $next_part = $printersMatrix->nextPart();
        $summary = $printersMatrix->summary();
        
        $factory_employees = Employee::factoryEmployees($neighborhood_factory_id)->get();
        
        $view = View::make('admin.parts.state')
        ->with('printersMatrix', $printersMatrix)
        ->with('statePrinterMatrix', $state_printer_matrix)
        ->with('rows', $rows)
        ->with('columns', $printersMatrix->columns($rows))
        ->with('start_date_time', $this->start_date_time)
        ->with('start_date_time_print',$printersMatrix->start_date_time_print)
        ->with('next_part', $next_part)
        ->with('summary', $summary)
        ->with('min', $min)
        ->with('max', $max)
        ->with('factory_employees',$factory_employees);
        
        return $view;
        
    }
    
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    public function insertPartLog($printersMatrix,$action,$printer_id,$part_id){
        foreach ($printersMatrix as $printerTimeline){
            if ($printerTimeline->printer_id == $printer_id){
                $partLog = new PartLog();
                foreach ($printerTimeline as $printerSlot){
                    
                    if ($printerSlot->part ==  $part_id){
                        
                        
                        $partLog->part_id = $printerSlot->part;
                        $partLog->file_id = $printerSlot->file;
                        $partLog->file_part_name = $printerSlot->file_part_name;
                        $partLog->version_part_name = $printerSlot->version_part_name;
                        $partLog->order_id = $printerSlot->order;
                        $partLog->final_product_id = $printerSlot->final_product;
                        //$partLog->filament_id = $printerSlot->filament_id;
                        $partLog->printer_id = $printerTimeline->printer_id;
                        $partLog->state_id = $printerSlot->state;
                        $partLog->name = $printerSlot->file_name;
                        $partLog->start_datetime = $printerSlot->start;
                        $partLog->end_datetime = $printerSlot->end;
                        $partLog->weight = $printerSlot->weight;
                        $partLog->initiated = $printerSlot->initiated;
                        $partLog->action = $action;
                        
                        $partLog->save();
                        return $partLog;
                    }
                }
                if ( $partLog->part_id==""){
                    $partLog->part_id = $printerTimeline->last_slot_out_range->part;
                    $partLog->file_id = $printerTimeline->last_slot_out_range->file;
                    $partLog->file_part_name = $printerTimeline->last_slot_out_range->file_part_name;
                    $partLog->version_part_name = $printerTimeline->last_slot_out_range->version_part_name;
                    $partLog->order_id = $printerTimeline->last_slot_out_range->order;
                    $partLog->final_product_id = $printerTimeline->last_slot_out_range->final_product;
                    //$partLog->filament_id = $printerTimeline->last_slot_out_range->filament_id;
                    $partLog->printer_id = $printerTimeline->printer_id;
                    $partLog->state_id = $printerTimeline->last_slot_out_range->state;
                    $partLog->name = $printerTimeline->last_slot_out_range->file_name;
                    $partLog->start_datetime = $printerTimeline->last_slot_out_range->start;
                    $partLog->end_datetime = $printerTimeline->last_slot_out_range->end;
                    $partLog->weight = $printerTimeline->last_slot_out_range->weight;
                    $partLog->initiated = $printerTimeline->last_slot_out_range->initiated;
                    $partLog->action = $action;
                    
                    $partLog->save();
                    return $partLog;
                }
                break;
            }
        }
    }
    
    
    public function insertIncident($request,$partLog){
        $incident = new Incident();
        $incident->action =   $partLog->action;
        $incident->printer_error_id =  request('error_id');
        $incident->employee_id = request('employe_id');
        $incident->printer_id = $partLog->printer_id;
        $incident->part_id = $partLog->part_id;
        $incident->file_id = $partLog->file_id;
        $incident->description = request('incident_description');
        $incident->hours = request('final_hours');
        $incident->minutes = request('final_minutes');
        
        $incident->save();
    }
}
