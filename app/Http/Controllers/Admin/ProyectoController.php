<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
//use Illuminate\Support\Str;
use Illuminate\Http\Request;

use Yajra\Datatables\Datatables;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProyectoRequest;

use App\Models\DB\Proyecto;
use App\Models\DB\Language;
use App\Models\DB\Media;
use App\Models\DB\Folder as DBFolder;
use App\Models\DB\Folder;
use App\Models\DB\Imagen as DBImagen;

use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;
use App\Vendor\Saizfact\Imagen\Imagen;
//use App\Vendor\Saizfact\Folder\Folder as EDFolder;
//use App\Vendor\Saizfact\Folder\Descriptor as EDDescriptor;


class ProyectoController extends Controller
{
    protected $permissions  = ['ver.proyectos', 'edit.proyectos'];
    protected $proyecto;
    protected $language;
    protected $locale;
    protected $locale_categorias;
    protected $locale_slug_seo;
    protected $locale_slug_seo_categorias;
    protected $datatables;

    protected $folder;
    protected $imagen;
    protected $db_imagen;
    protected $db_folder;
    protected $media;
    
    function __construct(
        Proyecto $proyecto,
        Language $language,
        Locale $locale,
        Datatables $datatables,
        Locale $locale_categorias,
        LocaleSlugSeo $locale_slug_seo,
        LocaleSlugSeo $locale_slug_seo_categorias,
        Media $media,
        Folder $folder,
        Imagen $imagen, 
        DBImagen $db_imagen,
        DBFolder $db_folder)
    {
        $this->middleware('auth');

        $this->proyecto = $proyecto;
        $this->language = $language;
        
        $this->locale = $locale;
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_categorias = $locale_categorias;
        $this->locale_slug_seo_categorias = $locale_slug_seo;
        
        $this->datatables = $datatables;

        $this->proyecto->activo = 1;
        $this->locale->setParent('proyectos.form');
        $this->locale_slug_seo->setParent('proyectos.form');
        $this->locale_categorias->setParent('proyectos.categorias.form');
        $this->locale_slug_seo_categorias->setParent('proyectos.categorias.form');
        
        $this->folder = $folder;
        $this->media = $media;
        $this->imagen = $imagen;
        $this->db_imagen = $db_imagen;
        $this->db_folder = $db_folder;
        
        $this->imagen->setType('media');
        
    }

    public function index()
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        return view('admin.proyectos.index');
    }

    public function indexJson()
    {
        $query = $this->proyecto
        ->with('categoria')
        ->select('t_proyecto.*');

        return $this->datatables->of($query)->toJson();   
    }

    public function create()
    {
        $view = View::make('admin.proyectos.edit')
        ->with('proyecto', $this->proyecto)
        ->with('locale', $this->locale->create())
        ->with('seo', $this->locale_slug_seo->create())
        ;
        
        if(request()->ajax()) {
            $sections = $view->renderSections();
            
            return response()->json([
                'layout' => $sections['content'],
                'form_with_two_modals' => $sections['form_with_two_modals'],
            ]);
        }
        
        return $view;
    }

    public function store(ProyectoRequest $request)
    {           
        $proyecto = Proyecto::updateOrCreate([
            'id' => request('id')],[
            'nombre' => request('nombre'),
            'categoria_id' => request('categoria_id'),
            'activo' => request('activo') ? 1 : 0,
            'orden' => request('orden'),
        ]);

        $proyecto->touch();
        
        $languages = $this->language->where('activo' , 1)->get();
        $seo = request('seo');
        
        $locale_slug_seo_categoria_id = $this->locale_slug_seo_categorias->getIdsByCategory(request('categoria_id'))->pluck('id','rel_profile'); 
        
        foreach($languages as $language){
            $seo['title.'.$language->alias] = request('locale')['titulo.'.$language->alias];
            $seo['parent.'.$language->alias] = $locale_slug_seo_categoria_id[$language->alias];
        }
        $this->locale_slug_seo->setParent('proyectos.form');
        $locale_slug_seo = $this->locale_slug_seo->store($seo, $proyecto->id);
        
        
        $locale_slug_seo = $this->locale_slug_seo->show($proyecto->id); 
        
        $locale = $this->locale->store(request('locale'), $proyecto->id);
        
        $locale = $this->locale->show($proyecto->id);
        
        if (request('id')){
            $message = "Proyecto actualizado ". $proyecto->nombre;
        }else{
            $message = "Proyecto creado ". $proyecto->nombre;
        }

        $medias = $this->media->where('proyecto_id',$proyecto->id)->orderBy('orden','ASC')->get();
        $images = collect();
        $docs = collect();
        foreach ($medias as $media){
            $image = $this->db_imagen
            ->where('entity_type','media')
            ->where('entity_id',$media->id)
            ->where('grid', 'parrilla')
            ->first();
            if (isset($image)){
                $image->nombre = $media->nombre;
                $images->push($image);
            }else{
                $doc = $this->db_folder
                ->where('entity_type','media-doc')
                ->where('entity_id',$media->id)
                //->where('grid', 'parrilla')
                ->first();
                //$obj = (object) array('entity_id'=>$media->id,'nombre'=>$media->nombre);
                if (isset($doc)){
                    $doc->nombre = $doc->name;
                    $docs->push($doc);
                }
            }
        }
        
        
        
        $view = View::make('admin.proyectos.edit')
        ->with('proyecto', $proyecto)
        ->with('locale', $locale)
        ->with('seo', $locale_slug_seo)
        ->with('images',$images)
        ->with('docs',$docs)
        
        ;        

        if(request()->ajax()) {
            $sections = $view->renderSections();
            
            return response()->json([
                'layout' => $sections['content'],
                'form_with_two_modals' => $sections['form_with_two_modals'],
                'id' => $proyecto->id,
                'message' => $message,
            ]);
        }
        
        
        
    }

    public function show(Proyecto $proyecto)
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        
        $folders_aux =  $proyecto->folders;
        
        $locale = $this->locale->show($proyecto->id);
        $locale_slug_seo = $this->locale_slug_seo->show($proyecto->id);
        
        $folders_aux =  $proyecto->folders;
        
        $folders = array();
        $folders_name = array();
        foreach ($folders_aux as $folder){
            $folders[$folder->anchor]= $folder->filename;
            $folders_name[$folder->anchor] = $folder->name;
        }
        
        
        
        $id= $proyecto->id;
        
        
        
        $medias = $this->media->where('proyecto_id',$proyecto->id)->orderBy('orden','ASC')->get();
        $images = collect();
        $docs = collect();
        foreach ($medias as $media){
            $image = $this->db_imagen
            ->where('entity_type','media')
            ->where('entity_id',$media->id)
            ->where('grid', 'parrilla')
            ->first();
            if (isset($image)){
                $image->nombre = $media->nombre;
                $images->push($image);
            }else{
                $doc = $this->db_folder
                ->where('entity_type','media-doc')
                ->where('entity_id',$media->id)
                //->where('grid', 'parrilla')
                ->first();
                //$obj = (object) array('entity_id'=>$media->id,'nombre'=>$media->nombre);
                if (isset($doc)){
                    $doc->nombre = $doc->name;
                    $docs->push($doc);
                }
            }
        }

        $view = View::make('admin.proyectos.edit')
        ->with('proyecto', $proyecto)
        ->with('locale', $locale)
        //->with('languages',$languages)
        ->with('id',$id)
        ->with('images',$images)
        ->with('docs',$docs)
        ->with('folders',$folders)
        ->with('folders_name',$folders_name)
        ->with('seo', $locale_slug_seo);   
        
        if(request()->ajax()) {
            $sections = $view->renderSections(); 
    
            return response()->json([
                'layout' => $sections['content'],
                'form_with_two_modals' => $sections['form_with_two_modals'],
                'id' => $proyecto->id
            ]); 
        }
                
        return $view;
    }

    public function destroy(Proyecto $proyecto)
    {
        $proyecto->delete();
        $this->locale->delete($proyecto->id);

        $message = "Proyecto borrado ". $proyecto->nombre;

        $view = View::make('admin.proyectos.index')
            ->with('proyectos', $this->proyecto->get())
            ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'view' => $view['table'],
            'message' => $message,
        ]);
    }

    public function reorderTable(Request $request)
    {
        $order = request('orden');

        if (is_array($order)) {
            foreach ($order as $index => $tableItem) {
                $item = $this->proyecto->findOrFail($tableItem);
                $item->orden = $index + 1;
                $item->save();
            }
        }
    }
}
