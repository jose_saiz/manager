<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;

use Yajra\Datatables\Datatables;

use app\Http\Requests\Admin\OrderRequest;
use Illuminate\Support\Facades\Auth;
use App\Models\DB\FinalProduct;
use App\Models\DB\Order;
use App\Models\DB\Language;
use App\Models\DB\OrderLine;
use App\Models\DB\Part;
use App\Models\DB\Product;
use App\Vendor\Saizfact\Locale\Locale;
use App\Models\DB\ProductQuantityParts;



class OrderController extends Controller
{    
    protected $permissions  = ['ver.orders', 'edit.orders'];
    protected $order;
    protected $language;
    protected $locale;
    protected $locale_categorias;
    protected $datatables;
    protected $products;
    protected $total_price;
    
    function __construct(Order $order,Language $language, Locale $locale, Datatables $datatables,Locale $locale_categorias)
    {
        $this->middleware('auth');
        $this->order = $order;
        $this->language = $language;
        $this->locale = $locale;
        $this->locale_categorias = $locale_categorias;
        $this->datatables = $datatables;
        $this->locale->setParent('orders.form');
        $this->products = Product::where('active',1)->select('*')->get();
        $this->total_price = 0;
        foreach ($this->products as $product){
            $this->quantity[$product->id] = 0;
        }
        
        
        
    }
    
    
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        //
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        return view('admin.orders.index');
    }

    public function indexJson()
    {
        $query = $this->order
        ->with('customer')
        ->with('state')
        //->with('final_products')
        ->with('parts')
        ->with('factory')
        ->select('order_g.*')
        ->where('neighborhood_id',Auth::guard('web')->user()->factory_id);
        //->orderBy('id','asc')
        ;
        
        
        return $this->datatables->of($query)
            ->editColumn('parts',function($query){
                return  $query->parts->count();
        })
        ->escapeColumns([])
        ->make(true);
        
        return $this->datatables->of($query)->toJson();
    }
    
   
    public function indexOrderLinesJson($order_id) {
        $sql= '
            SELECT 
                p.id,
                quantity,
                p.price as unit_price,
                p.name as product_name,
                quantity*p.price AS total 
            FROM order_line ol 
            INNER JOIN product as p ON ol.product_id =  p.id
            INNER JOIN order_g as o ON ol.order_id = o.id
            INNER JOIN customer as c ON c.id = o.customer_id
            WHERE order_id = '.$order_id.'
            ORDER BY p.id ASC';
        
        $query =DB::select($sql);
        
        return $this->datatables->of($query)->toJson();
        
    }
    
    public function indexFinalProductsJson($order_id) {
        
        
        $sql='
            SELECT 
                fp.id,p.name AS product_name,"percent"
            FROM final_product fp 
            INNER JOIN product as p ON fp.product_id =  p.id
            INNER JOIN order_line as ol ON fp.order_line_id = ol.id
            INNER JOIN order_g as o ON ol.order_id = o.id
            WHERE order_id = '.$order_id;
        
        $query =DB::select($sql);
        
        return $this->datatables->of($query)
        ->editColumn('percent',function($query){
            
            $parts = Part::where('final_product_id', $query->id);
            $percent = 0;
            foreach ($parts as $part){
                $total =strtotime( $part->end_datetime) - strtotime( $part->start_datetime);
                $printed = strtotime( session('start_date_time')) -strtotime($part->start_datetime);
                $percent += (100*$printed/$total);
            }
            return number_format($percent/$parts->count(),2)."%";
          
        })
        ->make(true);
        
        //return $this->datatables->of($query)->toJson();
        
    }
    
    public function indexPartsJson($order_id) {
        
        $sql='
        SELECT
            fp.id AS id_final_product,
            p.name AS product_name,
            f.name AS part_name,
            weight,
            pr.code AS printer_name,
            start_datetime,
            end_datetime,
            initiated
        FROM part AS pa
        INNER JOIN file as f ON pa.file_id =  f.id
        INNER JOIN final_product as fp ON pa.final_product_id =  fp.id
        INNER JOIN product as p ON p.id =  fp.product_id
        INNER JOIN order_line as ol ON fp.order_line_id = ol.id
        INNER JOIN order_g as o ON ol.order_id = o.id
        LEFT JOIN printer as pr ON pa.printer_id = pr.id
        WHERE o.id = '.$order_id .'
        ORDER BY fp.id';
        
        $query =DB::select($sql);
        
        return $this->datatables->of($query)->toJson();
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $view = View::make('admin.orders.edit')
        ->with('order_id', 0)
        ->with('order', $this->order)
        ->with('quantity', $this->quantity)
        ->with('total_price',$this->total_price)
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $message = "";
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////// CREATE + UPDATE ORDER ///////////////////////////////////////////////////////////////////////////////////
        $order = Order::updateOrCreate([
            'id' => request('id')],[
                'customer_id' => request('customer_id'),
                'state_id' => request('state_id'),
                'priority' => request('priority'),
                'neighborhood_id' => Auth::guard('web')->user()->factory_id
        ]);
        $order->touch();
        
        ////////// CREATE + UPDATE ORDER LINES ////////////////////////////////////////////////////////////////////////////////
        if (request('id')==""){
            //Create
            foreach ($this->products as $product){
                $this->quantity[$product->id]= request('quantity-'.$product->id);
                if ((request('quantity-'.$product->id) !=0) && (request('quantity-'.$product->id) !== "")){
                    $order_line = OrderLine::updateOrCreate([
                            'quantity' => request('quantity-'.$product->id),
                            'order_id' => $order->id,
                            'product_id' => $product->id,
                            'price' => $product->price
                        ]);
                    $order_line->touch();
                }
            }
        }else{
            //Update
            foreach ($this->products as $product){
                $this->quantity[$product->id]= request('quantity-'.$product->id);
                $order_line =  OrderLine::select('*')
                                        ->where('product_id',$product->id)
                                        ->where('order_id',request('id'))
                                        ->first();
                $order_line_id = "";
                //if ($order_line->count() != 0){
                if (isset($order_line->id)){
                    $order_line_id = $order_line->id;
                    
                }
                if ((request('quantity-'.$product->id) !=0) && (request('quantity-'.$product->id) !== "")){
                    $order_line = OrderLine::updateOrCreate([
                        'id' => $order_line_id],[
                            'quantity' => request('quantity-'.$product->id),
                            'order_id' => $order->id,
                            'product_id' => $product->id,
                            'price' => $product->price
                        ]);
                    $order_line->touch();
                }else{
                    //Borrar de la order line ese producto
                    //$order_line = OrderLine::find(request('id'));
                    $order_line = OrderLine::where('id',$order_line_id);
                    $order_line->delete();
                }
            }
        }
        
        //New Order Lines
        $order_lines =  OrderLine::select('*')
        ->where('order_id',$order->id)
        ->get();
        
        //////// CREATE + UPDATE FINAL PRODUCT ////////////////////////////////////////////////////////////////////////////////
        //Delete Old Final Products
        $delete_final_products = [];
        foreach ($order_lines as $order_line){
            $final_products = FinalProduct::where('order_line_id',$order_line->id);
            //Save the delete old final products for delete old parts 
            foreach ($final_products->get() as $final_product ){
                array_push($delete_final_products,$final_product->id);
            }
            $final_products->delete();
        }
        //Create Final Products
        foreach ($order_lines as $order_line){
            for ($i=1;$i<=$order_line->quantity;$i++){
                $final_product = FinalProduct::create([
                        'product_id' => $order_line->product_id,
                        'order_line_id'=> $order_line->id
                    ]);
                $final_product->touch();
            }
            $this->total_price += $order_line->quantity*$order_line->price;
        }
        
        ///////////// CREATE + UPDATE PART ////////////////////////////////////////////////////////////////////////////////////
        //Delete Parts
        foreach ($delete_final_products as $final_product_id){
            $parts = Part::where('final_product_id',$final_product_id);
            $parts->delete();
        }
        //Create Parts
        foreach ($order_lines as $order_line){
            $final_products = FinalProduct::where('order_line_id',$order_line->id)->get();
            foreach ($final_products as $final_product){
                $product_quantity_parts = ProductQuantityParts::GetFileInformation($final_product->product_id);
                foreach ($product_quantity_parts as $product_quantity_part) {
                    for ($i=1;$i<=$product_quantity_part->quantity;$i++){
                        $part = Part::create([
                                'file_id'=> $product_quantity_part->file_id,
                                'file_part_name'=> $product_quantity_part->file_part_name,
                                'version_part_name'=> $product_quantity_part->version_part_name,
                                 'order_id'=> $order->id,
                                'final_product_id'=>$final_product->id,
                                'filament_id'=> 1,
                                'state_id'=> 8,
                                'name'=> '',
                                'weight'=> $product_quantity_part->weight,
                                'initiated'=> 0,
                            ]);
                        $part->touch();
                    }
                }
            }
        }
        
        ///////////// END CREATE + UPDATE /////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        if (request('id')){
            $message = "Order updated ". $order->id.'';
        }else{
            $message = "Order created ". $order->id;
        }
        
        $view = View::make('admin.orders.edit')
        ->with('order', $order)
        ->with('order_id', $order->id)
        ->with('customer', $order->customer)
        ->with('quantity', $this->quantity)
        ->with('total_price',$this->total_price)
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
            'id' => $order->id,
            'message' => $message,
        ]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        
        $order_lines = OrderLine::where('order_id',$order->id)
            ->select('product_id', 'quantity','price')
            ->get();
        foreach ( $order_lines as $order_line){
        //foreach ($order->order_lines() as $order_line){
            $this->quantity[$order_line->product_id]= $order_line->quantity;
            $this->total_price += $order_line->quantity*$order_line->price;
        }

        $view = View::make('admin.orders.edit')
        ->with('order', $order)
        ->with('order_id', $order->id)
        //->with('order_lines', $order->order_lines())
        //->with('order_lines', OrderLine::where('order_id',$order->id)->select('*')->get())
        ->with('quantity', $this->quantity)
        ->with('customer', $order->customer)
        ->with('total_price',$this->total_price);
       
        if(request()->ajax()) {
            $sections = $view->renderSections();
            
            return response()->json([
                'layout' => $sections['content'],
                'form' => $sections['form'],
            ]);
        }
        
        return $view;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        
       
        //return view('admin.orders.index');
        
        $order_lines = OrderLine::where('order_id',$order->id);
        foreach ($order_lines->get() as $order_line){
            $final_products = FinalProduct::where('order_line_id',$order_line->id);
            foreach ($final_products->get() as $final_product){
                $parts = Part::where('final_product_id',$final_product->id);
                $parts->delete();
            }
            $final_products->delete();
        }
        $order_lines->delete();
        //$order = Order::where('id',$order_id);
        $order->delete();
        
        
        $message = "Order Deleted ". $order->id;
        
        $view = View::make('admin.orders.index')
        ->with('orders', $this->order->get())
        ->renderSections();
        
        
        
        
        return response()->json([
            'layout' => $view['content'],
            'view' => $view['table'],
            'message' => $message,
        ]);
        
        /*
        $view = View::make('admin.orders.index')
        ->with('order', $this->order->get());
        
        if(request()->ajax()) {
            $sections = $view->renderSections();
            return response()->json([
                'layout' => $sections['content'],
                'view' => $sections['table'],
                'message' => $message,
            ]);
        }
        
        return $view;
        */
        
    }
}
