<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Yajra\Datatables\Datatables;
use App\Vendor\Caffeinated\Shinobi\Models\Permission;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\UserPermissionRequest;

class PermissionController extends Controller
{
    protected $permissions  = ['ver.permisos', 'edit.permisos'];
    protected $permission;
    protected $datatables;

    public function __construct(Permission $permission, Datatables $datatables)
    {
        $this->middleware('auth');
        
        $this->permission = $permission;
        $this->datatables = $datatables;
                
        $this->permission->activo = 1;
    }

    public function index()
    {
        if (! Auth::guard('web')->user()->isSaizfact()) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        return view('admin.permissions.index')->with('permissions', $this->permission->get());
    }

    public function indexJson()
    {
        $query = $this->permission
        ->select('t_permission.*');

        return $this->datatables->of($query)->toJson();   
    }

    public function create()
    {
        $view = View::make('admin.permissions.edit')
        ->with('permission', $this->permission)
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
        ]);
    }

    public function store(UserPermissionRequest $request)
    {
        $permission = Permission::updateOrCreate([
            'id' => request('id')],[
            'name' => request('name'),
            'activo' => request('activo') ? 1 : 0,
            'slug' => request('slug'),
            'description' => request('description'),
            'route' => request('route'),
        ]);

        if (request('id')){
            $message = "Permiso actualizado ". $permission->name;
        }else{
            $message = "Permiso creado ". $permission->name;
        }

        $view = View::make('admin.permissions.edit')
        ->with('permission', $permission)
        ->renderSections();       

        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
            'id' => $permission->id,
            'message' => $message,
        ]);
    }

    public function show(Permission $permission)
    {
        if (! Auth::guard('web')->user()->isSaizfact()) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        
        $view = View::make('admin.permissions.edit')
        ->with('permission', $permission);
        
        if(request()->ajax()) {
            $sections = $view->renderSections(); 
    
            return response()->json([
                'layout' => $sections['content'],
                'form' => $sections['form'],
            ]); 
        }
                
        return $view;
    }

    public function destroy(Permission $permission)
    {
        $permission->delete();
    
        $message = "Permiso borrado ". $permission->name;

        $view = View::make('admin.permissions.index')
        ->with('permissions', $this->permission->get())
        ->renderSections();  
        
        return response()->json([
            'layout' => $view['content'],
            'view' => $view['table'],
            'message' => $message,
        ]);
    }
}
