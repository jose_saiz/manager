<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Models\DB\Language;
use App\Vendor\Saizfact\Locale\Models\LocaleSeo;
use App\Vendor\Saizfact\Locale\Models\LocaleRedirect;
use App\Vendor\Saizfact\Locale\Manager;

class LocaleSeoController extends Controller
{
    protected $permissions  = ['ver.seo', 'edit.seo'];
    protected $datatables;
    protected $manager;
    protected $language;
    protected $locale_seo;
    protected $locale_redirect;
    
    function __construct(Datatables $datatables, Manager $manager, Language $language, LocaleSeo $locale_seo, LocaleRedirect $locale_redirect)
    {
        $this->middleware('auth');
        $this->datatables = $datatables;
        $this->manager = $manager;
        $this->language = $language;
        $this->locale_seo = $locale_seo;
        $this->locale_redirect = $locale_redirect;
    }

    public function index()
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }

        $this->manager->importTranslations();

        $seo = $this->locale_seo->orderBy('id', 'desc')->first();

        $seos = $this->locale_seo->where('key', $seo->key)->get();
        $seos->key = $seo->key;
        $seos->group = $seo->group;
        $seos->id = $seo->id;

        $languages = $this->language->get();

        foreach($languages as $language){
            $seo = $seos->filter(function($item) use($language) {
                return $item->rel_profile == $language->alias;
            })->first();

            $locale = array();
            $locale['url.'. $language->alias] = empty($seo->url) ? '': $seo->url; 
            $locale['description.'. $language->alias] = empty($seo->description) ? '': $seo->description; 
            $locale['keywords.'. $language->alias] = empty($seo->keywords) ? '': $seo->keywords; 
        }

        return view('admin.seo.index')
        ->with('locale', $locale)
        ->with('seos', $seos);
    }

    public function indexJson()
    {
        $query = $this->locale_seo
        ->groupBy('key')
        ->select('t_locale_seo.*');

        return $this->datatables->of($query)->toJson();   
    }

    public function create()
    {

    }

    public function store(Request $request)
    {                   
        foreach (request('locale') as $rel_anchor => $value){

            $rel_anchor = str_replace(['-', '_'], ".", $rel_anchor); 
            $explode_rel_anchor = explode('.', $rel_anchor);
            $rel_profile = end($explode_rel_anchor);

            $locale_seo = $this->locale_seo::updateOrCreate([
                'rel_profile' => $rel_profile,
                'group' => request('group'),
                'key' => request('key')],[
                'rel_profile' => $rel_profile,
                'group' => request('group'),
                'key' => request('key'),
                'url' => request('locale')['url.'. $rel_profile],
                'redirection' => request('locale')['old_url.'. $rel_profile] != request('locale')['url.'. $rel_profile] ? 1 : 0,
                'description' => request('locale')['description.'. $rel_profile],
                'keywords' => request('locale')['keywords.'. $rel_profile],
            ]);

            if($locale_seo->redirection == 1){

                $this->locale_redirect::updateOrCreate([
                    'old_url' => $rel_profile .'/'. request('locale')['old_url.'. $rel_profile],
                    ],[
                    'rel_profile' => $rel_profile,
                    'group' => request('group'),
                    'key' => request('key'),
                    'subdomain' => $locale_seo->subdomain,
                    'old_url' => $rel_profile .'/'. request('locale')['old_url.'. $rel_profile],
                    'locale_seo_id' => $locale_seo->id,
                ]);
            }
        }  

        $seos = $this->locale_seo->where('key', $locale_seo->key)->get();
        $seos->key = $locale_seo->key;
        $seos->group = $locale_seo->group;
        $seos->id = $locale_seo->id;

        $this->manager->exportTranslations(request('group'));   

        if (request('id')){
            $message = "Información de SEO actualizada ";
        }else{
            $message = "Información de SEO creada ";
        }

        $view = View::make('admin.seo.index')
        ->with('seos', $seos)
        ->with('locale', request('locale'))
        ->renderSections(); 

        return response()->json([
            'layout' => $view['content'],
            'table' => $view['table'],
            'form' => $view['form'],
            'id' => $locale_seo->id,
            'message' => $message,
        ]);
    }

    public function show(LocaleSeo $seo)
    {
        $seos = $this->locale_seo->where('key', $seo->key)->get();
        $seos->key = $seo->key;
        $seos->group = $seo->group;
        $seos->id = $seo->id;

        $languages = $this->language->get();

        foreach($languages as $language){
            $seo= $seos->filter(function($item) use($language) {
                return $item->rel_profile == $language->alias;
            })->first();

            $locale = array();
            $locale['url.'. $language->alias] = empty($seo->url) ? '': $seo->url; 
            $locale['description.'. $language->alias] = empty($seo->description) ? '': $seo->description; 
            $locale['keywords.'. $language->alias] = empty($seo->keywords) ? '': $seo->keywords; 
        }
        
        $view = View::make('admin.seo.index')
        ->with('locale', $locale)
        ->with('seos', $seos);
        
        if(request()->ajax()) {
            $sections = $view->renderSections(); 
    
            return response()->json([
                'layout' => $sections['content'],
                'form' => $sections['form'],
            ]); 
        }
                
        return $view;
    }

    public function destroy()
    {
       
    }
}
