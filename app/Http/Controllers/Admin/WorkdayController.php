<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;

use Yajra\Datatables\Datatables;

use App\Http\Requests\Admin\WorkdayRequest;
use App\Models\DB\Workday;
use App\Models\DB\Language;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;



class WorkdayController extends Controller
{
    //
    protected $permissions  = ['view.workdays', 'edit.workdays'];
    protected $workday;
    protected $language;
    protected $locale;
    protected $locale_categorias;
    protected $locale_slug_seo;
    protected $locale_slug_seo_categorias;
    protected $datatables;
    
    function __construct(Workday $workday,Language $language, Locale $locale, Datatables $datatables,Locale $locale_categorias, LocaleSlugSeo $locale_slug_seo, LocaleSlugSeo $locale_slug_seo_categorias)
    {
        $this->middleware('auth');
        
        $this->workday = $workday;
        $this->language = $language;
        
        $this->locale = $locale;
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_categorias = $locale_categorias;
        $this->locale_slug_seo_categorias = $locale_slug_seo;
        
        $this->datatables = $datatables;
        
        $this->workday->active = 1;
        $this->locale->setParent('workdays.form');
        $this->locale_slug_seo->setParent('workdays.form');
        $this->locale_categorias->setParent('workdays.categorias.form');
        $this->locale_slug_seo_categorias->setParent('workdays.categorias.form');
        
    }
    
    public function index()
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        //session()->put('locale', $language);
        //App::setLocale($language);
        //return  View::make('admin.workdays.index')->with('language', $language);
        return view('admin.workdays.index');
    }
    
    public function indexJson()
    {
        $query = $this->workday
        ->with('category')
        ->select('workday.*')
        ->where('neighborhood_factory_id',Auth::guard('web')->user()->factory_id);
        
        //->orderBy('id','asc')
        ;
        
        return $this->datatables->of($query)
        ->editColumn('day',function($query){
            return  __('weekdays.'.$query->day);
        })
        ->escapeColumns([])
        ->make(true);
        
    }
    
    public function create()
    {
        $view = View::make('admin.workdays.edit')
        ->with('workday', $this->workday)
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
        ]);
    }
    
    public function store(WorkdayRequest $request)
    {
        $workday = Workday::updateOrCreate([
        'id' => request('id')],[
        'neighborhood_factory_id' =>Auth::guard('web')->user()->factory_id,
        'day' => request('day'),
        'begin_time' => request('begin_time'),
        'end_time' => request('end_time'),
        'category_id' => 3,
        'active' => 1,
        ]);
             
        
        $workday->touch();
        
        if (request('id')){
            $message = "Workday updated ". $workday->nombre;
        }else{
            $message = "Workday created ". $workday->nombre;
        }
        
        $view = View::make('admin.workdays.edit')
        ->with('workday', $workday)
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
            'id' => $workday->id,
            'message' => $message,
        ]);
    }
    
    public function show(Workday $workday)
    {

        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        
        $view = View::make('admin.workdays.edit')->with('workday', $workday);
        
        if(request()->ajax()) {
            $sections = $view->renderSections();
            
            return response()->json([
                'layout' => $sections['content'],
                'form' => $sections['form'],
            ]);
        }
        
        return $view;
    }
    
    
    public function destroy(Workday $workday)
    {
        $workday->delete();
        $this->locale->delete($workday->id);
        
        $message = "Workday borrado ". $workday->code;
        
        $view = View::make('admin.workdays.index')
        ->with('workdays', $this->workday->get())
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'view' => $view['table'],
            'message' => $message,
        ]);
    }
    
    public function reorderTable(Request $request)
    {
        $order = request('orden');
        
        if (is_array($order)) {
            foreach ($order as $index => $tableItem) {
                $item = $this->workday->findOrFail($tableItem);
                $item->orden = $index + 1;
                $item->save();
            }
        }
    }
}
