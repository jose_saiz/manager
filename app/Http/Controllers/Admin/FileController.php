<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use Yajra\Datatables\Datatables;

//use App\Http\Requests\Admin\FileRequest;
use App\Models\DB\File;
use App\Models\DB\Language;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;


class FileController extends Controller
{
    protected $permissions  = ['ver.files', 'edit.files'];
    protected $file;
    protected $language;
    protected $locale;
    protected $locale_categorias;
    protected $locale_slug_seo;
    protected $locale_slug_seo_categorias;
    protected $datatables;
    
    function __construct(File $file,Language $language, Locale $locale, Datatables $datatables,Locale $locale_categorias, LocaleSlugSeo $locale_slug_seo, LocaleSlugSeo $locale_slug_seo_categorias)
    {
        set_time_limit(0);
        $this->middleware('auth');
        
        $this->file = $file;
        $this->language = $language;
        
        $this->locale = $locale;
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_categorias = $locale_categorias;
        $this->locale_slug_seo_categorias = $locale_slug_seo;
        
        $this->datatables = $datatables;
        
        
        $this->locale->setParent('files.form');
        $this->locale_slug_seo->setParent('files.form');
        $this->locale_categorias->setParent('files.categorias.form');
        $this->locale_slug_seo_categorias->setParent('files.categorias.form');
        
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        //session()->put('locale', $language);
        //App::setLocale($language);
        //return  View::make('admin.printers.index')->with('language', $language);
        
       
        /*
        $files = getFiles();
        //var_dump($files);
        $result_table = array();
        foreach ($files as $id){
            foreach ($id as $letter){
                foreach ($letter as $version){
                    foreach ($version as $slicer ){
                        foreach ($slicer as $file){

                            $line = array();
                            $line['id'] =  $file['data']['id'];
                            $line['fileHash'] = $file['data']['fileHash'];
                            $line['partNo'] = $file['data']['partNo'];
                            $line['modelVersion'] = $file['data']['modelVersion'];
                            $line['slicedVersion'] = $file['data']['slicedVersion'];
                            $line['filament_used'] = $file['data']['filament used [g]'];
                            $line['estimated_printing_time'] = $file['data']['estimated printing time (normal mode)'];
                            $line['fileTime'] = $file['data']['fileTime'];
                            $line['fileSize'] = $file['data']['fileSize'];
                            $line['files'] ="";
                            foreach ($file['files'] as $file_name){
                                $line['files'].= $file_name .',';
                            }
                            $line['files'] = substr($line['files'], 0, -1);
                            $line['files'].= '.';
                            
                            
                            array_push( $result_table,$line);
                        }
                    }
                }
            }
        }
        */
        
        //return view('admin.files.index',compact('result_table'));
        return view('admin.files.index');
        
        
    }
    
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexJson()
    {
        //
        $files = getFiles();
        //var_dump($files);
        $result_table = array();
        foreach ($files as $id){
            foreach ($id as $letter){
                foreach ($letter as $version){
                    foreach ($version as $slicer ){
                        foreach ($slicer as $file){
                            /*
                            var_dump($file);
                            var_dump($file['data']['id']);
                            var_dump($file['data']['fileHash']);
                            var_dump($file['data']['partStatus']);
                            var_dump($file['data']['partNo']);
                            var_dump($file['data']['modelVersion']);
                            var_dump($file['data']['slicedVersion']);
                            var_dump($file['data']['filament used [g]']);
                            var_dump($file['data']['estimated printing time (normal mode)']);
                            var_dump($file['data']['fileTime']);
                            var_dump($file['data']['fileSize']);
                            */
                            $line = array();
                            $line['id'] =  $file['data']['id'];
                            $line['fileHash'] = $file['data']['fileHash'];
                            $line['partNo'] = $file['data']['partNo'];
                            $line['modelVersion'] = $file['data']['modelVersion'];
                            $line['slicedVersion'] = $file['data']['slicedVersion'];
                            $line['filament_used'] = $file['data']['filament used [g]'];
                            $line['estimated_printing_time'] = $file['data']['estimated printing time (normal mode)'];
                            $line['fileTime'] = $file['data']['fileTime'];
                            $line['fileSize'] = $file['data']['fileSize'];
                            $line['files'] ="";
                            foreach ($file['files'] as $file_name){
                                $line['files'].= $file_name .',';
                            }
                            $line['files'] = substr($line['files'], 0, -1);
                            $line['files'].= '.';
                            
                            
                            array_push( $result_table,$line);
                        }
                    }
                }
            }
        }
        
        $datatable= $this->datatables->of( $result_table);
        
        return $datatable;
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
