<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Models\DB\Language;
use App\Vendor\Saizfact\Locale\Models\LocaleTag;
use App\Vendor\Saizfact\Locale\Manager;

class LocaleTagController extends Controller 
{
    protected $permissions  = ['ver.traducciones', 'edit.traducciones'];
    protected $locale_tag;
    protected $datatables;
    protected $language;

    function __construct(LocaleTag $locale_tag, Datatables $datatables, Language $language, Manager $manager)
    {
        $this->middleware('auth');
        
        $this->locale_tag = $locale_tag;
        $this->datatables = $datatables;
        $this->language = $language;
        $this->manager = $manager;

        $this->locale_tag->active = 1;
    }

    public function index()
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }

        $tag = new LocaleTag();
        $languages = $this->language->get();
        $locale = array();
        foreach ($languages as $language) {
            $locale['value.'. $language->alias] = ''; 
        }

        return View::make('admin.tags.index')->with('locale', $locale)->with('tag', $tag);
    }

    public function indexJson()
    {
        $query = $this->locale_tag
        ->groupBy('key')
        ->where('group', 'not like', 'admin/%')
        ->select('t_locale_tag.*');

        return $this->datatables->of($query)->toJson();   
    }

    public function create()
    {

    }

    public function store(Request $request)
    {
        $locale_tag = null;
        foreach (request('locale') as $rel_anchor => $value) {
            $rel_anchor = str_replace(['-', '_'], ".", $rel_anchor); 
            $explode_rel_anchor = explode('.', $rel_anchor);
            $rel_profile = end($explode_rel_anchor);

            $locale_tag = $this->locale_tag::updateOrCreate([
                'rel_profile' => $rel_profile,
                'group' => request('group'),
                'key' => request('key')],[
                'rel_profile' => $rel_profile,
                'group' => request('group'),
                'key' => request('key'),
                'value' => $value
            ]);
        }
        
        $this->manager->exportTranslations(request('group'));
        
        // Sacamos el tag elegido para todos los idiomas
        $tags = LocaleTag::where([
            'group' => $locale_tag->group,
            'key' => $locale_tag->key
        ])->get();
        $tag = $tags->first();
        
        $languages = $this->language->get();
        $locale = array();
        foreach ($languages as $language) {
            $current_tag = $tags->filter(function ($item) use ($language) {
                return $item->rel_profile == $language->alias;
            })->first();
            
            $locale['value.' . $language->alias] = $current_tag && $current_tag->value ? $current_tag->value : '';
        }

        $message = "Traducción actualizada";

        $view = View::make('admin.tags.index')->with('locale', $locale)->with('tag', $tag);
        $sections = $view->renderSections();

        return response()->json([
            'layout' => $sections['content'],
            'table' => $sections['table'],
            'id' => $locale_tag->id,
            'message' => $message,
        ]);
    }

    public function show(LocaleTag $tag)
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        
        // Sacamos el tag elegido para todos los idiomas
        $tags = LocaleTag::where([
            'group' => $tag->group,
            'key' => $tag->key
        ])->get();
        
        $languages = $this->language->get();
        $locale = array();
        foreach ($languages as $language) {
            $current_tag = $tags->filter(function ($item) use ($language) {
                return $item->rel_profile == $language->alias;
            })->first();
            
            $locale['value.' . $language->alias] = $current_tag && $current_tag->value ? $current_tag->value : '';
        }
        
        $view = View::make('admin.tags.index')->with('locale', $locale)->with('tag', $tag);
        
        if (request()->ajax()) {
            $sections = $view->renderSections();
            
            return response()->json([
                'layout' => $sections['content'],
                'form' => $sections['form']
            ]);
        }
        
        return $view;
    }

    public function destroy()
    {
       
    }
}
