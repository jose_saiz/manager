<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\DB\Folder as DBFolder;
use App\Models\DB\Folder;
use App\Models\DB\Media;
use App\Http\Requests\Admin\MediaRequest;
use App\Models\DB\Imagen as DBImagen;
//use Debugbar;
use App\Models\DB\Proyecto;
//use App\Models\DB\ProyectoCategoria;
use App\Models\DB\Language;
use App\Vendor\Saizfact\Locale\Locale;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Imagen\Imagen;

class MediaController extends Controller
{
    protected $permissions  = ['ver.media', 'edit.media'];
    protected $folder;
    protected $imagen;
    protected $db_imagen;
    protected $db_folder;
    protected $media;
    
    protected $locale_slug_seo;
    

    function __construct(Media $media, Folder $folder, Imagen $imagen, DBImagen $db_imagen, DBFolder $db_folder, LocaleSlugSeo $locale_slug_seo)
    {
        $this->middleware('auth');

        $this->folder = $folder;
        $this->media = $media;
        $this->imagen = $imagen;
        $this->db_imagen = $db_imagen;
        $this->db_folder = $db_folder;
        $this->imagen->setType('media');
        
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_slug_seo->setParent('proyectos.form');
    }

    public function index()
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }

        $media = collect();
        
        $documents = $this->folder->where('entity_type','media')->get();

        foreach($documents as $document){
            $media->push($document);
        }

        $images = $this->db_imagen->where('entity_type','media')->where('grid', 'preview')->get();

        foreach($images as $image){
            $media->push($image);
        }
        
        return view('admin.media.index')->with('media', $media->sortByDesc('updated_at'));
    }

    public function create()
    {
       
    }

    public function store(MediaRequest $request)
    {   
        $media = Media::updateOrCreate([
            'id' => request('id')],[
            'rel_profile' => request('rel_profile'),
            'nombre' => request('nombre'),
            'orden' => request('orden'),
            'proyecto_id' => request('proyecto_id'),
            'tipo_id' => request('tipo_id'),
        ]);

        if(request('input_file')){

            //$name = Str::slug($media->nombre);
            $name = pathinfo(request('input_file')->getClientOriginalName(), PATHINFO_FILENAME);
            
            $extension = request('input_file')->clientExtension();
    
            if ($extension == 'jpeg' || $extension == 'jpg'|| $extension == 'gif' || $extension == 'png' ){
    
                $image = $this->imagen->store(request('input_file'), $media->id, request('rel_profile'), 'media_image');
    
            }else{

                $counter = 2;
                $path = $name.'.'.$extension;  

                while (Storage::disk('media_documents')->exists($path)) {
                    $path = $name.'-'. $counter.'.'.$extension;
                    $counter++;
                }

                $upload =  Storage::disk('media_documents')->put('', request('input_file'));
                Storage::disk('media_documents')->move($upload, $path);
    
                Folder::create([
                    'name' => '/' . $path,
                    'entity_type' => 'media-doc',
                    'entity_id' =>  $media->id,
                    'anchor'  => request('rel_profile'),
                    'filename' => $name . '.' . $extension,
                    'content_type' => 'document/'. $extension,
                    'size' => Storage::disk('media_documents')->size($path),
                    'width' => null,
                    'height' => null,
                ]);
                
            }
        }
        
        if (request('id')){
            $message = "Elemento actualizado ". $media->nombre;
        }else{
            $message = "Elemento creado ". $media->nombre;
        }
        $proyecto_id = request('proyecto_id');
        if (isset($proyecto_id)){
            
            $proyecto =  Proyecto::where('id',$proyecto_id)->first();
            
            $languages = Language::get();
            $folders_aux =  $proyecto->folders;
            
            $folders = array();
            $folders_name = array();
            foreach ($folders_aux as $folder){
                $folders[$folder->anchor]= $folder->filename;
                $folders_name[$folder->anchor] = $folder->name;
            }
            
                        
            $locale_aux =  new Locale();
            
            $locale_aux ->setParent('proyectos.form');
            $locale = $locale_aux ->show($proyecto->id);
            
            $medias = $this->media->where('proyecto_id',request('proyecto_id'))->orderBy('orden','ASC')->get();
            $images = collect();
            $docs = collect();
            foreach ($medias as $media){
                $image = $this->db_imagen
                ->where('entity_type','media')
                ->where('entity_id',$media->id)
                ->where('grid', 'preview')
                ->first();
                if (isset($image)){
                    $image->nombre = $media->nombre;
                    $images->push($image);
                }else{
                    //$obj = (object) array('entity_id'=>$media->id,'nombre'=>$media->nombre);
                    //$images->push($obj);
                    $doc = $this->db_folder
                    ->where('entity_type','media-doc')
                    ->where('entity_id',$media->id)
                    //->where('grid', 'parrilla')
                    ->first();
                    //$obj = (object) array('entity_id'=>$media->id,'nombre'=>$media->nombre);
                    if (isset($doc)){
                        $doc->nombre = $doc->name;
                        $docs->push($doc);
                    }
                    
                }
            }
            /*
            if($proyecto->categoria_id){
                $subcategorias_ini = collect(ProyectoSubcategoria::select('id')->where('categoria_id', $proyecto->categoria_id)->get())->pluck('id')->toArray();
            }else{
                $subcategorias_ini =[];
            }
            */
            
            $seo = $this->locale_slug_seo ->show($proyecto->id);
            
            $view = View::make('admin.proyectos.edit')
            ->with('proyecto',$proyecto)
            ->with('locale',$locale)
            ->with('languages',$languages)
            ->with('id',$proyecto_id)
            ->with('images',$images)
            ->with('docs',$docs)
            ->with('folders',$folders)
            ->with('folders_name',$folders_name)
            ->with('seo', $seo)
            //->with('subcategorias_ini', $subcategorias_ini)
            //->with('media', $media->where('proyecto_id',$proyecto->id)->sortByDesc('orden_at'));
            // compact('proyecto','locale','languages','folders','folders_name','id'));
            ;
            
            if(request()->ajax()) {
                $sections = $view->renderSections();
                
                return response()->json([
                    'layout' => $sections['content'],
                    'form_with_two_modals' => $sections['form_with_two_modals'],
                    'id' => $proyecto->id,
                    'message' => $message
                ]);
            }
            
            return $view;
        }else{
            $media = collect();
            
            $documents = $this->folder->where('entity_type','media')->get();
    
            foreach($documents as $document){
                $media->push($document);
            }
    
            $images = $this->db_imagen->where('entity_type','media')->where('grid', 'preview')->get();
    
            foreach($images as $image){
                $media->push($image);
            }
            
            $view = View::make('admin.media.index')
            ->with('media', $media->sortByDesc('updated_at'))
            ->renderSections();
            
            return response()->json([
                'layout' => $view['content'],
                'message' => $message,
            ]);
        }
        

        
    }

    public function show(Media $media)
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        
        $document = $this->folder
            ->where('entity_type','media-doc')
            ->where('entity_id', $media->id)
            ->first();
    
        $image = $this->db_imagen
            ->where('entity_type','media')
            ->where('grid','preview')
            ->where('entity_id', $media->id)
            ->first();
        
        $path = "";

        if($document != null){
            $content_type = $document['content_type'];
            $path = $document['name'];
        }
        
        else if($image != null){
            $content_type = $image['content_type'];
            $path = $image['path'];
        }else{
            $content_type = 'vacío';
            $path = 'vacío';
        }
        
        if (isset($media->proyecto_id)){
            if ($image != null)
                $view = View::make('admin.proyectos.modal');
            else
                $view = View::make('admin.proyectos.modal_doc');
        }else{
            $view = View::make('admin.media.modal');
        }

        $view = $view
        ->with('content_type', $content_type)
        ->with('path', $path)
        ->with('media', $media)
        ->render();    

        return response()->json([
            'modal' => $view,
        ]); 
    }

    public function destroy(Media $media)
    {
        //TODO: Hacer se que borran los ficheros 
        $document = $this->folder
            ->where('entity_type','media')
            ->where('entity_id', $media->id)
            ->first();

        $image = $this->db_imagen
            ->where('entity_type','media')
            ->where('entity_id', $media->id)
            ->first();
        
        if($document != null){
            $folder = 'media_documents';

            $this->folder
                ->where('entity_type','media')
                ->where('entity_id', $media->id)
                ->delete();

            Storage::disk('media_documents')->delete($document->name);
        }
        
        else if($image != null){
            $folder = 'media_image';
            $this->imagen->delete($media->id, $folder);
        }        

        $media->delete();
        
        $message = "Elemento multimedia borrado ";  

        $proyecto_id = $media->proyecto_id;
        if (isset($proyecto_id)){
            
            
            $proyecto =  Proyecto::where('id',$proyecto_id)->first();
            
            $languages = Language::get();
            
            $folders = array();
            
            if(isset($proyecto->folders)) {
                $folders_aux =  $proyecto->folders;
                
                $folders_name = array();
                foreach ($folders_aux as $folder){
                    $folders[$folder->anchor]= $folder->filename;
                    $folders_name[$folder->anchor] = $folder->name;
                }
            }
            
            
            $locale_aux =  new Locale();
            
            $locale_aux ->setParent('proyectos.form');
            $locale = $locale_aux ->show($proyecto->id);
            
            $medias = $this->media->where('proyecto_id',$proyecto_id)->orderBy('orden','ASC')->get();
            $images = collect();
            $docs = collect();
            foreach ($medias as $media){
                $image = $this->db_imagen
                ->where('entity_type','media')
                ->where('entity_id',$media->id)
                ->where('grid', 'preview')
                ->first();
                if (isset($image)){
                    $image->nombre = $media->nombre;
                    $images->push($image);
                }else{
                    //$obj = (object) array('entity_id'=>$media->id,'nombre'=>$media->nombre);
                    //$images->push($obj);
                    
                    $doc = $this->db_folder
                    ->where('entity_type','media-doc')
                    ->where('entity_id',$media->id)
                    //->where('grid', 'parrilla')
                    ->first();
                    if (isset($doc)){
                        $doc->nombre = $doc->name;
                        $docs->push($doc);
                    }
                    
                }
            }
            
            $seo = $this->locale_slug_seo ->show($proyecto->id);
            
            $view = View::make('admin.proyectos.edit')
            ->with('proyecto',$proyecto)
            ->with('locale',$locale)
            ->with('languages',$languages)
            ->with('id',$proyecto_id)
            ->with('images',$images)
            ->with('docs',$docs)
            ->with('folders',$folders)
            ->with('folders_name',$folders_name)
            ->with('seo', $seo)
            //->with('media', $media->where('proyecto_id',$proyecto->id)->sortByDesc('orden_at'));
            // compact('proyecto','locale','languages','folders','folders_name','id'));
            ;
            
            if(request()->ajax()) {
                $sections = $view->renderSections();
                
                return response()->json([
                    'layout' => $sections['content'],
                    'form_with_two_modals' => $sections['form_with_two_modals'],
                    'id' => $proyecto->id,
                    'message' => $message
                ]);
            }
            
            return $view;
        }else{
            $media = collect();
            
            $documents = $this->folder->where('entity_type','media')->get();
    
            foreach($documents as $document){
                $media->push($document);
            }
    
            $images = $this->db_imagen->where('entity_type','media')->where('grid', 'preview')->get();
    
            foreach($images as $image){
                $media->push($image);
            }
            
            $view = View::make('admin.media.index')
                ->with('media', $media->sortByDesc('updated_at'))
                ->renderSections();
            
            return response()->json([
                'layout' => $view['content'],
                'message' => $message,
            ]);
        }
    }

    public function filter(Request $request)
    {
        switch (request('filter')){
            
            case 'todos':

                $media = collect();
            
                $documents = $this->folder->where('entity_type','media')->get();
        
                foreach($documents as $document){
                    $media->push($document);
                }
        
                $images = $this->db_imagen->where('entity_type','media')->where('grid', 'preview')->get();
        
                foreach($images as $image){
                    $media->push($image);
                }
            
                break;

            case 'imagenes':

                $media = $this->db_imagen->where('entity_type','media')->where('grid', 'preview')->get();


            
                break;
        
            case 'documentos':

                $media = $this->folder->where('entity_type','media')->get();
            
                break;
        }

        $view = View::make('admin.media.index')
        ->with('media', $media)
        ->renderSections(); 
    
        return response()->json([
            'layout' => $view['content'],
            'portfolio' => $view['portfolio'],
        ]); 

    }
}
