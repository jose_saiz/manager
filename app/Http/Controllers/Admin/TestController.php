<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\TestRequest;
use App\Models\DB\Test;
use App\Models\DB\Language;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;
//use Illuminate\Support\Str;

class TestController extends Controller
{
    protected $permissions  = ['ver.tests', 'edit.tests'];
    protected $test;
    protected $language;
    protected $locale;
    protected $locale_categorias;
    protected $locale_slug_seo;
    protected $locale_slug_seo_categorias;
    protected $datatables;

    function __construct(Test $test,Language $language, Locale $locale, Datatables $datatables,Locale $locale_categorias, LocaleSlugSeo $locale_slug_seo, LocaleSlugSeo $locale_slug_seo_categorias)
    {
        $this->middleware('auth');

        $this->test = $test;
        $this->language = $language;
        
        $this->locale = $locale;
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_categorias = $locale_categorias;
        $this->locale_slug_seo_categorias = $locale_slug_seo;
        
        $this->datatables = $datatables;

        $this->test->activo = 1;
        $this->locale->setParent('tests.form');
        $this->locale_slug_seo->setParent('tests.form');
        $this->locale_categorias->setParent('tests.categorias.form');
        $this->locale_slug_seo_categorias->setParent('tests.categorias.form');
        
    }

    public function index()
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        return view('admin.tests.index');
    }

    public function indexJson()
    {
        $query = $this->test
        ->with('categoria')
        ->select('t_test.*');

        return $this->datatables->of($query)->toJson();   
    }

    public function create()
    {
        $view = View::make('admin.tests.edit')
        ->with('test', $this->test)
        ->with('locale', $this->locale->create())
        ->with('seo', $this->locale_slug_seo->create())
        ->renderSections();

        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
        ]);
    }

    public function store(TestRequest $request)
    {           
        $test = Test::updateOrCreate([
            'id' => request('id')],[
            'nombre' => request('nombre'),
            'categoria_id' => request('categoria_id'),
            'activo' => request('activo') ? 1 : 0,
            'orden' => request('orden'),
        ]);

        $test->touch();
        
        $languages = $this->language->where('activo' , 1)->get();
        $seo = request('seo');
        
        $locale_slug_seo_categoria_id = $this->locale_slug_seo_categorias->getIdsByCategory(request('categoria_id'))->pluck('id','rel_profile'); 
        
        foreach($languages as $language){
            $seo['title.'.$language->alias] = request('locale')['titulo.'.$language->alias];
            $seo['parent.'.$language->alias] = $locale_slug_seo_categoria_id[$language->alias];
        }
        $this->locale_slug_seo->setParent('tests.form');
        $locale_slug_seo = $this->locale_slug_seo->store($seo, $test->id);
        
        
        $locale_slug_seo = $this->locale_slug_seo->show($test->id); 
        
        $locale = $this->locale->store(request('locale'), $test->id);
        
        if (request('id')){
            $message = "Test actualizada ". $test->nombre;
        }else{
            $message = "Test creada ". $test->nombre;
        }

        $view = View::make('admin.tests.edit')
        ->with('test', $test)
        ->with('locale', $locale)
        ->with('seo', $locale_slug_seo)
        ->renderSections();        

        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
            'id' => $test->id,
            'message' => $message,
        ]);
    }

    public function show(Test $test)
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        
        $locale = $this->locale->show($test->id);
        $locale_slug_seo = $this->locale_slug_seo->show($test->id); 

        $view = View::make('admin.tests.edit')
        ->with('test', $test)
        ->with('locale', $locale)
        ->with('seo', $locale_slug_seo);   
        
        if(request()->ajax()) {
            $sections = $view->renderSections(); 
    
            return response()->json([
                'layout' => $sections['content'],
                'form' => $sections['form'],
            ]); 
        }
                
        return $view;
    }

    public function destroy(Test $test)
    {
        $test->delete();
        $this->locale->delete($test->id);

        $message = "Test borrado ". $test->nombre;

        $view = View::make('admin.tests.index')
            ->with('tests', $this->test->get())
            ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'view' => $view['table'],
            'message' => $message,
        ]);
    }

    public function reorderTable(Request $request)
    {
        $order = request('orden');

        if (is_array($order)) {
            foreach ($order as $index => $tableItem) {
                $item = $this->test->findOrFail($tableItem);
                $item->orden = $index + 1;
                $item->save();
            }
        }
    }
}
