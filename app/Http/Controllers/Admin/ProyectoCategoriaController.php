<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\View;
use Yajra\Datatables\Datatables;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\ProyectoCategoriaRequest;
use App\Models\DB\ProyectoCategoria;
use App\Models\DB\Language;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;
//use App\Exceptions\ProyectoCategoriaException;
use Exception;

class ProyectoCategoriaController extends Controller
{
    protected $permissions  = ['ver.categorias.proyectos', 'edit.categorias.proyectos'];
    protected $categoria;
    protected $language;
    protected $locale;
    protected $locale_slug_seo;
    protected $datatables;

    function __construct(ProyectoCategoria $categoria,Language $language, Locale $locale, Datatables $datatables, LocaleSlugSeo $locale_slug_seo)
    {
        $this->middleware('auth');
        
        $this->categoria = $categoria;
        $this->language = $language;
        
        $this->locale = $locale;
        $this->locale_slug_seo = $locale_slug_seo;
        $this->datatables = $datatables;

        $this->categoria->activo = 1;
        $this->locale->setParent('proyectos.categorias.form');
        $this->locale_slug_seo->setParent('proyectos.categorias.form');
    }

    public function index()
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        return view('admin.proyectos_categorias.index')
        ->with('categorias', $this->categoria->get())
        ->with('categoria', $this->categoria)
        ->with('locale', $this->locale->create())
        ->with('seo', $this->locale_slug_seo->create());
    }

    public function indexJson()
    {
        $query = $this->categoria
        ->select('t_proyecto_categoria.*');

        return $this->datatables->of($query)->toJson();   
    }

    public function create()
    {
        $view = View::make('admin.proyectos_categorias.index')
        ->with('categorias', $this->categoria->get())
        ->with('categoria', $this->categoria)
        ->with('locale', $this->locale->create())
        ->with('seo', $this->locale_slug_seo->create())
        ->renderSections();

        return response()->json([
//             'layout' => $view['content'],
//             'table' => $view['table'],
            'form' => $view['form'],
        ]);
    }

    public function store(ProyectoCategoriaRequest $request)
    {
        $categoria = ProyectoCategoria::updateOrCreate([
            'id' => request('id')],[
            'nombre' => request('nombre'),
            'activo' => request('activo') ? 1 : 0
        ]);
        
        $languages = $this->language->where('activo' , 1)->get();
        $seo = request('seo');
        foreach($languages as $language){
            $seo['title.'.$language->alias] = request('locale')['titulo.'.$language->alias];
        }
        $locale_slug_seo = $this->locale_slug_seo->store($seo, $categoria->id);
        
        $locale_slug_seo = $this->locale_slug_seo->show($categoria->id);
        
        $locale = $this->locale->store(request('locale'), $categoria->id);

        if (request('id')){
            $message = "Categoría actualizada ". $categoria->nombre;            
        }else{
            $message = "Categoría creada ". $categoria->nombre;              
        }

        $view = View::make('admin.proyectos_categorias.index')
        ->with('categorias', $this->categoria->get())
        ->with('categoria', $categoria)
        ->with('locale', $locale)
        ->with('seo', $locale_slug_seo);
               
        if(request()->ajax()) {
            $sections = $view->renderSections(); 
            return response()->json([
                'layout' => $sections['content'],
                //'table' => $sections['table'],
                'form' => $sections['form'],
                'id' => $categoria->id,
                'message' => $message,
            ]);
        }
        return $view;
    }

    public function show(ProyectoCategoria $categoria)
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        
        $locale = $this->locale->show($categoria->id);
        $locale_slug_seo = $this->locale_slug_seo->show($categoria->id); 
        
        $view = View::make('admin.proyectos_categorias.index')
        ->with('categorias', $this->categoria->get())
        ->with('categoria', $categoria)
        ->with('locale', $locale)
        ->with('seo', $locale_slug_seo);
        
        
        if(request()->ajax()) {
            $sections = $view->renderSections(); 
    
            return response()->json([
//                 'layout' => $sections['content'],
//                 'table' => $sections['table'],
                'form' => $sections['form'],
            ]); 
        }
                
        return $view;
    }

    public function destroy(ProyectoCategoria $categoria)
    {   

        try {
            $categoria->delete();
            $this->locale->delete($categoria->id);
        } catch (Exception $exception) {
            echo($exception);
            //throw new ProyectoCategoriaException($exception);
        }

        /*throw_unless($categoria->delete(), new ProyectoCategoriaException);*/
        
        /*try {
            $categoria->delete();

        }catch(QueryException $exception) {

            switch ($exception->errorInfo[0]){
                
                case '23000':
    
                    return response()->json([
                        'errors' =>  "Está intentando eliminar una categoría que está siendo usada por algunas 
                            Proyecto's, asigne una nueva categoría a las Proyecto's afectadas para poder eliminar esta 
                            categoría."
                    ], 422);

                    break;
    
                default:
    
                    return response()->json([
                        'errors' => $exception->errorInfo[2],
                    ], 422);
            }
        }*/

        $message = "Categoría borrada ". $categoria->nombre;

        $view = View::make('admin.proyectos_categorias.index')
        ->with('categorias', $this->categoria->get())
        ->with('categoria', $this->categoria)
        ->with('locale', $this->locale->create())
        ->renderSections();
        
        return response()->json([
//             'layout' => $view['content'],
//             'table' => $view['table'],
            'form' => $view['form'],
            'message' => $message,
        ]);
    }
}
