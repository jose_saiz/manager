<?php

namespace App\Http\Controllers\Admin;

//use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
//use Illuminate\Support\Facades\Lang;
//use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Models\DB\User;
use App\Models\DB\RoleUser;
use App\Http\Requests\Admin\UserRequest;
//use App\Mail\RememberMailCliente;
//use Debugbar;

class UserController extends Controller
{
    protected $permissions  = ['ver.proyectos','ver.usuarios', 'edit.usuarios'];
    protected $user;
    protected $datatables;

    public function __construct(User $user, Datatables $datatables)
    {
        $this->middleware('auth');
        
        $this->user = $user;
        $this->datatables = $datatables;

        $this->user->activo = 1;
    }
    
    public function index()
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        return view('admin.users.index');
    }

    public function indexJson()
    {
        $query = $this->user
        ->with('roles')
        ->with('factory')
        ->select('t_user.*');

        return $this->datatables
            ->of($query)
            ->toJson();   
    }
    
    public function create()
    {
        $view = View::make('admin.users.edit')
        ->with('user', $this->user)
        ->with('roles_assigned', $this->user->roles->pluck('id')->all())
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
        ]);
    }
    
    public function store(UserRequest $request)
    {   
        if (request('password') !== null) {
            $user = User::updateOrCreate([
                'id' => request('id')],[
                'name' => request('name'),
                'email' => request('email'),
                'factory_id' => request('factory_id'),
                'password' => bcrypt(request('password')),
                'activo' => request('activo') ? 1 : 0,
            ]);
            
        }else{
            $user = User::updateOrCreate([
                'id' => request('id')],[
                'name' => request('name'),
                'email' => request('email'),
                'factory_id' => request('factory_id'),
                'activo' => request('activo') ? 1 : 0,
            ]);
        }

        $user->revokeAllRoles();
        if (request('role_id') != null){
            foreach (request('role_id') as $key => $value) { 
                RoleUser::create([
                    'role_id' => $value, 
                    'user_id'  => $user->id 
                ]);
            } 
        }
        
        
        if (request('id')){
            $message = "Usuario actualizado ". $user->name;
        }else{
            $message = "Usuario creado ". $user->name;
        }

        $view = View::make('admin.users.edit')
        ->with('user', $user)
        ->with('roles_assigned', $user->roles->pluck('id')->all())
        ->renderSections();

        return response()->json([
            'layout' => $view['content'],
            'form' => $view['form'],
            'id' => $user->id,
            'message' => $message,
        ]);
    }
    
    public function show(User $user)
    {
        if (! Auth::guard('web')->user()->canAtLeast($this->permissions)) {
            return Auth::guard('web')->user()->redirectPermittedSection();
        }
        
        $view = View::make('admin.users.edit')
        ->with('user', $user)
        ->with('roles_assigned', $user->roles->pluck('id')->all());
        
        if(request()->ajax()) {
            $sections = $view->renderSections(); 
    
            return response()->json([
                'layout' => $sections['content'],
                'form' => $sections['form'],
            ]); 
        }
                
        return $view;
    }
    
    public function destroy(User $user)
    {
        $user->delete();

        $message = "Usuario borrado ". $user->name;

        $view = View::make('admin.users.index')
        ->with('users', $this->user->get())
        ->renderSections();
        
        return response()->json([
            'layout' => $view['content'],
            'view' => $view['table'],
            'message' => $message,
        ]);
    }

}

