<?php

namespace App\Http\Controllers\Front;

use App\Models\DB\ProyectoCategoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DB\Proyecto;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;
class ProyectoCategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    function __construct(Locale $locale,Locale $locale_categorias, LocaleSlugSeo $locale_slug_seo, LocaleSlugSeo $locale_slug_seo_categorias)
    {
        
        $this->locale = $locale;
        $this->locale->setProfile(app()->getLocale());
        $this->locale->setParent('proyectos.form');
        
        $this->locale_categoria = $locale_categorias;
        $this->locale_categoria->setProfile(app()->getLocale());
        $this->locale_categoria->setParent('proyectos.categorias.form');
        
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_slug_seo->setProfile(app()->getLocale());
        $this->locale_slug_seo->setParent('proyectos.form');
        
        $this->locale_slug_seo_categoria = $locale_slug_seo;
        $this->locale_slug_seo_categoria->setProfile(app()->getLocale());
        $this->locale_slug_seo_categoria->setParent('proyectos.categorias.form');
    }
    
    public function index($categoria)
    {
        //
        
        $this->locale->setProfile(app()->getLocale());
        $this->locale_categoria->setProfile(app()->getLocale());
        
        $this->locale_slug_seo->setProfile(app()->getLocale());
        $this->locale_slug_seo_categoria->setProfile(app()->getLocale());
        
        $proyectos_contents = $this->locale->getAllByLanguage();
        
        $proyecto_locale_slug_seo_categoria_contents =  $this->locale_slug_seo_categoria->getIdByLanguage($categoria);
        $proyecto_categoria_contents =  $this->locale_categoria->getIdByLanguage($proyecto_locale_slug_seo_categoria_contents->key);
        
        $this->locale_slug_seo->setParent('proyectos.form');
        $proyectos_locale_slug_seo_contents = $this->locale_slug_seo->getAllByLanguage();
        
        //App::setLocale('en');
        
        $proyectos = Proyecto::where('categoria_id',$proyecto_locale_slug_seo_categoria_contents->key)->get();
        $proyecto_categoria = ProyectoCategoria::where('id',$proyecto_locale_slug_seo_categoria_contents->key)->first();
        
        return view('front.proyectos.category')
        ->with('proyectos',$proyectos)
        ->with('proyecto_categoria',$proyecto_categoria)
        ->with('proyecto_categoria_contents',$proyecto_categoria_contents)
        ->with('proyectos_contents',$proyectos_contents)
        ->with('proyectos_locale_slug_seo_categoria_contents',$proyecto_locale_slug_seo_categoria_contents)
        ->with('proyectos_locale_slug_seo_contents',$proyectos_locale_slug_seo_contents)
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DB\ProyectoCategoria  $proyectoCategoria
     * @return \Illuminate\Http\Response
     */
    public function show(ProyectoCategoria $proyectoCategoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DB\ProyectoCategoria  $proyectoCategoria
     * @return \Illuminate\Http\Response
     */
    public function edit(ProyectoCategoria $proyectoCategoria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DB\ProyectoCategoria  $proyectoCategoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProyectoCategoria $proyectoCategoria)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DB\ProyectoCategoria  $proyectoCategoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProyectoCategoria $proyectoCategoria)
    {
        //
    }
}
