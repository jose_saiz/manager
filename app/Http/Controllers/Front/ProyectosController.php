<?php

namespace App\Http\Controllers\Front;

use App;
use App\Http\Controllers\Controller;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;
use App\Models\DB\ProyectoCategoria;
//use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\DB\Proyecto;

class ProyectosController extends Controller
{
    
    protected $proyecto;
    protected $locale;
    protected $locale_categorias;
    protected $locale_slug_seo;
    protected $locale_slug_seo_categorias;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(Locale $locale,Locale $locale_categorias, LocaleSlugSeo $locale_slug_seo, LocaleSlugSeo $locale_slug_seo_categorias)
    {
        
        $this->locale = $locale;
        $this->locale->setProfile(app()->getLocale());
        $this->locale->setParent('proyectos.form');
        
        $this->locale_categorias = $locale_categorias;
        $this->locale_categorias->setProfile(app()->getLocale());
        $this->locale_categorias->setParent('proyectos.categorias.form');
        
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_slug_seo->setProfile(app()->getLocale());
        $this->locale_slug_seo->setParent('proyectos.form');
        
        $this->locale_slug_seo_categorias = $locale_slug_seo;
        $this->locale_slug_seo_categorias->setProfile(app()->getLocale());
        $this->locale_slug_seo_categorias->setParent('proyectos.categorias.form');
    }
    
    public function index()
    {
        $this->locale->setProfile(app()->getLocale());
        $this->locale_categorias->setProfile(app()->getLocale());
        
        $this->locale_slug_seo->setProfile(app()->getLocale());
        $this->locale_slug_seo_categorias->setProfile(app()->getLocale());
        
        $proyectos_categorias_contents =  $this->locale_categorias->getAllByLanguage();
        $proyectos_contents = $this->locale->getAllByLanguage();
        
        $proyectos_locale_slug_seo_categorias_contents =  $this->locale_slug_seo_categorias->getAllByLanguage();
        $this->locale_slug_seo->setParent('proyectos.form');
        $proyectos_locale_slug_seo_contents = $this->locale_slug_seo->getAllByLanguage();
        
        //App::setLocale('en');
        
        $proyectos = Proyecto::all();
        $proyectos_categorias = ProyectoCategoria::all();
        

            
        return view('front.proyectos.index')
        ->with('proyectos',$proyectos)
        ->with('proyectos_categorias',$proyectos_categorias)
        ->with('proyectos_categorias_contents',$proyectos_categorias_contents)
        ->with('proyectos_contents',$proyectos_contents)
        ->with('proyectos_locale_slug_seo_categorias_contents',$proyectos_locale_slug_seo_categorias_contents)
        ->with('proyectos_locale_slug_seo_contents',$proyectos_locale_slug_seo_contents)
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DB\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function show(Proyecto $proyecto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DB\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function edit(Proyecto $proyecto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DB\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proyecto $proyecto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DB\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proyecto $proyecto)
    {
        //
    }
}
