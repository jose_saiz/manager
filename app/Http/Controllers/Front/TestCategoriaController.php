<?php

namespace App\Http\Controllers\Front;

use App\Models\DB\TestCategoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\DB\Test;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;
class TestCategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    function __construct(Locale $locale,Locale $locale_categorias, LocaleSlugSeo $locale_slug_seo, LocaleSlugSeo $locale_slug_seo_categorias)
    {
        
        $this->locale = $locale;
        $this->locale->setProfile(app()->getLocale());
        $this->locale->setParent('tests.form');
        
        $this->locale_categoria = $locale_categorias;
        $this->locale_categoria->setProfile(app()->getLocale());
        $this->locale_categoria->setParent('tests.categorias.form');
        
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_slug_seo->setProfile(app()->getLocale());
        $this->locale_slug_seo->setParent('tests.form');
        
        $this->locale_slug_seo_categoria = $locale_slug_seo;
        $this->locale_slug_seo_categoria->setProfile(app()->getLocale());
        $this->locale_slug_seo_categoria->setParent('tests.categorias.form');
    }
    
    public function index($categoria)
    {
        //
        
        $this->locale->setProfile(app()->getLocale());
        $this->locale_categoria->setProfile(app()->getLocale());
        
        $this->locale_slug_seo->setProfile(app()->getLocale());
        $this->locale_slug_seo_categoria->setProfile(app()->getLocale());
        
        $tests_contents = $this->locale->getAllByLanguage();
        
        $test_locale_slug_seo_categoria_contents =  $this->locale_slug_seo_categoria->getIdByLanguage($categoria);
        $test_categoria_contents =  $this->locale_categoria->getIdByLanguage($test_locale_slug_seo_categoria_contents->key);
        
        $this->locale_slug_seo->setParent('tests.form');
        $tests_locale_slug_seo_contents = $this->locale_slug_seo->getAllByLanguage();
        
        //App::setLocale('en');
        
        $tests = Test::where('categoria_id',$test_locale_slug_seo_categoria_contents->key)->get();
        $test_categoria = TestCategoria::where('id',$test_locale_slug_seo_categoria_contents->key)->first();
        
        return view('front.tests.category')
        ->with('tests',$tests)
        ->with('test_categoria',$test_categoria)
        ->with('test_categoria_contents',$test_categoria_contents)
        ->with('tests_contents',$tests_contents)
        ->with('tests_locale_slug_seo_categoria_contents',$test_locale_slug_seo_categoria_contents)
        ->with('tests_locale_slug_seo_contents',$tests_locale_slug_seo_contents)
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DB\TestCategoria  $testCategoria
     * @return \Illuminate\Http\Response
     */
    public function show(TestCategoria $testCategoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DB\TestCategoria  $testCategoria
     * @return \Illuminate\Http\Response
     */
    public function edit(TestCategoria $testCategoria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DB\TestCategoria  $testCategoria
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TestCategoria $testCategoria)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DB\TestCategoria  $testCategoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(TestCategoria $testCategoria)
    {
        //
    }
}
