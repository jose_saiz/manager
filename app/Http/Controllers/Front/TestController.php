<?php

namespace App\Http\Controllers\Front;

use App;
use App\Models\DB\Test;
//use App\Models\DB\TestCategoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;
//use Carbon\Language;

class TestController extends Controller
{
    
    protected $test;
    protected $locale;
    protected $locale_categorias;
    protected $locale_slug_seo;
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(Locale $locale,Locale $locale_categorias, LocaleSlugSeo $locale_slug_seo, LocaleSlugSeo $locale_categorias_slug_seo)
    {
        
        $this->locale = $locale;
        $this->locale->setProfile(app()->getLocale());
        $this->locale->setParent('tests.form');
        
        $this->locale_categorias = $locale_categorias;
        $this->locale_categorias->setProfile(app()->getLocale());
        $this->locale_categorias->setParent('tests.categorias.form');
        
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_slug_seo->setProfile(app()->getLocale());
        $this->locale_slug_seo->setParent('tests.form');
        
        $this->locale_categorias_slug_seo = $locale_slug_seo;
        $this->locale_categorias_slug_seo->setProfile(app()->getLocale());
        $this->locale_categorias_slug_seo->setParent('tests.categorias.form');
    }
    
    public function index($categoria, $test)
    {

        $this->locale_slug_seo->setParent('tests.form');
        $seo_test_contents = $this->locale_slug_seo->getIdByLanguage($test);
        $test = Test::where('id',$seo_test_contents->key)->first();
        
        $test_contents = $this->locale->getIdByLanguage($test->id);
        
        $this->locale_categorias_slug_seo->setParent('tests.categorias.form');
        $seo_test_categoria_contents = $this->locale_categorias_slug_seo->getIdByLanguage($categoria);
        $test_categoria_contents = $this->locale_categorias->getIdByLanguage($test->categoria_id);
        
        return view('front.test.index')
        ->with('test',$test)
        ->with('seo_test_categoria_contents',$seo_test_categoria_contents)
        ->with('seo_test_contents',$seo_test_contents)
        ->with('test_categoria_contents',$test_categoria_contents)
        ->with('test_contents',$test_contents)
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DB\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DB\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DB\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DB\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        //
    }
}
