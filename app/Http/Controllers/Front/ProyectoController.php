<?php

namespace App\Http\Controllers\Front;

use App;
use App\Models\DB\Proyecto;
//use App\Models\DB\ProyectoCategoria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
//use App\Http\Requests\Admin\TestRequest;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;
//use Carbon\Language;

class ProyectoController extends Controller
{
    
    protected $proyecto;
    protected $locale;
    protected $locale_categorias;
    protected $locale_slug_seo;
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(Locale $locale,Locale $locale_categorias, LocaleSlugSeo $locale_slug_seo, LocaleSlugSeo $locale_categorias_slug_seo)
    {
        
        $this->locale = $locale;
        $this->locale->setProfile(app()->getLocale());
        $this->locale->setParent('proyectos.form');
        
        $this->locale_categorias = $locale_categorias;
        $this->locale_categorias->setProfile(app()->getLocale());
        $this->locale_categorias->setParent('proyectos.categorias.form');
        
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_slug_seo->setProfile(app()->getLocale());
        $this->locale_slug_seo->setParent('proyectos.form');
        
        $this->locale_categorias_slug_seo = $locale_slug_seo;
        $this->locale_categorias_slug_seo->setProfile(app()->getLocale());
        $this->locale_categorias_slug_seo->setParent('proyectos.categorias.form');
    }
    
    public function index($categoria, $proyecto)
    {

        $this->locale_slug_seo->setParent('proyectos.form');
        $seo_proyecto_contents = $this->locale_slug_seo->getIdByLanguage($proyecto);
        $proyecto = Proyecto::where('id',$seo_proyecto_contents->key)->first();
        
        $proyecto_contents = $this->locale->getIdByLanguage($proyecto->id);
        
        $this->locale_categorias_slug_seo->setParent('proyectos.categorias.form');
        $seo_proyecto_categoria_contents = $this->locale_categorias_slug_seo->getIdByLanguage($categoria);
        $proyecto_categoria_contents = $this->locale_categorias->getIdByLanguage($proyecto->categoria_id);
        
        return view('front.proyecto.index')
        ->with('proyecto',$proyecto)
        ->with('seo_proyecto_categoria_contents',$seo_proyecto_categoria_contents)
        ->with('seo_proyecto_contents',$seo_proyecto_contents)
        ->with('proyecto_categoria_contents',$proyecto_categoria_contents)
        ->with('proyecto_contents',$proyecto_contents)
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DB\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function show(Proyecto $proyecto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DB\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function edit(Proyecto $proyecto)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DB\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Proyecto $proyecto)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DB\Proyecto  $proyecto
     * @return \Illuminate\Http\Response
     */
    public function destroy(Proyecto $proyecto)
    {
        //
    }
}
