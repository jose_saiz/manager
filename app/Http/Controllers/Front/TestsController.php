<?php

namespace App\Http\Controllers\Front;

use App;
use App\Http\Controllers\Controller;
use App\Vendor\Saizfact\Locale\LocaleSlugSeo;
use App\Vendor\Saizfact\Locale\Locale;
use App\Models\DB\TestCategoria;
//use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\DB\Test;

class TestsController extends Controller
{
    
    protected $test;
    protected $locale;
    protected $locale_categorias;
    protected $locale_slug_seo;
    protected $locale_slug_seo_categorias;
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function __construct(Locale $locale,Locale $locale_categorias, LocaleSlugSeo $locale_slug_seo, LocaleSlugSeo $locale_slug_seo_categorias)
    {
        
        $this->locale = $locale;
        $this->locale->setProfile(app()->getLocale());
        $this->locale->setParent('tests.form');
        
        $this->locale_categorias = $locale_categorias;
        $this->locale_categorias->setProfile(app()->getLocale());
        $this->locale_categorias->setParent('tests.categorias.form');
        
        $this->locale_slug_seo = $locale_slug_seo;
        $this->locale_slug_seo->setProfile(app()->getLocale());
        $this->locale_slug_seo->setParent('tests.form');
        
        $this->locale_slug_seo_categorias = $locale_slug_seo;
        $this->locale_slug_seo_categorias->setProfile(app()->getLocale());
        $this->locale_slug_seo_categorias->setParent('tests.categorias.form');
    }
    
    public function index()
    {
        $this->locale->setProfile(app()->getLocale());
        $this->locale_categorias->setProfile(app()->getLocale());
        
        $this->locale_slug_seo->setProfile(app()->getLocale());
        $this->locale_slug_seo_categorias->setProfile(app()->getLocale());
        
        $tests_categorias_contents =  $this->locale_categorias->getAllByLanguage();
        $tests_contents = $this->locale->getAllByLanguage();
        
        $tests_locale_slug_seo_categorias_contents =  $this->locale_slug_seo_categorias->getAllByLanguage();
        $this->locale_slug_seo->setParent('tests.form');
        $tests_locale_slug_seo_contents = $this->locale_slug_seo->getAllByLanguage();
        
        //App::setLocale('en');
        
        $tests = Test::all();
        $tests_categorias = TestCategoria::all();
        

            
        return view('front.tests.index')
        ->with('tests',$tests)
        ->with('tests_categorias',$tests_categorias)
        ->with('tests_categorias_contents',$tests_categorias_contents)
        ->with('tests_contents',$tests_contents)
        ->with('tests_locale_slug_seo_categorias_contents',$tests_locale_slug_seo_categorias_contents)
        ->with('tests_locale_slug_seo_contents',$tests_locale_slug_seo_contents)
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DB\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function show(Test $test)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DB\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function edit(Test $test)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DB\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Test $test)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DB\Test  $test
     * @return \Illuminate\Http\Response
     */
    public function destroy(Test $test)
    {
        //
    }
}
