<?php

namespace app\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class PartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            //'locale.description->en'=> 'required',
        ];
    }
    
    public function messages()
    {
        return [
            'name' => 'Name is required',
            'locale.description->en.required'=> 'English description is required',
        ];
    }
}
