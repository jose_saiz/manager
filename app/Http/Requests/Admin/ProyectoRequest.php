<?php

/*
|--------------------------------------------------------------------------
| Validaciones del formulario de la sección Proyectos
|--------------------------------------------------------------------------
|
| **authorize: determina si el usuario debe estar autorizado para enviar el formulario. 
|
| **rules: recoge las normas que se deben cumplir para validar el formulario. Los errores son 
|   devueltos en forma de objeto JSON en un error 422.
| 
| **messages: mensajes personalizados de error.
|    
*/

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProyectoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nombre' => 'required',
            'categoria_id' => 'required|numeric',
            'orden' => 'required',
            'locale.titulo->es'=> 'required',
            'locale.descripcion_corta->es'=> 'required',
            
            
        ];
    }

    public function messages()
    {
        return [
            'nombre.required' => 'El nombre es obligatorio',
            'categoria_id.required' => 'Debe seleccionar una categoría',
            'orden.required' => 'Debe indicar con un número en el campo orden en qué posición de la lista aparecera este Proyecto',
            'locale.titulo->es.required'=> 'El título en Español es obligatorio',
            'locale.descripcion_corta->es.required'=> 'La Descripción Corta en Español es obligatoria',
           
        ];
    }
}
