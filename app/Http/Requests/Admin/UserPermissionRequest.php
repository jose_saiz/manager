<?php

/*
|--------------------------------------------------------------------------
| Validaciones del formulario de la sección Permisos
|--------------------------------------------------------------------------
|
| **authorize: determina si el usuario debe estar autorizado para enviar el formulario. 
|
| **rules: recoge las normas que se deben cumplir para validar el formulario. Los errores son 
|   devueltos en forma de objeto JSON en un error 422.
| 
| **messages: mensajes personalizados de error.
|    
*/

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserPermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|min:3|max:64|regex:/^[a-záéíóúàèìòùäëïöüñ\s]+$/i',
            'slug' => ['required', 'max:255', Rule::unique('t_permission')->ignore($this->id)], 
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'El nombre es obligatorio',
            'name.min' => 'El mínimo de caracteres permitidos para el nombre son 3',
            'name.max' => 'El máximo de caracteres permitidos para el nombre son 64',
            'name.regex' => 'Sólo se aceptan letras en el nombre',
            'slug.required' => 'El slug es obligatorio',
            'slug.max' => 'El máximo de caracteres permitidos en el slug son 255',
            'slug.unique' => 'El slug ya existe',
        ];
    }
}
