<?php

/*
|--------------------------------------------------------------------------
| Validaciones del formulario de la sección Categorías de Proyectos'
|--------------------------------------------------------------------------
|
| **authorize: determina si el usuario debe estar autorizado para enviar el formulario. 
|
| **rules: recoge las normas que se deben cumplir para validar el formulario. Los errores son 
|   devueltos en forma de objeto JSON en un error 422.
| 
| **messages: mensajes personalizados de error.
|    
*/

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class ProyectoCategoriaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required',
            'locale.titulo->es'=> 'required',
        ];
    }

    public function messages()
    {
        return [
            'nombre' => 'El nombre es obligatorio',
            'locale.titulo->es.required'=> 'El título en Español es obligatorio',
        ];
    }
}
