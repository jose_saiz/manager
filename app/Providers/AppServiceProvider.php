<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema; //NEW: Import Schema
use Illuminate\Support\Facades\Blade;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191); //NEW: Increase StringLength
        
        /*
        
        View::share('key', 'value');
        Schema::defaultStringLength(191);
       
        //view()->composer('*', function ($view)
        //{
            $working_days = array();
            
            //$working_days_aux =  Workday::where('neighborhood_factory_id', Auth::user()->factory_id)
            $working_days_aux =  Workday::where('neighborhood_factory_id', )
            ->where('category_id',3)
            ->get();
            
            
            foreach ($working_days_aux as $working_day){
                $working_days[$working_day['day']]['froms']['hour'] = explode(':', $working_day['begin_time'])[0];
                $working_days[$working_day['day']]['from']['minute'] = explode(':', $working_day['begin_time'])[1];
                $working_days[$working_day['day']]['to']['hour'] = explode(':', $working_day['end_time'])[0];
                $working_days[$working_day['day']]['to']['minute'] = explode(':', $working_day['end_time'])[1];
            }
            
            //$company=DB::table('company')->where('id',1)->first();
            config(['working_days' => $working_days]);
           
        //})
        */;
        
    }
}
