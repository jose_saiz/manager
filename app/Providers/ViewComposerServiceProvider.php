<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        
        view()->composer(
            ['admin.*', 'front.*'],
            'App\Http\ViewComposers\Admin\Locale'
            );
        
        view()->composer(
            'admin.roles.edit',
            'App\Http\ViewComposers\Admin\UsersPermissions'
            );
        
        view()->composer(
            'admin.users.edit',
            'App\Http\ViewComposers\Admin\UsersRoles'
            );
        
        view()->composer([
            'admin.printers.edit',
            'admin.printers.index'],
            'App\Http\ViewComposers\Admin\PrintersCategories'
            );
        view()->composer([
            'admin.parts.state','admin.partials.modal_part'],
            'App\Http\ViewComposers\Admin\PrintersErrors'
            );
        view()->composer([
            'admin.orders.edit',
            'admin.orders.quick_edit',
            'admin.orders.index'],
            'App\Http\ViewComposers\Admin\Products'
            );
        view()->composer([
            'admin.orders.edit',
            'admin.orders.quick_edit',
            'admin.orders.index'],
            'App\Http\ViewComposers\Admin\Customers'
            );
        view()->composer([
            'admin.users.edit',
            'admin.users.index'],
            'App\Http\ViewComposers\Admin\Factories'
            );
        view()->composer([
            'admin.orders.edit',
            'admin.orders.quick_edit',
            'admin.orders.index'],
            'App\Http\ViewComposers\Admin\StatesOrders'
            );
        view()->composer([
            'admin.tests.edit',
            'admin.tests.index'],
            'App\Http\ViewComposers\Admin\TestsCategorias'
            );
        view()->composer([
            'admin.proyectos.edit',
            'admin.proyectos.index'],
            'App\Http\ViewComposers\Admin\ProyectosCategorias'
            );
        view()->composer(
            ['admin.*'],
            'App\Http\ViewComposers\Admin\Summary'
            );
    }

    
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
