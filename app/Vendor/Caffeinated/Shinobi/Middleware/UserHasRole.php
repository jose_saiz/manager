<?php

namespace App\Vendor\Caffeinated\Shinobi\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use App\Models\DB\User;

class UserHasRole
{
    /**
     * @var Illuminate\Contracts\Auth\Guard
     */
    protected $auth;

    /**
     * Create a new UserHasPermission instance.
     *
     * @param Guard $auth
     */
    public function __construct(Guard $auth)
    {
        $this->auth = $auth;

        
    }

    /**
     * Run the request filter.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $closure
     * @param string                   $role
     *
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        //Saizfact:  para que si el usuario no se identificado no de error      
        if ($this->auth->user() !=null){
            if ($this->auth->user()->isRole($role)) {
                if ($request->ajax()) {
                    /*return response('Unauthorized.', 401);*/
                    return redirect()->guest(route('login'));
                }
                //return abort(401);
                return $next($request);
            }
        }else{
            return redirect()->guest(route('login'));
        }
        return $next($request);
    }
}
