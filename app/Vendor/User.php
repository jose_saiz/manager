<?php

namespace App;

use App\Vendor\Caffeinated\Shinobi\Traits\ShinobiTrait;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use ShinobiTrait;
    
    protected $fillable = ['name', 'email', 'password', 'activo'];
    
    protected $table = 't_user';
    
    // public function rolesAssigned()
    // {
    //     return $this->hasManyThrough(Role::class, RoleUser::class, 'role_id', 'id');
    // }
    
    /**
     * Devuelve la fecha created_at formateada
     *
     * @param string $format
     * @return string
     */
    public function createdAtFormatted($format = 'd/m/Y H:i')
    {
        if ($this->created_at == null) {
            return null;
        }
        return $this->created_at->format($format);
    }
    
    /**
     * Devuelve la fecha updated_at formateada
     *
     * @param string $format
     * @return string
     */
    public function updatedAtFormatted($format = 'd/m/Y H:i')
    {
        if ($this->updated_at == null) {
            return null;
        }
        return $this->updated_at->format($format);
    }
    
    public function isSaizfact() {
        return strstr($this->email, 'jose.saiz@gmail.com');
    }
