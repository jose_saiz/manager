<?php

/*
|--------------------------------------------------------------------------
| Traducciones
|--------------------------------------------------------------------------
|   
| Trabaja en combinación con /views/admin/partials/locale
|
| Documentación completa de este componente en README.md 
|    
*/

namespace App\Vendor\Saizfact\Locale;

use App\Models\DB\Locale as DBLocale;
use App\Models\DB\Language;
use Illuminate\Support\Str;

class Locale
{
    protected $rel_parent;
    protected $rel_profile;

    public function setParent($rel_parent)
    {
        $this->rel_parent = $rel_parent;
    }

    public function getParent()
    {
        return $this->rel_parent;
    }
    
    public function setProfile($rel_profile)
    {
        $this->rel_profile = $rel_profile;
    }

    public function create()
    {
        return new DBLocale();
    }

    public function store($locale, $key, $rel_parents = null)
    {  

        foreach ($locale as $rel_anchor => $value){

            $rel_anchor = str_replace(['-', '_'], ".", $rel_anchor); 
            $explode_rel_anchor = explode('.', $rel_anchor);
            $rel_profile = end($explode_rel_anchor);
            array_pop($explode_rel_anchor); 
            $tag = implode(".", $explode_rel_anchor); 

            if($rel_parents !== null){
                $this->rel_parent = Str::slug($rel_parents['titulo.'. $rel_profile]);
            }

            $locale[] = DBLocale::updateOrCreate([
                    'key' => $key,
                    'rel_parent' => $this->rel_parent,
                    'rel_profile' => $rel_profile,
                    'rel_anchor' => $rel_anchor],[
                    'rel_parent' => $this->rel_parent,
                    'rel_anchor' => $rel_anchor,
                    'rel_profile' => $rel_profile,
                    'tag' => $tag,
                    'value' => $value,
            ]);
        }

        return $locale;
    }

    public function show($key)
    {
        return DBLocale::getValues($this->rel_parent, $key)->pluck('value','rel_anchor')->all();   
    }

    public function delete($key)
    {
        if (DBLocale::getValues($this->rel_parent, $key)->count() > 0) {

            DBLocale::getValues($this->rel_parent, $key)->delete();   
        }
    }
    
    public function languages(){ 
        return Language::where('activo', 1)->get();
    }

    public function getIdByLanguage($key){ 
        return DBLocale::getIdByLanguage($this->rel_parent, $this->rel_profile, $key)->pluck('value','tag')->all();
    }

    public function getAllByLanguage(){ 
        $items = DBLocale::getAllByLanguage($this->rel_parent, $this->rel_profile)->get()->groupBy('key');

        $items =  $items->map(function ($item) {
            return $item->pluck('value','tag');
        });

        return $items;
    }

    public function updateRelParent($older_parent, $new_parent){

        foreach (languages() as $language){
            return DBLocale::updateRelParent($older_parent['titulo.'.$language->alias], Str::slug($new_parent['titulo.'.$language->alias]));
        }
    }
    
}
    

