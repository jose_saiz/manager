<?php

namespace App\Vendor\Saizfact\Locale;

use App\Vendor\Saizfact\Locale\Models\LocaleSlugSeo as DBLocaleSlugSeo;
use Illuminate\Support\Str;

class LocaleSlugSeo
{
    protected $rel_parent;
    protected $rel_profile;
    protected $locale_slug_seo;

    function __construct(DBLocaleSlugSeo $locale_slug_seo)
    {
        $this->locale_slug_seo = $locale_slug_seo;
    }

    public function setParent($rel_parent)
    {
        $this->rel_parent = $rel_parent;
    }

    public function getParent()
    {
        return $this->rel_parent;
    }

    public function setProfile($rel_profile)
    {
        $this->rel_profile = $rel_profile;
    }

    public function create()
    {
        return new DBLocaleSlugSeo();
    }
    
    public function store($seo, $key)
    {  

        foreach ($seo as $rel_anchor => $value){

            $rel_anchor = str_replace(['-', '_'], ".", $rel_anchor); 
            $explode_rel_anchor = explode('.', $rel_anchor);
            $rel_profile = end($explode_rel_anchor);
            $locale_slug_seo_aux = array();

            $locale_slug_seo = $this->locale_slug_seo::updateOrCreate([
                'rel_profile' => $rel_profile,
                'rel_parent' => $this->rel_parent,
                'key' => $key],[
                'rel_profile' => $rel_profile,
                'rel_parent' => $this->rel_parent,
                'key' => $key,
                'parent_slug' => isset($seo['parent.'. $rel_profile]) ? $seo['parent.'. $rel_profile] : null,
                'slug' => Str::slug($seo['title.'. $rel_profile]),
                'title' => $seo['title.'. $rel_profile] ,
                'description' => isset($seo['description.'. $rel_profile])? $seo['description.'. $rel_profile] : '' ,
                'keywords' => isset($seo['keywords.'. $rel_profile])? $seo['keywords.'. $rel_profile] : '' ,
            ]);
        }

        return $locale_slug_seo;
    }

    public function show($key)
    {
        $seo = null;
        $values =  DBLocaleSlugSeo::getValues($this->rel_parent, $key)->get();   

        foreach($values as $value){
            $seo['title.'.$value->rel_profile] = $value->title;
            $seo['description.'.$value->rel_profile] = $value->description;
            $seo['keywords.'.$value->rel_profile] = $value->keywords;
        }

        return $seo;
    }

    public function delete($key)
    {
        if (DBLocaleSlugSeo::getValues($this->rel_parent, $key)->count() > 0) {

            DBLocaleSlugSeo::getValues($this->rel_parent, $key)->delete();   
        }
    }

    public function getIdByLanguage($slug){ 
        return  DBLocaleSlugSeo::getIdByLanguage($this->rel_parent, $this->rel_profile, $slug)->first();
    }

    public function getIdByKey($rel_parent, $rel_profile, $key){ 
        return  DBLocaleSlugSeo::getIdByKey($rel_parent, $rel_profile, $key)->first()->id;
    }
    
    public function getIdByOnlyKey($key){
        return  DBLocaleSlugSeo::getIdByKey($this->rel_parent, $this->rel_profile, $key)->first()->slug;
    }

    public function getIdsByCategory($key){ 
        return DBLocaleSlugSeo::getIdsByCategory($this->rel_parent,$key)->get();
    }
    
    public function getSectionsParameters(){
        return  DBLocaleSlugSeo::getSectionsParameters($this->rel_parent, $this->rel_profile)->get();
    }
    public function getAllByLanguage(){
        
        $items = DBLocaleSlugSeo::getAllByLanguage($this->rel_parent, $this->rel_profile)->get()->groupBy('key');
        
        $items =  $items->map(function ($item) {
            return $item->pluck('slug','title');
        });
            
            return $items;
    }
    public function getAllByLanguageNew(){
        
        $items = DBLocaleSlugSeo::getAllByLanguage($this->rel_parent, $this->rel_profile)->get()->groupBy('key');
        
        $items =  $items->map(function ($item) {
            return $item[0]->pluck('slug','key');
        });
        return $items;
    }
}
    

