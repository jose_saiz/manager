<?php

namespace App\Vendor\Saizfact\Locale\Console;

use App\Vendor\Saizfact\Locale\Manager;
use Illuminate\Console\Command;

class FindCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'translations:find {name? : The path where to search. Default is the project path.}';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Find translations in php/twig files';
    
    /** @var App/Vendor/Saizfact/Locale/Manager */
    protected $manager;

    public function __construct(Manager $manager)
    {
        $this->manager = $manager;
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $counter = $this->manager->findTranslations($this->argument('name'));
        $this->info('Done importing, processed ' . $counter . ' items!');
    }
}
