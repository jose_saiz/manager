<?php

namespace App\Vendor\Saizfact\Locale\Exceptions;

use Exception;

class UnsupportedLocaleException extends Exception
{
}
