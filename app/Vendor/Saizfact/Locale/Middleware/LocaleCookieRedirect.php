<?php 

namespace App\Vendor\Saizfact\Locale\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;

class LocaleCookieRedirect extends LocalizationSeoMiddlewareBase 
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     *
     * @return mixed
     */
     public function handle($request, Closure $next) {
         // If the URL of the request is in exceptions.
         if ($this->shouldIgnore($request)) {
             return $next($request);
         }

         $params = explode('/', $request->path());
         $locale = $request->cookie('locale', false);

         if (\count($params) > 0 && app('localizationseo')->checkLocaleInSupportedLocales($params[0])) {
            return $next($request)->withCookie(cookie()->forever('locale', $params[0]));
         }

         if ($locale && app('localizationseo')->checkLocaleInSupportedLocales($locale) && !(app('localizationseo')->getDefaultLocale() === $locale && app('localizationseo')->hideDefaultLocaleInURL())) {
           $redirection = app('localizationseo')->getLocalizedURL($locale);
           $redirectResponse = new RedirectResponse($redirection, 302, ['Vary' => 'Accept-Language']);

           return $redirectResponse->withCookie(cookie()->forever('locale', $params[0]));
         }

         return $next($request);
     }
}
