<?php

namespace App\Vendor\Saizfact\Locale\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;

class LocaleSessionRedirect extends LocalizationSeoMiddlewareBase
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // If the URL of the request is in exceptions.
        if ($this->shouldIgnore($request)) {
            return $next($request);
        }

        $params = explode('/', $request->path());
        $locale = session('locale', false);

        if (\count($params) > 0 && app('localizationseo')->checkLocaleInSupportedLocales($params[0])) {
            session(['locale' => $params[0]]);

            return $next($request);
        }

        if ($locale && app('localizationseo')->checkLocaleInSupportedLocales($locale) && !(app('localizationseo')->getDefaultLocale() === $locale && app('localizationseo')->hideDefaultLocaleInURL())) {
            app('session')->reflash();
            $redirection = app('localizationseo')->getLocalizedURL($locale);

            return new RedirectResponse($redirection, 302, ['Vary' => 'Accept-Language']);
        }

        return $next($request);
    }
}
