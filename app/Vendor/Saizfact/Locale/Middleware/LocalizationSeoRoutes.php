<?php

namespace App\Vendor\Saizfact\Locale\Middleware;

use Closure;
use Exception;
use App\Vendor\Saizfact\Locale\Models\LocaleSeo;

class LocalizationSeoRoutes extends LocalizationSeoMiddlewareBase
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        // If the URL of the request is in exceptions.
        if ($this->shouldIgnore($request)) {
            return $next($request);
        }

        $app = app();

        $routeName = $app['app\vendor\saizfact\locale\localizationseo']->getRouteNameFromAPath($request->getUri());

        $app['app\vendor\saizfact\locale\localizationseo']->setRouteName($routeName);

        return $next($request);
    }
}
