<?php

namespace App\Vendor\Saizfact\Locale\Models;

use Illuminate\Database\Eloquent\Model;

class LocaleSlugSeo extends Model
{
    protected $table = 't_locale_slug_seo';
    protected $guarded = [];

    public function parents()
    {
        return $this->belongsTo(LocaleSlugSeo::class, 'parent_slug')->with('parents');
    }

    public function scopeGetValues($query, $rel_parent, $key){
        
        return $query->where('key', $key)
            ->where('rel_parent', $rel_parent);
    }

    public function scopeGetIdByLanguage($query, $rel_parent, $rel_profile, $slug){
        
        return $query->where('slug', $slug)
            ->where('rel_profile', $rel_profile)
            ->where('rel_parent', $rel_parent);
    }

    public function scopeGetIdByKey($query, $rel_parent, $rel_profile, $key){ 
        return $query->where('rel_parent', $rel_parent)
            ->where('rel_profile', $rel_profile)
            ->where('key', $key);
    }
    public function scopeGetAllByKey($query, $rel_parent, $rel_profile, $key){
        return $query->select('*')
        ->where('rel_parent', $rel_parent)
        ->where('rel_profile', $rel_profile)
        ->where('key', $key);
    }

    public function scopeGetIdsByCategory($query,$rel_parent,$key){
        return $query->select('*')
        ->where('rel_parent', $rel_parent)
        ->where('key', $key);
    }
    public function scopeGetSectionsParameters($query, $rel_parent, $rel_profile){

        return $query->where('rel_profile', $rel_profile)
            ->where('rel_parent', $rel_parent)
            ->where('slug', '!=' , null);
    }
    
    public function scopeGetAllByLanguage($query, $rel_parent, $rel_profile){
        
        return $query->where('rel_profile', $rel_profile)
        ->where('rel_parent', $rel_parent);
    }

}
