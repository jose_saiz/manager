<?php 

namespace App\Vendor\Saizfact\Locale\Models;

use Illuminate\Database\Eloquent\Model;
//use DB;

class LocaleRedirect extends Model{

    protected $table = 't_locale_redirect';
    protected $guarded = [];

    public function current_url()
    {
        return $this->belongsTo(LocaleSeo::class, 'locale_seo_id');
    }

}
