<?php

/*
|--------------------------------------------------------------------------
| Tratamiento de imágenes 
|--------------------------------------------------------------------------
|
| Se hace uso de la siguiente librería para el tratamiento de imágenes: http://image.intervention.io/
| instalada a través de composer.json
|
| *function store:
|   
|   $file recoge la imagen que se ha introducido en input_file, 
|   $anchor optativamente se puede definir el idioma, si se utilizan distinta imagen por idioma. 
|
|   $settings son recogidos de la tabla de la base de datos t_imagen_configuracion donde se especifica la 
|   extensión que tendrá el archivo guardado, la calidad de la imagen, su alto (no se especifica ancho 
|   porque heighten redimensiona respetando las proporciones de la original) y la carpeta donde será 
|   guardada.
|
|   Se realiza un foreach porque se guarda cada imagen en los distintos formatos de tamaño que requieren 
|   en las diferentes partes de la web. 
|    
*/

namespace App\Vendor\Saizfact\Imagen;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Models\DB\ImagenConfiguracion;
use App\Models\DB\Imagen as DBImagen;

class Imagen
{
	protected $entity_type;
	protected $rel_profile;
	
	public function setType($entity_type)
	{
		$this->entity_type = $entity_type;
	}

	public function setProfile($rel_profile)
    {
        $this->rel_profile = $rel_profile;
    }

	public function store($file, $entity_id, $rel_profile, $folder){

		$name = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);

		self::delete($entity_id, $folder);
		
		$settings = ImagenConfiguracion::where('entity_type', $this->entity_type)->get();

		foreach ($settings as $setting => $value) {

			//la barra dentro de content_type distingue diferentes formatos de archivos permitidos
			$extension = explode("/", $value->content_type);
		   
			
			$value->content_type = $file->extension();
			
			/*if(in_array($file->extension(), $extension)){
				$value->content_type = $file->extension();
			}else{
				$value->content_type = $extension[0];
			}*/
			
			$path = $value->directory . $name . '.' . $value->content_type;
			$path = str_replace(" ", "-", $path);

			//Si existe un archivo con el mismo nombre usaremos counter para diferenciarlos
			$counter = 2;

			while (Storage::disk($folder)->exists($path)) {
				$path =  $value->directory . $name.'-'. $counter.'.'. $value->content_type;;
				$counter++;
			}
			
			// if($value->content_type == 'gif'){
			//     $path =  str_replace("idb/", "", $path);
			//     $upload = Storage::disk('public_uploads')->put('media/', $file);
			//     Storage::disk('public_uploads')->move($upload, $path);   
			//     $path = "idb/". $path; 
			// }

			if(isset($value->height)){
				
				$image = Image::make($file);
				
				$image->resize(null, $value->height, function ($constraint) {
					$constraint->aspectRatio();
				});

				Storage::disk($folder)->put($path, (string) $image->encode($value->content_type, $value->quality));
			
			}else{
				$image = Image::make($file)
				->encode($value->content_type, $value->quality);

				Storage::disk($folder)->put($path, (string) $image);
			}

			DBImagen::create([
				'path' => $path,
				'grid' => $value->grid,
				'entity_type' => $this->entity_type,
				'entity_id' => $entity_id,
				'rel_profile' => $rel_profile,
				'filename' => $name . '.' . $value->content_type,
				'content_type' => 'image/'. $value->content_type,
				'size' => Storage::disk($folder)->size($path),
				'width' => $value->width,
				'height' => $value->height,
				'quality' => $value->quality,
			]);
		}

		return DBImagen::getPreviewImage($this->entity_type, $entity_id, $rel_profile)->first();
	}

	public function storeFile($pathToFile, $entity_id, $rel_profile, $folder){
	    $partes_ruta = pathinfo($pathToFile);
	    $name = basename($pathToFile);
	    
	    self::delete($entity_id, $folder);
	    
	    $settings = ImagenConfiguracion::where('entity_type', $this->entity_type)->get();
	    //dd($settings.'    '.$this->entity_type);
	    foreach ($settings as $setting => $value) {
	        
	        //la barra dentro de content_type distingue diferentes formatos de archivos permitidos
	        $extension = explode("/", $value->content_type);
	        
	        if(in_array($partes_ruta['extension'], $extension)){
	            $value->content_type = $partes_ruta['extension'];
	        }else{
	            $value->content_type = $extension[0];
	        }
	        
	        $path = $value->directory . $name . '.' . $value->content_type;
	        $path = str_replace(" ", "-", $path);
	        
	        //Si existe un archivo con el mismo nombre usaremos counter para diferenciarlos
	        $counter = 2;
	        //dd($folder.'-----'.$pathToFile.' ----   '.$path);
	        while (Storage::disk($folder)->exists($path)) {
	            $path =  $value->directory . $name.'-'. $counter.'.'. $value->content_type;;
	            $counter++;
	        }
	        
	        // if($value->content_type == 'gif'){
	        //     $path =  str_replace("idb/", "", $path);
	        //     $upload = Storage::disk('public_uploads')->put('media/', $file);
	        //     Storage::disk('public_uploads')->move($upload, $path);
	        //     $path = "idb/". $path;
	        // }
	        
	        if(isset($value->height)){
	            
	            $image = Image::make($pathToFile);
	            
	            $image->resize(null, $value->height, function ($constraint) {
	                $constraint->aspectRatio();
	            });
	                Storage::disk($folder)->put($path, (string) $image->encode($value->content_type, $value->quality));
	                
	        }else{
	            $image = Image::make($pathToFile)->encode($value->content_type, $value->quality);
	            Storage::disk($folder)->put($path, (string) $image);
	        }
	        
	        DBImagen::create([
	            'path' => $path,
	            'grid' => $value->grid,
	            'entity_type' => $this->entity_type,
	            'entity_id' => $entity_id,
	            'rel_profile' => $rel_profile,
	            'filename' => $name . '.' . $value->content_type,
	            'content_type' => 'image/'. $value->content_type,
	            'size' => Storage::disk($folder)->size($path),
	            'width' => $value->width,
	            'height' => $value->height,
	            'quality' => $value->quality,
	        ]);
	}
	
	return DBImagen::getPreviewImage($this->entity_type, $entity_id, $rel_profile)->first();
	}
	
	public function show($entity_id, $rel_profile)
	{
	    return DBImagen::getPreviewImage($this->entity_type, $entity_id, $rel_profile)->first();
	}

	public function getAllByLanguage(){ 

        $items = DBImagen::getAllByLanguage($this->entity_type, $this->rel_profile)->get()->groupBy('entity_id');

        $items =  $items->map(function ($item) {
            return $item->pluck('path','grid');
        });

        return $items;
    }
    
    public function getAllByLanguageForId($entity_id){
        
        $items = DBImagen::getAllByLanguageForId($this->entity_type, $entity_id,  $this->rel_profile)->get()->groupBy('entity_id');
        $items =  $items->map(function ($item) {
            return $item->pluck('path','grid');
        });
            
            return $items;
    }

	public function delete($entity_id, $folder)
	{
		if (DBImagen::getImages($this->entity_type, $entity_id)->count() > 0) {

			$images = DBImagen::getImages($this->entity_type, $entity_id);

			foreach ($images->get() as $image){
				$path =  ltrim($image->path, '/');
				Storage::disk($folder)->delete($path);
			}
			
			$images->delete();
		}
	}
}
