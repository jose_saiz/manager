<?php
namespace App\Vendor\Saizfact\Folder;

use App\Models\DB\Folder as ModelFolder;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Folder
{

    const FORCE_GEOMETRY = 9231;

    const FILL_GEOMETRY = 9232;

    const FOLDER_MD5_SIZE = 15;

    private $geomScale = false;

    private $geomForce = false;

    private $table = null;

    private $entityType = null;

    private $entityId = null;

    private $path = null;

    private $folder = array();

    public function __construct($entityType, $entityId, $path = 'idb/')
    {
        $this->entityType = $entityType;
        $this->entityId = $entityId;
        $this->path = $path;
        $this->table = $table = 't_folder';
        
    }

    /**
     * Add a file
     */
    public function add(Descriptor $descriptor, $anchor, $documentId = null)
    {
        if ($descriptor->getFilename()) {
            do {
                $filename = 'f' . substr(md5(uniqid(rand())), 0, self::FOLDER_MD5_SIZE) . '.' . $descriptor->getExtension();
            } while (file_exists($this->path . $filename));
            
            copy($descriptor->getFilename(), $this->path . $filename);
            App::getInstance()->getConnection()->insert($this->table, array(
                'name' => $descriptor->getName(),
                'inserted_at' => 'CURRENT_TIMESTAMP',
                'updated_at' => 'CURRENT_TIMESTAMP',
                'entity_type' => $this->entityType,
                'entity_id' => $this->entityId,
                'anchor' => $anchor,
                'filename' => $filename,
                'content_type' => $descriptor->getContentType(),
                'size' => $descriptor->getFilesize(),
                'width' => $descriptor->getWidth(),
                'height' => $descriptor->getHeight(),
                'document_id' => $documentId
            ));
        }
    }

    /**
     * Add a file, but if file exist , the file is update
     */
    public function update(Descriptor $descriptor, $entity_type, $entity_id , $anchor)
    {
        //dd($descriptor->getFilename());
        if ($descriptor->getFilename()) {
            /*
            $row = App::getInstance()->getConnection()->queryRow("
                SELECT * FROM " . $this->table . " 
                WHERE entity_type = '" . $this->entityType . "'" . " 
                AND entity_id = " . $this->entityId . " 
                AND anchor = '" . $anchor . "'");
            */
            $row = ModelFolder::where('entity_type',$entity_type)
                        ->where('entity_id',$entity_id)
                        ->where('anchor',$anchor)
                        ->first();
            //dd(isset($row['id']));
            /*do {
                $filename = 'f' . substr(md5(uniqid(rand())), 0, self::FOLDER_MD5_SIZE) . '.' . $descriptor->getExtension();
            } while (file_exists($this->path . $filename));*/
            $filename =$descriptor->getName();
            if (isset($row['id'])) {
                $currentFilename = $this->path . $row['filename'];
                if (file_exists($currentFilename)) {
                    unlink($currentFilename);
                }
                copy($descriptor->getFilename(), $this->path . $filename);
                /*
                App::getInstance()->getConnection()->update($this->table, array(
                    'name' => $descriptor->getName(),
                    'updated_stamp' => 'CURRENT_TIMESTAMP',
                    'filename' => $filename,
                    'content_type' => $descriptor->getContentType(),
                    'size' => $descriptor->getFilesize(),
                    'width' => $descriptor->getWidth(),
                    'height' => $descriptor->getHeight()
                    
                    
                ), array(
                    'id' => $row['id']
                ));
                
                $folder = new ModelFolder();
                $folder->id =  $row['id'];
                $folder->name = $descriptor->getName();
                //$folder->updated_at = 'CURRENT_TIMESTAMP';
                $folder->filename = $filename;
                $folder->content_type = $descriptor->getContentType();
                $folder->size = $descriptor->getFilesize();
                $folder->width = $descriptor->getWidth();
                $folder->height = $descriptor->getHeight();
                
                $folder->update();
                */

                $folder = ModelFolder::updateOrCreate(
                    ['id'=>$row['id']],
                    ['name' => $descriptor->getName(),
                    //'create_at' => 'CURRENT_TIMESTAMP',
                    //'insertupdated_stamp' => 'CURRENT_TIMESTAMP',
                    'entity_type' => $this->entityType,
                    'entity_id' => $this->entityId,
                    'anchor' => $anchor,
                    'filename' => $filename,
                    'content_type' => $descriptor->getContentType(),
                    'size' => $descriptor->getFilesize(),
                    'width' => $descriptor->getWidth(),
                    'height' => $descriptor->getHeight()
                    ]
                );
            } else {
                //dd($descriptor);
                copy($descriptor->getFilename(), asset('storage/media/'. $this->path.$filename));
                
                /*
                App::getInstance()->getConnection()->insert($this->table, array(
                    'name' => $descriptor->getName(),
                    'inserted_stamp' => 'CURRENT_TIMESTAMP',
                    'updated_stamp' => 'CURRENT_TIMESTAMP',
                    'entity_type' => $this->entityType,
                    'entity_id' => $this->entityId,
                    'anchor' => $anchor,
                    'filename' => $filename,
                    'content_type' => $descriptor->getContentType(),
                    'size' => $descriptor->getFilesize(),
                    'width' => $descriptor->getWidth(),
                    'height' => $descriptor->getHeight()
                    
                    
                ));
                */
                
                $folder = ModelFolder::create([
                    'name' => $descriptor->getName(),
                    //'inserted_stamp' => 'CURRENT_TIMESTAMP',
                    //'updated_stamp' => 'CURRENT_TIMESTAMP',
                    'entity_type' => $this->entityType,
                    'entity_id' => $this->entityId,
                    'anchor' => $anchor,
                    'filename' => $filename,
                    'content_type' => $descriptor->getContentType(),
                    'size' => $descriptor->getFilesize(),
                    'width' => $descriptor->getWidth(),
                    'height' => $descriptor->getHeight()
                ]);
                
            }
            

            
            
            
            return $folder;
        }
    }

    /**
     * Add a file, but if file exist , the file is update
     */
    public function updateImport($file_name_origin, $file_nombre, $entity_type, $entity_id , $anchor)
    {
    	if ($file_nombre) {
    		
    		$row = ModelFolder::where('entity_type',$entity_type)
    		->where('entity_id',$entity_id)
    		->where('anchor',$anchor)
    		->get();
    		
    		do {
    			$filename = 'f' . substr(md5(uniqid(rand())), 0, self::FOLDER_MD5_SIZE) . '.pdf';
    		} while (file_exists($this->path . $filename));
    		
    		$nombre_archivo_hash = DB::select("SELECT filename FROM t_folder WHERE name = '".$file_name_origin."' 
				AND entity_type = '".$entity_type."' AND entity_id = '".$entity_id."' AND anchor = '".$anchor."';");
    		
    		if (isset($nombre_archivo_hash[0]->filename)) {
    			$currentFilename = $this->path . $nombre_archivo_hash[0]->filename; // idb/descargas/(num_cat)/file_name_hash.ext
    			
    			if (file_exists($currentFilename)) {
    				unlink($currentFilename);
    			}
    			
    			copy($file_name_origin, $this->path . $nombre_archivo_hash[0]->filename);

    			$folder = new ModelFolder();
    			//$folder->id = $row['id'];
    			$folder->name = $file_name_origin;
    			//$folder->updated_stamp = 'CURRENT_TIMESTAMP';
    			$folder->filename = $nombre_archivo_hash[0]->filename;
    			$folder->content_type = 'application/pdf';
    			$folder->size = filesize($file_name_origin);
    			//$folder->width = $descriptor->getWidth();
    			//$folder->height = $descriptor->getHeight();
    			
    			$folder->save();
    			
    			
    		} else {
    			//dd($descriptor);
    			copy($file_name_origin, $this->path . $filename);

    			
    			$folder = ModelFolder::create([
    					'name' => $file_name_origin,
    					//'inserted_stamp' => 'CURRENT_TIMESTAMP',
    					//'updated_stamp' => 'CURRENT_TIMESTAMP',
    					'entity_type' => $this->entityType,
    					'entity_id' => $this->entityId,
    					'anchor' => $anchor,
    					'filename' => $filename,
    					'content_type' => 'application/pdf',
    					'size' => filesize($file_name_origin)
    					//'width' => $descriptor->getWidth(),
    					//'height' => $descriptor->getHeight()
    			]);
    		}
    	}
    }
    
    public function updateGuideline(Descriptor $descriptor, $anchor)
    {
        if ($descriptor->getFilename()) {
            $row = App::getInstance()->getConnection()->queryRow("SELECT * FROM " . $this->table . " WHERE entity_type = '" . $this->entityType . "'" . " AND anchor = '" . $anchor . "'");
            
            do {
                $filename = 'f' . substr(md5(uniqid(rand())), 0, self::FOLDER_MD5_SIZE) . '.' . $descriptor->getExtension();
            } while (file_exists($this->path . $filename));
            
            if ($row['id']) {
                $currentFilename = $this->path . $row['filename'];
                if (file_exists($currentFilename)) {
                    unlink($currentFilename);
                }
                copy($descriptor->getFilename(), $this->path . $filename);
                App::getInstance()->getConnection()->update($this->table, array(
                    'name' => $descriptor->getName(),
                    'updated_stamp' => 'CURRENT_TIMESTAMP',
                    'filename' => $filename,
                    'content_type' => $descriptor->getContentType(),
                    'size' => $descriptor->getFilesize(),
                    'width' => $descriptor->getWidth(),
                    'height' => $descriptor->getHeight()
                ), array(
                    'id' => $row['id']
                ));
            } else {
                copy($descriptor->getFilename(), $this->path . $filename);
                App::getInstance()->getConnection()->insert($this->table, array(
                    'name' => $descriptor->getName(),
                    'inserted_stamp' => 'CURRENT_TIMESTAMP',
                    'updated_stamp' => 'CURRENT_TIMESTAMP',
                    'entity_type' => $this->entityType,
                    'entity_id' => $this->entityId,
                    'anchor' => $anchor,
                    'filename' => $filename,
                    'content_type' => $descriptor->getContentType(),
                    'size' => $descriptor->getFilesize(),
                    'width' => $descriptor->getWidth(),
                    'height' => $descriptor->getHeight()
                ));
            }
        }
    }

    /**
     * Delete a file from id
     */
    /*
    public function delete($id)
    {
        if (intval($id)) {
            $sql = "SELECT * FROM " . $this->table . " WHERE entity_type = '" . $this->entityType . "' AND id = $id";
            if ($this->entityId == null) {
                $sql .= " AND entity_id IS NULL";
            } else {
                $sql .= " AND entity_id = " . $this->entityId;
            }
            
            $row = App::getInstance()->getConnection()->queryRow($sql);
            if ($row['id']) {
                $currentFilename = $this->path . $row['filename'];
                if (file_exists($currentFilename)) {
                    unlink($currentFilename);
                }
                App::getInstance()->getConnection()->delete($this->table, array(
                    'id' => $row['id']
                ));
            }
        }
    }

    /**
     * Delete a this
     */

    public function delete(){
        $folder = ModelFolder::where('entity_type',$this->entityType)
        ->where('entity_id',$this->entityId)
        ->first();
        
        if ($folder->id) {
            $currentFilename = $this->path . $folder->filename;
            if (file_exists($currentFilename)) {
                unlink($currentFilename);
            }
            
            $folder->delete();
        }
        
    }

    
    /**
     * Delete all files
     */
    public function deleteAll()
    {
        $sql = "SELECT * FROM " . $this->table . " WHERE entity_type = '" . $this->entityType . "'";
        if ($this->entityId == null) {
            $sql .= " AND entity_id IS NULL";
        } else {
            $sql .= " AND entity_id = " . $this->entityId;
        }
        // print_r($sql);
        $stmt = App::getInstance()->getConnection()->query($sql);
        while ($row = $stmt->fetch()) {
            $currentFilename = $this->path . $row['filename'];
            if (file_exists($currentFilename)) {
                unlink($currentFilename);
            }
            App::getInstance()->getConnection()->delete($this->table, array(
                'id' => $row['id']
            ));
        }
    }
}
?>