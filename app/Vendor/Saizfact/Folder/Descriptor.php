<?php

namespace App\Vendor\Saizfact\Folder;

class Descriptor {
	
	private $filename = null;
	private $name = null;
	private $contentType = null;
	private $filesize = 0;
	private $width = null;
	private $height = null;
	
	public function __construct($temporal) {
		if ($temporal) {
			$filename = $temporal['tmp_name'];
			$name = $temporal['name'];
			$contentType = $temporal['type'];
			
			if ($filename) {
				$this -> filename = $filename;
				$this -> name = $name;
				$this -> contentType = $contentType;
				$this -> filesize = round(filesize($filename) / 1024);
				$info = @getimagesize($filename);
				if ($info) {
    				$this -> width = $info[0];
    				$this -> height = $info[1];
				}
			}
		}
	}
	
	public function getFilename() {
		return $this -> filename;
	}
	
	public function getName() {
		return $this -> name;
	}
	
	public function getContentType() {
		return $this -> contentType;
	}
	
	public function getFilesize() {
		return $this -> filesize;
	}
	
	public function getWidth() {
		return $this -> width;
	}
	
	public function getHeight() {
		return $this -> height;
	}
	
	public function getExtension() {
		return pathinfo($this -> name, PATHINFO_EXTENSION);
	}
	
	public function testExtension($params) {
		$a = func_get_args();
		if (is_array($params)) {
			$extension = $params;
		} else {
			$extension = $a;
		}
		array_walk($extension, create_function('&$v,$k', '$v = strtolower($v);'));
		return in_array(strtolower($this -> getExtension()), $extension);
	}
	
	public function getNamebase() {
		return pathinfo($this -> name, PATHINFO_FILENAME);
	}
}
