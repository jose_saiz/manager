<?php
namespace App\Vendor\Saizfact\Illuminate\Database\Migrations;

use Illuminate\Database\Migrations\MigrationCreator;

class SfMigrationCreator extends MigrationCreator
{

    /**
     *
     * {@inheritdoc}
     * @see \Illuminate\Database\Migrations\MigrationCreator::stubPath()
     */
    public function stubPath()
    {
        return __DIR__ . '/stubs';
    }
}
