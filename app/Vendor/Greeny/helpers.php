<?php

use Illuminate\Container\Container;
use Illuminate\Support\Facades\Auth;
use App\Models\DB\File;
use App\Models\DB\Workday;
if (! function_exists('app')) {
    /**
     * Get the available container instance.
     *
     * @param  string  $abstract
     * @param  array   $parameters
     * @return mixed|\Illuminate\Foundation\Application
     */
    function app($abstract = null, array $parameters = [])
    {
        if (is_null($abstract)) {
            return Container::getInstance();
        }
        
        return Container::getInstance()->make($abstract, $parameters);
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////  R O L L ////////////////////////////////////////////////////////
define ('DEFAULT_ROLL_WEIGHT',2159);

function conf_default_roll_weight():float{
    return DEFAULT_ROLL_WEIGHT;
}
define ('DAYS_OUT_RANGE_SLOT', 2);
function conf_days_out_range_slot():int{
    return DAYS_OUT_RANGE_SLOT;
}

function conf_parts_range():array{
    return PARTS_RANGES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////  C O N S T A N T S  ///////////////////////////////////////////////

define ('WORKING_DAYS',array( //'WORKING_DAYS[2]['from']['hour'] => Wednesday - from - hour = 8
    array('from' => array('hour'=>'11','minute'=>'00'),'to'=>array('hour'=>'18','minute'=>'30')), //Sunday = 0
    array('from' => array('hour'=>'09','minute'=>'00'),'to'=>array('hour'=>'20','minute'=>'30')), //'Monday =1
    array('from' => array('hour'=>'09','minute'=>'00'),'to'=>array('hour'=>'20','minute'=>'30')), //Tuesday = 2
    array('from' => array('hour'=>'09','minute'=>'00'),'to'=>array('hour'=>'20','minute'=>'30')), //Wednesday = 3
    array('from' => array('hour'=>'09','minute'=>'00'),'to'=>array('hour'=>'20','minute'=>'30')), //Thursday = 4
    array('from' => array('hour'=>'09','minute'=>'00'),'to'=>array('hour'=>'20','minute'=>'30')), //Friday = 5
    array('from' => array('hour'=>'10','minute'=>'00'),'to'=>array('hour'=>'19','minute'=>'30')) //Saturday = 6
));

define ('WORKDAY',array('from_hour'=>'08','from_minute'=>'00' ,'to_hour'=>'20','to_minute'=>'30'));

//define('ID_NEIGHBORHOOD_FACTORY',1);

define ('DELAYS',array('minutes_start'=>10,'minutes_end'=>5));

define ('PARTS_RANGES',array('biggest' => array(38,80),'big'=>array(19,38),'median'=>array(7,19),'small'=>array(7,3),'smallest'=> array(0,3)));

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////  C O N F I G U R A T I O N ////////////////////////////////////////
if ( ! function_exists('getPartsColors')){
    function getPartsColors(){
        
        $parts_colors =  array();
        $parts = File::select('color','color_background','file_part_name')->get();
        //Cargo la matriz de colores de los slots
        foreach ($parts as $part) {
            if ($part->color){
                $rgb = explode(',',$part->color);
                $rgb_background = explode(',',$part->color_background);
                $parts_colors[$part['file_part_name']][0][0] = $rgb[0];
                $parts_colors[$part['file_part_name']][0][1] = $rgb[1];
                $parts_colors[$part['file_part_name']][0][2] = $rgb[2];
                
                $parts_colors[$part['file_part_name']][1][0] = $rgb_background[0];
                $parts_colors[$part['file_part_name']][1][1] = $rgb_background[1];
                $parts_colors[$part['file_part_name']][1][2] = $rgb_background[2];
            }
        }
        return $parts_colors;
    }
}
if ( ! function_exists('conf_parts_colors')){
    function conf_parts_colors():array{
        return getPartsColors();
    }
}


if ( ! function_exists('conf_working_days'))
{
    function conf_working_days():array{
        $working_days_aux =  Workday::where('neighborhood_factory_id', Auth::guard('web')->user()->factory_id)
        ->where('category_id',3)
        ->get();
        $working_days = array();
        foreach ($working_days_aux as $working_day){
            $working_days[$working_day['day']]['from']['hour'] = explode(':', $working_day['begin_time'])[0];
            $working_days[$working_day['day']]['from']['minute'] = explode(':', $working_day['begin_time'])[1];
            $working_days[$working_day['day']]['to']['hour'] = explode(':', $working_day['end_time'])[0];
            $working_days[$working_day['day']]['to']['minute'] = explode(':', $working_day['end_time'])[1];
        }
        return $working_days;
    }
}
if ( ! function_exists('conf_workday'))
{
    function conf_workday($date_time):array{
        //$workday = WORKDAY;
        $day = $date_time->format('w');
        $workday = session('working_days')[$day];
        
        return $workday;
    }
}

function conf_neighborhood_factory_id():int{
    return Auth::guard('web')->user()->factory_id;
    //return ID_NEIGHBORHOOD_FACTORY;
}

function conf_delays():array{
    return DELAYS;
}



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////  T O O L S ////////////////////////////////////////////////////

if ( ! function_exists('alert')){
    function alert(string $message)
    {
        echo "<script type='text/javascript'> alert('$message');</script>";
    }
}

if ( ! function_exists('multiKeyExists')){
    function multiKeyExists(array $arr, $key) {
        
        // is in base array?
        if (array_key_exists($key, $arr)) {
            return true;
        }
        
        // check arrays contained in this array
        foreach ($arr as $element) {
            if (is_array($element)) {
                if (multiKeyExists($element, $key)) {
                    return true;
                }
            }
            
        }
        
        return false;
    }
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////  S O R T ////////////////////////////////////////////////////

function sort_by_gap_start_asc ($a, $b) {
    $t1 = strtotime($a['gap_start']->format('Y-m-d H:i:s'));
    $t2 = strtotime($b['gap_start']->format('Y-m-d H:i:s'));
    return $t1 - $t2;
}

function sort_by_gap_start_desc ($a, $b) {
    $t1 = strtotime($a['gap_start']->format('Y-m-d H:i:s'));
    $t2 = strtotime($b['gap_start']->format('Y-m-d H:i:s'));
    return $t2 - $t1;
}



if ( ! function_exists('sort_by_hours')){
    function sort_by_hours ($a, $b) {
        return $a['estimated_printing_hours'] - $b['estimated_printing_hours'];
    }
}



if ( ! function_exists('sort_by_hours_asc')){
    function sort_by_hours_asc ($a, $b) {
        return $a['estimated_printing_hours'] - $b['estimated_printing_hours'];
    }
}

if ( ! function_exists('sort_by_hours_desc')){
    function sort_by_hours_desc ($a, $b) {
        return  $b['estimated_printing_hours'] - $a['estimated_printing_hours'];
    }
}

if ( ! function_exists('sort_by_hours_minutes_asc')){
    function sort_by_hours_minutes_asc ($a, $b) {
        $a_total_minutes = $a['estimated_printing_hours']*60+$a['estimated_printing_minutes'];
        $b_total_minutes = $a['estimated_printing_hours']*60+$b['estimated_printing_minutes'];
        return $a_total_minutes - $b_total_minutes;
    }
}

if ( ! function_exists('sort_by_hours_minutes_desc')){
    function sort_by_hours_minutes_desc ($a, $b) {
        $a_total_minutes = $a['estimated_printing_hours']*60+$a['estimated_printing_minutes'];
        $b_total_minutes = $a['estimated_printing_hours']*60+$b['estimated_printing_minutes'];
        return $b_total_minutes - $a_total_minutes;
    }
}

if ( ! function_exists('sort_by_minutes_asc')){
    function sort_by_minutes_asc ($a, $b) {
        return  $a['minutes'] - $b['minutes'];
    }
}
if ( ! function_exists('sort_by_minutes_desc')){
    function sort_by_minutes_desc ($a, $b) {
        return  $b['minutes'] - $a['minutes'];
    }
}
if ( ! function_exists('sort_by_datetime')){
    function sort_by_datetime ($a, $b) {
        $t1 = strtotime($a['datetime']->format('Y-m-d H:i:s'));
        $t2 = strtotime($b['datetime']->format('Y-m-d H:i:s'));
        return $t1 - $t2;
    }
}
if ( ! function_exists('cmp')){
    function cmp($a, $b) {
        if ($a == $b) {
            return 0;
        }
        return ($a < $b) ? -1 : 1;
    }
}

if ( ! function_exists('sort_timeline_by_slot_size_asc')){
    function sort_timeline_by_slot_size_asc ($a,$b)
    {
        if ($a->minutes == $b->minutes) {
            return 0;
        } else if ($a->minutes > $b->minutes)
            return 1;
            else
                return - 1;
                
    }
}
if ( ! function_exists('sort_timeline_by_slot_size_desc')){
    function sort_timeline_by_slot_size_desc ($a,$b)
    {
        
        if ($a->minutes == $b->minutes) {
            return 0;
        } else if ($a->minutes < $b->minutes)
            return 1;
            else
                return - 1;
                
    }
}
if ( ! function_exists('startAscendingComparison')){
    function startAscendingComparison($val1, $val2)
    {
        if ($val1->start == $val2->start) {
            return 0;
        } else if ($val1->start > $val2->start)
            return 1;
            else
                return - 1;
    }
}

if ( ! function_exists('startDescendingComparison')){
    function startDescendingComparison($val1, $val2)
    {
        if ($val1->start == $val2->start) {
            return 0;
        } else if ($val1->start < $val2->start)
            return 1;
            else
                return - 1;
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////   P E R M U T A T I O N S O R T ////////////////////////////////////////////


/*
 $input=array('€','€','€','€');
 
 //$input=array('A','B','C');
 
 $resultado=permute_1($input);
 var_dump(count($resultado));
 for($f=0;$f<count($resultado);$f++){
 echo $resultado[$f];
 echo "<br>";
 }
 */
/*******************************************************/
if ( ! function_exists('permute_1')){
    function permute_1($input)
    {
        $miarray = array();
        $cadena="";
        //copio el array
        $temporal=$input;
        //borro el primer numero del array
        
        
        array_shift($input);
        //ahora la cuenta esta en que solo quedan 3
        for($u=0;$u<count($temporal);$u++)
        
        {
            
            
            for($i=0;$i<count($input);$i++)
            {
                
                array_push($input,$input[0]);
                array_shift($input);
                
                
                for($e=0;$e<count($input);$e++)
                {
                    $cadena.=$input[$e];
                    
                }
                
                
                
                array_push($miarray,$temporal[$u].$cadena);
                array_push($miarray,$temporal[$u].strrev($cadena));
                $cadena="";
                
            }
            array_shift($input);
            array_push($input,$temporal[$u]);
            
        }
        return $miarray;
    }
}


//$arr = array('a', 'b', 'c','d');

//$arr = array('a', 'b', 'c');
//combine_array($arr);
/*
 var_dump(count($arr));
 var_dump($arr);
 */
if ( ! function_exists('combine_array')){
    function combine_array($arr, $index=0) {
        static $num = 0;
        $arr_len = count($arr);
        if($arr_len == $index) {
            ++$num;
            echo $num.' '. join(",", $arr) . '<br/>';
        } else {
            for($i=$index; $i<$arr_len; $i++) {
                list($arr[$index], $arr[$i]) = array($arr[$i], $arr[$index]);
                combine_array($arr, $index+1);
                list($arr[$index], $arr[$i]) = array($arr[$i], $arr[$index]);
            }
        }
    }
}
/*
 $arr = array(array('a'=>'A','1a'=>'1a'), array('b'=>'B','2B'=>'2b'), array('c'=>'C','3C'=>'3c'));
 $permutations=array();
 permute($arr,$permutations);
 var_dump($permutations);
 */
if ( ! function_exists('permute')){
    function permute($arr, &$permutations, $index=0) {
        static $num = 0;
        $arr_len = count($arr);
        if($arr_len == $index) {
            ++$num;
            // echo $num.' '. join(",", $arr) . '<br/>';
            array_push($permutations, $arr);
            
        } else {
            for($i=$index; $i<$arr_len; $i++) {
                list($arr[$index], $arr[$i]) = array($arr[$i], $arr[$index]);
                permute($arr, $permutations,$index+1);
                list($arr[$index], $arr[$i]) = array($arr[$i], $arr[$index]);
            }
        }
    }
}
//$arr = array(1,2,3);
//$B= generate (3,$arr);
//var_dump($B);
//var_dump($arr);
//var_dump(count($arr));
if ( ! function_exists('generate')){
    function generate(int $n, array &$A){
        if ($n == 1){
            //return $A;
            var_dump($A);
            //insertar en el array
        }else{
            for ($i = 0; $i < $n; $i ++){
                generate($n - 1, $A);
                if (($n % 2) == 0) {
                    swap($A[$i], $A[$n-1]);
                }else{
                    swap($A[0], $A[$n-1]);
                }
            }
        }
    }
}

/*
 $a=array(1,2,3);
 $m=2;
 Perm($a,0,$m);
 */
if ( ! function_exists('Perm')){
    function  Perm(array &$list, int $k ,int $m)
    {
        // list array almacena el número de permutaciones, K representa el número de capas ym representa la longitud del array
        if($k==$m)
        {
            // K == m significa que se ha alcanzado el último número y ya no se puede intercambiar. El número permutado final debe enviarse;
            for($i=0 ;$i<=$m ;$i++){
                //cout<<list[i];
                var_dump($list[$i]);
                return;
                //cout<<endl;
            }
        }
        else{
            for($i=$k;$i<=$m;$i++){
                swap($list[$i],$list[$k]);
                Perm($list,$k+1,$m);
                swap($list[$i] , $list[$k]);
            }
        }
        
        
    }
}
if ( ! function_exists('swap')){
    function swap(&$a,&$b){
        $temp = $a;
        $a = $b;
        $b=$temp;
    }
}



if (! function_exists('getFiles')) {
    function getFiles(){
        $curl = curl_init();
        
        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://192.168.1.177/API/info",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Content-Type: application/xml"
            ),
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_SSL_VERIFYPEER => false
            
        ));
        
        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        return json_decode($response,true);
    }
    
}
