<?php
namespace App\Vendor\Greeny\Main;

///////////////////////////////////////////////////////////// P R I N T E R S L O T  - C L A S S ////////////////////////////////////////////////////////////////
class PrinterSlot
{
    public $part;
    public $order;
    public $file;
    public $file_part_name;
    public $file_name;
    public $version_part_name;
    public $product;
    public $product_name;
    public $final_product;
    public $start;
    public $end;
    public $weight;
    public $state;
    public $initiated;
    public $minutes;

    function __construct($part, $order, $file,  $file_part_name,$file_name, $version_part_name, $product, $product_name, $final_product, $start, $end, $weight, $state,$initiated)
    {

        
        $this->part = $part;
        $this->order = $order;
        $this->file = $file;
        $this->file_part_name = $file_part_name;
        $this->file_name = $file_name;
        $this->version_part_name = $version_part_name;
        $this->product = $product;
        $this->product_name = $product_name;
        $this->final_product = $final_product;
        $this->start = $start;
        $this->end = $end;
        $this->weight = $weight;
        $this->state = $state;
        $this->initiated = $initiated;
        $this->minutes = $this->minutes();
        
    }
    
    
    
    static function  new(PrinterSlot $printerSlot)
    {
        $new = new PrinterSlot( $printerSlot->part, 
                                $printerSlot->order, 
                                $printerSlot->file,
                                $printerSlot->file_part_name,
                                $printerSlot->file_name,
                                $printerSlot->version_part_name,
                                $printerSlot->product,
                                $printerSlot->product_name,
                                $printerSlot->final_product, 
                                $printerSlot->start, 
                                $printerSlot->end, 
                                $printerSlot->weight, 
                                $printerSlot->state, 
                                $printerSlot->initiated,
                                $printerSlot->minutes);
        
        return $new;
        
    }

    function update(PrinterSlot $printerSlot)
    {
        $this->part = $printerSlot->part;
        $this->order = $printerSlot->order;
        $this->file = $printerSlot->file;
        $this->file_part_name = $printerSlot->file_part_name;
        $this->file_name = $printerSlot->file_name;
        $this->version_part_name = $printerSlot->version_part_name;
        $this->product =  $printerSlot->product;
        $this->product_name =  $printerSlot->product_name;
        $this->final_product =  $printerSlot->final_product;
        $this->start = $printerSlot->start;
        $this->end = $printerSlot->end;
        $this->weight = $printerSlot->weight;
        $this->state = $printerSlot->state;
        $this->initiated = $printerSlot->initiated;
        $this->minutes = $printerSlot->minutes;
        
    }
    
    function toPendingPart():array
    {
        
        $diff =  DateTimeG::newDateTime($this->start)->diffRangeHM(DateTimeG::newDateTime($this->end));
        $pending_part = array();
        $pending_part ['estimated_printing_hours'] = $diff['hours'];
        $pending_part ['estimated_printing_minutes'] = $diff['minutes'];
        
        $pending_part ['id'] = $this->part;
        $pending_part ['order_id'] = $this->order;
        $pending_part ['file_id'] = $this->file;
        $pending_part ['file_part_name'] = $this->file_part_name;
        $pending_part ['file_name'] = $this->file_name;
        $pending_part ['version_part_name'] = $this->version_part_name;
        $pending_part ['product_id'] = $this->product;
        $pending_part ['product_name'] = $this->product_name;
        $pending_part ['final_product_id'] = $this->final_product;
        $pending_part ['weight'] = $this->weight;
        $pending_part ['state_id'] = $this->state;
        $pending_part ['initiated'] = $this->initiated;
        $pending_part ['minutes'] = $this->minutes;
        
        return $pending_part;
    }
    static function fromPendingPart(array $pending_part,$start)
    {
        $end = new DateTimeG($start);
        $end ->modify('+'.$pending_part ['estimated_printing_hours'].' hours');
        $end ->modify('+'.$pending_part ['estimated_printing_minutes'].' minutes');
        $printerSlot = new PrinterSlot(
                                $pending_part ['id'], 
                                $pending_part ['order_id'], 
                                $pending_part ['file_id'] , 
                                $pending_part ['file_part_name'] , 
                                $pending_part ['file_name'] ,
                                $pending_part ['version_part_name'], 
                                $pending_part ['product_id'], 
                                $pending_part ['product_name'], 
                                $pending_part ['final_product_id'], 
                                $start, 
                                $end->format('Y-m-d H:i:s'), 
                                $pending_part ['weight'], 
                                $pending_part ['state_id'], 
                                $pending_part ['initiated'],
                                $pending_part ['minutes']);
        return $printerSlot;
    }
    
    private function minutes(){
        $minutes = 0;
        if ($this->start != null){
            $slot_start = DateTimeG::newDateTime($this->start);
            $slot_end = DateTimeG::newDateTime($this->end);
            $minutes = $slot_start->diffMinutes($slot_end);
        }
        return $minutes;
    }
    
}
