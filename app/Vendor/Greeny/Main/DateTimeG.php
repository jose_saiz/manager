<?php 
namespace App\Vendor\Greeny\Main;
use DateTime;
use DateTimeZone;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////  S O R T //////////////////////////////////////////////////
function sort_by_gap_start_asc ($a, $b) {
    $t1 = strtotime($a['gap_start']->format('Y-m-d H:i:s'));
    $t2 = strtotime($b['gap_start']->format('Y-m-d H:i:s'));
    return $t1 - $t2;
}

function sort_by_gap_start_desc ($a, $b) {
    $t1 = strtotime($a['gap_start']->format('Y-m-d H:i:s'));
    $t2 = strtotime($b['gap_start']->format('Y-m-d H:i:s'));
    return $t2 - $t1;
}

function sort_by_hours_asc ($a, $b) {
    return $a['estimated_printing_hours'] - $b['estimated_printing_hours'];
}

function sort_by_hours_desc ($a, $b) {
    return  $b['estimated_printing_hours'] - $a['estimated_printing_hours'];
}

function sort_by_hours_minutes_asc ($a, $b) {
    $a_total_minutes = $a['estimated_printing_hours']*60+$a['estimated_printing_minutes'];
    $b_total_minutes = $a['estimated_printing_hours']*60+$b['estimated_printing_minutes'];
    return $a_total_minutes - $b_total_minutes;
}

function sort_by_hours_minutes_desc ($a, $b) {
    $a_total_minutes = $a['estimated_printing_hours']*60+$a['estimated_printing_minutes'];
    $b_total_minutes = $a['estimated_printing_hours']*60+$b['estimated_printing_minutes'];
    return $b_total_minutes - $a_total_minutes;
}

function sort_by_minutes_asc ($a, $b) {
    return  $a['minutes'] - $b['minutes'];
}

function sort_by_minutes_desc ($a, $b) {
    return  $b['minutes'] - $a['minutes'];
}



function sort_by_datetime ($a, $b) {
    $t1 = strtotime($a['datetime']->format('Y-m-d H:i:s'));
    $t2 = strtotime($b['datetime']->format('Y-m-d H:i:s'));
    return $t1 - $t2;
    //return $a['datetime'] - $b['datetime'];
    /*
     if ($a['datetime'] == $b['datetime']) {
     return 0;
     }
     
     return $b['datetime'] > $b['datetime'] ? -1 : 1;
     */
    
}

function cmp($a, $b) {
    if ($a == $b) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}


function sort_timeline_by_slot_size_asc ($a,$b)
{
    if ($a->minutes == $b->minutes) {
        return 0;
    } else if ($a->minutes > $b->minutes)
        return 1;
        else
            return - 1;
            
}
function sort_timeline_by_slot_size_desc ($a,$b)
{
    
    if ($a->minutes == $b->minutes) {
        return 0;
    } else if ($a->minutes < $b->minutes)
        return 1;
        else
            return - 1;
            
}
/*
 function sort_timeline_by_slot_size_asc ($a,$b)
 {
 $minutes_a = DateTimeG::newDateTime($a->start)->diffMinutes( DateTimeG::newDateTime($a->end));
 $minutes_b = DateTimeG::newDateTime($b->start)->diffMinutes( DateTimeG::newDateTime($b->end));
 
 if ($minutes_a == $minutes_b) {
 return 0;
 } else if ($minutes_a > $minutes_b)
 return 1;
 else
 return - 1;
 
 }
 function sort_timeline_by_slot_size_desc ($a,$b)
 {
 $minutes_a = DateTimeG::newDateTime($a->start)->diffMinutes( DateTimeG::newDateTime($a->end));
 $minutes_b = DateTimeG::newDateTime($b->start)->diffMinutes( DateTimeG::newDateTime($b->end));
 
 if ($minutes_a == $minutes_b) {
 return 0;
 } else if ($minutes_a < $minutes_b)
 return 1;
 else
 return - 1;
 
 }
 */
 function startAscendingComparison($val1, $val2)
 {
     if ($val1->start == $val2->start) {
         return 0;
     } else if ($val1->start > $val2->start)
         return 1;
         else
             return - 1;
 }
 
 
 function startDescendingComparison($val1, $val2)
 {
     if ($val1->start == $val2->start) {
         return 0;
     } else if ($val1->start < $val2->start)
         return 1;
         else
             return - 1;
 }
 
 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 //////////////////////////////////////////////////////  R A N G E S //////////////////////////////////////////////
 function isSizeHours(String $size, String $hours): bool
 {
     if (($hours >= conf_parts_range()[$size][0]) && ($hours <= conf_parts_range()[$size][1])) {
         return true;
     } else {
         return false;
     }
 }
 function isSizeDateTime(String $size, DateTime $start, DateTime $end): bool
 {
     $diff = $start->diff($end);
     
     $hours = $diff->h;
     $hours = $hours + ($diff->days * 24);
     
     $hours + 1;
     
     return isSizeHours($size, $hours);
}
 function is_overlap(DateTime $start_time1, DateTime $end_time1, DateTime $start_time2, DateTime $end_time2)
 {
     $result = (($start_time1) <= ($end_time2) && ($start_time2) <= ($end_time1) ? true : false);
     return $result;
 }
 
 
 function is_overlap_whithout_wrap(DateTime $start_time1, DateTime $end_time1, DateTime $start_time2, DateTime $end_time2)
 {
     //Si la datetime  2 envuelve al 1
     if (($start_time2 < $start_time1) && ($end_time2 > $end_time1)) {
         return false;
     }
     /* //Si la datetime  1 envuelve al 2
      if (($start_time2 > $start_time1) && ($end_time2 < $end_time1)) {
      return false;
      }
      */
     return is_overlap($start_time1, $end_time1, $start_time2, $end_time2);
 }
 
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// D A T A   T I M E //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DateTimeG extends DateTime
{
    
    
    public function __construct($time='now', $timezone='Europe/Madrid')
    {
        parent::__construct($time, new DateTimeZone($timezone));
        
    }
    
    
    /**
     * Return Date in ISO8601 format
     *
     * @return String
     */
    public function __toString(): String {
        return $this->format('Y-m-d H:i:s');
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////  B A S I C S /////////////////////////////////////////////////
    
    function createRealStartDateTime(){
        
        $real_start_date_time['start_date_time_print'] = $this;
        
        
        //$this->start_date_time = new DateTimeG($this->start_date_time_print->format('Y-m-d H:i:s'));
        $real_start_date_time['start_date_time'] = DateTimeG::newDateTime( $real_start_date_time['start_date_time_print']->format('Y-m-d H:i:s'));
        if ($real_start_date_time['start_date_time']->isOverlapAtNight()){
            if ($real_start_date_time['start_date_time']->format('H') > 12) {
                $real_start_date_time['start_date_time']->modify('+1 days');
            }
            $day_start = $real_start_date_time['start_date_time']->format('w');
            $real_start_date_time['start_date_time']->setTime(session('working_days')[$day_start]['from']['hour'], session('working_days')[$day_start]['from']['minute']);//+conf_delays()['minutes_start']);
        }
        
        return $real_start_date_time;
    }
    
    
    static function setDateTime(DateTime &$date_time, string $str_date_time){
        $str = strtotime($str_date_time);
        $year = date('Y',$str);
        $month  = date('m',$str);
        $day = date('d',$str);
        $date_time->setDate($year, $month, $day);
        $hour = date('H',$str);
        $minute = date('i',$str);
        $seconds = date('s',$str);
        $date_time->setTime($hour, $minute,$seconds);
    }
    
    public function createDateTime():DateTimeG{
        //return new DateTimeG($this);
        return new DateTimeG($this);
    }
    
    public static function newDateTime(string $date_time):DateTimeG{
        //$rdo = new DateTimeG($date_time);
        $rdo = new DateTimeG($date_time);
        return $rdo;
    }
    
    
    public static function newFromformatUTC (string $datetime){
        $new = DateTimeG::newDateTime($datetime);
        if (strpos($datetime, 'T')) {
            $start = explode("T", $datetime);
            $date = explode('-', $start[0]);
            $time = explode(':', $start[1]);
            $new->setDate($date[0], $date[1], $date[2]);
            $new->setTime($time[0], $time[1]);
        }
        return $new;
    }
    
    
    public static function CleanDatetimeForBD(string $date_time):string{
        $dateTime = new DateTimeG($date_time);
        $date = $dateTime->format('Y-m-d');
        $time = $dateTime->format('H:i:s');
        return $date .' ' . $time;
    }
    
    public function minuteOfTheDay():int{
        return  $this->format('H')*60+$this->format('i');
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////  O V E R L A P S /////////////////////////////////////////////
    
    
    public static function isOverlap(DateTimeG $start_time1, DateTimeG $end_time1, DateTimeG $start_time2, DateTimeG $end_time2)
    {
        $result = (($start_time1) <= ($end_time2) && ($start_time2) <= ($end_time1) ? true : false);
        return $result;
    }
    
    public static function isOverlapOrWrap(DateTimeG $start_time1, DateTimeG $end_time1, DateTimeG $start_time2, DateTimeG $end_time2)
    {
        //Si la datetime  2 envuelve al 1
        if (($start_time2 < $start_time1) && ($end_time2 > $end_time1)) {
            return true;
        }
        //Si la datetime  1 envuelve al 2
        if (($start_time2 > $start_time1) && ($end_time2 < $end_time1)) {
            return true;
        }
        return DateTimeG::isOverlap($start_time1, $end_time1, $start_time2, $end_time2);
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////  N I G H T S /////////////////////////////////////////////////
    
    function night(){
        $night_from = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_from = new DateTime($this->date);
        $day_from = $night_from->format('w');
        $night_from->setTime(session('working_days')[$day_from]['to']['hour'], session('working_days')[$day_from]['to']['minute']);
        if ($this->format('H') <= 12) {
            $night_from->modify('-1 days');
        }
        
        $night_to = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_to = new DateTime($this->date);
        $day_to = $night_to->format('w');
        $night_to->setTime(session('working_days')[$day_to]['from']['hour'], session('working_days')[$day_to]['from']['minute']);
        if ($this->format('H') > 12) {
            $night_to->modify('+1 days');
        }
        return array("from"=>$night_from,"to"=>$night_to);
        
    }
    
    
    function isOverlapAtNight()
    {
        
        $night_from = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_from = new DateTime($this->date);
        $day_from = $night_from->format('w');
        $night_from->setTime(session('working_days')[$day_from]['to']['hour'], session('working_days')[$day_from]['to']['minute']);
        if ($this->format('H') <= 12) {
            $night_from->modify('-1 days');
        }
        
        $night_to = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_to = new DateTime($this->date);
        $day_to = $night_to->format('w');
        $night_to->setTime(session('working_days')[$day_to]['from']['hour'], session('working_days')[$day_to]['from']['minute']);
        if ($this->format('H') > 12) {
            $night_to->modify('+1 days');
        }
        
        
        $result = (($this) <= ($night_to) && ($night_from) <= ($this) ? true : false);
        return $result;
    }
    function isOverlapAtNightFlex($minutesFlex=0)
    {
        
        $night_from = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_from = new DateTime($this->date);
        $day_from = $night_from->format('w');
        $night_from->setTime(session('working_days')[$day_from]['to']['hour'], session('working_days')[$day_from]['to']['minute']);
        if ($this->format('H') <= 12) {
            $night_from->modify('-1 days');
        }
        
        $night_to = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_to = new DateTime($this->date);
        $day_to = $night_to->format('w');
        $night_to->setTime(session('working_days')[$day_to]['from']['hour'], session('working_days')[$day_to]['from']['minute']);
        if ($this->format('H') > 12) {
            $night_to->modify('+1 days');
        }
        
        //$night_to->modify('-1 hours');
        $night_to->modify('-' .$minutesFlex.' minutes');
        
        $result = (($this) <= ($night_to) && ($night_from) <= ($this) ? true : false);
        return $result;
    }
    

    function OverlapAtNight():array
    {
        get_object_vars($this);
        
        //$night_from = new DateTimeG($this->format('Y-m-d H:i:s'));
        $night_from = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_from = new DateTime($this->date);
        $day_from = $night_from->format('w');
        $night_from->setTime(session('working_days')[$day_from]['to']['hour'], session('working_days')[$day_from]['to']['minute']);
        if ($this->format('H') <= 12) {
            $night_from->modify('-1 days');
        }
        
        //$night_to = new DateTimeG($this->format('Y-m-d H:i:s'));
        $night_to = new DateTimeG($this->format('Y-m-d H:i:s'));
        //$night_to = new DateTime($this->date);
        $day_to = $night_to->format('w');
        $night_to->setTime(session('working_days')[$day_to]['from']['hour'], session('working_days')[$day_to]['from']['minute']);
        if ($this->format('H') > 12) {
            $night_to->modify('+1 days');
        }
        
        $result = (($this) <= ($night_to) && ($night_from) <= ($this) ? true : false);
        
        if ($result){
            return array('night_from'=>$night_from,'night_to'=>$night_to);
        }
        return  false;
    }
    
    static function removeOverlapAtNight(array &$estimated_printing, array $pending_part, string $start_date_time): bool
    {
        //Comprobar que la nueva estimación no se solapa con la noche de incio o la de fin,
        //Si se solapa dependiendo de si es por delante o por detrás recalculamos la fecha estimada.
        $night = DateTimeG::estimatedPrintingNightsFlex($estimated_printing);
        if ((is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing['start'], $estimated_printing['end'])) && (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end']))) {
            //echo "point";
        };
        
        if ((is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing['start'], $estimated_printing['end']))
            ||
            (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end']))) {
                $estimated_printing_aux = array();
                get_object_vars($estimated_printing['end']);
                $estimated_printing_aux['start'] = new DateTimeG($estimated_printing['start']);
                //$estimated_printing_aux['start'] = new DateTime($estimated_printing['start']->date);
                
                if (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end'])){
                    $estimated_printing_aux['start']->modify('+1 days');
                }
                
                $day_start = $estimated_printing_aux['start']->format('w');
                $estimated_printing_aux['start']->setTime(conf_working_days()[$day_start]['from']['hour'], conf_working_days()[$day_start]['from']['minute'] + conf_delays()['minutes_start']);
                //$estimated_printing_aux['start'] ->setTime(conf_workday()['from_hour'],conf_workday()['from_minute']+1);
                
                $pending_part_aux = $pending_part;
                $pending_part_aux['estimated_printing_minutes'] ++;
                $estimated_printing_aux['end'] = $estimated_printing_aux['start']->estimatedPrintingEnd($pending_part_aux);
                
                $night = DateTimeG::estimatedPrintingNights($estimated_printing_aux);
                
                if ((is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing_aux['start'], $estimated_printing_aux['end']))
                    ||
                    (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing_aux['start'], $estimated_printing_aux['end']))) {
                        
                        $day_end = $estimated_printing['end']->format('w');
                        if ($estimated_printing['end']->format('H') > 12) {
                            $estimated_printing['end']->modify('+1 days');
                        }
                        $estimated_printing['end']->setTime(conf_working_days()[$day_end]['from']['hour'], conf_working_days()[$day_end]['from']['minute'] + conf_delays()['minutes_start']);
                        $estimated_printing['start'] = $estimated_printing['end']->estimatedPrintingStart($pending_part);
                        
                        if ((is_overlap_whithout_wrap($night['before']['start'], $night['before']['end'], $estimated_printing['start'], $estimated_printing['end']))
                            ||
                            (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing['start'], $estimated_printing['end']))) {
                                
                                
                                if ($estimated_printing['start'] ->format('H') < 12){
                                    $estimated_printing['start'] ->setTime(conf_working_days()[$day_end]['from']['hour'], conf_working_days()[$day_end]['from']['minute'] + conf_delays()['minutes_start']);
                                    $estimated_printing['end'] = $estimated_printing['start'] ->estimatedPrintingEnd($pending_part);
                                    
                                }
                            }
                            
                    } else {
                        $estimated_printing = $estimated_printing_aux;
                    }
            } else {
                //Si no se ha solapado
                return false;
            }
            return true;
    }
    
    static function removeOverlapPastFromStartAll(array $pending_part, array &$estimated_printing, string $start_date_time)
    {
        $now = new DateTimeG($start_date_time);
        //$now = new DateTime($start_date_time);
        if ($estimated_printing['start'] < $now) {
            //$day_now = $now->format('w');
            //$now->setTime(conf_working_days()[$day_now]['from']['hour'], conf_working_days()[$day_now]['from']['minute']);
            
            //$now->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
            $estimated_printing['start'] = $now;
            $estimated_printing['end'] = $estimated_printing['start']->estimatedPrintingEnd($pending_part);
            
            if (isOverlapAtNight($estimated_printing['end'])){
                //$start_night = estimatedPrintingNightFlex($estimated_printing['start']);
                
                
                
                $day_start = $estimated_printing['start']->format('w');
                
                $estimated_printing['start']->setTime(conf_working_days()[$day_start]['to']['hour'], conf_working_days()[$day_start]['to']['minute']- conf_delays()['minutes_start']);
                $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
                /*
                 $behind_night   = estimatedPrintingNightFlex($estimated_printing['end']);
                 
                 $TotalMinutesNight = rangeTotalM($behind_night['start'], $behind_night['end']);
                 $TotalMinutesPart = rangeTotalM($estimated_printing['start'], $estimated_printing['end']);
                 
                 $right_offset = intdiv($TotalMinutesPart - $TotalMinutesNight,2);
                 
                 $estimated_printing['end'] = $behind_night['end'];
                 $estimated_printing['end']->modify('+'. $right_offset.' minutes');
                 
                 $estimated_printing['start'] = estimatedPrintingStart($estimated_printing['end'], $pending_part);
                 */
            }
            
        }else{
            
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// E S T I M A T E S ///////////////////////////////////////////////////
    
    static function estimatedPrintingNightFlex(DateTime $date_time): array
    {
        $night = array();
        get_object_vars($date_time);
        //$night['start'] = new DateTime($date_time->date);
        $night['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$date_time->format('Y-m-d H:i:s'));
        if ($date_time->format('H') <= 12) {
            $night['start']->modify('-1 days');
        }
        $day_night_start = $night['start']->format('w');
        $night['start']->setTime(conf_working_days()[$day_night_start]['to']['hour'], conf_working_days()[$day_night_start]['to']['minute'] + conf_delays()['minutes_start']);
        
        get_object_vars($night['start']);
        //$night['end'] = new DateTime($night['start']->date);
        $night['end'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$night['start']->format('Y-m-d H:i:s'));
        $night['end']->modify('+1 days');
        $day_night_end = $night['end']->format('w');
        $night['end']->setTime(conf_working_days()[$day_night_end]['from']['hour'], conf_working_days()[$day_night_end]['from']['minute'] + conf_delays()['minutes_start']);
        
        return $night;
    }
    
    
    
    static function estimatedPrintedFromStart(array $pending_part, DateTime $estimated_printing_start, string $start_date_time): array
    {
        
        $estimated_printing_end = $estimated_printing_start->estimatedPrintingEnd($pending_part);
        
        //Si la fecha asignada es del pasado.
        
        //removeOverlapPast($pending_part, $estimated_printing_start, $estimated_printing_end, $start_date_time);
        
        //Miro si el inicio de la fecha estimada es fin de semana
        
        
        $estimated_printing = array(
            'start' => $estimated_printing_start,
            'end' => $estimated_printing_end
        );
        
        get_object_vars($estimated_printing['end']);
        get_object_vars($estimated_printing['start']);
        //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
        if (! DateTimeG::removeOverlapAtNight($estimated_printing, $pending_part, $start_date_time)) {}
        //removeOverlapPastFromStart($pending_part, $estimated_printing, $start_date_time);
        
        return $estimated_printing;
    }
    
    static function estimatedPrintingNightsFlex(array $estimated_printing): array
    {
        
        //Night Before
        get_object_vars($estimated_printing['start']);
        $night_before_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['start']->format('Y-m-d H:i:s'));
        //$night_before_start = new DateTime($estimated_printing['start']->date);
        
        //Si la hora estimada es menor de 12
        /*
         if ($estimated_printing['start']->format('H') <= 12) {
         $night_before_start->modify('-1 days');
         }
         */
        
        
        $day_start = $night_before_start->format('w');
        $night_before_start->setTime(conf_working_days()[$day_start]['to']['hour'], conf_working_days()[$day_start]['to']['minute'] /*+ conf_delays()['minutes_start']*/);
        
        get_object_vars($estimated_printing['end']);
        $night_before_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['start']->format('Y-m-d H:i:s'));
        //$night_before_end = new DateTime($estimated_printing['start']->date);
        if ($estimated_printing['start']->format('H') > 12) {
            $night_before_end->modify('+1 days');
        }
        $day_end = $night_before_start->format('w');
        $night_before_end->setTime(conf_working_days()[$day_end]['from']['hour'], conf_working_days()[$day_end]['from']['minute'] /*+ conf_delays()['minutes_start']*/);
        get_object_vars($night_before_end);
        
        //Night Behind
        $night_behind_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['end']->format('Y-m-d H:i:s'));
        //$night_behind_start = new DateTime($estimated_printing['end']->date);
        
        if ($estimated_printing['end']->format('H') <= 12) {
            $night_behind_start->modify('-1 days');
        } else {}
        $day_behind_start = $night_behind_start->format('w');
        $night_behind_start->setTime(conf_working_days()[$day_behind_start]['to']['hour'], conf_working_days()[$day_behind_start]['to']['minute'] /*+ conf_delays()['minutes_start']*/);
        
        get_object_vars($night_behind_start);
        
        $night_behind_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$night_behind_start->format('Y-m-d H:i:s'));
        //$night_behind_end = new DateTime($night_behind_start->date);
        if ($estimated_printing['end']->format('H') > 12) {} else {}
        
        $night_behind_end->modify('+1 days');
        $day_behind_end = $night_behind_end->format('w');
        $night_behind_end->setTime(conf_working_days()[$day_behind_end]['from']['hour'], conf_working_days()[$day_behind_end]['from']['minute'] /*+ conf_delays()['minutes_start']*/);
        
        $night = array(
            'before' => array(
                'start' => $night_before_start,
                'end' => $night_before_end
            ),
            'behind' => array(
                'start' => $night_behind_start,
                'end' => $night_behind_end
            )
        );
        return $night;
    }
    
    
    static function estimatedPrintingNights(array $estimated_printing): array
    {
        
        //Night Before
        get_object_vars($estimated_printing['start']);
        $night_before_start = new DateTimeG($estimated_printing['start']);
        //$night_before_start = new DateTime($estimated_printing['start']->date);
        //$night_before_start->setTime(conf_workday()['to_hour'], conf_workday()['to_minute'] + conf_delays()['minutes_start']);
        $day_before_start= $night_before_start->format('w');
        $night_before_start->setTime(conf_working_days()[$day_before_start]['to']['hour'], 
                                     conf_working_days()[$day_before_start]['to']['minute'] + conf_delays()['minutes_start']);
        //Si la hora estimada es menor de 12
        
        if ($estimated_printing['start']->format('H') <= 12) {
            $night_before_start->modify('-1 days');
        }
        
        get_object_vars($estimated_printing['end']);
        $night_before_end =  new DateTimeG($estimated_printing['start']);
        //$night_before_end = new DateTime($estimated_printing['start']->date);
        if ($estimated_printing['start']->format('H') > 12) {
            $night_before_end->modify('+1 days');
        }
        //$night_before_end->setTime(conf_workday()['from_hour'], conf_workday()['from_minute']);
        $day_night_before_end = $night_before_end->format('w');
        $night_before_end->setTime(conf_working_days()[$day_night_before_end]['from']['hour'], 
                                   conf_working_days()[$day_night_before_end]['from']['minute']);
        //Night Behind
        $night_behind_start = new DateTimeG($estimated_printing['end']);
        //night_behind_start = new DateTime($estimated_printing['end']->date);
        
        if ($estimated_printing['end']->format('H') <= 12) {
            $night_behind_start->modify('-1 days');
        } else {}
        //$night_behind_start->setTime(conf_workday()['to_hour'], conf_workday()['to_minute'] + conf_delays()['minutes_start']);
        $day_night_behind_start = $night_behind_start->format('w');
        $night_behind_start->setTime(conf_working_days()[$day_night_behind_start]['to']['hour'],
                                     conf_working_days()[$day_night_behind_start]['to']['minute'] + conf_delays()['minutes_start']);
        //Si la hora estimada es menor de 12
        get_object_vars($night_behind_start);
        
        $night_behind_end = new DateTimeG($night_behind_start);
        //$night_behind_end = new DateTime($night_behind_start->date);
        if ($estimated_printing['end']->format('H') > 12) {} else {}
        
        $night_behind_end->modify('+1 days');
        
        //$night_behind_end->setTime(conf_workday()['from_hour'], conf_workday()['from_minute'] /*+conf_delays()['minutes_start']*/);
        $day_night_behind_end = $night_behind_end->format('w');
        $night_behind_end->setTime(conf_working_days()[$day_night_behind_end]['from']['hour'],
                                   conf_working_days()[$day_night_behind_end]['from']['minute']);
        $night = array(
            'before' => array(
                'start' => $night_before_start,
                'end' => $night_before_end
            ),
            'behind' => array(
                'start' => $night_behind_start,
                'end' => $night_behind_end
            )
        );
        return $night;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////  D I F F ///////////////////////////////////////////////////
    function rangeTotalM(DateTimeG $end)
    {
        $interval = $this->diff($end);
        $days = $interval->format('%d');
        $hours = $interval->format('%h');
        $minutes = $interval->format('%i');
        return $days * 24 * 60 + $hours * 60 + $minutes;
    }
    
    
    function diffRangeHM(DateTimeG $end):array
    {
        $hours = $this->diff($end)->format('%h');
        $minutes = $this->diff($end)->format('%i');
        
        if ($this->diff($end)->format('%d') >=1){
            $hours += $this->diff($end)->format('%d')*24;
        }
        
        return array(
            'hours' => $hours,
            'minutes' => $minutes
        );
    }
    
    function diffHours( DateTime $date2)
    {
        $diff = $date2->diff($this);
        
        $hours = $diff->h;
        $hours = $hours + ($diff->days * 24);
        return $hours;
    }
    
    function diffMinutes(DateTimeG $end):int
    {
        $hours = $this->diff($end)->format('%h');
        $minutes = $this->diff($end)->format('%i');
        
        if ($this->diff($end)->format('%d') >=1){
            $hours += $this->diff($end)->format('%d')*24;
        }
        
        
        return $hours*60 + $minutes;
    }
    
    function diffDays( DateTime $date2)
    {
        $diff = $date2->diff($this);
        return $diff->days;;
    }
    
    function diffDaysOnly(DateTimeG $end):int
    {
        $days = $this->diff($end)->format('%a');
        
        return $days;
    }
    function diffHoursOnly(DateTimeG $end):int
    {
        $days = $this->diff($end)->format('%h');
        
        return $days;
    }
    
    function diffMinutesOnly(DateTimeG $end):int
    {
        $days = $this->diff($end)->format('%i');
        
        return $days;
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////// E S T I M A T E D ///////////////////////////////////////////////
    
    function estimatedPrintingEnd(array $pending_part): DateTimeG
    {
        $estimated_printing_end = new DateTimeG($this->format('Y-m-d H:i:s'));
        $estimated_printing_end->modify("+" . $pending_part['estimated_printing_hours'] . " hours");
        $estimated_printing_end->modify("+" . $pending_part['estimated_printing_minutes'] . " minutes");
        return $estimated_printing_end;
    }
    function estimatedPrintingEndSlot(PrinterSlot $slot): DateTimeG
    {
        //return estimatedPrintingEnd($this, $slot->toPendingPart());
        return $this->estimatedPrintingEnd($slot->toPendingPart());
    }
    
    function estimatedPrintingStart(array $pending_part): DateTimeG
    {
        $estimated_printing_start = new DateTimeG($this->format('Y-m-d H:i:s'));
        $estimated_printing_start->modify("-" . $pending_part['estimated_printing_hours'] . " hours");
        $estimated_printing_start->modify("-" . $pending_part['estimated_printing_minutes'] . " minutes");
        return $estimated_printing_start;
    }
    
    function estimatedPrintingStartSlot(PrinterSlot $slot): DateTimeG
    {
       // return estimatedPrintingStart($this, $slot->toPendingPart());
        return $this->estimatedPrintingStart($slot->toPendingPart());
    }
    
    
    function workDay(): array
    {
        $workday_start = new DateTimeG($this->format('Y-m-d'));
        $workday_start->setTime(conf_workday($workday_start)['from']['hour'], conf_workday($workday_start)['from']['minute']);
        $workday_end = new DateTimeG($this->format('Y-m-d'));
        $workday_end->setTime(conf_workday($workday_end)['to']['hour'], conf_workday($workday_end)['to']['minute']);
        
        return array(
            'start' => $workday_start,
            'end' => $workday_end
        );
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    
    

    
    
    
    /**
     * Return Age in Years
     *
     * @param Datetime|String $now
     * @return Integer
     */
    public function getAge( $now = 'NOW'):Int {
        return $this->diff($now)->format('%y');
    }
    
    
    
    
    
    
}

?>