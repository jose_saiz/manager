<?php

namespace App\Vendor\Greeny\Main;

use ArrayObject;
use App\Models\DB\Part;
use DateTimeG;

///////////////////////////////////////////////////////////// P R I N T E R S T I M E L I N E - C L A S S ////////////////////////////////////////////////////////////////
class PrinterState extends ArrayObject
{
    public $printer_id, $code_printer;
    public $previous_slot,$current_slot,$next_slot;
    public $lamp;
    public $current_time_left,$current_time_printing,$current_percent_printing;
    public $current_days,$current_time,$next_days,$next_time;
    public $roll_weight, $roll_replacement_datetime,$rolls_end;
    public $color,$blink_class;
    public $last_slot_out_range;
    public $current_button,$next_button;
    public $current_action,$next_action;
    public $available, $order_id;
    
    
    function __construct(int $printer_id=NULL, string $code_printer=NULL,$last_slot_out_range =null, float $roll_weight=NULL, DateTimeG $roll_replacement_datetime=NULL,array $rolls_end = NULL )
    {
        parent::__construct(array(), ArrayObject::ARRAY_AS_PROPS);
        $this->printer_id = $printer_id;
        $this->code_printer = $code_printer;
        $this->roll_weight = $roll_weight;
        $this->roll_replacement_datetime = $roll_replacement_datetime;
        $this->rolls_end = array();
        //array('from' => array('hour'=>'8','minute'=>'0')
        
    }
    
}
