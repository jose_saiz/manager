<?php
//namespace App\Http\Controllers\Admin;
namespace App\Vendor\Greeny\Main;
set_time_limit(0);
use ArrayObject;
use DateTimeG;
//use App\Vendor\Greeny\Main\DateTimeG;
//////////////////////////////////////////////////////////// P R I N T E R S M A T R I X - C L A S S ////////////////////////////////////////////////////////////////
class PrintersMatrix extends ArrayObject
{
    public $neighborhood_factory_id;
    public $start_date_time;
    public $start_date_time_print;
    public $missing_parts;
    public $parts;
    public $available_printers;
    public $blocks_parts_matrix;
    public $no_block_parts;
    public $pending_parts_count;

    
    function __construct(int $neighborhood_factory_id, $start_date_time)
    {
        parent::__construct(array(), ArrayObject::ARRAY_AS_PROPS);
        $this->neighborhood_factory_id = $neighborhood_factory_id;
        
        $this->start_date_time_print = $start_date_time;
        
        //$this->start_date_time = new DateTimeG($this->start_date_time_print->format('Y-m-d H:i:s'));
        $this->start_date_time = DateTimeG::newDateTime($this->start_date_time_print->format('Y-m-d H:i:s'));
        if ($this->start_date_time->isOverlapAtNight()){
            if ($this->start_date_time->format('H') > 12) {
                $this->start_date_time->modify('+1 days');
            }
            $day_start = $this->start_date_time->format('w');
            $this->start_date_time->setTime(session('working_days')[$day_start]['from']['hour'], session('working_days')[$day_start]['from']['minute']);//+conf_delays()['minutes_start']);
        }
        
        
        /*
        $real_start_date_time = $start_date_time->createRealStartDateTime();
        $this->start_date_time = $real_start_date_time['start_date_time'];
        $this->start_date_time_print = $real_start_date_time['start_date_time_print'];
        */

        
    }
    
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// Clear //////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function clear(){
        //Vacía las timeline y missing parts  de una printer Matrix
        $printers = array();
        foreach ($this as $printerTimeline){
            $slots=array();
            foreach($printerTimeline as $key_timeline => $printerSlot){
                //unset ($printerTimeline[$key_timeline]);
                array_push($slots, $key_timeline); 
            }
            foreach ($slots as $slot){
                unset ($printerTimeline[$slot]);
            }
        }
        
        $missing_parts = array();
        foreach ($this->missing_parts as $key => $missing_part){
            //unset($this->missing_parts[$key]);
            array_push($missing_parts,$key);
        }
        foreach ($missing_parts  as $missing_part){
            unset($this->missing_parts[$missing_part]);
        }
        
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// New //////////////// //////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function new(PrintersMatrix $printersMatrix):PrintersMatrix
    {
        
        $newPrintersMatrix = new PrintersMatrix($printersMatrix->neighborhood_factory_id, $printersMatrix->start_date_time);
        foreach($printersMatrix as $printerTimeline){
            $printerTimeLineAux = PrinterTimeline::copy($printerTimeline);
            $newPrintersMatrix->append($printerTimeLineAux);
        }
        
        return $newPrintersMatrix;
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// Copy ///////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public static function copy(PrintersMatrix &$printersMatrixDestination,PrintersMatrix $printersMatrixOrigin)
    {
        foreach ($printersMatrixDestination as $key_timeline =>$printerTimeline){
            $slots=array();
            foreach ($printerTimeline as $key_slot=>$slot){
                array_push($slots, $key_slot); 
            }
            foreach ($slots as $slot){
                unset ($printerTimeline[$slot]);
            }
        }
        
        foreach($printersMatrixOrigin as $printerTimeline){
            $printerTimeLineAux = PrinterTimeline::copy($printerTimeline);
            $printersMatrixDestination->append($printerTimeLineAux);
        }
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// summary ///////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function summary(){
        $rdo = array();
        
        $parts = array();
        $parts['total'] = 0;
        $parts['printed'] =  array();
        $parts['assigned'] =  array();
        $parts['unassigned'] =  array();
        $parts['other'] =  array();
        $parts['missing'] =  array();
        
        $printers_count= 0;
        
        $greenies=array(array());
        $greenies['total'] = 0;
        $greenies['printed'] =  array();
        $greenies['assigned'] =  array();
        $greenies['partially_assigned'] =  array();
        $greenies['unassigned'] =  array();
        $greenies['other'] =  array();
        
        
        $test = array();
        $count=0;
        //Calculo Resumen del estado de PrinerMatrix
        foreach($this as $PrinterLine){
            $printers_count ++;
            foreach ($PrinterLine as $slot){
                // El slot no esta ni asignado, ni sin asignar, ni es otro caso
                if (
                    !in_array($slot->final_product, $greenies['printed'] )
                    ||
                    !in_array($slot->final_product, $greenies['assigned'] )
                    ||
                    !in_array($slot->final_product, $greenies['unassigned'] )
                    ||
                    !in_array($slot->final_product, $greenies['other'] )
                    )
                {
                    
                    //Resumen de Orders
                    if (($slot->state == 10) && (!in_array($slot->final_product, $greenies['printed'] ))){
                        array_push($greenies['printed'], $slot->final_product);
                        $greenies['total'] ++;
                    }elseif (($slot->state == 9) && (!in_array($slot->final_product, $greenies['assigned'] ))){
                        array_push($greenies['assigned'], $slot->final_product);
                        $greenies['total'] ++;
                    }elseif (($slot->state == 8)&& (!in_array($slot->final_product, $greenies['unassigned'] ))){
                        array_push($greenies['unassigned'], $slot->final_product);
                        $greenies['total'] ++;
                    }elseif (($slot->state != 8) && ($slot->state != 9) && ($slot->state != 10) && (!in_array($slot->final_product, $greenies['other']))){
                        //Esto debera ser siempre 0
                        array_push($greenies['other'], $slot->final_product);
                        $greenies['total'] ++;
                    }
                    
                }
                
                //Si el slot esta en asignadas y no asignadas es que debe estar en parcialmente asignadas: Lo pongo en parcialmente asignadas y lo quito de las otras
                if ((in_array($slot->final_product, $greenies['printed'] )) && (in_array($slot->final_product, $greenies['assigned'] )) && (in_array($slot->final_product, $greenies['unassigned'] ))){
                    //afadsfasf
                    //$greenies['total'] --;
                    $greenies['total'] -= 2;
                    array_push($greenies['partially_assigned'], $slot->final_product);
                    //busco las claves y lo borro de asignadas y no asignadas
                    $key = array_search($slot->final_product, $greenies['printed']);
                    unset($greenies['printed'][$key]);
                    $key = array_search($slot->final_product, $greenies['assigned']);
                    unset($greenies['assigned'][$key]);
                    $key = array_search($slot->final_product, $greenies['unassigned']);
                    unset($greenies['unassigned'][$key]);
                }
                
                //Resumen de Parts
                
                if ($slot->state == 9) $count ++;
                
                if (($slot->state == 10) && (!in_array($slot->part, $parts['printed'] ))){
                    array_push($parts['assigned'], $slot->part);
                    //array_push($test, $slot->part);
                }elseif (($slot->state == 9) && (!in_array($slot->part, $parts['assigned'] ))){
                    array_push($parts['assigned'], $slot->part);
                    array_push($test, $slot->part);
                }elseif (($slot->state == 8)&& (!in_array($slot->part, $parts['unassigned'] ))){
                    array_push($parts['unassigned'], $slot->part);
                }elseif (($slot->state != 8) && ($slot->state != 9) && (!in_array($slot->part, $parts['other']))){
                    //Esto debera ser siempre 0
                    array_push($parts['other'], $slot->part);
                }else{
                    array_push($parts['missing'], $slot->part);
                }
                
                $parts['total']++;
            }
        }
        
        $rdo['parts'] = $parts;
        $rdo['greenies'] = $greenies;
        $rdo['printers_count'] = $printers_count;
        
        return $rdo;
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// New Pending Part ///////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function newPendingPart(array $array):array{
        $pending_part = array();
        
        $pending_part['id'] = $array['id'];
        $pending_part['order_id'] = $array['order_id'];
        $pending_part['file_id'] = $array['file_id'];
        $pending_part['file_part_name'] = $array['file_part_name'];
        $pending_part['file_name'] = $array['file_name'];
        $pending_part['version_part_name'] = $array['version_part_name'];
        $pending_part['product_id'] = $array['product_id'];
        $pending_part['product_name'] = $array['product_name'];
        $pending_part['final_product_id'] = $array['final_product_id'];
        $pending_part['weight'] = $array['weight'];
        $pending_part['state_id'] = '8';
        $pending_part['initiated'] = $array['initiated'];
        
        return $pending_part;
    }
    
    public function addSlotToPrinter($printer_id, $code_printer, $id_part = NULL, $order_id = NULL,
                                    $file = NULL,$file_name = NULL,$version=NULL,
                                    $product = NULL, $product_name = NULL, $final_product = NULL, 
                                    $estimated_printing_start = NULL, $estimated_printing_end = NULL, $part_weight = NULL, $state = NULL, $initiated = NULL)
    {
        $printerSlot = new PrinterSlot($id_part, $order_id, $file, $file_name, $version, $product, $product_name, $final_product, 
                                       $estimated_printing_start,$estimated_printing_end, $part_weight, $state,$initiated);
        
        $this->append($printerSlot);
        
        //TODO !!!!!!!!!!!!!!!!!!!!!!! añadir a la bdd
        
        return true;
    }
    
    public function getPrinter($printer_id){
        
        foreach ($this as $printerTimeline){
            
            if ($printerTimeline->printer_id == $printer_id){
                return  $printerTimeline;
            }
        }
        return 0;
    }
    public function getGapStart(DateTimeG $start):DateTimeG
    {
        $gap_start  = new DateTimeG($this->start_date_time);
        //$gap_start = new DateTime($this->start_date_time->format('Y-m-d H:i:s'));
        if ($gap_start->isOverlapAtNight()) {
            //Parece que lo correcto es: if ($gap_start->format('H') > conf_workday()['from']['hour'] ){
            if ($gap_start->format('H') > 23) {
                $gap_start->modify('+1 days');
            }
            $day_start = $start->format('w');
            $gap_start->setTime(session('working_days')[$day_start]['from']['hour'], session('working_days')[$day_start]['from']['minute']);
        } else {
            if ($gap_start < $this->start_date_time) {
                $gap_start->setTime($this->start_date_time->format('H'), $this->start_date_time->format('i'));
            } else {
                //$gap_start -> setTime(conf_workday()['to']['hour'],conf_workday()['to']['minute']);
            }
        }
        //Si la hora actual(start_date_time) es mayor que la hora de inicio de jornada entonces el hueco empieza en la hora actual.
        
        //$gap_start->modify('+' . conf_delays()['minutes_start'] . ' minutes');
        return $gap_start;
    }
    public function searchPrinterTimeLine(array $printer): PrinterTimeline
    {
        $current_printer_time_line = new PrinterTimeline($printer['id'], $printer['code'], $printer['roll_weight'],$printer['roll_replacement_datetime']);
        foreach ($this as $printerTimeLine) {
            if ($printer['id'] == $printerTimeLine->printer_id) {
                $current_printer_time_line = $printerTimeLine;
            }
        }
        return $current_printer_time_line;
    }

    public function searchPrintersTimeLineWhitoutWork(){
        $printerTimeLineWhitoutWork = array();
        
        for ($i=0;$i< count($this);$i++){
            if (($this[$i]->getPrintingMinutes($this->start_date_time) == 0)&&($this[$i]->available == 1)){
                $aux = array();
                $aux['printer'] = $this[$i];
                $aux['index'] = $i;
                array_push($printerTimeLineWhitoutWork,$aux);
            }
        }
        return $printerTimeLineWhitoutWork;
    }
    
    public function searchPrinterTimeLineMoreIdle(): PrinterTimeline
    {
        $printerTimeLineMoreIdle = $this[0];
        
        
        $printing_0_minutes =  new PrinterTimeline();
        
        for ($i=0;$i< count($this);$i++){
            if ($this[$i]->available==0){
                break;
            }
            if ($this[$i]->getPrintingMinutes($this->start_date_time) == 0){
                $printing_0_minutes = $this[$i];
            }
            if ($printerTimeLineMoreIdle->getPrintingMinutes($this->start_date_time) > $this[$i]->getPrintingMinutes($this->start_date_time)){
                $printerTimeLineMoreIdle =  $this[$i];
            }
        }
        if ($printerTimeLineMoreIdle->available == 0){
            return null;
        }
        
        if ($printing_0_minutes->printer_id != NULL){
            return $printing_0_minutes;
        }
        
        return $printerTimeLineMoreIdle;
    }
    
    
    public function searchPrinterTimeLineFinishFirst(): PrinterTimeline
    {
        $printerTimeLineFinishFirst = $this[0];
        $this[0]->uasort('startDescendingComparison');
        $min_datetime = new DateTimeG;
        $first_iteration=true;
        $printing_0_minutes =  new PrinterTimeline();
        for ($i=0;$i< count($this);$i++){
            if ($this[$i]->available == 0){
                //break;
                //$printerTimeLineFinishFirst = $this[$i+1];
                continue;
            }
            if ($this[$i]->getPrintingMinutes($this->start_date_time) == 0){
                $printing_0_minutes = $this[$i];
            }
            
            $this[$i]->uasort('startDescendingComparison');
            $arrayIteratorPrinter = $this[$i]->getIterator();
            if ( $arrayIteratorPrinter->valid()){
                $slot = $arrayIteratorPrinter->current();
                $end = DateTimeG::newDateTime($slot->end);
                
                if ($first_iteration || $end < $min_datetime){
                    $printerTimeLineFinishFirst = $this[$i];
                    $min_datetime = DateTimeG::newDateTime( $slot->end);
                }
                $first_iteration = false;
            }
        }
        if ($printerTimeLineFinishFirst->available == 0){
            return null;
        }
        
        if ($printing_0_minutes->printer_id != NULL){
            return $printing_0_minutes;
        }
        return $printerTimeLineFinishFirst;
    }
    
    public function searchPrinterTimeLineFitBiggestGap(array $pending_part): PrinterTimeline
    {
        $printerTimeLine = $this[0];
        $pending_part_minutes = $pending_part['estimated_printing_hours']*60+$pending_part['estimated_printing_minutes'];
        for ($i=0;$i<count($this);$i++){
            $biggest_gap = $this[$i]->getBiggestGap($this->start_date_time);
            if ($biggest_gap['minutes'] > $pending_part_minutes ) {
                $printerTimeLine =  $this[$i];
                break;
            }
        }
        return $printerTimeLine;
    }
    
    
    public function searchPrinterTimeLineBiggestGap(): array
    {
        $printerTimeLine = $this[0];
        $max_gap_minutes = 0;
        for ($i=0;$i<count($this);$i++){
            $biggest_gap = $this[$i]->getBiggestGap($this->start_date_time);
            if ($biggest_gap['minutes'] > $max_gap_minutes ) {
                $printerTimeLine =  $this[$i];
                $max_gap_minutes = $biggest_gap['minutes'];
            }
        }
        $printerTimeLine_biggestGap = array ('printerTimeLine' =>$printerTimeLine, 'biggestGap'=> $biggest_gap );
        return $printerTimeLine_biggestGap;
    }
    
    
    
    public function searchPrinterTimeLineWithGap(array $pending_part): PrinterTimeline
    {
        $printerTimeLine = $this[0];
        
        for ($i=0;$i<count($this);$i++){
            $pending_part_minutes = $pending_part['estimated_printing_hours']*60+$pending_part['estimated_printing_minutes'];
            $biggest_gap = $this[$i]->getBiggestGap($this->start_date_time);
            if ($biggest_gap['minutes'] > $pending_part_minutes ){
                $printerTimeLine =  $this[$i];
                break;
            }
        }
        return $printerTimeLine;
    }
    
    public function searchPrinterTimeLineWithGapNoRepeat(array $pending_part, PrinterTimeline $printerTimeLine): PrinterTimeline
    {
        $biggest_gap = $printerTimeLine->getBiggestGap($this->start_date_time);
        for ($i=0;$i<count($this);$i++){
            $pending_part_minutes = $pending_part['estimated_printing_hours']*60+$pending_part['estimated_printing_minutes'];
            if ($this[$i]->code_printer != $printerTimeLine->code_printer){
                $biggest_gap = $this[$i]->getBiggestGap($this->start_date_time);
            }
            
            if ($biggest_gap['minutes'] > $pending_part_minutes ){
                $printerTimeLine =  $this[$i];
                //break;
            }
        }
        
        return $printerTimeLine;
    }
    
    public function searchPrinterTimeLineWithGapNoRepeat2(array $pending_part, PrinterTimeline $printerTimeLine): PrinterTimeline
    {
        $biggest_gap = $printerTimeLine->getBiggestGap($this->start_date_time);
        $code_printer_ini = $printerTimeLine->code_printer;
        for ($i=0;$i<count($this);$i++){
            $pending_part_minutes = $pending_part['estimated_printing_hours']*60+$pending_part['estimated_printing_minutes'];
            if ($this[$i]->code_printer != $printerTimeLine->code_printer){
                $biggest_gap = $this[$i]->getBiggestGap($this->start_date_time);
            }
            
            if ($biggest_gap['minutes'] > $pending_part_minutes ){
                $printerTimeLine =  $this[$i];
                //break;
            }
        }
        if ( $code_printer_ini== $this[count($this)-1]->code_printer){
            return $this[0];
        }
        
        return $printerTimeLine;
    }
    
    
    
    
    
    public function searchPrinterTimeLineMin(): PrinterTimeline
    {
        $printerTimeLineResult = $this[0];
        $min_slot_last_datetime  = new DateTimeG($this->start_date_time);
        $min_slot_last_datetime -> modify('+3 month');
        foreach ($this as $printerTimeLine){
            $slot_last_datetime =  new DateTimeG($this->start_date_time);
            foreach ($printerTimeLine as $slot){
                if ( new DateTimeG($slot->end) > $slot_last_datetime){
                    $slot_last_datetime = new DateTimeG($slot->end) ;
                }
            }
            if ($slot_last_datetime <= $min_slot_last_datetime ){
                $printerTimeLineResult = $printerTimeLine;
                $min_slot_last_datetime = $slot_last_datetime;
            }
        }
        
        return $printerTimeLineResult;
    }
    
    
    public function searchPrinterTimeLineMin2(): PrinterTimeline
    {
        $printerTimeLineResult = $this[0];
        $min_slot_last_datetime  = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
        //$min_slot_last_datetime -> modify('+3 month');
        foreach ($this as $printerTimeLine){
            $slot_last_datetime =  DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
            foreach ($printerTimeLine as $slot){
                if ( DateTime::CreateFromFormat('Y-m-d H:i:s',$slot->end) > $slot_last_datetime){
                    $slot_last_datetime = DateTime::CreateFromFormat('Y-m-d H:i:s',$slot->end) ;
                }
            }
            if ($slot_last_datetime <= $min_slot_last_datetime ){
                $printerTimeLineResult = $printerTimeLine;
                $min_slot_last_datetime = $slot_last_datetime;
            }
        }
        
        return $printerTimeLineResult;
    }
    
    
    
    public function truncatePrinterTimeLineBiggestGap(): PrinterTimeline
    {
        $printerTimeLine = $this[0];
        
        for ($i=0;$i<count($this);$i++){
            
            $biggest_gap = $this[$i]->getBiggestGap($this->start_date_time);
            $delete = false;
            //array_multisort(array_column($this->missing_parts, 'weight'), SORT_DESC, $this->missing_parts);
            
            $this[$i]->uasort('startAscendingComparison');
            
            $aux_slots = array(array());
            $j = 0;
            //Recorro la PrinterLine acutal y la guardo en un array auxiliar. Para luego poder BorarBorrar sin problemas.
            foreach ($this[$i] as $key => $slot){
                $aux_slots[$j]['key'] = $key;
                $aux_slots[$j]['slot'] = $slot;
                $j++;
            };
            //Se borra el hueco grande. Para ello buscamos el hueco y borramos todos los slots que hay despues.
            $delete = false;
            for ($j=0;$j<count($aux_slots);$j++){
                //echo $key.' -> '.$slot->part.'-<br>';
                if (
                    (isset($aux_slots[$j]['slot']))
                    &&
                    (
                        ($delete)
                        ||
                        (($aux_slots[$j]['slot']->part == $biggest_gap['slot']->part))&&($biggest_gap['minutes'] > 180)
                        )
                    ){
                        //Actualizo las partes no asignadas y borro el slot de la PrinterLine
                        $this->missing_parts[$aux_slots[$j]['slot']->part] =$this->parts[$aux_slots[$j]['slot']->part];
                        unset($this[$i][$aux_slots[$j]['key']]);
                        
                        
                        
                        $delete = true;
                }
            }
            unset($key);
        }
        return $printerTimeLine;
    }
    
    public function unitTest($fh,$module)
    {
        
        echo "<br>".$this->start_date_time->format('Y-m-d H:i:s')."<br>";
        
        $part_count = 0;
        
        $code_printers= array();
        $parts = array();
        $ranges = array();
        $assignaments = array();
        $assignaments_totals = array();
        $assignaments_totals['created']=0;
        $assignaments_totals['assigned']=0;
        $assignaments_totals['separated']=0;
        
        $start_min = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
        $end_max = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
        $end_min = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
        $percent_sum = 0;
        
        $printing_minutes_totals = 0;
        $gap_minutes_totals = 0;
        
        $overlaps_night = array();
        $count_overlaps_night = 0;
        $overlaps=array();
        $count_overlaps = 0;
        
        $i = 0;
        $first_iteration = true;
        
        $total_parts = array();
        $created = array();
        $assigned = array();
        $separated = array();
        $printing_minutes = array();
        $gap_minutes = array();
        $percent = array();
        
        foreach ($this as $printerTimeLine) {
            
            $printerTimeLine->uasort('startAscendingComparison');
            $code_printers[$i] = $printerTimeLine->code_printer;
            
            $ranges[$i]=$printerTimeLine->getRange();
            
            if ($first_iteration){
                $end_min = DateTime::CreateFromFormat('Y-m-d H:i:s',$ranges[$i]['end']);
            }
            $first_iteration = false;
            
            $start = DateTime::CreateFromFormat('Y-m-d H:i:s',$ranges[$i]['start']);
            if ($start< $start_min){
                $start_min = $start;
            }
            $end = DateTime::CreateFromFormat('Y-m-d H:i:s',$ranges[$i]['end']);
            if ($end > $end_max){
                $end_max = $end;
            }
            if ($end < $end_min){
                $end_min = $end;
            }
            
            $assignaments = $printerTimeLine->getAssignaments();
            $parts[$i] = count($printerTimeLine).';'.$assignaments['created'].';'.$assignaments['assigned'].';'.$assignaments['separated'].';'.
                    $printerTimeLine->getPrintingMinutes($this->start_date_time).';'.
                    $printerTimeLine->getGapsMinutes($this->start_date_time).';'.
                    number_format($printerTimeLine->percent($this->start_date_time),2);
                    
            $total_parts[$i] = count($printerTimeLine);
            $created[$i] = $assignaments['created'];
            $assigned[$i] = $assignaments['assigned'];
            $separated[$i] = $assignaments['separated'];
            $printing_minutes[$i] = $printerTimeLine->getPrintingMinutes($this->start_date_time);
            $gap_minutes[$i] = $printerTimeLine->getGapsMinutes($this->start_date_time);
            $percent[$i]= number_format($printerTimeLine->percent($this->start_date_time),2);
            
            $percent_sum += $printerTimeLine->percent($this->start_date_time);
            $assignaments_totals['created']+=$assignaments['created'];
            $assignaments_totals['assigned']+=$assignaments['assigned'];
            $assignaments_totals['separated']+=$assignaments['separated'];
            $printing_minutes_totals += $printerTimeLine->getPrintingMinutes($this->start_date_time);
            $gap_minutes_totals += $printerTimeLine->getGapsMinutes($this->start_date_time);
            
            foreach ($printerTimeLine as $printer_slot){
                if (isOverlapAtNight(DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start))){
                    $overlaps_night[$i][$printer_slot->part]= $printer_slot->file;
                    $count_overlaps_night++;
                    echo $printerTimeLine->code_printer.': '. $printer_slot->part.':'.$printer_slot->file.' <br/>';
                }
                
            }
            //$count_overlaps += $printerTimeLine->countOverlaps($this->start_date_time);
            $overlap_aux = $printerTimeLine->getOverlaps($this->start_date_time);
            if (count($overlap_aux)!=0){
                $overlaps[$i] = $overlap_aux;
            }
            
            $part_count += count($printerTimeLine);
            $i++;
            
            
        }
        $text ="Printer Code;Start;End;Parts;Created;Assigned;Separated;Minutes Printing; Minutes Stop; Efficient".PHP_EOL;
        //fwrite($fh, $text) or die("No se pudo escribir en el archivo");
        for ($i=0;$i<count($code_printers);$i++){
            if (isset($overlaps[$i])){
                $overlap =  count($overlaps[$i]);
            }else{
                $overlap = 0;
            }
            if (isset($overlaps_night[$i])){
                $overlap_night =  count($overlaps_night[$i]);
            }else{
                $overlap_night = 0;
            }
            $text = $code_printers[$i].';'.
                    $ranges[$i]['start'].';'.
                    $ranges[$i]['end'].';'.
                    $parts[$i].';'.
                    $overlap.';'.
                    $overlap_night.
                    PHP_EOL;
            //fwrite($fh, $text) or die("No se pudo escribir en el archivo");
        }
        $text =";;;;;;;;;".PHP_EOL;
        //fwrite($fh, $text) or die("No se pudo escribir en el archivo");
        
        
        $text =";;;;;;;;;;;;;;".PHP_EOL;
        $printing_minutes_totals =  intdiv($printing_minutes_totals,count($code_printers));
        $gap_minutes_totals = intdiv($gap_minutes_totals,count($code_printers));
        if ($start_min!=false){
            
            $hours_min = $start_min->diff($end_min)->format('%h');
            $minutes_min = $start_min->diff($end_min)->format('%i');
            $days_min = $start_min->diff($end_min)->format('%d');
            $months_min = $start_min->diff($end_min)->format('%m');
            if ($months_min >1){
                $days_min += $months_min*30;
            }
            
            $hours_max = $start_min->diff($end_max)->format('%h');
            $minutes_max = $start_min->diff($end_max)->format('%i');
            $days_max = $start_min->diff($end_max)->format('%d');
            $months_max = $start_min->diff($end_max)->format('%m');
            if ($months_max >1){
                $days_max += $months_max*30;
            }
            
            
            $printing = calcMinutes($printing_minutes_totals);
            $gap = calcMinutes($gap_minutes_totals);
            $average = calcMinutes($printing_minutes_totals+$gap_minutes_totals);
        

            
            $text = count($code_printers).';'.
                    $start_min->format('Y-m-d H:i:s').';'.
                    $end_max->format('Y-m-d H:i:s').';'.
                    $part_count.';'.
                    $assignaments_totals['created'].';'.
                    $assignaments_totals['assigned'].';'.
                    $assignaments_totals['separated'].';'.
                    $printing_minutes_totals.';'.
                    $gap_minutes_totals.';'.
                    $count_overlaps.';'.
                    $count_overlaps_night.';'.
                    number_format($percent_sum/count($code_printers),2).';'.
                    'Days: '.$days_max.' - Hours: '.$hours_max.' - Minutes: '.$minutes_max.';'.
                    'Days: '.$days_min.' - Hours: '.$hours_min.' - Minutes: '.$minutes_min.';'.
                    'Days: '.$average['days'].' - Hours: '.$average['hours'].' - Minutes: '.$average['minutes'].';'.
                    'Days: '.$printing['days'].' - Hours: '.$printing['hours'].' - Minutes: '.$printing['minutes'].';'.
                    'Days: '.$gap['days'].' - Hours: '.$gap['hours'].' - Minutes: '.$gap['minutes'].
                    PHP_EOL;
                    echo "<br>--- Asignadas: ".$assignaments_totals['assigned']." - End:".$end_max->format('Y-m-d H:i:s')." - Efficient: ".number_format($percent_sum/count($code_printers),2).'<br>';
                
        }else{
            $text = count($code_printers).';'.
                ''.';'.
                ''.';'.
                $part_count.';'.
                $assignaments_totals['created'].';'.
                $assignaments_totals['assigned'].';'.
                $assignaments_totals['separated'].';'.
                $printing_minutes_totals.';'.
                $gap_minutes_totals.';'.
                number_format($percent_sum/count($code_printers),2).';'.
                ''. ';'.
                ''.';'.
                ''.';'.
                ''.';'.
                ''.
                PHP_EOL;
        }
                
        fwrite($fh, $text) or die("No se pudo escribir en el archivo");
        
        
        
        
        if (isset($overlaps)){
            $overlap =  count($overlaps);
        }else{
            $overlap = 0;
        }
        if (isset($overlaps_night)){
            $overlap_night =  count($overlaps_night);
        }else{
            $overlap_night = 0;
        }
        
        /*
        if (!$start_min){
            $start_min = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
        }
        */
        
        $sql = sprintf('INSERT INTO unit_test 
                        (
                        printers,
                        start_min,
                        end_max,
                        part_count,
                        created_parts,
                        assigned_parts,
                        separated_parts,
                        minutes_printing,
                        minutes_stop,
                        overlaps,
                        overlaps_night,
                        efficent,
                        max_day,
                        max_hours,
                        max_minutes,
                        min,
                        average_printing,
                        average_stop,
                        module
                        )
                    VALUES
                        (' .
                        count($code_printers).',"'.
                        $start_min->format('Y-m-d H:i:s').'","'.
                        $end_max->format('Y-m-d H:i:s').'",'.
                        $part_count.','.
                        $assignaments_totals['created'].','.
                        $assignaments_totals['assigned'].','.
                        $assignaments_totals['separated'].','.
                        $printing_minutes_totals.','.
                        $gap_minutes_totals.','.
                        $overlap.','.
                        //$overlap_night.','.
                        $count_overlaps_night.','.
                        number_format($percent_sum/count($code_printers),2).','.
                        $days_max.', '.$hours_max.', '.$minutes_max.',"'.
                        $hours_min.'h. '.$minutes_min.'m.","'.
                        $printing['hours'].'h. '.$printing['minutes'].'m.","'.
                        $gap['hours'].'h. '.$gap['minutes'].'m.",'.
                        $module.
                        ')'
                    );
        
        $result = DataBase::getConnection()->query($sql);
        
        $id_inserted = "";
        if ($result) {
            $id_inserted = (string) mysqli_insert_id(DataBase::getConnection());
        }
        
        for ($i=0;$i<count($code_printers);$i++){
            if (isset($overlaps[$i])){
                $json_overlap =  json_encode($overlaps[$i]);
            }else{
                $json_overlap = "";
            }
            if (isset($overlaps_night[$i])){
                $json_overlap_night =  json_encode($overlaps_night[$i]);
            }else{
                $json_overlap_night = "";
            }
        
            $sql = sprintf('INSERT INTO unit_test_detail
                        (
                        id_unit_test,
                        start,
                        end,
                        parts,
                        code_printer,
                        created,
                        assigned,
                        separated,
                        printing_minutes,
                        gap_minutes,
                        percent,
                        overlaps,
                        overlaps_night
                        )
                    VALUES
                        (' .
                        $id_inserted.',"'.
                        $ranges[$i]['start'].'","'.
                        $ranges[$i]['end'].'",'.
                        $total_parts[$i].',"'.
                        $code_printers[$i].'",'.
                        $created[$i].','.
                        $assigned[$i].','.
                        $separated[$i].','.
                        $printing_minutes[$i].','.
                        $gap_minutes[$i].','.
                        $percent[$i].','. '\'' . 
                        $json_overlap. '\'' . ','. '\'' .
                        $json_overlap_night. '\'' .
                        ')'
            );
            $result = DataBase::getConnection()->query($sql);
        }
        
       
    }
    
    public function printAll()
    {
        echo ('</br>');
        $part_count = 0;
        foreach ($this as $printer_time_line) {
            $count = $this->print($printer_time_line);
            echo "<br>Subtotal: ". $count."<br>";
            $part_count += $count;
        }
        echo "<br/>Num Total Parts: ". $part_count;
        if (isset($this->missing_parts)){
            echo "<br/>Num Total Parts Unassinged: ". count($this->missing_parts);
            echo "<br/>Unsigned Details:<br/>";
            
            $resumen_missing_parts = array();
            $time_missing_parts = array();
            foreach($this->missing_parts as $parts){
                if (array_key_exists($parts['file_id'], $resumen_missing_parts)){
                    $resumen_missing_parts[$parts['file_id']]++;
                }else{
                    $resumen_missing_parts[$parts['file_id']]=1;
                    $time_missing_parts[$parts['file_id']] = $parts['estimated_printing_hours'];
                }
            }
            foreach($resumen_missing_parts as $key => $value){
                echo 'File: '.$key.'='.$value.'-'.$time_missing_parts[$key]. 'h.<br>';
                
            }
            
            
        }
    }
    
    public function printAllMissing()
    {
        echo "------------------------------------------ M I S S I N G - P A R T S ----------------------------------------------";
        echo ('</br>');
        $part_count = 0;
        foreach ($this as $printer_time_line) {
            $count = $this->countParts($printer_time_line);
            //echo "<br>Subtotal: ". $count."<br>";
            $part_count += $count;
        }
        echo "<br/>Num Total Parts: ". $part_count;
        if (isset($this->missing_parts)){
            echo "<br/>Num Total Parts Misssing: ". count($this->missing_parts);
            echo "<br/>Missing Details:<br/>";
            
            $resumen_missing_parts = array();
            $time_missing_parts = array();
            $parts_unassinged_parts = array();
            foreach($this->missing_parts as $parts){
                if (array_key_exists($parts['file_id'], $resumen_missing_parts)){
                    $resumen_missing_parts[$parts['file_id']]++;
                    $parts_unassinged_parts[$parts['file_id']].=','.$parts['id'];
                }else{
                    $resumen_missing_parts[$parts['file_id']]=1;
                    $time_missing_parts[$parts['file_id']] = $parts['estimated_printing_hours'].':'. $parts['estimated_printing_minutes'];
                    $parts_unassinged_parts[$parts['file_id']]=$parts['id'];
                }
            }
            foreach($resumen_missing_parts as $key => $value){
                echo 'File: '.$key.'->('.$parts_unassinged_parts[$key].') ='.$value.'->'.$time_missing_parts[$key]. 'h.<br>';
                
            }
            
            
        }
    }
    
    
    public function printAllSimple()
    {
        echo ('</br>');
        $part_count = 0;
        foreach ($this as $printer_time_line) {
            $part_count += count($printer_time_line);
        }
        echo "Num Total Parts: ". $part_count;
        if (isset($this->missing_parts)){
            echo "<br/>Num Total Parts Unassinged: ". count($this->missing_parts);
            //var_dump($this->missing_parts);
        }
    }
    
    public function print(PrinterTimeline $printerTimeLine):int
    {
        echo ('</br>');
        echo ('</br><b>Name printer>>></b>');
        print_r($printerTimeLine->code_printer);
        echo ('</br>');
        $part_count = 1;
        foreach ($printerTimeLine as $slot_part_printer) {
            echo $part_count . ": ";
            print_r($slot_part_printer);
            echo ('</br>');
            $part_count ++;
        }
        return  $part_count -1;
    }
    
    public function toCsv(PrinterTimeline $printerTimeLine):int
    {
        echo ('</br>');
        echo ('</br><b>Name printer>>></b>');
        print_r($printerTimeLine->code_printer);
        echo ('</br>');
        $part_count = 1;
        foreach ($printerTimeLine as $slot_part_printer) {
            echo $part_count . ": ";
            print_r($slot_part_printer);
            echo ('</br>');
            $part_count ++;
        }
        return  $part_count -1;
    }
    
    public function countParts(PrinterTimeline $printerTimeLine):int
    {
        $part_count = 1;
        foreach ($printerTimeLine as $slot_part_printer) {
            $part_count ++;
        }
        return  $part_count -1;
    }
    
    public function countNotInitiatedParts():int
    {
        $count_not_initated_parts = 1;
        foreach ($this as $printer_timeline){
            foreach ($printer_timeline as $slot_part_printer) {
                if ($slot_part_printer->initiated == 0){
                    $count_not_initated_parts ++;
                }
                
            }
        }
        return  $count_not_initated_parts -1;
    }
    

    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////// Print Gap and Slot ////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function printGapSlot(
                        &$gap_start,$start_last,$end_last,$start_real,
                        $slot,
                        $tooltip_text,
                        $printerTimeLine,
                        $is_first_iteration,$is_first_stlot,$scale,
                        $height_gap,$height_slot,
                        $slot_total_hours_real,
                        $smallest_slot_size,$small_slot_size,$medium_slot_size,$big_slot_size,$minimum_slot_size)
    {
        $gap_start_last = $gap_start;
        $gap_end_last = DateTimeG::CreateFromFormat('Y-m-d H:i:s',$start_last);
        
        //pinta gap del last slot
        $gap_interval_last = $gap_start_last->diff($start_last);
        $gap_total_minutes_last = $gap_interval_last->format('%d') * 24 * 60 + $gap_interval_last->format('%h') * 60 + $gap_interval_last->format('%i');
        $gap_pixels_last = intdiv($gap_total_minutes_last, $scale);
        
        $background_gap = "grey";
        if ($is_first_iteration){
            $background_gap = "rgb(237,237,237)";
        }
        echo '<div class="gap" style="height:' . $height_gap . ';width:' . $gap_pixels_last . 'px;float:left;background:'.$background_gap.';border: solid;border-width: 0px;">';
        echo '</div>';
        
        //pinta l last slot
        $padding_top = 10;
        
        $slot_interval_last = $start_last->diff($end_last);
        $slot_interval_real_last = $start_real->diff($end_last);
        
        $slot_total_minutes_last = $slot_interval_last->format('%d') * 24 * 60 + $slot_interval_last->format('%h') * 60 + $slot_interval_last->format('%i');
        
        //Apaño por bug en la función diff para la fechas: $end=2022-03-26 11:00:00, $start=2022-03-27 09:00:00
        //30-03$end_aux = createDateTime($start->format('Y-m-d H:i:s'));
        $end_aux_last = $start_last->createDateTime();
        
        $end_aux_last ->modify("+ ".$slot_total_minutes_last."minutes");
        if ($end_aux_last != $end_last){
            $slot_interval_aux_last = date_diff($end, $end_aux_last);
            $slot_total_minutes_aux_last = $slot_interval_aux_last->format('%d') * 24 * 60 + $slot_interval_aux_last->format('%h') * 60 + $slot_interval_aux_last->format('%i');
            $slot_total_minutes_last +=$slot_total_minutes_aux_last;
        }
        
        $slot_pixels_last = intdiv($slot_total_minutes_last, $scale);
        
        
        if  ($slot_pixels_last < $smallest_slot_size){
            $style_slot = 'padding-left: 0px; margin-top: 7px;';
            $padding_top = 0;
        }elseif  ($slot_pixels_last < $small_slot_size){
            $style_slot = 'padding-left: 5px; margin-top: 4px;';
            $padding_top = 0;
        }elseif  ($slot_pixels_last < $medium_slot_size){
            $style_slot = 'padding-left: 6px; margin-top: -7px;';
        }else {
            $padding_left = "padding-left: 20px;";
            $style_slot = $padding_left. 'margin-top: -7px;';
        }
        echo '<a name= "part_link_'.$printerTimeLine->last_slot_out_range->part.'">';
        echo '<div style="height:' . $height_slot . ';padding-top: '.$padding_top.'px;font-size: larger;font-family: sans-serif;width:' . $slot_pixels_last . 'px;float:left;
                          background-color: 
                               rgb('. conf_parts_colors()[$printerTimeLine->last_slot_out_range->file_part_name][0][0] . ','
                                    . conf_parts_colors()[$printerTimeLine->last_slot_out_range->file_part_name][0][1] . ','
                                    . conf_parts_colors()[$printerTimeLine->last_slot_out_range->file_part_name][0][2] . ');';
        echo              'color: rgb(255,255,255);';
        echo              'border: solid;border-width: 0px;">';
        echo '      <div style="height: 141px;' . $style_slot;
        echo              'background: repeating-linear-gradient(45deg,
                               rgb('. conf_parts_colors()[$printerTimeLine->last_slot_out_range->file_part_name][0][0] . ','
                                    . conf_parts_colors()[$printerTimeLine->last_slot_out_range->file_part_name][0][1] . ','
                                    . conf_parts_colors()[$printerTimeLine->last_slot_out_range->file_part_name][0][2] . ') 15px,
                          #000000 22px,
                               rgb('. conf_parts_colors()[$printerTimeLine->last_slot_out_range->file_part_name][0][0] . ','
                                    . conf_parts_colors()[$printerTimeLine->last_slot_out_range->file_part_name][0][1] . ','
                                    . conf_parts_colors()[$printerTimeLine->last_slot_out_range->file_part_name][0][2] . ') 35px
                         )"
                    >'; //0 / 10px 10px;';
                    
                        $text = "";
                        $slot_interval_real_last = $start_real->diff($end_last);
                        $slot_interval_last = $start_last->diff($end_last);
                        $slot_total_minutes_last = $slot_interval_last->format('%d') * 24 * 60 + $slot_interval_last->format('%h') * 60 + $slot_interval_last->format('%i');
                        
                        $slot_pixels_last = intdiv($slot_total_minutes_last, $scale);
                        
                        if  ($slot_pixels_last > $big_slot_size){
                         $text .= '<B>'.$printerTimeLine->last_slot_out_range->product_name .' - '.$printerTimeLine->last_slot_out_range->final_product. '</B><br>';
                         $text .= 'Order: ' . $printerTimeLine->last_slot_out_range->order . ' - ';
                         $text .= 'File:&nbsp; ' . $printerTimeLine->last_slot_out_range->file_part_name . '</br>';
                         $text .= 'Part: ' . $printerTimeLine->last_slot_out_range->part . ' -';
                         $text .=  $printerTimeLine->last_slot_out_range->file_name . '</br>';
                         $text .= 'Days: ' . $slot_interval_real_last->format('%d') . ' - Time: ' . $slot_total_hours_real. ':' . $slot_interval_real_last->format('%i') . '</br>';
                         $text .= $start_last->format('d/m/Y H:i') . '</br>';
                         $text .=  $end_last->format('d/m/Y H:i') . '</br>';
                        }elseif  ($slot_pixels_last > $medium_slot_size){
                         $text .= '<B>'.$printerTimeLine->last_slot_out_range->product_name .' - '.$printerTimeLine->last_slot_out_range->final_product. '</B><br>';
                         $text .= 'O: ' . $printerTimeLine->last_slot_out_range->order . '';
                         $text .= 'F:&nbsp; '. $printerTimeLine->last_slot_out_range->file_part_name . '</br>';
                         $text .= 'P: ' . $printerTimeLine->last_slot_out_range->part . ' </br> ';
                         $text .=  $printerTimeLine->last_slot_out_range->file_name . '</br>';
                         $text .= 'D: ' . $slot_interval_real_last->format('%d') . ' - Time: ' . $slot_total_hours_real. ':' . $slot_interval_real_last->format('%i') . '</br>';
                         $text .= $start_last->format('d/m/Y H:i') . '</br>';
                         $text .=  $end_last->format('d/m/Y H:i') . '</br>';
                        }elseif ($slot_pixels_last > $small_slot_size){
                         $text .= '<B>'.$printerTimeLine->last_slot_out_range->final_product. '</B> - ';
                         $text .= 'O: ' . $printerTimeLine->last_slot_out_range->order . '</br>';
                         $text .= 'F:&nbsp; ' . $printerTimeLine->last_slot_out_range->file_part_name . '</br>';
                         $text .= 'P: ' . $printerTimeLine->last_slot_out_range->part . '</br> ';
                         $text .=  $printerTimeLine->last_slot_out_range->file_name . '</br>';
                         $text .= 'D: ' . $slot_interval_real_last->format('%d') . ' T: ' . $slot_total_hours_real. ':' . $slot_interval_real_last->format('%i') . '</br>';
                         $text .= $start_last->format('d/m/Y H:i') . '</br>';
                         $text .=  $end_last->format('d/m/Y H:i') . '';
                        }elseif ($slot_pixels_last > $smallest_slot_size){
                         $text .= 'O: ' . $printerTimeLine->last_slot_out_range->order . '</br>';
                         $text .= 'F: ' . $printerTimeLine->last_slot_out_range->file_part_name . '</br>';
                         $text .= 'P: ' . $printerTimeLine->last_slot_out_range->part . '</br>';
                         $text .= 'T: ' . $slot_total_hours_real . ':' . $slot_interval_real_last->format('%i') . '</br>';
                         $text .= $start_last->format('H:i') . ' - ';
                         $text .= $end_last->format('H:i') . '</br></br>'.'<br><br>';
                        }elseif ($slot_pixels_last > $minimum_slot_size){
                         $text .= 'O: ' . $printerTimeLine->last_slot_out_range->order . '</br>';
                         $text .= 'F: ' . $printerTimeLine->last_slot_out_range->file_part_name . '</br>';
                         $text .= 'P: ' . $printerTimeLine->last_slot_out_range->part . '</br>';
                         $text .= $slot_interval_real_last->format('%h') . ':'.$slot_interval_real_last->format('%i') ;
                         if ((($is_first_stlot)||($printerTimeLine->last_slot_out_range->initiated == 1)) && (($printerTimeLine->last_slot_out_range->state == 9)||($printerTimeLine->last_slot_out_range->state == 10))){
                             $text .= '<br><br>';
                         }else{
                             $text .= '<br><br><br>';
                         }
                        }else{
                         $text .= '</br></br></br></br></br></br></br>';
                        }
                        
                        
                        
                        $class = '';
       /* 
        echo            '<a '.$class.'  title="'.  $tooltip_text.'" style ="color: yellow;text-decoration: none;" class="submit tooltip-text-aux" 
                                        href="reassign_part.php?
                                        part=' .$slot->part . '
                                        &start-date-time=' .  $this->start_date_time_print->format('Y-m-d H:i:s') . '" 
                                        target="_self" >';
        */
        echo            '<a '.$class.'  title="'.  $tooltip_text.'" style ="color: yellow;text-decoration: none;" class="submit tooltip-text-aux">';
        
        echo                $text;
        echo            '</a>';
        echo '      </div>';
        echo '</div>';
        echo '</a>';
                
        echo '<div   title="'.  $tooltip_text.'" style ="position: absolute;margin-top: 42px;width: 68px;margin-left: '.($slot_total_minutes_last+$gap_pixels_last-64).'px;" class="tooltip-text-aux">';
        echo    $printerTimeLine->last_slot_out_range->order.'<br>'.
                $printerTimeLine->last_slot_out_range->file_part_name.'<br>'.
                $printerTimeLine->last_slot_out_range->part;
             $a_c_start_last = '';
             $a_c_end_last =   '';
             if ($printerTimeLine->last_slot_out_range->state == 9){
                 $a_c_start_last = '<a  id="button-time-line-cancel-{{$printerTimeLine->last_slot_out_range->part}}" class="button-time-line"
                                        href= "/admin/parts/cancel/'.$this->start_date_time_print->format('Y-m-d H:i:s').'/'
                                                                    .$printerTimeLine->last_slot_out_range->part.'/'
                                                                    .$printerTimeLine->printer_id.'">';
                 $img_c_start_last=   'aaa'.'<img  style="width:50px" src="'.'/assets/admin/images/cancel.png'.'">';
                 $a_c_end_last =   '</a>';
             }else{
                 $img_c_start_last=   'bbb'.'<img  style="width:50px" src="'.'/assets/admin/images/printed.png'.'">';
             }
        echo    $a_c_start_last.$img_c_start_last.$a_c_end_last;
        echo '</div>';
         //$gap_start = $gap_start_last;
        $gap_start = $end_last;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////// Print Slots ////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function printSlots(int $zoom, int &$total_pixels, PrinterTimeline $printerTimeLine,array $next_part): int
    {
        //var_dump($printerTimeLine->roll_end['datetime']);
        
        
        
        echo '<div class="zoom " style="zoom:' . $zoom . '%;width:'.($total_pixels+20000).'px;height: 155px;;border-color: black;border-style: double;border-width: 1px;">';
        //Inicializaciones
        $init_timeline = new DateTimeG($this->start_date_time_print->format('Y-m-d H:i:s'));
        //$now = new DateTimeImmutable( $this->start_date_time_print->format('Y-m-d H:i:s'));
        
        if ((intval($init_timeline->format('H')) < intval(conf_workday($init_timeline)['from']['hour']))) {
            $init_timeline->modify('-1 days');
        }
        
        
        
        $is_first_iteration = true;
        
        $gap_start = new DateTimeG($this->start_date_time_print->format('Y-m-d H:i:s'));
        //$gap_start = new DateTimeG( $this->start_date_time_print->format('Y-m-d H:i:s'));
        //$gap_end = new DateTimeG();
        $scale = 1;
        //$hours =0;
        $slot_pixels = 0;
        
        $height_gap = "154px";
        $height_slot = "154px";
        
        $big_slot_size = 240;
        $medium_slot_size = 170;
        $small_slot_size = 150;
        $smallest_slot_size = 80;
        $minimum_slot_size = 26;
        //echo '<a name="arriba">'.$printerTimeLine->code_printer.'</a>';
        $color_disabled ="";
        if ($printerTimeLine->available != 1) {
            $color_disabled = 'color:darkgrey;';
        }
        echo '<div style="'.$color_disabled.'background:gainsboro;height:' . 5 . ';width:77px;float:left;background: withe;border: solid;border-width: 0px;font-size: x-large;padding-top: 5px;padding-left: 10px;">';
        echo $printerTimeLine->code_printer;
        echo '</div>';
        
        //$arrObject = $this;
        //$arrayIterator = $arrObject->getIterator();
        //foreach ($arrayIterator->current() as $slot){
        $is_first_stlot = true;
        
        if (isset($printerTimeLine->last_slot_out_range->start) && ($printerTimeLine->last_slot_out_range->start !=  'NULL')) {
            $tooltip_text ="";
            $tooltip_text .= $printerTimeLine->last_slot_out_range->product_name .' - '.$printerTimeLine->last_slot_out_range->final_product. '&#013;';
            $tooltip_text .= 'Order: ' . $printerTimeLine->last_slot_out_range->order . ' - ';
            $tooltip_text .= 'File:&nbsp; ' . $printerTimeLine->last_slot_out_range->file_part_name . '&#013;';
            $tooltip_text .= 'Part: ' . $printerTimeLine->last_slot_out_range->part . ' - ';
            $tooltip_text .=  $printerTimeLine->last_slot_out_range->file_name . '&#013;';
            
            $start_last = new DateTimeG($printerTimeLine->last_slot_out_range->start);
            $end_last = new DateTimeG($printerTimeLine->last_slot_out_range->end);
            
            
            $slot_interval_last = $start_last->diff($end_last);
            $slot_total_hours_real = $slot_interval_last->format('%d') * 24 + $slot_interval_last->format('%h');
            
            $tooltip_text .= 'Days: ' . $slot_interval_last->format('%d') . ' - Time: ' . $slot_total_hours_real. ':' . $slot_interval_last->format('%i') . '&#013;';
            $tooltip_text .= $start_last->format('d/m/Y H:i') . '&#013;';
            $tooltip_text .=  $end_last->format('d/m/Y H:i') . '&#013; ';
        }
        
        
        if (($printerTimeLine->count() == 0) &&  (isset ( $printerTimeLine->last_slot_out_range->part)) ){
            echo '<div   title="'.  $tooltip_text.'" style ="position: absolute;margin-top: 53px;width: 68px;margin-left: 14px;" class="tooltip-text-aux">';
            echo    $printerTimeLine->last_slot_out_range->order.'<br>'.
                    $printerTimeLine->last_slot_out_range->file_part_name.'<br>'.
                    $printerTimeLine->last_slot_out_range->part.'<br>';
            
                    $start_ini= new DateTimeG($printerTimeLine->last_slot_out_range->start);
                    $end_ini= new DateTimeG($printerTimeLine->last_slot_out_range->end);
                    $slot_interval = $start_ini->diff($end_ini);
                    $slot_total_hours = $slot_interval->format('%d') * 24 + $slot_interval->format('%h');
            echo    $slot_total_hours . ':' . $slot_interval->format('%i') . '</br>';
            echo    $end_ini->format('H:i') . '</br>';
                
                    $a_start_last ='';
                    $a_end_last='';
                    $a_start = '';
                    $a_end ='';
                    $img_start ='';
                if ($printerTimeLine->last_slot_out_range->state == 9){
                    $a_start_last='<a id="button-time-line-cancel-{$printerTimeLine->last_slot_out_range->part}}" class="button-time-line"
                                    href= "/admin/parts/cancel/'.$this->start_date_time_print->format('Y-m-d H:i:s').'/'.$printerTimeLine->last_slot_out_range->part.'/'.$printerTimeLine->printer_id.'/">';
                    $img_start_last=     '<img  style="width:50px" src="'.'/assets/admin/images/cancel.png'.'">';
                    $a_end_last='</a>';
                    
                    
                    
                    $a_start='<a id="button-time-line-play-{{$printerTimeLine->last_slot_out_range->part}}" class="button-time-line play-button"
                                    data-start ="'.$this->start_date_time_print->format('Y-m-d H:i:s').'"
                                    data-printer = "'.$printerTimeLine->printer_id.'"
                                    data-part = "'.$printerTimeLine->last_slot_out_range->part.'"
                                    data-state = "10"
                                    href= "/admin/parts/play/'.$this->start_date_time_print->format('Y-m-d H:i:s').'/'.$printerTimeLine->printer_id.'/'.$printerTimeLine->last_slot_out_range->part.'/10">';
                    
                    
                    $img_start=     '<img  style="width:50px" src="'.'/assets/admin/images/finished.png'.'">';
                    $a_end="</a>";
                    
                }else{
                    $img_start_last=     '<img  style="width:50px" src="'.'/assets/admin/images/printed.png'.'">';
                }
                
                
                echo    $a_start_last.$img_start_last.$a_end_last.$a_start.$img_start.$a_end;
                
                echo '</div>';
        }

        foreach ($printerTimeLine as $slot) {
            
            $end = new DateTimeG($slot->end);
            
            $break = false;
            //Jose 19/05/2022 if ($end < $this->start_date_time){
            $init_timeline_workday = new DateTimeG($init_timeline->format('Y-m-d H:i:s'));
            $init_timeline_workday->setTime(conf_workday($init_timeline_workday)['from']['hour'], 0);
            if ($end < $init_timeline_workday){
                continue;
            }
            
            
            //$end = new DateTime($slot->end);
            $start= new DateTimeG($slot->start);
            $start_real= new DateTimeG($slot->start);
            
            

            
            //Gaps
            if ($is_first_iteration) {
                //$gap_start = $monday;
                get_object_vars($init_timeline);
                $gap_start = new DateTimeG($init_timeline->format('Y-m-d H:i:s'));
                //$gap_start = new DateTime($init_timeline->format('Y-m-d H:i:s'));
                //$gap_start = new DateTime($init_timeline->date);
                //$gap_start = new DateTime($start->date);
                //Funciona a las 6 de la mañana
                //&& ($init_timeline < $start)
                $init_timeline_0_0 = new DateTimeG($init_timeline->format('Y-m-d H:i:s'));
                //$init_timeline_0_0 = new DateTime($init_timeline->format('Y-m-d H:i:s'));
                $init_timeline_0_0->setTime(0, 0);
                $init_timeline_0_59 = new DateTimeG($init_timeline->format('Y-m-d H:i:s'));
                //$init_timeline_0_59 = new DateTime($init_timeline->format('Y-m-d H:i:s'));
                $init_timeline_0_59->setTime(0, 59);
                $init_timeline_13_0 = new DateTimeG($init_timeline->format('Y-m-d H:i:s'));
                //$init_timeline_13_0 = new DateTime($init_timeline->format('Y-m-d H:i:s'));
                $init_timeline_13_0->setTime(13, 0);
                
                $init_timeline_from_hour  = new DateTimeG($init_timeline->format('Y-m-d H:i:s'));
                //$init_timeline_from_hour = new DateTime($init_timeline->format('Y-m-d H:i:s'));
                $init_timeline->setTime(conf_workday($init_timeline)['from']['hour'], 0);
                //if (conf_workday()['from_hour'] >= 30
                
                //Segón la hora del días y si es lunes hay que realizar ajustes TODO analizar
                //El resto de díass los ajustes se debe a la hora de inicio de jornada
                if (($init_timeline->format('N') == 1) && ($init_timeline_0_0 < $init_timeline) && ($init_timeline_0_59 > $init_timeline)) {
                    //Lunes entre la 00:00 y las 00:59
                    $gap_start->modify('-1 days');
                    $gap_start->setTime(conf_workday($gap_start)['from']['hour'], 0);
                } elseif (($init_timeline->format('N') == 1) && ($init_timeline_13_0 < $init_timeline)) {
                    //Lunes entre las 00:59 y las 13:00
                    $gap_start->setTime(conf_workday($gap_start)['from']['hour'], 0);
                } elseif (($init_timeline->format('N') == 1) && (conf_workday($init_timeline)['from']['hour'] > $init_timeline->format('h'))) {
                    //Lunes y la horar catual es menor que la de incio de jornada
                   //05-04-2022 $gap_start->modify('-1 days');
                    $gap_start->setTime(conf_workday($gap_start)['from']['hour'], 0);
                } elseif ($init_timeline->format('N') == 1) {
                    //Otros casos del lunes: se puede quitar else if de los anteriores.Pero depues hay que probar todos los casos.
                    //$gap_start->modify('-1 days');
                    $gap_start->setTime(conf_workday($gap_start)['from']['hour'], 0);
                } elseif (($init_timeline->format('N') > 1) && ($init_timeline_from_hour <= $init_timeline)) {
                    //No es Lunes y hora de la jornada es menor que la hora actual
                    $gap_start->setTime(conf_workday($gap_start)['from']['hour'], 0);
                } elseif ($init_timeline->format('N') > 1) {
                    //Otros casos que no es Lunes
                    //$gap_start->modify('-' . '1' . ' days');
                    $gap_start->setTime(conf_workday($gap_start)['from']['hour'], 0);
                }
                
                //$gap_end = $start;
                //Mostrar el elemnto que se acaba de imprimir o se está imprimiendo
                

                
                if (
                    ($slot->state == 9)
                    &&
                    (isset ( $printerTimeLine->last_slot_out_range->start)) 
                    && 
                    ($printerTimeLine->last_slot_out_range->start !=  'NULL')
                    && 
                    (isset ( $printerTimeLine->last_slot_out_range->part)) 
                    && 
                    ($slot ->part != $printerTimeLine->last_slot_out_range->part)
                   )
                {
                    //Pintar last slot como un slot normal y no pintar el div en la cabecera de la impresora
                    if (
                        (($this->start_date_time_print < $start_last) && ($init_timeline < $this->start_date_time_print))
                        ||
                        //La fecha incio del last slot es menor que la marca de la fecha actual. 
                        //Y
                        ////La fecha fin del last slot es mayor que la marca de la fecha actual. 
                        (($start_last < $this->start_date_time_print) && ( $this->start_date_time_print < $end_last))
                        ||
                        //La fecha inicio de la cinta metrica es menor que el inicio del last slot 
                        //Y
                        //la fecha fin del last slot es menor que la fecha que la marca de la fecha actual
                        (($init_timeline < $start_last) && ( $end_last < $this->start_date_time_print))
                        ||
                        (($this->start_date_time_print > $end_last) && ($init_timeline < $this->start_date_time_print))
                        
                       )
                    
                    {
                        /*
                        $this->printGapSlot(
                            $gap_start,$start_last,$end_last,$start_real,
                            $slot,
                            $tooltip_text,
                            $printerTimeLine,
                            $is_first_iteration,$is_first_stlot,$scale,
                            $height_gap,$height_slot,
                            $slot_total_hours_real,
                            $smallest_slot_size,$small_slot_size,$medium_slot_size,$big_slot_size,$minimum_slot_size);
                         */
                        echo '<div   title="'.  $tooltip_text.'" style ="position: absolute;margin-top: 42px;width: 68px;margin-left: 14px;" class="tooltip-text-aux">';
                        echo    $printerTimeLine->last_slot_out_range->order.'<br>'.
                                $printerTimeLine->last_slot_out_range->file_part_name.'<br>'.
                                $printerTimeLine->last_slot_out_range->part;
                            $a_start_last ='';
                            $a_end_last='';
                            if ($printerTimeLine->last_slot_out_range->state == 9){
                                $a_start_last='<a id="button-time-line-cancel-{{$printerTimeLine->last_slot_out_range->part}}" class="button-time-line"
                                    href= "/admin/parts/cancel/'.$this->start_date_time_print->format('Y-m-d H:i:s').'/'.$printerTimeLine->last_slot_out_range->part.'/'.$printerTimeLine->printer_id.'/">';
                                $img_start_last=     '<img  style="width:50px" src="'.'/assets/admin/images/cancel.png'.'">';
                                $a_end_last='</a>';
                            }else{
                                $img_start_last=     '<img  style="width:50px" src="'.'/assets/admin/images/printed.png'.'">';
                            }
                            
                            
                        echo    $a_start_last.$img_start_last.$a_end_last;
                        
                        echo '</div>';
                }else{
                    
                        /*
                        $this->printGapSlot(
                            $gap_start,$start_last,$end_last,$start_real,
                            $slot,
                            $tooltip_text,
                            $printerTimeLine,
                            $is_first_iteration,$is_first_stlot,$scale,
                            $height_gap,$height_slot,
                            $slot_total_hours_real,
                            $smallest_slot_size,$small_slot_size,$medium_slot_size,$big_slot_size,$minimum_slot_size);
                        */
                        echo '<div   title="'.  $tooltip_text.'" style ="position: absolute;margin-top: 42px;width: 68px;margin-left: 14px;"> class="tooltip-text-aux"';
                        echo    $printerTimeLine->last_slot_out_range->order.'<br>'.
                            $printerTimeLine->last_slot_out_range->file_part_name.'<br>'.
                            $printerTimeLine->last_slot_out_range->part;
                            $a_start_last ='';
                            $a_end_last='';
                            if ($printerTimeLine->last_slot_out_range->state == 9){
                                $a_start_last='<a id="button-time-line-cancel-{{$printerTimeLine->last_slot_out_range->part}}" class="button-time-line"
                                            href= "/admin/parts/cancel/'.$this->start_date_time_print->format('Y-m-d H:i:s').'/'.$printerTimeLine->last_slot_out_range->part.'/'.$printerTimeLine->printer_id.'/">';
                                $img_start_last=     ''.'<img  style="width:50px" src="'.'/assets/admin/images/cancel.png'.'">';
                                $a_end_last='</a>';
                            }else{
                                $img_start_last=     '<img  style="width:50px" src="'.'/assets/admin/images/printed.png'.'">';
                            }
                        echo    $a_start_last.$img_start_last.$a_end_last;
                        
                        echo '</div>';
                    }
                }elseif
                       (
                        ($slot->state == 9)
                        &&
                        (isset ( $printerTimeLine->last_slot_out_range->part))
                        &&
                        ($slot ->part != $printerTimeLine->last_slot_out_range->part)
                       )
                {
                    //de momento se haría lo mismo que antes: poner last slot en la cabecera de la impresorna y su tipsy
                    if (isset ( $printerTimeLine->last_slot_out_range->start) && ($printerTimeLine->last_slot_out_range->start !=  'NULL')){
                        echo '<div   title="'.  $tooltip_text.'" style ="position: absolute;margin-top: 42px;width: 68px;margin-left: 14px;" class="tooltip-text-aux">';
                        echo    $printerTimeLine->last_slot_out_range->order.'<br>'.
                                $printerTimeLine->last_slot_out_range->file_part_name.'<br>'.
                                $printerTimeLine->last_slot_out_range->part;
                                
                                
                                $a_c_start_last='';
                                
                                $img_c_start_last=     'ccc'.'<img  style="width:50px" src="'.'/assets/admin/images/printed.png'.'">';
                                $a_c_end_last='';
                        echo    $a_c_start_last.$img_c_start_last.$a_c_end_last;
                            
                        echo '</div>';
                    }
                }
            }
            //if ($is_first_stlot) {
                if ($start < $this->start_date_time) {
                    //get_object_vars($start_datetime);
                    if  ($start <$gap_start){
                        $start = $gap_start;
                    }
                }
            //}
            
            $gap_interval = $gap_start->diff($start);
            $gap_total_minutes = $gap_interval->format('%d') * 24 * 60 + $gap_interval->format('%h') * 60 + $gap_interval->format('%i');
            $gap_pixels = intdiv($gap_total_minutes, $scale);
            
            if ((intval($init_timeline->format('H')) < intval(conf_workday($init_timeline)['from']['hour']))) {}
            //Dibujo el hueco
            
            $background_gap = "grey";
            if ($is_first_iteration){
                $background_gap = "rgb(237,237,237)";
                //$is_first_iteration = false;
            }
            echo '<div class="gap" style="height:' . $height_gap . ';width:' . $gap_pixels . 'px;float:left;background:'.$background_gap.';border: solid;border-width: 0px;">';
            echo '</div>';
            
            //Slots
            get_object_vars($start);
            get_object_vars($end);
            $slot_interval = $start->diff($end);
            $slot_interval_real = $start_real->diff($end);
            
            
            $slot_total_minutes = $slot_interval->format('%d') * 24 * 60 + $slot_interval->format('%h') * 60 + $slot_interval->format('%i');
            $slot_total_minutes_real = $slot_interval_real->format('%d') * 24 * 60 + $slot_interval_real->format('%h') * 60 + $slot_interval_real->format('%i');
            //Apaño por bug en la función diff para la fechas: $end=2022-03-26 11:00:00, $start=2022-03-27 09:00:00
            
            //30-03$end_aux = createDateTime($start->format('Y-m-d H:i:s'));
            $end_aux = $start->createDateTime();
            
            $end_aux ->modify("+ ".$slot_total_minutes."minutes");
            if ($end_aux != $end){
                $slot_interval_aux =  date_diff($end, $end_aux);
                $slot_total_minutes_aux = $slot_interval_aux->format('%d') * 24 * 60 + $slot_interval_aux->format('%h') * 60 + $slot_interval_aux->format('%i');
                $slot_total_minutes +=$slot_total_minutes_aux;
            }
            
            $slot_pixels = intdiv($slot_total_minutes, $scale);
            $padding_left = "padding-left: 20px;";
            //$slot_total_hours = $slot_interval->format('%d') * 24 + $slot_interval->format('%h');
            $slot_total_hours_real = $slot_interval_real->format('%d') * 24 + $slot_interval_real->format('%h');
            //background-image: repeating-linear-gradient(45deg, #444cf7 0, #444cf7 1px, #e5e5f7 0, #e5e5f7 1%);
            get_object_vars($this->start_date_time_print);
            
            $padding_top = 10;
            if  ($slot_pixels < $smallest_slot_size){
                $style_slot = 'padding-left: 0px; margin-top: 7px;';
                $padding_top = 0;
            }elseif  ($slot_pixels < $small_slot_size){
                $style_slot = 'padding-left: 5px; margin-top: 4px;';
                $padding_top = 0;
            }elseif  ($slot_pixels < $medium_slot_size){
                $style_slot = 'padding-left: 6px; margin-top: -7px;';
            }else {
                $style_slot = $padding_left. 'margin-top: -7px;';
            }
            echo '<a name= "part_link_'.$slot->part.'">';
            echo '<div style="height:' . $height_slot . ';padding-top: '.$padding_top.'px;font-size: larger;font-family: sans-serif;width:' . $slot_pixels . 'px;float:left;
                              background-color: rgb(' . conf_parts_colors()[$slot->file_part_name][0][0] . ',' 
                                                      . conf_parts_colors()[$slot->file_part_name][0][1] . ',' 
                                                      . conf_parts_colors()[$slot->file_part_name][0][2] . ');
                  ';
            
            if (($slot->state == 9) || ($slot->state == 10)){
                echo 'color: rgb(255,255,255);';
            } else {
                echo 'color: rgb(' . conf_parts_colors()[$slot->file_part_name][1][0] . ',' . conf_parts_colors()[$slot->file_part_name][1][1] . ',' . conf_parts_colors()[$slot->file_part_name][1][2] . ');';
            }
            echo 'border: solid;border-width: 0px;">';
            

            echo '<div style="height: 141px;' . $style_slot;
            if (($slot->state == 9) ||($slot->state == 10)){
                echo 'background: repeating-linear-gradient(45deg,
                        rgb(' . conf_parts_colors()[$slot->file_part_name][0][0] . ',' . conf_parts_colors()[$slot->file_part_name][0][1] . ',' . conf_parts_colors()[$slot->file_part_name][0][2] . ') 15px,
                        #000000 22px,
                        rgb(' . conf_parts_colors()[$slot->file_part_name][0][0] . ',' . conf_parts_colors()[$slot->file_part_name][0][1] . ',' . conf_parts_colors()[$slot->file_part_name][0][2] . ') 35px
                        ) '; //0 / 10px 10px;';
            }
            echo '">';
            
            
            $tooltip_text = "";
            
            $tooltip_text .= $slot->product_name .' - '.$slot->final_product. '&#013;';
            $tooltip_text .= 'Order: ' . $slot->order . ' - ';
            $tooltip_text .= 'File:&nbsp; ' . $slot->file_part_name . '&#013;';
            $tooltip_text .= 'Part: ' . $slot->part . ' - ';
            $tooltip_text .=  $slot->file_name . '&#013;';
            $tooltip_text .= 'Days: ' . $slot_interval_real->format('%d') . ' - Time: ' . $slot_total_hours_real. ':' . $slot_interval_real->format('%i') . '&#013;';
            $tooltip_text .= $start_real->format('d/m/Y H:i') . '&#013;';
            $tooltip_text .=  $end->format('d/m/Y H:i') . '&#013;';
            
            if (($slot->state == 9) ||($slot->state == 10)){
                $class = '';
                if (isset($next_part['slot']) && $next_part['slot']->part == $slot->part){
                    $class = 'class="blink_next_part tooltip-text-aux" ';
                }
                
                /*
                echo '<a '.$class.' title="'.  $tooltip_text.'" style ="color: yellow;text-decoration: none;" data-html="true" class="submit tooltip-text-aux" 
                                    href="reassign_part.php?
                                    part=' .$slot->part . '
                                    &start-date-time=' .  $this->start_date_time_print->format('Y-m-d H:i:s') . '
                                    " target="_self" >';
                */
                echo '<a '.$class.' title="'.  $tooltip_text.'" style ="color: yellow;text-decoration: none;" data-html="true" class="submit tooltip-text-aux">';
            }else{
                echo '<a title="'.$tooltip_text.'" class="tooltip-text-aux">';
            }
            
            
            $text = "";
            if  ($slot_pixels > $big_slot_size){
                $text .= '<B>'.$slot->product_name .' - '.$slot->final_product. '</B><br>';
                $text .= 'Order: ' . $slot->order . ' - ';
                $text .= 'File:&nbsp; ' . $slot->file_part_name . '</br>';
                $text .= 'Part: ' . $slot->part . ' -';
                $text .=  $slot->file_name . '</br>';
                $text .= 'Days: ' . $slot_interval_real->format('%d') . ' - Time: ' . $slot_total_hours_real. ':' . $slot_interval_real->format('%i') . '</br>';
                $text .= $start_real->format('d/m/Y H:i') . '</br>';
                $text .=  $end->format('d/m/Y H:i') . '</br>';
            }elseif  ($slot_pixels > $medium_slot_size){
                $text .= '<B>'.$slot->product_name .' - '.$slot->final_product. '</B><br>';
                $text .= 'O: ' . $slot->order . '';
                $text .= 'F:&nbsp; '. $slot->file_part_name . '</br>';
                $text .= 'P: ' . $slot->part . ' </br> ';
                $text .=  $slot->file_name . '</br>';
                $text .= 'D: ' . $slot_interval_real->format('%d') . ' - Time: ' . $slot_total_hours_real. ':' . $slot_interval_real->format('%i') . '</br>';
                $text .= $start_real->format('d/m/Y H:i') . '</br>';
                $text .=  $end->format('d/m/Y H:i') . '</br>';
            }elseif ($slot_pixels > $small_slot_size){  
                $text .= '<B>'.$slot->final_product. '</B> - ';
                $text .= 'O: ' . $slot->order . '</br>';
                $text .= 'F:&nbsp; ' . $slot->file_part_name . '</br>';
                $text .= 'P: ' . $slot->part . '</br> ';
                $text .=  $slot->file_name . '</br>';
                $text .= 'D: ' . $slot_interval_real->format('%d') . ' T: ' . $slot_total_hours_real. ':' . $slot_interval_real->format('%i') . '</br>';
                $text .= $start_real->format('d/m/Y H:i') . '</br>';
                $text .=  $end->format('d/m/Y H:i') . '';
            }elseif ($slot_pixels > $smallest_slot_size){
                $text .= 'O: ' . $slot->order . '</br>';
                $text .= 'F: ' . $slot->file_part_name . '</br>';
                $text .= 'P: ' . $slot->part . '</br>';
                $text .= 'T: ' . $slot_total_hours_real . ':' . $slot_interval_real->format('%i') . '</br>';
                $text .= $start_real->format('H:i') . ' - ';
                $text .= $end->format('H:i') . '</br></br>'.'<br><br>';
            }elseif ($slot_pixels > $minimum_slot_size){
                $text .= 'O: ' . $slot->order . '</br>';
                $text .= 'F: ' . $slot->file_part_name . '</br>';
                $text .= 'P: ' . $slot->part . '</br>';
                $text .= $slot_interval_real->format('%h') . ':'.$slot_interval_real->format('%i') ;
                if ((($is_first_stlot)||($slot->initiated == 1)) && (($slot->state == 9)||($slot->state == 10))){
                    $text .= '<br><br>';
                }else{
                    $text .= '<br><br><br>';
                }
            }else{
                $text .= '</br></br></br></br></br></br></br>';
            }
            echo $text;
            echo '</a>';
            
            echo '</div>';
            if ((($is_first_stlot)||($slot->initiated == 1)) && (($slot->state == 9)||($slot->state == 10))){
                $name="/assets/admin/images/printed.png";
                $a_start="";
                $a_end="";
                if ($slot->initiated == 0){
                    $name="/assets/admin/images/play.png";
                    /*
                    $a_start='<a class="button-time-line" 
                                    href= "index.php
                                    ?start-date-time='.$this->start_date_time_print->format('Y-m-d H:i:s').'
                                    &printer_id='.$printerTimeLine->printer_id.'
                                    &state_id=9
                                    &button=play
                                    &start='.$start->format('Y-m-d H:i:s').'
                                    &end='.$end->format('Y-m-d H:i:s').'
                                    &how=void
                                    &part_id='.$slot->part.'">';
                    $a_end="</a>";
                    */
                    
                    $a_start='<a id="button-time-line-play-'.$slot->part.'" class="button-time-line play-button"
                                    data-start ="'.$this->start_date_time_print->format('Y-m-d H:i:s').'"
                                    data-printer = "'.$printerTimeLine->printer_id.'"
                                    data-part = "'.$slot->part.'"
                                    data-state = "9"
                                    href= "/admin/parts/play/'.$this->start_date_time_print->format('Y-m-d H:i:s').'/'.$printerTimeLine->printer_id.'/'.$slot->part.'/9">';
                    
                    $a_end="</a>";
                    
                    
                    
                }else{
                    /*
                    $a_c_start='<a class="button-time-line" 
                                    href= "index.php
                                    ?start_date_time='.$this->start_date_time_print->format('Y-m-d H:i:s').'
                                    &printer_id='.$printerTimeLine->printer_id.'
                                    &state_id=11
                                    &button=cancel
                                    &start='.$start->format('Y-m-d H:i:s').'
                                    &end='.$end->format('Y-m-d H:i:s').'
                                    &how=void
                                    &part_id='.$slot->part.'">';
                    */
                    $a_c_start='<a id="button-time-line-cancel-'.$slot->part.'" class="button-time-line"
                                    href= "/admin/parts/cancel/'.$this->start_date_time_print->format('Y-m-d H:i:s').'/'.$slot->part.'/'.$printerTimeLine->printer_id.'/">';
                    
                    $a_c_end="</a>";
                }
                $name_c = "/assets/admin/images/cancel.png";
                
                if ($slot_pixels < 159){
                    $left = 0;
                    $left_c = 0;
                }
                $left = 150;
                $left_c= 208;
                $bottom=41;
                $bottom_c=41;

                if (($slot_pixels<=$minimum_slot_size)){
                    $bottom = 110;
                    $bottom_c = 107;
                    $left = -46;
                    $left_c = -46;
                }
                if (($slot_pixels < $smallest_slot_size)&&($slot_pixels>=$minimum_slot_size)){
                    $bottom = 90;
                    //$bottom_c = 87;
                    $bottom_c = 111;
                    $left = -28;
                    //$left_c = -30;
                    $left_c = -8;
                }elseif (($slot_pixels < $small_slot_size)&&($slot_pixels>=$smallest_slot_size)){
                    
                    $left = 5;
                    $left_c= 67;
                    $bottom=43;
                    $bottom_c=43;
                    /*
                    $bottom=54;
                    $bottom_c=103;
                    $left = -20;
                    $left_c = 43;
                    */
                }elseif (($slot_pixels < $big_slot_size)&&($slot_pixels>=$small_slot_size)){
                    $bottom=42;
                    $bottom_c=42;
                    $left = 108;
                    $left_c = 108;
                    //$bottom=111;
                    //$bottom_c=106;
                    //$left = -20;
                    //$left_c = -20;
                }

                if ($slot->initiated == 1){
                    if ($slot->state == 9){
                        $a_finish_start='';
                        $img_finish= '';
                        $a_finsish_end ='';
                        if (($printerTimeLine->count() == 1)&&($this->start_date_time_print > $end)){
                            $a_finish_start='<a id="button-time-line-play-'.$slot->part.'" class="button-time-line play-button"
                                    data-start ="'.$this->start_date_time_print->format('Y-m-d H:i:s').'"
                                    data-printer = "'.$printerTimeLine->printer_id.'"
                                    data-part = "'.$slot->part.'"
                                    data-state = "10"
                                    href= "/admin/parts/play/'.$this->start_date_time_print->format('Y-m-d H:i:s').'/'.$printerTimeLine->printer_id.'/'.$slot->part.'/10">';
                            $img_finish= '<img  style="width:50px" src="/assets/admin/images/finished.png">';
                            $a_finsish_end="</a>";
                            
                        }
                        
                        echo '<div title="'.$tooltip_text.'" style ="position:relative;bottom:'.$bottom_c.'px;left:'.$left_c.'px" class="tooltip-text-aux">'.
                                    $a_c_start.'
                                        <img  style="width:50px" src="'.$name_c.'">'.
                                    $a_c_end;
                        echo   $a_finish_start.$img_finish.$a_finsish_end;
                        echo '</div>';
                    }else{
                        echo '<div title="'.$tooltip_text.'" style ="position:relative;bottom:'.$bottom_c.'px;left:'.$left_c.'px" class="tooltip-text-aux">'.
                             '      <img  style="width:50px" src="'.$name.'">'.
                             '</div>';
                             
                    }
                }else{
                    echo '<div title="'.$tooltip_text.'" style ="position:relative;bottom:'.$bottom.'px;left:'.$left.'px" class="tooltip-text-aux">'.
                        $a_start.'
                            <img  style="width:50px" src="'.$name.'" class="tooltip-text-aux">'.
                            $a_end.'
                          </div>';
                }
            }
            if ($slot->initiated == 0){
                $is_first_stlot = false;
            }
            echo '</div></a>';
            

            $total_pixels = $total_pixels + $gap_pixels + $slot_pixels;
            $gap_start = $end;
            $is_first_iteration = false;
        }
        echo "</div>";
        
        //echo "Total Pixels:".$total_pixels;
        return $total_pixels;
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////// Dibujo la regla ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function printRule(int $zoom, int &$total_pixels, PrinterTimeline $printerTimeLine)
    {
        //Div Principal
        
        echo '<div class="zoom" style="  zoom:' . $zoom . '%;
                            float:none;
                            width:'.($total_pixels+2000).'px;
                            height:50px;
                            border-color: blue;
                            border-top-style: dotted;
                            border-width:1px;
                            margin-top: 1px;
                            margin-left: 28px;"
                            >';
        
        //19320 - 19280 = 40px
        //$week = 0;
        
        //$total_pixels = $printerTimeLine -> getTotalMinutes($this->start_date_time_print);
        
        $half_hours_count = 0;
        $hours_count = 0;
        //$days_aux = intdiv((($half_hours_count+conf_workday()['from']['hour'])-($half_hours_count/2)),24);
        
        $init_timeline = new DateTimeG($this->start_date_time_print->format('Y-m-d H:i:s'));
        //$init_timeline = new DateTimeG( $this->start_date_time_print->format('Y-m-d H:i:s'));
        get_object_vars($init_timeline);
        $init_timeline_real = new DateTimeG($init_timeline->format('Y-m-d H:i:s'));
        //$init_timeline_real = new DateTimeG($init_timeline->date);
        //if ((intval($init_timeline->format('H'))< intval(conf_workday()['from']['hour']))&&($init_timeline->format('D') != 'Mon')){
        //Si la hora actual es menor que la de inicio de jornada mostrará tambien el día anterior.Antes además no tenía que ser el lunes
        //Esto no funciona debido al cambio de start_date_time_print por eso lo quito
        if ((intval($init_timeline->format('H')) < intval(conf_workday($init_timeline)['from']['hour']))) {
            $init_timeline->modify('-1 days');
        }
        $init_timeline_hour = $init_timeline->format('H');
        $current_datetime = new DateTimeG($init_timeline->format('Y-m-d H:i:s'));
        //$current_datetime = $init_timeline;
        $current_datetime->setTime(intval(conf_workday($current_datetime)['from']['hour']), 0);
        $current_week = $current_datetime->format('W');
        $current_month = $current_datetime->format('n');
        
        $color = "black";
        $change_font_style = true;
        
        //$relative_day = 1;
        echo '<div style="  height:24px;
                            width:50px;
                            float:left;
                            background: withe;
                            border: solid;
                            border-width: 0px;
                            font-size: larger;
                            padding-top: 2px;">';
        //echo $init_timeline->format('H').':'.$init_timeline->format('i');
        echo '</div>';
        
        for ($i = 0; $i <= $total_pixels; $i = $i + 30) {
            //Para el cálculo de las noches
            $relative_days = (int)((($half_hours_count + conf_workday($init_timeline_real)['from']['hour']) - ($half_hours_count / 2))/ 24);
            $day_hour = (($half_hours_count + conf_workday($init_timeline_real)['from']['hour']) - ($half_hours_count / 2)) - ($relative_days * 24);
            //Destacar el días y la hora actual, cambiando el estilo de la fuente

            
            //Destacar las noches y los fines de semana, con  color rojo. No tiene en cuenta minutos
            if (($day_hour > conf_workday($init_timeline_real)['from']['hour']) && ($day_hour < conf_workday($init_timeline_real)['to']['hour'])
                /*
                 &&
                 ($current_datetime->format('D') !='Sat')
                 &&
                 ($current_datetime->format('D') !='Sun')
                 */
                )
            {
                $color = "black";
            } else {
                $color = "red";
            }
            
            if ((intval($day_hour) == intval($init_timeline_hour)) && ($change_font_style)) {
                $font_style = 'font-family: sans-serif!important;font-size: 20px!important;';
                $change_font_style = false;
                $color = "#449b23";
            } else {
                $font_style = '';
            }
            
            //Poner el bloque de hora, días y mes.
            echo '<div      style=" width:30px;position: relative;float:left;background: white;border: solid;
                                    border-width: 0px;font-family: sans-serif;font-size: medium;
                                    color:' . $color . ';' . $font_style . '">';
            //echo $hours_count;
            echo '  <div style="' . $font_style . ';">|</div>';
            //Inicio
            if ($half_hours_count == 1) {
                /*
                 echo '<div style="height:160px;"></div>';
                 echo '<div style="width: 118px;padding-bottom: 4px;">Last Week: ' . $current_datetime->format('W') . '</div>';
                 echo '<div style="float: left;display: inline-block;border: groove;font-size: x-large;border-block-style: inherit;width: 803px;height: 56px;padding-top: 5px;padding-left: 10px;"' . $font_style . '">';
                 echo '<div style="float: left;display: inline-block;border: groove;font-size: x-large;border-block-style: inherit;width: 357px;height: 36px;padding-top: 5px;padding-left: 10px;"' . $font_style . '">' . $init_timeline_real->format('l') . ', ' . $init_timeline_real->format('j') . ' ' . $init_timeline_real->format('F') . ' ' . $init_timeline_real->format('Y') . '
                 </div>';
                 echo '<div style="float: left;display: inline-block;border: groove;font-size: x-large;border-block-style: inherit;width: 76px;height: 36px;padding-top: 5px;padding-left: 10px;"' . $font_style . '">' . $init_timeline_real->format('H') . ':' . $init_timeline_real->format('i') . '
                 </div>';
                 echo '<div style="float: left;display: inline-block;border: groove;font-size: x-large;border-block-style: inherit;width: 300px;height: 36px;padding-top: 5px;padding-left: 10px;"' . $font_style . '">Working day: ' . conf_workday()['from']['hour'] . ':' . conf_workday()['from']['minute'] . ' - ' . conf_workday()['to']['hour'] . ':' . conf_workday()['to']['minute'] . '
                 </div>';
                 echo '</div>';
                 */
            } elseif ((($half_hours_count) % 2) == 0) {
                //Es un número par: Pongo la hora, las medias horas no se indican.
                echo $day_hour . 'h';
                $hours_count ++;
                
                //días
                echo '  <div style="width:30px;' . $font_style . '">' . $current_datetime->format('D') . '</div>';
                //Pongo el días de la semana en texto
                echo '<div style="font-size: small;' . $font_style . '">' . $current_datetime->format('j') . ' ' . $current_datetime->format('M') . '</div>';
                /*
                if ($current_week != $current_datetime->format('W')) {
                    //Semana
                    echo '  <div style="font-size: x-large;position:absolute;top:170px;' . $font_style . '">Week:' . $current_datetime->format('W') . '</div>';
                    $current_week = $current_datetime->format('W');
                }
                if ($current_month != $current_datetime->format('n')) {
                    //Mes
                    echo '  <div style="font-size: xx-large;position:absolute;top:110px;' . $font_style . '">' . $current_datetime->format('F') . '</div>';
                    $current_month = $current_datetime->format('n');
                }
                */
                //Aumento en una hora, el datetime que se va a pintar
                $current_datetime->modify('+1 hours');
            }
            ;
            
            echo '</div>';
            
            $half_hours_count ++;
        }
        ; //END FOR
        
        echo '</div>'; //Fin Div Principal
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// printAllFilamentCalculations ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function printAllFilamentCalculations(int $zoom, int &$total_pixels, PrinterTimeline $printerTimeLine)
    {
        //Div Principal
        $total_pixels = $printerTimeLine -> getTotalMinutes($this->start_date_time_print);
        echo '<div class="zoom" style="  zoom:' . $zoom . '%;
                            float:none;
                            width:'.($total_pixels+20000).'px;
                            height:125px;
                            border-color: blue;
                            //border-top-style: dotted;
                            border-width:1px";
                            margin-top: 10px;>';
        
        
        $with = 0;
        
        //Calculo el DateTime en que se empieza a dibujar la regla (marcas de tiempo)
        $start_workday = new DateTimeG($this->start_date_time_print->format('Y-m-d H:i:s'));
        
        if ((intval($start_workday->format('H')) < intval(conf_workday($start_workday)['from']['hour']))) {
            $start_workday->modify('-1 days');
        }
        $start_workday->setTime(conf_workday($start_workday)['from']['hour'], conf_workday($start_workday)['from']['minute']);
        
        if ($start_workday <$this->start_date_time_print){
            $rdo = $start_workday-> diffRangeHM($this->start_date_time_print);
            $with += $rdo['hours']*60+$rdo['minutes'];
        }
        $with += 100;
        //$with=0;
        
        echo '<div style="  height:24px;
                            width:'. $with.'px;
                            float:left;
                            background: withe;
                            border: solid;
                            border-width: 0px;
                            font-size: larger;
                            padding-top: 2px;">';
        //echo $init_timeline->format('H').':'.$init_timeline->format('i');
        echo '</div>';
        
        usort($printerTimeLine->rolls_end, 'sort_by_datetime');
        
        //Faltan las piezas desde que se cambio el rollo hasta la primera que tenemos ahora
        //Hay contruir una printer timeline auxiliar para procesar las partes que faltan
        $printerTimeLineAux = new PrinterTimeline($printerTimeLine->printer_id,
            $printerTimeLine->code_printer,
            $printerTimeLine->roll_weight,
            $printerTimeLine->roll_replacement_datetime);
        
        $printerTimeLineAux->chargePrinterTimeline($this->start_date_time_print,$this->start_date_time,$this->neighborhood_factory_id);
        //$printerTimeLineAux->chargePrinterTimeline($this->start_date_time_print);
        //$this->assignPrintersSlots();
        
        //Jose 20/05/2022 Análisis 
        $printerTimeLineAux->filamentCalculations ($printerTimeLine->roll_replacement_datetime);
        
        //$printerTimeLineAux->filamentCalculations ($this->start_date_time_print);
        
        
        if (count($printerTimeLineAux) == 0){
            $printerTimeLine->last_slot_out_range = $printerTimeLineAux->last_slot_out_range;  
            $printerTimeLineAux = $printerTimeLine;
            
        }
        
        $this->printFilamentCalculations($printerTimeLineAux,$start_workday,$with);
        
        //$this->printFilamentCalculations($printerTimeLine,$start_workday,$pixels_to_finish_roll_aux,$with);


    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// printAllFilamentCalculations ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function printFilamentCalculations(PrinterTimeline $printerTimeLine,$start_workday,$with)
    {
        $pixels_to_finish_roll_aux = 0;
        $start_date_time_aux = new DateTimeG($this->start_date_time_print->format('Y-m-d H:i:s'));
        usort($printerTimeLine->rolls_end, 'sort_by_datetime');
        foreach ($printerTimeLine->rolls_end as $key => $roll_end){
                
                //$pixels_to_finish_roll =  $printerTimeLine ->getPartialMinutes($this->start_date_time_print,$roll_end_datetime);
                //Si el cambio de filamento es anterior a la linea de espacio tiempo que estoy dibujando, solo tengo que dibujar el espacio,
                //Pero solo en caso de que sea la penúltima.
                //if (!in_array($roll_end['datetime'],$printerTimeLineAux->rolls_end)){
                //if ($roll_end['datetime']>= $start_workday){
                
            
            $pixels_to_finish_roll = $start_date_time_aux->diffMinutes($roll_end['datetime']);
                
                //$pixels_to_finish_roll =    $pixels_to_finish_roll_aux - $pixels_from_start_to_roll_end ;//+ 
                $pixels_to_finish_roll_aux = $pixels_to_finish_roll;
                
                if ($start_date_time_aux >$roll_end['datetime']){
                    $pixels_to_finish_roll_show = $pixels_to_finish_roll * (-1);
                }else{
                    $pixels_to_finish_roll_show = $pixels_to_finish_roll;
                }
                //Lo paso a medias horas
                //$pixels_to_finish_roll_aux += $pixels_to_finish_roll;
                
                //Poner el bloque de hora, dias y mes.
                $color = "red";
                $font_style ="";
                
                
                if ($pixels_to_finish_roll < 25){// Para que si la fecha es muy cercana, el div de la flecha no sea muy pequeño.
                    $pixels_to_finish_roll_aux = 25;
                }
                
                $blink_class="";
                
                //Esto de momento no vale para nada
                $margin_top_offset = 0;
                /*
                if (($zoom >= 25) && ($zoom < 50)) {
                    $margin_top_offset = -10;
                }
                */
                //Fin para nada
                if ($pixels_to_finish_roll_show > -30){
                    if (($pixels_to_finish_roll<30) &&($pixels_to_finish_roll>-60)){
                        //$margin_top = -40 + $margin_top_offset;
                        $margin_top = 76 + $margin_top_offset;
                        $margin_left = $with; //+373
                        echo '      <div style="margin-top: '.$margin_top.';margin-left: '.$margin_left.'px;width:630px;position: relative;font-size: xx-large;">';
                        echo '          <div class="fillament blink" style = "color:red" href="">The filament will be end in '.$pixels_to_finish_roll_show.' min.</div>';
                        //var_dump($roll_end['datetime']);
                        echo '          <div class="blink"  style="font-family: cursive;font-size: xx-large;color: red;text-align: left;">&#8659;</div>';
                        echo '      </div>';
                    }elseif (($pixels_to_finish_roll < 300)&& ($pixels_to_finish_roll_show>0)){
                        //$margin_top = -20 + $margin_top_offset;
                        $margin_top = 77 + $margin_top_offset;
                        $margin_left = $with+82;//400
                        echo '      <div style="margin-top: '.$margin_top.'px;margin-left: '.$margin_left.'px;width:353px;position: relative;font-size: x-large;">';
                        
                        echo '          <div class="" style = "color:red" href="">The filament will be end at '.$roll_end['datetime']->format('H').':'.$roll_end['datetime']->format('i').'</div>';
                        
                        //var_dump($roll_end['datetime']);
                        echo '      </div>';
                        echo '      <div class=""  style="margin-left: '.($margin_left+82).'px;font-family: cursive;font-size: xx-large;color: red;text-align: left;">&#8659;</div>';
                    };
                    //$margin_top = 15 + $margin_top_offset;;
                    $margin_top = 0 + $margin_top_offset;;
                    echo '<div style="  margin-top: '.$margin_top.'px;width:'.$pixels_to_finish_roll_aux.'px;position: relative;float:left;background: white;
                                        border: solid;border-width: 0px;font-family: sans-serif;font-size: medium;color:' . $color . ';' . $font_style . '">';
            
                    echo '      <div style="font-size: x-large;color: red;text-align: right;">';
                    if (($pixels_to_finish_roll < 300) && ($pixels_to_finish_roll_show>0)){
                        //echo '      <div   style="font-family: cursive;font-size: xxx-large;color: red;text-align: right;">&#8659;</div>';
                    }elseif (($pixels_to_finish_roll < 600) && ($pixels_to_finish_roll_show>0)){
                        echo '      <div class="fillament" style = "color:red" href="">The filament will end on '.$roll_end['datetime']->format('H').':'.$roll_end['datetime']->format('i').'</div>';
                    }elseif ($pixels_to_finish_roll_show > 0) {
                    echo '      <div class="fillament"  style = "color:red" href="">The filament will end on '.$roll_end['datetime']->format('Y-m-d H:i:s').'</div>';
                    }
                    echo '      </div>';
                    //if ((($pixels_to_finish_roll_show > 0) || ($pixels_to_finish_roll<30)) && ($pixels_to_finish_roll>-60)) {
                    //if (($pixels_to_finish_roll_show > 0) && ($pixels_to_finish_roll>30)) {
                    //if (($pixels_to_finish_roll_show > 0) && ($pixels_to_finish_roll>600)) {
                    if (($pixels_to_finish_roll_show > 0) && ($pixels_to_finish_roll>300)) {
                    echo '      <div class="'.$blink_class.'"  style="font-family: cursive;font-size: xx-large;color: red;text-align: right;">&#8659;</div>';
                    }
                    echo '</div>';
                    //}else{
                        
                    //}
                    $start_date_time_aux = $roll_end['datetime'];
                }//END if ($pixels_to_finish_roll_show > -30){
                
                
        }//END foreach ($printerTimeLine->rolls_end as $key => $roll_end){
        
        echo '</div>'; //Fin Div Principal
        //$roll_end_aux = $roll_end;
    }
    
    
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////// printFilamentCalculations OLD ///////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function printFilamentCalculationsOld(int $zoom, int &$total_pixels, PrinterTimeline $printerTimeLine)
    {
        //Div Principal
        
        
        
        
        echo '<div class="zoom" style="  zoom:' . $zoom . '%;
                            float:none;
                            width:393200px;
                            height:142px;
                            border-color: blue;
                            //border-top-style: dotted;
                            border-width:1px";
                            margin-top: 10px;>';
        
        //19320 - 19280 = 40px
        //$week = 0;
        
        
        
        
        
        $half_hours_count = 0;
        $hours_count = 0;
        //$days_aux = intdiv((($half_hours_count+conf_workday()['from']['hour'])-($half_hours_count/2)),24);
        
        $now = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
        //$now = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
        get_object_vars($now);
        $now_real = DateTime::CreateFromFormat('Y-m-d H:i:s',$now->format('Y-m-d H:i:s'));
        //$now_real = new DateTime($now->date);
        //if ((intval($now->format('H'))< intval(conf_workday()['from']['hour']))&&($now->format('D') != 'Mon')){
        //Si la hora actual se menor que la de inicio de jornada muestrará tambien el días anterior.Antes ademas no tenía que se el lunes
        if ((intval($now->format('H')) < intval(conf_workday($now)['from']['hour']))) {
            $now->modify('-1 days');
        }
        $now_hour = $now->format('H');
        
        
        
        $current_datetime = new DateTime($this->start_date_time->format('Y-m-d H:i:s'));
        //$current_datetime = $now;
        $current_datetime->setTime(intval(conf_workday($current_datetime)['from']['hour']), 0);
        //var_dump($printerTimeLine->roll_end_datetime);
        $finished_rolls = array();
        $total_pixels = $printerTimeLine -> getTotalMinutes($this->start_date_time);
        foreach ($printerTimeLine->rolls_end as $roll_end_datetime){
            
            //$pixels_to_finish_roll =  $printerTimeLine ->getPartialMinutes($this->start_date_time,$roll_end_datetime);
            $rdo = diffRangeHM($this->start_date_time,$roll_end_datetime);
            $pixels_to_finish_roll = $rdo['hours']*60+$rdo['minutes']- 120;
            //Lo paso a medias horas
            //$pixels_to_finish_roll = intval($pixels_to_finish_roll /30)*30;
            $finished_rolls[ $pixels_to_finish_roll ] = $roll_end_datetime ;
            
        }
        
        
        
        
        //$now_day =  $now->format('D');
        //$now_week =  $now->format('W');
        //var_dump($now);
        $current_datetime = new DateTime($now->format('Y-m-d H:i:s'));
        //$current_datetime = $now;
        $current_datetime->setTime(intval(conf_workday($current_datetime)['from']['hour']), 0);
        $current_week = $current_datetime->format('W');
        $current_month = $current_datetime->format('n');
        
        $color = "black";
        $change_font_style = true;
        
        //$relative_day = 1;
        echo '<div style="  height:24px;
                            width:250px;
                            float:left;
                            background: withe;
                            border: solid;
                            border-width: 0px;
                            font-size: larger;
                            padding-top: 2px;">';
        //echo $now->format('H').':'.$now->format('i');
        echo '</div>';
        
        for ($i = 0; $i <= $total_pixels; $i ++) {
            //Para el cálculo de las noches
            $relative_days = (int)((($half_hours_count + conf_workday($now_real)['from']['hour']) - ($half_hours_count / 2))/ 24);
            $day_hour = (($half_hours_count + conf_workday($now_real)['from']['hour']) - ($half_hours_count / 2)) - ($relative_days * 24);
            //Destacar el días y la hora actual, cambiando el estilo de la fuente
            if ((intval($day_hour) == intval($now_hour)) && ($change_font_style)) {
                $font_style = 'font-family: cursive;font-size: x-large;';
                $change_font_style = false;
            } else {
                $font_style = '';
            }
            
            //Destacar las noches y los fines de semana, con  color rojo. No tiene en cuenta minutos
            if (($day_hour > conf_workday($now_real)['from']['hour']) && ($day_hour < conf_workday($now_real)['to']['hour'])
                )
            {
                $color = "black";
            } else {
                $color = "red";
            }
            
            
            
            
            
            //Poner el bloque de hora, días y mes.
            echo '<div style="width:1px;position: relative;float:left;background: white;border: solid;border-width: 0px;font-family: sans-serif;font-size: medium;color:' . $color . ';' . $font_style . '">';
            //echo $hours_count;
            
            if (array_key_exists($i, $finished_rolls)){
                
                echo '  <div style="font-family: cursive;font-size: x-large;color: red;margin-top: 52px; width: 507px;">
                            <a hre="#">Finish the filament('.$finished_rolls[$i]->format('Y-m-d H:i:s').')!!!</a>
                        </div>';
                echo '  <div style="font-family: cursive;font-size: xx-large;color: red;margin-top: -4px;">&#8595;</div>';
            }else{
                echo '  <div style="height: 2px;' . $font_style . '">&nbsp;</div>';
                //echo '  <div style="width:1px"></div>';
            }
            
            
            echo '</div>';
            if ((($half_hours_count) % 2) == 0) {
                $hours_count ++;
            }
            
            $half_hours_count ++;
        }
        ; //END FOR
        
        echo '</div>'; //Fin Div Principal
    }
    
    
    
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////// printHtmlAll ///////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function printHtmlAll()
    {
        //echo ('</br>');
        $total_percent = 0;
        
        $slots_num=0;
        //echo "<br><br><br><br><br><br><br><br><br><br>";
        echo '<div style= "overflow:auto" class="overflow-panel"  data-fl-scrolls>';
        $next_part = $this->nextPart();
        foreach ($this as $printer_time_line) {
            $slots_num+= count($printer_time_line);
            $total_percent += $this->printHtml($printer_time_line,$next_part);
            //echo "</br></br></br>";
        }
        
        if (count($this)== 0){
            echo 'Total Media: 0.0 %';
        }else{
            echo 'Total Media: '. number_format($total_percent/count($this),2).'%';
        }
        
        echo ' - Num Slots:'. $slots_num;
        echo "</div>";
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////// printHtml //////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function printHtml(PrinterTimeline $printerTimeLine, array $next_part):float
    {
        
        echo '<a name="printer'.$printerTimeLine->printer_id.'"></a>';
        //Print HTML
        $zoom = '50';
        $total_pixels = 0;
        $printerTimeLine->uasort('startAscendingComparison');
        
        $this->printAllFilamentCalculations($zoom, $total_pixels, $printerTimeLine);
        
        
        $this->printSlots($zoom, $total_pixels, $printerTimeLine, $next_part);
        echo '<div class="zoom"  style="zoom:' . $zoom.'%;height: 2px;"></br></div>';
        //echo "</br>|";
        $this->printRule($zoom, $total_pixels, $printerTimeLine);
        
        
        //echo "</br></br></br></br></br>";
        
        $percent = $printerTimeLine->percent($this->start_date_time);
        echo '<div style="margin-top:20px;">';
        echo ' &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                Printing Minutes: '.$printerTimeLine->getPrintingMinutes($this->start_date_time). ' | Gap Minutes: '.$printerTimeLine->getGapsMinutes($this->start_date_time).' | '. number_format($percent,2).'%';
        echo "</div>";
        return $percent;
        
        echo '</a>';
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////// printHtml //////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function percent():float
    {
        $total_percent = 0;
        $count = 0;
        foreach ($this as $printerTimeLine) {
            if ($printerTimeLine->available==1){
                $printerTimeLine->uasort('startAscendingComparison');
                $total_percent += $printerTimeLine->percent($this->start_date_time);
                $count++;
            }
        }
        if ($count == 0) return 0;
        return number_format($total_percent/$count,2);
    }
    
    
    public function nextPartOld(){
        $next_part = array();
        $min_start = new DateTimeG($this->start_date_time);
        $min_start->modify('+ 48 hours');
        $min_start_candidate = new DateTimeG($min_start);
        $first_iteration= true;
        foreach ($this as $printerTimeLine) {
            $arrayIteratorPrinter = $printerTimeLine->getIterator();
            if ( $arrayIteratorPrinter->valid()){
                $slot = $arrayIteratorPrinter->current();
                while ( $arrayIteratorPrinter->valid() && ($slot->initiated == 1 )){
                    $arrayIteratorPrinter->next();
                    if ($slot->state==8) //Salgo ya que es un slot sin asignar y no cuenta
                    {
                        break;
                    }
                    $slot = $arrayIteratorPrinter->current();
                }
                if ( $arrayIteratorPrinter->valid()){
                    $min_start_candidate = new DateTimeG($slot->start);
                    if ($first_iteration || ($min_start_candidate < $min_start)){
                        $next_part['slot'] = $slot;
                        $next_part['printer'] = $printerTimeLine;
                        $min_start = $min_start_candidate;
                        $first_iteration= false;
                    }
                }
            }else{
                continue;
            }
        }
        return ($next_part);
    }
    
    
    public function nextPart(){
        $next_part = array();
        $min_end = new DateTimeG($this->start_date_time);
        $min_end->modify('-48 hours');
        $min_end_candidate = new DateTimeG($min_end);
        $first_iteration= true;
        foreach ($this as $printerTimeLine) {
            $arrayIteratorPrinter = $printerTimeLine->getIterator();
            if ( $arrayIteratorPrinter->valid()){
                $slot = $arrayIteratorPrinter->current();
                $count = 0;
                while ( $arrayIteratorPrinter->valid() && ($slot->initiated == 1 )){
                    $arrayIteratorPrinter->next();
                    if (($slot->state==9)) //Salgo ya que es un slot que se está imprimiendo y por lo tanto la pieza que está despues puede ser la nextpart
                    {
                        break;
                        
                    }
                    $count ++;
                    
                    $slot = $arrayIteratorPrinter->current();
                }
                if ( $arrayIteratorPrinter->valid()){
                    $min_end_candidate = new DateTimeG($slot->end);
                    if ($first_iteration xor (($min_end_candidate < $min_end)/*&&($count!=0)&&($slot->state==9)*/)){
                        $slot = $arrayIteratorPrinter->current();
                        $next_part['slot'] = $slot;
                        $next_part['printer'] = $printerTimeLine;
                        $min_end = $min_end_candidate;
                        $first_iteration= false;
                    }
                }
            }else{
                
                continue;
            }
        }
        if (array_key_exists('slot', $next_part)){
            $start = new DateTimeG ($next_part['slot']->start);
            if ($start == $this->start_date_time){
                unset($next_part['slot']);
                unset($next_part['printer']);
            }
        }
        return ($next_part);
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// chargePrintersMatrix ///////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function chargePrintersMatrix()
    {
        
        //Carga de BDD
        //TODO Filtro quitar fechas del pasado
        //$sql = DataBase::queryAvailablePrinters($this->neighborhood_factory_id);
        $sql = DataBase::queryAllPrinters($this->neighborhood_factory_id);
        $available_printers = DataBase::getConnection()->query($sql);
        //Incializo PrinterMatrix
        $count = 0;
        
        foreach ($available_printers as $available_printer) {
            $roll_replacement_datetime = new DateTimeG($available_printer['roll_replacement_datetime']);
            $printerTimeLine = new PrinterTimeline($available_printer['id'], $available_printer['code'],$available_printer['roll_weight'],$roll_replacement_datetime,NULL,intval($available_printer['available']));
            $sql = DataBase::queryChargePrintersMatrix($available_printer['id'],$this->neighborhood_factory_id);
            $printer_time_line = DataBase::getConnection()->query($sql);
           
            $printer_slot_aux = array();
            $is_first_iteration = true;
            $is_last_slot_out_range = false;
            if ($printer_time_line ){
                foreach ($printer_time_line as $printer_slot) {
                    
                    if ((($printer_slot['state_id'] == 9) || ($printer_slot['state_id'] == 10)) && ($printer_slot['neighborhood_id']==$this->neighborhood_factory_id)) {
                        $count++;
                        //$printerTimeLine->addSlot(
                        $slot_start_datetime = new DateTimeG($printer_slot['start_datetime']);
                        //$slot_start_datetime = new DateTime($printer_slot['start_datetime']);
                        
                        /*
                        if ($first_iteration) {
                            if ($slot_start_datetime < $this->start_date_time) {
                                //get_object_vars($start_datetime);
                                $slot_start_datetime = $this->start_date_time;
                            }
                            $first_iteration = false;
                        }
                        */
                        $slot_end_datetime = new DateTimeG($printer_slot['end_datetime']);
                        //$slot_end_datetime = new DateTime($printer_slot['end_datetime']);
                        get_object_vars($slot_start_datetime);
                        get_object_vars($slot_end_datetime);
                        
                        $init_timeline = new DateTimeG($this->start_date_time_print);
                        if ((intval($init_timeline->format('H')) < intval(conf_workday($init_timeline)['from']['hour']))) {
                            $init_timeline->modify('-1 days');
                        }
                        $init_timeline->setTime(conf_workday($init_timeline)['from']['hour'], 0);
                        
                        if ($slot_end_datetime >= $init_timeline) {
                        //if ($slot_end_datetime > $init_timeline) {
                            $printerSlot = new PrinterSlot(
                                $printer_slot['id'],
                                $printer_slot['order_id'],
                                $printer_slot['file_id'],
                                $printer_slot['file_part_name'],
                                $printer_slot['file_name'],
                                $printer_slot['version_part_name'],
                                $printer_slot['product_id'],
                                $printer_slot['product_name'],
                                $printer_slot['final_product_id'],
                                //$printer_slot['start_datetime'],
                                //$slot_start_datetime->date,
                                $slot_start_datetime ->format('Y-m-d H:i:s'),
                                //$printer_slot['end_datetime'],
                                $slot_end_datetime ->format('Y-m-d H:i:s'),
                                $printer_slot['weight'],
                                $printer_slot['state_id'],
                                $printer_slot['initiated']);
                            $printerTimeLine->append($printerSlot);
                        }
                        //if ((!$is_last_slot_out_range)&&(!$is_first_iteration)){
                        if (isset($printer_slot_aux['start_datetime'])){
                            //$printer_slot_aux_start_datetime = new DateTimeG ($printer_slot_aux['start_datetime']);
                            $printer_slot_aux_start_datetime = new DateTimeG($printer_slot_aux['start_datetime']);
                            //if ((!$is_first_iteration)&& ($this->start_date_time > $printer_slot_aux_start_datetime)){
                            if (($init_timeline > $printer_slot_aux_start_datetime)){
                                $printerTimeLine->last_slot_out_range = new PrinterSlot(
                                    $printer_slot_aux['id'],
                                    $printer_slot_aux['order_id'],
                                    $printer_slot_aux['file_id'],
                                    $printer_slot_aux['file_part_name'],
                                    $printer_slot_aux['file_name'],
                                    $printer_slot_aux['version_part_name'],
                                    $printer_slot_aux['product_id'],
                                    $printer_slot_aux['product_name'],
                                    $printer_slot_aux['final_product_id'],
                                    $printer_slot_aux['start_datetime'],
                                    //$slot_start_datetime->date,
                                    //$slot_start_datetime ->format('Y-m-d H:i:s'),
                                    $printer_slot_aux['end_datetime'],
                                    // $slot_end_datetime ->format('Y-m-d H:i:s'),
                                    $printer_slot_aux['weight'],
                                    $printer_slot_aux['state_id'],
                                    $printer_slot_aux['initiated']);
                                $is_last_slot_out_range = true;
                            }
                        }
                        
                    }//END if ($printer_slot['state_id'] == 9 || == 10) {
                    $is_first_iteration = false;
                    $printer_slot_aux = $printer_slot;
                }//END foreach ($printer_time_line as $printer_slot) {
                
                if (isset ($slot_end_datetime) && ($slot_end_datetime < $this->start_date_time_print)) {
                    $printerTimeLine->last_slot_out_range = new PrinterSlot(
                        $printer_slot['id'],
                        $printer_slot['order_id'],
                        $printer_slot['file_id'],
                        $printer_slot['file_part_name'],
                        $printer_slot['file_name'],
                        $printer_slot['version_part_name'],
                        $printer_slot['product_id'],
                        $printer_slot['product_name'],
                        $printer_slot['final_product_id'],
                        //$printer_slot['start_datetime'],
                        //$slot_start_datetime->date,
                        $slot_start_datetime ->format('Y-m-d H:i:s'),
                        //$printer_slot['end_datetime'],
                        $slot_end_datetime ->format('Y-m-d H:i:s'),
                        $printer_slot['weight'],
                        $printer_slot['state_id'],
                        $printer_slot['initiated']);
                }
                
                
                $this->append($printerTimeLine);
            }
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// filamentCalculations ///////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function filamentCalculations (){
        //Caculo del filamento
        foreach ($this as $printer_time_line) {
            $printer_time_line->filamentCalculations($this->start_date_time);
        }

    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////// G A P S ///////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function RemoveGapsToLeft($available_printers_aux, array $pending_parts_processed)
    {
        //$printer_aux = $available_printers_aux->fetch_assoc();
        foreach ($available_printers_aux as $printer_aux) {
            if (count($printer_aux) > 0) {
                $current_printer_time_line = $this->searchPrinterTimeLine($printer_aux);
                
                //foreach ($current_printer_time_line as $slot){
                $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
                //$gap_start = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
                $gap_start->modify('+' . conf_delays()['minutes_start'] . ' minutes');
                get_object_vars($gap_start);
                $CurrentPrinterTimeLineIterator = $current_printer_time_line->getIterator();
                While ($CurrentPrinterTimeLineIterator->valid()) {
                    $printerSlot = $CurrentPrinterTimeLineIterator->current();
                    if ($printerSlot->state == 8) {
                        if (! is_overlap_at_night($gap_start) && ! isWeekend($gap_start)) {
                            $printerSlot->start = $gap_start->format('Y-m-d H:i:s');
                            //$printerSlot->start = $gap_start->date;
                            
                            $printerSlotEnd = estimatedPrintingEnd($gap_start, $pending_parts_processed[$printerSlot->part]);
                            get_object_vars($printerSlotEnd);
                            $printerSlot->end = $printerSlotEnd->format('Y-m-d H:i:s');
                            //$printerSlot->end = $printerSlotEnd->date;
                        } elseif (isWeekend($gap_start)) {
                            
                            $printerSlotEnd = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
                            //$printerSlotEnd = new DateTime($printerSlot->end);
                            
                            //alert($message);
                        } elseif (is_overlap_at_night($gap_start)) {
                            //TODO comprobar si es antes o despues de las 00:00 en cuyo caso tambien hay que aumentar un días:
                            $gap_start->setTime(conf_workday($gap_start)['from']['hour'], conf_workday($gap_start)['from']['minute'] + 1);
                            get_object_vars($gap_start);
                            $printerSlot->start = $gap_start->format('Y-m-d H:i:s');
                            //$printerSlot->start = $gap_start->date;
                            
                            $printerSlotEnd = estimatedPrintingEnd($gap_start, $pending_parts_processed[$printerSlot->part]);
                            get_object_vars($printerSlotEnd);
                            $printerSlot->end = $printerSlotEnd->format('Y-m-d H:i:s');
                            //$printerSlot->end = $printerSlotEnd->date;
                        } else {
                            $printerSlotEnd = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
                            //$printerSlotEnd = new DateTime($printerSlot->end);
                        }
                    }
                    
                    //$pendir
                    
                    $gap_start = $printerSlotEnd;
                    $gap_start->modify('+' . conf_delays()['minutes_start'] . ' minutes');
                    
                    $CurrentPrinterTimeLineIterator->next();
                }
            }
        }
        //Fin $this->RemoveGapsToLeft();
    }
    
    public function RemoveGapsToLeftAll($available_printers_aux, array $pending_parts_processed)
    {
        //$printer_aux = $available_printers_aux->fetch_assoc();
        foreach ($available_printers_aux as $printer_aux) {
            if (count($printer_aux) > 0) {
                $current_printer_time_line = $this->searchPrinterTimeLine($printer_aux);
                
                //foreach ($current_printer_time_line as $slot){
                $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
                //$gap_start = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
                $gap_start->modify('+' . conf_delays()['minutes_start'] . ' minutes');
                get_object_vars($gap_start);
                $CurrentPrinterTimeLineIterator = $current_printer_time_line->getIterator();
                While ($CurrentPrinterTimeLineIterator->valid()) {
                    $printerSlot = $CurrentPrinterTimeLineIterator->current();
                    if ($printerSlot->state == 8) {
                        if (! is_overlap_at_night($gap_start)){
                            $printerSlot->start = $gap_start ->format('Y-m-d H:i:s');
                            
                            //var_dump($pending_parts_processed);
                            $printerSlotEnd = estimatedPrintingEnd($gap_start, $pending_parts_processed[$printerSlot->part]);
                            get_object_vars($printerSlotEnd);
                            $printerSlot->end = $printerSlotEnd ->format('Y-m-d H:i:s');
                        }elseif (is_overlap_at_night($gap_start)) {
                            //TODO comprobar si es antes o despues de las 00:00 en cuyo caso tambien hay que aumentar un días:
                            
                            
                            
                            $gap_day_start = $gap_start->format('w');
                            $gap_start->setTime(session('working_days')[$gap_day_start]['from']['hour'], session('working_days')[$gap_day_start]['from']['minute']);
                            //$gap_start->setTime(conf_workday()['from']['hour'], conf_workday()['from']['minute'] + 1);
                            
                            get_object_vars($gap_start);
                            $printerSlot->start = $gap_start ->format('Y-m-d H:i:s');
                            
                            $printerSlotEnd = estimatedPrintingEnd($gap_start, $pending_parts_processed[$printerSlot->part]);
                            get_object_vars($printerSlotEnd);
                            $printerSlot->end = $printerSlotEnd ->format('Y-m-d H:i:s');
                        } else {
                            $printerSlotEnd = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
                            //$printerSlotEnd = new DateTime($printerSlot->end);
                        }
                    }else{
                        $printerSlotEnd = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end);
                        //$printerSlotEnd = new DateTime($printerSlot->end);
                        //$end = new DateTime($slot->end);
                        
                    }
                    
                    //$pendir
                    
                    $gap_start = $printerSlotEnd;
                    $gap_start->modify('+' . conf_delays()['minutes_start'] . ' minutes');
                    
                    $CurrentPrinterTimeLineIterator->next();
                }
            }
        }
        //Fin $this->RemoveGapsToLeft();
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////A S S I G N - P R I N T E R - S L O T S//////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////A S S I G N - P R I N T E R - S L O T S//////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsWeekdays()
    {
        
        //TODO Falta guardarlos en bdd pero lo tiene que hacer en la función addSlotToPrinter()
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //TODO Comprobar si el pedido ya tiene creadas Parts???????????????????????
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////// Carga de BDD //////////////////////////////////////
        $sql = sprintf('
                SELECT  *
                FROM    printer AS pri
                WHERE   available = 1 AND neighborhood_factory_id = ' . $this->neighborhood_factory_id . '
                ORDER BY pri.id;
               ');
        $available_printers = DataBase::getConnection()->query($sql);
        $available_printers_aux = DataBase::getConnection()->query($sql);
        
        /*
         $arrayPrintersIterator = $available_printers->getIterator();
         while( $arrayPrintersIterator->valid()) {
         //if( $arrayIterator->valid()) {
         $printer = $arrayPrintersIterator->current();
         $arrayPrintersIterator->next();
         }
         */
        $sql = sprintf('
            SELECT *
            FROM  time_without_operators AS two
            WHERE neighborhood_factory_id  = ' . $this->neighborhood_factory_id . ' AND active = 1
            ORDER BY day;');
        
        //$time_without_operator = DataBase::getConnection()->query($sql);
        
        $sql = DataBase::queryPendingParts($this->neighborhood_factory_id);
        //ORDER BY p.weight DESC,p.order_id  ASC
        //DataBase::getConnection()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE);
        $pending_parts = DataBase::getConnection()->query($sql);
        ////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////// Inicializo estados ////////////////////////////////
        //$day_week = 5;
        $day_week = 4;
        $week_weekend = 0;
        $week = 0;
        
        $is_gap_start_ini_change = false;
        ////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////// Recorrer $pendig_part ////////////////////////////////////
        $printer = array();
        $order_id = - 1;
        
        $pending_parts_processed = array();
        
        $available_printers_num = $available_printers->num_rows;
        $available_printers_count = 0;
        
        while ($pending_part = $pending_parts->fetch_assoc()) {
            //foreach ($pending_parts as $pending_part){
            $pending_parts_processed[$pending_part['id']] = $pending_part;
            //Si ha cambiado el pedido inicializamos
            if ($order_id != $pending_part['order_id']) {
                //Trasladar huecos de la impresora actual pequeños a la izquierda a la noche.
                
                //Si ya he usado todas las impresoras, vuelvo a la primera impresora
                if (++ $available_printers_count > $available_printers_num) {
                    //do something
                    $available_printers_count = 0;
                    $available_printers->data_seek(0);
                }
                
                $printer = $available_printers->fetch_assoc();
                
                $day_week = 4;
                $week_weekend = 0;
                $week = 0;
                
                $is_gap_start_ini_change = false;
            }
            
            //Busco la printer time line de la impresora actual
            
            $current_printer_time_line = $this->searchPrinterTimeLine($printer);
            
            $order_id = $pending_part['order_id'];
            
            if (isSizeHours('biggest', $pending_part['estimated_printing_hours'])) {
                $this->assignPrintersSlotsBiggest($pending_part, $week_weekend, $current_printer_time_line);
            } elseif (isSizeHours('big', $pending_part['estimated_printing_hours'])) {
                $this->assignPrintersSlotsBig($pending_part, $day_week, $week_weekend, $week, $current_printer_time_line);
            } elseif (isSizeHours('median', $pending_part['estimated_printing_hours'])) {
                //$is_gap_start_ini_change = $this->assignPrintersSlotsMedian($pending_part,$is_gap_start_ini_change);
                $this->assignPrintersSlotsMedian($pending_part, $is_gap_start_ini_change, $current_printer_time_line);
            } elseif (isSizeHours('small', $pending_part['estimated_printing_hours'])) {
                echo "</br>--------------> small       </br>";
            } elseif (isSizeHours('smallest', $pending_part['estimated_printing_hours'])) {
                echo "</br>--------------> the smallest       </br>";
            } else {
                echo "</br>--------------> out of range       </br>";
            }
            ;
            $week = 0;
        } //END foreach ($pending_parts as $pending_part){
        
        $this->RemoveGapsToLeft($available_printers_aux, $pending_parts_processed);
        
        //Quitar huecos grandes moviendo de las piezas biggest y big
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////A S S I G N - P R I N T E R - S L O T S//////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function assignPrintersSlotsFitIntoLargestGap($pending_parts,&$pending_parts_processed,$available_printers)
    {
        //echo "<br>-----------------------------  A S I G N A C I O N  CABEN DENTRO DEL HUECO MAS GRANDE-------------------------------------------------<br>";
        while ($pending_part = $pending_parts->fetch_assoc()) {
            if (($pending_part['state_id'] == 9) || ($pending_part['state_id'] == 10)) break;
            $this->parts[$pending_part['id']] = $pending_part;
            $pending_parts_processed[$pending_part['id']] = $pending_part;
            $current_printer_time_line = $this->searchPrinterTimeLineFitBiggestGap($pending_part);
            $current_printer_time_line->uasort('startAscendingComparison');
            if ($current_printer_time_line->printer_id == $this[0]->printer_id ){
                $current_printer_time_line = $this->searchPrinterTimeLineMoreIdle($available_printers);
            }
            
            $this->assignPrintersSlotsAllOriginal($pending_part, $current_printer_time_line);
            $current_printer_time_line->uasort('startAscendingComparison');
        } //END foreach ($pending_parts as $pending_part){
        
    }
    public function assignPrintersSlotsMoveFromEndToTheGaps()
    {
        //echo "<br>-----------------------------  A S I G N A C I O N  MOVER DEL FINAL A LOS HUECOS QUE HAY POR DELANTE -------------------------------------------------<br>";
        //Recorro todas la printerLine
        $printerTimeLine = $this[0];
        foreach ($this as $printerTimeLine){
            $printerTimeLine->uasort('startAscendingComparison');
            //Inicializacion
            $slot_last_datetime = array();
            $slot_last_datetime['start'] =  DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
            $slot_last_datetime['end'] =  DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
            //Busco el BiggestGap
            $biggest_gap = $printerTimeLine->getBiggestGap($this->start_date_time);
            
            //Busco el ultimo elemento(el que acaba más tarde y que sera pequeño, probablemente)
            foreach ($printerTimeLine as $slot){
                if ( DateTime::CreateFromFormat('Y-m-d H:i:s',$slot->end) > $slot_last_datetime){
                    $slot_last_datetime['start'] = $slot->start ;
                    $slot_last_datetime['end'] = $slot->end ;
                    $slot_last_datetime['part'] = $slot->part ;
                }
            }
            //Muevo el ultimo slot al gap
            foreach ($printerTimeLine as $slot){
                if ( $slot->part == $slot_last_datetime['part']){
                    $slot->start = $biggest_gap['start']->format('Y-m-d H:i:s');
                    $diff = diffRangeHM(DateTime::CreateFromFormat('Y-m-d H:i:s',$slot_last_datetime['start']),
                        DateTime::CreateFromFormat('Y-m-d H:i:s',$slot_last_datetime['end']));
                    $pending_part = array();
                    $pending_part['estimated_printing_hours']=$diff['hours'];
                    $pending_part['estimated_printing_minutes']=$diff['minutes'];
                    $slot->end = estimatedPrintingEnd($biggest_gap['start']->modify('+' . conf_delays()['minutes_start'] . ' minutes'), $pending_part)->format('Y-m-d H:i:s');
                }
            }
            $printerTimeLine->uasort('startAscendingComparison');
        }
    }
    public function assignPrintersSlotsPendingPartsAlfa(&$pending_parts_processed)
    {
        //echo "<br>-----------------------------  A S I G N A C I O N   PARTES PENDIENTES DE ASIGNAR ALFA --------------------------------------------------<br>";
        
        if (!is_null($this->missing_parts)){
            //echo ('<br>Partes pendientes de asingar: '.count($this->missing_parts));
            foreach ($this->missing_parts as $pending_part){
                //TODO acarla de desasignadas
                //$key = array_search($pending_part['id'], array_column($this->missing_parts, 'id'));
                unset($this->missing_parts[$pending_part['id']]);
                
                //$key = array_search($slot->part, array_column($this->missing_parts, 'part'));
                //unset($this->missing_parts[$key]);
                
                
                $pending_parts_processed[$pending_part['id']] = $pending_part;
                //$current_printer_time_line =
                $printerTimeLineMin = $this->searchPrinterTimeLineMin();
                
                
                
                $this->assignPrintersSlotsALL($pending_part, $printerTimeLineMin);
                $printerTimeLineMin->uasort('startAscendingComparison');
            } //END foreach ($pending_parts as $pending_part){
        }
    }
    
    
    public function assignPrintersSlotsPendingPartsBeta(&$pending_parts_processed)
    {
        //echo "<br>---------------------------------  A S I G N A C I O N   PARTES PENDIENTES DE ASIGNAR GAMMA ------------------------------------------------------<br>";
        
        
        
        if (!is_null($this->missing_parts)){
            //echo ('<br>Partes pendientes de asingar: '.count($this->missing_parts));
            foreach ($this->missing_parts as $pending_part){
                //TODO acarla de desasignadas
                //$key = array_search($pending_part['id'], array_column($this->missing_parts, 'id'));
                unset($this->missing_parts[$pending_part['id']]);
                
                //$key = array_search($slot->part, array_column($this->missing_parts, 'part'));
                //unset($this->missing_parts[$key]);
                
                
                $pending_parts_processed[$pending_part['id']] = $pending_part;
                //$current_printer_time_line =
                $printerTimeLineMin = $this->searchPrinterTimeLineWithGap($pending_part);
                
                
                
                $this->assignPrintersSlotsALL($pending_part, $printerTimeLineMin);
                $printerTimeLineMin->uasort('startAscendingComparison');
            } //END foreach ($pending_parts as $pending_part){
        }
    }
    
    public function assignPrintersSlotsPendingPartsGamma(&$pending_parts_processed)
    {
        //echo "<br>---------------------------------  A S I G N A C I O N   PARTES PENDIENTES DE ASIGNAR GAMMA ------------------------------------------------------<br>";
        
        
        $printerTimeLineMin = $this[0];
        if (!is_null($this->missing_parts)){
            //echo ('<br>Partes pendientes de asingar: '.count($this->missing_parts));
            foreach ($this->missing_parts as $pending_part){
                //TODO acarla de desasignadas
                //$key = array_search($pending_part['id'], array_column($this->missing_parts, 'id'));
                
                unset($this->missing_parts[$pending_part['id']]);
                
                //$key = array_search($slot->part, array_column($this->missing_parts, 'part'));
                //unset($this->missing_parts[$key]);
                
                
                $pending_parts_processed[$pending_part['id']] = $pending_part;
                //$current_printer_time_line =
                $printerTimeLineMin = $this->searchPrinterTimeLineWithGapNoRepeat($pending_part,$printerTimeLineMin);
                
                
                
                $this->assignPrintersSlotsALL($pending_part, $printerTimeLineMin);
                $printerTimeLineMin->uasort('startAscendingComparison');
            } //END foreach ($pending_parts as $pending_part){
        }
    }
    
    public function assignPrintersSlotsPendingPartsDelta(&$pending_parts_processed)
    {
        //echo "<br>---------------------------------  A S I G N A C I O N   PARTES PENDIENTES DE ASIGNAR GAMMA ------------------------------------------------------<br>";
        
        
        $printerTimeLineMin = $this[0];
        if (!is_null($this->missing_parts)){
            //echo ('<br>Partes pendientes de asingar: '.count($this->missing_parts));
            foreach ($this->missing_parts as $pending_part){
                //TODO acarla de desasignadas
                //$key = array_search($pending_part['id'], array_column($this->missing_parts, 'id'));
                unset($this->missing_parts[$pending_part['id']]);
                
                //$key = array_search($slot->part, array_column($this->missing_parts, 'part'));
                //unset($this->missing_parts[$key]);
                
                
                $pending_parts_processed[$pending_part['id']] = $pending_part;
                //$current_printer_time_line =
                $printerTimeLineMin = $this->searchPrinterTimeLineMoreIdle();
                
                
                $this->assignPrintersEndSlots($pending_part, $printerTimeLineMin);
                $printerTimeLineMin->uasort('startAscendingComparison');
            } //END foreach ($pending_parts as $pending_part){
        }
    }
    
    public function assignPrintersSlotsPendingPartsEpsilon(&$pending_parts_processed)
    {
        //echo "<br>---------------------------------  A S I G N A C I O N   PARTES PENDIENTES DE ASIGNAR GAMMA ------------------------------------------------------<br>";
        
        $printerTimeLineMin = $this[0];
        if (!is_null($this->missing_parts)){
            
            usort($this->missing_parts, 'sort_by_hours');
            
            
            //echo ('<br>Partes pendientes de asingar: '.count($this->missing_parts));
            foreach ($this->missing_parts as $pending_part){
                //TODO acarla de desasignadas
                //$key = array_search($pending_part['id'], array_column($this->missing_parts, 'id'));
                unset($this->missing_parts[$pending_part['id']]);
                
                //$key = array_search($slot->part, array_column($this->missing_parts, 'part'));
                //unset($this->missing_parts[$key]);
                
                
                $pending_parts_processed[$pending_part['id']] = $pending_part;
                //$current_printer_time_line =
                //$printerTimeLineMin = $this->searchPrinterTimeLineMoreIdle();
                //$printerTimeLineMin = $this->searchPrinterTimeLineWithGap($pending_part);
                $printerTimeLineMin = $this->searchPrinterTimeLineWithGap($pending_part);
                
                
                
                /*
                 foreach ($this as $printerLine){
                 
                 $result = $printerLine->getGapWithoutNight($this->start_date_time,$pending_part);
                 var_dump($printerLine->code_printer);
                 var_dump($result);
                 }
                 */
                
                
                
                
                
                
                $this->assignPrintersSlotsFinal($pending_part, $printerTimeLineMin);
                $printerTimeLineMin->uasort('startAscendingComparison');
            } //END foreach ($pending_parts as $pending_part){
        }
        
    }
    
    
    public function assignPrintersSlotsOverlaps()
    {
        echo "<br>------------------------------------------  S O L A P A M I E N T O S ------------------------------------------------------------<br>";
        //Buscar solapamientos
        
        $printer_slot_aux = new PrinterSlot("","", "", "","","", "", "","", "",$this->start_date_time->format('Y-m-d H:i:s'), $this->start_date_time->format('Y-m-d H:i:s'), "", "","");
        $first_iteration = true;
        foreach ($this as $printerTimeLine){
            //echo "Solapamientos-".$printerTimeLine->code_printer.": ";
            foreach ($printerTimeLine as $printer_slot){
                $printerTimeLine->uasort('startAscendingComparison');
                if (
                    (is_overlap_or_wrap(DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start),
                        DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end),
                        DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->start),
                        DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end))
                        )
                    &&
                    (!$first_iteration)
                    ){
                        
                        //echo '-'. $printer_slot->part.':'.$printer_slot->file.'-';
                        $pending_part = array();
                        $diff = diffRangeHM( DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start),
                            DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end));
                        
                        
                        //La nueva fecha será despues del primer slot del solapamiento
                        $printer_slot_datetime = array();
                        $printer_slot_datetime['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end)->modify("+" . conf_delays()['minutes_start'] . " minutes");
                        
                        if (is_overlap_at_night($printer_slot_datetime['start'])){
                            $day_night_start = (int)$printer_slot_datetime['start']->format('W');
                            $printer_slot_datetime['start']->setTime(
                                session('working_days')[$day_night_start]['from']['hour'],
                                session('working_days')[$day_night_start]['from']['minute'] + conf_delays()['minutes_start']);
                        }
                        
                        $pending_part['estimated_printing_hours']=$diff['hours'];
                        $pending_part['estimated_printing_minutes']=$diff['minutes'];
                        
                        $printer_slot_datetime['end'] = estimatedPrintingEnd($printer_slot_datetime['start'],$pending_part);
                        /*
                         if (is_overlap_at_night($printer_slot_datetime['end'])){
                         $day_night_start = (int)$printer_slot_datetime['end']->format('W');
                         $printer_slot_datetime['end']->setTime(
                         session('working_days')[$day_night_start]['to']['hour'],
                         session('working_days')[$day_night_start]['to']['minute'] + conf_delays()['minutes_start']);
                         }
                         */
                        $printer_slot->start = $printer_slot_datetime['start']->format('Y-m-d H:i:s');
                        $printer_slot->end = $printer_slot_datetime['end']->format('Y-m-d H:i:s');
                        
                }else{
                    $first_iteration = false;
                }
                $printer_slot_aux = $printer_slot;
            }
            //echo '<br>';
        }
    }
    public function assignPrintersSlotsOverlaps2()
    {
        //echo "<br>------------------------------------------  S O L A P A M I E N T O S  2------------------------------------------------------------<br>";
        //Buscar solapamientos
        $printer_slot_aux = new PrinterSlot("","", "", "","","", "", "","","", $this->start_date_time->format('Y-m-d H:i:s'), $this->start_date_time->format('Y-m-d H:i:s'), "", "","");
        
        foreach ($this as $printerTimeLine){
            $printerTimeLine->uasort('startAscendingComparison');
            $first_iteration = true;
            //echo "Solapamientos-".$printerTimeLine->code_printer.": ";
            foreach ($printerTimeLine as $printer_slot){
                $printer_slot_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end);
                $printer_slot_aux_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end);
                //Si la slot actual($printer_slot) se solapa o envuelve al anterior($printer_slot_aux)l entoces hay que moveerlo.
                //Ojo: la primera iteración no se tiene en cuanta ya que no existe slot anterior($printer_slot_aux)
                if (
                    (
                        (is_overlap_or_wrap(DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start),
                            DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end),
                            DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->start),
                            DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end))
                            )
                        ||
                        //Puede ser que al adelantar el slot anterior se hayan quedaddo slots en medio por lo que hay que ponerlos despues
                        (FALSE)//($printer_slot_end < $printer_slot_aux_start)
                        )
                    &&
                    (!$first_iteration)
                    ){
                        
                        //echo '-'. $printer_slot->part.':'.$printer_slot->file.'-';
                        $pending_part = array();
                        
                        //La nueva fecha será despues del primer slot del solapamiento
                        $printer_slot_datetime = array();
                        $printer_slot_datetime['end'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end);
                        
                        $diff = diffRangeHM( DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->start),
                            DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end));
                        
                        $pending_part['estimated_printing_hours'    ]=$diff['hours'];
                        $pending_part['estimated_printing_minutes']=$diff['minutes'];
                        //Si la fecha fin se solapa por adelantamos el slot
                        if (is_overlap_at_night($printer_slot_datetime['end'])){
                            
                            if (($printer_slot_datetime['end'])->format('H') > 12 ){
                                $printer_slot_datetime['end']->modify('+1 days');
                            }
                            $day_night_start = (int)$printer_slot_datetime['end']->format('w');
                            //var_dump($printer_slot_datetime['end']->format('W'));
                            //var_dump($day_night_start);
                            //var_dump($printer_slot_datetime['end']);
                            
                            $printer_slot_datetime['end']->setTime(
                                session('working_days')[$day_night_start]['from']['hour'],
                                session('working_days')[$day_night_start]['from']['minute'] + conf_delays()['minutes_start']);
                            
                            $printer_slot_datetime['start'] = estimatedPrintingStart($printer_slot_datetime['end'],$pending_part);
                            
                            $printer_slot_aux->start = $printer_slot_datetime['start']->format('Y-m-d H:i:s');
                            $printer_slot_aux->end = $printer_slot_datetime['end']->format('Y-m-d H:i:s');
                            
                            $printer_slot_datetime['start'] = $printer_slot_datetime['end'];//->modify("+" . conf_delays()['minutes_start'] . " minutes");
                            $diff = diffRangeHM( DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start),
                                DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end));
                            
                            $pending_part['estimated_printing_hours'    ]=$diff['hours'];
                            $pending_part['estimated_printing_minutes']=$diff['minutes'];
                            
                            
                            $printer_slot_datetime['end'] = estimatedPrintingEnd($printer_slot_datetime['start'],$pending_part);
                            
                            $printer_slot->start =$printer_slot_datetime['start']->modify("+" . conf_delays()['minutes_start'] . " minutes")->format('Y-m-d H:i:s');
                            $printer_slot->end = $printer_slot_datetime['end']->format('Y-m-d H:i:s');
                            
                        }else{
                            //Como en $printer_slot_datetime['end'] tenemos el final del slot anterior calculo slot  sumandole 5 minutos
                            
                            $printer_slot_datetime['start'] = $printer_slot_datetime['end']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                            $printer_slot_datetime['end'] = estimatedPrintingEnd($printer_slot_datetime['start'],$pending_part);
                            
                            $printer_slot->start = $printer_slot_datetime['start']->format('Y-m-d H:i:s');
                            $printer_slot->end = $printer_slot_datetime['end']->format('Y-m-d H:i:s');
                        }
                        
                        
                }else{
                    if (($printer_slot_end < $printer_slot_aux_start)&&(!$first_iteration)){
                        //echo "Warnnig";
                        
                        $printer_slot_datetime['start'] = $printer_slot_datetime['end']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                        $printer_slot_datetime['end'] = estimatedPrintingEnd($printer_slot_datetime['start'],$pending_part);
                        
                        $printer_slot->start = $printer_slot_datetime['start']->format('Y-m-d H:i:s');
                        $printer_slot->end = $printer_slot_datetime['end']->format('Y-m-d H:i:s');
                        
                        //var_dump($printer_slot);
                    }
                }
                if ($first_iteration){
                    $first_iteration = false;
                }
                $printer_slot_aux = $printer_slot;
            }
            //echo '<br>';
        }
        
    }
    public function printOverlapsSimple()
    {
        //echo "<br>------------------------------------------  S O L A P A M I E N T O S PRINT ----------------------------------------------------------<br>";
        $printer_slot_aux = new PrinterSlot("","","", "", "","","", "", "", "","",$this->start_date_time->format('Y-m-d H:i:s'), $this->start_date_time->format('Y-m-d H:i:s'), "", "","");
        $first_iteration = true;
        echo "<br>";
        echo "Solapamientos: ";
        $total_solapamientos = 0;
        
        foreach ($this as $printerTimeLine){
            $printerTimeLine->uasort('startAscendingComparison');
            //$printerTimeLine->uasort('startAscendingComparison');
            echo $printerTimeLine->code_printer.": ";
            $solapamientos = 0;
            foreach ($printerTimeLine as $printer_slot){
                if (
                    (is_overlap_or_wrap(DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start),
                        DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end),
                        DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->start),
                        DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end))
                        )
                    &&
                    (!$first_iteration)
                    )
                {
                    $solapamientos ++;
                    
                }
                $first_iteration = false;
                $printer_slot_aux = $printer_slot;
            }
            echo '('.$solapamientos.'),';
            $total_solapamientos += $solapamientos;
        }
        echo '<br>Total Solapamientos: '.$total_solapamientos;
        echo "<br>";
        
    }
    public function printOverlaps()
    {
        //Buscar solapamientos
        echo "<br>------------------------------------------  S O L A P A M I E N T O S PRINT ----------------------------------------------------------<br>";
        
        foreach ($this as $printerTimeLine){
            $printer_slot_aux = new PrinterSlot("","", "","","", "", "", "", "",$this->start_date_time->format('Y-m-d H:i:s'), $this->start_date_time->format('Y-m-d H:i:s'), "", "","");
            $first_iteration = true;
            //echo "<br>";
            $count = 0;
            
            $printerTimeLine->uasort('startAscendingComparison');
            //$printerTimeLine->uasort('startAscendingComparison');
            //echo "Solapamientos-".$printerTimeLine->code_printer.": <br>";
            if (count($printerTimeLine)>1){
                foreach ($printerTimeLine as $printer_slot){
                    if (
                        //(is_overlap_or_wrap(DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start),
                        (DateTimeG::isOverlapOrWrap(new DateTimeG($printer_slot->start),
                            new DateTimeG($printer_slot->end),
                            new DateTimeG($printer_slot_aux->start),
                            new DateTimeG($printer_slot_aux->end))
                        )
                        &&
                        (!$first_iteration)
                       )
                    {
                        $aux = new DateTimeG($printer_slot->start);
                        $diff = $aux ->diffRangeHM(new DateTimeG($printer_slot->end));
                        echo '-------------------'.$printerTimeLine->code_printer.' - Part:'. $printer_slot->part.':'.$printer_slot->file.' - '. $diff['hours'].':'.$diff['minutes'].'<br/>';
                        
                        $count++;
                    }else{
                        $first_iteration = false;
                    }
                    $printer_slot_aux = $printer_slot;
                }
                //echo '<br>';
            }
        }
        echo '<br>Total: '.$count.'<br>';
    }
    
    public function printNightOverlaps()
    {
        //Buscar solapamientos
       // echo "<br>------------------------------------------ P R I N T  -  S O L A P A M I E N T O S POR LA NOCHE ------------------------------------------------<br>";
        echo "<br><b>Night Overlaps:</b> <br>";
        $count = 0;
        foreach ($this as $printerTimeLine){
            $printerTimeLine->uasort('startAscendingComparison');
            //$printerTimeLine->uasort('startAscendingComparison');
            //echo "Solapamientos-".$printerTimeLine->code_printer.": <br>";
            foreach ($printerTimeLine as $printer_slot){
                $start = new DateTimeG($printer_slot->start);
                //$start ->modify('-1 minutes');
                if ( $start->isOverlapAtNight()){
                    $aux = new DateTimeG($printer_slot->start);
                    $diff = $aux->diffRangeHM(new DateTimeG($printer_slot->end));
                    echo $printerTimeLine->code_printer.': <a href= "#part_link_'.$printer_slot->part.'">'. $printer_slot->part.'</a>: '.$printer_slot->file_part_name.' -  '.$printer_slot->start.' - '. $diff['hours'].':'.$diff['minutes'].' <br/>';
                    $count ++;
                }
                
            }
            //echo '<br>';
        }
        echo '<b>Total: '.$count.'</b>';
    }
    
    public function printBlocks()
    {
        //Buscar solapamientos
        echo "<br>------------------------------------------ P R I N T - BLOCKS ---------------------------------------------<br>";
        echo "<br>";
        if (isset($this->blocks_parts_matrix)){
            $count = 0;
            foreach ($this->blocks_parts_matrix as $key_blocks => $blocks){
                echo $count.". Block: ".$key_blocks.". <br>";
                foreach ($blocks as $key_block => $block){
                    if ($key_block!='minutes'){
                        echo "Id_part:".$key_block;
                        echo " - File: ".$block['file_part_name'];
                        echo ' - Hours: '.$block['estimated_printing_hours'];
                        echo ' - Minutes: '.$block['estimated_printing_minutes'].'<br>';
                    }
                }
                $count++;
                //echo '<br>';
            }
            echo '<br>Total: '.$count.'<br>';
        }
    }
    
    public function calculateMissingPrintersSlots(){
        $sql = DataBase::queryPendingParts($this->neighborhood_factory_id);
        $pending_parts = DataBase::getConnection()->query($sql);
        $slots=array();
        $count = 0;
        $printers = 0;
        foreach($this as $printerTimeLine){
            foreach ($printerTimeLine as $printerSlot){
                if ($printerSlot->state == 8){
                    array_push($slots,$printerSlot->part);
                }
                $count++;
            }
            $printers++;
        }
        //unset($this->missing_parts);
        $this->missing_parts = array();
        $count_pending_parts=0;
        $count_missing_parts=0;
        while ($pending_part = $pending_parts->fetch_assoc()) {
            $count_pending_parts++;
            //if (!in_array( $pending_part['id'],$slots) && !array_key_exists($pending_part['id'], $this->missing_parts)){
            if ( !in_array( $pending_part['id'],$slots)){
                $count_missing_parts ++;
                $this->missing_parts[$pending_part['id']]=  $pending_part;
            }
        }

    }

    public function calculateMissingPrintersSlots2(){
        $parts = array();
        $parts['total'] = 0;
        $parts['missing'] = array();
        $parts['printed'] =  array();
        $parts['assigned'] =  array();
        $parts['unassigned'] =  array();
        $parts['other'] =  array();
        $parts['repeated'] =  array();
        
        $printers_count= 0;
        
        
        $test = array();
        $count_printed=0;
        $count_assigned=0;
        $count_unassigned=0;
        //Calculo Resumen del estado de PrinerMatrix
        foreach($this as $PrinterLine){
            $printers_count ++;
            foreach ($PrinterLine as $slot){
                // El slot no esta ni asignado, ni sin asignar, ni es otro caso

                
                //Resumen de Parts
                if ($slot->state == 10) $count_printed ++;
                if ($slot->state == 9) $count_assigned ++;
                if ($slot->state == 8) $count_unassigned ++;
                
                if (($slot->state == 10) && (!in_array($slot->part, $parts['printed'] ))){
                    array_push($parts['printed'], $slot->part);
                    //array_push($test, $slot->part);
                }elseif (($slot->state == 9) && (!in_array($slot->part, $parts['assigned'] ))){
                    array_push($parts['assigned'], $slot->part);
                    array_push($test, $slot->part);
                }elseif (($slot->state == 8)&& (!in_array($slot->part, $parts['unassigned'] ))){
                    array_push($parts['unassigned'], $slot->part);
                    
                /*
                }elseif (($slot->state == 8)&& (in_array($slot->part, $parts['unassigned'] ))){
                    array_push($parts['repeated'], $slot->part);
                */
                }elseif (($slot->state != 8) && ($slot->state != 9) && ($slot->state != 10) && (!in_array($slot->part, $parts['other']))){
                    //Esto debera ser siempre 0
                    array_push($parts['other'], $slot->part);
                }else{
                    array_push($parts['missing'], $slot->part);
                   
                }
                
                $parts['total']++;
            }
        }
        
        $sql = DataBase::queryPendingParts($this->neighborhood_factory_id);
        $pending_parts = DataBase::getConnection()->query($sql);
        $this->missing_parts = array();
        $count_pending_parts = 0;
        $count_missing_parts = 0;
        
        while ($pending_part = $pending_parts->fetch_assoc()) {
            $count_pending_parts++;
            //if (!in_array( $pending_part['id'],$slots) && !array_key_exists($pending_part['id'], $this->missing_parts)){
            if ( in_array( $pending_part['id'],$parts['missing'])){
                $count_missing_parts ++;
                $this->missing_parts[$pending_part['id']]=  $pending_part;
            }
        }

    }
    
    public function clean(){
    //Eliminamos las piezas que están repetidas.   
        $slots=array();
        foreach($this as $PrinterLine){
            foreach ($PrinterLine as $slot){
                array_push($slots, $slot->part);
            }
        }
        $repeated_slots = array_diff_assoc($slots, array_unique($slots));
        $reviewed_slots = array();
        
        foreach($this as $printerTimeLine){
            $delete_keys= array();
            foreach ($printerTimeLine as $key => $slot){
                if ( in_array( $slot->part,$repeated_slots)){
                    $start = createDateTime($slot->start);
                    if (!isOverlapAtNight($start)&&(!in_array( $slot->part,$reviewed_slots))){
                    //Si no se solapa con la noche y no está en el array de revisados:
                        //Ponerlo en revisados 
                        array_push($reviewed_slots, $slot->part);
                    }else{
                        //Hay que quitarlo de la printer line;
                        array_push($delete_keys, $key);
                    }
                    //Quitarlo de la printerline y ponerlo en unassigned, si no está.
                }
            }
            foreach ($delete_keys as $key){
                if (isset($printerTimeLine[$key])){
                    unset($printerTimeLine[$key]);
                }
            }
        }


    }
    
    
    public function assignPrintersSlotsMovingToRight($printerTimeLine,$printer_slot):Bool{
        
        
        
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////// N E A R  -  L A S T - N I G H T ///////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function reassignPrintersSlotsNearLastNight()
    {
        //Si una pieza de entre 4-5h empieza muy cerca de la noche, hay que ponerla en otra impresora que tenga tenga un hueco
        /*
         foreach ($this as $printerTimeLine){
         $printerTimeLine->SlotSortAsc();
         $copy = $printerTimeLine->getArrayCopy();
         $item = end($copy);
         var_dump($item);
         
         }
         die;
         */
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////// N E A R  -  N I G H T ///////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function reassignPrintersSlotsNearNight93($pending_parts)
    {
        //Si una pieza mediana acaba muy cerca de la noche hay que reasignarla
        //Para ello utilizamos:
        //La pieza que está delante $printerSlot_1
        //la pieza que queremos reasignar $printerSlot_2
        //La pieza que está detrás $printerSlot_3
        //Luego recolocamos las piezas que hay a continación hasta que podamos
        
        foreach ($this as $printerTimeLine){
            if ($printerTimeLine->percent($this->start_date_time) < 94){
                $printerTimeLine->uasort('startAscendingComparison');
                
                $arrayIterator = $printerTimeLine->getIterator();
                
                While ($arrayIterator->valid()) {
                    
                    $printerSlot_1 = $arrayIterator->current();
                    $start_1 = createDateTime($printerSlot_1->start);
                    $end_1= createDateTime($printerSlot_1->end);
                    
                    if (isSizeHours('biggest', diffHours($start_1, $end_1)) || isSizeHours('big', diffHours($start_1, $end_1))) {
                        $arrayIterator->next();
                        if ($arrayIterator->valid()){
                            $printerSlot_2 = $arrayIterator->current();
                            $start_2 = createDateTime($printerSlot_2->start);
                            $end_2 = createDateTime($printerSlot_2->end);
                            $key_2= $arrayIterator->key();
                            //Si es mediana o pequeña y acaba cerca del incio de  la noche me y me planteo el cambio 
                            if (
                                !isSizeHours('biggest', diffHours($start_2, $end_2)) 
                                &&
                                !isSizeHours('big', diffHours($start_2, $end_2)) 
                                &&
                                isNearStartNight($end_2)
                               )
                            {
                                //Este foreach es para recuperar toda la información de la pending part: TODO optimización.
                                
                                foreach ($pending_parts as $pending_part){
                                    if ($pending_part['id'] == $printerSlot_2->part){
                                        $arrayIterator->next();
                                        if ($arrayIterator->valid()){
                                            $printerSlot_3 = $arrayIterator->current();
                                            $start_3 = createDateTime($printerSlot_3->start);
                                            $end_3 = createDateTime($printerSlot_3->end);
                                            $diff = diffRangeHM($start_3, $end_3); 
                                            //Movemos el tercer slot  justo al incio de la noche en que acaba el primer slot, 
                                            //menos los minutes_start * 2, ya que la funcion estimatedPrintingNightFlex añade minutes_start
                                            $start_3 = estimatedPrintingNightFlex($end_1)['start']
                                                                    ->modify("-" . 2 * conf_delays()['minutes_start'] . " minutes");
                                                                    
                                            
                                            $end_3 = estimatedPrintingNightFlex($end_1)['start']
                                                                    ->modify("-" . 2 * conf_delays()['minutes_start'] . " minutes")
                                                                    ->modify("+". $diff['hours'].' hours' )
                                                                    ->modify("+". $diff['minutes'].' minutes' );
                                            
                                            //Si la fecha en que hemos movido vuelve a producir un hueco nocturno grande;
                                            //No sirve para nada hacer el cambio, por lo tanto lo mejor será optar por otra estrategia 
                                            if (isNearEndNight($end_3)){
                                                //Miramos si de las piezas asignadas hay alguna que encaje de manera optima en el hueco.
                                                //El hueco es lo que queda entre el final de la pieza 1 y el inicio de la pieza 3 
                                                //TODO
                                                $gap_hours = diffHours($end_1, $start_3);
                                                usort($this->missing_parts, 'sort_by_hours_desc');
                                                $gap_filled = false;
                                                foreach ($this->missing_parts as $key =>$unassigned_part){
                                                    if ($gap_hours > $unassigned_part['estimated_printing_hours']){
                                                        //Ponemos en desasignadas la pieza que vamos machacar
                                                        $this->missing_parts[$printerSlot_2->part] = $pending_part;
                                                        
                                                        $new_part_id = $printerSlot_2->part;
                                                        //Machacamos la pieza: TODO función changePart o changeSlot o swapSlotPart
                                                        $printerSlot_2->part = $unassigned_part['id'];
                                                        $printerSlot_2->order  = $unassigned_part['order_id'];
                                                        $printerSlot_2->file = $unassigned_part['file_id'];
                                                        $printerSlot_2->file_part_name = $unassigned_part['file_part_name'];
                                                        $printerSlot_2->file_name = $unassigned_part['file_name'];
                                                        $printerSlot_2->version_part_name = $unassigned_part['version_part_name'];
                                                        $printerSlot_2->product = $unassigned_part['product_id'];
                                                        $printerSlot_2->product_name = $unassigned_part['product_name'];
                                                        $printerSlot_2->final_product = $unassigned_part['final_product_id'];
                                                        $printerSlot_2->start = $end_1
                                                                                ->modify("+" .  conf_delays()['minutes_start'] . " minutes")
                                                                                ->format('Y-m-d H:i:s');
                                                        $printerSlot_2->end =   $end_1
                                                                                ->modify("+" . $unassigned_part['estimated_printing_hours'] . " hours")
                                                                                ->modify("+" . $unassigned_part['estimated_printing_minutes'] . " minutes")
                                                                                ->format('Y-m-d H:i:s');
                                                        $printerSlot_2->weight = $unassigned_part['weight']; 
                                                        $printerSlot_2->initiated = $unassigned_part['initiated'];
                                                        
                                                        //Quitar de desaignadas la pieza con la que hemos machacado
                                                        unset($this->missing_parts[$key]);
                                                        
                                                        //Cambiar la hora del slot 3
                                                        $printerSlot_3->start = $start_3->format('Y-m-d H:i:s');
                                                        $printerSlot_3->end = $end_3->format('Y-m-d H:i:s');
                                                        $gap_filled = true;
                                                        break;
                                                    }
                                                }
                                                
                                                if (!$gap_filled){
                                                    $printerSlot_3->start = $start_3->format('Y-m-d H:i:s');
                                                    $printerSlot_3->end = $end_3->format('Y-m-d H:i:s');
                                                    $this->missing_parts[$printerSlot_2->part] = $pending_part;
                                                    unset($printerTimeLine[$key_2]);
                                                }
                                                //Hay que mover todas la piezas que hay a continuación hacia la izquierda (retrasarlas)
                                                //Hay que hacerlo sin que empiecen por la noche.
                                                //TODO   
                                                $printer_slot_aux = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
                                                $is_first_iteration = true;
                                                $arrayIterator->next();
                                                while ($arrayIterator->valid()){
                                                    $printerSlot = $arrayIterator->current();
                                                    if ($is_first_iteration){
                                                        //Si es la primera iteración el incio del nuevo slot es el fin tercer slot de los procesados anteriormente
                                                        $start = createDateTime($printerSlot_3->end);
                                                        
                                                    }else{
                                                        //Si no es la primera
                                                        //En caso de no ser la primera iteración el inicio del nuevo es el fin del elemento anterior
                                                        $start = createDateTime($printer_slot_aux->end);
                                                    }
                                                    
                                                    
                                                    if (isOverlapAtNight($start)){
                                                        //Calculo la nueva fecha para el slot actual y compuebo solapamientos por la noche
                                                        //Comprobar si  el final es por la noche ya que el inicio de la siguente pieza:
                                                        //Estará al inicio de la siguiente jornada.
                                                        if ($start->format('H') >= 12) {
                                                            $start->modify('+1 days');
                                                        }
                                                        $day_from = $start->format('w');
                                                        $start->setTime(session('working_days')[$day_from]['from']['hour'], session('working_days')[$day_from]['from']['minute']);
                                                    }
                                                    //Hay que añadir el retraso incial.
                                                    $start->modify("+" . conf_delays()['minutes_start'] . " minutes");
                                                    $end =  estimatedPrintingEndSlot($start, $printerSlot);
                                                    //Comprobamos que el incio no se solapa por la noche;
                                                    //Y que no hay un hueco nocturno grande
                                                    if (!isOverlapAtNight($start) && (!isOverlapAtNight($end) || isNearEndNight($end))){
                                                        $printerSlot->start=$start->format('Y-m-d H:i:s');
                                                        $printerSlot->end=$end->format('Y-m-d H:i:s');
                                                    }elseif (isOverlapAtNight($end) &&!isNearEndNight($end)){
                                                        //Si el final no está cerca de la noche probablemente se pueda intercalar una pieza sin asginar
                                                        //Y así reducir el hueco.
                                                        $gap_hours = diffHours(
                                                                            $start, 
                                                                            estimatedPrintingNightFlex($end)['start']
                                                                            ->modify("-" . conf_delays()['minutes_start'] . " minutes"));
                                                        usort($this->missing_parts, 'sort_by_hours_desc');
                                                        foreach ($this->missing_parts as $unassigned_part){
                                                            if ($gap_hours > $unassigned_part['estimated_printing_hours']){
                                                                //Insertar Slot
                                                                $estimated_printing = array();
                                                                $estimated_printing['start'] = $start;
                                                                $estimated_printing['end'] = estimatedPrintingNightFlex($end)['start']
                                                                                            ->modify("-" . conf_delays()['minutes_start'] . " minutes");
                                                                $pending_part_aux =  $this->newPendingPart($unassigned_part);
                                                                
                                                                $printerTimeLine->addSlotAndSortAsc($estimated_printing, $pending_part_aux);
                                                                
                                                                unset($this->missing_parts[$unassigned_part['id']]);
                                                                break;
                                                            }
                                                        }
                                                        $printerSlot->start = estimatedPrintingNightFlex($end)['start']->format('Y-m-d H:i:s');
                                                        //$end = 
                                                        $printerSlot->end = estimatedPrintingEndSlot(estimatedPrintingNightFlex($end)['start'], $printerSlot)
                                                                            ->format('Y-m-d H:i:s');
                                                        
                                                    }else{
                                                        break;
                                                    }

                                                    $printer_slot_aux->update($printerSlot);
                                                    $arrayIterator->next();
                                                    $is_first_iteration = false;
                                                }//END while ($arrayIterator->valid()){
                                                
                                            }else{
                                                //La otra estrategia es poner piezas no asignadas que hagan que el hueco nocturno sea menor
                                                //Por ejemplo: quiero desasignar una pieza de 7h.
                                                //Busco una de 6, 5 o hasta 4h horas y la asigno
                                                //y despues asigno la de 7h, así reduciré el hueco en 6,5 o 4h.
                                                //TODO
                                            }
                                        }
                                        break;
                                    }
                                }
                            }
                        }else{
                            break;
                        }
                    }
                    $arrayIterator->next();
                }//END While ($arrayIterator->valid()) {
            }//END if ($printerTimeLine->percent($this->start_date_time) < 94){
        }//END foreach ($this as $printerTimeLine){
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////// N I G H T  - O V E L A P S //////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function reassignNightOverlapsSimple($pending_parts)
    {
        foreach ($this as $printerTimeLine){
            $delete_keys = array();
            $missing_parts = array();
            foreach ($printerTimeLine as $key =>$printer_slot){
                $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start);
                if (($printer_slot->state == 8) && (isOverlapAtNight($slot_start))){
                    
                    $overlaped_slot = $printer_slot->toPendingPart();
                    $missing_parts[$key] =  $overlaped_slot;
                    
                    unset($printerTimeLine[$key]);
                }
            }
        }
        
        foreach ($this as $printerTimeLine){
            foreach ($printerTimeLine as $key =>$printer_slot){
                
            }
        }
    }
    
    public function NightOverlaps($action)
    {
        
        foreach ($this as $key => $printerTimeLine){
            //$printerTimeLine->uasort('startAscendingComparison');
            
            switch ($action) {
                case 'reassign':
                    $printerTimeLineNew = PrinterTimeline::reassignNightOverlaps($this->start_date_time,$printerTimeLine);
                    $printerTimeLine->uasort('startAscendingComparison');
                    $printerTimeLineNew->uasort('startAscendingComparison');
                    
                    
                    if ($printerTimeLine->percent($this->start_date_time) < ($printerTimeLineNew->percent($this->start_date_time)+0.5)){
                        $this[$key] = $printerTimeLineNew;
                    }
                    break;
                case 'swap':
                    $printerTimeLineNew = PrinterTimeline::swapNightOverlaps($this->start_date_time,$printerTimeLine);
                    $printerTimeLine->uasort('startAscendingComparison');
                    $printerTimeLineNew->uasort('startAscendingComparison');
                    
                    
                    if ($printerTimeLine->percent($this->start_date_time) < ($printerTimeLineNew->percent($this->start_date_time)+1)){
                        $this[$key] = $printerTimeLineNew;
                    }
                    break;
            }
            

        }
    }
    
    
    public function reassignNightOverlaps()
    {
        
        foreach ($this as $key => $printerTimeLine){
            //$printerTimeLine->uasort('startAscendingComparison');

            $printerTimeLineNew = PrinterTimeline::reassignNightOverlaps($this->start_date_time,$printerTimeLine);

            $printerTimeLine->uasort('startAscendingComparison');
            $printerTimeLineNew->uasort('startAscendingComparison');

            
            if ($printerTimeLine->percent($this->start_date_time)< ($printerTimeLineNew->percent($this->start_date_time)+0.5)){
                $this[$key] = $printerTimeLineNew;
            }
        }
    }
    
    
    public function unassignNightOverlaps($pending_parts)
    {
        //Buscar solapamientos
        //echo "<br>------------------------------------------ U N A S S I G N E D  -  S O L A P A M I E N T O S POR LA NOCHE ---------------------------------------<br>";
        //echo "<br>";
        foreach ($this as $printerTimeLine){
            $printerTimeLine->uasort('startAscendingComparison');
            //$printerTimeLine->uasort('startAscendingComparison');
            //echo "Solapamientos-".$printerTimeLine->code_printer.": <br>";
            //foreach ($printerTimeLine as $printer_slot){
            $delete_keys = array();
            foreach ($printerTimeLine as $key =>$printer_slot){
                $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start);
                if (($printer_slot->state == 8) && (isOverlapAtNight($slot_start))){
                    //echo $printerTimeLine->code_printer.': '. $printer_slot->part.':'.$printer_slot->file.' <br/>';
                    //unset($printer_slot);
                    /*
                    if (($printer_slot->state != 8)){
                        
                        $night = OverlapAtNight();
                        $diff = diffRangeHM($night['nigth_to'], $slot_start);
                        if (($diff['hours']==0)&&($diff['minutes']<=conf_delays()['minutes_start'])){
                            $rdo = makeGapMovingToRight($printerTimeLine,$printer_slot);
                        }
                    }
                    */
                    foreach ($pending_parts as $pending_part){
                        if ($pending_part['id'] == $printer_slot->part){
                            $this->missing_parts[$printer_slot->part] = $pending_part;
                        }
                    }
                    array_push($delete_keys, $key);
                    
                    
                    //$printer_slot = null;
                    //echo $printerTimeLine->code_printer.': '. $printer_slot->part.':'.$printer_slot->file.' <br/>';
                }
            }
            foreach ($delete_keys as $key){
                unset($printerTimeLine[$key]);
            }
            
            //echo '<br>';
        }
    }
    
    public function unassignedOutRangeSlots(){
        $maxPrinterTimeline = array();
        $total_days = 0;
        $max_days = 0;
        $count=0;
        
        foreach ($this as $printerTimeLine){
            if (isset($printerTimeLine)&& (count($printerTimeLine)>0)){
                $printerTimeLine->uasort('startAscendingComparison');
                $copy = $printerTimeLine->getArrayCopy();
                $last_slot = end($copy);
                $last_datetime = DateTimeG::newDateTime($last_slot->end);
                $days = $this->start_date_time->diffDays($last_datetime);
                
                if ($days > $max_days){
                    $max_days = $days;
                    $maxPrinterTimeline = $printerTimeLine;
                }
                
                $total_days += $days;
                $count++;
            }
        };
        if ($count!=0){
            $media_days = $total_days/$count;
            $limit_datetime = $this->start_date_time->createDateTime()
                                ->modify('+'.intval($media_days).' days');
            if ($media_days + conf_days_out_range_slot() < $max_days ){
            //Si el slot que acaba más tarde está fuera de rango, 
            //hay que desasignar todas las piezas que está fuera de rango
                $maxPrinterTimeline->uasort('startAscendingComparison');
                $delete_keys = array();
                foreach ($maxPrinterTimeline as $key => $printerSlot){
                    if ($limit_datetime < createDateTime($printerSlot->end)){
                        $this->missing_parts[$printerSlot->part] = $printerSlot->toPendingPart();
                        array_push($delete_keys, $key);
                    }
                }
                
                foreach ($delete_keys as $key){
                    unset($maxPrinterTimeline[$key]);
                }
            }
        }
    }
    
    public function unassignSmallPartsNightOverlaps($pending_parts)
    {
        
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////// B L O C K  //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function assignPrintersSlotsMain($module,$pending_parts,$pending_parts_processed,$available_printers)
    {
        $this->assignPrintersSlotsFitIntoLargestGap($pending_parts,$pending_parts_processed,$available_printers);
        //echo "<br>-----------------------------  A S I G N A C I O N  MOVER DEL FINAL A LOS HUECOS QUE HAY POR DELANTE -------------------------------------------------<br>";
        //$this->assignPrintersSlotsMoveFromEndToTheGaps();
        
        //echo "<br>-----------------------------  A S I G N A C I O N   PARTES PENDIENTES DE ASIGNAR BETA --------------------------------------------------<br>";
        //echo "<br>Sin Asignar".$this->countUnassigned ()."<br>";
        
        $this->assignPrintersSlotsPendingPartsAlfa($pending_parts_processed);
        
        if ($module == 2){
            //echo "<br>Sin Asignar".$this->countUnassigned ()."<br>";
            //echo "<br>---------------------------------  A S I G N A C I O N   PARTES PENDIENTES DE ASIGNAR GAMMA ------------------------------------------------------<br>";
            $this->assignPrintersSlotsPendingPartsBeta($pending_parts_processed);
            //echo "<br>Sin Asignar".$this->countUnassigned ()."<br>";
           
            $this->assignPrintersSlotsPendingPartsGamma($pending_parts_processed);
            //echo "<br>Sin Asignar".$this->countUnassigned ()."<br>";
            
            $this->assignPrintersSlotsPendingPartsDelta($pending_parts_processed);
            
            //echo "<br>Sin Asignar".$this->countUnassigned ()."<br>";
            //$this->assignPrintersSlotsPendingPartsEpsilon($pending_parts_processed);
            //echo "<br>Sin Asignar".$this->countUnassigned ()."<br>";
        }
        $this->assignPrintersSlotsRemoveEndGaps();
        
        $this->assignPrintersSlotsRemoveGapsWhithEndSlots();
        
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////// I N I T I A L I T A T I O N S /////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function intialitations()
    {
    
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////// Carga de BDD //////////////////////////////////////

        $sql = DataBase::queryAvailablePrinters($this->neighborhood_factory_id );
        $available_printers = DataBase::getConnection()->query($sql);
        
        //$time_without_operator = DataBase::getConnection()->query($sql);
        $sql = DataBase::queryPendingParts($this->neighborhood_factory_id);
        $pending_parts = DataBase::getConnection()->query($sql);
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////// Contador de piezas /////////////////////////////////////////////
        $this->pending_parts_count = $pending_parts->num_rows;
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////// Incializo  la printerMatrix ///////////////////////////////////
        
        foreach ($available_printers as $avilable_printer){
            $this->available_printers[$avilable_printer['id']] = $avilable_printer;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////// Cargo pending parts //////////////////////////////////////
        foreach ($pending_parts as $pending_part){
            $this->parts[$pending_part['id']] = $pending_part;
            $this->no_block_parts[$pending_part['id']] = $pending_part;
            $this->no_block_parts[$pending_part['id']]['minutes'] = $pending_part['estimated_printing_hours']*60+$pending_part['estimated_printing_minutes'];
        } //END foreach ($pending_parts as $pending_part)
    }
    
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////// A S S I G N - P R I N T E R S - S L O T S  ///////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlots()
    {
        
        $this->intialitations();
        ////////////////////////////////////////////////////////////////////////////////////////////////
        $block_sizes = array(24,48);
        //$block_sizes = array(24);
        //$block_sizes = array(48);
        
        $minutesFlex=0;
        $limit_hours=60;
        
        if (!isset($this[0])) return false;
        
        $this->createBlocksMatrix2($block_sizes);
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //$this-> assignPrintersSlotsBlockFlex();
        
        if (($this->countFreePrinters() == $this->count()) || ((isset($this->parts))&&($this->countFreePrinters()>= count($this->parts)))){
            
            for ($minutesFlex=60;$minutesFlex<=300;$minutesFlex+=30){
                $this-> assignPrintersSlotsBlockFlex($minutesFlex);
                if (($this->blocks_parts_matrix == null)||(!isset($this->blocks_parts_matrix))||(count($this->blocks_parts_matrix)==0)){
                    break;
                }
            } 
            ////////////////////////////////////////////////////////////////////////////////////////////////
            $this-> reassignPrintersSlotsBlockFlex();
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        /*
        for ($minutesFlex=60;$minutesFlex<=300;$minutesFlex+=30){
            $this-> assignPrintersSlotsBlockFlex($minutesFlex);
            if (count($this->blocks_parts_matrix)==0){
                break;
            }
        } 
        */
        /*
        $this-> assignPrintersSlotsBlockFlex(60);
        
        $this-> assignPrintersSlotsBlockFlex(90);
        
        $this-> assignPrintersSlotsBlockFlex(180);
        
        $this-> assignPrintersSlotsBlockFlex(240);
        
        $this-> assignPrintersSlotsBlockFlex(300);
        */
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //$this-> assignPrintersSlotsBlockPendingMix();
        ////////////////////////////////////////////////////////////////////////////////////////////////
        /*
        $this->calculateMissingPrintersSlots();
        $count_missing_parts=count ($this->missing_parts);
        while (count ($this->missing_parts)>0){
            $this-> assignPrintersSlotsMissingParts(120);
            //TODO llamada flexible
            $this->calculateMissingPrintersSlots();
            if ($count_missing_parts <= count ($this->missing_parts)){
                break;
            }
            $count_missing_parts=count ($this->missing_parts);
        }
        */
        ////////////////////////////////////////////////////////////////////////////////////////////////
        
        if ((isset($this->parts))&&($this->countFreePrinters()>0)){
            $this->assignPrintersSlotsToFreePrinters();
            if (count($this->parts) > 0){
                $this-> assignPrintersSlotsMissingPartsFree();
            }
        }else{
            $this-> assignPrintersSlotsMissingPartsFree();
        }
        
       
        ////////////////////////////////////////////////////////////////////////////////////////////////
        
        /*
        $this->no_block_parts = array();
        $this->assignPrintersSlotsRemoveEndGaps();
        $this-> assignPrintersSlotsMissingPartsFree();
        */
        $this->reassignPrintersSlotsEndSmallToPerformance();
        $this->reassignPrintersSlotsEndSwapToPerformance();
        $this->calculateMissingPrintersSlots();
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// A S S I G N - B L O C K S - F R E E - P R I N T E R S  //////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function assignPrintersSlotsToFreePrinters(){
        
        $free_printers_array = $this->searchFreePrinters();
        $free_printers = new ArrayObject( $free_printers_array );
        $free_printers_iterator = $free_printers->getIterator();
        while ($free_printers_iterator->valid()){
            
            
            $slot_part_printer = array_pop($this->parts);
            
            if ($slot_part_printer==null){
                break;
            }
            $now = new DateTimeG();
            if ($now->workDay()['start']>$now){
                $now = $now->workDay()['start'];
            }
            $estimated_printing= array();
            $estimated_printing['start'] = $now;
            $estimated_printing['end'] = $now->estimatedPrintingEnd($slot_part_printer);
            
            $printer = $free_printers_iterator->current();
            $printer->addSlotAndSortAsc($estimated_printing,$slot_part_printer);
            
            $free_printers_iterator->next();
        }
        
        
        /*
        if (count($free_printers)>0){
            foreach ($this->parts as $slot_part_printer) {
                //añadir slot a la impresora libre
                $now = new DateTimeG();
                if ($now->workDay()['start']>$now){
                    $now = $now->workDay()['start'];
                }
                $estimated_printing= array();
                $estimated_printing['start'] = $now;
                $estimated_printing['end'] = $now->estimatedPrintingEnd($slot_part_printer);
                $printer = array_pop($free_printers);
                $printer->addSlotAndSortAsc($estimated_printing,$slot_part_printer);
            }
        }
        */
    }
    
    public function countFreePrinters(){
        $count_free_printers = 1;
        foreach ($this as $printer_timeline){
            if ($printer_timeline->available == 0){
                continue;
            }
            $free_printer = true;
            foreach ($printer_timeline as $slot_part_printer) {
                //if (($slot_part_printer->initiated == 1) && ($slot_part_printer->state == 9)){
                if (($slot_part_printer->state == 9)){
                    $free_printer = false;
                    break;
                }
            }
            if ($free_printer){
                $count_free_printers ++;
            }
        }
        return $count_free_printers-1;
    }
    
    
    function searchFreePrinters(){
        $free_printers = array();
        foreach ($this as $printer_timeline){
            if ($printer_timeline->available == 0){
                continue;
            }
            $free_printer = true;
            foreach ($printer_timeline as $slot_part_printer) {
                //if (($slot_part_printer->initiated == 1) && ($slot_part_printer->state == 9)){
                if (($slot_part_printer->state == 9)){
                    $free_printer = false;
                    break;
                }
            }
            if ($free_printer){
                array_push($free_printers, $printer_timeline);
            }
        }
        return $free_printers;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////// A S S I G N - B L O C K S - F L E X  ////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function assignPrintersSlotsBlockFlex(int $minutesFlex=0,int $limit_hours= 60)
    {
        
        if (isset($this->blocks_parts_matrix)){
            $unassigned = array();
            foreach ($this->blocks_parts_matrix as $key_block => $block){
                $current_printer_time_line = $this->searchPrinterTimeLineMoreIdle();
                $current_printer_time_line->uasort('startDescendingComparison');
                $arrayIteratorPrinter = $current_printer_time_line->getIterator();
                
                //$unassigned_block_parts = array();
                //Si la PrinterTimeLine está vacía se puede asignar sin comprobaciones.
                //Ojo que puede haber imprimiendose una pieza.para corregir fecha de inicio. 
                $start_date_time_block = $this->start_date_time->createDateTime();
                if ($arrayIteratorPrinter->valid()){
                    $last_slot = $arrayIteratorPrinter->current();
                    //Comprobamos si  hay que corregir la fecha de inicio
                    
                    $current_printer_time_line->uasort('startAscendingComparison');
                    $arrayIteratorPrinterAscending = $current_printer_time_line->getIterator();
                    $first_slot = $arrayIteratorPrinterAscending->current();
                    
                    
                    if ($first_slot->initiated == 1){
                        $start_date_time_block = new DateTimeG( $first_slot->end);
                    }
                    
                    
                    $start_date_time_block = DateTimeG::newDateTime($last_slot->end);
                    if ($start_date_time_block->isOverlapAtNightFlex()){
                        $night = $start_date_time_block->OverlapAtNight();
                        $start_date_time_block = $night['night_to'];
                    }
                    
                    $start_date_time_block->modify("+" . conf_delays()['minutes_start'] . " minutes");
                }
                $unassigned_block_parts = $this->assignBlockToPrinter3($block,$key_block,$current_printer_time_line,$unassigned,$start_date_time_block,$minutesFlex,$limit_hours);
                //While para saber donde empezamos
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////
            //Quito los bloques que he asignado
            foreach ($unassigned as $key => $block){
                unset($this->blocks_parts_matrix[$key] );
                
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////  A S S I G N - B L O C K - T O - P R I N T E R - 3 - F L E X //////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignBlockToPrinter3(array $block,int $key, PrinterTimeline $current_printer_time_line, array &$unassigned, DateTimeG $start_date_time_block, int $minutesFlex=0,int $limit_hours=60):array
    {
        $unassigned_block_parts = array();
        $block_parts = array();
        foreach ($block as $key_aux => $block_part){
            if ($key_aux != "minutes"){
                array_push($block_parts,$block_part);
            }
        }
        //usort($this->no_block_parts, 'sort_by_minutes_desc');
        //minuto del día minimo en el que es acceptado que acabe el nuevo bloque: 24*60+4. 4 min añadidos para que supere la noche.
        $day_minutes_2_min =  1444;
        
        //Añadir tercera pieza al bloque
        $candidate = $this->addThirdPartToBlock( $block_parts, $day_minutes_2_min, $minutesFlex,$start_date_time_block,$limit_hours);
        if (isset($candidate)&&(count($candidate)>0)){
            $unassigned_block_parts[$candidate['unassigned_block_parts']['key']] = $candidate['unassigned_block_parts']['no_block_part'];
            $this->addThirdPartBlockToTimeline($candidate, $current_printer_time_line , $unassigned, $minutesFlex);
            
            $unassigned[$key]=$block;
            unset ($this->no_block_parts[$candidate['unassigned_block_parts']['key']]);
        }
        
        foreach ($unassigned as $key => $block){
            unset($this->blocks_parts_matrix[$key] );
            
        }
        
        return $unassigned_block_parts;
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// A D D - T H I R D - P A R T - T O - B L O C K ///////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function addThirdPartToBlock( array $block_parts,int  $day_minutes_2_min, int $minutesFlex,DateTimeG $start_block, int $limit_hours= 60 ):array{
        $candidate = array();
        foreach($this->no_block_parts as $key_no_block_part => $no_block_part) {
            //echo '<'.$no_block_part['id'];
            //echo '-'.$block_parts[1]['id'].'>';
            if (($no_block_part ['id'] != $block_parts[1]['id'])){

                $no_block_part['key'] = $key_no_block_part;
                $elements=array($block_parts[0],$block_parts[1],$no_block_part);
                $permutations = array();
                permute($elements,$permutations);
                foreach ($permutations as $permutation){
                    $start_0 =  $start_block->createDateTime();
                    $start_0->modify("+" . conf_delays()['minutes_start'] . " minutes");
                    $end_0 = $start_0->estimatedPrintingEnd($permutation[0]);
                    if (!$end_0->isOverlapAtNightFlex($minutesFlex)){
                        $start_1 = $end_0->createDateTime();
                        $start_1->modify("+" . conf_delays()['minutes_start'] . " minutes");
                        $end_1 = $start_1->estimatedPrintingEnd($permutation[1]);
                        if (!$end_1->isOverlapAtNightFlex($minutesFlex)){
                            $start_2 = $end_1->createDateTime();
                            $start_2->modify("+" . conf_delays()['minutes_start'] . " minutes");
                            $end_2 = $start_2->estimatedPrintingEnd($permutation[2]);
                            if (!$end_2->isOverlapAtNightFlex($minutesFlex)){
                                $minutes_2 = $end_2->minuteOfTheDay();
                                $limit = $start_0->createDateTime();
                                //Para limitar que no se pongan muchas piezas grandes juntas
                                //y por lo tanto una impresora tenga mucha más carga que el resto, por lo tanto quede libre más tarde, retrasando el final.
                                $limit->modify('+'.$limit_hours.' hours');
                                if (($minutes_2 < $day_minutes_2_min)&& ($end_2 < $limit)){
                                    $day_minutes_2_min = $minutes_2;
                                    
                                    $permutation[0]['start']     = $start_0;
                                    $permutation[0]['end']       = $end_0;
                                    $permutation[1]['start']     = $start_1;
                                    $permutation[1]['end']       = $end_1;
                                    $permutation[2]['start']     = $start_2;
                                    $permutation[2]['end']       = $end_2;
                                    $candidate['permutation']   = $permutation;
                                    
                                    $candidate['unassigned_block_parts']['key'] =  $no_block_part['key'];
                                    $candidate['unassigned_block_parts']['no_block_part'] = $no_block_part;;
                                }
                            }
                        }
                    }
                    //var_dump($permutation);
                }//END foreach ($permutations as $permutation){
            }//END if ($no_block_part ['id'] != $block_parts['id']){}
        }//END foreach($this->no_block_parts as $key_no_block_part => $no_block_part) {
        
        return $candidate;
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////// A D D - T H I R D - P A R T - B L O C K - T O - T I M E L I N E ////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    function addThirdPartBlockToTimeline(array $candidate, PrinterTimeline $current_printer_time_line,array &$unassigned,int $minutesFlex)
    {
        if ($minutesFlex == 0){
            
            $estimated_printing['start'] = $candidate['permutation'][0]['start'];
            $estimated_printing['end'] =$candidate['permutation'][0]['end'];
            $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $candidate['permutation'][0]);
            
            $estimated_printing['start'] = $candidate['permutation'][1]['start'];
            $estimated_printing['end'] =  $candidate['permutation'][1]['end'];
            $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $candidate['permutation'][1]);
            
            $estimated_printing['start'] =  $candidate['permutation'][2]['start'];
            $estimated_printing['end'] =  $candidate['permutation'][2]['end'];
            $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $candidate['permutation'][2]);
            
        }else{
            $estimated_printing['start'] = $candidate['permutation'][0]['start'];
            $estimated_printing['end'] =$candidate['permutation'][0]['end'];
            $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $candidate['permutation'][0]);
            if ($estimated_printing['end']->isOverlapAtNightFlex()){
                $night = $estimated_printing['end']->OverlapAtNight();
                $estimated_printing['start'] = $night['night_to']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                $estimated_printing['end'] = $estimated_printing['start']->estimatedPrintingEnd($candidate['permutation'][1]);
                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $candidate['permutation'][1]);
                if ($estimated_printing['end']->isOverlapAtNightFlex()){
                    $night = $estimated_printing['end']->OverlapAtNight();
                    $estimated_printing['start'] = $night['night_to']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                }else{
                    $estimated_printing['start'] =  $estimated_printing['end']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                }
                $estimated_printing['end'] = $estimated_printing['start']->estimatedPrintingEnd($candidate['permutation'][2]);
                if (!$estimated_printing['end']->isOverlapAtNightFlex($minutesFlex)){
                    $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $candidate['permutation'][2]);
                }
                
            }else{
                $estimated_printing['start'] = $candidate['permutation'][1]['start'];
                $estimated_printing['end'] =  $candidate['permutation'][1]['end'];
                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $candidate['permutation'][1]);
                if ($estimated_printing['end']->isOverlapAtNightFlex()){
                    $night = $estimated_printing['end']->OverlapAtNight();
                    $estimated_printing['start'] = $night['night_to']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                }else{
                    $estimated_printing['start'] =  $estimated_printing['end']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                }
                $estimated_printing['end'] = $estimated_printing['start']->estimatedPrintingEnd($candidate['permutation'][2]);
                if (!$estimated_printing['end']->isOverlapAtNightFlex($minutesFlex)){
                    $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $candidate['permutation'][2]);
                }
            }
        }
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////  R E A S S I G N - B L O C K - T O - P R I N T E R - E N D - S M A L L - T O - I M P R O V E - P E R F O R M A N C E ///////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function reassignPrintersSlotsEndSmallToPerformance(){
        $relation = 2/3;
        foreach ($this as $printerTimeline){
            $printer_slot_aux  = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
            $printer_slot_swap = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
            //Vamos a recorrenos la timeline en sentido inverso 
            $printerTimeline->uasort('startDescendingComparison');
            $first_iteration=true;
            $gaps_with_slots = array();
            if ($printerTimeline->percent($this->start_date_time) < 90){
                foreach ($printerTimeline as $printer_slot){
                    
                    $gap_end =   new DateTimeG($printer_slot_aux->start);
                    $gap_start = new DateTimeG($printer_slot->end);
                    
                    
                    $slot_start =  new DateTimeG($printer_slot->start);
                    $slot_end   =  new DateTimeG($printer_slot->end);
                    
                    if ($first_iteration){
                        $first_iteration = false;
                        
                        $workday_minutes = $slot_start->WorkDay()['start']->diffMinutes($slot_end->WorkDay()['end']);
                        if ($workday_minutes * $relation > $slot_start->diffMinutes( $slot_end)){
                            $printer_slot_swap->update($printer_slot) ;
                        }else{
                            break 2;
                        }
                        continue;
                    }
                    
                    //$workday_minutes_aux = $printer_slot_aux->start->WorkDay()['start']->diffMinutes($printer_slot_aux->end->WorkDay()['end']);
                    $gap_minutes = $gap_start->diffMinutes($gap_end);
                    $night_minutes = $gap_start->night()['from']->diffMinutes($gap_start->night()['to']);
                    
                    if ($gap_minutes > ($night_minutes * $relation)){
                    //El hueco es lo sumicientemente grande como para intentar reducirlo
                        $gap_with_slots = array();
                        $gap_with_slots['slot_start'] = $printer_slot;
                        $gap_with_slots['gap_start'] = $gap_start;
                        $gap_with_slots['gap_end'] = $gap_end;
                        $gap_with_slots['slot_end'] = $printer_slot_aux;
                        array_push($gaps_with_slots,$gap_with_slots);
                        
                    }
                    $printer_slot_aux->update($printer_slot) ;
                }
                usort( $gaps_with_slots, 'sort_by_gap_start_asc');
                foreach ($gaps_with_slots  as $gap_with_slots){
                    
                }
            }
            
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////  R E A S S I G N - B L O C K - T O - P R I N T E R - E N D - S M A L L - T O - I M P R O V E - P E R F O R M A N C E ///////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function reassignPrintersSlotsEndSwapToPerformance(){
        foreach ($this as $printerTimeline){
            $printer_slot_last  = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
            $printer_slot_penult = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
            //Vamos a recorrenos la timeline en sentido inverso
            $printerTimeline->uasort('startAscendingComparison');
            
            if ($printerTimeline->percent($this->start_date_time) < 90){
                
                $iterator = $printerTimeline->getIterator();
                if ($iterator->valid()){
                    $printer_slot_penult = $iterator->current();
                    $iterator->next();
                    
                    if ($iterator->valid()){
                        $printer_slot_last = $iterator->current();
                        $iterator->next();
                        while ($iterator->valid()){
                            $printer_slot_penult = $printer_slot_last;
                            $printer_slot_last = $iterator->current();
                            $iterator->next();
                        }
                    }else{
                        break;
                    }
                }else{
                    break;
                }
            }
            
            if (($printer_slot_last->part != '') &&  ($printer_slot_penult->part != '') && ($printer_slot_last->state == 8 )&& ($printer_slot_penult->state == 8 )){
               
                
                $penult_end   = new DateTimeG($printer_slot_penult->end);
                $last_start = new DateTimeG($printer_slot_last->start);
                
                $gap_minutes = $penult_end->diffMinutes($last_start);
                
                //Swap last with penault
                
                // penault_aux <- last
                $printer_slot_penult_aux = PrinterSlot::new($printer_slot_last);
                $penult_start_aux = new DateTimeG($printer_slot_penult->start);
                $penult_end_aux = $penult_start_aux->estimatedPrintingEndSlot( $printer_slot_penult_aux);
                $printer_slot_penult_aux->start = $penult_start_aux->format('Y-m-d H:i:s');
                $printer_slot_penult_aux->end = $penult_end_aux->format('Y-m-d H:i:s');
                
                // last_aux <- penault
                $printer_slot_last_aux = PrinterSlot::new($printer_slot_penult);
                $last_start_aux = $penult_end_aux;
                if ($last_start_aux->isOverlapAtNightFlex(5)){
                    $night = $last_start_aux->night();
                    $last_start_aux = $night['to'];
                }
                $last_start_aux =  $last_start_aux->modify("+" . conf_delays()['minutes_start'] . " minutes");
                $last_end_aux = $last_start_aux->estimatedPrintingEndSlot( $printer_slot_last_aux);
                $printer_slot_last_aux->start = $last_start_aux->format('Y-m-d H:i:s');
                $printer_slot_last_aux->end = $last_end_aux->format('Y-m-d H:i:s');
                
                
                // last_aux <- penault
                /*
                $printer_slot_last_aux = PrinterSlot::new($printer_slot_last);
                $last_start_aux = new DateTimeG($printer_slot_penult->start);
                $printer_slot_last_aux->start = $last_start_aux->format('Y-m-d H:i:s');
                $last_end_aux = $last_start_aux->estimatedPrintingEndSlot( $printer_slot_last_aux);
                $printer_slot_last_aux->end = $last_end_aux->format('Y-m-d H:i:s');
                */
                // penault_aux <- last
                /*
                $printer_slot_penult_aux = PrinterSlot::new($printer_slot_penult);
                
                if ($last_end_aux->isOverlapAtNightFlex(5)){
                    $night = $last_end_aux->night();
                    $last_end_aux = $night['to'];
                }
                
                $penult_start_aux =  $last_end_aux->modify("+" . conf_delays()['minutes_start'] . " minutes");
                $penult_end_aux = $penult_start_aux->estimatedPrintingEndSlot( $printer_slot_penult_aux);
                $printer_slot_penult_aux->start = $penult_start_aux->format('Y-m-d H:i:s');
                $printer_slot_penult_aux->end = $penult_end_aux->format('Y-m-d H:i:s');
                */
                $gap_minutes_aux = $penult_end_aux->diffMinutes($last_start_aux);
                
                //Si el hueco es menor con la nueva asignación se hace el swap
                if ($gap_minutes > $gap_minutes_aux){
                    $printer_slot_penult->update($printer_slot_penult_aux);
                    $printer_slot_last->update($printer_slot_last_aux);
                    //break ;
                }
            }
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////  R E A S S I G N - B L O C K - T O - P R I N T E R - 3 - F L E X //////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function reassignPrintersSlotsBlockFlex(){
        //Vamos a intentar colocar los bloques pendientes teniendo en cuenta las siguientes cuestiones:
        //Puede haber impresoras paradas:
        //-Pondremos el bloque en la impresora vacía de manera más flexible ($minutesFlex) hasta que pueda asignar.
        //Puede que se no esté asignado un bloque con piezas más grandes que otro que si que lo está:
        //-Intentaremos poner el bloque de manera mas flexible ($minutesFlex) en impresora que tiene piezas más faciles de colocar
        //--Esta impresora es la que tiene la pieza grande más pequeña y si hay varias iguales, la segunda pieza más grande que sea más pequeña
        //Puede que al asignar un bloque a impresora parada solo podamos asignar dos elementos, en cuyo caso mezclaremos las piezas de la citada asignación y un bloque
        $reverse_printer_count = count($this)-1;
        $is_necessary_mix_block = false;
        if ((isset($this->blocks_parts_matrix))&&(count($this->blocks_parts_matrix)>0)){
            usort($this->no_block_parts, 'sort_by_minutes_asc');
            $unassigned_blocks = array();
            foreach ($this->blocks_parts_matrix as $key_block => $block){
                //var_dump($block_parts);
                $printers_timeline_whitout_work =  $this->searchPrintersTimeLineWhitoutWork();
                $start_date_time_block = $this->start_date_time->createDateTime();
                if (count($printers_timeline_whitout_work) != 0){
                    //Si hay impresoras paradas
                    
                    $this->reassignPrintersSlotsBlockFlexPrintersStopped($block, $key_block, $printers_timeline_whitout_work[0],
                                                                        $unassigned_blocks, $start_date_time_block, 
                                                                        $is_necessary_mix_block, $reverse_printer_count);
                    
                }else{
                    
                    //Elijo la impresora que tiene la pieza más grande e inmeditamente más pequeña que la más grande del bloque que prentendo asignar:
                    //Por esto voy eliginedo las impresoras en orden inverso a su Id
                    if ($reverse_printer_count<0){
                        break;
                    }
                    $last_printer_timeline = $this[$reverse_printer_count--];
                    $last_printer_timeline->uasort('sort_timeline_by_slot_size_desc');
                    if ($last_printer_timeline->getIterator()->valid()){
                        
                        //Comprobamos si  hay que corregir la fecha de inicio debido a que hay una pieza imprimientdo
                        
                        $current_printer_time_line = PrinterTimeline::copy($last_printer_timeline);
                        $current_printer_time_line->uasort('startAscendingComparison');
                        $arrayIteratorPrinterAscending = $current_printer_time_line->getIterator();
                        $first_slot = $arrayIteratorPrinterAscending->current();

                        if ($first_slot->initiated == 1){
                            if (count($current_printer_time_line)==1){
                                break;
                            }
                            $start_date_time_block = new DateTimeG( $first_slot->end);
                        }
                        
                        
                        
                        
                         if ($is_necessary_mix_block){
                            $this->reassignPrintersSlotsBlockFlexNecessaryMixBlock($block,$key_block,$last_printer_timeline,$unassigned_blocks,$start_date_time_block);
                        }else{
                            $this->reassignPrintersSlotsBlockFlexLargerBigPart($block,$key_block,$last_printer_timeline,$unassigned_blocks,$start_date_time_block);
                        }
                    }
                }
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////
            //Quito los bloques que he asignado
            //Quizas no ha falta.
            foreach ($unassigned_blocks as $key => $block){
                unset($this->blocks_parts_matrix[$key] );
                
            }
            ////////////////////////////////////////////////////////////////////////////////////////////////
        }
        
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////  R E A S S I G N - B L O C K - T O - P R I N T E R -  F L E X - S T O P P E D//////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function reassignPrintersSlotsBlockFlexPrintersStopped($block,$key_block,$printers_timeline_whitout_work_0,&$unassigned_blocks,$start_date_time_block,&$is_necessary_mix_block,&$reverse_printer_count){
        /////////////////////////////////////////// Impresoras Vacias //////////////////////////////////////////////////
        //Si hay impresoras paradas
        $unassigned_block_parts = array();
        for ($minutesFlex=60;$minutesFlex<=300;$minutesFlex+=30){
            //TODO parametros en configuration.php
            $limit_hours = 72;
            $unassigned_block_parts = $this->assignBlockToPrinter3($block,$key_block,$printers_timeline_whitout_work_0['printer'],$unassigned_blocks,$start_date_time_block,$minutesFlex,$limit_hours);
            if (count($unassigned_block_parts)>0){
                if (count($printers_timeline_whitout_work_0['printer']) < 3){
                    //es necesario mezclar las piezas que hemos acabamos poner en la printer line con las de otro bloque, por ejemplo el de la siguiente iteración.
                    unset($this->blocks_parts_matrix[$key_block] );
                    $is_necessary_mix_block = true;
                }else{
                    $reverse_printer_count = --$printers_timeline_whitout_work_0['index'];
                }
                break;
            }
        }
    }

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////  R E A S S I G N - B L O C K - T O - P R I N T E R - N E C E S S A R Y - M I X - B L O C K //////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function reassignPrintersSlotsBlockFlexNecessaryMixBlock($block,$key_block,$last_printer_timeline,&$unassigned_blocks,$start_date_time_block){
        /////////////////////////////////////////////////////////// necessary_mix_block ////////////////////////////////
        $arrayIteratorLastPrinter =  $last_printer_timeline->getIterator();
        //El bloque estará formado por:
        //1. El slot grande: hay que añadirlo al bloque.
        //Además hay que quitar el slot pequeño de la printeline y añadirlo a no_block_parts
        $big_slot = $arrayIteratorLastPrinter->current();
        $big_slot_key = $arrayIteratorLastPrinter->key();
        $block [$big_slot->part] = $big_slot->toPendingPart();
        
        $arrayIteratorLastPrinter->next();
        $small_slot = $arrayIteratorLastPrinter->current();
        $small_slot_key = $arrayIteratorLastPrinter->key();
        array_push($this->no_block_parts,$small_slot->toPendingPart());
        
        unset($arrayIteratorLastPrinter[$big_slot_key]);
        unset($arrayIteratorLastPrinter[$small_slot_key]);
        if ($arrayIteratorLastPrinter->key() != null){
            unset($arrayIteratorLastPrinter[$arrayIteratorLastPrinter->key()]);
        }
        //2. Dejar la pieza grande del bloque, pero  poner la pequeña en no_block_parts y quitarla del bloque.
        //Quitar del bloque pequeño y añadirlo al no_block_parts
        $block_array_object = new ArrayObject( $block );
        $arrayIteratorBlock = $block_array_object->getIterator();
        $arrayIteratorBlock->next();
        $small_block_part = $arrayIteratorBlock->current();
        //$this->no_block_parts[$small_block_part['id']]= $small_block_part;
        array_push($this->no_block_parts,$small_block_part);
        unset($block[$small_block_part['id']]);
        //3. Una de la piezas de la sin asignar de no_block_parts
        for ($minutesFlex=60;$minutesFlex<=300;$minutesFlex+=30){
            //TODO parametros en configuration.php
            $limit_hours = 72;
            $unassigned_block_parts = $this->assignBlockToPrinter3($block,$key_block,$last_printer_timeline,$unassigned_blocks,$start_date_time_block,$minutesFlex,$minutesFlex,$limit_hours);
            if (count($unassigned_block_parts)>0){
                //TODO hay que quitarla de $this->no_block_parts
                break;
            }
        }
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////  R E A S S I G N - B L O C K - T O - P R I N T E R - N E C E S A R Y - M I X - B L O C K ////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function reassignPrintersSlotsBlockFlexLargerBigPart($block,$key_block,$last_printer_timeline,&$unassigned_blocks,$start_date_time_block){
        /////////////////////////////////////////////// reasignar bloque con una pieza grande más grande
        //Preparo los tres primeros slots
        $arrayIteratorLastPrinter =  $last_printer_timeline->getIterator();
        $slots[0] = $arrayIteratorLastPrinter->current();
        $arrayIteratorLastPrinter->next();
        $slots[1] = $arrayIteratorLastPrinter->current();
        $arrayIteratorLastPrinter->next();
        $slots[2] = $arrayIteratorLastPrinter->current();
        //Calculo el tiempo de las piezas del blocke sin asignar
        $block_array_object = new ArrayObject( $block );
        $arrayIteratorBlock = $block_array_object->getIterator();
        $block_part[0] = $arrayIteratorBlock->current();
        $block_part[0]['total_minutes'] = $block_part[0]['estimated_printing_hours']*60+$block_part[0]['estimated_printing_minutes'];
        $arrayIteratorBlock->next();
        $block_part[1] = $arrayIteratorBlock->current();
        //Si la pieza grande del slot es más pequeña que la grande del bloque.
        if ($slots[0]->minutes < $block_part[0]['total_minutes']){
            //Limpio La última printer timeline
            /*
            $count = count($last_printer_timeline);
            for ($i=0;$i<$count;$i++){
                unset($last_printer_timeline[$i]);
            }
            */
            $keys = array();
            foreach ($last_printer_timeline as $key =>$value){
                array_push($keys,$key);
            }
            foreach ($keys as $key){
                unset($last_printer_timeline[$key]);
            }
            //Quito del bloque la segunda pieza, que es la más pequeña.
            unset($block[$block_part[1]['id']]);
            //Actualizo $this->no_block_parts
            array_push($this->no_block_parts,$block_part[1]);
            //Asigno al bloque  el slot más grande que tenía la última impresora.
            $block [$slots[0]->part] = $slots[0]->toPendingPart();
            //TODO parametros en configuration.php
            $limit_hours = 72;
            for ($minutesFlex=60;$minutesFlex<=300;$minutesFlex+=30){
                $unassigned_block_parts = $this->assignBlockToPrinter3($block,$key_block,$last_printer_timeline,$unassigned_blocks,$start_date_time_block,$minutesFlex,$limit_hours);
                if (count($unassigned_block_parts)>0){
                    foreach($unassigned_block_parts as $unassigned_block_part){
                        array_push($this->no_block_parts,$unassigned_block_part);
                    }
                    
                    break;
                }
            }
            //echo "Point";
        }
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////// R E A S S I G N - P A R T S - A L L - S E N D - R E Q U E S T - I N I T - F O R M ////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    static function reassignPartsAllSendRequestInitForm($get_part){
        //Inicializamos el formulario con el id de la pieza seleccionada.
        $sql = DataBase::querySelectReassignPart($get_part);
        $part = DataBase::getConnection()->query($sql)->fetch_assoc();
        
        $start = new DateTimeG($part['start_datetime']);
        $end = new DateTimeG($part['end_datetime']);
        
        $min = new DateTimeG($start->format('Y-m-d H:i:s'));
        $min->modify('-1 years');
        
        $max =  new DateTimeG($start->format('Y-m-d H:i:s'));
        $max->modify('+10 years');
        
        $slot_interval = $start->diff($end);
        $slot_total_hours = $slot_interval->format('%d') * 24 + $slot_interval->format('%h');
        
        $sql = sprintf('
            SELECT  *
            FROM    printer AS p
            WHERE   available = 1
            ORDER BY id
            ;
        ');
        $printers = DataBase::getConnection()->query($sql);

        $sql = sprintf('
            SELECT  *
            FROM    state AS s
            WHERE   state_id_type = 3
            ORDER BY id
            ;
        ');
        $states = DataBase::getConnection()->query($sql);
        return array('min'=>$min,'max'=>$max,'start'=>$start,'end'=>$end,'part'=>$part,
                    'slot_total_hours'=>$slot_total_hours,'slot_interval'=>$slot_interval,
                    'printers'=>$printers,'states'=>$states);
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////// R E A S S I G N - P A R T S - A L L //////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    function reassignPartsAll($start_datetime,$action, $neighborhood_factory_id,$get_part_id){
        //Apartar las unassinged: state 8->7
        $this->separatedUnassignedParts();
        
        //$this->chargePrintersMatrix($start_datetime);
        
        //Solo se cargan los asignados como desasignados: 9 -> 8. Y se guarda la asignaci�n de los iniciados
        //Antes de cargar hay que borrar la PrinterMatix
        $this->prepareToReassignPartsAll($start_datetime,$action,$get_part_id);
        
        //$this->clearAssignPrintersMatrix();
        
        //$this->chargePrintersMatrix($start_datetime);
        
        $this->assignPrintersSlots();
        $this->updateAssignPrintersMatrix($action,$get_part_id);
        $this->recoverSeparatedUnssignedParts();
        //$this->chargePrintersMatrix();
        
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////// P R E P A R E - T O - R E A S S I G N - P A R T S - A L L ////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    function prepareToReassignPartsAll($start_datetime,$action,$id_part){
        //Hay que recupera la pieza de la bdd, para disponer del tiempo exacto, no el de la timeline
        $sql = DataBase::querySelectReassignPart($id_part,$this->neighborhood_factory_id);
        $part = DataBase::getConnection()->query($sql)->fetch_assoc();
        
        foreach ($this as $printerTimeline){
            
            $is_last_slot_out_range = false;
            
            if
            (
              (isset($printerTimeline->last_slot_out_range->part))  
              &&
              ($printerTimeline->last_slot_out_range->part == $id_part)
              &&
              (
                (($printerTimeline->last_slot_out_range->initiated == 0) && ($action == "play"))
                ||
                (($printerTimeline->last_slot_out_range->initiated == 1) && ($action == "cancel"))
              )
            ){
                $this->updatePartByAction($part,$action, $printerTimeline->last_slot_out_range,$printerTimeline,$start_datetime);
                
                $is_last_slot_out_range = true;
            }
            foreach ($printerTimeline as $slot){
                //Si no se ha iniciado su impresion y no es la pieza que estamos arrancando
                if ((($slot->initiated == 0) && ($slot->part <> $id_part))||( $is_last_slot_out_range)){
                //if (($action == "cancel") &&  ($slot->initiated == 0)){
                    //Desasignamos la pieza
                    $slot->state = 8;
                    $slot->printer_id = NULL;
                    $slot->start = NULL;
                    $slot->end = NULL;
                    
                    $sql = sprintf("UPDATE part
                    SET state_id = 8 ,
                        printer_id = NULL, start_datetime = NULL, end_datetime = NULL
                    WHERE id = " . $slot->part);
                    $result = DataBase::getConnection()->query($sql);
                    
                }elseif 
                  (
                    ($slot->part == $id_part)
                    &&
                    (  
                        (($slot->initiated == 0) && ($action == "play")) 
                        ||
                        (($slot->initiated == 1) && ($action == "cancel"))
                    )
                  )
                {
                //}elseif (($action == "play") && ($slot->part == $id_part)){
                    $this->updatePartByAction($part,$action, $slot,$printerTimeline,$start_datetime);
                }
            }
        }
        //Limpio la printer matrix:
        //Si es Play: Sólo dejo asignadas las que se están imprimiendo y la que acabo de arrancar
        //Si es Cancel: Sólo dejo asignadas las que están imprimiendo
        if ($action == "play"){
            foreach($this as $printerTimeLine){
                $delete_keys = array();
                foreach ($printerTimeLine as $key_printerSlot=>$printerSlot){
                    if (!(($printerSlot->part == $id_part )||($printerSlot->initiated ==1))){
                        array_push($delete_keys,$key_printerSlot);
                    }
                }
                foreach ($delete_keys as $key){
                    unset($printerTimeLine[$key]);
                }
                
            }
        }elseif ($action == "cancel"){
            foreach($this as $printerTimeLine){
                $delete_keys = array();
                foreach ($printerTimeLine as $key_printerSlot=>$printerSlot){
                    if ($printerSlot->initiated !=1){
                        array_push($delete_keys,$key_printerSlot);
                    }
                }
                foreach ($delete_keys as $key){
                    unset($printerTimeLine[$key]);
                }
                
            }
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////// U P D A T E - P A R T - B Y - A C T I O N  ///////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function updatePartByAction($part,$action, $slot,$printerTimeline,$start_datetime){
        //Falta Asignar la nueva pieza a partir de la fecha actual.
        
        
        $initiated = 1;
        $state_id = 9;
        if ($action == "cancel"){
            $initiated = 0;
            $state_id = 8;
        }
        $slot->state = $state_id;
        $slot->initiated = $initiated;
        
        $start = $start_datetime->createDateTime();
        $end = $start->estimatedPrintingEnd($part);
        
        if ($action == "cancel"){
            $slot->start = "NULL";
            $slot->end = "NULL";
            
            $sql = sprintf('UPDATE part
                            SET state_id = '.$slot->state.' , initiated = '.$slot->initiated.' , start_datetime ='.$slot->start.', end_datetime = '.$slot->end.'
                            WHERE id = ' . $slot->part);
            $result = DataBase::getConnection()->query($sql);
            
        }else{
            $slot->start = $start->format('Y-m-d H:i:s');
            $slot->end = $end->format('Y-m-d H:i:s');
            
            //Poner a OK la pieza anterior: state = 11
            
            if ((isset($printerTimeline->last_slot_out_range->part)) && ($slot->part != $printerTimeline->last_slot_out_range->part)){
                $sql = sprintf('UPDATE part
                                SET state_id = 10
                                WHERE id = ' . $printerTimeline->last_slot_out_range->part);
                $result = DataBase::getConnection()->query($sql);
            }
            
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////// BLOCKS - P E N D I N G - M I X ///////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    function assignPrintersSlotsBlockPendingMix(int $minutesFlex=0,int $limit_hours= 60)
    {
        //Intento montar otros bloques mezclando las partes de los bloques que no se han podido asignar con las piezas no están en los bloques
        //Aparentemente no mejora nada.
        if (isset($this->blocks_parts_matrix)){
            $unassigned = array();
            foreach ($this->blocks_parts_matrix as $key_block_ini => $block_ini){
                $current_printer_time_line = $this->searchPrinterTimeLineMoreIdle();
                $arrayIteratorPrinter = $current_printer_time_line->getIterator();
                if (! $arrayIteratorPrinter->valid()){
                    $block_parts_ini = array();
                    //Cargo solo las partes del bloque, los minutos me estorban
                    foreach ($block_ini as $key_block_part_ini => $block_part_ini){
                        if ($key_block_part_ini != "minutes"){
                            array_push($block_parts_ini,$block_part_ini);
                        }
                    }
                    
                    //Borro la segunda pieza del bloque para crear uno nuevo sin ella, con las piezas que no están en bloques 
                    $part_ini_aux = $block_ini[$block_parts_ini[1]['id']];
                    unset($block_ini[$block_parts_ini[1]['id']]);
                    foreach($this->no_block_parts as $no_block_part) {
                        $block_ini[$no_block_part['id']] = $no_block_part;
                        $unassigned_block_parts = $this->assignBlockToPrinter3($block_ini,$key_block_ini,$current_printer_time_line,$unassigned,$minutesFlex,$limit_hours);
                        if (count($unassigned_block_parts)==0){
                            unset( $block_ini[$no_block_part['id']]);
                        }else{
                            break;
                        }
                    }
                    //Si no he conseguido nada, Borro la primera pieza del bloque para crear uno nuevo sin ella, con las piezas que no están en bloques 
                    if (count($unassigned_block_parts)==0){
                        unset($block_ini[$block_parts_ini[0]['id']]);
                        $block_ini[$part_ini_aux['id']] = $part_ini_aux;
                        foreach($this->no_block_parts as $no_block_part) {
                            $block_ini[$no_block_part['id']] = $no_block_part;
                            $unassigned_block_parts = $this->assignBlockToPrinter3($block_ini,$key_block_ini,$current_printer_time_line,$unassigned,$minutesFlex,$limit_hours);
                            if (count($unassigned_block_parts)==0){
                                unset( $block_ini[$no_block_part['id']]);
                            }else{
                                break;
                            }
                        }
                    }
                    
                    if (count($unassigned_block_parts)==0){
                        //unset($this->blocks_parts_matrix[$key_block_ini]);
                    }
                }
            }
            
            foreach ($unassigned as $key => $block){
                unset($this->blocks_parts_matrix[$key] );
                
            }
            
            usort($this->no_block_parts, 'sort_by_minutes_asc');
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////// M I S S I N G - P A R T S ////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function assignPrintersSlotsMissingParts()
    {
        $this->calculateMissingPrintersSlots();
        
        if (isset($this->missing_parts)){
            usort($this->missing_parts, 'sort_by_hours_desc');
            foreach ($this->missing_parts as $part){
                $current_printer_time_line = $this->searchPrinterTimeLineFinishFirst();
                $current_printer_time_line->uasort('startDescendingComparison');
                
                $arrayIteratorPrinter = $current_printer_time_line->getIterator();
                
                if ($arrayIteratorPrinter->valid()){
                    $printerSlotFinal = $arrayIteratorPrinter->current();
                    $start = DateTimeG::newDateTime($printerSlotFinal->start);
                    $estimated_printing = array();
                    if ($start->isOverlapAtNightFlex()){
                        $night = $start->OverlapAtNight();
                        $estimated_printing['start'] =  $night['night_to']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                        
                    }else{
                        $estimated_printing['start'] =  $start->modify("+" . conf_delays()['minutes_start'] . " minutes");
                    }
                    $estimated_printing['end'] =  $estimated_printing['start']->estimatedPrintingEnd($part);
                    if (!$estimated_printing['end']->isOverlapAtNightFlex(60)){
                        $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $part);
                    }
                    
                }
            }
        }
    }
    

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////// M I S S I N G - P A R T S - F R E E //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    function assignPrintersSlotsMissingPartsFree()
    {
        $this->calculateMissingPrintersSlots();
        
        if (isset($this->missing_parts)){
            usort($this->missing_parts, 'sort_by_hours_desc');
            foreach ($this->missing_parts as $part){
                $current_printer_time_line = $this->searchPrinterTimeLineFinishFirst();
                $current_printer_time_line->uasort('startDescendingComparison');
                
                $arrayIteratorPrinter = $current_printer_time_line->getIterator();
                
                $start = DateTimeG::newDateTime($this->start_date_time);
                if ($arrayIteratorPrinter->valid()){
                    $printerSlotFinal = $arrayIteratorPrinter->current();
                    $start = DateTimeG::newDateTime($printerSlotFinal->end);
                }
                    
                
                $estimated_printing = array();
                if ($start->isOverlapAtNightFlex()){
                    $night = $start->OverlapAtNight();
                    $estimated_printing['start'] =  $night['night_to']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                    
                }else{
                    $estimated_printing['start'] =  $start->modify("+" . conf_delays()['minutes_start'] . " minutes");
                }
                $estimated_printing['end'] =  $estimated_printing['start']->estimatedPrintingEnd($part);
                get_object_vars($current_printer_time_line);
                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $part);
                
                
                
                unset($this->no_block_parts[$part['id']]);
                //Borrar de los bloques las piezas que vamos asignando. 
                if (isset($this->blocks_parts_matrix)  && count ($this->blocks_parts_matrix)>0){
                    foreach ($this->blocks_parts_matrix as $key_block =>$block){
                        foreach ($block as $key_block_part =>$block_part){
                            if (isset($block_part['id'])&&($part['id'] == $block_part['id'])){
                                unset($this->blocks_parts_matrix[$key_block][$key_block_part]);
                            }
                        }
                        
                    }
                    //Si borro todas las piezas de un bloque tambien borro el bloque
                    foreach ($this->blocks_parts_matrix as $key_block =>$block){
                        //El 1 es porque nos queda el elemento ['minutes']
                        if (count($block)==1){
                            unset($this->blocks_parts_matrix[$key_block]);
                        }
                    }
                }
                
            }
        }
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////  C R E A T E - B L O C K S - M A T R I X - 2 //////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function createBlocksMatrix2(array $block_sizes){
    //Creamos la matriz de bloques
        $blocks_count = 0;
        if (isset($this->parts)){
            foreach ($this->parts as $key_part => $part){
                foreach ($this->no_block_parts as $key_no_block_part => $no_block_part){
                    if ($key_part == $key_no_block_part){
                        //unset($this->no_block_parts[$key_part]);
                        continue;
                    }
                    
                    $break = false;
                    if (isset($this->blocks_parts_matrix)){
                        foreach ($this->blocks_parts_matrix as $block){
                            //if (array_key_exists( $part['id'], $block)){
                            if ((array_key_exists( $no_block_part['id'], $block))||(array_key_exists( $part['id'], $block))){
                                $break = true;
                                break;
                            }
                        }
                        if($break) break;
                    }
                    
                    $part_minutes =  $part['estimated_printing_hours']*60+$part['estimated_printing_minutes'];
                    $no_block_part_minutes = $no_block_part['estimated_printing_hours']*60+$no_block_part['estimated_printing_minutes'];
                    $total_minutes = $part_minutes + $no_block_part_minutes;
                    
                    if ((in_array(48, $block_sizes)) && ($part['estimated_printing_hours'] > 24) && ($total_minutes+20 >= 44*60) && ($total_minutes+20 < 49*60)){
                        //Creo un nuevo bloque con las dos piezas
                        $this->blocks_parts_matrix[$blocks_count][$key_part]= $part;
                        $this->blocks_parts_matrix[$blocks_count][$key_no_block_part]= $no_block_part;
                        $this->blocks_parts_matrix[$blocks_count]['minutes']= $total_minutes+20;
                        
                        //Quito las piezas del vector de sin bloque
                        unset($this->no_block_parts[$key_part]);
                        unset($this->no_block_parts[$key_no_block_part]);
                        
                        $blocks_count++;
                        break;
                        
                    }
                    
                    if ((in_array(24, $block_sizes)) && ($part['estimated_printing_hours'] <= 24) && ($total_minutes+20 >= 22*60) && ($total_minutes+20 < 24*60))
                    {
                        //Creo un nuevo bloque con las dos piezas
                        $this->blocks_parts_matrix[$blocks_count][$key_part]= $part;
                        $this->blocks_parts_matrix[$blocks_count][$key_no_block_part]= $no_block_part;
                        $this->blocks_parts_matrix[$blocks_count]['minutes']= $total_minutes+20;
                        //Quito las piezas del vector de sin bloque
                        unset($this->no_block_parts[$key_part]);
                        unset($this->no_block_parts[$key_no_block_part]);
                        
                        $blocks_count++;
                        break;
                    }
                    
                }//END foreach ($this->no_block_parts as $key_no_block_part => $no_block_part){
            }//END foreach ($this->parts as $key_part => $part){
        }//END if (isset($this->parts)){
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////  A S S I G N - B L O C K - T O - P R I N T E R - 2 ////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignBlockToPrinter2OLD(array $block,int $key, PrinterTimeline $current_printer_time_line, array &$unassigned){
        
        //Asignar Bloque a la impresora actual: Printer Block, Printer Line
        $block_parts = array();
        foreach ($block as $block_part){
            array_push($block_parts,$block_part);
        }
        
        $start_0 =  $this->start_date_time->createDateTime();
        $start_0->modify("+" . conf_delays()['minutes_start'] . " minutes");
        $end_0 = $start_0->estimatedPrintingEnd($block_parts[0]);
        
        
        if (!$end_0->isOverlapAtNightFlex()){
            //Intento asignar en el orden de llegada
            $start_1 = $end_0->createDateTime();
            $start_1->modify("+" . conf_delays()['minutes_start'] . " minutes");
            $end_1 = $start_1->estimatedPrintingEnd($block_parts[1]);
            if (!$end_1->isOverlapAtNightFlex()){
                //Se asignan en el orden que llegan
                $estimated_printing['start'] = $start_0;
                $estimated_printing['end'] = $end_0;
                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[0]);
                
                $estimated_printing['start'] = $start_1;
                $estimated_printing['end'] = $end_1;
                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[1]);
                
                $unassigned[$key]=$block;
            }
        }else{
            //Intento asignar en el orden inverso al de llegada
            $end_0 = $start_0->estimatedPrintingEnd($block_parts[1]);
            if (!$end_0->isOverlapAtNightFlex()){
                $start_1 = $end_0->createDateTime();
                $start_1->modify("+" . conf_delays()['minutes_start'] . " minutes");
                $end_1 = $start_1->estimatedPrintingEnd($block_parts[0]);
                if (!$end_1->isOverlapAtNightFlex()){
                    //Se asignan en el orden inverso al que llegan
                    $estimated_printing['start'] = $start_0;
                    $estimated_printing['end'] = $end_0;
                    $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[1]);
                    
                    $estimated_printing['start'] = $start_1;
                    $estimated_printing['end'] = $end_1;
                    $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[0]);
                    
                    $unassigned[$key]=$block;
                }
            }
        }
    }
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////  A S S I G N - B L O C K - T O - P R I N T E R - 3 - O L D ////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    public function assignBlockToPrinter3OLD(array $block,int $key, PrinterTimeline $current_printer_time_line, array &$unassigned):array
    {
        $block_parts = array();
        foreach ($block as $key_aux => $block_part){
            if ($key_aux != "minutes"){
                array_push($block_parts,$block_part);
            }
        }
        
        $unassigned_block_parts = array();
        foreach($this->no_block_parts as $key_no_block_part => $no_block_part) {
            $start_0 =  $this->start_date_time->createDateTime();
            $start_0->modify("+" . conf_delays()['minutes_start'] . " minutes");
            $end_0 = $start_0->estimatedPrintingEnd($no_block_part);
            
            if (!$end_0->isOverlapAtNightFlex()){
                //Es C??
                $start_1 = $end_0->createDateTime();
                $start_1->modify("+" . conf_delays()['minutes_start'] . " minutes");
                $end_1 = $start_1->estimatedPrintingEnd($block_parts[0]);
                
                if (!$end_1->isOverlapAtNightFlex()){
                    //Es CA?
                    $start_2 = $end_1->createDateTime();
                    $start_2->modify("+" . conf_delays()['minutes_start'] . " minutes");
                    $end_2 = $start_2->estimatedPrintingEnd($block_parts[1]);
                    if (!$end_2->isOverlapAtNightFlex()){
                        //Es CAB
                        //Se asignan
                        $estimated_printing['start'] = $start_0;
                        $estimated_printing['end'] = $end_0;
                        $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $no_block_part);
                        
                        $estimated_printing['start'] = $start_1;
                        $estimated_printing['end'] = $end_1;
                        $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[0]);
                        
                        $estimated_printing['start'] = $start_2;
                        $estimated_printing['end'] = $end_2;
                        $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[1]);
                        
                        $unassigned[$key]=$block;
                        $unassigned_block_parts[$key_no_block_part]= $no_block_part;
                        break;
                    }else{
                        $start_1 = $end_0->createDateTime();
                        $start_1->modify("+" . conf_delays()['minutes_start'] . " minutes");
                        $end_1 = $start_1->estimatedPrintingEnd($block_parts[1]);
                        if (!$end_1->isOverlapAtNightFlex()){
                            //Es CB?
                            $start_2 = $end_1->createDateTime();
                            $start_2->modify("+" . conf_delays()['minutes_start'] . " minutes");
                            $end_2 = $start_2->estimatedPrintingEnd($block_parts[1]);
                            if (!$end_2->isOverlapAtNightFlex()){
                                //Es CBA
                                //Se asignan
                                $estimated_printing['start'] = $start_0;
                                $estimated_printing['end'] = $end_0;
                                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $no_block_part);
                                
                                $estimated_printing['start'] = $start_1;
                                $estimated_printing['end'] = $end_1;
                                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[1]);
                                
                                $estimated_printing['start'] = $start_2;
                                $estimated_printing['end'] = $end_2;
                                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[0]);
                                
                                $unassigned[$key]=$block;
                                $unassigned_block_parts[$key_no_block_part]= $no_block_part;
                                break;
                                
                            }
                        }
                        
                    }
                }
                
            }else{
                // Es A?? o B??
                $start_0 =  $this->start_date_time->createDateTime();
                $start_0->modify("+" . conf_delays()['minutes_start'] . " minutes");
                $end_0 = $start_0->estimatedPrintingEnd($block_parts[0]);
                if (!$end_0->isOverlapAtNightFlex()){
                    //Es A??
                    $start_1 = $end_0->createDateTime();
                    $start_1->modify("+" . conf_delays()['minutes_start'] . " minutes");
                    $end_1 = $start_1->estimatedPrintingEnd($no_block_part);
                    if (!$end_1->isOverlapAtNightFlex()){
                        //Es AC?
                        $start_2 = $end_1->createDateTime();
                        $start_2->modify("+" . conf_delays()['minutes_start'] . " minutes");
                        $end_2 = $start_2->estimatedPrintingEnd($block_parts[1]);
                        if (!$end_2->isOverlapAtNightFlex()){
                            //Es ACB
                            //Se asigna
                            $estimated_printing['start'] = $start_0;
                            $estimated_printing['end'] = $end_0;
                            $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[0]);
                            
                            $estimated_printing['start'] = $start_1;
                            $estimated_printing['end'] = $end_1;
                            $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $no_block_part);
                            
                            $estimated_printing['start'] = $start_2;
                            $estimated_printing['end'] = $end_2;
                            $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[1]);
                            
                            $unassigned[$key]=$block;
                            $unassigned_block_parts[$key_no_block_part]= $no_block_part;
                            break;
                            
                        }
                    }else{
                        $start_1 = $end_0->createDateTime();
                        $start_1->modify("+" . conf_delays()['minutes_start'] . " minutes");
                        $end_1 = $start_1->estimatedPrintingEnd($block_parts[1]);
                        if (!$end_1->isOverlapAtNightFlex()){
                            //Es AB?
                            $start_2 = $end_1->createDateTime();
                            $start_2->modify("+" . conf_delays()['minutes_start'] . " minutes");
                            $end_2 = $start_2->estimatedPrintingEnd($no_block_part);
                            if (!$end_2->isOverlapAtNightFlex()){
                                //Es ABC
                                //ASe asigna
                                $estimated_printing['start'] = $start_0;
                                $estimated_printing['end'] = $end_0;
                                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[0]);
                                
                                $estimated_printing['start'] = $start_1;
                                $estimated_printing['end'] = $end_1;
                                $current_printer_time_line->addSlotAndSortAsc($estimated_printing,$block_parts[1]);
                                
                                $estimated_printing['start'] = $start_2;
                                $estimated_printing['end'] = $end_2;
                                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $no_block_part );
                                
                                $unassigned[$key]=$block;
                                $unassigned_block_parts[$key_no_block_part]= $no_block_part;
                                break;
                            }
                        }
                    }
                }else{
                    $start_0 =  $this->start_date_time->createDateTime();
                    $start_0->modify("+" . conf_delays()['minutes_start'] . " minutes");
                    $end_0 = $start_0->estimatedPrintingEnd($block_parts[1]);
                    if (!$end_0->isOverlapAtNightFlex()){
                        //Es B??
                        $start_1 = $end_0->createDateTime();
                        $start_1->modify("+" . conf_delays()['minutes_start'] . " minutes");
                        $end_1 = $start_1->estimatedPrintingEnd($no_block_part);
                        if (!$end_1->isOverlapAtNightFlex()){
                            //Es BC?
                            $start_2 = $end_1->createDateTime();
                            $start_2->modify("+" . conf_delays()['minutes_start'] . " minutes");
                            $end_2 = $start_2->estimatedPrintingEnd($block_parts[0]);
                            if (!$end_2->isOverlapAtNightFlex()){
                                //Es BCA
                                //Se asignan
                                $estimated_printing['start'] = $start_0;
                                $estimated_printing['end'] = $end_0;
                                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[1]);
                                
                                $estimated_printing['start'] = $start_1;
                                $estimated_printing['end'] = $end_1;
                                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $no_block_part);
                                
                                $estimated_printing['start'] = $start_2;
                                $estimated_printing['end'] = $end_2;
                                $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[0]);
                                
                                $unassigned[$key]=$block;
                                $unassigned_block_parts[$key_no_block_part]= $no_block_part;
                                break;
                                
                            }else{
                                
                            }
                        }else{
                            $start_1 = $end_0->createDateTime();
                            $start_1->modify("+" . conf_delays()['minutes_start'] . " minutes");
                            $end_1 = $start_1->estimatedPrintingEnd($block_parts[0]);
                            if (!$end_1->isOverlapAtNightFlex()){
                                //es BA?
                                $start_2 = $end_1->createDateTime();
                                $start_2->modify("+" . conf_delays()['minutes_start'] . " minutes");
                                $end_2 = $start_2->estimatedPrintingEnd($no_block_part);
                                if (!$end_2->isOverlapAtNightFlex()){
                                    //Es BAC
                                    //Se asignan
                                    $estimated_printing['start'] = $start_0;
                                    $estimated_printing['end'] = $end_0;
                                    $current_printer_time_line->addSlotAndSortAsc($estimated_printing, $block_parts[1]);
                                    
                                    $estimated_printing['start'] = $start_1;
                                    $estimated_printing['end'] = $end_1;
                                    $current_printer_time_line->addSlotAndSortAsc($estimated_printing,$block_parts[0] );
                                    
                                    $estimated_printing['start'] = $start_2;
                                    $estimated_printing['end'] = $end_2;
                                    $current_printer_time_line->addSlotAndSortAsc($estimated_printing,$no_block_part );
                                    
                                    $unassigned[$key]=$block;
                                    $unassigned_block_parts[$key_no_block_part]= $no_block_part;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $unassigned_block_parts;
    }
    
    
    
    
    
    
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////// S L O T S - O R I G I N A L /////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsOringinal()
    {
        
        //TODO Falta guardarlos en bdd pero lo tiene que hacer en la función addSlotToPrinter()
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //TODO Comprobar si el pedido ya tiene creadas Parts???????????????????????
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////// Carga de BDD //////////////////////////////////////
        $sql = sprintf('
                SELECT  *
                FROM    printer AS pri
                WHERE   available = 1 AND neighborhood_factory_id = ' . $this->neighborhood_factory_id . '
                ORDER BY pri.id;
               ');
        $available_printers = DataBase::getConnection()->query($sql);
        $available_printers_aux = DataBase::getConnection()->query($sql);
        
        /*
         $arrayPrintersIterator = $available_printers->getIterator();
         while( $arrayPrintersIterator->valid()) {
         //if( $arrayIterator->valid()) {
         $printer = $arrayPrintersIterator->current();
         $arrayPrintersIterator->next();
         }
         */
        $sql = sprintf('
            SELECT *
            FROM  time_without_operators AS two
            WHERE neighborhood_factory_id  = ' . $this->neighborhood_factory_id . ' AND active = 1
            ORDER BY day;');
        
        //$time_without_operator = DataBase::getConnection()->query($sql);
        
        $sql = sprintf('
            SELECT  p.id,p.file_id,p.file_part_name,p.version_part_name,p.order_id,p.final_product_id,state_id,p.weight,
                    f.name as file_name,f.estimated_printing_hours,f.estimated_printing_minutes,f.filament_used,
                    p.start_datetime,p.initiated,
                    fp.product_id,pd.name as product_name
            FROM    part AS p
            INNER   JOIN file AS f           ON p.file_id = f.id
            INNER   JOIN final_product AS fp ON fp.id = p.final_product_id
            INNER   JOIN product AS pd       ON pd.id = fp.product_id 
            WHERE   state_id = 8
            ORDER BY   f.estimated_printing_hours DESC, p.order_id,p.final_product_id  ASC,  f.estimated_printing_minutes DESC, p.id;
        ');
        //ORDER BY p.weight DESC,p.order_id  ASC
        //DataBase::getConnection()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE);
        $pending_parts = DataBase::getConnection()->query($sql);
        $this->pending_parts_count = $pending_parts->num_rows;
        ////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////// Inicializo estados ////////////////////////////////
        //$day_week = 5;
        ////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////// Recorrer $pendig_part ////////////////////////////////////
        /*

        */
        
        $this->assignPrintersSlotsModule(1,$pending_parts, $available_printers);
        $module =1;
        /*
        $PrinterMatrix1 = PrintersMatrix::new($this);
        
        $PrinterMatrix2 = PrintersMatrix::new($this);
        */
       
        $this->calculateMissingPrintersSlots();
        
        $count1 = count($this->missing_parts);
        $count2=0;
        
        
        if (count($this->missing_parts)>0){
            $this->clear();
            $pending_parts = DataBase::getConnection()->query($sql);
            $this->assignPrintersSlotsModule (2,$pending_parts, $available_printers);
            $this->calculateMissingPrintersSlots();
            $count2 = count($this->missing_parts);
            //PrintersMatrix::copy($PrinterMatrix2 , $this);
            echo "Calculate Module 2<br>";
            $module ="2";
        }else{
            echo "Calculate Only Module 1. Missingparts = 0.<br>";;
        }
        
        //if (count($PrinterMatrix2->missing_parts) < count($PrintersMatrix1->missing_parts)){
        if ($count1 < $count2){
          //PrintersMatrix::copy($this, $PrinterMatrix2);
            $this->clear();
            $pending_parts = DataBase::getConnection()->query($sql);
            $this->assignPrintersSlotsModule(1,$pending_parts, $available_printers);
            //PrintersMatrix::copy($this,$PrinterMatrix1);
            $this->calculateMissingPrintersSlots();
            echo "Module 1 is better Modulo 2<br>";
            $module =1;
        }
        

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $this->calculateMissingPrintersSlots();
        

        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
     
        
        //$this->RemoveGapsToLeftAll($available_printers_aux, $pending_parts_processed);
        
        
        
        //Quitar huecos grandes moviendo de las piezas biggest y big
        return $module;
        
    }
    
    public function assignPrintersSlotsModule($module, $pending_parts, $available_printers){
        $pending_parts_processed = array();
        
        $this->assignPrintersSlotsMain($module,$pending_parts, $pending_parts_processed, $available_printers);
        
        $this->whileassignPrintersSlotsMain ($module,$pending_parts, $pending_parts_processed, $available_printers);
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        $this->assignPrintersSlotsUnassigned94Swap();
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $this->assignPrintersSlotsUnassigned92Gap();
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        $this->unassignedOutRangeSlots();
        
        
        //Si una pieza de entre 4-6h empieza muy cerca de la noche, hay que ponerla en otra impresora que tenga tenga un hueco.
        //El problema se plantea si no hay hueco: (para 4h uno de 3h y para uno de 6 uno de 5)
        //se puede intentar hacer mas grande un hueco similiar y deplazar las piezas de despues siemre y cuando no sean más de dos
        //$this->reassignPrintersSlotsNearLastNight();//TODO Funtion
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $this->NightOverlaps('reassign');
        //$this->assignPrintersSlotsMain($module,$pending_parts, $pending_parts_processed, $available_printers);
        //$this->whileassignPrintersSlotsMain ($module,$pending_parts, $pending_parts_processed, $available_printers);
        $this->NightOverlaps('swap');
        //$this->assignPrintersSlotsMain($module,$pending_parts, $pending_parts_processed, $available_printers);
        //$this->whileassignPrintersSlotsMain ($module,$pending_parts, $pending_parts_processed, $available_printers);
        $this->unassignedOutRangeSlots();
        //$this->unassignNightOverlaps($pending_parts); //TODO Revisar
        
        //$this->reassignPrintersSlotsNearNight93($pending_parts);
        //$this->whileassignPrintersSlotsMain ($module,$pending_parts, $pending_parts_processed, $available_printers);
        $this->clean();
        /**/
    }
    

    public function whileassignPrintersSlotsMain ($module,$pending_parts, &$pending_parts_processed, $available_printers){
        $count=0;
        if (isset ($this->missing_parts))
            $count_parts=count($this->missing_parts);
        else
            $count_parts = 0;
                
        if (isset($this->missing_parts)){
            //$count_unassigned = count($this->missing_parts);
            while (count($this->missing_parts)!=0){
                
                $this->assignPrintersSlotsMain($module,$pending_parts, $pending_parts_processed, $available_printers);
                
                $count ++;
                
                if ($count_parts==count($this->missing_parts)){
                    //if ($count>20){
                    break;
                }
                $count_parts=count($this->missing_parts);
                
            }
        }
    }

    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////// countUnassigned //////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    public function countUnassigned (){
        $cont_slot_aux = 0;
        foreach($this as $PrinterLine){
            foreach ($PrinterLine as $slot_aux){
                if ($slot_aux->state == 8){
                    $cont_slot_aux++;
                }
            }
        }
        return $cont_slot_aux;
    }
    

    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////// A  L  L  ////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsAll(array $pending_part, $printerTimeLine)
    {
        
      
    }
    
    //END FUNCTION
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////// A  L  L  O R I G I N A L////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsAllOriginal(array $pending_part, $printerTimeLine)
    {
        
        $can_be_assigned = false;
        $estimated_printing = array();
        //La primera fecha estimada será la actual,siempre y cuando no se solape con la noche
        //si se solapa me voy al principo de la jornada del días siguiente
        $estimated_printing['start'] =  new DateTimeG($this->start_date_time);
        //$estimated_printing['start']->modify("+" . conf_delays()['minutes_start'] . " minutes");
        //$estimated_printing['start'] = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
        
        if ($estimated_printing['start']->isOverlapAtNight()) {
            $day_start = $estimated_printing['start']->format('w');
            $estimated_printing['start']->setTime(session('working_days')[$day_start]['from']['hour'], session('working_days')[$day_start]['from']['minute']);
        }
        
        //$estimated_printing['start']->modify('+' . conf_delays()['minutes_start'] . ' minutes');
        $estimated_printing['end'] = $estimated_printing['start']->estimatedPrintingEnd($pending_part);
        
        if (! DateTimeG::removeOverlapAtNight($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))) {}
        DateTimeG::removeOverlapPastFromStartAll($pending_part, $estimated_printing, $this->start_date_time->format('Y-m-d H:i:s'));
        
        get_object_vars($estimated_printing['start']);
        get_object_vars($estimated_printing['end']);
        
        ///////////////////////////////////////////////////////////////////////////////
        /////////////////////// C H E C K  -  O V E R L A P S /////////////////////////
        //Incializaciones
        $gap_start = $this->getGapStart($estimated_printing['start']);
        
        get_object_vars($gap_start);
        //$is_gap_start_ini_change = false;
        $gap_end = "";
        $arrayIteratorFinal = $printerTimeLine->getIterator();
        $printer_slot_aux = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
        
        $is_first_iteration = true;
        //Si la PrinterTimeLine está vacía se puede asignar sin comprobaciones.
        if (! $arrayIteratorFinal->valid()){
            $can_be_assigned = true;
            $estimated_printing['start'] = $gap_start;
            $estimated_printing['end'] = $estimated_printing['start']->estimatedPrintingEnd($pending_part);
            get_object_vars($estimated_printing['start']);
            get_object_vars($estimated_printing['end']);
            if (! DateTimeG::removeOverlapAtNight($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))) {}
            DateTimeG::removeOverlapPastFromStartAll($pending_part, $estimated_printing, $this->start_date_time->format('Y-m-d H:i:s'));
            get_object_vars($estimated_printing['start']);
            get_object_vars($estimated_printing['end']);
        }
        //Comprobar solapamiento por la noche
        
        
        
        //Recorro los slots actuales ya que la nueva estimación puede solaparse con uno de ellos
        While ($arrayIteratorFinal->valid()) {
            
            $printerSlotFinal = $arrayIteratorFinal->current();
            
            $printer_slot_aux->update($printerSlotFinal);
            //$printer_slot_aux = $arrayIteratorFinal->current();
            $slot_start = new DateTimeG($printerSlotFinal->start);
            //$slot_start = new DateTime($printerSlotFinal->start);
            $slot_end = new DateTimeG($printerSlotFinal->end);
            //$slot_end = new DateTime($printerSlotFinal->end);
            
            //Actulaizamos el Hueco
            $gap_end = new DateTimeG($printerSlotFinal->start);
            //$gap_end = new DateTime($printerSlotFinal->start);
            get_object_vars($gap_end);
            
            if ($is_first_iteration) {
                
                $gap_end_ini = new DateTimeG($printerSlotFinal->end);
                //$gap_end_ini = new DateTime($printerSlotFinal->end);
                if (is_overlap($slot_start, $slot_end, $estimated_printing['start'], $estimated_printing['end'])) {
                    $estimated_printing = DateTimeG::estimatedPrintedFromStart($pending_part, $gap_end->modify('+1 minutes'), $this->start_date_time->format('Y-m-d H:i:s'));
                }
                get_object_vars($gap_end_ini);
                $is_first_iteration = false;
            }
            
            get_object_vars($slot_start);
            get_object_vars($slot_end);
            get_object_vars($gap_start);
            get_object_vars($gap_end);
            $gap_total_minutes = $gap_start->rangeTotalM( $gap_end);
            //Si se solapan el slot y el tiempo estimado, buscamos el tiempo estimado en los huecos
            if (is_overlap($slot_start, $slot_end, $estimated_printing['start'], $estimated_printing['end'])) {
                $arrayIteratorFinal->next();
                
                //Hay que comprobar que el gap no se solapa con la noche y  arreglarlo
                
                //if (removeOverlapWeekendGap($gap_start)){}
                
                //Comprobamos si la fecha estimada cabe en el hueco de la semana que estamos trabajando.
                $estimated_printing_total_minutes = $estimated_printing['start']->rangeTotalM($estimated_printing['end']);
                if (($gap_total_minutes >= $estimated_printing_total_minutes) &&
                    //Cabe en el hueco
                    ($slot_end->format('W') == $estimated_printing['start']->format('W')) &&
                    //La semana de fecha de inicio estimada es de la misma semana que la del fin del slot
                    ($slot_end->format('W') == $gap_start->format('W')))
                    //El hueco es de la misma semana que la del fin del slot
                {
                    //El tiempo estimado empezará al principio del hueco
                    //$estimated_printing = estimatedPrintedFromStart($pending_part, $gap_start->modify('+1 minutes'),  $this->start_date_time->format('Y-m-d H:i:s'));
                    
                    $estimated_printing = DateTimeG::estimatedPrintedFromStart($pending_part,$gap_start->modify("+" . conf_delays()['minutes_start'] . " minutes"),$this->start_date_time->format('Y-m-d H:i:s'));
                    
                    //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
                    /*
                     if (!removeOverlapAtNightMedian($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))){
                     }
                     
                     //Si la fecha de inicio estimada es del pasado hay que volver a calcularla
                     removeOverlapPastFromStart($pending_part,$estimated_printing, $this->start_date_time->format('Y-m-d H:i:s'));
                     */
                    if (is_overlap($slot_start, $slot_end, $estimated_printing['start'], $estimated_printing['end'])) {
                        //Si se solapa el slot con la fecha estimada, desplazo la fecha estimada la derecha al final del slot
                        $estimated_printing['start'] = $slot_end->createDateTime();
                        //Jose 14/02/2022: La lia parda
                        /**/
                        //$estimated_printing['start']->modify('+1 minutes');
                        //$estimated_printing['start']->modify("+" . conf_delays()['minutes_start'] . " minutes");
                        /**/
                        $estimated_printing['end'] = $estimated_printing['start']->estimatedPrintingEnd($pending_part);
                        
                        //Si la fecha de inicio estimada es del pasado hay que volver a calcularla
                        DateTimeG::removeOverlapPastFromStartAll($pending_part, $estimated_printing,  $this->start_date_time->format('Y-m-d H:i:s'));
                    }
                    
                    $gap_start = $slot_end;
                    //Jose 14/02/2022: La lia parda
                    //$gap_start = $estimated_printing['start'];
                } else {
                    //Si no cabe en el hueco actual, busco en el siguiente hueco
                    //El tiempo estimado empezará al final del hueco,para que nos de un solapamiento en la siguiente iteración
                    //y calcule el nuevo hueco despues del salto con el que se ha solapado
                    
                    //$estimated_printing = estimatedPrintedFromStart($pending_part, $slot_end->modify("+" . conf_delays()['minutes_start'] . " minutes"), $this->start_date_time->format('Y-m-d H:i:s'));
                    
                    
                    $estimated_printing = DateTimeG::estimatedPrintedFromStart($pending_part,
                        $slot_end->modify("+" . conf_delays()['minutes_start'] . " minutes"),
                        $this->start_date_time->format('Y-m-d H:i:s'));
                    
                    //$estimated_printing = estimatedPrintedFromStart($pending_part, $slot_end->modify("+" . conf_delays()['minutes_start'] . " minutes"), $this->start_date_time->format('Y-m-d H:i:s'));
                    //Si la fecha de inicio estimada es del pasado hay que volver a calcularla
                    DateTimeG:: removeOverlapPastFromStartAll($pending_part, $estimated_printing, $this->start_date_time->format('Y-m-d H:i:s'));
                    //Jose 15/02/2022: La lia parda
                    /*
                     if (isOverlapAtNight($estimated_printing['start'])) {
                     $day_start = $estimated_printing['start']->format('w');
                     $estimated_printing['start']->setTime(session('working_days')[$day_start]['from']['hour'], session('working_days')[$day_start]['from']['minute']);
                     }
                     $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
                     
                     $gap_start = $estimated_printing['end'];
                     */
                    
                    $gap_start = $slot_end;
                }
            } else {
                $gap_start = $slot_end;
                
                $arrayIteratorFinal->next();
            }
        } //END WHILE
        if ($printer_slot_aux->start == ''){
            $slot_aux_start = new DateTimeG($printer_slot_aux->start);
            
        }else{
            $slot_aux_start = DateTimeG::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->start);
        }
        
        //
        if ($printer_slot_aux->end == ''){
            $slot_aux_end = new DateTimeG($printer_slot_aux->end);
        }else{
            $slot_aux_end = DateTimeG::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end);
        }
        
        //
        
        //Compruebo si la estimación inicial ha cambiado, si no ha cambiado es que no se va asignar,
        //esto puedo ocurrir  por dos motivos:
        //Que la pieza cabe en el hueco inicial o primer hueco disponible, la fecha estimada se calcula en el hueco inicial
        //O que la pieza no cabe en los huecos actuales, la fecha estima está despues del último slot que es el slot aux
        
        /*
         if (($estimated_printing_ini['start'] == $estimated_printing['start']) && ($estimated_printing_ini['end'] == $estimated_printing['end'])){
         
         $estimated_printing_end = estimatedPrintingEnd($gap_start_ini, $pending_part);
         
         if ($estimated_printing_end <= $gap_end_ini){
         //la pieza cabe en el hueco inicial
         //if (is_wrap($gap_start_ini,$gap_end_ini,$estimated_printing['start'],$estimated_printing['end'])){
         $estimated_printing['start'] = $gap_start_ini;
         $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
         
         //Hay que actualizar el $gap_start_ini
         }else{
         //la pieza no cabe en los huecos actuales
         get_object_vars($slot_aux_end);
         $estimated_printing['start'] = new DateTime($slot_aux_end->date);
         $estimated_printing = estimatedPrintedMedianFromStart($pending_part, $estimated_printing['start']->modify('+1 minutes'), $this->start_date_time->format('Y-m-d H:i:s'));
         }
         }
         */
        
        //Asigno cuando el inicio y el fin no son por la noche. o el principio y el fin no son en fin de semana
        $now = new DateTimeG($this->start_date_time->format('Y-m-d H:i:s'));
        //$now = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
        if (! is_overlap($slot_aux_start, $slot_aux_end, $estimated_printing['start'], $estimated_printing['end']) && $estimated_printing['start'] >= $now) {
            $can_be_assigned = true;
            //break;
        }
        ;
        
        if ($can_be_assigned) {
            $printerTimeLine->addSlotAndSortAsc($estimated_printing, $pending_part);
            
            //break;
        } else {
            get_object_vars($estimated_printing['start']);
            get_object_vars($estimated_printing['end']);
            $message = "Warning: the part could not be assigned. \\nPart: " . $pending_part['id'] . ' \\nOrder: ' . $pending_part['order_id'] . '\\nFile: ' . $pending_part['file_id'] . '\\nEstimate printing start: ' . $estimated_printing['start'] ->format('Y-m-d H:i:s') . '\\nEstimate printing end: ' . $estimated_printing['end'] ->format('Y-m-d H:i:s');
            //alert($message);
            $this->missing_parts[$pending_part['id']] = $pending_part;
        }
        
        //} //END FOREACH ($this as $printerTimeLine){
        //return $is_gap_start_ini_change;
        /*
         echo "</br></br>";
         $this->printAll();
         echo "</br></br>";
         
         echo ('</br><b>Printer TimeLine: </b></br>');
         $this->printHtmlAll();
         echo "</br></br></br></br></br></br>";
         */
    }
    
    //END FUNCTION
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// R E M O V E - E N D - G A P S ///////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsRemoveEndGaps()
    {
        
        //$gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
        $gap_start = new DateTimeG($this->start_date_time);
        
        
        foreach($this as $printerTimeline){
            if ($printerTimeline->percent($this->start_date_time)< 90){
                $printerTimeline->uasort('startAscendingComparison');
                
                $gap_start = new DateTimeG($this->start_date_time);
                $gap_end = new DateTimeG($this->start_date_time);
                $slot_start = new DateTimeG($this->start_date_time);
                $slot_end = new DateTimeG($this->start_date_time);
                
                $printer_slot_aux = new PrinterSlot("","", "", "","","","","", "", $this->start_date_time, $this->start_date_time, "", "", "","");
                $gaps = array();
                $key_printer_slot_aux = false;
                $count = 0;
                foreach ($printerTimeline as $key_printer_slot => $printer_slot){
                    if (($printer_slot->state == 9) || ($printer_slot->state == 10)) break;
                    $count ++;
                    $slot_start = new DateTimeG($printer_slot->start);
                    $slot_end = new DateTimeG($printer_slot->end);
                    
                    $gap_start = new DateTimeG($printer_slot_aux->end);
                    $gap_end =  new DateTimeG($printer_slot->start);
                    
                    //TODO Configuration: maximum gap = 10
                    if ($gap_start->diffMinutes($gap_end) > 10){
                        $gap_aux = array();
                        $gap_aux['start'] = $gap_start;
                        $gap_aux['end'] = $gap_end;
                        $gap_aux['slot_1'] = PrinterSlot::new($printer_slot_aux);
                        $gap_aux['key_slot_1'] = $key_printer_slot_aux;
                        $gap_aux['slot_2'] = $printer_slot;
                        $gap_aux['key_slot_2'] = $key_printer_slot;
                        $gap_aux['count'] = $count;
                        array_push($gaps,$gap_aux);
                    }
                    
                    $printer_slot_aux->update($printer_slot);
                    $key_printer_slot_aux = $key_printer_slot;
                }
                //TODO Configuration: maximum night gap = 6
                
                if (($gap_start->diffHoursOnly($gap_end) > 6) && (count($gaps)>1)){
                    if ($gaps[1]['count']-$gaps[0] ['count']  == 2){
                        $start_date_time_block = new DateTimeG($gaps[0]['slot_1']->start);
                        
                        $part_1 = $gaps[0]['slot_1']->toPendingPart();
                        unset($printerTimeline[$gaps[0]['key_slot_1']]);
                        $part_2 = $gaps[0]['slot_2']->toPendingPart();
                        unset($printerTimeline[$gaps[0]['key_slot_2']]);
                        
                        $part_3 = $gaps[1]['slot_1']->toPendingPart();
                        unset($printerTimeline[$gaps[1]['key_slot_1']]);
                        $part_4 = $gaps[1]['slot_2']->toPendingPart();
                        unset($printerTimeline[$gaps[1]['key_slot_2']]);
                        
                        
                        $this->blocks_parts_matrix[0][$part_1['id']]= $part_1;
                        $this->blocks_parts_matrix[0][$part_2['id']]= $part_2;
                        $block = $this->blocks_parts_matrix[0];
                        $key_block = 0;
                        
                        
                        
                        $this->no_block_parts[$part_3['id']] = $part_3;
                        $this->no_block_parts[$part_4['id']] = $part_4;
                        
                        $unassigned = array();
                        
                        $limit_hours=72;
                        //$minutesFlex = 300;
                        for ($minutesFlex=60;$minutesFlex<=300;$minutesFlex+=30){
                            $unassigned_block_parts = $this->assignBlockToPrinter3($block,$key_block,$printerTimeline,$unassigned,$start_date_time_block,$minutesFlex,$limit_hours);
                            if (count($unassigned_block_parts)>0){
                                /*
                                $printerTimeline->uasort('startAscendingComparison');
                                $last_slot= end($printerTimeline->getArrayCopy());
                                $start = 
                                */
                                
                                //hay que quitarla de $this->no_block_parts
                                break;
                            }
                        }
                        //Ir al final del printerline para saber la fecha hora en que hay que poner la pieza que nos ha sobrado. Esta pieza es la que está en $this->no_block_parts
                       
                        //Poner en la timeline la pieza.
                        
                        
                    }
                }
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////// R E M O V E - E N D - G A P S ///////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsRemoveEndGapsOLD()
    {
        
        $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
        
        foreach($this as $printerTimeline){
            $printerTimeline->uasort('startAscendingComparison');
            $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
            $gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
            $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
            $slot_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
            
            
            $printer_slot_aux = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
            foreach ($printerTimeline as $printer_slot){
                if (($printer_slot->state == 9) || ($printer_slot->state == 10)) break;
                
                $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start);
                $slot_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end);
                
                $gap_start =  DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end);
                $gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start);
                
                $printer_slot_aux->update($printer_slot);
                
                
                
            }
            
            if ((isset($gap_start))&&($gap_start) &&  (isset($printer_slot)) && ($printer_slot->state != 9)){
                $gap_total_minutes = rangeTotalM($gap_start, $gap_end);
                $printer_slot_aux_minutes = rangeTotalM($slot_start, $slot_end);
                //$printer_slot_aux['estimated_printing_hours']*60+$printer_slot_aux['estimated_printing_minutes'];
                
                if ($printer_slot_aux_minutes < $gap_total_minutes) {
                    
                    //$printer_slot->start = $gap_start->modify('+1 minutes')->format('Y-m-d H:i:s');
                    $printer_slot->start = $gap_start->modify("+" . conf_delays()['minutes_start'] . " minutes")->format('Y-m-d H:i:s');
                    $gap_start->modify('+'.$printer_slot_aux_minutes.' minutes')->format('Y-m-d H:i:s');
                    $printer_slot->end = DateTime::CreateFromFormat('Y-m-d H:i:s',$gap_start->format('Y-m-d H:i:s'))->format('Y-m-d H:i:s');
                    $printerTimeline->uasort('startAscendingComparison');
                }
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////// R E M O V E - G A P S -W I T H - E N D - S L O T S /////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsRemoveGapsWhithEndSlots()
    {
        
        $gap_start = new DateTimeG($this->start_date_time);
        
        foreach($this as $printerTimeline){
            $printerTimeline->uasort('startAscendingComparison');
            $gap_start = new DateTimeG($this->start_date_time);
            $gap_end = new DateTimeG($this->start_date_time);
            $slot_start = new DateTimeG($this->start_date_time);
            $slot_end = new DateTimeG($this->start_date_time);
            
            
            $printer_slot_aux = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
            
            
            
            foreach ($printerTimeline as $printer_slot){
                if (($printer_slot->state == 9) || ($printer_slot->state == 10)) break;
                
                $slot_start = new DateTimeG($printer_slot->start);
                $slot_end = new DateTimeG($printer_slot->end);
                
                
                
                $printer_slot_aux->update($printer_slot);
                
                
                
            }
            
            //$gap_total_minutes = rangeTotalM($gap_start, $gap_end);
            $printer_slot_aux_minutes = $slot_start->rangeTotalM($slot_end);
            
            if ((isset($printer_slot))&&($printer_slot->state == 8)){
                $biggest_gap = $printerTimeline->getBiggestGap($this->start_date_time);
            }
            if (isset($biggest_gap['start'])){
                $biggest_gap_total_minutes = $biggest_gap['start']->rangeTotalM($biggest_gap['end']);
                
                $printer_slot_aux_minutes = $slot_start->rangeTotalM($slot_end);
                
                //$printer_slot_aux['estimated_printing_hours']*60+$printer_slot_aux['estimated_printing_minutes'];
                //$biggest_gap_total_minutes['minutes']
                //$biggest_gap_total_minutes['slot'] =
                if ($printer_slot_aux_minutes < $biggest_gap_total_minutes) {
                    
                    //$printer_slot->start = $biggest_gap['start']->modify('+1 minutes')->format('Y-m-d H:i:s');
                    $printer_slot->start = $biggest_gap['start']->modify("+" . conf_delays()['minutes_start'] . " minutes")->format('Y-m-d H:i:s');
                    
                    $biggest_gap['start']->modify('+'.$printer_slot_aux_minutes.' minutes');
                    $printer_slot->end = new DateTimeG($biggest_gap['start']);
                    $printerTimeline->uasort('startAscendingComparison');
                }
            }
            /*
             echo "-------------------------------------------------------------------------------------------------";
             echo "<br>-- Slot ---";
             var_dump($slot_start);
             echo "-----";
             var_dump($slot_end);
             echo "------ Gap ----";
             
             var_dump($gap_start);
             echo "-----";
             var_dump($gap_end);
             echo "--------- Printer Slot-----------";
             var_dump($printer_slot);
             
             echo "----------------------------------------------------------------------------------------------------<br>";
             */
            
        }
        
        
        
        
        
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////// S W A P  -  9 4 ////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsUnassigned94Swap()
    {
        //llenar los huecos grandes con las partes que empieza por la noche
        if (isset($this->missing_parts)){
            usort($this->missing_parts, 'sort_by_hours_desc');
            $key = array_key_first($this->missing_parts);
            if ((isset($key))&&($this->missing_parts[$key]['estimated_printing_hours']>3)){
                foreach ($this as $printerTimeLine){
                    $percent = $printerTimeLine->percent($this->start_date_time);
                    
                    if ($percent <94){
                        $printer_slot_aux = new PrinterSlot("","", "", "","","", "", "","","", $this->start_date_time->format('Y-m-d H:i:s'),$this->start_date_time->format('Y-m-d H:i:s'), "", "","");
                        $printer_slot_move = new PrinterSlot("","", "", "","","", "", "","", "",$this->start_date_time->format('Y-m-d H:i:s'), $this->start_date_time->format('Y-m-d H:i:s'), "", "","");
                        $is_first_iteration = true;
                        $is_change_unassigned = false;
                        $gap_start = $this->getGapStart($this->start_date_time);
                        //$slot_aux_start = new DateTime();
                        //$slot_aux_end = new DateTime();
                        foreach($printerTimeLine as $printer_slot){
                            $gap_end = new DateTimeG($printer_slot->start);
                            $slot_start = new DateTimeG($printer_slot->start);
                            $slot_end = new DateTimeG($printer_slot->end);
                            $slot_aux_start = new DateTimeG($printer_slot_aux->start);
                            $slot_aux_end = new DateTimeG($printer_slot_aux->end);
                            $slot_move_start = new DateTimeG($printer_slot_move->start);
                            $slot_move_end = new DateTimeG($printer_slot_move->end);
                            
                            $diff_aux = diffRangeHM($slot_aux_start,$slot_aux_end);
                            $diff_move = diffRangeHM($slot_move_start,$slot_move_end);
                            $diff_gap = diffRangeHM($gap_start,$gap_end);
                            $aux_minutes = $diff_aux['hours']*60+$diff_aux['minutes'];
                            $move_minutes = $diff_move['hours']*60+$diff_move['minutes'];
                            $gap_minutes = $diff_gap['hours']*60+$diff_gap['minutes'];
                            
                            
                            
                            if (($is_change_unassigned) &&( $move_minutes <$gap_minutes+ conf_delays()['minutes_start'])){
                                
                                $printer_slot_move->start = $slot_aux_end->modify('+'.conf_delays()['minutes_start'].' minutes')->format('Y-m-d H:i:s');
                                $printer_slot_move->end = $slot_aux_end->modify('+'.$move_minutes.' minutes')->format('Y-m-d H:i:s');
                                
                                
                                $printerTimeLine->addSlotSimple($printer_slot_move);
                                break;
                                
                            }elseif ((!$is_first_iteration) && (!$is_change_unassigned)){
                                
                                //la pieza aux tiene que ser menor que la no asignada y acabar por la noche
                                //Y la pieza no asignada puesta en el lugar de la aux no tiene que solaparse con la actual.
                                
                                $key = array_key_first($this->missing_parts);
                                if (isset ($key)){
                                    $unssigned_minutes =    $this->missing_parts[$key]['estimated_printing_hours']*60
                                    +
                                    $this->missing_parts[$key]['estimated_printing_minutes'];
                                    $slot_new_start = $slot_aux_start;
                                    $slot_new_end = estimatedPrintingEnd($slot_new_start,  $this->missing_parts[$key]);
                                    
                                    
                                    
                                    if (($aux_minutes < $unssigned_minutes)
                                        &&
                                        isOverlapAtNight($slot_aux_end)
                                        &&
                                        !is_overlap_whithout_wrap($slot_new_start, $slot_new_end, $slot_start,$slot_end))
                                    {
                                        //Guardo la pieza que voy a sustituir para ponerla más alante
                                        $printer_slot_move->part = $printer_slot_aux->part;
                                        $printer_slot_move->order =$printer_slot_aux->order;
                                        $printer_slot_move->start = $printer_slot_aux->start;
                                        $printer_slot_move->end = $printer_slot_aux->end;
                                        $printer_slot_move->weight = $printer_slot_aux->weight;
                                        $printer_slot_move->state = $printer_slot_aux->state;
                                        $printer_slot_move->file = $printer_slot_aux->file;
                                        $printer_slot_move->file_name = $printer_slot_aux->file_name;
                                        $printer_slot_move->product = $printer_slot_aux->product;
                                        $printer_slot_move->product_name = $printer_slot_aux->product_name;
                                        $printer_slot_move->final_product = $printer_slot_aux->final_product;
                                        $printer_slot_move->state = 8;
                                        //La pieza no asigndad se pone donde empieza aux, ya que la sustituye
                                        $printer_slot_aux->end = $slot_new_end->format('Y-m-d H:i:s');
                                        $printer_slot_aux->order = $this->missing_parts[$key]['order_id'];
                                        $printer_slot_aux->file = $this->missing_parts[$key]['file_id'];
                                        $printer_slot_aux->part = $this->missing_parts[$key]['id'];
                                        $printer_slot_aux->weight = $this->missing_parts[$key]['weight'];
                                        $printer_slot_aux->state = 8;
                                        
                                        unset($this->missing_parts[$key]);
                                        $key = array_key_first($this->missing_parts);
                                        
                                        
                                        
                                        $is_change_unassigned = true;
                                    }
                                }//if (!isset ($key)){
                            }//END if (!$is_first_iteration){
                            
                            $gap_start = $slot_end;
                            $printer_slot_aux=$printer_slot;
                            $is_first_iteration = false;
                        }//END foreach($printerTimeLine as $printer_slot){
                    } //END if ($percent <94){
                    $key = array_key_first($this->missing_parts);
                    $printerTimeLine->uasort('startAscendingComparison');
                    if (!isset($key) || ($this->missing_parts[$key]['estimated_printing_hours']<4)){
                        break;
                    }
                }//END foreach ($this as $printerTimeLine){
            } //END if ($this->missing_parts[$key]['estimated_printing_hours']>3){
        } //END if (isset($this->missing_parts)){
        
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////// G A P  -  9 4 ///////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsUnassigned92Gap()
    {
        //llenar los huecos grandes con las partes que empieza por la noche
        if (isset($this->missing_parts)){
            usort($this->missing_parts, 'sort_by_hours_desc');
            $key = array_key_first($this->missing_parts);
            if (isset($key)){
                foreach ($this as $printerTimeLine){
                    //$percent = $printerTimeLine->percent($this->start_date_time);
                    
                    //if ($percent <92){
                    $printer_slot_aux = new PrinterSlot("","", "", "","","", "", "", "", "",$this->start_date_time->format('Y-m-d H:i:s'),$this->start_date_time->format('Y-m-d H:i:s'), "", "","");
                    $printer_slot_new = new PrinterSlot("","", "", "","","", "", "", "","", $this->start_date_time->format('Y-m-d H:i:s'), $this->start_date_time->format('Y-m-d H:i:s'), "", "","");
                    $is_first_iteration = true;
                    $gap_start = $this->getGapStart($this->start_date_time);
                    //$slot_aux_start = new DateTime();
                    //$slot_aux_end = new DateTime();
                    foreach($printerTimeLine as $printer_slot){
                        $gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start);
                        $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start);
                        $slot_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end);
                        $slot_aux_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end);
                        
                        $diff_gap = diffRangeHM($gap_start,$gap_end);
                        $gap_minutes = $diff_gap['hours']*60+$diff_gap['minutes'];
                        
                        if ((!$is_first_iteration)){
                            $key = array_key_first($this->missing_parts);
                            
                            if (isset ($key)){
                                $unssigned_minutes =    $this->missing_parts[$key]['estimated_printing_hours']*60
                                +
                                $this->missing_parts[$key]['estimated_printing_minutes'];
                                $slot_new_start = $slot_aux_end ->modify('+'.conf_delays()['minutes_start'].' minutes');
                                $slot_new_end = estimatedPrintingEnd($slot_new_start,  $this->missing_parts[$key]);
                                
                                if (($unssigned_minutes < $gap_minutes + conf_delays()['minutes_start'] )
                                    &&
                                    !isOverlapAtNight($slot_new_start)
                                    &&
                                    !is_overlap_whithout_wrap($slot_new_start, $slot_new_end, $slot_start,$slot_end))
                                {
                                    //La pieza no asigndad se pone donde empieza aux, ya que la sustituye
                                    $printer_slot_new->start = $slot_new_start->format('Y-m-d H:i:s');
                                    $printer_slot_new->end = $slot_new_end->format('Y-m-d H:i:s');
                                    $printer_slot_new->order = $this->missing_parts[$key]['order_id'];
                                    $printer_slot_new->file = $this->missing_parts[$key]['file_id'];
                                    $printer_slot_new->file_part_name = $this->missing_parts[$key]['file_part_name'];
                                    $printer_slot_new->file_name = $this->missing_parts[$key]['file_name'];
                                    $printer_slot_new->version_part_name = $this->missing_parts[$key]['version_part_name'];
                                    $printer_slot_new->part = $this->missing_parts[$key]['id'];
                                    $printer_slot_new->product = $this->missing_parts[$key]['product_id'];
                                    $printer_slot_new->product_name = $this->missing_parts[$key]['product_name'];
                                    $printer_slot_new->final_product = $this->missing_parts[$key]['final_product_id'];
                                    
                                    $printer_slot_new->weight = $this->missing_parts[$key]['weight'];
                                    $printer_slot_new->state = 8;
                                    
                                    $printerTimeLine->addSlotSimple($printer_slot_new);
                                    //$this->uasort('startAscendingComparison');
                                    
                                    unset($this->missing_parts[$key]);
                                    $key = array_key_first($this->missing_parts);
                                    break;
                                }
                            }//if (!isset ($key)){
                        }//END if (!$is_first_iteration){
                        
                        $gap_start = $slot_end;
                        $printer_slot_aux=$printer_slot;
                        $is_first_iteration = false;
                    }//END foreach($printerTimeLine as $printer_slot){
                    //} //END if ($percent <94){
                    $key = array_key_first($this->missing_parts);
                    if (!isset($key)){
                        break;
                    }
                }//END foreach ($this as $printerTimeLine){
            } //END if ($this->missing_parts[$key]['estimated_printing_hours']>3){
        } //END if (isset($this->missing_parts)){
        
    }
    

    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////// E N D - S L O T S  //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersEndSlots(array $pending_part, $printerTimeLine)
    {
        $printerTimeLine->uasort('startAscendingComparison');
        $can_be_assigned = false;
        
        $arrayIteratorFinal = $printerTimeLine->getIterator();
        $printer_slot_aux = new PrinterSlot("","", "", "","","","", "", "", "", "", "", "", "","");
        
        
        //Si la PrinterTimeLine está vacía se puede asignar sin comprobaciones.
        
        //Comprobar solapamiento por la noche
        
        $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
        
        //Recorro los slots actuales ya que la nueva estimación puede solaparse con uno de ellos
        While ($arrayIteratorFinal->valid()) {
            
            $printerSlotFinal = $arrayIteratorFinal->current();
            
            $printer_slot_aux->update($printerSlotFinal);
            //$printer_slot_aux = $arrayIteratorFinal->current();
            $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->start);
            //$slot_start = new DateTime($printerSlotFinal->start);
            $slot_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->end);
            //$slot_end = new DateTime($printerSlotFinal->end);
            
            //Actulaizamos el Hueco
            if ($this->start_date_time > $printerSlotFinal->start){
                $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->start);
            }
            $gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->start);
            //$gap_end = new DateTime($printerSlotFinal->start);
            
            
            
            get_object_vars($slot_start);
            get_object_vars($slot_end);
            get_object_vars($gap_start);
            get_object_vars($gap_end);
            
            
            $gap_total_minutes = rangeTotalM($gap_start, $gap_end);
            $pending_part_total_minutes = $pending_part['estimated_printing_hours']*60+$pending_part['estimated_printing_minutes'];
            $estimated_printing = array();
            //Si se solapan el slot y el tiempo estimado, buscamos el tiempo estimado en los huecos
            if ($pending_part_total_minutes < $gap_total_minutes) {
                
                
                $estimated_printing['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$gap_start->modify('+1 minutes')->format('Y-m-d H:i:s'));
                $estimated_printing['end'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$gap_start->format('Y-m-d H:i:s'));
                $estimated_printing['end']->modify('+'.$pending_part['estimated_printing_hours'].' hours');
                $estimated_printing['end']->modify('+'.($pending_part['estimated_printing_minutes']+1).' minutes');
                
                //$estimated_printing ['end']= estimatedPrintingEnd($estimated_printing['start'], $pending_part);
                $can_be_assigned = true;
                break;
                //Hay que comprobar que el gap no se solapa con la noche y  arreglarlo
                
                //if (removeOverlapWeekendGap($gap_start)){}
                
                //Comprobamos si la fecha estimada cabe en el hueco de la semana que estamos trabajando.
            }
            $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->end);
            $arrayIteratorFinal->next();
        }
        
        if ($can_be_assigned) {
            //var_dump($estimated_printing);
            $printerTimeLine->addSlotAndSortAsc($estimated_printing, $pending_part);
            
            //break;
        } else {
            
            //get_object_vars($estimated_printing['start']);
            //get_object_vars($estimated_printing['end']);
            $message = "Warning: the part could not be assigned. \\nPart: " . $pending_part['id'] . ' \\nOrder: ' . $pending_part['order_id'] . '\\nFile: ' . $pending_part['file_id'] ;
            //$message = "Warning: the part could not be assigned. \\nPart: " . $pending_part['id'] . ' \\nOrder: ' . $pending_part['order_id'] . '\\nFile: ' . $pending_part['file_id'] . '\\nEstimate printing start: ' . $estimated_printing['start'] ->format('Y-m-d H:i:s') . '\\nEstimate printing end: ' . $estimated_printing['end'] ->format('Y-m-d H:i:s');
            //alert($message);
            /*
             $estimated_printing['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end);
             
             $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
             
             if (!removeOverlapAtNight($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))){
             
             }
             $printerTimeLine->addSlotAndSortAsc($estimated_printing, $pending_part);
             */
            $this->missing_parts[$pending_part['id']] = $pending_part;
            
        }
        
    }
    
    
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////// E N D - S L O T S  //////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsFinal(array $pending_part, $printerTimeLine)
    {
        $printer_slot_aux = new PrinterSlot("","", "", "","","","", "", "", "", "", "", "", "","");
        foreach ($printerTimeLine as $printer_slot){
            
            
            $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->start);
            $slot_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end);
            $printer_slot_aux->update($printer_slot);
            
        }
        $estimated_printing = array();
        $estimated_printing['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot->end);
        $estimated_printing['start']->modify("+1 minutes");
        $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
        
        /*
         if (!removeOverlapAtNight($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))){
         
         }
         */
        $printerTimeLine->addSlotAndSortAsc($estimated_printing, $pending_part);
    }
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////A S S I G N - P R I N T E R - S L O T S//////////////////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsWeekdays2()
    {
        
        //TODO Falta guardarlos en bdd pero lo tiene que hacer en la función addSlotToPrinter()
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //TODO Comprobar si el pedido ya tiene creadas Parts???????????????????????
        
        ////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////////////////////////////// Carga de BDD //////////////////////////////////////
        $sql = sprintf('
                SELECT  *
                FROM    printer AS pri
                WHERE   available = 1 AND neighborhood_factory_id = ' . $this->neighborhood_factory_id . '
                ORDER BY pri.id;
               ');
        $available_printers = DataBase::getConnection()->query($sql);
        $available_printers_aux = DataBase::getConnection()->query($sql);
        
        /*
         $arrayPrintersIterator = $available_printers->getIterator();
         while( $arrayPrintersIterator->valid()) {
         //if( $arrayIterator->valid()) {
         $printer = $arrayPrintersIterator->current();
         $arrayPrintersIterator->next();
         }
         */
        $sql = sprintf('
            SELECT *
            FROM  time_without_operators AS two
            WHERE neighborhood_factory_id  = ' . $this->neighborhood_factory_id . ' AND active = 1
            ORDER BY day;');
        
        //$time_without_operator = DataBase::getConnection()->query($sql);
        
        $sql = sprintf('
            SELECT  p.id,p.file_id,p.file_part_name,p.version_part_name,p.order_id,state_id,p.weight,f.name as file_name,f.estimated_printing_hours,f.estimated_printing_minutes,f.filament_used,p.start_datetime,p.initiated
            FROM    part AS p
            INNER   JOIN file AS f ON p.file_id = f.id
            WHERE   state_id = 8
            ORDER BY p.order_id  ASC, f.estimated_printing_hours DESC, f.estimated_printing_minutes DESC,p.id;
            ;
        ');
        //ORDER BY p.weight DESC,p.order_id  ASC
        //DataBase::getConnection()->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_CLASS | PDO::FETCH_CLASSTYPE);
        $pending_parts = DataBase::getConnection()->query($sql);
        ////////////////////////////////////////////////////////////////////////////////////////////////
        /////////////////////////////////////////// Inicializo estados ////////////////////////////////
        //$day_week = 5;
        $day_week = 4;
        $week_weekend = 0;
        $week = 0;
        
        $is_gap_start_ini_change = false;
        ////////////////////////////////////////////////////////////////////////////////////////////////
        ///////////////////////////////////// Recorrer $pendig_part ////////////////////////////////////
        $printer = array();
        $order_id = - 1;
        
        $pending_parts_processed = array();
        
        $available_printers_num = $available_printers->num_rows;
        $available_printers_count = 0;
        
        while ($pending_part = $pending_parts->fetch_assoc()) {
            //foreach ($pending_parts as $pending_part){
            $pending_parts_processed[$pending_part['id']] = $pending_part;
            //Si ha cambiado el pedido inicializamos
            if ($order_id != $pending_part['order_id']) {
                //Trasladar huecos de la impresora actual pequeños a la izquierda a la noche.
                
                //Si ya he usado todas las impresoras, vuelvo a la primera impresora
                if (++ $available_printers_count > $available_printers_num) {
                    //do something
                    $available_printers_count = 0;
                    $available_printers->data_seek(0);
                }
                
                $printer = $available_printers->fetch_assoc();
                
                $day_week = 4;
                $week_weekend = 0;
                $week = 0;
                
                $is_gap_start_ini_change = false;
            }
            
            //Busco la printer time line de la impresora actual
            
            $current_printer_time_line = $this->searchPrinterTimeLine($printer);
            
            $order_id = $pending_part['order_id'];
            
            if (isSizeHours('biggest', $pending_part['estimated_printing_hours'])) {
                $this->assignPrintersSlotsBiggest($pending_part, $week_weekend, $current_printer_time_line);
            } elseif (isSizeHours('big', $pending_part['estimated_printing_hours'])) {
                $this->assignPrintersSlotsBig($pending_part, $day_week, $week_weekend, $week, $current_printer_time_line);
            } elseif (isSizeHours('median', $pending_part['estimated_printing_hours'])) {
                //$is_gap_start_ini_change = $this->assignPrintersSlotsMedian($pending_part,$is_gap_start_ini_change);
                $this->assignPrintersSlotsMedian($pending_part, $is_gap_start_ini_change, $current_printer_time_line);
            } elseif (isSizeHours('small', $pending_part['estimated_printing_hours'])) {
                echo "</br>--------------> small       </br>";
            } elseif (isSizeHours('smallest', $pending_part['estimated_printing_hours'])) {
                echo "</br>--------------> the smallest       </br>";
            } else {
                echo "</br>--------------> out of range       </br>";
            }
            ;
            $week = 0;
        } //END foreach ($pending_parts as $pending_part){
        
        $this->RemoveGapsToLeft($available_printers_aux, $pending_parts_processed);
        
        //Quitar huecos grandes moviendo de las piezas biggest y big
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////// B I G G E S T ///////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsBiggest(array $pending_part, &$week_weekend, $printerTimeLine)
    {
        $can_be_assigned = false;
        //Recorro Las lóneas de tiempo de cada impresora
        //$printerTimeLine = $this[0];
        //foreach ($this as $printerTimeLine){
        $next_weekend = nextWeekend( $this->start_date_time->format('Y-m-d H:i:s'), $week_weekend);
        $estimated_printing = estimatedPrintedInNextWeekend($pending_part,  $this->start_date_time->format('Y-m-d H:i:s'), $week_weekend);
        get_object_vars($estimated_printing['start']);
        get_object_vars($estimated_printing['end']);
        //Si no es el primer slot de $printerTimeLine
        if ($printerTimeLine->count() != 0) {
            //Recorro la linea de tiempo de la impresora hasta encontrar el fin de semana y compruebo si tiene slot ocupandolo
            //Inicializo la variable que guardará la copia del ultimo slot para que cuando salga del foreach
            $printer_slot_aux = new PrinterSlot("","", "", "","","","", "", "", "", "", "", "", "","");
            //Busco el último slot antes del fin de semana
            foreach ($printerTimeLine as $printerSlot) {
                //Buscar rango del fin de semana, si no está ocupado asignarlo
                //Hago la copia del slot actual para que cuando salga tener el último slot de la printertimeline
                $printer_slot_aux->update($printerSlot);
                if ($printerSlot->start && $printerSlot->end) {
                    $slot_start= DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->start );
                    //$slot_start = new DateTime($printerSlot->start);
                    $slot_end= DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlot->end );
                    //$slot_end = new DateTime($printerSlot->end);
                    //Compruebo que el slot no ocupa el domingo al que queráa asignar la pieza biggest
                    if ($slot_end > $next_weekend['friday']) {
                        
                        if (is_overlap($slot_start, $slot_end, $estimated_printing['start'], $estimated_printing['end'])) {
                            //$next_weekend = nextWeekend(++$week_weekend);
                            $estimated_printing = estimatedPrintedInNextWeekendSlot($pending_part, $printerSlot);
                        } else {
                            //break;
                        }
                        
                        //break;
                    } else {
                        $estimated_printing = estimatedPrintedInNextWeekendSlot($pending_part, $printerSlot);
                    }
                    
                    get_object_vars($estimated_printing['start']);
                    get_object_vars($estimated_printing['end']);
                } else {
                    $message = "Warning: Faulty printer slot. Order:" . $printerSlot->order . '. Part: ' . $printerSlot->part;
                    alert($message);
                    //break;
                }
            } //END FOREACH ($printerTimeLine as $printerSlot)
            $slot_aux_start= DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->start );
            //$slot_aux_start = new DateTime($printer_slot_aux->start);
            $slot_aux_end= DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end );
            //$slot_aux_end = new DateTime($printer_slot_aux->end);
            //Si el último Slot no se solapa con el fin de semana lo asigno.
            
            if (
                (! datesOverlap($slot_aux_start, $slot_aux_end, $next_weekend['friday'], $next_weekend['sunday'])) 
                || 
                (
                 (! datesOverlap($estimated_printing['start'], $estimated_printing['start'], $next_weekend['friday'], $next_weekend['sunday'])) 
                 && 
                 (($printer_slot_aux->state == 9) || ($printer_slot_aux->state == 10))
                ) 
               )
            {
                //if (!datesOverlap($slot_aux_start,$slot_aux_end,$next_weekend['friday'],$next_weekend['sunday'])){
                $can_be_assigned = true;
                $week_weekend ++;
            };
        } else { //ELSE Si no es el primer slot de $printerTimeLine
            //Add primer slot de la $printerTimeLine actual
            $can_be_assigned = true;
            $week_weekend ++;
        }
        //echo "</br>------------------------->Biggest";
        
        if ($can_be_assigned) {
            $printerTimeLine->addSlotAndSortAsc($estimated_printing, $pending_part);
            /*
             echo "</br></br></br>";
             $this->print($printerTimeLine);
             echo "</br></br></br>";
             $this->printHtml ($printerTimeLine);
             */
            //break;
        } else {
            get_object_vars($estimated_printing['start']);
            get_object_vars($estimated_printing['end']);
            $message = "Warning: the part could not be assigned. \\nPart: " . $pending_part['id'] . ' \\nOrder: ' . $pending_part['order_id'] . '\\nFile: ' . $pending_part['file_id'] . '\\nEstimate printing start: ' . $estimated_printing['start'] ->format('Y-m-d H:i:s') . '\\nEstimate printing end: ' . $estimated_printing['end'] ->format('Y-m-d H:i:s');
            //alert($message);
        }
        
        //Si está libre asigno y break;
        
        //Si no está libre lo busco en la siguiente semana;
        //Si es la última impresora y no se ha asignado la pieza ....
        
        //Si tengo más de 5 impresoras
        
        //Si tengo más de 15 impresoras
        
        //Si tengo más de 25 impresoras
        
        //Puedo ponerla al final de esta semana y tengo más de 26 impresora
        
        //Si no Puedo ponerla al final de la semana que viene
        //if ($can_be_assigned) break;
        
        //} //END foreach ($this as $printerTimeLine){
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////// B I G  //////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsBig($pending_part, &$day_week, &$week_weekend, &$week, $printerTimeLine)
    {
        $can_be_assigned = false;
        //Recorro las impresoras
        //$printerTimeLine = $this[0];
        //foreach ($this as $printerTimeLine){
        //////////////////////////////////////////////////////////////////////////////////////////////////////////
        //////////////////// I N I T I A L  -  E S T I M A T E D  -  P R I N T I N G  -  T I M I N G ////////////
        $estimated_printing = estimatedPrintedBig($pending_part,  $this->start_date_time->format('Y-m-d H:i:s'), conf_week_working_days()[$day_week]);
        //Si no es el primer slot de $printerTimeLine
        if ($printerTimeLine->count() != 0) {
            //Inicializo la variable que guardará la copia del último slot para cuando salga del foreach
            $printer_slot_aux = new PrinterSlot("","", "", "","","","", "", "", "", "", "", "", "","","");
            //Recorro los slots actuales y comprueba si la fecha estimada no se solapa con ellos, si es así se recalcula la fecha estimada
            $arrayIterator = $printerTimeLine->getIterator();
            while ($arrayIterator->valid()) {
                //if( $arrayIterator->valid()) {
                $printerSlot = $arrayIterator->current();
                //Buscar rango del fin de semana, si no está ocupado asignarlo
                //Hago la copia del slot actual para que cuando salga tener el último slot de la printertimeline
                $printer_slot_aux->update($printerSlot);
                if ($printerSlot->start && $printerSlot->end) {
                    ///////////////////////////////////////////////////////////////////////////////
                    /////////////////////// C H E C K  -  O V E R L A P S /////////////////////////
                    //Me recorro los slots actuales por si la estimacion actual se solapa con alguno.
                    
                    //Incializaciones los huecos
                    $gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
                    //$gap_end = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
                    $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
                    //$gap_start = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
                    
                    $gap_start->modify('+' . conf_delays()['minutes_start'] . ' minutes');
                    //Si el hueco empiza por la noche tenemos que adelantarlo
                    if (is_overlap_at_night($gap_start)) {
                        $gap_start->setTime(conf_workday($gap_start)['from']['hour'], conf_workday($gap_start)['from']['minute']);
                    }
                    //Recorro los slots actuales ya que la nueva estimación puede solaparse con uno de ellos
                    $arrayIteratorFinal = $printerTimeLine->getIterator();
                    While ($arrayIteratorFinal->valid()) {
                        
                        $printerSlotFinal = $arrayIteratorFinal->current();
                        $printer_slot_aux->update($printerSlotFinal);
                        
                        $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->start);
                        //$slot_start = new DateTime($printerSlotFinal->start);
                        $slot_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->end);
                        //$slot_end = new DateTime($printerSlotFinal->end);
                        //Si se solapa buscamos el timepo estimado en los huecos
                        if (is_overlap($slot_start, $slot_end, $estimated_printing['start'], $estimated_printing['end'])) {
                            //TODO Este Next IteratorFinal puede ir al final y a nivel visual es más correcto. Cambiar y probar
                            $arrayIteratorFinal->next();
                            //Hueco
                            $gap_end= DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->start);
                            //$gap_end = new DateTime($printerSlotFinal->start);
                            
                            //if (removeOverlapWeekendGap($gap_start)){}
                            
                            $gap_total_minutes = rangeTotalM($gap_start, $gap_end);
                            
                            //Hay que comprobar que el gap no empieza ni acaba por la noche.
                            //$night_gap =estimatedPrintingNights(array ('start'=>$gap_start,'end'=>$gap_end));
                            
                            //Estimación actual
                            $estimated_printing_total_minutes = rangeTotalM($estimated_printing['start'], $estimated_printing['end']);
                            //Comprobamos si la fecha estimada cabe en el hueco de la semana que estamos trabajando
                            if (($slot_end->format('W') == $estimated_printing['start']->format('W')) && //Son de la misma semana
                                ($slot_end->format('W') == $gap_start->format('W')) && //Son de la misma semana
                                ($gap_total_minutes >= $estimated_printing_total_minutes)) //Cabe en el hueco
                                /*
                                 &&
                                 //Si el gap se solapa con la noche por lo que no se puede utilizar para estimar.
                                 !(
                                 (is_overlap_whithout_wrap($night_gap['before']['start'], $night_gap['before']['end'], $gap_start,$gap_end))
                                 ||
                                 (is_overlap_whithout_wrap($night_gap['behind']['start'], $night_gap['behind']['end'], $gap_start,$gap_end))
                                 )
                                 */
                            {
                                //Como Cabe en el hueco de esta semana el tiempo estimado empezará al principio del hueco
                                $estimated_printing['start'] = $gap_start->modify('+1 minutes');
                                $estimated_printing['end'] = estimatedPrintingEnd($gap_start, $pending_part);
                                
                                //¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ O J O !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                                //La comprobación de solapamiento con el slot actual de la nueva fecha estimada la hace en la siguiente vuelta.
                                
                                if (! removeOverlapAtNightBig($estimated_printing, $pending_part,  $this->start_date_time->format('Y-m-d H:i:s'))) {}
                                
                                //Si la fecha estimada de incio es del pasado, hay que recalcular.
                                $now  = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
                                //$now = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
                                if ($estimated_printing['start'] < $now)
                                    $estimated_printing = estimatedPrintedBig($pending_part,  $this->start_date_time->format('Y-m-d H:i:s'), conf_week_working_days()[$day_week], ++ $week);
                                    
                                    while (is_overlap($slot_start, $slot_end, $estimated_printing['start'], $estimated_printing['end'])) {
                                        $estimated_printing = estimatedPrintedBig($pending_part,  $this->start_date_time->format('Y-m-d H:i:s'), conf_week_working_days()[$day_week], ++ $week);
                                        if (! removeOverlapAtNightBig($estimated_printing, $pending_part,  $this->start_date_time->format('Y-m-d H:i:s'))) {}
                                    }
                                    
                            } else {
                                
                                if ($gap_total_minutes < $estimated_printing_total_minutes) {
                                    //Si no cabe en el hueco actual
                                    //echo "Voy por buleráas";
                                    //TODO Antes de pasar a la siguiente semana deberáa intentarse pone en esta
                                    
                                    //Estimo una nueva hora en la semana siguiente
                                    $estimated_printing_aux_0 = estimatedPrintedBig($pending_part,  $this->start_date_time->format('Y-m-d H:i:s'), conf_week_working_days()[$day_week], ++ $week);
                                    
                                    $printerSlotFinal_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->start);
                                    $printerSlotFinal_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->end);
                                    if (is_overlap($printerSlotFinal_start, $printerSlotFinal_end, $estimated_printing_aux_0['start'], $estimated_printing_aux_0['end'])) {
                                        //if (is_overlap(new DateTime($printerSlotFinal->start), new DateTime($printerSlotFinal->end), $estimated_printing_aux_0['start'], $estimated_printing_aux_0['end'])) {
                                        //La nueva fecha estimada se solapa con el slot actual:
                                        //Utilizo el final de la fecha del slot actual para calcular la nueva fecha estimada
                                        /////////////////////////////////////////////////////////////////////////////////////////////////////////
                                        $estimated_printing_aux_1 = array();
                                        
                                        $estimated_printing_aux_1['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->end);
                                        $estimated_printing_aux_1['start'] = new DateTime($printerSlotFinal->end);
                                        $estimated_printing_aux_1['end'] = estimatedPrintingEnd($estimated_printing_aux_1['start'], $pending_part);
                                        
                                        $now = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
                                        //$now = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
                                        if ($estimated_printing_aux_1['start'] >= $now) {
                                            // Si la fecha estimada es mayor o igual que la actual, la fecha estimada es correcta.
                                            $estimated_printing = $estimated_printing_aux_1;
                                        } else {
                                            //Si la nueva fecha estimada de inicio es menor de la actual, no nos sirve. Ya que no podemos poner fechas en el pasado.
                                            //Por lo tanto nos vamos al último días de la semana siguiente y volvemos a calcular una fecha estimada para que ses valida
                                            //en la siguiente iteración.
                                            $day_week = 4;
                                            $week ++;
                                            $estimated_printing = estimatedPrintedBig($pending_part,  $this->start_date_time->format('Y-m-d H:i:s'), conf_week_working_days()[$day_week], $week);
                                        }
                                    } else {
                                        //Si la nueva fecha no se solapa nos sirve, por lo que la recupero;
                                        $estimated_printing = $estimated_printing_aux_0;
                                        //Como para calcular la fecha estimada hemos tendio que saltar de semana, iniciamos el contador de díass;
                                        $day_week = 3;
                                    }
                                    $gap_start = $slot_end;
                                } else {
                                    //Cabe en el hueco pero es de otra semana
                                    $day_week --;
                                    $estimated_printing = estimatedPrintedBig($pending_part,  $this->start_date_time->format('Y-m-d H:i:s'), conf_week_working_days()[$day_week], $week);
                                    //Comprobar que no se solapa con el actual antes de seguir con los otros
                                    //Si se solapa calculamos la fecha estimada un días antes
                                    if (is_overlap($slot_start, $slot_end, $estimated_printing['start'], $estimated_printing['end'])) {
                                        
                                        $day_week = 2;
                                        $week ++;
                                        $estimated_printing = estimatedPrintedBig($pending_part,  $this->start_date_time->format('Y-m-d H:i:s'), conf_week_working_days()[$day_week], $week);
                                        
                                    }
                                }
                            }
                        } else {
                            $gap_start = $slot_end;
                            $arrayIteratorFinal->next();
                        } //END WHILE Iterador Final
                    } //END WHILE Iterador inicial
                } else { //ELSE if ($printerSlot->start && $printerSlot->end)
                    $message = "Warning: Faulty printer slot(data missing). Order:" . $printerSlot->order . '. Part: ' . $printerSlot->part;
                    alert($message);
                } //END if ($printerSlot->start && $printerSlot->end)
                
                $slot_aux_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->start);
                //$slot_aux_start = new DateTime($printer_slot_aux->start);
                $slot_aux_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end);
                //$slot_aux_end = new DateTime($printer_slot_aux->end);
                
                //Asigno cuando el inicio y el fin no son por la noche. o el principio y el fin no son en fin de semana
                $now = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
                //$now = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
                if (! is_overlap($slot_aux_start, $slot_aux_end, $estimated_printing['start'], $estimated_printing['end']) && $estimated_printing['start'] >= $now) {
                    $can_be_assigned = true;
                    break;
                }
                ;
                if ($day_week == 0)
                    $day_week = 4;
                    else
                        $day_week --;
                        
                        $arrayIterator->next();
            } //END While arrayIterator
        } else { //ELSE Si no es el primer slot de $printerTimeLine
            //Add primer slot de la $printerTimeLine actual
            $can_be_assigned = true;
        }
        //echo "</br>------------------------->Big";
        
        if ($can_be_assigned) {
            
            $printerTimeLine->addSlotAndSortAsc($estimated_printing, $pending_part);
            /*
             echo "</br></br></br>";
             $this->print($printerTimeLine);
             echo "</br></br></br>";
             $this->printHtml ($printerTimeLine);
             */
            //break;
        } else {
            get_object_vars($estimated_printing['start']);
            get_object_vars($estimated_printing['end']);
            $message = "Warning: the part could not be assigned. \\nPart: " . $pending_part['id'] . ' \\nOrder: ' . $pending_part['order_id'] . '\\nFile: ' . $pending_part['file_id'] . '\\nEstimate printing start: ' . $estimated_printing['start'] ->format('Y-m-d H:i:s') . '\\nEstimate printing end: ' . $estimated_printing['end'] ->format('Y-m-d H:i:s');
            //alert($message);
        }
        //} //END foreach ($this as $printerTimeLine){
    }
    
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////// M E D I A N /////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function assignPrintersSlotsMedian(array $pending_part, bool &$is_gap_start_ini_change, $printerTimeLine)
    {
        //die;
        //echo "</br></br>";
        //$this->print($printerTimeLine);
        //echo ('</br><b>Printer TimeLine: </b></br>');
        //$this->printHtml();
        
        //foreach ($this as $printerTimeLine){
        $can_be_assigned = false;
        $estimated_printing = array();
        $estimated_printing_ini = array();
        //La primera fecha estimada será la actual,siempre y cuando no se solape con la noche
        //si se solapa me voy al principo de la jornada del días siguiente
        $estimated_printing['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
        //$estimated_printing['start'] = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
        if (is_overlap_at_night($estimated_printing['start'])) {
            $estimated_printing['start']->setTime(conf_workday($estimated_printing['start'])['from']['hour'], conf_workday($estimated_printing['start'])['from']['minute']);
        }
        
        $estimated_printing['start']->modify('+' . conf_delays()['minutes_start'] . ' minutes');
        $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
        
        //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
        if (! removeOverlapAtNightMedian($estimated_printing, $pending_part,  $this->start_date_time->format('Y-m-d H:i:s'))) {}
        
        get_object_vars($estimated_printing['start']);
        get_object_vars($estimated_printing['end']);
        $estimated_printing_ini['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['start'] ->format('Y-m-d H:i:s'));
        //$estimated_printing_ini['start'] = new DateTime($estimated_printing['start'] ->format('Y-m-d H:i:s'));
        $estimated_printing_ini['end'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$estimated_printing['end'] ->format('Y-m-d H:i:s'));
        //$estimated_printing_ini['end'] = new DateTime($estimated_printing['end'] ->format('Y-m-d H:i:s'));
        get_object_vars($estimated_printing_ini['start']);
        get_object_vars($estimated_printing_ini['end']);
        
        ///////////////////////////////////////////////////////////////////////////////
        /////////////////////// C H E C K  -  O V E R L A P S /////////////////////////
        //Incializaciones
        $gap_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
        //$gap_start = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
        if (is_overlap_at_night($gap_start)) {
            //Parece que lo correcto es: if ($gap_start->format('H') > conf_workday()['from']['hour'] ){
            if ($gap_start->format('H') > 23) {
                $gap_start->modify('+1 days');
            }
            $gap_start->setTime(conf_workday($gap_start)['from']['hour'], conf_workday($gap_start)['from']['minute']);
        } else {
            if ($gap_start < $this->start_date_time) {
                $gap_start->setTime($this->start_date_time->format('H'), $this->start_date_time->format('i'));
            } else {
                //$gap_start -> setTime(conf_workday()['to']['hour'],conf_workday()['to']['minute']);
            }
        }
        //Si la hora actual(start_date_time) es mayor que la hora de inicio de jornada entonces el hueco empieza en la hora actual.
        
        $gap_start->modify('+' . conf_delays()['minutes_start'] . ' minutes');
        
        /*
         if (is_overlap_at_night($gap_start)){
         $gap_start -> modify('+1 days');
         }
         */
        
        get_object_vars($gap_start);
        $gap_start_ini  = DateTime::CreateFromFormat('Y-m-d H:i:s',$gap_start->format('Y-m-d H:i:s'));
        //$gap_start_ini = new DateTime($gap_start->date);
        //$is_gap_start_ini_change = false;
        $gap_end = "";
        $gap_end_ini = "";
        $arrayIteratorFinal = $printerTimeLine->getIterator();
        $printer_slot_aux = new PrinterSlot("","", "", "","","","", "", "", "", "", "", "", "");
        
        get_object_vars($gap_start_ini);
        
        $is_first_iteration = true;
        //Si la PrinterTimeLine está vacía se puede asignar sin comprobaciones.
        if (! $arrayIteratorFinal->valid())
            $can_be_assigned = true;
            
            //Recorro los slots actuales ya que la nueva estimación puede solaparse con uno de ellos
            While ($arrayIteratorFinal->valid()) {
                
                $printerSlotFinal = $arrayIteratorFinal->current();
                
                $printer_slot_aux->update($printerSlotFinal);
                //$printer_slot_aux = $arrayIteratorFinal->current();
                
                $slot_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->start);
                //$slot_start = new DateTime($printerSlotFinal->start);
                $slot_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->end);
                //$slot_end = new DateTime($printerSlotFinal->end);
                
                //Actulaizamos el Hueco
                $gap_end = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->start);
                //$gap_end = new DateTime($printerSlotFinal->start);
                
                if ($is_first_iteration) {
                    
                    $gap_end_ini = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->start);
                    //$gap_end_ini = new DateTime($printerSlotFinal->start);
                    if (is_overlap($slot_start, $slot_end, $estimated_printing['start'], $estimated_printing['end'])) {
                        $estimated_printing = estimatedPrintedMedianFromStart($pending_part, $gap_end_ini->modify('+1 minutes'),  $this->start_date_time->format('Y-m-d H:i:s'));
                    }
                    get_object_vars($gap_end_ini);
                    $is_first_iteration = false;
                } elseif ($is_gap_start_ini_change) {
                    
                    //$gap_end_ini = new DateTime($printerSlotFinal->start);
                    
                    $printerSlotFinal_aux = DateTime::CreateFromFormat('Y-m-d H:i:s',$printerSlotFinal->start);
                    //$printerSlotFinal_aux = new DateTime($printerSlotFinal->start);
                    
                    $estimated_printing = estimatedPrintedMedianFromStart($pending_part, $printerSlotFinal_aux,  $this->start_date_time->format('Y-m-d H:i:s'));
                    
                    /*
                     get_object_vars($estimated_printing['end']);
                     get_object_vars($estimated_printing['start']);
                     //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
                     if (!removeOverlapAtNightMedian($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))){
                     }
                     removeOverlapPastFromStart($pending_part,$estimated_printing, $this->start_date_time->format('Y-m-d H:i:s'));
                     */
                    
                    $is_gap_start_ini_change = false;
                }
                
                get_object_vars($slot_start);
                get_object_vars($slot_end);
                get_object_vars($gap_start);
                get_object_vars($gap_end);
                $gap_total_minutes = rangeTotalM($gap_start, $gap_end);
                //Si se solapan el slot y el tiempo estimado, buscamos el tiempo estimado en los huecos
                if (is_overlap($slot_start, $slot_end, $estimated_printing['start'], $estimated_printing['end'])) {
                    $arrayIteratorFinal->next();
                    
                    //Hay que comprobar que el gap no se solapa con la noche y se es así arreglarlo
                    
                    //if (removeOverlapWeekendGap($gap_start)){}
                    
                    //Comprobamos si la fecha estimada cabe en el hueco de la semana que estamos trabajando.
                    $estimated_printing_total_minutes = rangeTotalM($estimated_printing['start'], $estimated_printing['end']);
                    if (($gap_total_minutes >= $estimated_printing_total_minutes) &&
                        //Cabe en el hueco
                        ($slot_end->format('W') == $estimated_printing['start']->format('W')) &&
                        //La semana de fecha de incio estimada es de la misma semana que la del fin del slot
                        //El hueco es de misma semana que la del fin del slot
                        ($slot_end->format('W') == $gap_start->format('W')))
                    {
                        //El tiempo estimado empezará al principio del hueco
                        $estimated_printing = estimatedPrintedMedianFromStart($pending_part, $gap_start->modify('+1 minutes'),  $this->start_date_time->format('Y-m-d H:i:s'));
                        
                        //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
                        /*
                         if (!removeOverlapAtNightMedian($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))){
                         }
                         
                         //Si la fecha de inicio estimada es del pasado hay que volver a calcularla
                         removeOverlapPastFromStart($pending_part,$estimated_printing, $this->start_date_time->format('Y-m-d H:i:s'));
                         */
                        if (is_overlap($slot_start, $slot_end, $estimated_printing['start'], $estimated_printing['end'])) {
                            $estimated_printing['start'] = $slot_end;
                            $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
                            
                            //Si la fecha de inicio estimada es del pasado hay que volver a calcularla
                            removeOverlapPastFromStart($pending_part, $estimated_printing,  $this->start_date_time->format('Y-m-d H:i:s'));
                        }
                        
                        $gap_start = $slot_end;
                        if ($is_gap_start_ini_change) {
                            get_object_vars($gap_start);
                            $gap_start_ini= DateTime::CreateFromFormat('Y-m-d H:i:s',$gap_start ->format('Y-m-d H:i:s'));
                            //$gap_start_ini = new DateTime($gap_start->date);
                            //$is_gap_start_ini_change = false;
                        }
                        
                        /*
                         while (is_overlap($slot_start, $slot_end, $estimated_printing['start'],$estimated_printing['end'])) {
                         $estimated_printing = estimatedPrintedMedianFromStart($pending_part,  $gap_start->modify('+5 minutes'));
                         
                         if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
                         }
                         
                         }
                         */
                        /*
                         $night = estimatedPrintingNights($estimated_printing_aux);
                         
                         if (is_overlap_whithout_wrap($night['behind']['start'], $night['behind']['end'], $estimated_printing_aux['start'],$estimated_printing_aux['end'])){
                         //break;
                         $estimated_printing = estimatedPrintedMedianFromStart($pending_part,  $gap_start->modify('+1 minutes'));
                         }else{
                         $estimated_printing =  $estimated_printing_aux;
                         }
                         
                         if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
                         }
                         */
                        //Como Cabe en el hueco de esta samana: Intento ajustar la fecha estimada para que no halla solapamientos.
                        //Si puedo ajustarla la fecha sirve por lo que dejo de buscar, en caso contrario tengo que seguir buscando
                        
                        
                        //¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ O J O !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                        //La comprobación de solapamiento con el slot actual de la nueva fecha estimada la hace en la siguiente vuelta.
                    } else {
                        //Si no cabe en el hueco actual, busco en el siguiente hueco
                        //El tiempo estimado empezará al final del hueco,para que nos de un solapamiento en la siguiente iteración
                        //y calcule el nuevo hueco despues del salto con el que se ha solapado
                        
                        $estimated_printing = estimatedPrintedMedianFromStart($pending_part, $slot_end->modify('+1 minutes'),  $this->start_date_time->format('Y-m-d H:i:s'));
                        /*
                         get_object_vars($estimated_printing['end']);
                         get_object_vars($estimated_printing['start']);
                         //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
                         if (!removeOverlapAtNightMedian($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))){
                         }
                         //Si la fecha de inicio estimada es del pasado hay que volver a calcularla
                         removeOverlapPastFromStart($pending_part,$estimated_printing, $this->start_date_time->format('Y-m-d H:i:s'));
                         */
                        $gap_start = $slot_end;
                        if ($is_gap_start_ini_change) {
                            get_object_vars($gap_start);
                            $gap_start_ini = DateTime::CreateFromFormat('Y-m-d H:i:s',$gap_start ->format('Y-m-d H:i:s'));
                            //$gap_start_ini = new DateTime($gap_start->date);
                            //$is_gap_start_ini_change = false;
                        }
                        
                        /*
                         if ($gap_total_minutes < $estimated_printing_total_minutes ){
                         
                         $estimated_printing = estimatedPrintedMedianFromStart($pending_part, $slot_end->modify('+1 minutes'), $this->start_date_time->format('Y-m-d H:i:s'));
                         get_object_vars($estimated_printing['end']);
                         get_object_vars($estimated_printing['start']);
                         //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
                         if (!removeOverlapAtNightMedian($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))){
                         }
                         //Si la fecha de inicio estimada es del pasado hay que volver a calcularla
                         removeOverlapPastFromStart($pending_part,$estimated_printing, $this->start_date_time->format('Y-m-d H:i:s'));
                         
                         $gap_start = $slot_end;
                         if ($is_gap_start_ini_change){
                         get_object_vars($gap_start);
                         $gap_start_ini =  new DateTime($gap_start->date);
                         //$is_gap_start_ini_change = false;
                         
                         }
                         
                         }else{
                         //Cabe en el hueco pero es de otra semana, busco en el siguiente días.
                         $estimated_printing = estimatedPrintedMedianFromStart($pending_part, $slot_end->modify('+1 minutes'), $this->start_date_time->format('Y-m-d H:i:s'));
                         //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
                         if (!removeOverlapAtNightMedian($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))){
                         }
                         //Si la fecha de inicio estimada es del pasado hay que volver a calcularla
                         removeOverlapPastFromStart($pending_part,$estimated_printing, $this->start_date_time->format('Y-m-d H:i:s'));
                         
                         $gap_start = $slot_end;
                         if ($is_gap_start_ini_change){
                         get_object_vars($gap_start);
                         $gap_start_ini =  new DateTime($gap_start->date);
                         //$is_gap_start_ini_change = false;
                         
                         }
                         
                         //Comprobar que no se solapa con el actual antes de seguir con los otros
                         //Si se solapa calculamos la fecha estimada un días antes
                         }
                         */
                    }
                } else {
                    /*
                     $estimated_printing = estimatedPrintedMedianFromStart($pending_part, $gap_start->modify('+1 minutes'), $this->start_date_time->format('Y-m-d H:i:s'));
                     get_object_vars($estimated_printing['end']);
                     get_object_vars($estimated_printing['start']);
                     //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
                     if (!removeOverlapAtNightMedian($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))){
                     }
                     removeOverlapPastFromStart($pending_part,$estimated_printing, $this->start_date_time->format('Y-m-d H:i:s'));
                     */
                    $gap_start = $slot_end;
                    
                    if ($is_gap_start_ini_change) {
                        get_object_vars($gap_start);
                        $gap_start_ini = DateTime::CreateFromFormat('Y-m-d H:i:s',$gap_start ->format('Y-m-d H:i:s'));
                        //$gap_start_ini = new DateTime($gap_start->date);
                        //$is_gap_start_ini_change = false;
                    }
                    
                    $arrayIteratorFinal->next();
                }
            } //END WHILE
            
            $slot_aux_start = DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->start);
            //$slot_aux_start = new DateTime($printer_slot_aux->start);
            $slot_aux_end= DateTime::CreateFromFormat('Y-m-d H:i:s',$printer_slot_aux->end);
            //$slot_aux_end = new DateTime($printer_slot_aux->end);
            
            //Compruebo si la estimación inicial ha cambiado, si no ha cambiado es que no se va asignar,
            //esto puedo ocurrir  por dos motivos:
            //Que la pieza cabe en el hueco inicial o primer hueco disponible, la fecha estimada se calcula en el hueco inicial
            //O que la pieza no cabe en los huecos actuales, la fecha estima está despues del último slot que es el slot aux
            
            if (($estimated_printing_ini['start'] == $estimated_printing['start']) && ($estimated_printing_ini['end'] == $estimated_printing['end'])) {
                
                $estimated_printing_end = estimatedPrintingEnd($gap_start_ini, $pending_part);
                
                if ($estimated_printing_end <= $gap_end_ini) {
                    //la pieza cabe en el hueco inicial
                    //if (is_wrap($gap_start_ini,$gap_end_ini,$estimated_printing['start'],$estimated_printing['end'])){
                    $estimated_printing['start'] = $gap_start_ini;
                    $estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start'], $pending_part);
                    
                    //Hay que actualizar el $gap_start_ini
                    $is_gap_start_ini_change = true;
                } else {
                    //la pieza no cabe en los huecos actuales
                    get_object_vars($slot_aux_end);
                    $estimated_printing['start'] = DateTime::CreateFromFormat('Y-m-d H:i:s',$slot_aux_end->format('Y-m-d H:i:s'));
                    //$estimated_printing['start'] = new DateTime($slot_aux_end->date);
                    $estimated_printing = estimatedPrintedMedianFromStart($pending_part, $estimated_printing['start']->modify('+1 minutes'),  $this->start_date_time->format('Y-m-d H:i:s'));
                    //$estimated_printing['end'] = estimatedPrintingEnd($estimated_printing['start']->modify('+1 minutes'),$pending_part);
                    //if (!removeOverlapAtNightMedian($estimated_printing, $pending_part)){
                    /*
                     if (!removeOverlapAtNightMedian($estimated_printing, $pending_part, $this->start_date_time->format('Y-m-d H:i:s'))){
                     }
                     removeOverlapPastFromStart($pending_part,$estimated_printing, $this->start_date_time->format('Y-m-d H:i:s'));
                     */
                }
            }
            
            //Asigno cuando el inicio y el fin no son por la noche. o el principio y el fin no son en fin de semana
            $now = DateTime::CreateFromFormat('Y-m-d H:i:s',$this->start_date_time->format('Y-m-d H:i:s'));
            //$now = new DateTime( $this->start_date_time->format('Y-m-d H:i:s'));
            if (! is_overlap($slot_aux_start, $slot_aux_end, $estimated_printing['start'], $estimated_printing['end']) && $estimated_printing['start'] >= $now) {
                $can_be_assigned = true;
                //break;
            }
            ;
            
            if ($can_be_assigned) {
                $printerTimeLine->addSlotAndSortAsc($estimated_printing, $pending_part);
                /*
                 echo "</br></br></br>";
                 $this->print($printerTimeLine);
                 echo "</br></br></br>";
                 $this->printHtml ($printerTimeLine);
                 */
                //break;
            } else {
                get_object_vars($estimated_printing['start']);
                get_object_vars($estimated_printing['end']);
                $message = "Warning: the part could not be assigned. \\nPart: " . $pending_part['id'] . ' \\nOrder: ' . $pending_part['order_id'] . '\\nFile: ' . $pending_part['file_id'] . '\\nEstimate printing start: ' . $estimated_printing['start'] ->format('Y-m-d H:i:s') . '\\nEstimate printing end: ' . $estimated_printing['end'] ->format('Y-m-d H:i:s');
                //alert($message);
            }
            
            //} //END FOREACH ($this as $printerTimeLine){
            return $is_gap_start_ini_change;
    }
    
    //END FUNCTION
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////// S A V E - A S S I G N - P R I N T E R - S L O T S //////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function updateAssignPrintersMatrix($action, $get_part_id = null)
    {
        //Recorrer las lineas del tiempo guardando como asignados los slots
        $result = false;
        foreach ($this as $printerTimeLine) {
            foreach ($printerTimeLine as $printerSlot) {
                if ($printerSlot->state == 8) {
                    //$start = DateTimeG::CleanDatetimeForBD($printerSlot->start);
                    //$end   = DateTimeG::CleanDatetimeForBD($printerSlot->end);
                    
                    if (($get_part_id != null) && ($get_part_id ==  $printerSlot->part) && isset($printerTimeLine->last_slot_out_range->part)){
                        if ($action = 'play'){
                            //Marcamos como impresa la pieza anterior a la que hemos arrancado
                            $sql = "UPDATE part
                                    SET state_id = 10
                                    WHERE id = " . $printerTimeLine->last_slot_out_range->part;
                            $result = DataBase::getConnection()->query($sql);
                        }else{
                            //Marcamos como impresa la pieza anterior a la que hemos arrancado
                            $sql = "UPDATE part
                                    SET state_id = 8
                                    WHERE id = " . $printerTimeLine->last_slot_out_range->part;
                            $result = DataBase::getConnection()->query($sql);
                        }
                    }else{
                        $sql = "UPDATE part
                                    SET printer_id = " . $printerTimeLine->printer_id . ", state_id = 9 , start_datetime = '" . $printerSlot->start . "', end_datetime = '" . $printerSlot->end. "'
                                    WHERE id = " . $printerSlot->part;
                        $result = DataBase::getConnection()->query($sql);
                        $printerSlot->printer_id = $printerTimeLine->printer_id;
                        $printerSlot->state = 9;
                    }
                    $sql = "UPDATE part
                            SET printer_id = " . $printerTimeLine->printer_id . ", state_id = 9 , start_datetime = '" . $printerSlot->start . "', end_datetime = '" . $printerSlot->end. "'
                            WHERE id = " . $printerSlot->part;
                    $result = DataBase::getConnection()->query($sql);
                    $printerSlot->printer_id = $printerTimeLine->printer_id;
                    $printerSlot->state = 9;
                }
                

                //$count++;
            }
            
            
        }
        return $result ;
    }
    
    public function clearAssignPrintersMatrix()
    {
        $sql = "UPDATE part
                SET printer_id = NULL, state_id = 8,  start_datetime =NULL,end_datetime=NULL
                 WHERE initiated <> 1; ";

        $result = DataBase::getConnection()->query($sql);
        foreach ($this as $printerTimeLine) {
            foreach ($printerTimeLine as $printerSlot) {
                if ($printerSlot->initiated != 1) {
                    $printerSlot->printer = NULL;
                    $printerSlot->state = 8;
                    $printerSlot->start_datetime = NULL;
                    $printerSlot->end_datetime = NULL;
                }
            }
        }
        return $result;
    }
    
    public function deleteOrders()
    {
        $sql = "TRUNCATE TABLE order_g;";
        $result = DataBase::getConnection()->query($sql);
        
        $sql = "TRUNCATE TABLE order_line;";
        $result = DataBase::getConnection()->query($sql);
        
        $sql = "TRUNCATE TABLE final_product;";
        $result = DataBase::getConnection()->query($sql);
        
        $sql = "TRUNCATE TABLE part;";
        $result = DataBase::getConnection()->query($sql);
        
        $sql = "ALTER TABLE part AUTO_INCREMENT=1000;";
        $result = DataBase::getConnection()->query($sql);
        
        return $result;
    }
    
    public function deleteUnassigned()
    {
        
        $sql = sprintf('
                SELECT  *
                FROM    part AS par
                WHERE   state_id = 8
                ORDER BY id;
               ');
        $missing_parts = DataBase::getConnection()->query($sql);
        $orders = array();
        $final_products = array();
        foreach ($missing_parts as $unassigned_part) {
            if (!in_array($unassigned_part['order_id'],$orders)){
                array_push($orders, $unassigned_part['order_id']);
            }
            if (!in_array($unassigned_part['final_product_id'],$final_products)){
                array_push($final_products, $unassigned_part['final_product_id']);
            }
            $result = DataBase::getConnection()->query($sql);
        }
        
        foreach ($orders as $order){
            $sql = "DELETE FROM order_g WHERE id=".$order.";";
            $result = DataBase::getConnection()->query($sql);
            $sql = "DELETE FROM order_line WHERE order_id=".$order.";";
            $result = DataBase::getConnection()->query($sql);
        }
        
        foreach ($final_products as $final_product){
            $sql = "DELETE FROM final_product WHERE id=".$final_product.";";
            $result = DataBase::getConnection()->query($sql);
        }
        
        $sql = "DELETE FROM part WHERE state_id=8;";
        $result = DataBase::getConnection()->query($sql);
        

        
        return $result;
    }
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////// R E C O V E R  -  S E P A R A T E D  -  P A R T S //////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function separatedUnassignedParts()
    {
        $sql = sprintf("UPDATE part
                    SET state_id = 7
                    WHERE state_id = 8");
        $result = DataBase::getConnection()->query($sql);
        //ORDER BY p.weight DESC,p.order_id  ASC
        foreach ($this as $printerTimeLine){
            foreach ($printerTimeLine as $slot){
                if ($slot->state == 8){
                    $slot->state = 7;
                }
            }
        }
        return $result;
    }
    
    
    
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////////////// R E C O V E R  -  S E P A R A T E D  -  P A R T S //////////////////////////////////////////////////////
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    public function recoverSeparatedUnssignedParts()
    {
        $sql = "UPDATE part
                SET state_id = 8
                WHERE state_id = 7";
        $result = DataBase::getConnection()->query($sql);
        foreach ($this as $printerTimeLine){
            foreach ($printerTimeLine as $slot){
                if ($slot->state == 7){
                    $slot->state = 8;
                }
            }
        }
        return $result;
        
    }
    
    
    public function columns($rows){
        $columns = ($this->count()/($rows+1))-1;
        if (($this->count() % ($rows+1))  != 0){
            $columns = intdiv($this->count(),$rows+1);
        }
        return $columns;
    }
    
    
    public function createStatePrintersMatrix($reverse,$rows){
        $this->start_date_time = new DateTimeG ();
        $state_printer_matrix = array();
        
        //$rows = $rows-1; //El -1 por que los indicies empiezan en 0
        $columns = $this->columns($rows);
        
        $cont = 1;
        $row=0;
        $column=0;
        
        if ($reverse){
            $column=$columns;
        }
        
        foreach ($this as $PrinterLine){
                //Calculo el DateTime en que se empieza a dibujar la regla (marcas de tiempo)
                $start_workday = new DateTimeG($this->start_date_time_print->format('Y-m-d H:i:s'));
                if ((intval($start_workday->format('H')) < intval(conf_workday($start_workday)['from']['hour']))) {
                    $start_workday->modify('-1 days');
                }
                $start_workday->setTime(conf_workday($start_workday)['from']['hour'], conf_workday($start_workday)['from']['minute']);
                
                usort($PrinterLine->rolls_end, 'sort_by_datetime');
                
                
                $printerTimeLineAux = new PrinterTimeline($PrinterLine->printer_id,
                    $PrinterLine->code_printer,
                    $PrinterLine->roll_weight,
                    $PrinterLine->roll_replacement_datetime);
                
                $printerTimeLineAux->chargePrinterTimeline($PrinterLine->roll_replacement_datetime,$this->start_date_time,conf_neighborhood_factory_id());
                //$printerTimeLineAux->chargePrinterTimeline($this->start_date_time_print);
                //$this->assignPrintersSlots();
                
                //TODO Jose 20/05/2022 Análisis
                $printerTimeLineAux->filamentCalculations ($PrinterLine->roll_replacement_datetime);
                
                //$printerTimeLineAux->filamentCalculations ($this->start_date_time_print);
                
                
                if (count($printerTimeLineAux) == 0){
                    $printerTimeLineAux = $PrinterLine;
                }
                
                //Asigno estilos
                $color="";
                $blink_class =  "";
                foreach ($printerTimeLineAux->rolls_end as $roll_end){
                    $diff = $roll_end['datetime']->diffRangeHM($this->start_date_time);
                    $color="";
                    $blink_class =  "";
                    if ($diff['hours']<10){
                        $color =  "red";
                    }
                    if (($diff['hours']<1)&& ($diff['minutes']<30)){
                        $blink_class =  "blink";
                    }
                    $PrinterLine->color= $color;
                    $PrinterLine->blink_class= $blink_class;
                    break;
                }
                
                $printerState = new PrinterState($PrinterLine->printer_id, $PrinterLine->code_printer,$PrinterLine->last_slot_out_range, $PrinterLine->roll_weight, $PrinterLine->roll_replacement_datetime,$PrinterLine->rolls_end);
            $order_id=0;
            if ($PrinterLine->available == 1){
                $is_next_slot = false;
                $is_current_slot = false;
                
                $printer_slot_aux = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
                $printer_slot_first = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
                $printer_slot_second = new PrinterSlot("","", "", "","","","","", "", "", "", "", "", "","");
                
                
                $count = 0;
                
                
                foreach ($PrinterLine as $printer_slot){
                    $order_id = $printer_slot->order; 
                    if ($printer_slot->state == 8){
                        continue;
                    }
                    if ($count==0){
                        $printer_slot_first->update($printer_slot);
                    }elseif  ($count==1){
                        $printer_slot_second->update($printer_slot);
                    }
                    $slot_start = new DateTimeG($printer_slot->start);
                    $slot_end = new DateTimeG($printer_slot->end);
                    
                    if  (
                        ($printer_slot->state == 9)
                        &&
                        ($printer_slot->initiated == 1)
                        ){
                            $printerState->current_slot = $printer_slot;
                            $is_current_slot = true;
                    }elseif ($is_current_slot){
                        $is_next_slot = true;
                        $printerState->next_slot = $printer_slot;
                        break;
                    }
                    
                    $printer_slot_aux ->update($printer_slot);
                    $count ++;
                }
                
                if ((!$is_current_slot)  && isset($printer_slot_first->part) && ($printer_slot_first->part != "")){
                    //Ojo que puede haber una arrancada del día anterior
                    
                    if ($printer_slot_first->initiated == 0){
                        if (isset($PrinterLine->last_slot_out_range)){
                            $printerState->current_slot = $PrinterLine->last_slot_out_range;
                            $printerState->next_slot = $printer_slot_first;
                            $is_current_slot = true;
                            $is_next_slot = true;
                        }else {
                            $printerState->current_slot = $printer_slot_first;
                            $is_current_slot = true;
                        }
                    }else{
                        $printerState->current_slot = $printer_slot_first;
                        $is_current_slot = true;
                        if (($printer_slot_second->part != "") && ($printer_slot_second->initiated == 0)){
                            $printerState->next_slot = $printer_slot_second;
                            $is_next_slot = true;
                        }elseif (($printer_slot_second->part != "") && ($printer_slot_second->initiated == 1)){
                            $printerState->current_slot = $printer_slot_second;
                            if ($printer_slot_aux->part != $printer_slot_second->part){
                                $printerState->next_slot = $printer_slot_aux;
                                $is_next_slot = true;
                            }
                        }
                        
                    }
                } elseif (($printer_slot_first->part != "") 
                        && (isset($PrinterLine->last_slot_out_range))
                        && ($PrinterLine->last_slot_out_range->state == 9) 
                        && ($PrinterLine->last_slot_out_range->initiated == 1)){
                    $printerState->current_slot = $PrinterLine->last_slot_out_range;
                    $is_current_slot = true;
                }
                $light = "red";
                if (($is_current_slot)&&($printerState->current_slot!=null)){
                    //if (isset($printerState->current_slot->start) && ($printerState->current_slot->start != null)){
                    $current_start = new DateTimeG($printerState->current_slot->start);
                    $current_end = new DateTimeG($printerState->current_slot->end);
                    
                    $current_end_minus_10 = new DateTimeG($printerState->current_slot->end);
                    $current_end_minus_10->modify('-10 minutes');
                    
                    if (($printerState->current_slot->initiated == 0)||($printerState->current_slot->state == 10)){
                        $light ="red";
                    }elseif (( $this->start_date_time >= $current_start )&&($this->start_date_time <= $current_end)){
                        if (($this->start_date_time >= $current_end_minus_10) && ($this->start_date_time <= $current_end)){
                            $light = "blue";
                        }else{
                            $light="green";
                        }
                    }else{
                        if (($this->start_date_time >= $current_end) && ($this->start_date_time <= $current_start)){
                            $light ="red";
                        }
                    }
                    
                    $printerState->lamp = $light;
                    
                    $printerState->current_time_printing = $current_start->diffRangeHM($this->start_date_time);
                    $printerState->current_time_left = $this->start_date_time->diffRangeHM($current_end);
                    $current_time_total_minutes = $current_start->diffMinutes($current_end);
                    $current_time_printing_minutes = $current_start->diffMinutes($this->start_date_time);
                    $printerState->current_percent_printing = number_format($current_time_printing_minutes * 100/$current_time_total_minutes, 2, ',','.');
                    
                    $slot_interval_last_current = $current_start->diff($current_end);
                    $slot_total_hours_real_current = $slot_interval_last_current->format('%d') * 24 + $slot_interval_last_current->format('%H');
                    
                    $printerState->current_days = $slot_interval_last_current->format('%d');
                    $printerState->current_time = $slot_total_hours_real_current. ':' . $slot_interval_last_current->format('%i');
                    
                    
                    $printerState->current_start_compact = $current_start->format('H:i - d F ');
                    $printerState->current_end_compact = $current_end->format('H:i - d F');
                    
                    
                    $printerState->current_button = "printed";
                    if ($printerState->current_slot->state == 9){
                        if ($printerState->current_slot->initiated == 0){
                            $printerState->current_button = "play";
                        }else{
                            $printerState->current_button = "cancel";
                        }
                    }
                    
                    $printerState->current_action = "printed";
                    if ($printerState->current_slot->state == 9){
                        if ($printerState->current_slot->initiated == 0){
                            $printerState->current_action = "play";
                        }else{
                            $printerState->current_action = "cancel";
                        }
                    }
                }
                
                
                if (($is_next_slot)&&($printerState->next_slot !=null)){
                    //if (isset($printerState->next_slot->start) && ($printerState->next_slot->start != null)){
                    $next_start = new DateTimeG($printerState->next_slot->start);
                    $next_end = new DateTimeG($printerState->next_slot->end);
                    
                    $next_end_minus_10 = new DateTimeG($printerState->next_slot->end);
                    $next_end_minus_10->modify('-10 minutes');
                    
                    
                    $printerState->lamp = $light;
                    
                    $printerState->next_time_printing = $next_start->diffRangeHM($this->start_date_time);
                    $printerState->next_time_left = $this->start_date_time->diffRangeHM($next_end);
                    $next_time_total_minutes = $next_start->diffMinutes($next_end);
                    $next_time_printing_minutes = $next_start->diffMinutes($this->start_date_time);
                    $printerState->next_percent_printing = number_format($next_time_printing_minutes * 100/$next_time_total_minutes, 2, ',','.');
                    
                    $slot_interval_last_next = $next_start->diff($next_end);
                    $slot_total_hours_real_next = $slot_interval_last_next->format('%d') * 24 + $slot_interval_last_next->format('%H');
                    
                    $printerState->next_days = $slot_interval_last_next->format('%d');
                    $printerState->next_time = $slot_total_hours_real_next. ':' . $slot_interval_last_next->format('%i');
                    
                    
                    $printerState->next_start_compact = $next_start->format('H:i - d F ');
                    $printerState->next_end_compact = $next_end->format('H:i - d F');
                    
                    
                    $printerState->next_button = "printed";
                    if ($printerState->next_slot->state == 9){
                        if ($printerState->next_slot->initiated == 0){
                            $printerState->next_button = "play";
                        }else{
                            $printerState->next_button = "cancel";
                        }
                    }
                    
                    $printerState->next_action = "printed";
                    if ($printerState->next_slot->state == 9){
                        if ($printerState->next_slot->initiated == 0){
                            $printerState->next_action = "play";
                        }else{
                            $printerState->next_action = "cancel";
                        }
                    }
                }
                
                
                if ( $PrinterLine->last_slot_out_range != null){
                    
                    if ( 
                            ($printerState->current_slot ==  null)
                            ||
                            (
                                ($printerState->current_slot !=  null) 
                                && 
                                ($printerState->current_slot->part == $PrinterLine->last_slot_out_range->part)
                                //&&
                                //(isset($printerState->next_slot))
                                &&
                                ($printerState->next_slot ==  null)
                            )
                       ){
                        if ($PrinterLine->last_slot_out_range->state == 10){
                            $printerState->lamp = "brown";
                        }else{
                            $printerState->lamp = "magenta";
                        }
                        
                    }
                    
                    $printerState->last_slot_out_range = $PrinterLine->last_slot_out_range;
                    
                    $last_slot_out_range_start = new DateTimeG($PrinterLine->last_slot_out_range->start);
                    $last_slot_out_range_end = new DateTimeG($PrinterLine->last_slot_out_range->end);
                    
                    
                    $printerState->last_slot_out_range_time_printing = $last_slot_out_range_start->diffRangeHM($this->start_date_time);
                    $printerState->last_slot_out_range_time_left = $this->start_date_time->diffRangeHM($last_slot_out_range_end);
                    $last_slot_out_range_time_total_minutes = $last_slot_out_range_start->diffMinutes($last_slot_out_range_end);
                    $last_slot_out_range_time_printing_minutes = $last_slot_out_range_start->diffMinutes($this->start_date_time);
                    $printerState->last_slot_out_range_percent_printing = number_format($last_slot_out_range_time_printing_minutes * 100/$last_slot_out_range_time_total_minutes, 2, ',','.');
                    
                    $slot_interval_last_next = $last_slot_out_range_start->diff($last_slot_out_range_end);
                    $slot_total_hours_real_next = $slot_interval_last_next->format('%d') * 24 + $slot_interval_last_next->format('%H');
                    
                    $printerState->last_slot_out_range_days = $slot_interval_last_next->format('%d');
                    $printerState->last_slot_out_range_time = $slot_total_hours_real_next. ':' . $slot_interval_last_next->format('%i');
                    
                    $printerState->last_slot_out_range_start_compact = $last_slot_out_range_start->format('H:i - d F ');
                    $printerState->last_slot_out_range_end_compact = $last_slot_out_range_end->format('H:i - d F');
                }elseif ($PrinterLine->count() == 0){
                    $printerState->lamp = 'white';
                    $printerState->current_slot= NULL;
                    $printerState->next_slot= NULL;
                    $printerState->last_slot_out_range_days = NULL;
                }
                
            }else{
                $printerState->lamp = 'grey';
                $printerState->current_slot= NULL;
                $printerState->next_slot= NULL;
                $printerState->last_slot_out_range_days = NULL;
            }
            $printerState->available = $PrinterLine->available;
            $printerState->order_id = $order_id;
            
            $state_printer_matrix[$row][$column] = $printerState;
            
            //$state_printer_matrix[$row][$column] = $cont;
            if ($cont%($rows+1) == 0){
                if ($reverse){
                    $column --;
                }else{
                    $column ++;
                }
            }
            
            if ($row == $rows){
                $row = 0;
            }else{
                $row ++;
            }
            
            $cont ++;
                

        }
        
        return $state_printer_matrix;
    }
    
    
}//END CLASS
?>