<?php
namespace App\Vendor\Greeny\Main;

use MySQLi;

// Db configs.
define('HOST', 'localhost');
define('PORT', 3307);
define('DATABASE', 'greeny-manager');
define('USERNAME', 'root');
define('PASSWORD', '');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////// D A T A   B A S E //////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
class DataBase
{
    private static $db;
    private $connection;

    private function __construct()
    {
        $this->connection = new MySQLi(HOST, USERNAME, PASSWORD, DATABASE, PORT);
    }

    function __destruct()
    {
        $this->connection->close();
    }

    public static function getConnection()
    {
        if (self::$db == null) {
            self::$db = new Database();
        }
        return self::$db->connection;
    }
    
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////// Q U E R I E S //////////////////////////////////////////////////////////////////
    
    
    public static function queryTimeWithoutOperators(int $neighborhood_factory_id){
        $sql = sprintf('
                SELECT *
                FROM  time_without_operators AS two
                WHERE neighborhood_factory_id  = ' . $neighborhood_factory_id . ' AND active = 1
                ORDER BY day;');
        return $sql;
    }
    
    public static function queryAvailablePrinters(int $neighborhood_factory_id){
        $sql = sprintf('
                SELECT  *
                FROM    printer AS pri
                WHERE   available = 1 AND neighborhood_factory_id = ' . $neighborhood_factory_id . '
                ORDER BY pri.id;
               ');
        return $sql;
    }
    
    public static function queryAllPrinters(int $neighborhood_factory_id){
        $sql = sprintf('
                SELECT  *
                FROM    printer AS pri
                WHERE   neighborhood_factory_id = ' . $neighborhood_factory_id . '
                ORDER BY pri.id;
               ');
        return $sql;
    }
    
    public static function queryPendingParts(int $neighborhood_factory_id){
        return  sprintf('
                SELECT  p.id,p.file_id,p.file_part_name,f.name AS file_name, p.version_part_name,p.order_id,fp.product_id, pr.name AS product_name, p.final_product_id,p.state_id,p.weight,
                        f.estimated_printing_hours,f.estimated_printing_minutes,f.filament_used,p.start_datetime,p.initiated,o.neighborhood_id
                FROM    part               AS p
                INNER   JOIN file          AS f ON p.file_id = f.id
                INNER   JOIN final_product AS fp ON fp.id = p.final_product_id
                INNER   JOIN product       AS pr ON pr.id = fp.product_id
                INNER   JOIN order_g       AS o  ON o.id = p.order_id
                WHERE   p.state_id = 8 AND neighborhood_id = ' . $neighborhood_factory_id . ' AND o.state_id <> 21
                ORDER BY   p.order_id ASC,p.final_product_id  ASC,  f.estimated_printing_hours DESC, f.estimated_printing_minutes DESC;

                ');
    }
    
    
    public static function chargePrinterTimeline(int $printer_id,int $neighborhood_factory_id){
        return sprintf('
            SELECT      pri.id,code,neighborhood_factory_id,pri.state_id AS printer_state_id ,roll_replacement_datetime, roll_weight,
                        f.name AS file_name, pr.name AS product_name, fp.product_id, par.*
            FROM    printer AS pri
            LEFT JOIN part AS par ON pri.id = par.printer_id
            INNER JOIN file AS f ON par.file_id = f.id 
            INNER   JOIN final_product AS fp ON fp.id = par.final_product_id
            INNER   JOIN product       AS pr ON pr.id = fp.product_id
            INNER   JOIN order_g       AS o ON o.id = par.order_id
            WHERE   pri.id = ' . $printer_id. ' AND neighborhood_factory_id = ' . $neighborhood_factory_id . 'AND o.state_id <> 21
            ORDER BY start_datetime, order_id, pri.id;
           ');
    }
    
    
    public static function querySelectReassignPart(int $id_part,int $neighborhood_factory_id){
        return sprintf('
                SELECT  p.id,p.file_id, p.file_part_name, f.name AS file_name, p.version_part_name,p.order_id,fp.product_id, pr.name AS product_name,final_product_id,p.state_id,p.weight,
                        f.estimated_printing_hours,f.estimated_printing_minutes,f.filament_used,
                        p.start_datetime,p.initiated, p.end_datetime,p.printer_id,o.neighborhood_id
                FROM    part AS p
                INNER   JOIN file AS f ON p.file_id = f.id 
                INNER   JOIN final_product AS fp ON fp.id = p.final_product_id
                INNER   JOIN product       AS pr ON pr.id = fp.product_id
                INNER   JOIN order_g       AS o ON o.id = p.order_id
                WHERE   p.id =' . $id_part . ' AND neighborhood_id = ' . $neighborhood_factory_id . ' AND o.state_id <> 21;
                '); 
            //ORDER BY p.order_id  ASC, p.final_product_id  ASC, f.estimated_printing_hours DESC, f.estimated_printing_minutes DESC,p.id;
        
    }
    public static function queryChargePrintersMatrix(int $printer_id,int $neighborhood_factory_id){
        //TODO solo cargar los pedidos pendientes
        return sprintf('
                SELECT  pri.id,code,neighborhood_factory_id,pri.state_id AS printer_state_id ,roll_replacement_datetime, roll_weight, 
                        f.name AS file_name, fp.product_id, pr.name AS product_name,final_product_id,o.neighborhood_id,par.*
                FROM    printer AS pri
                LEFT    JOIN part AS par ON pri.id = par.printer_id
                INNER   JOIN file          AS f ON par.file_id = f.id
                INNER   JOIN final_product AS fp ON fp.id = par.final_product_id
                INNER   JOIN product       AS pr ON pr.id = fp.product_id
                INNER   JOIN order_g       AS o ON o.id = par.order_id
                WHERE   pri.id = ' . $printer_id . ' AND neighborhood_factory_id = ' . $neighborhood_factory_id . ' AND o.state_id <> 21 
                ORDER BY start_datetime, order_id, pri.id;
           ');
        //ORDER BY order_id,start_datetime, pri.id;
    }
    public static function queryPendingPartsOLD(){
        return  sprintf('
                SELECT  p.id, p.file_id, p.file_part_name, p.version_part_name,p.order_id,p.final_product_id,state_id,p.weight,f.name as file_name,f.estimated_printing_hours,f.estimated_printing_minutes,f.filament_used,p.start_datetime,p.initiated
                FROM    part AS p
                INNER   JOIN file AS f ON p.file_id = f.id
                WHERE   state_id = 8
                ORDER BY   f.estimated_printing_hours DESC, p.order_id,p.final_product_id  ASC,  f.estimated_printing_minutes DESC, p.id;
                ');
    }
    
}
?>