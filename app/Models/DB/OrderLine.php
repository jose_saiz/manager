<?php

namespace App\Models\DB;

class OrderLine extends DBModel
{
    //
    protected $table = 'order_line';
    protected $fillable = [ 'quantity','order_id','product_id','price'];
    public function product()
    {
        return $this->hasOne(Product::class,'id','product_id');
    }
    public function customer()
    {
        //No funciona
        return $this->hasOneThrough(Customer::class,Order::class,'customer_id','id','order_id','id');
        
    }
    public function order()
    {
        return $this->hasOne(Order::class,'id','order_id');
    }
    /*
    public function order()
    {
        return $this->belongsTo(Order::class,'id','order_id');
    }
    public function products()
    {
        return $this->belongsTo(Product::class);
    }
    */

}
