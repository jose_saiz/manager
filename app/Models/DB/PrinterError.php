<?php

namespace App\Models\DB;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PrinterError extends Model
{
    use HasFactory;
    
    protected $fillable = ['id','name','remark','active','created_at','updated_at'];
    
    protected $table = 'printer_error';
    
}
