<?php

namespace App\Models\DB;


class FinalProduct extends DBModel
{
    //
    protected $table = 'final_product';
    protected $fillable = [ 'product_id','order_line_id'];
    
    public function product()
    {
        return $this->hasOne(Product::class);
    }
    
    public function parts()
    {
        return $this->hasMany(Part::class);
    }
    
    
}
