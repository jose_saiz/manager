<?php

namespace App\Models\DB;

use App\Vendor\Caffeinated\Shinobi\Models\Permission;
use App\Vendor\Caffeinated\Shinobi\Models\Role;

class PermissionRole extends DBModel
{
    protected $table = 't_permission_role';

    public function permission()
    {
        return $this->belongsTo(Permission::class);
    }

    public function rol()
    {
        return $this->belongsTo(Role::class);
    }
}
