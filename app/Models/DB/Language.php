<?php

namespace App\Models\DB;

use App\Vendor\Saizfact\Database\CacheQueryBuilder;


class Language extends DBModel
{
    /**
     * For Caching all Queries.
     */
    use CacheQueryBuilder;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_language';
}
