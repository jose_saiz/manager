<?php

namespace App\Models\DB;

use App\Vendor\Saizfact\Database\CacheQueryBuilder;

class Imagen extends DBModel
{
    /**
     * For Caching all Queries.
     */
    use CacheQueryBuilder;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_imagen';

    public function mediaItem()
    {
        return $this->belongsTo(Media::class, 'entity_id');
    }

    public function scopeGetImages($query, $entity_type, $entity_id){
        
        return $query->where('entity_id', $entity_id)
            ->where('entity_type', $entity_type);
    }

    public function scopeGetAllByLanguage($query, $entity_type, $rel_profile){
        
        return $query->where('rel_profile', $rel_profile)
            ->where('entity_type', $entity_type);
    }
    
    public function scopeGetAllByLanguageForId($query, $entity_type, $entity_id, $rel_profile){
        
        return $query->where('rel_profile', $rel_profile)
        ->where('entity_type', $entity_type)
        ->where('entity_id', $entity_id);
    }

    public function scopeGetPreviewImage($query, $entity_type, $entity_id, $rel_profile){
        
        return $query->where('entity_id', $entity_id)
            ->where('entity_type', $entity_type)
            ->where('rel_profile', $rel_profile)
            ->where('grid', 'preview')
            ->select('path','filename');
    }
}
