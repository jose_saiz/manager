<?php

namespace App\Models\DB;


class Factory extends DBModel
{
    //
    //protected $primaryKey = array('file_id','version_id');
    protected $table = 'factory';
    protected $fillable = ['name','address','phone','email','active'];
    
    public function users()
    {
        return $this->hasMany(User::class,factory_id,id);
    }
    
    
}
