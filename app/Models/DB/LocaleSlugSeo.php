<?php

namespace App\Models\DB;

class LocaleSlugSeo extends DBModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_locale_slug_seo';
}
