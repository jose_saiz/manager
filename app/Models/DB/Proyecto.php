<?php

namespace App\Models\DB;

use Illuminate\Database\Eloquent\Model;

class Proyecto extends Model
{
    //
    protected $table = 't_proyecto';
    protected $fillable = ['nombre','activo','orden','categoria_id'];
    
    public function categoria()
    {
        return $this->belongsTo(ProyectoCategoria::class);
    }
    public function folders(){
        return $this->hasMany(Folder::class,'entity_id')->where('entity_type','curso');
    }
    public function media()
    {
        return $this->hasOne(Media::class,'proyecot_id');
    }
    public function mediasGaleria()
    {
        return $this->hasMany(Media::class,'proyecto_id')->where('tipo_id','3');
    }

    
}
