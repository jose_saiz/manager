<?php

namespace App\Models\DB;

class Folder extends DBModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_folder';

    public function scopeGetImages($query, $entity_type, $entity_id){
        
        return $query->where('entity_id', $entity_id)
            ->where('entity_type', $entity_type);
    }

    public function scopeGetLargeImage($query, $entity_type, $entity_id){
        
        return $query->where('entity_id', $entity_id)
            ->where('entity_type', $entity_type)
            ->where('anchor', 'es.lg')
            ->select('name','filename');
    }
}
