<?php

namespace App\Models\DB;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Employee extends DBModel
{
    use HasFactory;
    
    protected $fillable = ['id','id_employee_type','name','active','id_factory'];
    
    protected $table = 'employee';
    

    
    public function scopeFactoryEmployees($query,$id_factory){
        
        return $query   ->where('id_employee_type', 1)
                        ->where('id_factory', $id_factory);
    }
    
}
