<?php

namespace App\Models\DB;

class RoleUser extends DBModel
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_role_user';
}
