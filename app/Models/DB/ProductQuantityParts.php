<?php

namespace App\Models\DB;


use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

class ProductQuantityParts extends DBModel
{
    //
    protected $table = 'product_quantity_parts';
    protected $fillable = [ 'product_id','file_id','file_part_name','version_part_name','quantity','checklist'];
    /*
    public function product()
    {
        return $this->hasOne(Product::class);
    }
    
    public function  part(){
        return $this->hasOne(Product::class);
    }
    */
    
    
    ///////////////////////////////////////////////////// SCOPES /////////////////////////////////////////////////////////
    
    public function scopeGetFileInformation($query,int $product_id){

        return DB::select('
                    SELECT p.file_id, p.file_part_name, p.version_part_name, p.quantity, f.quantity AS quantity_print_x_order_line,filament_used AS weight
                    FROM    product_quantity_parts AS p
                    INNER JOIN file AS f ON f.id =  p.file_id
                    WHERE product_id = ' . $product_id. '
                    ORDER BY p.id');
        
        
        
    }
    

}
