<?php

namespace App\Models\DB;

class Customer extends DBModel
{
    //
    protected $table = 'customer';
    protected $fillable = [ 'name','address','phone','email'];
    
    public function orders()
    {
        return $this->hasMany(Order::class);
    }
    
    public function final_products()
    {
        return $this->hasMany(FinalProduct::class);
    }
    

    
}
