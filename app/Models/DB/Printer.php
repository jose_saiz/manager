<?php

namespace App\Models\DB;


class Printer extends DBModel
{
    //
    protected $table = 'printer';
    protected $fillable = [ 'id','code','neighborhood_factory_id','filament_id','state_id','name','remark',
                            'roll_end_datetime','roll_replacement_datetime','roll_weight',
                            'nozzle_diameter','available','category_id'];
    
    public function category()
    {
        return $this->belongsTo(Category::class)->where('category_type_id',1);//Type Printer
    }
    
    
    public function parts(){
        return $this->hasMany(Part::class,'printer_id','id')->orderBy('start_datetime','Asc');
    }
    
}
