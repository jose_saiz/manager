<?php

namespace App\Models\DB;

class Part extends DBModel
{
    
    
    
    protected $table = 'part';
    protected $fillable = [ 'file_id','file_part_name','version_part_name','order_id','final_product_id','filament_id','printer_id','state_id',
                            'name','start_datetime','end_datetime','weight','initiated'];
    
    public function file()
    {
        return $this->hasOne(File::class,'id','file_id');
    }
    public function printer()
    {
        return $this->hasOne(Printer::class,'id','printer_id');
    }
    public function state()
    {
        return $this->hasOne(State::class,'id','state_id');
    }
    
    public function order()
    {
        return $this->hasOne(Order::class,'id','order_id');
    }
    /*
    public function final_product()
    {
        return $this->hasOne(FinalProduct::class);
    }
    public function order()
    {
        return $this->hasOne(Order::class);
    }
    public function product_quantity_parts()
    {
        return $this->hasOne(ProductQuantityParts::class);
    }
    */

    
}
