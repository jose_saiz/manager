<?php

namespace App\Models\DB;

class Product extends DBModel
{
    //
    protected $table = 'product';
    protected $fillable = [ 'name','remark','active','price'];
    /*
    public function final_products()
    {
        return $this->hasMany(FinalProduct::class);
    }
    public function product_quantity_parts()
    {
        return $this->hasMany(ProductQuantityParts::class);
    }
    */

}
