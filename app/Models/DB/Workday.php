<?php

namespace App\Models\DB;


class Workday extends DBModel
{
    //
    protected $table = 'workday';
    protected $fillable = [ 'neighborhood_factory_id','day','begin_time','end_time','category_id','active'];
    
    public function category()
    {
        return $this->hasOne(Category::class,'id','category_id');//Type Printer
    }
    
    
}
