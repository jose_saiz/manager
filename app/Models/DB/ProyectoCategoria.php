<?php

namespace App\Models\DB;

use Illuminate\Database\Eloquent\Model;

class ProyectoCategoria extends Model
{
    //
    protected $fillable = ['id','nombre','activo'];
    
    protected $table = 't_proyecto_categoria';
    
    
    public function proyectos()
    {
        return $this->hasMany(Proyecto::class, 'categoria_id');
    }
}
