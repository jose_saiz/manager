<?php

namespace App\Models\DB;

use App\Vendor\Saizfact\Database\CacheQueryBuilder;

class Locale extends DBModel
{
    /**
     * For Caching all Queries.
     */
    use CacheQueryBuilder;
    
    protected $table = 't_locale';

    public function scopeGetValues($query, $rel_parent, $key){
        
        return $query->where('key', $key)
            ->where('rel_parent', $rel_parent);
    }

    public function scopeGetIdByLanguage($query, $rel_parent, $rel_profile, $key){
        
        return $query->where('key', $key)
            ->where('rel_profile', $rel_profile)
            ->where('rel_parent', $rel_parent);
    }

    public function scopeGetAllByLanguage($query, $rel_parent, $rel_profile){
        
        return $query->where('rel_profile', $rel_profile)
            ->where('rel_parent', $rel_parent);
    }

    public function scopeUpdateRelParent($query, $older_parent, $new_parent){ 
        return $query->where('rel_parent', $older_parent)
            ->update(['rel_parent' => $new_parent]);
    }
}
