<?php

namespace App\Models\DB;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    //
    protected $table = 't_test';
    protected $fillable = ['nombre','activo','orden','categoria_id'];
    
    public function categoria()
    {
        return $this->belongsTo(TestCategoria::class);
    }
}
