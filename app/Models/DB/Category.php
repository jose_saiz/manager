<?php

namespace App\Models\DB;

use Illuminate\Database\Eloquent\Model;

class Category extends DBModel
{
    //
    protected $fillable = ['id','category_type_id','name','active','remark'];
    
    protected $table = 'category';
    
    
    public function Printer()
    {
        return $this->belongsTo(Printer::class, 'category_id');
    }
    public function Printers()
    {
        return $this->hasMany(Printer::class,'category_type_id', 1);
        //return $this->where('category_type_id', 1)->get(1);
    }
}
