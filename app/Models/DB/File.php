<?php

namespace App\Models\DB;


class File extends DBModel
{
    //
    //protected $primaryKey = array('file_id','version_id');
    protected $table = 'file';
    protected $fillable = [ 'file_id','version_id','name','checklist','filament_used',
                    'estimated_printing_time','estimated_printing_hours','estimated_printing_minutes','estimated_printing_hours_total','estimated_printing_minutes_total',
                    'quantity','quantity_x_printing','quantity_x_tower','slicing_program','parrameters','price','type_id','act','color','color_background','color_background'];
    /*
    public function parts()
    {
        return $this->hasMany(Parts::class,)->where('category_type_id',1);//Type Printer
    }
    */
    
}
