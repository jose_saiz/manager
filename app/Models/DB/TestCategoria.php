<?php

namespace App\Models\DB;

use Illuminate\Database\Eloquent\Model;

class TestCategoria extends Model
{
    //
    protected $fillable = ['id','nombre','activo'];
    
    protected $table = 't_test_categoria';
    
    
    public function tests()
    {
        return $this->hasMany(Test::class, 'categoria_id');
    }
}
