<?php

namespace App\Models\DB;

use App\Vendor\Saizfact\Database\CacheQueryBuilder;

class Config extends DBModel
{
    /**
     * For Caching all Queries.
     */
    use CacheQueryBuilder;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 't_config';

    public static function getConfig($key, $dominio)
    {
        $config = self::where('clave', $key);
        if ($dominio) {
            $config->where('dominio', $dominio);
        } else {
            $config->where(function ($query) {
                $query->whereNull('dominio')
                    ->orWhere('dominio', '');
            });
        }
        $config = $config->first();
        
        if (isset($config)) {
            return $config->valor;
        } else {
            return null;
        }
    }

    public static function getConfigOrGeneric($key, $dominio)
    {
        $val = self::getConfig($key, $dominio);
        if (! $val) {
            // Si no encontramos valor, miramos la clave sin dominio fijado
            $val = self::getConfig($key, null);
        }
        return $val;
    }
}
