<?php

namespace App\Models\DB;

class Order extends DBModel
{
    //
    protected $table = 'order_g';
    protected $fillable = [ 'state_id','customer_id','neighborhood_id','priority'];
    
    public function state()
    {
        return $this->hasOne(State::class,'id','state_id');
    }
    public function customer()
    {
        return $this->hasOne(Customer::class,'id','customer_id');
    }
    /*
    public function final_products()
    {
        return $this->hasMany(FinalProduct::class);
    }
    */
    public function parts()
    {
        //return $this->hasMany(Part::class,'order_id','id');
        return $this->hasMany(Part::class);
    }
    public function final_products()
    {
        //return $this->hasMany(Part::class,'order_id','id');
        return $this->hasManyThrough(
            FinalProduct::class, //Modelo destion
            OrderLine::class, //Model intermedio
            'order_id', // Clave foránea en la tabla intermedia
            'order_line_id', // Clave foránea en la tabla de destino
            'id', // Clave primaria en la tabla de origen
            'id'); // Clave primaria en la tabla intermedia
    }
    public function order_lines()
    {
        return $this->hasMany(OrderLine::class);
    }
    public function factory()
    {
        return $this->hasOne(Factory::class,'id','neighborhood_id');
    }

    
    

    
}
