<?php

namespace App\Models\DB;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Incident extends DBModel
{
    use HasFactory;
    protected $fillable = ['id','printer_error_id','employee_id','printer_id','part_id','file_id','description','hours','minutes','created_at','updated_at'];
    
    
    protected $table = 'incident';
}
