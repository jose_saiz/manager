<?php
namespace App\Console\Commands;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Console\ModelMakeCommand;
use Illuminate\Support\Str;

class SfModelMakeCommand extends ModelMakeCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sf:make:model';
    

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new Eloquent model class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Filesystem $files)
    {
        parent::__construct($files);
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        parent::handle();
    }

    /**
     *
     * {@inheritdoc}
     * @see \Illuminate\Foundation\Console\ModelMakeCommand::getStub()
     */
    protected function getStub()
    {
        return __DIR__ . '/../../Vendor/Saizfact/Illuminate/Foundation/Console/stubs/model.stub';
    }

    /**
     *
     * {@inheritdoc}
     * @see \Illuminate\Console\GeneratorCommand::buildClass()
     */
    protected function buildClass($name)
    {
        $stub = parent::buildClass($name);
        $stub = $this->replaceTable($stub, $name);
        return $stub;
    }

    /**
     * Replace the table name for the given stub.
     *
     * @param string $stub
     * @param string $name
     * @return string
     */
    protected function replaceTable($stub, $name)
    {
        $class = str_replace($this->getNamespace($name) . '\\', '', $name);
        $name = 't_' . Str::snake($class);
        //$name = Str::snake($class);
        return str_replace('DummyTable', $name, $stub);
    }
}
