<?php

namespace App\Exceptions;

use Exception;

use Illuminate\Database\QueryException;

class TestCategoriaException extends Exception
{

    public function report()
    {
        //
    }

    public function render(Exception $exception, $request){

        /*return response()->json( ['errors' => "Está intentando eliminar una categoría que está siendo usada por algunas 
        Test's, asigne una nueva categoría a las Test's afectadas para poder eliminar esta 
        categoría."],422);*/

        if ($exception instanceof QueryException) {

            return response()->json( ['errors' => "Está intentando eliminar una categoría que está siendo usada por algunas 
            Test's, asigne una nueva categoría a las Test's afectadas para poder eliminar esta 
            categoría."],422);

            switch ($exception->errorInfo[0]){
                
                case '23000':
    
                    return response()->json([
                        'errors' =>  "Está intentando eliminar una categoría que está siendo usada por algunas 
                            Test's, asigne una nueva categoría a las Test's afectadas para poder eliminar esta 
                            categoría."
                    ], 422);

                    break;
    
                default:
    
                    return response()->json([
                        'errors' => $this->errorInfo[2],
                    ], 422);
            }
        }
    }
}
